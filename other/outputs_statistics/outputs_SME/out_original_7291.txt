
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Vrabec
                                        &gt;
                Nezaradené
                     
                 Ako vznikla demokracia .... 

        
            
                                    25.5.2010
            o
            23:00
                        |
            Karma článku:
                5.56
            |
            Prečítané 
            949-krát
                    
         
     
         
             

                 
                    Čo nám o vzniku demokracie vraví Anastasia v ôsmej knihe .... nie je snáď citovaný príbeh príznačný pre  naše "naháňanie sa" za bubáčikmi?
                 

                     Démon Kracios       Pomaly šli otroci jeden za druhým a každý niesol otesaný kameň. Štyri rady, dlhé jeden a pol kilometra od kamenárov až po miesto, kde sa začínala stavba mestskej pevnosti, strážili dozorcovia.   Na desať otrokov pripadal jeden ozbrojený dozorca. Stranou od idúcich otrokov, na vrchole trinásťmetrovej umelej hory z otesaných kameňov, sedel Kracios, jeden z hlavných žrecov. Štyri mesiace mlčky pozoroval dianie dole. Nikto ho nerozptyľoval, nikto ani len pohľadom nesmel prerušiť jeho premýšľanie.   Pre otrokov a strážnikov sa stala umelá hora s trónom na jej vrchole súčasťou krajiny. A človeka, ktorý na nej raz nepohnute sedel, inokedy sa zasa prechádzal po plošine, si už nikto nevšímal.   Kracios si dal za úlohu prebudovať štát, upevniť na tisícročie moc žrecov, podriadiť im všetkých ľudí na Zemi a urobiť z nich svojich otrokov vrátane všetkých vládcov štátov.   Raz sa Kracios spustil dole, zanechajúc na tróne svojho dvojníka. Žrec sa prezliekol a prikázal hlavnému dozorcovi, aby mu nasadili okovy a zaradili ho za mladého, silného otroka, ktorý sa volal Nard.   Keď pozoroval tváre otrokov, všimol si Kracios, že pohľad tohto mladého človeka je hĺbavý a hodnotiaci, a nie rozptýlený a neprítomný ako u mnohých. Nardova tvár bola raz sústredená, zamyslená, inokedy vzrušená. „To znamená, že v ňom dozrieva nejaký plán," pochopil žrec, ale chcel sa uistiť, nakoľko zodpovedá jeho pozorovanie pravde.   Dva dni a dve noci pozoroval Kracios Narda, mlčky nosiac kamene. Na tretiu noc, keď prišiel povel "spať", otočil sa Kracios k mladému otrokovi a zašeptal s horkosťou a zúfalstvom v hlase akoby len tak pre seba: „Vari to takto pôjde ďalej po celý život?"   Žrec videl, že mladý otrok sa mykol a okamžite sa obrátil tvárou k žrecovi. Oči sa mu leskli aj v prítmí baraka.   „Dlho to takto nebude. Už mám skoro hotový plán. A ty, starček, sa môžeš na ňom tiež zúčastniť," zašepkal mladý otrok.   „Aký plán?" Ľahostajne a so vzdychom sa opýtal žrec.   Nard mu začal zapálene vysvetľovať:   „Aj ty, starček, aj ja, aj my všetci budeme onedlho slobodnými ľuďmi. Len si to zrátaj: na každú desiatku otrokov je jeden dozorca. A na pätnásť otrokýň, ktoré pripravujú jedlo a šijú šaty, je tiež jeden dozorca. Ak sa vo vhodnom čase všetci vrhneme na dozorcov, zvíťazíme nad nimi, aj keď sú oni ozbrojení a my v okovách. Je nás desať na jedného a okovy nám tiež môžu poslúžiť ako zbraň proti meču. Dozorcov porazíme, zviažeme a zbrane si vezmeme."   „Ach, mládenec," vzdychol si Kracios a akoby nezúčastnene povedal: „Tvoj plán nie je domyslený. Dozorcov, ktorí sú tu, môžeme odzbrojiť. Ale vládca pošle ďalších, možno celú armádu a pozabíja vzbúrencov."   „Aj o tom som rozmýšľal, starček. Musíme si vybrať taký čas, keď tu nebude armáda. A ten čas prichádza. Vidíme, že armáda sa chystá na pochod. Pripravujú proviant na tri mesiace. To znamená, že za tri mesiace príde armáda na bojisko. V boji zoslabne, hoci zvíťazí a privedie ďalších otrokov. Už pre nich stavajú nové baraky. Musíme začať s odzbrojovaním stráže hneď ako sa armáda nášho vládcu stretne v boji s druhou armádou. Poslovia budú potrebovať mesiac na to, aby priniesli správu o nevyhnutnosti jej návratu. Oslabená armáda sa bude vracať aspoň tri mesiace. Za štyri mesiace sa dokážeme pripraviť na zrážku. Nebude nás menej než vojakov v armáde. Zajatí otroci, keď uvidia, čo sa deje, sa určite budú chcieť pridať k nám. Všetko som dobre premyslel, starček."   „Hej, mládenec, s plánom, svojou mysľou dokážeš odzbrojiť strážnikov a zvíťaziť nad armádou," odpovedal žrec, tentoraz povzbudzujúco a dodal: „ale čo potom budú robiť otroci a čo sa stane s vládcami, strážnikmi a vojakmi?"   „O tom som veľa nepremýšľal. A napadá mi iba jedno: všetci, ktorí boli otrokmi, už nebudú otrokmi. A všetci tí, ktorí teraz nie sú otroci, sa stanú otrokmi," odpovedal Nard nie veľmi presvedčivo, akoby len nahlas premýšľal.   „A žreci? Povedz, mládenec, zaradíš žrecov k otrokom alebo k slobodným?"   „Žrecov? O tom som tiež nepremýšľal. Ale teraz mi napadá: nech žreci ostanú, ako sú. Počúvajú ich otroci i vládcovia. Hoci im niekedy ťažko rozumieť, myslím si, že sú neškodní. Nech rozprávajú o bohoch, ale ako si lepšie zariadiť život, to vieme my sami."   „Ako lepšie - to je dobre," odvetil žrec a robil sa, že sa mu chce strašne spať.   Ale Kracios v tú noc nespal. Premýšľal. „To je jasné," dumal Kracios, „najjednoduchšie by bolo povedať panovníkovi o sprisahaní. Chytia mladého otroka, lebo je zjavné, že on podnecuje ostatných. Ale to nevyrieši problémy. Otroci budú vždy túžiť vyslobodiť sa z otroctva. Objavia sa noví vodcovia, nové plány a už je to raz tak - hlavné nebezpečenstvo pre štát bude stále jestvovať vnútri štátu."   Kracios mal pre sebou úlohu: vypracovať plán zotročenia celého sveta. Chápal, že tento cieľ nedosiahne len pomocou fyzického násilia. Je nevyhnutné, aby sa na každého človeka, na celé národy pôsobilo psychologicky. Treba transformovať ľudskú myseľ, každému vsugerovať, že otroctvo je vyššie blaho. Musí sa spustiť program, ktorý sa bude ďalej samostatne rozvíjať a ktorý bude dezorientovať celé národy v priestore, čase a pojmoch.   Ale čo je najhlavnejšie - v adekvátnom vnímaní skutočnosti.   Kraciova myseľ pracovala čoraz rýchlejšie. Prestal si cítiť telo, ťažké okovy na rukách i nohách. A zrazu, doslova ako blesk z jasného neba, vznikol program. Ešte nebol podrobný a nie celkom jasný, ale pociťoval jeho spaľujúcu veľkoleposť. Kracios sa už cítil jediným vládcom sveta.   Žrec ležal na prični v okovách a nadchýnal sa sám sebou: „Zajtra ráno dám znamenie hlavnému dozorcovi, aby mi sňal okovy a premyslím podrobnosti svojho plán. Potom vyrieknem niekoľko slov a svet sa začne meniť."   To je neuveriteľné! Len niekoľko slov - a celý svet sa mi podriadi, podriadi sa mojej mysli. Boh dal človeku naozaj silu, ktorá vo Vesmíre nemá páru. Tou silou je ľudská myseľ. Ona tvorí slová a mení chod dejín.   Vytvorila sa neobyčajne vhodná situácia. Otroci pripravili plán povstania. Je racionálny a nepochybne ich môže priviesť k pozitívnemu prechodnému výsledku. Ale ja len pomocou niekoľkých viet donútim nielen ich, ale aj potomkov dnešných otrokov, ba dokonca aj pozemských vládcov, aby boli otrokmi nastávajúcich tisíc rokov.       Ráno mu na znamenie Kracia sňal hlavný dozorca okovy. A už na druhý deň pozval na svoju pozorovaciu plošinu ostatných päť žrecov a faraóna. Potom k nim Kracios prehovoril:   „To, čo budete teraz počuť, nesmie nikto zapísať ani ďalej rozprávať. Okolo nás nie sú steny a moje slová nikto okrem vás nebude počuť. Vymyslel som spôsob, ako zo všetkých ľudí žijúcich na zemi urobiť otrokov nášho faraóna. Nedá sa to dosiahnuť s pomocou početných vojsk ani úmorných vojen. Ale ja to dosiahnem pomocou niekoľkých viet. Už dva dni po ich vyslovení sa budete môcť presvedčiť, ako sa svet začína meniť."   Pozrite sa dole: dlhé rady otrokov spútaných v okovách nesú po jednom kameni. Stráži ich množstvo vojakov. Čím je viacej otrokov, tým lepšie pre štát - to sme si vždy mysleli. Ale čím je viacej otrokov, tým viac sa musíme obávať ich vzbury. Zosilňujeme stráž. Musíme dobre kŕmiť svojich otrokov, pretože ináč nebudú schopní robiť ťažkú fyzickú prácu. Ale aj tak sú leniví a majú sklony búriť sa.   Pozrite sa, ako sa pomaly hýbu a dozorcovia, ktorí tiež zleniveli, ich nepoháňajú bičom a nebijú dokonca ani zdravých a silných otrokov. Ale budú sa hýbať oveľa rýchlejšie a nebudú potrebovať ani dozorcov. A dozorcovia sa tiež zmenia na otrokov. A dosiahnuť sa to dá takto:   Nech sa dnes pred západom slnka rozhlási všade nariadenie faraóna:   "S úsvitom nového dňa darujem všetkým otrokom úplnú slobodu. Za každý kameň prinesený do mesta, dostane slobodný človek jednu mincu. Mince možno zamieňať za jedlo, odev, obydlie, palác v meste aj samotné mesto. Odteraz ste slobodnými ľuďmi."   Keď si žreci uvedomili, čo Kracios povedal, najstarší z nich riekol:   „Ty si démon, Kracios. To, čo si vymyslel, zdémonizuje množstvo pozemských národov."   „Nech som teda démon a to, čo som vymyslel, nech ľudia v budúcnosti nazývajú demokraciou."   Nariadenie na súmraku oznámili otrokom. Všetci užasli a mnohí v noci nespali, premýšľajúc o novom šťastnom živote.   Ráno nasledujúceho dňa vyšli žreci a faraón znova na plošinu umelej hory. Obraz, ktorý sa im naskytol, predčil všetky ich predstavy. Tisíce ľudí, bývalých otrokov, opreteky vláčili tie isté kamene ako predtým. Mnohí, upotení, niesli po dva kamene. Iní, ktorí niesli po jednom, bežali až sa za nimi prášilo. Niektorí dozorcovia tiež vláčili kamene. Ľudia, považujúc sa za slobodných - veď im sňali okovy, sa snažili získať čo najviac vytúžených mincí, aby si vybudovali šťastný život.   Kracios strávil na svojej plošinke ešte niekoľko mesiacov, spokojne pozorujúc, čo sa dole deje. A boli to kolosálne zmeny. Časť otrokov sa spolčila do neveľkých skupín, zostrojili vozíčky a naložiac ich doplna kamením, zaliati potom ťahali si tie vozíčky.   "Ešte vynájdu veľa zariadení," pomyslel si spokojne Kracios. Už sa objavili aj vnútorné služby: donášači vody a potravy. Časť otrokov jedla za chodu - nechceli strácať čas na cestu do baraka kvôli jedlu a platili donášačom mincami, ktoré dostali. Objavili sa aj lekári: ošetrujú poranených za chodu a tiež za mince. Vybrali si už aj dopravných dozorcov. Čoskoro si vyberú aj predstavených, sudcov. Nech si len vyberajú - predsa sa považujú za slobodných. Ale podstata sa nezmenila a tak ako predtým vláčia kamene...   A tak utekajú celými tisícročiami, v prachu, zaliati potom, vlečúc ťažké kamene. Aj dnes pokračujú potomkovia týchto otrokov vo svojom nezmyselnom behu...              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Nešťastie ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Mňam a mľask ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Dokonalý svet ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Podpultový tovar?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Pravda
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Vrabec
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Vrabec
            
         
        vrabec.blog.sme.sk (rss)
         
                                     
     
         Som tu na návšteve ... asi ako dieťa - pozorovateľ, aspoň sa o to stále snažím. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    450
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisník "športovca" ... :o)
                        
                     
                                     
                        
                            V MHD
                        
                     
                                     
                        
                            Láska
                        
                     
                                     
                        
                            Oplatí sa vidieť
                        
                     
                                     
                        
                            Čo ma teší ...
                        
                     
                                     
                        
                            Medzi ľuďmi
                        
                     
                                     
                        
                            Čo ma se... srdí ....
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ebola a iné vírusové ochorenia, ktoré strašia našu civilizáciu.
                     
                                                         
                       Prečo milujem svoju ženu?
                     
                                                         
                       Dobré časy, časť prvá: Kanab
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




