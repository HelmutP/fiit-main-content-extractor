
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Putra
                                        &gt;
                Nezaradené
                     
                 Pán Priemer, keby blbosť kvitla... 

        
            
                                    20.4.2010
            o
            22:22
                        (upravené
                20.4.2010
                o
                22:31)
                        |
            Karma článku:
                13.14
            |
            Prečítané 
            10861-krát
                    
         
     
         
             

                 
                    Je utorok, 20. apríla 2009, pár minút pred desiatou hodinou a ja sa snažím spamätať sa z tlačovky nášho geniálneho, neomylného a vždy pravdovravného premiéra. Samotný obsah tlačovky nebol síce až tak zaujímavý, keďže na jeho agresívne a klamlivé vyjadrenia som si už za tých pár rokov bohužiaľ zvykol, no aj v tejto časti sa nájde zopár perličiek, ktoré stoja za zmienku.
                 

                    Pre prehľadnosť ich uvádzam v bodoch:     1. „Máme úplne absolútne základný politický cieľ, ktorý spočíva v tom aby sa, tak ako tomu bolo v roku 2006, mala zachovať zásada, že novú vládu bude zostavovať víťaz parlamentných volieb.“   No nádhera ako sa patrí. Urobme celoštátne to, čo funguje už teraz v regionálnych voľbách, ergo, že víťaz berie všetko. Nie nadarmo sa vraví, že politika je umením možného, čo rozmenené na drobné znamená slová konsenzus, ale častejšie kompromis. Slová, ktoré sú nášmu premiérovi absolútne cudzie. Pre neho existujú len dva názory: Jeho a ten nesprávny. Ak by mala vládu vždy zostavovať len strana, ktorá vyhrá voľby, svet by bol doslova a do písmena plný totalitných režimov, lebo presvedčiť povedzme 25-30% voličov, z povedzme 50-60% zúčastnených (čo stačí spravidla na víťazstvo vo voľbách), tak by o osude krajiny mohlo rozhodnúť teoreticky aj nejakých 12,5% oprávnených voličov. Treba to snáď ďalej komentovať?   2. „Na pravej strane politického spektra chce päť politických subjektov zostaviť pravicovú vládu. My sme racionálni politici a musíme pripustiť aj takúto alternatívu.“   No konečne mu dochádza, že mu tečie do topánok. Najskôr sa snažil ignorovať SaS, aby im nedajbože neurobil reklamu a tváril sa, že nejestvujú. Lenže SaS stojí na programe, a nie na boji s niekým (čo je pre triedneho bojovníka úplne nepochopiteľné), a tak sa im podarilo dostať sa s preferenciami na úroveň okolo 9%. Následne pokračoval v paľbe na Dzurindu vo viere, že tým položí SDKÚ-DS na kolená... a čo sa stalo? Pravý opak: strana prešla revíziou vedenia, ktorú potrebovala ako soľ a zastabilizovala si voličov. Ešte pár takýchto kiksov a stane sa mu to, čo Mečiarovi v roku 98.   3. „Rovnako je úplne zrejmé, ako by sa takáto vláda so silným mandátom správala, pretože my už nie sme nepopísaný papier a je úplne evidentné, ako by sa orientovala hospodárska a sociálna politika takéhoto kabinetu.“   Tu vravel o sebe, resp. svojej vláde, pričom odpoveď je jasná každému. Že nie sú nepopísaným papierom je jasné. A na tom papieri sú jasne napísané ukradnuté miliardy, aj keď to už rátame v eurách (mýto, emisie, nástenka, učebnice, TIPOS, záchranky, atď., bolo by to nadlho). Hospodárska politika pre nich znamená naplánovať si rast HDP na nereálnej úrovni, aj keď všetci vedia, že pôjde do červených čísel, naplánovať k nemu pre istotu aj horibilný deficit, a aby toho nebolo málo, minúť neplánovane ešte viac, ako bolo napísané už v tak zle nastavenom rozpočte (veď po nás potopa). A sociálna politika? Stačí jedna analýza od pána Sulíka, ktorý si dal tú námahu a spočítal mesačný prínos Ficových „silných sociálnych programov“: „Keď teda je niekto dôchodca s nízkym dôchodkom, má dieťa, je raz za mesiac u lekára, kupuje si priemerne lieky, kúri plynom a kupuje knižky ostošesť, ušetrí až 221 Sk. Nedôchodcovia ušetria trochu menej, a síce 41 Sk.“ To myslím ako sociálna politika ani nepotrebuje komentár.   4. „Asi by nikto nepochyboval, že spojenie piatich pravicových strán znamená predovšetkým politickú nestabilitu. Sú to všetko strany, ktoré prešli rozpadom klubov, odchodom poslancov, nehovoriac o tom, že sú to strany, ktoré majú diametrálne odlišné postoje. Máme v tejto päťzostave strany, ktoré sú veľmi rigidné, pokiaľ ide o isté otázky rodiny. Na druhej strane máme politické strany, ktoré sú úplne liberálne a hovoria napríklad o zväzkoch homosexuálov alebo o legalizácii marihuany, kým iné strany sú veľmi prísne z hľadiska rešpektovania trestného zákona.“   K vete o rozpadoch politických klubov a odchodoch poslancov sa vrátim v závere článku, kde aj uvediem dôvod. Pán premiér si neuvedomuje, že politika je o diskusii a kompromise. V prípade predloženého návrhu zákona o reg.partnerstvách z roku 2001, ktorý predkladal poslanec SDĽ Ištván sa napríklad niektorí poslanci KDH(zrejme ich Fico pokladá za rigidných) zdržali hlasovania, niektorí poslanci SDKÚ-DS boli pre zmenu za a niektorí poslanci SDĽ, ktorých poslanec to predkladal, boli naopak proti. Výnimočná ukážka, že poslanci hlasovali podľa svojho presvedčenia a nie „ako moje vedomie a svedomie v straníckej centrále káže.“ Toto je podľa neho ale asi nestabilita: slobodný názor...bodaj by nie, veď ten vždy ohrozoval „stabilitu“ každej diktatúry. Neviem, kde počul, že niekto presadzuje legalizáciu marihuany, lebo ako doktor práva by mal vedieť, že dekriminalizácia(zvlášť v znení, v akom ju presadzuje SaS) má k pojmu legalizácia na míle ďaleko. Ďalej neviem, prečo spojil tieto dve otázky s rešpektom voči trestnému zákonu. Registrované partnerstvá s ním nesúvisia vôbec a návrh na zmenu trestného zákona znamená len toľko, že čin začne, alebo prestane byť trestným....s rešpektom voči trestnému zákonu, ktorý sa musí stále podľa potrieb spoločnosti reformovať to nemá nič.   Monológ pána premiéra pokračoval klasickými vyjadreniami, ktoré sú preňho typické, keďže mu ani po štyroch rokoch nezaplo, že už nie je v opozícii a strašil privatizáciou strategických podnikov, zdražovaním energií, diktátom monopolov a inými obľúbenými frázami, pričom ani na moment nezaváhal, že tú haldu arbitráží za ešte väčšiu haldu peňazí (zákaz zisku zdravotným poisťovniam, regulácia cien sieťových odvetví na prianie vlády a.i.) Slovenská republika bezpochyby vyhrá. Žiaľ si neuvedomuje, že medzinárodné arbitráže riešia inštitúcie, kde nie je pánom bohom Len On, alebo nejaký Harabin. A keď aj prehráme, tak to prehlási za spiknutie zlých imperialistických kapitalistov a dlhé prsty ekonomickej loby.   Potom nastúpila pre neho nepríjemnejšia časť tlačovky, a síce otázky novinárov. Ako obvykle neodpovedal ani na jednu, resp. odpovedal na niečo, na čo sa ho nikto nepýtal, čo žiaľ nie je len tento prípad, ale taktiež ako keď sa ho nikto nepýtal ani či tu chceme mať americký radar, alebo raketový systém, napriek tomu svoj svetonázor vytruboval kde sa len dalo. Za zmienku tým pádom stoja len dve z jeho reakcií na novinárske otázky (resp. skôr otázky ako také):   1.      Novinárskou otázkou bolo, prečo na svoju stranícku agitáciu a útoky voči iným stranám využil pôdu úradu vlády(čo som si ja všimol skôr ako tlačovka vôbec začala), kde sa tlačová beseda konala. Odpoveď bola presne v štýle jeho a jeho „hrubokrkejšieho“ predchodcu Mečiara. Tvrdil, že útok vyzerá podľa neho inak a je legitímne, ak si on, ako predseda vlády drží svoju pozíciu a varuje pred „hrozbami pre Slovensko“ z daného miesta.   2.      A toto je perla, ktorú som spomínal už vyššie a zámerne som si ju nechal na záver. Redaktorka sa ho spýtala, prečo obviňuje súčasnú opozíciu (a „hroziacu“ koalíciu) z rozpadov poslaneckých klubov, odchodov poslancov a následnej nestability, keď presne na tomto vznikol SMER a SNS a HZDS by o tom mohli skladať eposy. Pani redaktorka pripojila ešte jednu otázku, na čo ju však pán Génius zahriakol, povedal jej, nech si svoje politické názory nechá doma v obývačke za zatvorenými dverami a nech kladie otázky, na ktoré bude on odpovedať. Pointa nie je ani tak v tom, že išlo o legitímnu otázku, resp. otázky, ale to, že Fico sa okamžite po tomto vyjadrení zodvihol, pričom ho nasledovali jeho verní komparzisti (Maďarič, Kaliňák a Čaplovič), ktorí na tlačovke nepredniesli ani mäkké F a odpochodoval kade ľahšie, aby unikol potrebe venovať sa rozumnému dialógu a vlastnej minulosti.   Pán premiér, túto krajinu ste dostali na ekonomické dno, no osobne pokladám za horšie to druhé dno, a síce morálne. Táto tlačovka a úroveň politickej kultúry, ktorá na ňom bola prítomná to ukázala v plnej nahote. Toto podľa mňa nemôže urobiť žiadny človek, ktorý má v sebe kúska rozumu alebo slušnosti. Tu si pomôžem výrokom môjho dobrého priateľa: „Kdyby blbost kvetla, tak vy jste jak Flóra Olomouc!“ 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            „ROH už nie je čo bývalo“ alebo „Ako úplatné odbory Gorila zožrala“
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Sú skutočne protinožcami?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            „Rómska otázka“ nám podpaľuje podprdelník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Nie som fašista ani Kotlebovec!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Naštartujme štartovacie bývanie!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Putra
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Putra
            
         
        putra.blog.sme.sk (rss)
         
                                     
     
        Presvedčený pravicový liberál s úctou k životnému prostrediu. Som predseda OZ Hnutie Ganymedes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2162
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Oznam riaditeľa školy
                     
                                                         
                       Od neúspešného štrajku k štrajkovému prasiatku
                     
                                                         
                       My všetci sme prehrali
                     
                                                         
                       Rozdať si to s Robertom Ficom
                     
                                                         
                       Družstevná Malacky - ako Reportéri zavádzali verejnosť.
                     
                                                         
                       Kým sa pokonajú
                     
                                                         
                       Asistent z Gorily sa stal majetným
                     
                                                         
                       Vyšetrovanie Gorily – výnimka alebo pravidlo?
                     
                                                         
                       „ROH už nie je čo bývalo“ alebo „Ako úplatné odbory Gorila zožrala“
                     
                                                         
                       O tom, ako úradníkov v Bruseli zneužívajú lobisti zo Slovenska
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




