

 V priebehu jedného týždňa sa organizačný tím Tibor Javorek, Milada Balčáková a Linda Kohútová dohodol na termíne a mieste natáčania. Väčšina učinkujúcich, vrátane organizačného tímu sú ubytovaný v Mlynskej Doline a preto hľadalii priestory v tejto oblasti. V ústrety im vyšlo UPeCe so svojimi priestormi a študentská televízia MC2 v podaní Jakuba Seewalda, ktorý video natáčal a aj počítačovo upravil. Učinkujúci vo videu sú študenti a nie všetci sa pred natáčaním aj reálne poznali. 
 "Dôvody, prečo chceme povzbudiť ľudí k tomu, aby išli voliť sú bsiahnuté v samotnom spote. Myslíme si, že voľby sú dôležité, pretože to je jedna z najväčších možností akosa dá niečo zmeniť, pretože je to kultivovaný spôsob, ako vyjadriť svoj názor a pretože je potrebné uvedmoiť si, že nemožno kritizovať politikov alebo vládu, ak sa dotyčný dobrovoľne volieb nezúčastnil. Pre demokraciu je potrebná sloboda, ale aj zodpovednosť. Volič je vlastne zodpovedný za zákony a inštitúcie." hovorí Tibor Javorek. 
 Formu videa si zvolili kvôli tomu, že boli inšpirovaní už spomínaným videom "5more friends uncesored" a tiež preto, lebo sa im to zdá byť dynamické, pútavé, zábavné, krátke a tiež z toho dôvodu, že videom chceli osloviť hlavne mladých, ktorí dnes trávia vačšinu času za internetom, kde sa môže najjednoduchšie a voľne šíriť toto video. 
 I napriek tomu, že mnohí z účinkujúcich alebo z organizačného tímu už vedia, akú stranu budú voliť, toto video je nestranícke. Primárnym cieľom je, aby ľuďia išli voliť, aby ich hlas neprepadol s predpokladom, že volič sa rozhoduje slobodne a zodpovedne, takže si bude plne uvedomovať svoju zodpovednosť za to, že strana, ktorú volil je taká, aká je. Video má podporiť mladých ľudí, aby si uvedomovali dôležitosť aktívneho zúčastňovaňia sa na politickom dianí a to nie je len opčas volieb, ale hlavne počas celého volebného obdobia a to hlavne petíciami, prostestmi či pripomienkovaním návrhov zákonov, čím sa buduje politická kultúra u mladých, ktorá je nevyhnutná pre fungovanie demokracie nielen na Slovensku. 
 "Ak sa nám podarí osloviť aspoň jedného človeka, ktorý nebol rozhodnutý či ísť voliť alebo nie, potom to nebolo zbytočné. Aj keby to tak nebolo, veríme, že aj snaha, ktorá sa nekončí úspechom sa cení. Osobne môžem za seba povedať, že v tomto smere mám pokojné svedomie, že som sa snažil a že stále nabádam ľudí k politickej aktivite, ktorá sa prejavuje aj počas volieb." dodáva Tibor Javorek. 
 



 
 Štefan Moravčík 
 So súhlasom organizačného tímu projektu. 
   
   

