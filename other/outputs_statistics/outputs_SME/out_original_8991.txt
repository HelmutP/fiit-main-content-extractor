
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marta Novotná
                                        &gt;
                Práca, dôchodok, spomienky
                     
                 Mýlite sa a zabúdate, tak ako ja? Závisí to od veku? 

        
            
                                    17.6.2010
            o
            8:00
                        (upravené
                16.6.2010
                o
                21:13)
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            1862-krát
                    
         
     
         
             

                 
                    Nedávno sme si s kolegyňou vypočuli mládež ako sa rozprávajú o starcoch - rodičoch, ktorým už rozum neslúži a potom stvárajú rôzne hlúposti. Machrovali a predbiehali sa, kto povie vtipnejší príbeh o svojich starcoch a o ich omyloch a zabúdaní.  Moja mladá kolegyňa, niečo po dvadsiatke, mi na margo rozhovoru mladých, porozprávala o tom ako si minulý týždeň zabudla doma všetky doklady a došla do práce autom zo vzdialenej dediny do okresného mesta, bez nich. Takých príbehov, by sa v jej živote našlo ešte dosť a nielen v jej, ale aj v mojom. Zhodli sme sa na tom, že zabúdanie a omyly, to nie je o veku, ale o preťaženosti, momentálnej psychickej situácii a niekedy aj o zdravotnom stave jedinca.  Nakoniec moja priateľka, blogérka Janka, vyvolala vo mne, svojím článkom o manželovi, ktorý išiel omylom v nedeľu ráno do práce, spomienky na moje pracovné začiatky v Tatratoure, hneď po maturite. Bola som mladá a pod ťarchou povinností a aj choroby, som stvárala hlúposti. Potvrdzuje to naše domnienky, že vek tu nehrá hlavnú úlohu. Snáď vás pobavím mojím nasledujúcim príbehom.
                 

                 
  
   Bolo to hneď po maturite. Nastúpila som pracovať do cestovnej kancelárie Tatratour v Bratislave. V zimnom období, keď sme robili účtovné uzávierky a bolo veľa práce, ma navštívila nechcená viróza. Na PN-ku som nemohla, resp. nechcela, lebo cez uzávierku bolo nemysliteľné, aby som zostala doma.   Bola som najmladšia z kolektívu a brala som prácu veľmi vážne. Čo povedali nadriadení, to bolo sväté. Chodila som do práce biela ako stena, s teplotou. Už som takmer nevládala a  ani práca mi nešla tak ako by som chcela. Všimli si to aj kolegyne a šéfka a v jedno poobedie ma presvedčili, aby som išla domov skôr . Bývala som na priváte na Záluhoch. Horko-ťažko som prišla autobusom na Lamačskú cestu a potom som sa štverala hore kopcom, k bytovke, kde som bývala. Doma som si hneď ľahla.    Unavená a utrápená chorobou som zaspala. Zobudila som sa ráno, keď bola ešte tma niekedy po pol šiestej. Obliekla som sa, a vypila som si čerstvo uvarený čaj a zišla som dole na Lamačskú cestu, na autobusovú zastávku. Na zastávke stáli úplne cudzí ľudia,  teda nie tí, čo som ráno poväčšine stretávala. Únava a istotne aj teplota urobili svoje a nejako som to prehliadla a posadila sa do poloprázdneho autobusu a odviezla som sa do mesta. Vystúpila som a ťažkým krokom som sa vybrala smerom k cestovnej kancelárii.   Čudovala som sa, že čím idem bližšie k Diebrovmu námestiu, tak sa viac stmieva. Vôbec ma nenapadlo, že nie je ráno. Došla som k cestovnej kancelárii a už bola celkom tma. Vo vnútri cestovky bola tiež tma a dvere boli zatvorené. Zbytočne som nimi lomcovala. Pozrela som na hodinky a vtedy som prišla na to, že nie je ráno sedem hodín, ale večer sedem hodín.   Keď som si to uvedomila, tak som sa pred zamknutými dverami cestnej kancelárie, rozosmiala a slzy mi stekali od smiechu po tvári. Náhodní okoloidúci sa zastavovali a isto si mysleli, že som stratila rozum. Noó..., nie práve vtedy, ale predtým... chi,...:-) Dokonca, jedna pani podišla ku mne a spýtala sa ma, že čo sa mi stalo a či mi môže nejako pomôcť. Niektorí si mysleli, že som pripitá. Chi, chi,...smiešne,... ja a alkohol. :-) Ja som však nemala silu vysvetľovať, že som si pomýlila večer s ránom a že...  Myslela som už len na to ako sa rýchlo dostanem späť do postele.   Najbližším autobusom som sa vrátila domov. Ľahla som si a spala som až do rána. Ráno mi už bolo lepšie, a tak som sa vychystala do práce. Najskôr som však zapla rádio, aby som sa presvedčila, či je skutočne ráno, lebo vonku bolo ešte šero. Nechcela som zažiť ešte raz, zbytočnú večernú prechádzku do mesta. Skutočne bolo ráno a ja som sa vybrala do práce.   Po príchode do cestovky som svojím príbehom pobavila kolegyne, ktoré sa pri mojom rozprávaní smiali a sadali si od smiechu až na dlážku. Prvých zákazníkov sme prijímali rozžiarené a usmiate.   Zažili ste aj vy niečo podobné? Čo poviete? Sú omyly a zabúdanie len výsadou starých ľudí? Sama som sa presvedčila a už veľakrát, že to tak nie je. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (70)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Spoločná jeseň života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Požiar v susedovej chate
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Deň blbec, alebo ponáhľajúci sa predavač
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Kniha života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Mladý, nervózny vodič a starká
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marta Novotná
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marta Novotná
            
         
        martanovotna.blog.sme.sk (rss)
         
                                     
     
         Som optimista, ktorý niekedy odoláva tvrdej realite, niekedy sa vracia do minulosti, detstva a mladosti, ale snažím sa veselo a i vážne, žiť v tomto reálnom svete. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    200
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1438
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vtipné príbehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Automobil
                        
                     
                                     
                        
                            Doprava a dopr. predpisy
                        
                     
                                     
                        
                            Služby
                        
                     
                                     
                        
                            Foto - články
                        
                     
                                     
                        
                            Sviatky, kultúra a tradície
                        
                     
                                     
                        
                            Škola a spomienky
                        
                     
                                     
                        
                            Staroba
                        
                     
                                     
                        
                            Počítač a rodina
                        
                     
                                     
                        
                            Príroda a jedlo
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Žena, rodina, deti, spomienky
                        
                     
                                     
                        
                            Príroda a zvieratká
                        
                     
                                     
                        
                            Vzťahy
                        
                     
                                     
                        
                            Práca, dôchodok, spomienky
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Drogy
                        
                     
                                     
                        
                            Zdravie
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            články Viawebtour
                                     
                                                                             
                                            články Natankuj
                                     
                                                                             
                                            články Sme-Žena,
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vitamínová abeceda od Evy Krajmerovej (čítam so svojím vnukom)
                                     
                                                                             
                                            Varovanie z lásky od Venuši Indrákovej
                                     
                                                                             
                                            Slovensko v Šanghaji,Čína okolo nás od E.Krajmerovej,I.Magátovej
                                     
                                                                             
                                            Odsúdená na lásku, Tvoje oči sú cesta od Danky Janebovej
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Každú dobrú hudbu, podľa nálady.
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mnohé, práve v tom okamžiku pre mňa zaujímavé.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Žena-Sme
                                     
                                                                             
                                            www.viawebtour.sk
                                     
                                                                             
                                            www.natankuj.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Noc patrí básnikom
                     
                                                         
                       Ráno so slzou v oku
                     
                                                         
                       Ponuka
                     
                                                         
                       100 dní - veľa, alebo málo v živote človeka ?
                     
                                                         
                       Koncom týždňa
                     
                                                         
                       Píšem list v machu a papradí
                     
                                                         
                       Spoločná jeseň života
                     
                                                         
                       Požiar v susedovej chate
                     
                                                         
                       Náš krásny a predsa prázdny svet
                     
                                                         
                       Dotkni sa ma
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




