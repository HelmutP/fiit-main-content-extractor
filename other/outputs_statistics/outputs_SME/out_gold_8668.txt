

 pridať Quick Launch toolbar. Keď som ho presúval po lište, našiel som pre neho skvelé miesto. 
  
 Predtým som mal na lište “pripnuté” ikony viacerých programov, ak som ešte spustil zopár ďalších, ktoré neboli “pripnuté” musel som posúvať ponuku keď som chcel prepínať medzi oknami (..áno, poznám klávesovú skratku Alt+Tab ;) ). Teraz mám potrebné ikony programov v Quick Launch toolbar a vedľa neho dostatok priestoru pre zobrazenie otvorených okien. 

