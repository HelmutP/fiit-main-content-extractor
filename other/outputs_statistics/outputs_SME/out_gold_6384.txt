

  
 Nový zvuk a smerovanie naznačil singel "Už", ktorý sa stal hitom rádiového éteru. Je veľmi silným úvodom nového Richardovho albumu. Chytľavá melódia s príjemnou atmosférou a ľahko zapamätateľným refrénom je tým najlepším začiatkom. Videoklip k tejto skladbe je  pripravený na premiéru a podľa prvých krátkych upútaviek bude patriť medzi top klipy vyprodukované na Slovensku. 
 Nový album je veľmi príjemný na počúvanie. Nostalgia, komorná atmosféra, pomalšie tóny a výrazné krátke výstižné texty. Album má niekoľko silných momentov, ktoré vo mne zanechali veľmi dobrý dojem. Melanchóliu naordinoval do hudby a textov, ktoré sú odrazom Richardovej osobnosti a človečiny. 
 Je vyzretým spevákom, ktorému sa dá ľahko uveriť. V jeho speve sú precítené všetky slová. Nové výpovede, otvorená duša hovoria o človeku, ktorý padol na dno. V textoch rezonujú retro myšlienky na lásku a vzťahy. Pre mňa osobne najviac trafil klinec po hlave v piesňach "Čiara", "Sviečky", "Nočný vták", "Netreba". Mnohým z nás v nich nastavil pravdivé zrkadlo. Krútiac hlavou len očami prechádzam jednotlivé slová a musím mu dať za pravdu. 
 "Každý zlomok života/Predstavuje hodnotu/Ktorá v danom momente/Zodpovedá životu/Moje zlomky niekedy/Nedajú sa prečítať/Je to matematika/Pre človeka zložitá/Rátaj s tou hrubou čiarou/Pod ňou a nad ňou som ja sám," 
 Ak by niekto čakal hity formátu „Milovanie v daždi, Tlaková níž" bude možno sklamaný. Sám som zvedavý v akej zvukovej podobe bude Richard predstavovať tento album na koncertnom turné. Intimita albumu skôr zvádza ku klubovej atmosfére. 
 Album je veľkým svetom myšlienok, ku ktorým sa budem veľmi rád vracať 
   

