
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marie Stracenská
                                        &gt;
                o deťoch
                     
                 Šťastné konce 

        
            
                                    5.5.2010
            o
            21:19
                        |
            Karma článku:
                7.01
            |
            Prečítané 
            670-krát
                    
         
     
         
             

                 
                    „Maminka! Oni ju zachránili! Princeznú zachránili, Fantázia nezmizne!" Matúškovi žiarili oči ako málokedy. Kričal od vzrušenia a od radosti. Práve sa skončil Nekonečný príbeh, a ako vieme my veľkí, skončil sa dobre. Je úžasné, ako to malý prežíva, pomyslela som si. Aký nedotknutý ešte je, zdieľala som radosť s ním.
                 

                 A popoludnie mi ten deň prinieslo ešte jednu skúsenosť, z rovnakého súdka.   Draci, oheň a ľad. Už na pohľad vyzeral a znel názov rozprávky (či čoho) strašidelne, ale keď deti chcú, tak chcú. Sadla som si k počítaču s nimi - takto to budem mať pod kontrolou a keď bude najhoršie, zakročím a vypnem to. Zvláštna animácia, ale rýchly spád. A hlavne bojovníci, princ aj princezná, draci a čarodejník - skrátka, rozprávka mala všetky atribúty, aby sa mojim malým páčila. Sledovali sme so záujmom, až do momentu posledného boja. A ako to už býva, kladní hrdinovia doňho vstupovali s hendikepom a hodnú chvíľu to s nimi vôbec nevyzeralo dobre. „Maminka! Bojím sa! Už nechcem pozerať!" Matúškov hlas znel dosť zúfalo, už som siahala po myši - ale ozvala sa Julinka: "Nie! Mne sa to páči! Nechaj to!" Film som stopla a začala vyjednávať. Nie, Matúško sa nepôjde hrať do izbičky, kým sa to neskončí, aby sa Julka do pozerala sama. Nech nepozerá ani ona. Ale ona chce! „A čoho sa bojíš, Matúško? Veď ten drak je dobrý, to vieš." „Že im čarodejník ublíži...", dostal zo seba potichučky. „A keď to pretočím na koniec a uvidíš, že sa to dobre skončí, môžeme to potom zas vrátiť a dopozerať?", napadlo mi. Matúškovi sa rozsvietila tvárička a súhlasil. Tak sme si pozreli dej o desať minút vpredu, ktorý ukázal happy end a potom sme sa zas vrátili do centra bojov.   Vtedy som si uvedomila, aký je detský svet nádherný aj v tom, že ešte nie je plný klišé a cynizmu. Už o pár rokov zrejme dieťa kývne rukou a nebude čakať na koniec - bude ho tušiť. Dôležitejšia než dej bude forma. Tá bude zaujímavá. A tak ako ostatné veľké deti, aj to moje bude vedieť, že rozprávky sa končia víťazstvom dobra. Zatiaľ to malí nevedia. Svet len objavujú. Kiežby im to objavovanie trvalo ešte veľmi dlho...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Vianoce vo štvrtok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Nepoznám žiadne dieťa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Ranná káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Cez hrádzu zrýchlene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Poschodová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marie Stracenská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marie Stracenská
            
         
        stracenska.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som žena. Novinárka, lektorka a konzultantka. Mama nádherných a šikovných dvojčiat. Manželka, dcéra, sestra, priateľka. Neľahostajná. Mám rada svet. Rovnako rýchlo sa viem nadchnúť, potešiť a rozosmiať ako pobúriť a nahlas rozhnevať. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    701
                
                
                    Celková karma
                    
                                                12.18
                    
                
                
                    Priemerná čítanosť
                    1779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o deťoch
                        
                     
                                     
                        
                            o vzťahoch
                        
                     
                                     
                        
                            o kadečom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Moja rozprávka
                     
                                                         
                       Zaťovia
                     
                                                         
                       Nápadník
                     
                                                         
                       Univerzitná nemocnica vyriešila problém Richterových prezervatívov
                     
                                                         
                       Obrovská stará dáma
                     
                                                         
                       Tutoľa máme kopčok
                     
                                                         
                       Bojovníčka
                     
                                                         
                       Vifon
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




