

 Má však  obavu o svojho manžela. Dedko pribral 5 kg. Ale to som tu ešte nebola, keď pribral. A tak mi zakázala vyvárať mu veľa. Len jedno jedlo a dosť. Ak bude mať polievku, potom už nič a naopak. Vraj sa málo pohybuje, čo je pravda. Jeho zdravotný stav mu to ani nedovoľuje. A ja tak rada varím! Výhodou je, že budem mať raz-dva navarené, ale ja by rada variť pre celý regiment dedkov. Musím sa polepšiť, no a potom aj ja schudnem. Už sa teším. 
 Keď sme sa o tom všetkom pri raňajkách rozprávali, povedala som mu, že oddnes ideme chodiť po byte na prechádzky, každý deň aspoň 15 min. Dedko zvážnel a povedal nie. Tak som mu navrhla 5 minút. Súhlasil. Tak som zvedavá. 
 Potom som im povedala 1. moju historku po nemecky (už sa lepším) :-). Ako mi môj manžel často hovoril, že som tučná a ja som mu raz na to odpovedala, keď mi to už hovoril pričasto: - To nie je veľký problém. Ak som pre teba tučná, nuž si budem musieť nájsť väčšieho manžela. 
 A keď som videla, že sa pri mojej historke aj smejú, tak som vedela, že mi začínajú rozumieť. 
   
 Tak som spokojná. Moji "dedkovci" sú ako moji rodičia. Jednoducho, padli sme si do oka už na diaľku. Už vtedy, keď som dostala z agentúry ponuku do Berlína a prečítala som si všetko o ňom. 
 Len som netušila, že dedko bude voňať ako dieťa... :-) 
 ...dedko vonia ako dieťa napriek tomu, že už neudrží moč... 
   

