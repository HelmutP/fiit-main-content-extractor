
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Spálová
                                        &gt;
                -Ulica-
                     
                 Prechádzka k jazeru 

        
            
                                    14.5.2010
            o
            18:36
                        (upravené
                1.6.2010
                o
                0:13)
                        |
            Karma článku:
                3.61
            |
            Prečítané 
            749-krát
                    
         
     
         
             

                 
                    Slnečné lúče už boli na ústupe, no s mojou sestrou Emou sme chceli ísť ešte na nejakú menšiu prechádzku neďaleko k jazeru. Vonku bolo pomerne teplo. Hodili sme na seba len letné šaty a Ema nezabudla na jej obľúbené sandále. Dostala ich od mamy k sedemnástym  narodeninám. Ešte si pamätám na jej iskričky v očiach. Akoby to bolo včera. Škoda. Odvtedy prešlo už dosť času, ale mama by bola na nás určite hrdá. Ema vždy tvrdila, že mama sa na nás pozerá zhora a teší sa z každého nášho úspechu. Aj ja sa pokúšam tomu uveriť. Večer sa dívam na oblohu, s nádejou, že zazriem jej obraz. Zatiaľ márne. Vždy uzriem maximálne padajúcu hviezdu. Síce, možno mi dáva znamenie. Neviem.
                 

                   
  ,,Veronika, ešte sa cestou zastavte u tety Žofie, upiekla vám nejaké koláče na cestu.“ počula som, ako na nás otec kričí z domu. Potešilo ma to. Teta Žofka robila vždy najlepšie ,,pagáče“ . Vždy vedela, čo je pre nás najlepšie. V živote mi viackrát pomohla. Mama sa nahradiť nedá, no ona mi naozaj za tých šesť rokov dodala vždy energiu na ďalší výkon.    Keď sme k nej prišli, bola k nám opäť milá. Dokázala byť aj prísna, ale vždy iba pre naše dobro. Ema bola už nedočkavá, chcela byť už pri jazere. Západ slnka sa blížil. Rozlúčili sme sa a popriali jej všetko dobré.    ,,Stihli sme to!“ užasnuto na mňa zvolala Ema, keď sme dorazili. Romantické srdce získala určite od mamy. Často sme sa s ňou chodievali pozerať na západ. Napĺňalo nás to šťastím a pokojom. Najkrajšie na tom boli určite spomienky na ňu. Bola veľmi krásna a dobrá. Starala sa o nás s oddanosťou a láskou. Ema veľmi ťažko prežívala jej odchod. Každý večer som ju počula plakať. Vedela, že by som sa trápila, preto mlčala. Teraz je to určite  lepšie. Oveľa viac sa smeje a život si užíva plnými dúškami.    Na druhej strane jazera sme odrazu počuli nejaký hluk. Niečo ,,treslo“ na zem. Muselo to byť niečo plechové alebo podobného typu. Obidve sme sa v reflexe na seba pozreli a utekali k miestu, kde sme začuli hluk. Všade naokolo bolo pusto. Nikde sme nič nepočuli. ,,Žeby ticho pred búrkou?“ pomyslela som si vystrašene. Ešteže nám nesvietili hlavy od strachu, ako som to videla v jednej rozprávke. Ema bola predomnou a ukazovala mi cestu, keďže ja som večný ,,strachoput“. Odrazu si prstom zakryla ústa, znamenie, že mám byť ticho. Tišie sa už zrejme ani nedalo, možno len môj dych bol badateľný.    Na zemi ležal akýsi muž a vedľa neho starší typ motorky. Hneď mi bolo jasné, čo spôsobilo ten buchot. Nevidela som mu poriadne do tváre, ale pôsobil neupraveným dojmom.  Hlavou mi prebehlo tisíco myšlienok.    ,,Poď sem Veronika, niečo som objavila.“  šepkala sestra z neďalekých kríkov a v ruke držala mobil s rozpísanou správou. ,,Ahoj Marti, odkáž mame, že dnes domov neprídem. Opäť som to trošku prehnal, ale to jej nehovor. Veď vieš, ako to skončilo minule. Momentálne som…“       ,,Neruším dievčatá?“  počula som hlas za mojím chrbtom.  Stál tam on, prebudil sa. Vyzeral ako z amerického filmu, niečo sa vo mne pohlo. V ruke som držala mobil. Nevedela som, či ho mám odhodiť alebo mu ho podať. Ostali sme ticho stáť s vystrašeným pohľadom v očiach.    ,,Keď sa nepohneváte, mobil si zoberiem, je totiž môj. Za veľa by ste ho aj tak nepredali, je v horšom stave, ako keď som ho kupoval.“ s úsmevom podotkol muž. ,,Nie nie, nie je to tak, ako si myslíte, my vám ho nekradneme.“ odvetila Ema. Muž bol už na odchode, prešliapoval z nohy na nohu. Zrejme bol už triezvejší, ako keď písal správu.    ,,Prepáčte, ani neviem ako to zo mňa vyhrklo, my sme len počuli hluk, a tak…“ ,,Nie nehnevám sa, skočil mi do reči, ale dávajte si pozor. Nikdy neviete, či vám teraz neublížím, čo ak som nejaký masový vrah? Možno vás len pozvem na kávu, za vďaku, že ste mi našli môj mobil. Prijali by ste?“ ,,Nie, ponáhľame sa už, odvrkla v rýchlosti Ema.“ ,,A prečo nie?“ povedala som. ,,Čo robím?“ pomyslela som si. Padol mi do oka v prvom momente, už sa nedá ustúpiť. Už som prijala ponuku, povedala som Eme, keď na mňa opýtavo pozrela.    ,,Choďte bezo mňa, otec sa už aj tak bude strachovať, odkážem mu, že sa zdržíš dlhšie.“ povedala Ema, a už sa zberala preč. ,,Ďakujem sestrička.“ pošepkala som jej ešte.    ,,Ja som Matúš.“ hovoril muž a už mi podával ruku a nakláňal sa po pusu na zoznámenie.   ,,Veronika, teší ma.“ usmiala som sa a nastavila líce a ruku.    Ešte prepísal nedopísanú správu sestre. Stálo tam, že sa zdrží dlhšie, a že ho čaká pekný večer. Pokračovanie nabudúce.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Spálová 
                                        
                                            Prax verzus teória študentov masmediálnych štúdií
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Spálová 
                                        
                                            Veríme médiám, ktoré sú skôr biznisová záležitosť ako informačná?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Spálová 
                                        
                                            Fejtón- Povolanie dcéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Spálová 
                                        
                                            Iné kafé- Košice 24.1.2011
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Spálová 
                                        
                                            7. september
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Spálová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Spálová
            
         
        spalova.blog.sme.sk (rss)
         
                                     
     
        Každý by mal denne počúvať trochu hudby, prečítať nejakú báseň, pozrieť si pekný obraz a pokiaľ možno povedať niekoľko rozumných slov.
(Johann Wolfgang Goethe)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    943
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            texty piesní
                        
                     
                                     
                        
                            To, čo si každý deň neuvedomíš
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            -Ulica-
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            bernátová erika
                                     
                                                                             
                                            Karol Gajdoš
                                     
                                                                             
                                            Lucy Ludvíková
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Anjeli a démoni-Dan Brown
                                     
                                                                             
                                            Drogy v ulicích-Andrew Tyler
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Viktória Morihlátková
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Satelity na satelitoch
                     
                                                         
                       Nájdi mi bývanie v Košiciach a ja ti kúpim poldeci
                     
                                                         
                       Stále len pôda
                     
                                                         
                       Prax verzus teória študentov masmediálnych štúdií
                     
                                                         
                       Pomohol by nám švajčiarsky systém?
                     
                                                         
                       Veríme médiám, ktoré sú skôr biznisová záležitosť ako informačná?
                     
                                                         
                       Fejtón- Povolanie dcéra
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




