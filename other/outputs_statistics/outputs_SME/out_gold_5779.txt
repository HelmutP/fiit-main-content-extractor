

 Ú S M E V 
  Päť písmen a pritom jedinečný pohyb pier ktorý je najväčšou výsadou nás ľudí. V smútku dokáže rozjasniť tvár, v plači osušiť slzy, v hneve vykúzli dobrú náladu a je najlepším liekom na bolesť v srdci. Nestojí nás nič a ked ho darujeme sme stále rovnaký, len možno ochudobnení o pár vrások menej na našej tvári. 
  Najkrajším pocitom na zemi je byť šťastným. No poznám ešte krajší pocit ako robiť šťastným samého seba a to je urobiť šťastným niekoho iného. Nie je to krásny pocit ked vám človek povie, že každý deň mu dokážete vyčariť úsmev na tvári? A že aj tomu najsmutnejšiemu človeku na svete práve vy dokážete vykúzliť úsmev na jeho skleslej tváričke? Poznám mnoho ľudí s množstvom postojov k životu. Ale to najkrajšie čo som kedy videla a mohla spoznať je človek ktorý už len pri pohľade na neho vo vás vyvoláva nepochopiteľný pokoj a úsmev na tvári. Stačí aby ste sa na takého človeka pozreli a váš pohľad na svet v danej chvíli sa otočí o 180 stupňov. Úsmev spontánny a prirodzený prejav ľudskej tváre.Toľko zázrakov v takom malom geste. Toľko čara v takom obyčajnom pohybe pier. Toľko nepochopiteľného šťastia v darovaní seba. Také spektrum pocitov na také malé úprimne vykrivenia pier ktoré sa ťažko chápe ľudmi. Toľko mágie v sebe ukrýva, že sa mi ťažko hľadajú slová a vyráža mi to dych. Prečo je zadarmo? Ved za takýto prejav by sa malo platiť to je ozajstná hodnota! Nie tie pseudohodnoty dnešného sveta! MILUJEM úsmev. A milujem ľudí ktorí ho dokážu rozdávať aj napriek tomu ako sa cíťia. Aj napriek tomu aké je veľké ich trápenie. Napriek tomu čo všetko musia znášať! Milujem a obdivujem ľudí ktorí sa vedia povzniesť a ukázať všetkým okolo, že úsmev je ten najkrajší dar na svete! Neviem nájsť nič krajšie ako úsmev od ľudí ktorí pre nás znamenajú veľa. Ved úsmev ma toľko podôb a toľko rôznych veci môže povedať. Je viac ako tisíc slov. A ukrýva tak mnoho tajomstiev. 
 
   Ďakujem za každý úsmev ktorý mi ktokoľvek daroval! 
 
   
 "Usmejte sa. A vo vašom úsmeve nech je mnoho dobroty a iba trošku, trošičku trpkosti. Odpusťte životu a svetu, že nie sú dokonalí." Dušan Radovič 

