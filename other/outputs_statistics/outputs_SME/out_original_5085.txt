
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viera Mikulská
                                        &gt;
                feministické okienko
                     
                 Ako ju zmeniť 

        
            
                                    23.4.2010
            o
            9:29
                        (upravené
                23.4.2010
                o
                11:20)
                        |
            Karma článku:
                17.58
            |
            Prečítané 
            3520-krát
                    
         
     
         
             

                 
                    Redakcia samizdatového magazínu Sme muži týmto vyjadruje svoje pobúrenie nad minulotýždňovým článkom v Sme ženy s názvom Ako ho zmeniť. Muža, samozrejme. Dojímavý príbeh o žene - úbohej obeti netvora nevykonávajúceho domáce práce a nevenujúceho sa starostlivosti o dieťa, "rady" psychologičky, ako takéto správanie preonačiť, a instantné pseudorafinované a najmä zbytočné návody, ako naložiť s nešťastníkom, ktorý značkuje byt použitými ponožkami, pivovými výparmi a požiadavkami na mamičkine buchty. Redakčnej rade sa vidí nespravodlivé, že muž je v takýchto lajfstajlových traktátoch vykreslený vždy ako slaboduchý, mierne agresívny prostáčik bez základných sociálnych a hygienických návykov, zatiaľčo žena sa prečítaním "článku" mení na bytosť prešibanú, šikovnú a múdru, plne disponovanú na to, aby úbožiaka nenápadne okresala do svojej šablóny. Redakcia Sme muži sa túto donebavolajúcu nespravodlivosť podujala aspoň trochu zmierniť.
                 

                 Milí muži. Vieme, že žena idúc do vzťahu verí, že svoju obeť pretvorí podľa jej predstáv, zatiaľčo vy dúfate v presný opak. Žiaľ, vaše očakávania sú často nereálne. Zmenil sa váš anjel - po nejakom čase, keď už ste dostatočne pevne lapení v osídlach partnerského použitia - na rozkysnutú fúriu? A nechce sa vám, resp. z objektívnych príčin (deti, spoločná hypotéka) nemôžete partnerku vymeniť? Ako ju znovu prerobiť do prijateľnej podoby, aspoň vzdialene pripomínajúcej pôvodný stav, vám poradí náš externý spolupracovník Mgr. Chosé z psychologickej poradne Budvar.   Ako sme na vlastnej, ťažko skúšanej koži mnohokrát pocítili, najčastejšie prejavy neblahej premeny žien počas párového spolužitia sú tieto:   1. pribrala   2. sťažuje sa na nedostatok pozornosti   3. vyžaduje "pomoc v domácnosti"   4. sťažuje sa na konzumáciu piva v domácnosti   5. sťažuje sa na konzumáciu piva v pohostinských zariadeniach   6. sťažuje sa na zápach   7. stále sa sťažuje   Na akúkoľvek otázku týkajúcu sa jej hmotnosti v žiadnom prípade neodpovedáme podľa reálneho stavu, nie sú prípustné ani najopatrnejšie vyjadrenia typu: no, trošíčku si možno pribrala, ale stále sa mi páčiš. Jediná prijateľná odpoveď je absolútne negovanie jej výroku. Čo sa týka zmeny hmotnosti samotnej, najlepšie urobíme, ak si zvykneme. Druhý bod väčšinou úzko súvisí s bodom štyri a päť, čiastočne aj šiestym. Podujmeme sa preto vyčariť ilúziu "romantickej večere", počas ktorej sa môžeme pokúsiť korigovať partnerkine neoprávnené výčitky. Nezabudneme na vhodnú atmosféru a dekorácie (Náhrobné kahance nie sú sviečky. Pivová fľaša nie je váza. Trojdňové ponožky žena z nepochopiteľných dôvodov nepovažuje za čisté.). Vo fáze, keď sa žena dostane do romantickej nálady je vhodné ukázať jej škaredý ekzém, ktorý sa nám vyhodil pri umývaní riadu (bod č. 3). Ako imitácia ekzému výborne poslúži odrenina od štrku, ktorú sme získali pri poslednej problematickej ceste z pohostinstva. Pri bode sedem sme žiaľ zatiaľ neprišli na žiadne účinné riešenie, čiastočne sa síce osvedčilo zvýšenie frekvencie činností z bodov 4 a 5, avšak podľa našich pozorovaní to problém s ufrflanou ženou v konečnom dôsledku iba prehĺbi.   Telefónne čísla na stomatochirurgiu sú vám k dispozícii v redakcii.   Milé Sme ženy. Teraz chvíľu vážne. Spriatelená redakcia Sme muži síce vie (aj keď nechápe prečo), že Sme u svojich čitateliek nepredpokladá IQ vyššie, ako má Hiltonkina čivava, napriek tomu vám nezištne ponúka koncept článku o "zmene" partnera, ak by ste sa ešte raz rozhodli rozsievať zrno múdrosti na tému vzťahy. Ak vám niečo na partnerovi vadí, je to výlučne váš problém, nie jeho. Nútil vás niekto rozhodnúť sa pre neho? Že "sa zmenil" je nezmysel, nezmenil sa, iba nesplnil vaše nereálne očakávania, alebo ste v počiatočnom hormonálnom opare prehliadli skutočný stav veci. Ak vám vadia v podstate maličkosti (nereálne očakávania vs normálny muž), buď ste sebastredné naivky nevhodné pre funkčný vzťah, alebo ste hlúpe, keď ste si mysleli, že sa z lásky k vám zmení. Ak vám vadia vážne problémy (nereálne očakávania plus regulérny idiot) ste hlúpe, keď ste si vybrali idiota. Každý má takého partnera, akého si zaslúži. (Redakcia z pochopiteľných dôvodov ostáva naďalej v ilegalite.)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (233)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Mikulská 
                                        
                                            Ako baliť ženy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Mikulská 
                                        
                                            Deľba práce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Mikulská 
                                        
                                            Úbožiatko moje vystresované
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Mikulská 
                                        
                                            Ako zabiť pavúka?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Mikulská 
                                        
                                            O evolúcii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viera Mikulská
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viera Mikulská
            
         
        mikulska.blog.sme.sk (rss)
         
                        VIP
                             
     
        Jamais. 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    78
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4356
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o všeličom
                        
                     
                                     
                        
                            tiež o všeličom
                        
                     
                                     
                        
                            feministické okienko
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            apoštol Brngar
                                     
                                                                             
                                            Ando
                                     
                                                                             
                                            flegmatix
                                     
                                                                             
                                            Samo
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




