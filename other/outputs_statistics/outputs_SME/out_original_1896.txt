
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viliam Búr
                                        &gt;
                Veci verejné
                     
                 Drahá súdružka, 

        
            
                                    25.11.2009
            o
            23:00
                        |
            Karma článku:
                10.28
            |
            Prečítané 
            6722-krát
                    
         
     
         
             

                 
                    (reakcia na list, ktorý som ráno našiel v schránke napriek nápisu „prosím nevhadzovať reklamu“)
                 

                  Milé dámy, vážení páni,  dovoľte mi, aby som Vám touto cestou podakovala za Vašu dôveru a hlasy, ktoré ste mi dali vo voľbách. Je pre mňa cťou, cítiť Vašu podporu a dodáva mi to nielen chuť do ďalšej prace, ale aj veľkú zodpovednosť.    nič z uvedeného nemám na svedomí. Zrejme ste mi tento list poslali omylom.    Vždy som sa snažila presadzovať v politike slušnosť a profesionalitu. O to viac ma mrzí, že aj župné voľby boli nástrojom na vulgárne útoky voči sociálnym demokratom a ich kandidátom.    Asi je v tomto liste omylov viac. Priznám sa, že vôbec netuším, o čom hovoríte. A keďže v liste ani nič konkrétne nespomínate, pohľadal som trochu na internete. Ale indícií ste mi naozaj poskytli veľmi málo.   Niekoľkokrát som si Váš list prečítal, hľadajúc pointu. Jediná pointa, ktorú som našiel, bola tá, že máme voliť Bajana a nemáme voliť Freša. Spomínaným kandidátom sociálnych demokratov je teda zrejme Vladimír Bajan. Ja som si to myslel už predtým, len viete... z jeho webovej stránky to príliš jasné nebolo. Ďakujem teda za vysvetlenie.   Ťažšie to bolo s hľadaním vulgárnych útokov. Nevedel som, či mám teraz do Googlu zadávať vulgarizmy, aby som sa dopátral k tomu, čo vo Vašom liste tajomne naznačujete. Dopátral som sa iba k nasledujúcim článkom:     Bajan pomohol súkromnej firme k obchodu storočia   Milionár potvrdil, že kšeft storočia vybavil Bajan     Články sú to zaujímavé, ale k ich obsahu sa teraz vyjadrovať nebudem. Rozhodne by som to ale neoznačoval za „vulgárne“ útoky. Viete, čo je to vulgárnosť? To je napríklad keď si vypočujete Vášho koaličného partnera Jána Slotu. Napríklad: „Ty p... ! Okamžite pusť môjho šoféra! Kto si ty, načo si tu? Ako sa voláš, ty p...!?“ alebo „Vyj... Maďari, všetci ste tu Maďari sku...ní a všade budem rozširovať, že títo vtáci, čo sem chodia, sú maďarskí vtáci!“ alebo „Tá pani, to je zrúda... Kysucká nána, ktorej proste šibe.“ (podľa webovej stránky Wikicitáty). Ak niečo podobné niekto písal aj o Vašom kandidátovi, pridávam sa k Vašej kritike – je to naozaj nemiestne a ľudia s podobným spôsobom vyjadrovania by vo verejných funkciách nemali čo robiť. A ani by tam neboli, keby ich iní nevolili a nebrali ako partnerov do koalícií. Ale veď viete, vrana k vrane sadá...    Nie náhodou sa to najsilnejšie prejavilo práve u nás, v Bratislavskom kraji. Zatiaľ čo my, sociálni demokrati, sme Vám, voličom, ponúkali konkrétne riešenia, naši kolegovia s SDKÚ-DS a najmä ich kandidát na župana Pavol Frešo nás bezdôvodne arogantne urážali.    Na chvíľku som Vám skoro uveril... ;-)    Vadí mi to nielen ako žene, ale aj ako človeku, ktorý sa v politike voči svojim politickým oponentom správal vždy slušne a korektne, aj keď som s ich názormi nesúhlasila.    Nikdy by mi nenapadlo, že sa do duelu „Frešo verzus Bajan“ dá zamontovať aj ženská otázka. Klobúk dolu pred Vašou profesionalitou! Nepochybne každej žene musí vadiť, keď nejakí hulváti pred voľbami... ehm, čo to vlastne urobili? aha... keď nejakí hulváti pred voľbami (dámy, na chvíľku prosím prestaňte čítať, nasledujú veci nehodné vašich nežných očičiek) uverejnia informácie o nákupe a predaji pozemkov v Petržalke (už končím, prosím pomôžte vzkriesiť dámy, ktoré pri čítaní predchádzajúcich slov zamdleli). Takéto veci by nežné pohlavie čítať nemalo – skôr nejakú poéziu, dačo o prírode, o kvetinkách... a voliť len tak, buď na základe ženskej intuície alebo podľa dojímavého listu, ktorý pošle iná žena, vážne znepokojená necitlivosťou tých mrzkých chlapov v politike.   (Hm, ženy a politika – teraz trochu odbočím od témy, ale pripomenulo mi to českú operu „Zítra se bude...“ o komunistickom politickom procese s Miladou Horákovou. Niečo takéto by si mal ísť pozrieť každý slovenský volič, vždy pred voľbami. Aby sme nezabúdali. Nezabúdali na to, akí sú komunisti, keď sa dostanú k moci. Ich slová znejú naivným ľuďom sladko; ich činy sú ohavné. Vraždy, mučenie, krádeže, lži. Keď človek pochopí podstatu komunizmu, pokiaľ má v sebe trochu slušnosti, povie si – nikdy viac! Nikdy viac tým sviniam nedám šancu a urobím, čo bude v mojich silách, aby všetci skončili v kriminále. Toto si samozrejme vyžaduje určitý stupeň morálky; morálne nezrelý človek by si napríklad prečítal tento odstavec a našiel by v ňom iba jedinú pohoršujúcu vec – že sa nejaký bloger opovážil použiť vo svojom článku slovo „sviňa“. Ach, tá hrôza! Je to rôzne; niekoho pohoršujú slová, niekoho zase veci nimi označované. Ale to som už zabŕdol do filozofických úvah, ktoré s dnešnou témou nemajú nič spoločné. Asi na mňa lezie nostalgia z nedávneho výročia 17. 11. 1989. Dvadsať rokov a stále sú medzi nami a ešte majú aj tú drzosť moralizovať. Fuj... Nechajme však už túto tému na pokoji a vráťme sa k pôvodnému listu.)    Aj preto budem v druhom kole volieb opäť voliť súčasného župana Vladimíra Bajana. Je to slušný a korektný človek, ktorý si vážil nielen svojich spolupracovníkov, kolegyne a kolegov, ale rešpektoval aj svojich oponentov. Nepolepil síce celú Bratislavu svojimi plagátmi, ale sú za ním konkrétne výsledky.    Asi som si opäť niečo nevšimol, ale v tých častiach Bratislavy, kde sa bežne pohybujem, majú obaja kandidáti reklamy zhruba narovnako. Pre istotu som si to dnes všímal, nevidel som, že by niektorý z nich mal v tomto smere prevahu.   
 Ďakujem Vám, že ste si môj názor precitali a verím, že aj Vy svojim hlasom prispejete k podpore slušného človeka Vladimíra Bajana.   PhDr. Monika Flašíková-Beňová  europoslankyňa a poslankyňa BSK 
   Ako som spomínal už na začiatku, tento list ste mi poslali omylom. Môžete však samozrejme veriť, čomu len chcete a nepochybujem, že mnohí voliči zase uveria Vám. Taký už je život. Niekedy zvíťazí pravda, inokedy sa zase viac darí takzvanej sociálnej demokracii. Uvidíme, ako to dopadne tentokrát.   Ozaj, aby nedošlo k nedorozumeniu, to oslovenie na začiatku tohto článku nemá vyjadrovať nejaké sympatie ani nič podobné; je to len akási narážka na to, že niektorí politici vyjdú svojich voličov pomerne draho. Tak treba dávať pozor, aby sme ich nabudúce nevolili a prípadne trochu poučiť menej informovaných ľudí okolo nás, aby sa náhodou nestali obeťami nejakého demagóga.   Mgr. Viliam Búr 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (39)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Mýliť sa menej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Milí katolíci, vychovali ste si Kotlebu, teraz ho riešte!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Bývalý minister Galko zvyšuje čítanosť blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Digitálne učivo - áno, ale poriadne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Špongia 2011
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viliam Búr
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viliam Búr
            
         
        bur.blog.sme.sk (rss)
         
                        VIP
                             
     
        Programátor.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7854
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Medzinárodný jazyk
                        
                     
                                     
                        
                            Manipulácia a kulty
                        
                     
                                     
                        
                            Počítačové programy
                        
                     
                                     
                        
                            Postrehy a úvahy
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Webové diskusie treba moderovať (2)
                                     
                                                                             
                                            Webové diskusie treba moderovať
                                     
                                                                             
                                            Koniec diskusie
                                     
                                                                             
                                            Ako sa stráca dôvera
                                     
                                                                             
                                            Hurá, prázdniny!
                                     
                                                                             
                                            mor(ho); // počítačom [de]generovaná poézia
                                     
                                                                             
                                            Plošinovka za štyri dni
                                     
                                                                             
                                            Vodná mapa pre Wesnoth, vylepšená
                                     
                                                                             
                                            Reálne čísla v jazyku Java
                                     
                                                                             
                                            Celé čísla v jazyku Java
                                     
                                                                             
                                            Informačná diéta
                                     
                                                                             
                                            Od februára učím (tretia časť)
                                     
                                                                             
                                            Vodná mapa pre Wesnoth, uverejnená
                                     
                                                                             
                                            Vodná mapa pre Wesnoth
                                     
                                                                             
                                            	JavaScript pre začiatočníkov (štvrtá časť)
                                     
                                                                             
                                            JavaScript pre začiatočníkov (tretia časť)
                                     
                                                                             
                                            JavaScript pre začiatočníkov (druhá časť)
                                     
                                                                             
                                            Závislosť na počítači
                                     
                                                                             
                                            Od februára učím (druhá časť)
                                     
                                                                             
                                            Od februára učím
                                     
                                                                             
                                            Vydávanie slovenských kníh
                                     
                                                                             
                                            Wesnoth hľadá prekladateľov
                                     
                                                                             
                                            Hrdinovia
                                     
                                                                             
                                            JavaScript pre začiatočníkov
                                     
                                                                             
                                            Denná dávka socializmu
                                     
                                                                             
                                            Dobré a zlé správy o sedemnástom novembri
                                     
                                                                             
                                            Pole v jazyku Pascal (tretia časť)
                                     
                                                                             
                                            Pole v jazyku Pascal (druhá časť)
                                     
                                                                             
                                            Pole v jazyku Pascal
                                     
                                                                             
                                            Otec časti národa
                                     
                                                                             
                                            Cyklus v jazyku Pascal (tretia časť)
                                     
                                                                             
                                            Cyklus v jazyku Pascal (druhá časť)
                                     
                                                                             
                                            Cyklus v jazyku Pascal
                                     
                                                                             
                                            Hrach na stenu hádzať
                                     
                                                                             
                                            Uloženie obrázku z programu
                                     
                                                                             
                                            Dá sa úspech naučiť?
                                     
                                                                             
                                            Rekurzia
                                     
                                                                             
                                            Informatické dogmy
                                     
                                                                             
                                            Krok za krokom, blog za blogom
                                     
                                                                             
                                            Ako sa zbaviť reklamných letákov?
                                     
                                                                             
                                            Robert Kiyosaki – podnikateľ alebo rozprávkar?
                                     
                                                                             
                                            Moje obľúbené blogy
                                     
                                                                             
                                            Môj názor bez telefónneho čísla nemá cenu
                                     
                                                                             
                                            Kontroverzný návrh blogerského zákona
                                     
                                                                             
                                            Siete P2P
                                     
                                                                             
                                            Posledná vlna internetu
                                     
                                                                             
                                            Náhodný obrázok na webovej stránke
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Algorithmic Adventures (J. Hromkovič)
                                     
                                                                             
                                            Pavučinové pančušky (A. Adamová)
                                     
                                                                             
                                            Na samotu sa nezomiera  (A. Adamová)
                                     
                                                                             
                                            Na lásku sa nezomiera (A. Adamová)
                                     
                                                                             
                                            Gang Leader for a Day (S. Venkatesh)
                                     
                                                                             
                                            Z bludného kruhu (B. Baker)
                                     
                                                                             
                                            Gut Feelings (G. Gigerenzer)
                                     
                                                                             
                                            Labyrint světa a ráj srdce (J. A. Komenský)
                                     
                                                                             
                                            Čajka Jonathan Livingston (R. Bach)
                                     
                                                                             
                                            QED, Nezvyčajná teória svetla a látky (R. P. Feynman)
                                     
                                                                             
                                            Darwin's Dangerous Idea (D. Dennett)
                                     
                                                                             
                                            Rich Dad, Poor Dad (R. Kiyosaki)
                                     
                                                                             
                                            Sumerečnyj dozor (S. Lukjanenko)
                                     
                                                                             
                                            Dnevnoj dozor (S. Lukjanenko, V. Vasiliev)
                                     
                                                                             
                                            Nočnoj dozor (S. Lukjanenko)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Pathetic Hypermarket Band
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Kunder
                                     
                                                                             
                                            Evka Cucurachi
                                     
                                                                             
                                            Slovak Press Watch
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Manuál mladého konšpirátora
                     
                                                         
                       Bajanov zimný štadión v Petržalke: na deti sa zabudlo, profituje iba investor
                     
                                                         
                       Sťažnosť pre Kaliňáka – odporúčanie pracovníčky polície BA III.
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Vymenuje Kiska Harabinovu kandidátku?
                     
                                                         
                       Bude Bratislava smradľavá?
                     
                                                         
                       Fínske školstvo v 20 bodoch
                     
                                                         
                       Agresori
                     
                                                         
                       Milý homosexuálne cítiaci Peter!
                     
                                                         
                       Mýliť sa menej
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




