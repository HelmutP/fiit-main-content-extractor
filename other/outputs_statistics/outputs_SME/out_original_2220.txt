
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Robert Mihály
                                        &gt;
                Nezaradené
                     
                 Obavy z osláv výročia vzniku vojnového slovenského štátu 

        
            
                                    7.2.2010
            o
            20:20
                        (upravené
                7.2.2010
                o
                21:12)
                        |
            Karma článku:
                6.46
            |
            Prečítané 
            1979-krát
                    
         
     
         
             

                 
                    S každoročnou pravidelnosťou sa 14. marca spájajú takmer všetky krajne pravicové hnutia na Slovensku aby si pripomenuli výročie vzniku prvého slovenského fašistického štátu. Je však na túto oslavu skutočne dôvod alebo ide o prezentáciu sily a propagáciu myšlienok, ktorými znevažujú všetky obete zverstiev vojnovej samostatnosti?
                 

                 
  
       Rozpory v otázke vojnového slovenského štátu   Ak je reč o prvom slovenskom štáte, nevedia sa zhodnúť ani sami historici. Ja historik nie som, no touto problematikou sa zaoberám už niekoľko rokov pri štúdiu dejín a pozorovaní aktivít krajnej pravice a práve preto sa budem zaoberať faktami, ktoré považujem za najdôležitejšie. Faktom je, že slovenské protižidovské zákony, takzvaný Židovský kódex, boli podľa slov a chvastania sa ich zostavovateľov omnoho prísnejšie ako tie nemecké. Faktom je, že vtedajšia vláda za transporty slovenských Židov do koncentračných táborov zaplatila nemalú sumu. Práve tu je prvý rozpor. Obhajcovia a zástancovia prvého slovenského štátu a tým pádom i samotní organizátori týchto osláv argumentujú tým, že prezident Dr. Tiso ako i vláda nemali inú možnosť ako pristúpiť na nemecké požiadavky ak chceli ustanoviť samostatnosť Slovenska. Prečo však robili veci nad rámec nemeckých požiadaviek? Jediná odpoveď je tá, že s týmito myšlienkami museli plne súhlasiť. Môžeme pri oslave samostatnosti neprihliadať na tento fakt? Neoslavújú títo ľuďia tým pádom i smrť desiatok tisíc ľudí poslaných na smrť do koncentračných táborov? Ďalším argumentom, najmä starších ľudí je to, že počas prvej samostatnosti bola životná i hospodárska úroveň Slovenska oveľa vyššia ako dokonca dnes. Áno môže to byť pravda avšak i na toto je jednoduchá odpoveď. Nazýva sa to vojnová konjunktúra hospodárstva, ktorá je dočasná. Počas vojny bola skutočne veľmi nízka nezamestnanosť a skutočne sme mohli zaznamenať výrazný rast hospodárstva, avšak nebolo to zásluhou krokov vlády. Keďže sme boli Hitlerovým spojencom, slovenské produkty zásobovali nemeckú armádu alebo putovali do Nemecka. Aspoň veľká väčšina z nich. Preto nemôžme hovoriť o dlhodobom raste slovenskej ekonomiky a otázka životnej úrovne je taktiež tým pádom diskutabilná. Z morálneho hľadiska je však jasné jedno, nemôžeme oslavovať samostatnosť štátu, ktorá mala za následok smrť desitok tisíc ľudských životov.   Aktivity krajne pravicových hnutí alebo aký je ich program na rok 2010      Už niekoľko rokov krajne pravicové hnutia organizujú podujatia rôzneho charakteru avšak väčšinou s tým istým cieľom. Získať na sile a propagovať myšlienky fašizmu. Väčšina z nich bola rozpustená príslušnými orgánmi pre porušenie zákona kvôli vyzývaniu k porušovaniu ľudských práv a slobôd, ako napríklad v novembri 2008 pri takzvanom „Pochode za slobodu slova“, alebo v marci minulého roku na Hodžovom námestí v Bratislave, keď jeden z organizátorov zdravil prítomných pokrikom „Na stráž“. Potom ako koncom minulého roka Slovenská pospolitosť zmenila taktiku a vyzýva k boji proti „násilným“ Rómom, potom ako každý mesiac organizovala protirómske pochody v rôznych mestách na Slovensku, môžeme tento rok v Bratislave očakávať i akciu takéhoto charakteru. Ukázalo sa, že rómska otázka môže pritiahnuť širšiu skupinu ľudí. Prečo sa však títo ľudia nechávajú ovplyvniť a naverbovať? Jedna vec je rómska otázka, ktorá je v súčastnosti pálivou témou a za skutočne problémovú ju vnímajú i mnohí rómovia, druhá vec je vyzývať k násiliu proti tejto i iným menšinám či okolitým štátom. Násilie či nenávisť nie je riešením žiadneho problému. Práve v tejto súvislosti môžeme očakávať i verbálne útoky na maďarskú menšinu na Slovensku. Čo je skutočne alarmujúce je fakt, že počet zúčastnených na týchto akciách každým rokom narastá. Najmä mladí ľuďia, sú často ovplyvňovaní a manipulovaní myšlienkami fašizmu, myšlienkami, ktoré by už dávno mali zhynúť na smetisku dejín.   Otázka prístupu mesta   Nie som zástancom odporovania práv a i títo organizátori ako i všetci iní ľuďia majú právo na zhromaždenie či protest. Avšak s prihliadnutím na predošlé aktivity a akcie je namieste ak príslušné orgány javia zvýšený záujem a obozretnosť. Ak by sa preukázalo, že cieľom zúčastených je vyzývať k porušovaniu ľudských práv a slobôd ich zhromaždenie by malo byť jednoznačne zakázané prípadne rozpustené na mieste. O tomto ma osobne ubezpečil i primátor bratislavy Andrej Ďurkovský, ktorý tiež prislúbil zvýšenú obozretnosť pri týchto podujatiach. Avšak na moju otázku prečo sa on sám nepostaví proti týmto hnutiam, ako je to bežné v Nemecku či v iných štátoch, kde sami primátori a starostovia, stoja na čele davu ľudí hlásajúcich nesúhlas s fašistickými či neonacistickými oslavami, mi dal odpoveď: „Ešte by ma tam bodli nožom“. Ak sa primátor obáva o svoje zdravie, čo obyčajní ľuďia, ktorých na svojom poste primátora zastupuje?   Minuloročný 14.marec v Bratislave   Na Hodžovom námestí sa minulý rok pri oslave samostatnosti prvého slovenského štátu zišlo približne päťsto ľuďí, väčšinou holohlavých a oblečením inklinujúcich ku krajnej pravici. Po zhromaždení bol naplánovaný pochod mestom smerom k hrobu prvého prezidenta Dr. Tisa. V tom istom čase približne dvesto ľuďí blokovalo Poštovú ulicu kade mal pochod viesť. Dvesto ľuďí, ktorí nesúhlasili s pochodom fašistov a neonacistov ulicami Bratislavy. Ako je spomenuté vyššie zhromaždenie na Hodžovom námestí bolo rozpustené po pozdrave „Na stráž“.   Je dôležité aby zverstvá prvého slovenského štátu odsúdili i štátni predstavitelia Slovenska   Postoj slovenských štátnych činiteľov k činnosti krajne pravicových, fašistických či neonacistických hnutí je absolútne pasívny. Nie je to tak dávno kedy súčasná hlava slovenského súdnictva Štefan Harabín predniesol na pôde parlamentu svoj kontroverzný, no najmä odsúdenia hodný prejav. Nie je to tak dávno, keď sa hlasovalo o tom či Andrej Hlinka bude „Otec národa“. A závažný je i jednoznačný postoj cirkvy, ktorá pevne podporuje osobu prezidenta Dr. Tisa.   Je otázkou, ako bude tohtoročný 14.marec vyzerať a v akom duchu sa bude niesť. Isté však je, že rôzne iniciatívy a ľuďia, ktorí nesúhlasia s myšlienkami fašizmu, ľuďia, ktorí nesúhlasia s porušovaním ľudských práv, s násilím a inými zlami nezostanú ani tento rok ticho a v úzadí. Preto by som chcel vyzvať i Vás všetkých, Vás ktorí si ctíte obete holokaustu i Vás, ktorí zmýšlate inak ako spomínané hnutia, vyzývam všetkých slušných ľuďí nezostante tento rok ticho a v úzadí! O tejto otázke treba hovoriť a diskutovať. Je neprípustné aby bola história znásilňovaná!                

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (93)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Slovensko zrejme porušilo zbrojné embargo voči Číne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Hlinkov prostredník pred Ústavom pamäti národa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Robert Mihály
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Robert Mihály
            
         
        robertmihaly.blog.sme.sk (rss)
         
                                     
     
        Som človek, ktorý sa snaží hľadieť svojimi očami. Som človek, ktorý navôkol vidí iba ďalších ľudí, nikoho viac a nikoho menej.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    82
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2006
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Indiáni v Izraeli
                     
                                                         
                       Mier nie je extrém
                     
                                                         
                       Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                     
                                                         
                       Slovensko zrejme porušilo zbrojné embargo voči Číne
                     
                                                         
                       Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                     
                                                         
                       Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                     
                                                         
                       Hlinkov prostredník pred Ústavom pamäti národa
                     
                                                         
                       Kontraples pred operou!
                     
                                                         
                       Za rozdávanie jedla zdarma sú fyzicky napádaní!
                     
                                                         
                       Vycvičme si SS-ákov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




