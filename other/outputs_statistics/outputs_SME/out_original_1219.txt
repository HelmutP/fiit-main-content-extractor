
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ester Demjanová
                                        &gt;
                Moje osobné školstvo
                     
                 "Poďme sa bifľovať" alebo "aký to má zmysel?" 

        
            
                                    9.2.2009
            o
            20:22
                        (upravené
                18.3.2010
                o
                10:13)
                        |
            Karma článku:
                19.36
            |
            Prečítané 
            5047-krát
                    
         
     
         
             

                 
                    Druhá svetová vojna- klasické dejepisné učivo preberané asi miliónkrát od základnej až po strednú. Z mnohých hľadísk jedna z najzaujímavejších dejepisných látok, mne osobne vždy išli „vojny“ najlepšie, len tento rok mi akosi nejdú a tento raz mi tá látka  nedala spať až tak, že som sa rozhodla napísať vám o nej blog, ktorý napokon vôbec nie je o vojnách.
                 

                 
  
           Nie som odborníčka na učenie, na vzdelávanie, nie som pedagóg (hoci som pravé učiteľské dieťa, ktoré vychovávali slovami: „A postav sa, keď s tebou hovorím!“ :). Som len obyčajný študent so svojím obyčajným názorom.    Pravdaže, my žiaci sa ani nevieme iné ako na školu sťažovať. A kto by sa nesťažoval, keď celý systém učenia spočíva v nelogickom bifľovaní sa vecí naspamäť.    Ak chcem mať jednotku z chémie, musím vedieť napísať všetky vzorce a naučiť sa využitie všetkých látok,  ak chcem mať jednotku zo slovenčiny, musím sa naučiť obsahy mnohých diel, ktoré som ani neprečítala, ak chcem mať jednotku z nemčiny, učím sa naspamäť „telefónny zoznam“ slovíčok.   Toto celé nedávam za vinu učiteľom, z ktorých väčšina (hlavne na tom mojom milovanom gymku) sa snaží urobiť látky zaujímavejšie, čo najviac nám „osekať“ hlúposti a dostať sa k podstate veci skôr než zazvoní a zaznie neželané: „Naštudujte si zvyšok z učebnice.“, no oni tiež veľa nezmôžu, keďže im nezmyselné osnovy zväzujú ruky. Ani naša dejepisárka za to nemôže, ani chemikárka, slovenčinár, ani NOS-kár  (pozdravujem). Proste až príliš často dochádza k tým „krajným“ prípadom, keď sa len učíme Zlaté stránky.  Pravdaže, o tom je škola, na Slovensku je to tak.     Do šialenstva ma dohnalo, keď som uvidela v štyroch dejepisných látkach obsiahnutú druhú svetovú vojnu. V učebnici vyšli tieto látky na 37 strán zväčša popísaných textom (asi 12tka v Times New Roman). Pravdaže, naša dejepisárka patrí k tým normálnym, ktorý uznajú, že je toho na študenta veľa (a že milí páni dejepisní šialenci pozabudli, že tak ako oni v časoch svojich štúdií neznášali chémiu, niekto rovnako neznáša dejepis) a látku nám osekala do niekoľkých strán poznámok.     Sadla som si na to tri dni pred písomkou.    Vypísala som si roky.   Bolo ich sedemdesiat sedem.    Správne, šesť a pol tucta presných dátumov bitiek, konferencií a iných..... do troch dní.    Moji spolužiaci tvrdia, že sa treba učiť len tie podstatné roky a hlúposti vynechať. No a tak som vynechala (v domnienke, že keďže sa berú zvlášť svetové a domáce dejiny, robím správne) napríklad informáciu: kde bojovali československé légie v druhej svetovej vojne. Napokon odišli dva body dole vďaka môjmu šikovnému „okúsaniu“ faktov.   Napokon ale môj blog nechce byť o tej trojke, ktorú som schytala. Môj blog by chcel byť o tom, čo som sa z tých sedemdesiatich siedmych dátumov naučila a vzala do života. Myslím, že odpoveď je všetkým jasná.   Nikde nebola poriadna zmienka o koncentračných táboroch. A vlastne na hodine, zahrabaní všetkými tými faktami, sme sa ani nestihli dostať k utrpeniu, nenávisti a neuveriteľnej slepote vtedajších (a, čo je hrôzostrašné aj dnešných) prívržencov Hitlera.    My sme ľudia, ktorí vylezú zo škôl a budú voliť, rozhodovať o budúcnosti tohto malého (ale vraj priebojného) štátu. My sme tí, od ktorých to závisí. Naše vzdelanie a výchova je to, čo by nás v rozhodnutiach malo ovplyvniť. A my nemáme na hodinách čas porozprávať si o koncentrákoch, o tom prečo vlastne Hitler nenávidel Židov, o tom, že oni neboli jediní nenávidení (homosexuáli, Rómovia...), o extrémizme a smrtiach nevinných ľudí? Neprečítame si Sofiinu voľbu, ani Schindlerov zoznam. Nedozvieme sa. A to všetko len preto, že nejaký „ujo politik“ sa poradil s „odborníkmi“, ktorí asi v živote nestáli za katedrou a dohodli sa, že hoci oni sami si už z tých vecí nič nepamätajú, pre naše vzdelanie je to nevyhnutnosťou. Chcela by som ich vyskúšať z „novej látky“ rok za rokom, bitka za bitkou, napríklad takého ministra školstva. A potom sa ho spýtať, na čo sa to vlastne všetko učíme, keď zabudneme? A prečo radšej neopakovať stále tie najpodstatnejšie informácie miesto nezmyselných detailov, ktoré zaujímajú len malé percentá.        Je mi z toho nanič. Z toho, že z celého dejepisu si skoro nič nepamätám, lebo nemám fotografickú pamäť a bifľovať sa neviem.    Z toho, že je jedno, či ste z písomky mali jednotku alebo päťku, napokon si aj tak do dvoch týždňov nespomeniete ani jedinú z „podstatných“ informácií v texte.    A najviac na nič mi je z toho, že moji spolužiaci a rovesníci vyrastú, odídu z našej školy a zvolia si ďalšieho Slotu. A prečo? Lebo nevedia...  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (65)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Založili ste si živnosť? Prvý list, ktorý dostanete, bude podvod
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Taká obyčajná autonehoda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            9 dôvodov, prečo som po bakalárskych štátniciach nikto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Tri školy, dva testy, jedna hodina a stále nemám jazykovku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Diplomové práce na kľúč sú zlyhaním školiteľa a komisie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ester Demjanová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ester Demjanová
            
         
        demjanova.blog.sme.sk (rss)
         
                        VIP
                             
     
         "Pri pohľade na smrť a na tmu sa bojíme iba neznámeho, ničoho iného." (JKR) 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    74
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4282
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje osobné školstvo
                        
                     
                                     
                        
                            Rozhadzujem rukami
                        
                     
                                     
                        
                            Ockovi
                        
                     
                                     
                        
                            Aj o tom peknom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pán Spisovateľ, dobrý aj pečený
                                     
                                                                             
                                            Poberaj sa, starec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            SlovoSvet - aby vás slová počúvali na slovo
                                     
                                                                             
                                            nailepsy slovencynar na svete
                                     
                                                                             
                                            vdacim jej za moju radost :)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




