
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Junkers
                                        &gt;
                Recepty
                     
                 O varení a pečení + recept na rýchlomiznúce keksy 

        
            
                                    11.6.2010
            o
            10:02
                        (upravené
                11.6.2010
                o
                10:42)
                        |
            Karma článku:
                5.83
            |
            Prečítané 
            2724-krát
                    
         
     
         
             

                 
                    Staré filmy a knihy nám prinášajú nielen viac či menej nostalgické spomienky na časy minulé ale i obraz vtedajších domácností, s “maminkou - pilnou hospodiní”, ktorá rodine každý večer na stôl servíruje teplú večeru. Fast forward nejakých 70 rokov a nachádzame sa v ríši supermarketov, plných polotovarov, jedál pre mikrovlnku a donášok jedla do domu.
                 

                     Nebudem zbytočne zaberať miesto na blogu kázaním o zdravej výžive, pojmy ako diéta, delená strava, škodlivé karbohydráty a cholesterol u mňa nenájdete, vyznávam zásadu, že všetko jedlo je zdravé, ak ho jete s mierou. Chcem len ukázať, že čas strávený v supermarkete výberom polotovaru a potom v rade na pokladňu sa dá lepšie stráviť s rodinou, keď sa môžte zísť v kuchyni a popri varení rozprávať o tom, čo priniesol deň. Pri malom dieťatku nemá ani matka na materskej čas na prípravu troch chodov a rafinované pokrmy, u nás sa cez pracovný týždeň tiež varí rýchlo ale všetko “z gruntu”.   Ako štartovný bod to samozrejme vyžaduje solídne naplnenú špajzu alebo poličky v kuchyni, múka, cukor, soľ, cestoviny, ryža, konzervovaná zelenina, rôzne koreniny, cibuľa, cesnak a olej sú základom, na ktorom môžte stavať každé rýchle jedlo.   Výhoda takéhoto varenia je, že viete, čo jete. Sľúbila som, že nebudem kázať o zdravom stravovaní a spomínať tuky, cukry, presolené jedlá, ale predsa len trošku – už pri základných surovinách sa stávame dobrovoľnými obeťami potravinového priemyslu. Nie každý má zdroje a možnosti získavať čerstvé mäso priamo od chovateľa, zeleninu a ovocie od pestovateľa. Je teda ťažké kontrolovať, či ten kus hovädzieho je čerstvý, či kus rybieho filé nie je viac vody ako samotnej ryby. Čo potom možno očakávať od polotovarov alebo hotových jedál? Aké mäso tam dávajú, neboli papriky a paradajky v konzervovom guláši splesnené?       Na záver niečo pre milovníkov sladkého. Rýchle ale veľmi chutné keksy. Ich nevýhodou je, že obyčajne zmiznú rýchlejšie, ako sme ich stihli urobiť.       Keksy s čokoládou a orechami       čas prípravy : 10 minút   čas pečenia : 10 – 12 minút pre plech   výsledný počet keksov : cca 18   spotreba plechov : 3 na recept   teplota rúry : 190 stupňov       175 g múky   1 lyžička prášku do pečiva   125 gramov margarínu (ak použijete maslo, keksíky budú veľmi jemné a roztečú sa)   90 g muscovado cukru (podľa Wiki sa volá i barbadoský cukor alebo vlhký cukor) – ide o trstinový cukor s obsahom trstinovej šťavy   60 g jemného (nie práškového) cukru       ak nemáte poruke muscovado 	cukor, jednoucho použite 150 g normálneho cukru (ja používam 	aj na bežné varenie len hnedý cukor, myslím, 	že má zaujímavejšiu chuť ako biely)       pol lyžičky vanilkového extraktu   pol lyžičky škorice   1 vajce   125 g čokolády rozsekanej na kúsky (alebo už hotové čokoládové čipsy)   100 g sekaných orechov (vlašské, lieskové, pekanové, brazílske .... )       Všetky suroviny zmiešame, lyžičkou kladieme malé kopčeky na plech vystlaný papierom na pečenie. Pečieme vo vyhriatej rúre. Po upečení necháme najprv na plechu a potom na mriežke vychladnúť. Odháňame manžela, partnera, priateľov a deti, aby keksy nezjedli ešte pred servírovaním (a neochutnávame, lebo sa ostatným nič nezvýši).   Výborné len tak, alebo s kakaom, kávou, zmrzlinou.       Aby sme parafrázovali ohviezdičkovaného šéfkuchára Ramsayho : Keksy s čokoládou a orechami - hotovo!             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (87)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Dúhový pochod v Dubline
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Čo nám hovoria kosti z Tuam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Výlet do Antrim v Severnom Írsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Trampoty so zvieratami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            O mačkách a zelenine
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Junkers
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Junkers
            
         
        junkers.blog.sme.sk (rss)
         
                                     
     
         Jeden manžel, jedno bejby, jedna mačka. To všetko na zelenom ostrove. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    73
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Recepty
                        
                     
                                     
                        
                            Irsko
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            The Girl Who Kicked the Hornet's Nest
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            RTE Lyrics - vynikajuce radio
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zirafa na koni - aj take sa stava
                                     
                                                                             
                                            Nadherne ludske pribehy bez zbytocneho sentimentu
                                     
                                                                             
                                            A vraj knihovnici nie su sexy
                                     
                                                                             
                                            Medvede - co este k tomu dodat?
                                     
                                                                             
                                            O vsetkom moznom a aj vareni
                                     
                                                                             
                                            Fascinujuce fakty o slovenskej literature
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo spíme s mužmi s ktorými by sme nechodili?
                     
                                                         
                       Óda na neveru
                     
                                                         
                       Šťastné a veselé ...
                     
                                                         
                       Silvestrovské "mecheche" v Bratislave
                     
                                                         
                       obžerstvo na vianoce a rok 2013
                     
                                                         
                       Cesta stopom: Žilina-Bratislava. Offroad mercedes a čas na zmenu.
                     
                                                         
                       Ako som sa stala neplatičom respektíve obeťou exekúcie...
                     
                                                         
                       Whisky race
                     
                                                         
                       Pozrite si výber najkurióznejších správ a titulkov roka 2012
                     
                                                         
                       Dve americké školy - dva názory
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




