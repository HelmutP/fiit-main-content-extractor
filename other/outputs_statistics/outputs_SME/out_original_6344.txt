
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Dostál
                                        &gt;
                Politika
                     
                 Zákon len pre Maďarov 

        
            
                                    12.5.2010
            o
            9:00
                        (upravené
                12.5.2010
                o
                2:55)
                        |
            Karma článku:
                14.90
            |
            Prečítané 
            3435-krát
                    
         
     
         
             

                 
                    Keď ide o to pobuzerovať maďarských ochotníkov, tety z jazykovej polície sa unúvajú vycestovať trebárs až do Klasova. Keď k očividnému porušovaniu zákona o štátnom jazyku dochádza priamo pod oknami ministerstva kultúry, akurát nejde o Maďarov, tety jazykové policajtky sa tvária, že nič nevidia.
                 

                 
Ako obvykle výstižný Shooty.Zdroj: http://komentare.sme.sk/c/5340326/shooty.html
   Spravodlivosť by mala byť slepá. Maďaričova jazyková polícia má od spravodlivosti zjavne veľmi ďaleko. Spravodlivosť by nemala rozlišovať podľa toho, kto sa nejakého činu dopustil, ale hodnotiť čin samotný. Jazyková polícia s tým má problém. A opakovane.   Španielsky áno, maďarsky nie   Ochotníckym divadelníkom z Klasova bola jazyková polícia vyčistiť žalúdky za to, že na letákoch pozývajúcich na ich v maďarčine hrané predstavenia je iba stručná informácia po slovensky, zvyšok je celý po maďarsky. Podľa zákona o štátnom jazyku majú byť totiž príležitostné tlačoviny určené na verejnosť pre kultúrne účely v štátnom jazyku, alebo ak chcú byť aj v inom jazyku, tak najprv majú byť v štátnom jazyku. Divadelníci z Klasova zákon porušili, tak ich boli tety z ministerstva napomenúť.   Rovnakým spôsobom ako divadelníci z Klasova však porušilo zákon o štátnom jazyku aj občianske združenie Priatelia slobodných informácií (PSI), ktoré vydalo príležitostnú tlačovinu k divadelnému predstaveniu Túlavé srdce, ktorého autorom je zhodou okolností minister Maďarič. Rozdiel bol len v v tom, že príležitostná tlačovina na kultúrne účely nebola v maďarčine, ale v španielčine. Tiež však neobsahovala text v slovenskom jazyku. Hoci som príležitostnú tlačovinu na kultúrne účely vydanú len v španielskom jazyku poskytol médiám už v marci a zároveň som na porušenie zákona o štátnom jazyku upozornil ministerstvo kultúry, dodnes neprišla zo strany ministerstva žiadna reakcia. Ak odhliadneme od možnosti, žeby ministerstvo nereagovalo preto, lebo ide o hru, ktorej autorom je jeho šéf, ostáva len možnosť, že príležitostnými kultúrnymi tlačovinami v maďarčine k porušovaniu jazykového zákona dochádza, kým príležitostné kultúrne tlačoviny v španielčine sú z hľadiska jazykového zákona oukej.   Yes vs. nem   Už štvrťroka sa tety jazykové policajtky musia prichádzajúc do práce pozerať na plagát v angličtine s nápisom "Bridget, I love you, Mark", ktorým dochádza k porušeniu ustanovenia zákona o štátnom jazyku, podľa ktorého majú byť nápisy na verejnosti v slovenčine alebo aj v slovenčine. Aj na toto porušenie jazykového zákona zo strany Priateľov slobodných informácií som ministerstvo vo februári riadne upozornil a dosiaľ neprišla žiadna reakcia.   Oproti tomu sa už minister Maďarič nechal počuť, že proti jednojazyčným predvolebným bilbordom SMK použije jazykový zákon. Tie síce nie sú vylepené pred ministerstvom kultúry, no podstatnejší rozdiel zrejme bude, že nie sú v angličtine, ale v maďarčine. Hoci z hľadiska porušenia zákona o štátnom jazyku (neexistencia slovenskej verzie textu) je situácia rovnaká. A na problémy sa môže tešiť aj MOST-HÍD, ktorého predvolebné bilbordy sú síce dvojjazyčné, ale niektoré majú text v maďarčine pred textom slovenčine a maďarský text je väčším písmom ako slovenský text. Ach, toľká hrôza! To je, samozrejme, v rozpore s jazykovým zákonom.   Trúfnu si na prezidenta?   V pondelok 10. mája zorganizovali PSI verejné kultúrne podujatie Deň Európy s poéziou európskeho prezidenta, na ktorom zazneli verše predsedu Európskej rady Hermana van Rompuya vo flámčine (resp. holandčine), hoci kultúrne podujatia sa majú konať v štátnom jazyku. A poézia na rozdiel od hudby alebo divadla z tejto požiadavky výnimku nemá. Ministerstvo bolo na pripravované porušenie jazykového zákona vopred upozornené, ale - ako obvykle - reakcia žiadna.   Nevzdávam sa však. Audiovizuálny záznam z kultúrneho podujatia som dnes doručil na ministerstvo. A keďže dnes o siedmej večer hrajú v Astorke Maďaričovo Túlavé srdce, budem pred predstavením divákom rozdávať španielsky bulletín k nemu. Priamo pod oknami Maďaričovho ministerstva. Tam možno jazykový zákon porušovať beztrestne. Naozaj, mám to vyskúšané. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (45)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Konečne sa dozvieme mená všetkých členov Smeru
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Niekoľko viet o Táni Kratochvílovej
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Smeráčina á la Brixi: Len si tak niečo vybaviť
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Fico nám berie peniaze a dáva ich svojim kamarátom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Dostál
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Dostál
            
         
        dostal.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som predsedom OKS. V novembrových voľbách kandidujem v bratislavskom Starom Meste do miestneho i mestského zastupiteľstva. Ako jeden z členov
Staromestskej päťky. Viac na www.sm5.sk. 

  

 Ondrej Dostál on Facebook 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    202
                
                
                    Celková karma
                    
                                                15.07
                    
                
                
                    Priemerná čítanosť
                    5758
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Smrteľne vážne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                             
                                            Ivan Kuhn
                                     
                                                                             
                                            Juraj Petrovič
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Michal Novota
                                     
                                                                             
                                            Ondrej Schutz
                                     
                                                                             
                                            Tomáš Krištofóry
                                     
                                                                             
                                            Peter Spáč
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            INLAND
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Konzervatívny inštitút M.R.Štefánika
                                     
                                                                             
                                            Peter Gonda
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 robí Ficovi reklamu
                     
                                                         
                       V TA3 Fica hrýzkajú slabúčko
                     
                                                         
                       O tom, čo platí "štát"
                     
                                                         
                       Konečne sa dozvieme mená všetkých členov Smeru
                     
                                                         
                       My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                     
                                                         
                       Smeráčina á la Brixi: Len si tak niečo vybaviť
                     
                                                         
                       Potemkinov most v Bratislave
                     
                                                         
                       Fico nám berie peniaze a dáva ich svojim kamarátom
                     
                                                         
                       Výsmech občanom v priamom prenose alebo .... Mišíková v akcii ...
                     
                                                         
                       Ficov podrazík na voličoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




