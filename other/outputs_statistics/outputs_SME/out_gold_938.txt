

 
Ako som povedal, myšlienka prišla len tak, z jasného neba blesk, postaviť plavidlo z plastových fľaší a zopár dosiek. Žiaden ekologický protest, ani recesia, ani súťaž. Proste len tak. Fľaše sme zbierali doma, s prispením rodiny a kamarátov, naviac sme odbremenili Lido od ďalších pár sto kúskov (tety na vychádzke nás chválili, že zbierame smeti - vysvetlite im, že to je materiál na loď!). Po búrlivých brainstormingoch u Doda boli hotové návrhy. Konečný výsledok - okolo 600 fľaší s objemom cez 1000 litrov, odhadovaný výtlak takmer 900kg. Pre štvorčlennú posádku viac ako dosť. Projekt Warlatia začal.
 
 
Fáza prvá - stavba Warlatie
 
 
 
 
 
 
 
 
 
Na stovky plastových fľaší bol krázny pohľad, škoda bolo do nich rýpať... 
 
 
 
Záverečné núdzové doplnenie zásob, susedia prepáčia.
 
 
 
Tri fošne, chrbtová kosť celého plavidla.
 
 
 
Prvý pontón hotový!
 
 

 
 
Grafická vyzualizácia. Pôvodné nápady boli búrlivejšie, vrátanie klietky naplnenej fľašami.
 
 
 
Všetko bolo spočítané. Aspoň zhruba :) V detaile vidieť aj oporné body pre priečne spojnice.
 
 

 
 
Tri trupy hotové.
 
 
 
Fáza druhá - Plavba Warlatie
 
 

 
 
Favorit Frank mal tú česť viezť vzácny náklad ku Trenčínu.
 
 
Hrdinná posádka zostavuje loď.
 
 
Stavba paluby. Skladací prístrešok z papierových rúrok. Nebola to najlepšia voľba...
 
 
 
Posádka a jej vlajka. Chlapi z ocele.
 
 
 
Prvé metre plavby dopadli na jednotku. Prosím pohľad na palubu - nehostinné drevo bolo pokryté dvoma priklincovanými nafukovačkami a papierovými doskami.
 
 
 
Hill Billy - jedna z pohonno-smerovacích palíc.
 
 
 
Warlatia - no comment
 
 
 
Prvá skúška - strom krížom cez rieku zhrzil pozhŕnať palubu do vody a zakliesnený koráb by sme z tadiaľ už v nikdy nedostali. Pár metrov pred kontaktom sa nám podarilo hovado zatlačiť na breh.
 
 
 
Hadík. Kúsal. Snažil sa.
 
 
 
Prvý deň  bolo krásne počasie. O tom svedčí aj tento náhodný naháč vo Váhu.
 
 
 
Prvé táborisku. Môj návrh spať na lodi neprešiel.
 
 
 
Druhý deň. Zachmúrené pohľady posádky smerom k oblohe. Ešte sme nevedeli, že nás ten večer navštíví peklo. Ale tušili sme!
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Občasná búrka. Nič, čo by papierovo-igelitový prístrešok nezvládol. A medzitým sa hrali karty.
 
 
 
 
 
 
 
 
 
 
 
  
 
 
Druhé táborisko pri Považanoch. Warlatia odpočíva potiahnutá na plytčinu a posádka pózuje. Až kým...Až kým nevypukla najprudšia búrka v okolí Piešťan za posledných 5 rokov. Stromy padali a blesky blikali kadenciou protileteckého delostrelectva. Stany smer neuznávali, tak sme sa krčili pod kúskom igelitu a nechávali sa otĺkať krúpami. Ešte stále to nebolo také zlé - kým sme náhle nezistili, že Warlatia aj s väčšinou našich vecí zmizla. Lepšie povedané - odplavila sa, vďaka prudko zvýšenej hladine. Naša ľoď nás zanechala! Bez rozmýšľania sme sa rozbehli dolu prúdom - chvílu plávajúc, občas sa predierajúc nepriechodným brehom (ak ste videli v noci počas búrky bežať naháča po roli medzi Považanmi a Potvoricami, bol som to ja). Po viac ako hodinovom plávaní sme tú  potvoru konečne dostihli a dotiahli na breh. Návrat pre veci, návrat k lodi, hodiny v búrke. O štvrtej ráno sme opäť zaľahli, premočený do poslednej nitky, so zážitkami hodnými nočnej bitky kdesi v Mekongu. Po pár hodinách sme sa opäť postavili na nohy a doplavili sa k Prúdom, kde sme Warlatiu zanechali zamaskovanú. Príjazd do Piešťan si vyžadoval lepšiu formu.
 
 

Fáza tretia - koniec Warlatie
 
 
 
 
 
 
 
 
Záverečná plavba sa odohrala o  niekoľko týždňov. Spolu s prizvanými hosťami nás bolo na palube sedem. Pre Warlatiu hračka.
 
 
 
Prejazd popod most vyvolal záujem u nemeckých turistov aj pospolitého ľudu.
 
 
 
 
 
 
Dlho sme zvažovali, čo s loďou urobiť. Poškodenie bolo minimálne, prišli sme asi o 10 fľaší.
 
 
 
 
 
 
 
 
 
 
 
Napriek úvahám o jej zanechaní na brehu pre voľné užívanie sme ju nakoniec rozobrali - expedícia Warlatia sa skončila. 
 
 
 
 
 

