

   
 Na druhej  strane ulice pomedzi bezlisté konáre stromu krátko zasvietia kontúry jej okna. 
 Edo počíta v duchu do dvadsať. Jeden, dva...zamkla...tri, štyri, päť, sedem...beží po 
 schodoch...deväť, vybehla z vchodu a spomalí, aby ju nevidel bežať...dvanásť, trinásť, 
 štrnásť...prechádza cez cestu a zase sa nepozrie, či nejde auto...sedemnásť... ohorok Edovej 
 cigarety opíše oblúk a zasvieti Alici na cestu k jeho vchodu ako kométa. Dvadsať. Klope... 
   
   
 Tých pár sekúnd, kým sa pred Alicou otvoria dvere jeho bytu, jej zošívajú žalúdok. 
 Vchádza k nemu ako návšteva. O hodinu a dve hruškovice neskôr sa z nich na pár hodín 
 stávajú milenci. Alica zastane pred jeho posteľou vyrobenou na mieru 
 a zaprie ruky vbok. „ S tými červenými návlečkami to tu vyzrá ako v bordeli... 
 V tejto posteli ja veru súložiť nebudem..." „Ále budeš...," povie a naučeným hmatom ju zloží do 
 vodorovnej polohy. Smeje sa. Je to ich obľúbený vtip. 
   
 Svetlo z plazmovej obrazovky sa Alici zarezáva hlboko do lícnych kostí. Leží vedľa neho pod 
 paplónom na bruchu a tvári sa, že sleduje film. Niekde medzi piatym a šiestym reklamným 
 blokom z nej posťahuje sveter a nohavice. Nie je dôvod sa ponáhľať. Ešte sa nezhovárali. 
  „Som smutný," povie. „Inak by som tu nebola," chce sa povedať Alici, 
 ale mlčí. Vie, že keby bol veselý, neležala by v jeho posteli. Pred piatimi mesiacmi sa naučila nad 
 tým nepremýšľať. A zaklamala to aj Edovi.   
   
   
 Samozrejmosť, s akou leží v spodnej bielizni v jeho posteli, a spôsob, akým mu podlieha, 
 ho odzbrojujú. Poslúcha na slovo ako zvieratko a v prestávkach medzi dotykmi sa smeje. 
 Nahlas a srdečne. Nikdy nikde nepočul nikoho sa tak smiať. Ani jeho susedia nie. Zvykli si spoza  
 jeho steny počuť rôzne zvuky. Ten smiech iba raz za mesiac. Dá mu vždy všetko. 
 Neanalyzuje. Nekomplikuje. Presne tak to má Edo rád. „ Je čistá. Som 
 čistý. Neubližujem. Som voľný... " Veľmi tomu potrebuje uveriť. 
   
 Zaspal. Alica mu dopíše prstom na chrbát zvyšok textu piesne. Vie, že jej aj tak nebude 
 rozumieť. Oblieka sa. Nečaká, kým sa zobudí. Zaspávajú ako milenci. Nechce sa zobudiť ako 
 návšteva. Cez žalúzie sa do izby začína tlačiť ráno. Pomedzi konáre stromu 
 rozoznáva kontúry svojho okna. Dnes opäť neodostrie závesy. Kvôli tomu nahému stromu pred 
 jeho oknami. Pery sa jej nehlučne rozkmitajú v záchvevoch nemej vety, ktorú Edo nepočuje. 
   
 Jeden, dva...Alica zabuchne za sebou dvere. Tri, štyri, päť, sedem...beží po schodoch...deväť, 
 vybieha z vchodu, nespomaľuje. Dvanásť, trinásť, štrnásť...prechádza cez cestu. Nepozrie sa, 
 či nejde auto. Na okamih zatúži, aby sa zľava prirútil kamión v plnej rýchlosti. Ale je iba 
 päť hodín ráno a smetiari ešte len začali mechanickými metlami ulici pretierať oči. Sedemnásť, 
 osemnásť, devätnásť, dvadsať. Alica zamkne za sebou dvere a zosunie sa na koberec. 
   
 Edo zo seba zmýva sprchovým šampónom značky Nivea posledné 
 zvyšky Alicinej vône. Obracia tvár k ružici sprchy a zosilní prúd vody. Pichľavé kvapky mu 
 z mysle vypudzujú posledné rozostrené obrazy uplynulej noci. Usmieva sa. Už ju necíti. 
   
 Na opačnej strane ulice sedí Alica ešte stále na koberci. Do očí sa jej tlačí smútok po Edovi. 
 Už ho nevie zo seba zmyť. O pár dní vrhne strom pred jeho oknami nové listy. Edo je opäť veselý. Už ju 
 nepotrebuje. Alica podíde k oknu a odostrie závesy. Je v bezpečí. 
 Skrat v žalúdku jej pripomína, že zašla priďaleko. 
 Povracia sa. Očakávaný pocit úľavy sa nedostaví. 
   
   

