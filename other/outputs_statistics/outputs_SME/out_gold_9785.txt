

 Tento krátky dokument vznikol ako môj ročníkový film na tému: portrét osoby. Ako hlavnú postavu som si vybral svojho deda Šimona Dornáka, ktorý, aj keď bol obyčajným dedinským človekom, sa predsalen trochu vymykal z radu. 
 Film sa nakrúcal v dedinke Paprad na západe Slovenska. Do popredia som sa snažil vyniesť miestnu prírodu, nárečie a posolstvo, že nielen svetoznámi ľudia hýbu svetom. 
 





 
 Pri nakrúcaní som použil 2 fotoaparáty Canon 7D ako hlavnú a vedľajšiu kameru. 
 Za archívne zábery ďakujem pánovi Heinzovi Platzerovi, ktorého som nedopatrením nenapísal do titulkov. 

