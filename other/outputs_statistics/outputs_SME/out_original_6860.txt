
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Fořt
                                        &gt;
                Nezaradené
                     
                 Sloboda: moja, tvoja, ich a naša. 

        
            
                                    19.5.2010
            o
            22:20
                        (upravené
                19.5.2010
                o
                22:42)
                        |
            Karma článku:
                6.35
            |
            Prečítané 
            1542-krát
                    
         
     
         
             

                 
                    Niekedy sa nestačím diviť, čo všetko si ľudia predstavia, keď sa povie slovo sloboda. Čo vám prvé napadne, aké asociácie a pocity vo vás vyvoláva? Častokrát som počul, že absolútna sloboda neexistuje. Čo to vlastne sloboda je a čo sloboda nie je?
                 

                 Sloboda je kompasom môjho života. Spoľahol som sa na ňu pri mojom rozhodovaní a konaní. Zatiaľ ma nikdy nezradila ani nesklamala. Stala sa mojou životnou filozofiou. Životná skúsenosť ma priviedla k tomu, že som si vytvoril vyhranenú predstavu, čo to sloboda je a nie je. Tu je moj pohľad.    Sloboda pozostáva z troch súčasne naplnených zásad:    1. Človek je suverénom nad sebou samým.   Znamená to, že človek vo vzťahu k sebe vystupuje ako vlastník seba, nie len ako správca.   Kedysi za starovekého Ríma bol otrok predmetom vlastníctva - "hovoriaca vec". Nemohol o sebe rozhodovať. Ak by sa otrokyňa zohyzdila, alebo otrok zmrzačil, bolo by to chápané ako poškodenie majetku ich vlastníka, poškodzovanie cudzej veci. Z toho titulu by ich za to stihol trest. Nemali totiž na seba právo.   Slobodný človek je plne spôsobilý robiť rozhodnutia o sebe. Nemusí si k rozhodnutiam o sebe pýtať povolenie, či dovolenie či schválenie od iného. Má právo rozhodovať o svojom tele a živote. Môže napríklad darovať obličku, alebo naopak dispozíciu so svojim telom po smrti nepovoliť. Môže si vybrať povolanie. Môže disponovať so svojim časom.    2. Človek je spôsobilý uzatvárať dohody.   Slobodný človek má právo vstupovať s inými ľuďmi do dobrovoľnej výmeny. Má právo sa zaväzovať, t.j. dávať jednostranne záväzné sľuby alebo prijímať záväzky výmenou za práva. Nedodržanie sľubu ako aj porušenie svojho záväzku je porušenie slobody iného. Toto konanie zvykneme nazývať ako nezodpovedné. Nezodpovednosť je teda prejavom neslobody.   Ak sa dvaja slobodní ľudia dobrovoľne na niečom, čo sa týka výlučne ich dohodnú, neexistuje dôvod, aby niekto tretí ich dohodu narúšal, maril alebo zakazoval. Ak napríklad dvaja ľudia rovnakého pohlavia si chcú medzi sebou upraviť vzájomné vzťahy vyplývajce z ich životného partnerstva nemožno takú dohodu zakázať. Rovnako ak sa dvaja ľudia dohodnú na predaji veci, ktorá je v oprávnenom vlastníctve jedného z nich zákaz takejto dohody by bol veľmi ťažko obhájiteľný. V princípe ale nemožno zaväzovať niekoho, kto nie je stranou dohody, také konanie by bolo v rozpore so slobodou. Nemožem sa dohodnúť s mojim spolubývajúcim na internáte, že náš iný spoločný spolubývajúci bude chodiť vysýpať naše smeti. Z tohto dôvodu sa vyhlásila Mníchovská dohoda za nulitnú.   Každý slobodný človek sa tak napríklad dobrovoľne môže stať členom vybranej cirkvi alebo iného združenia. Prijatím členstva dáva sľuby a prijíma záväzky. Tieto záväzky a sluby musí plniť. Ak ich plniť nebude, šliape po slobode toho, s kým dohodu uzatvoril. Zároveň však nikto, kto k takejto dohode sám nepristúpil, nie je z nej povinný. V slobodnom svete nemožno nečlenom cirkvi zakázať nedeľný, sobotný alebo pondelkový predaj. Rovnako tak nemožno v súlade so slobodou vnútiť záväzky jednej zo zmluvných strán napríklad pri dojednávaní pracovnej zmluvy. Na druhej strane, ak niekto vstupi do klubu účastníkov cestnej premávky, pristupuje k dohode o pravidlách, ktoré si dohodli ich členovia. Rovnako, ako keď si ide zahrať so spoluhráčmi futbal či pristúpi k partii pokru.    3. Rešpekt k slobode iného.   Ak chce človek vstúpiť do práv a slobody iného, môže tak urobiť len v dobrovoľnej dohode s dotknutým. Súhlas musí byť daný vopred, neodopretie či mlčanie nemožno automaticky považovať za súhlas. Ak nedosiahol dobrovoľný súhlas dotknutého, dostal sa na hranicu, ktorá sa bez pošliapania slobody nedá prekročiť. Také zapálenie cigarety. Sloboda teda nie je svojvôľa voči svojmu okoliu. Svojvoľne môže človek nakladať len so sebou samým. Aj to len vtedy, keď tým nenarúša slobodu iného. Ak sa niekto stane dobrovoľne napríklad členom RKC a príjme záväzok, že neučiní sebausmrtenie, tak je týmto svojim záväzkom viazaný. Rovnako tak nemôže urobiť človek, ktorý má dlh voči inému, okrem prípadu, že sa tak v zmluve dohodli, alebo mu bol dlh odpustený. Porušiť sľub či záväzok môže slobodný človek učiniť bez narušenia slobody len ak ho ten, voči komu povinnosť smerovala, ho tejto povinnosti zbaví.   Slobodný človek má právo zvoliť si svoje životné ciele ako aj spôsob ako ich bude dosahovať, samozrejme rešpektujúc slobodu iných. Slobodná spoločnosť sa tak vyznačuje pluralitou a rôznorodosťou. Naopak neslobodná spoločnosť je často identifikovateľná s uniformitou - monopolom či už ekonomickým alebo svetonázorovým. Sloboda nie je ani nezodpovednosť ani svojvôľa.    Prameňom neslobody je násilie tak voči vlastníctvu ako aj osobnej integrite v najširšom slova zmysle, ktoré môže mať rôzne formy: nútenie prijať nedobrovoľné záväzky, neodobrené zásahy do svojej slobody: krádeže a zranenia, nedobrovoľné obmedzenia alebo odobratie suverenity nad sebou samým, alebo obmedzenie či odobratie spôsobilosti uzatvárať dohody. Sloboda je princíp, mimo dispozície človeka. Je nescudziteľná, nepremlčatelná, neodňateľná a neprevoditeľná. Slobodný človek sa napríklad nemôže sám seba alebo iného predať do otroctva, alebo svoju slobodu poskytnúť ako záloh. Človek slobodu nevlastní a teda sa jej ani nemôže vzdať. Sloboda je vyšší princíp, mimo neho. Slobodu nemožno zrušiť - možno ju len pošliapávať.   Nedá sa relativizovať.  Je večná. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Preniesť sa ponad seba a zažiť kus večnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Aky je odkaz bilbordu Aliancie za rodinu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Couchsurfing, moja vlastná 6 ročná skúsenosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Drobné prekvapenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Kabaret: milujem cesty vlakom 2. triedy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Fořt
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Fořt
            
         
        fort.blog.sme.sk (rss)
         
                                     
     
        Mám rád slobodu, je to moja životná filozofia a môj životný kompas. Som pozorovateľ sveta. Fascinujú ma neviditeľné veci, ktoré hýbu dušou človeka. Zaujímam sa o happyológiu a dlhovekosť. Som couchsurfer. Obohacujú ma stretnutia. 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2977
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Karol Kaliský
                                     
                                                                             
                                            Samuel Genzor
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       6 tipov pri výbere osobného trénera
                     
                                                         
                       Gladiátor HD tréning
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




