
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Petronela Ľachová
                                        &gt;
                Nezaradené
                     
                 Počúvajme 

        
            
                                    21.5.2010
            o
            19:01
                        (upravené
                21.5.2010
                o
                19:17)
                        |
            Karma článku:
                5.74
            |
            Prečítané 
            697-krát
                    
         
     
         
             

                 
                    Každý z nás je občas zrelý na psychiatra. Lebo sa málo rozprávame. A ešte menej počúvame..
                 

                     Nedávno som vyšla z podniku pripravená na odchod domov, keď spoza rohu vyšli dvaja pripití chlapi. Jeden viac ako druhý a ten čo ešte vládal stáť na vlastných ťahal druhého, ktorý čosi pokrikoval. Odrazu zastal a zreval na celé námestie: „Prečo ma nikto nepočúva, prečo si ma nechce nikto vypočuť?!"   A dovolím si tvrdiť, že toto je problém. Začiatok vysvetlenia, prečo sme sa všetci dopracovali tam, kde sme. Hľadáme problém v peniazoch, hľadáme problém vo vláde, hľadáme ho vo vede, vo vesmíre a on je pritom úplne banálny. Nevieme počúvať. Skutočne počúvať. Nemyslím psychiatrov, ktorí si vypýtajú nehorázne peniaze a dozviete sa to, čo ste vlastne aj tak vedeli. Tým ich samozrejme neodsudzujem, ich práca je potrebná. Ale myslím počúvanie toho, čo chceme naozaj povedať. Ako to vidíme my. Potrebujeme si vyliať dušu, vyjadriť svoj názor, podeliť sa o radosť alebo predať svoju skúsenosť. Máme problém počúvať a prijímať názory druhého. Nevnímame sa navzájom. A už vôbec nie ak náš niečo trápi. Každý si povie, že má svojich problémov dosť. Dnes neni na nič čas. Neni čas ani na to, aby sme sa počúvali.   Máme strach a sme zbabelci. Ako keď vám priateľ povie: „Mám len teba, si môj skutočný priateľ." Lichotí nám to, ale vydesí zároveň. Akoby sme dostali určitú zodpovednosť byť stále po ruke a nesklamať. To sme my ľudia. Aj v partnerstve.   Človeku najviac chutí zakázané ovocie, vždy chceme toho, kto nechce nás. Láka nás ten, koho nemôžeme mať, len si neuvedomujeme, že je to možno práve preto. Ako keď ste chorý a zakážu vám jesť čokoládu.. odrazu jej máte plnú hlavu a túžite si z nej aspoň kúsok odhryznúť, je lákadlom, pokušením, ktoré vám pomätie dočasne hlavu. Akonáhle ju budete môcť jesť, hltavo ochutnáte a opäť ju odložíte. Už je vaša, už je po ruke, už ju môžete mať. Niekto má o nás záujem, lichotí nám to, ale odmietame. A vo chvíli keď to vzdá a odchádza nám začína chýbať, začína nám byť ľúto.. Ale to som už odbočila od témy.   Častokrát sa desíme slov „mám ťa rád". Bojíme sa toho, kto sa chce na nás naviazať. Ak sa dusíme, utekáme. Bojíme sa slov, bojíme sa toho, čo nám kto povie. „Bolesť spôsobená jazykom je väčšia ako bolesť spôsobená mečom." Slovo má veľkú moc. A častokrát nevieme s touto sečnou zbraňou správne narábať, ani ju prijímať.           To je môj názor a chcela som ho dať zo seba von. Ďakujem, že ste si ma vypočuli.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petronela Ľachová 
                                        
                                            Sme len ľudia :)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Petronela Ľachová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Petronela Ľachová
            
         
        lachova.blog.sme.sk (rss)
         
                                     
     
        Učím sa byť realista, ale ešte stále verím v dobro ľudí.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    701
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




