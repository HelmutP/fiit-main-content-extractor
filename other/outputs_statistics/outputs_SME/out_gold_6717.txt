
 Si aký si...

nevieš riadiť lepšie 
svoje činy
nechceš prijať myšlienku
že každý tvoj čin
vráti sa ti späť
o desať
či o päťdesiat liet

Je to taký zákon
neba
či chceme
či nechceme
chleba podľa zásluh 
jesť budeme

potom nepýtaj sa
prečo ťa to
postihlo
prečo žobrákom si
ostal na duši
prišlo k tebe len to
čo ti prísluší

a čo ti prísluší?

nabalené všetko
čo si svetu dal
akú lásku i nenávisť
si rozsieval
možno vpísané máš 
už teraz na dlani
rozsudok
čo iste príde
raz v tichom svitaní

nečakane ako šelest
vetra
vrazí ti kôl do srdca
tvoja vlastná nevraživosť
čo dnes z teba krváca
a činí činy
hodné slova žobráka

*

Preto neškoď
keď nemôžeš pomôcť...


(Pre tých, ktorí sa neštítia druhým ubližovať...)

 
