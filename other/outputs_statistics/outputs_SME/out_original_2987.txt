
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Antónia Sedmáková
                                        &gt;
                Uniesli nam dieťa
                     
                 Uniesli nam dieťa 

        
            
                                    17.4.2010
            o
            18:47
                        (upravené
                17.4.2010
                o
                15:31)
                        |
            Karma článku:
                3.48
            |
            Prečítané 
            507-krát
                    
         
     
         
             

                 
                    Dúfame, že mu je tam dobre tak ako u nás...
                 

                 
  
   Mladí manželia mali len jedno dieťa a to 2- ročného chlapca, ktorý bol pre nich všetkým. Boli veľmi šťastní,pretože to ich krásne slniečko im spríjemňovalo dni. Rozhodli sa trošku zrelaxovať a preto cez leto odišli k svoje rodine do zahraničia. Bývali u rodiny a preto im nič nechýbalo. Chlapčeka uložili do detskej izby s balkónom, kde bola jeho izbička v ktorej sa hrával. Po niekoľkych týždňoch ako sa ich dieťa hralo, matka zbadala ako mu postupne hračky miznú. No zo začiatku si to vysvetľovala tým, že možno ich dieťa niekde skrylo. Postupne si to aj otec všimol a začali mať spolu obavy, nechápajúc, kto berie synčekovi hračky. Manželia potrebovali na chvíľu odísť nakúpiť niečo,, pod zub".Svoje dieťa dali do starostlivosti svoje rodine, ktorej plne dôverovali. Keď prišli z obchodu, otvorili detskú izbu a ich synček tam nebol, len zbadali pootvorené dvere na balkóne. Išli okamžite za svojou rodinou, či náhodou si ho nevzali k sebe, do inej izby,no však oni s trasúcim sa hlasom odvetili, že ,,nie". Hľadali ho všade, v celom byte, no po chlapčekovi, nebolo ani náznak stopy. Utekali do ulíc vyvetľujúc ľudom, čo sa im stalo. Či náhodou nezbadali ich dieťa. No však beznádejne, pretože nikto ho nikde nevydel. Čakali neustále pri telefóne s veľkým strachom a obavou a s nádejou, že dieťa niekto našiel a ozve sa. Okamžite informovali políciu v zahraničí a aj na Slovensku. Ponúkli peniaze tomu, kto im poskytne nejaké informácie. Sú rôzni ľudia a preto im koľkrát volali s falošnými nádejami a klamstvom, ktoré ich zármutok ešte väčšmi zhoršoval. Poroznášali a polepili letáky so synčekovou fotkou.   Je tomu, už niekoľko rokov, ale manželia ešte vždy cítia veľkú bolesť v srdci s nádejou a očakávaním, že ich slniečko sa raz vráti domov. Hrozné muky prežívali títo mladí ľudia, pretože nevediac kam, ich dieťa vzali. Po čase zistili, že ich milované dieťa uniesli, pre inú bohatšiu rodinu, ktorá si za to zaplatila, pretože nemohla mať deti. Zistili, že v tomto štáte, je to bežné, že nejaké pekné dieťa unesú pre inú rodinu s väčším kontom. Po čase manželom sa narodilo ďalšie dieťa-chlapček. No však nikdy nezabudnú na svojho prvorodeného, ktorého nesmierne a z láskou v srdci milovali a nestále milujú aj keď ich dieťa je v inej rodine. Stále pociťujú jeho prítomnosť, v celom byte. Spomienky a fotky sú vryte navždy v ich srdcia. Nebol by deň, kedy by si na neho nespomenuli a nepomodlili sa za neho.   ,,Dúfame, že mu je tam tak dobre ako u nás".To su slová, ktoré si rodičia prajú pre svoje dieťa aj keď už ho nedokážu telesne objať svojou láskou, ale v spomienkach ho nikdy nezabudnú pohladiť, objať a vysloviť slová: milujeme ťa z celého srdca.     RODIČOM, KTORÍ STRATILI DIEŤA   Je veľmi ťažké sa vžiť do vašej kože, čo ste prežili a nestále preživate vo svojom srdci. Bolesť, smútok a veľký žiaľ, pretože ste prišli o svoje dieťa. Aj napriek tomu, že vaše dieťa je niekde v ďiaľke, nikdy nedokážete pretrhnúť to vaše rodičovské puto, čo vás spájalo a nestále spája z vašou láskou k nemu. Ste navzájom prepojený, pretože aj v tejto chvíly cítite svoje dieťa aj keď len vo vašom srdci a vo vašej mysli. Práve tam sa s vami stretáva a objíma vás svojou láskou. Vedcte, že raz sa s ním stretnete vo večnosti, kde mu budete v úplnej blízkosti a vtedy ho budete môcť aj poláskať ,,zoči voči" a pritisnúť si ho navždy k svojmu srdcu.   ĽUDOM A MANŽELOM, KTORÍ SA PRIČINILI O NEŠŤASTIE   Peniaze dokážu vás natoľko pohltiť, že máte to srdce odňať dieťa od rodiča? Podieľate sa na tak hroznom čine, nehľadiac na veľký zármutok a žiaľ, ktorí lomcuje v srdcia manželov. Keby ste videli aspoň jedného rodiča, ktorý stratil dieťa a trápi sa s tým celý život, tak možno až vtedy by to s vami pohlo. Uvedomili by ste si, že nikto, nikdy nedokáže odňať dieťa od ich srdca, pretože sú navzajom prepojení,nech je ich dieťa kdekoľvek. Nemáte žiadne právo vziať nevinné stvorenie a vychovávať ho vo svojom príbytku. Sú rôzne spôsoby, ako môžte získať dieťa a to napr. adopciou. Veď koľko detí čaká na to, že si ich niekto osvojí, privinie k srdcu s objatím a s láskou v ňom. Nikdy ani len nepomyslite na to, že by ste dieťa ukradli, pretože tým sa dopúšťate strašného činu. Pretože po čase vás začne hrízť svedomie a ak sa dieťa napokon dozvie, že ste ho odňali od vlastný rodičov začne vás nenávidieť.       DRAHÍ RODIČIA   Strážte si svoje deti a majte ich pod dohľadom. Upozornite svoje deti na to, aby sa nedávali do rozhovoru s cudzími ľuďmi a napokon, aby sa nedali zlákať spoločným odchodom za cenu veľkého nešťastia, ktoré sa niekedy nedá vrátiť späť. Chráňte svoje deti zdravým spôsobom a dôverujte im. Pretože tak sa upevňuje rodičovske puto lásky medzi rodičom a dieťaťom. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Antónia Sedmáková 
                                        
                                            Dni spásy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Antónia Sedmáková 
                                        
                                            Ďakujem, že som
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Antónia Sedmáková 
                                        
                                            Za peniaze i dušu predá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Antónia Sedmáková 
                                        
                                            Smrť žije v nás
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Antónia Sedmáková 
                                        
                                            Mám sa rozviesť ?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Antónia Sedmáková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Antónia Sedmáková
            
         
        antoniasedmakova.blog.sme.sk (rss)
         
                                     
     
        S veľkou láskou objímem všetko, čokoľvek ma môže
pripodobniť Kristovi v živote i v spôsobe života. Túžim len po jedinom :
mať podiel na milosti, aby som napredovala ako Kristus, to je všetko moje šťastie, ktoré si prajem v tomto živote, javí sa mi to byť pre mňa lepšie ako akýkoľvek iný osoh. Budem snažiť o to, aby som v tomto krátkom čase na zemi až do konca tak dokonale slúžila Bohu, ako mi to len bude možné- aj keby som mala žiť čo i len jednu hodinu, alebo jeden okamih.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    55
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    570
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Pozri na moje poníženie
                        
                     
                                     
                        
                            Vianočný list pre Teba
                        
                     
                                     
                        
                            Vykúpenie
                        
                     
                                     
                        
                            Moja práca ma cenu
                        
                     
                                     
                        
                            Bože, pomôž mojej dcérke
                        
                     
                                     
                        
                            Nemám ruky
                        
                     
                                     
                        
                            Reakcia na všetky diskusie
                        
                     
                                     
                        
                            Anjelik s krídlami
                        
                     
                                     
                        
                            Srdce dobrovoľníka
                        
                     
                                     
                        
                            Ako milovať muža, ženu a deti 
                        
                     
                                     
                        
                            Rodič, ako najlepší priateľ...
                        
                     
                                     
                        
                            Musela som si vypiť
                        
                     
                                     
                        
                            Práve, to potrebujem
                        
                     
                                     
                        
                            Spadol som
                        
                     
                                     
                        
                            Nedokázala som jej prinavrát..
                        
                     
                                     
                        
                            Príčinou nevery, je nedostatok
                        
                     
                                     
                        
                            Ktorá viera, je ta pravá?
                        
                     
                                     
                        
                            Smrť človeka
                        
                     
                                     
                        
                            Moje orgány mi ostali, lebo so
                        
                     
                                     
                        
                            Lienka ako Valentínka
                        
                     
                                     
                        
                            Uniesli nam dieťa
                        
                     
                                     
                        
                            Bez rodiny sa človek chveje zi
                        
                     
                                     
                        
                            Splnil som sľub pre...
                        
                     
                                     
                        
                            Môjmu Oteckovi
                        
                     
                                     
                        
                            Život bez domova
                        
                     
                                     
                        
                            Mamička moja
                        
                     
                                     
                        
                            Kde je život, tam je nádej
                        
                     
                                     
                        
                            Oslavujte život
                        
                     
                                     
                        
                            50 vecí, ktoré som sa naučila
                        
                     
                                     
                        
                            Najkrajší deň v mojom živote
                        
                     
                                     
                        
                            Nežná láska a opatera sú...
                        
                     
                                     
                        
                            Nádej
                        
                     
                                     
                        
                            Anjeličkovia držte sa!
                        
                     
                                     
                        
                            Smejte sa!
                        
                     
                                     
                        
                            Láska lieči
                        
                     
                                     
                        
                            Je to tvoj život
                        
                     
                                     
                        
                            Dieťatko moje
                        
                     
                                     
                        
                            Milujem ťa!
                        
                     
                                     
                        
                            Zázračná láska
                        
                     
                                     
                        
                            Nebíčko
                        
                     
                                     
                        
                            Raňajšia prechádzka
                        
                     
                                     
                        
                            Mám sa rozviesť?
                        
                     
                                     
                        
                            Smrť žije v nás
                        
                     
                                     
                        
                            Za peniaze i dušu predá
                        
                     
                                     
                        
                            Ďakujem, že som
                        
                     
                                     
                        
                            Dni spásy
                        
                     
                                     
                        
                            Som alkoholik
                        
                     
                                     
                        
                            Viditeľné obrátenie
                        
                     
                                     
                        
                            Moja manželka sa neobrátila
                        
                     
                                     
                        
                            Boh ťa miluje
                        
                     
                                     
                        
                            Pomôž nám, Pane
                        
                     
                                     
                        
                            Anjelský dotyk neba
                        
                     
                                     
                        
                            Pane,chválim Ťa,že si ma utvor
                        
                     
                                     
                        
                            Ona je totiž iného vierovyznan
                        
                     
                                     
                        
                            Slobodní v láske
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




