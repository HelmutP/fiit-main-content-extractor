
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Maceják
                                        &gt;
                Nezaradené
                     
                 Socha vs. Smrť 

        
            
                                    30.5.2010
            o
            19:19
                        (upravené
                31.5.2010
                o
                11:02)
                        |
            Karma článku:
                26.74
            |
            Prečítané 
            4485-krát
                    
         
     
         
             

                 
                    Posledné dni sa na Slovensku nesú v duchu antikampane niektorých koaličných politických strán. Taktiež sa do antikampane pustili aj občianski aktivisti, ktorým budúcnosť tejto krajiny nie je ľahostajná.
                 

                 Napadla ma pri tejto antikampani jedna veľmi smutná paralela. Shooty vyzbieral za pár dní neočakávané množstvo peňazí na dobrú vec, ktorá odráža do veľkej miery nálady slovenských občanov a držím mu pri tom palce. Na druhej strane je tu zbierka na sochu Svätopluka, ktorú vyhlásilo občianske združenie troch grácií (Fico, Gašparovič a Paška). Odhaľovanie tejto sochy naplánovali úplne perfektne, pár dní pred voľbami. Svedčí to o ich úcte k tejto historickej postave. Ešte zaujímavejšie je pozadie financovania tejto sochy. Zbierajú sa na ne povinne členovia Smeru a prispeli aj Hornonitrianske bane a.s. , ktoré sú dotované štátom. Čiže občania nejako v týchto ťažkých časoch opomenuli zbierku na predvolebnú kampaň týchto pánov.    Minulý rok si všetci dobre pamätáme nehodu v Handlovskej bani, ktorá patrí pod firmu Hornonitrianske bane. Pri tejto smutnej nehode zomrelo 20 baníkov. Hornonitrianske bane odškodnili pozostalých rodinných príslušníkov v hodnote 600€. V tom období prišiel premiér s nápadom odškodniť pozostalých z rozpočtu štátu a to 1 miliónom korún cca 33 000€ za každého baníka.    Hornonitrianske bane prispeli na sochu Svätopluka sumou približne 17 000€. Kladiem si otázku, či je predstaviteľom Hornonitrianskych baní prednejšia socha Svätopluka ako ľudský život. Asi áno. Bolo potrebné sa poďakovať premiérovi za to, že zatiahol za nich odškodnenie zosnulých a oni na oplátku prispeli na jeho predvolebnú Svätoplukovu sochu. Premiér je spokojný, lebo za peniaze zosnulých ho nik kritizovať nebude, kdežto keby vyhodil na sochu v týchto ťažkých časoch, asi by ho jeho voliči nemali až tak radi, ako ho majú. Hornonitrianske bane to vyšlo oveľa lacnejšie ako vyplatenie pozostalých rodín. A ešte je nutné poznamenať, že jediný, kto by nemal byť spokojný sme my občania tejto krajiny. Dôvodom na to je, že všetky tieto peniaze boli zo štátneho rozpočtu.     Česť pamiatke všetkým zosnulým pri tejto tragickej nehode. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Zmätený Matovič
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Jadro  je energia budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            "Politologický analfabetizmus"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Zatmenie v Žiari nad Hronom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Kde sa stala chyba?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Maceják
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Maceják
            
         
        macejak.blog.sme.sk (rss)
         
                                     
     
        Politolog, ucitel, brat, syn, .......
www.stefanmacejak.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1546
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




