
 Cez stanicu v Senici sa prehnala cirkusová úderka, rozdávajúca pozvánky a z Vrboviec s nami dokonca cestovala pôvabná pokladníčka, ktorá nám predala vstupenky ... nezmeškali!  

Vítal nás  sneh, pľuvač ohňa, kúzelník, klaunka, ... a Alegria (v rodinných cirkusových kruhoch zvaná Alergia), ktorá sa stala našou hymno - znelkou počas najbližších troch a ešte mnohých ďalších dní.  

Mysleli ste si, že cirkusanti majú cez zimu prázdniny? Maringotky sú niekde zazimované a všetci sú v civilizácii, so svojimi rodinami? Tak to ešte nepoznáte Cirkus Plusko! Cirkusanti telom i dušou, cez zimu splavujú, rybárčia magnetické písmenká, aby poskladali čo najlepší slogan pre svoj cirkus, skúšajú vlastné atrakcie, aby mohli opraviť kolotoče, autodróm, preveriť strelnicu, či dom hrôzy. Popri tom všetkom stíhajú aj športovať, cestovať okolo sveta a voliť nového principála. Kto by veril, že sme to všetko stihli vlastne za 2 dni???  

A môj najväčší zážitok? Jedna veľká cirkusová rodina ... Človeče, 6-členné družstvá, za každý hod kockou treba obehnúť kolečko a splniť úlohu, aby ste mohli posunúť svojho panáčika. Všetci neustále behajú po vyšmýkanom okruhu, sneží. Nášmu družstvu sa podarilo hodiť 6-tku iba asi dvakrát, obiehame stále dookola za 2, 3, či 4 políčka. Ďaleko zaostávame za ostatnými. Prví sú už v cieli. Ale čo sa nestalo? Fanto sa rozhodol, že nám pomôže a obiehal naďalej s naším družstvom. A potom sa pridali ďalší a ďalší a ďalší. Dodnes verím, že by sme pôvodne do cieľa nedošli, nevládali sme a náskok bol veľký. Ale davová psychóza ti nedá a keď toľkí bežali za nás, bežali sme tiež. Až kým sme posledného panáčika nedostali domov. Úžasné!  

Potom by ma už nemalo prekvapovať ako rýchlo sme na záver cirkusového sústredenia pochopili, že šifru vylúštime, iba keď sa všetci spojíme. Záverečná figúra, prekrásne video a potlesk. Všetci sme uspeli, všetci sme sa stali cirkusantmi. Niektorí si nacvičili visenie na lanách, iní klaunskú zostavu, trénovali sme rôzne žonglérske odrody a odvážlivci si hádzali pravú horiacu guľu. Skupina tanečníkov sa učila aj synchronizovaný thriller dance .....:)   

Čo dodať? Určite si nenechajte ujsť nadchádzajúcu sezónu Cirkusu Plusko!   

Gabča   

P.S.: Ešte jedna súkromná špecialitka na záver. Cirkusanti sa v nedeľu vrátili do svojich bežných životov, prudko poznačení Alegriou, spoločným videom, fotkami a spomienkami. No ja som aj v bežnom živote klaunkou. Naozaj. S červeným nosom navštevujem deti v nemocnici, tak ako doteraz. Ale teraz až viem, že patrím do Cirkusu! ĎAKUJEM  

Pozn.: Gabča píše o zážitkovej akcii Cirkus Plusko. Viac fotiek nájdeš TU alebo TU. 
