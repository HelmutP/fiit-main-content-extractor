

 
Čo je mandala a odkiaľ k nám prišla 
 
 
Slovo mandala pochádza z indického slova "sanskrt" a znamená "kruh - stred". A naozaj, motívom kruhu je ľudstvo fascinované už od nepämeti a objavuje sa všade okolo nás. I samotný vesmír je mandalou, tak ako kruh nemá ani začiatok,ani koniec a jeho vznik pre nás nie je upresnený. Fyzici len hypoteticky odhadujú ako a kedy vesmír vznikol. Preto mandala ako symbol nekonečnosti ( nie je špecifický ani koniec , ani začiatok) je pomerne presná definícia. Okrem kruhového princípu mandalu môžeme stručne popísať ako útvar s pevným stredom okolo ktorého sa v harmónii a s presným poriadkom otáčajú ostatné elementy. U mandaly tieto elementy sú tvorené pravidelnými symbolmi v často súmernom usporiadaní. Teda i vesmír je podľa tejto symboliky multidimenzionálny priestor s pevným stredom - zdrojom (v mnohých náboženstvách je to Budha, Boh, Jehova, Aláh , atď) a nekonečným prienikom ďalších segmentov (tie najväčšie a najpresnejšie mandaly sú tvorené fraktálmi - obrazcami zobrazujúcimi nekonečne krát seba samého v stále menšom merítku) ako symbol večnosti a nekonečnosti. Je to teda ideálna jednota. 
 
 
Dôkazom toho, že mandala má pôvod už v samotnom vzniku vesmíru, je i samotné usporiadanie planét a každá bunka jedinca. Stačí keď pozorujeme pavúka ako vytvára pavučinu - mandalu. Pozrieme sa na peň stromu, kde sú viditeľné letokruhy. Hodíme kameň do vody a pozorujeme sústredné kružnice, ktoré sa vytvoria na hladine vody. A môžme takto pokračovať donekonečna. Preto je mandala staršia ako ľudstvo samo, ako vesmír, ktorého hranice nie sme schopní zachytiť, tak ako kruh nemá počiatok ani koniec. 
 
 
V rozličných formách sme schopní mandalu nájsť i v kultúrach po celom svete. Najznámejšia je mandala v Tibete. Klasická budhistická mandala pozostáva zo sústredných kružníc a je vyplnená pravidelnými tvarmi ako kružnice, štvorce, symboly. Tibetskí mnísi tvoria mandalu za dlhých modlidieb a rozjímania pomocou špeciálnych nástrojov a farebného piesku. Vysypávajú ju na podlahu chrámu za zvuku meditácie a presne "maľujú" pieskom známe symboly. Táto mandala má význam jednak obrazu sveta, jednak ľudského vnútra a ideálu ľudskej existencie ako aj cesty ako dosiahnuť životnej harmónie. Trpezlivé sypanie miliónov zrniečok piesku postupne má za výsledok nádhernú mandalu, ktorá ako symbol pominuteľnosti je zmetená a zrnka sú vysypané do rieky, kde sú ako ozdravná energia vyplavené do všetkých kútov sveta. 
 
 
S mandalou sa stretávame ešte pred obdobím budhizmu v období brahmanistickej Indie v podobe pôdorysov chrámov. Rovnako tak je súčasťou afrických, indiánskych a austrálskych kultúr. V Európe nájdeme mandaly v gotických katedrálach ako ružicové okná alebo pôdorysy kruhového labyrintu v dlažbe, ktorá je metaforou cesty k Bohu. Mandala ako symbol prepojenia mikrosveta a makrosveta sa v západnej kultúre objavuje v stredoveku v schémach alchymistov a astrológov. Alchymisti ju používali v globálnom ponímaní prírodných procesov a ich symbolickej personifikácii a astrológovia ju užívajú pri vytváraní analógie hviezdného makrosveta a mikrosveta ľudského osudu.  
 
 
Napríklad veľká duchovná liečiteľka, spisovateľká a poetka Abatyša Hildegarda z Bingenu v 12. storočí prostredníctvom mandal, ktoré kreslila, hľadala spôsob komunikácie s Bohom. Písala o svojich víziach, v ktorých Boha pripodobňuje ku kruhu, ktorý všetko uzatvára a zároveň presahuje. I Giordano Bruno, renesančný učenec, vytváral kruhové obrazy, ktoré mali dokonalú formu. Mali človeka učiť k predstavivosti a zlaďovať s vesmírom a udržovať tak v harmónii. V 20. storočí popisuje známy švajčiarsky psychiater C.G.Jung osobnú mandalu ako obraz vnútra určitého človeka. Mandala je podľa neho tušenie stredu osobnosti, určitý centrálny bod vo vnútri duše, ku ktorému sa všetko vzťahuje a podľa ktorého je všetko usporiadané. 
 
 
 
 
 
Ako si vybrať mandalku? 
 
 
Mandalu si človek vyberá intuitívne. Sám je zdrojom energie, ktorú vyžaruje do okolia. A samozrejme priťahuje to, čo potrebuje a čím je on sám. Stačí sa do mandalky zadívať a sám ucíti: "Áno je to ona". 
 
 
Často sa sama pozerám do stredu mandaly a cítim akoby sa celá roztočila a pohltila ma. Zároveň ucítim jemné mravenčenie, ktoré ide z mojej korunnej čakry (oblasť temena hlavy) až ku chodidlám a ku koreňovej čakre (oblasť pohlavných orgánov) . Niekto môže cítiť príval energie, niekto zároveň stíšenie a upokojenie sa. Iní zase pocítia veľkú chuť po mentálnej práci, mandala stimuluje ich myslenie. Niekto zase začne mať vízie a pocíti otvorenie intuície (čakry tretieho oka). 
 
 
Sama kreslím mandaly a čoskoro dám svoju ponuku na tieto stránky. Kto bude chcieť, môžem mu nejakú vyhotoviť, samozrejme zadarmo. Nepovažujem peniaze za dôležité v duchovnom rozvoji a pomoci druhým. 
 
 
K výkladu symbolov mandál slúži druhý článok - "Výklad symbolov mandál". 
 
 
Nech vás sprevádza pokoj a slnko v duši. 
 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 

