
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stela Berecova
                                        &gt;
                Puzzle môjho života
                     
                 Ad Revidendum 2015 

        
            
                                    28.3.2010
            o
            12:33
                        (upravené
                28.3.2010
                o
                14:06)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            653-krát
                    
         
     
         
             

                 
                    "Rozdiel medzi školou a životom je v tom, že v škole nás najprv naučia a potom otestujú, ale život nás najprv otestuje a potom naučí."
                 

                 Stredná škola. Prvé skutočné lásky, prvé náznaky našej dospelosti a voľnosti, prvé neskoré príchody domov, prvé zážitky z večierkov, ktoré si dovolím prirovnať k  večierkom v Beverly Hills.  Troška povrchné, nemyslíte?  Skutočne len týchto zopár vecí vám prebehne po rozume pri spomienke na roky strávené na strednej škole ?    Spomínam si na svôj prvý deň. Neistota. Zvedavosť. Nové tváre a oči, ktoré sa mi prihovárali pohľadom a boli rovnako vydesené ako tie moje. Krôčik za krôčikom schádzam dolu po schodoch. Nevedno kam vedú. Cítila som sa trošku stratená, trošku nesvoja. Teraz s odstupom času spomínam na to s úsmevom na perách. Možno preto, že som dospela a možno preto, že som zistila, že som sa nemala čoho obávať. Veď ľudia tam nehryzú a poniektorí sú tam dokonca aj milí. :)   Často mi ľudia hovoria, že roky, ktoré prežijeme na strednej sa už nikdy nevrátia, a že je to jedno z najlepších období, ktoré máme možnosť zažiť za pomerne krátky život. Rozhodne súhlasím.  Ako ste na tom vy ? Čo sa vám ako prvé vynorí pri tejto spomienke ? Priateľstvá, ktoré ste stihli za tú dobu vybudovať? Pondelkové rána, kedy bolo potrebné znova a znova vstávať do školy ?  Učenie na test do neskorých hodín, ktorý na vás čakal nasledujúci deň ? Školské výlety, či cesta autobusom na koncoročný výlet?  Prestávky strávené na chodbách s kamarátmi alebo čakanie v dlhočíznom rade pri bufete?  Nezhody s profesormi a opätovné ospravedlňovania? Áno, aj toto sme všetci prežili a prežívame. Je to taký náš malý svet vo veľkom svete. Miesto, kde si samy určujeme pravidlá a nečíha žiadne nebezpečenstvo (teda aspoň tak by to malo byť ). :)  Akokoľvek som sa tešila, že prichádza koniec a nakuknem do sveta, ktorý tam čaká na nás vonku za bránami školy. Prišiel deň, kedy som si povedala:"Skutočne to chcem?"  Chcela by som spraviť niečo, aby na mňa škola nezabudla. Nemyslím ľudí. Myslím budovu, ktorú som navštevovala štyri roky. A je jedno z akého dôvodu. :)   Vyryť naše mená na lavicu alebo na stenu na toaletách? Ukryť truhličku s našimi túžbami a prosbami a schovať na tajné miesto, o ktorom budeme vedieť len my a škola?   Ubehlo to rýchlo. Zatiaľ mám čerstvé spomienky, ale čo bude za takých desať rokov? Prídu už iba útržky? Vynechané spomienky, vynechané roky.  Najviac, čo si želám, aby si každý pri spomienke na strednú školu spomenul na ľudí, ktorým sme venovali kus svôjho života a niektorým i kus nášho srdca.  * Prosím, potom nás nevyzraďte. Chceme sa rozlúčiť. Nejak tak po svojom. Veď Vy viete, na koho myslím. Ďakujem. :) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            O  nás
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            Polnočné čakanie ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            (Ne)hrám
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            vo "svetle" reflektorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            zopár (ne)správnych dôvodov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stela Berecova
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stela Berecova
            
         
        stelaberecova.blog.sme.sk (rss)
         
                                     
     
        Život nie je o tom, aby sme vždy získali len tie najlepšie veci, či dosiahli pocit víťazstva. Naučiť sa byť šťastný s tým, čo nám život ponúka, i keď toho možno na pohľad nie je veľa, je omnoho ťažšie, ako zapáčiť sa niekomu alebo pridať sa na stranu silnejšieho. :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    803
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Puzzle môjho života
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jackie Collins - Hollywoodske deti
                                     
                                                                             
                                            Paulo Coelho - Láska ( citáty )
                                     
                                                                             
                                            Terry Pratchett
                                     
                                                                             
                                            John Grishman-Klient
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Sister Hazel
                                     
                                                                             
                                            The Grease ( platňa )
                                     
                                                                             
                                            The Corrs
                                     
                                                                             
                                            Paramore
                                     
                                                                             
                                            Blue October
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Michal Hlavaty
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Kníhkupectvo Martinus
                                     
                                                                             
                                            Labello
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




