

 Pozrel  som si tlačovku pána Bilasa o Interblue a emisiách. Nepoznám pointu predaja, ani jeho účel. Nie je to ani mojou úlohou. 
 Viem iba o cirkuse, ktorý to vyvolalo. Padli ministri, padli ostré slová, padli klamstvá a obvinenia. Slovensko predalo nevýhodne emisie - pod cenu. 
 Mali sme tu nejakú pani zo Švajčiarska, ktorá vraj riadila 600 firiem - a kauza jej zničila život. Alebo len renomé a meno? Či ako? Aj ten pán " donor" je odpísaný. Trpia jeho deti, manželka a jeho psychika. 
 Jedno viem iste. Opäť rozprával niekto nový, kto iba rozvíril novú hladinu pochybností. Nechápem možno súvislosti, ale sedlácky rozum mi hovorí - moja vlasť a krajina bola pekne okradnutá. 
 A tak mi to príde, že v tejto krajine je to ako v škole s lajdákmi. Svojou nepripravenosťou sme sa pripravili ... Alebo nás pripravili o poctivé peniaze ... Sme na smiech všetkým. Najlepšie sa asi smejú v Interblue - veď na emisii zarobili 3 eurá za kus. Slušné nie? 
 Mačka sa nedohodne s myšou - tak to chodí. Pred klamstvom neutečieš. Kto klame? Všetci. Nikto nemá čisté svedomie, ale pár ľudí má plné vrecká "papierikov šťastia". 
   
 No nič, počkajme si na ďalšie zaujímavé odhalenia slovenských novinárov. A pán premiér by si mohol zobrať príklad od pána Bilasa - impulzívnosť a nervozitu dokázal potlačiť na minimum. 

