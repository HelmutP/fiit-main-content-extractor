

 Čierna kniha je v prvom pláne príbehom advokáta Galipa, ktorému zmizla manželka Rüja. Temer zároveň sa ukáže, že zmizol aj jeho strýko Dželál, ktorému bola Rüja mladšou nevlastnou sestrou. Dželál bol novinárom, ktorý dlhé roky, desaťročia, písal dlhé články do denníka Milliyet. V článkoch sa venoval rodinným príhodám, tureckej histórii, mágii čísel a písmen v štýle hurúfizmu, plánom v plánoch - prosto všetkému. Získali si temer legendárny status, širokú obec priaznivcov, niektorí z nich boli priam fanatikmi. Galip teda pátra najskôr po Rüji, neskôr po Dželálovi a nakoniec aj po sebe samom. To je jedna veľká téma knihy - ako byť samým sebou, ako byť niekým iným, ako si zachovať alebo zmeniť identitu, odhaľovanie skrytých významov očividných skutočností. Ďalšie témy sú zrejmé z citátov nižšie. 
 
Pátranie po ľuďoch a identite sa odohráva v Istanbule, po ktorom Galip pobehuje krížom-krážom. Nakoniec je vo svojom pátraní svojím spôsobom úspešný, keď sa stane Dželálom a dokonca zaňho napíše nejaké články. Tým "nájde" Dželála umožní pokračovanie Dželálových pravidelných stĺpikov napriek jeho neprítomnosti - nikto ani nepríde na to, že autorom nie je starý novinár. Rüja sa tiež objaví... ale neprezradím všetko. Pravda, "neprezradiť všetko" je pri takej komplikovanej knihe ľahké. 
 
Text knihy je rozdelený do kapitol dvoch druhov. Striedajú sa tie, v ktorých sa postupne popisuje Galipovo hľadanie, a tie, ktoré sú vlastne Dželálovými článkami. Vďaka tejto štruktúre je možné do deja vložiť aj tie najbizarnejšie a zdanlivo úplne nesúvisiace motívy. Ale Pamuk túto barličku prakticky vôbec nepotrebuje - nemá problém s hladkým zaradením mystických či historických pasáží aj do toho najaktuálnejšieho akčného deja. Nie že by akcie bolo nejak veľa, ale však aká by to bola svojho druhu pátračka bez akcie? Mnoho motívov sa v knihe opakuje v rozličných variáciách a pritom čitateľ ani netuší, kam ho zaveje už najbližšia strana.  
 
Rozhodne výborné. 
 
Nasleduje zopár postrehov, ktoré som si počas čítania poznačil plus nejaký ten citát (kurzíva). 
 
 
 
 
Jednou z hlavných tém, ktoré som zatiaľ nespomenul, je porovnávanie Východu a Západu. Turecko je štátom na rozhraní kresťanstva a islamu, Istanbul - Konštantinopol - bol dlho sídlom východného patriarchu a dlho sídlom Osmanskej ríše. Atatürk z neho urobil sekulárnu republiku a boj medzi islamom a vplyvom západnej kultúry zúri naďalej. Islamský štát do Európskej únie? Prečo áno? Prečo nie? 
 

Kedykoľvek sa odvážim pustiť do nekonečnej ságy o tom, čo Západ ukradol Východu a Východ Západu, myslím si toto: Keby bola ríša snov, ktorú voláme svet, iba domom, kde blúdime ako námesačníci, potom sú naše literárne tradície ani nástenné hodiny, ktoré sú tam nato, aby sme sa tam cítili ako doma. Teda: 
1. Povedať, že jedny z tých hodín idú správne a druhé nesprávne, je vyložený nezmysel. 
2. Povedať, že jedny sú pred druhými o päť hodín, je takisto nezmysel. Pri rovnakej logike by sa dalo celkom ľahko povedať, sú o sedem hodín za nimi. 
3. Z toho istého dôvodu, ak je podľa jedných hodín 9.35 a aj druhé hodiny náhodou ukazujú 9.35, každý, kto tvrdí, že druhé imitujú prvé, tára nezmysly. 

 
Kto napísal správne alebo skôr Božskú komédiu, Robinsona alebo filozofiu rozumu? 
 

Podľa F. M. Üčündžüho sa svet delí ba dve proti sebe stojace polovice. Východ a Západ sa od seba líšia ako dobro a zlo, ako biela a čierna, ako anjeli a čerti. Ak neberieme do úvahy fantazírovanie jalových rojkov, šance, že by tieto dva svety mohli nažívať pokojne, sú nulové. Vždy mal navrch jeden alebo druhý. Ak bol jeden pánom, druhý bol otrokom. 

 
Víťazstvá sa prelievali zo strany na stranu. Alexander Veľký, križiaci, Hárún ar-Rašíd, Hannibal a Hispánia, islam v Andalúzii, Mehmet Dobyvateľ pokoril Konštantinopol, naposledy sa v Čiernej knihe spomínajú porážky Osmanov na mori. Ale táto história je štrbavá, na myseľ mi prichodia ešte Peržania v Grécku, Rimania v Judei, americké naftárske firmy v Iraku, pád dvojičiek, naposledy politicky korektné zaobchádzanie s moslimami v západnej Európe, ktoré má za následok rozširovanie vplyvu islamu. 
 
Možno sa nám islam zdá netolerantný... ale treba vziať do úvahy, že je o 600 rokov mladší ako kresťanstvo. A čo robili kresťania, keď bolo kresťanstvo o šesť storočí mladšie? V 15. storočí? Pristupovali k blížnym svojim tolerantne? Istý Ján Hus by vedel rozprávať... 
 
 
 
Poznáte pocit, keď sa dokážete vžiť do myšlienok niekoho iného, presne ho odhadnete? 
 

Keď si objednal u chlapca čaj, vytiahol z vrecka Dželálov článok a znova sa od začiatku pustil do jeho čítania. Písmená, slová a vety sa nijako nezmenili, no keď mu po nich putovali oči, vnukali mu myšlienky, akými sa ešte nikdy nezaoberal. Neboli Dželálove, ale jeho, Galipove, hoci sa akosi podivne odrážali v texte. Keď pobadal paralelu medzi svojimi a Dželálovými myšlienkami, zaliala ho vlna radosti, asi taká, keď sa mu ako chlapcovi podarilo dokonale zahrať muža, akým sa túžil stať. 

 
 
 
Jedno z miest, kde to podľa mňa prekladateľ pravdepodobne nezvládol: 
 

Predstavujem si, že som François Champollion, ktorý v noci vstáva z postele, aby rozlúštil na rozete hieroglyfy, a blúdi ako námesačný tmavými ulicami mojej mysle, aby sa stratil v slepých uličkách stratenej pamäti. 
 
Jean-François Champollion skutočne rozlúskol egytské hieroglyfy, tie však neboli na rozete, ale na Rosettskej doske. Môžeme sa len dohadovať o tom, že už Pamuk mohol použiť výraz "rozeta" v originále, môj tip je taký, ako som napísal vyššie. 
 
 
 
Pre 23. kapitolu "Príbeh o ľuďoch, ktorí nevedia rozprávať príbehy" si Pamuk vybral skvelé motto, v ktorom sa nájde temer každý milovník kníh. Teda každý taký, ktorý nie je skalopevne presvedčený o tom, že sám vie písať: 
 

"Ojoj," (prehovorí potešený čitateľ), "to je dôvtip, to je génius! Tomu rozumiem a obdivujem to! To isté som si myslel sto ráz i ja!" Inými slovami, tento človek mi pripomenul moju múdrosť, a preto ho obdivujem. 
Coleridge, Eseje o vlastných časoch 

 
To mi evokuje moje vlastné bývalé nadšenie Vieweghom. Ten teda vie čitateľom múdrosť pripomínať...  
 
 
 
Kefalín, čo takého si predstavujete pod pojmom Abecedná revolúcia? Zavedenie hlaholiky na území Veľkej Moravy v 9. storočí? Zavedenie hranoliky na Slovensku v 21. storočí? Nie, je to (v kontexte Čiernej knihy v podstate samozrejme) nahradenie arabskej abecedy latinkou v Turecku v roku 1928. 
 
Abecedná revolúcia priniesla veľký problém mystikom čítajúcim písmená v ľudských tvárach. Celé storočia budovaný systém sa zrútil. Namiesto písmen alif a lam, tvoriacich prvé štyri písmená v arabskom pravopise slova Alláh museli v tých istých tvárach nachádzať úplne iné arabské písmená. Už to je dosť silný dôvod na odmietnutie noviniek zo Západu, heh. 
 
 
 
Ach, ako mi nasledovný monológ (z ktorého vyberám len dva kúsky) pripomína našu realitu (a ach, aký je ten autor geniálny :-). 
 

Vtedy ste udreli, obvinili ste ho z urážky tureckého národa, ... 
[...] 
Potom ste sa postavili do úlohy veľkého obrancu našej slávnej histórie a "našej kultúry" a vyzvali ste čitateľov, aby písali majiteľovi novín sťažnosti. Vedeli ste, čo robíte - nešťastná čitateľská verejnosť napokon nič nemiluje väčšmi ako vyhlásiť dajaké nové križiacke ťaženie proti tým, ktorí chcú pošpiniť našu slávnu históriu. 
 
Pravda, u nás si vystačíme aj bez čitateľskej verejnosti, stačí voličská verejnosť a pekných pár pohárikov. 
 
 
 
Na inokedy som si nechal ešte jednu pasáž, v ktorej traja skúsení novinári dávajú Dželálovi rady, ako písať. Tuším, že by to tu zopár ľudí mohlo zaujímať, ale na dnes fakt stačí... všimli ste si ale, že prakticky všetky citáty sú úvahového charakteru, bez zjavného súvisu s vyššie popisovaným dejom? Rozhodne to nie je kniha na jedno posedenie, dá sa nad ňou veľa premýšľať. 
 
 
 

