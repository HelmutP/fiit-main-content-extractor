

  
   
 Voda sa rozbehla po oboch stranách rieky.  
 Z jednej strany sa vyliala na polia a z druhej do obce Rybany.  
  
 Zaliala domy, bytovky, pivnice, záhrady aj obchod. Ulice sú plné ľudí. 
 Niekto sa bezmocne pozerá a niekto ratuje čo sa dá.  
  
  
  
  
  
  
  
  
  
  
  
  
 Kamarátka mi vraví, že chalupu má pod vodou.  
 Ide spať ku známej.  
   
 Všade sú požiarnici a ťahajú z každého dvora z pivníc vodu.  
 Pani ma volá, aby som sa šla pozrieť na ich záhradu.  
   
 Kráčam do dvora a vidím ako požiarnik kontroluje hadicu, ktorá z pivnice smeruje do verejnej kanalizácie. Jeden starší pán vraví, že kanalizácia nevydrží odvádzať vodu, keďže je v jednej rovine aj s riekou. 
   
 Idem pozrieť spomínanú záhradu. 
 Všetko je zaplavené pod vodou, úroda zničená.  
   
 V diaľke vidím ako sa ľudia brodia po kolená vo vode a prenášajú veci. 
 Jeden starší pán mi vraví, že vraj taká voda tu už nebola 140 rokov. 
   
 
 Chcem sa dostať ku ďalším bytovkám a rozhodla som sa, že prejdem okolo starého mlyna.  
 Aj ten je zaplavený.  
   
 Auta s vysokým podvozkom prevážajú ľudí, a kto má čln využije aj ten.  
 Všetko je zaplavené a cesta k bytovkám zahataná.  
   
 Skúšam sa dostať k rieke Bebrava. 
   
 V diaľke vidím vozidlo povodňovej služby. 
  
 Má naložene vrecia s pieskom. 
 Všetci kompetentní riešia situáciu. 
   
 Prišla som pri rieku, je riadne divoká a v diaľke sa črtá akoby more. 
 Voda je rozliata aj po poliach.  
  Pri ceste späť vidím, že podzemná voda  prerazila už aj na druhú stranu ulice a obyvatelia ju odčerpávajú z pivníc. 
   
  
   
 Polícia usmerňuje dopravu. Ľudia žijúci v okolitých obciach a pracujúci v Bánovciach, ak sa chcú dostať domov musia si urobiť obchádzku smerom  na Topoľčany. 
   
  
 Veľa škôd napáchala táto povodeň. Zostáva len dúfať, že už takáto skaza viac nepríde. 
  
 Tu si občania môžu nájsť rady čo robiť v podobnej situácii: 
  
 http://www.zdravie.sk/sz/content/743-42783/zaplavy-rady-v-krizovej-situacii.html 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 
 
 
  V týchto dňoch sa počasie trošku umúdrilo. Ľudia dávajú do poriadku svoje obydlia. Dostali k dispozícii od obce kontajnery určené na odvoz odpadu. Vysúšajú byty a snažia sa všetko dať do pôvodného stavu 
 

  
  
  
  
  

   

