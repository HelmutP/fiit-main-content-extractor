
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jano Topercer
                                        &gt;
                Ekoblógia
                     
                 Diaľničenie slovenskej prírody 

        
            
                                    4.5.2010
            o
            11:40
                        (upravené
                4.5.2010
                o
                13:38)
                        |
            Karma článku:
                13.27
            |
            Prečítané 
            3456-krát
                    
         
     
         
             

                 
                    Pri výstavbe sporného povrchovo-tunelového variantu diaľnice D1 Turany - Hubová stavebníci ničia o. i. Prírodnú rezerváciu Rojkovské rašelinisko - najstaršie datované živé rašelinisko na Slovensku a stredoeurópsky ekologický unikát, ktorý tu prežil 15 000 rokov, no hrozí, že neprežije túto a budúcu štvorročnicu.
                 

                 24. apríla 2010 (v sobotu) okolo 8:30 som pri severozápadnom okraji Prírodnej rezervácie (PR) Rojkovské rašelinisko našiel skupinku 4 - 5 ľudí, ktorí kopali ryhu asi 1 m hlbokú a asi 0,4 m širokú, vedúcu od odbočky obslužnej cesty pripravovaného západného portálu diaľničného tunela Rojkov (od západu) rovnobežne s hlavnou cestou E50 vo vzdialenosti asi 6 - 8 m južne od nej k severnej hranici PR (obr. 1) a tesne popri nej k obci Rojkov (okres Ružomberok). V čase zistenia kopáči postúpili už takmer do polovice severného okraja rezervácie a trasa ich ďalšieho postupu bola jasne viditeľná podľa odkopávok mačiny i podľa výrubov krovín v páse asi 4 - 6 m širokom (obr. 2). Odhadujem, že na severnej hranici PR vyrúbali najmenej 40 % krovín, prevažne z biotopu národného významu Kr8 Vŕbové kroviny stojatých vôd (obr. 3) Podľa dovtedajšieho výkonu to vyzeralo, že výkopové práce začali najmenej deň predtým, t. j. v piatok 23. apríla 2010.      Obr. 1 Výkop ryhy pre preložku kábla, odvodňujúci severozápadnú časť PR Rojkovské rašelinisko (pohľad na západ, 24. apríl 2010)      Obr. 2 Výruby krovín, odmačinovanie a trasa plánovaného pokračovania výkopu ryhy pozdĺž severnej hranice PR Rojkovské rašelinisko (pohľad na východ, 24. apríl 2010)      Obr. 3 Zásahy do biotopu národného významu Kr8 Vŕbové kroviny stojatých vôd pri kopaní ryhy   Zistenie som hneď v ten deň oznámil ŠOP SR - Správe Národného parku Veľká Fatra vo Vrútkach a v pondelok 26. apríla 2010 aj Slovenskej inšpekcii životného prostredia (SIŽP) v Žiline a šéfovi Riaditeľstva B Príroda na DG Environment Európskej komisie v Bruseli RNDr. Ladislavovi Mikovi, CSc. Inšpekcia podnet prijala, začala vyšetrovanie a v stredu 28. apríla 2010 vykonala aj terénnu obhliadku. Už v pondelok však firma pracujúca pre stavebníka - Doprastav, a. s. Bratislava výkopové práce zastavila, hoci ešte v stredu dvaja geodeti Doprastavu aj inšpektori SIŽP tvrdili, že zásahy sú v súlade so stavebným povolením. Geodeti to dokladali podrobnou mapou z projektovej dokumentácie D1, na ktorej výkop pre preložku kábla síce zakreslený bol, ale stavebné povolenie ani rozhodnutie príslušného krajského úradu životného prostredia nepredložili. Po účinnom zverejnení (tlačová správa SOS/BirdLife Slovensko, Priateľov Zeme a Botanickej záhrady UK, Televízia Markíza, Denník Sme) sa už reakcii nemohlo vyhnúť ani Ministerstvo životného prostredia SR s Európskou komisiou a zrejme ešte v piatok 30. apríla 2010 stavebník nedokopanú ryhu rýchlo a bez preloženia kábla zasypal (v sobotu 1. mája 2010 som ju už našiel zasypanú - obr. 4). Žeby tiež v súlade so stavebným povolením?      Obr. 4 Zasypaný výkop ryhy pozdĺž severnej hranice PR Rojkovské rašelinisko (1. máj 2010)   Prvým kritickým bodom v ďalšom osude Rojkovského rašeliniska je, že výkop - hoci fyzicky zrejme tesne mimo rezervácie - ju celý týždeň intenzívne odvodňoval. V čase zistenia voda v ryhe odtekala rýchlosťou zhruba 1 - 2 l.s-1, v neskorších dňoch okolo 0,5 - 1 l.s-1, takže od piatka 23. apríla do piatka 30. apríla 2010 ryhou z rezervácie odtieklo niekoľko stoviek m3 vody (veľmi hrubý odhad 400 - 800 m3). Následkom toho poklesli hladiny podzemných vôd v severozápadnej nivnej časti rašeliniska priemerne asi o 9 - 10 cm, čo v tejto najcitlivejšej fáze životného cyklu môže znamenať významný nepriaznivý zásah do integrity biotopov (plôškovité preschnutie vrchnej časti rašelinového horizontu a zmeny ekofáz z hydrickej či litorálnej na limóznu, rozkolísanie hladín podzemných vôd, riziko zvýšenej mineralizácie organických látok a narušenia rašelinového procesu i iných pôdnych procesov) a do životaschopnosti vzácnych a ohrozených rastlín a živočíchov, predovšetkým plytkokoreniacich, menších, kompetične slabších a sukcesne skorších rastlín a málo pohyblivých vodných i epigeických bezstavovcov. Z európsky významných biotopov to postihuje najmä Ra6 Slatiny s vysokým obsahom báz, ale aj Lk5 Vysokobylinné spoločenstvá na vlhkých lúkach, Ra3 Prechodné rašeliniská a trasoviská, Vo3 Prirodzené dystrofné stojaté vody a prioritný biotop Ls7.1 Rašeliniskové brezové lesíky. Ak by niekto chcel nepriaznivé dopady tohto výkopu zľahčovať poukazovaním na neďalekú priekopu pri ceste E50, tak jej drenážny účinok je neporovnateľne menší, keďže je tu na viacerých miestach nespojitá (takmer neodvádza vodu) a jej dno leží zhruba o 0,4 - 0,6 m vyššie ako dno tej vykopanej ryhy.   Prvoradú dôležitosť tu má ešte fakt, že autori "štúdie" hodnotenia významnosti vplyvov D1 na územia Natura 2000 (PEŤKOVÁ, MIKA a kol. 2007 pod hlavičkou Creative, spol. s r. o. Pezinok) o výskyte biotopov Ra3, Vo3 a Ls7.1 v PR Rojkovské rašelinisko hlboko mlčia - tak ako mlčia o 3 z 5 devastovaných biotopov európskeho významu v trase D1 na Území európskeho významu (ÚEV) Malá Fatra, o 2 z 3 devastovaných biotopov európskeho významu v trase na ÚEV Veľká Fatra, o 3 zo 6 devastovaných biotopov európskeho významu v trase na ÚEV Rieka Váh, o mnohých druhoch európskeho významu ... a už len týmto mlčaním výrečne svedčia o hrubej neodbornosti a neprimeranosti ich hodnotenia (viac TOPERCER a kol. 2009 tu). Tú istú mieru neodbornosti a predpojatosti vidno aj v stanoviskách ústredia ŠOP SR v Banskej Bystrici a Ministerstva životného prostredia SR v Bratislave (najmä v stanovisku zo dňa 10. decembra 2007 č. 12025/2007-3).   Treba ešte zdôrazniť, že okrem vymenovaných biotopov sa na sotva trojhektárovom území PR (2,88 ha) skrýva najstaršie datované živé rašelinisko v SR (vek zhruba 15 000 rokov - HORSÁK 2003) a ojedinelá ukážka prelínania slatiniskových, prechodných a vrchoviskových biotopov s výnimočnou hustotou diverzity rastlín (až 17 typov rastlinných spoločenstiev a 160 druhov vyšších rastlín - KLIKA 1934; BOSÁČKOVÁ 1965, 1967; HÁBEROVÁ a FAJMONOVÁ 1995), z ktorých viaceré tu majú jedinú známu lokalitu na ÚEV Veľká Fatra (Carex diandra, Drosera rotundifolia, Ledum palustre, Stellaria palustris, Trichophorum pumilum, Triglochin maritima), alebo zvlášť početné populácie (Gymnadenia densiflora, Menyanthes trifoliata, Pinguicula vulgaris, Salix rosmarinifolia) či iný význam (Carex viridula a C. dioica, pozoruhodný glaciálny relikt - KLIMENT a kol. 2008). Z mnohých vzácnych živočíchov tu prežíva životaschopná populácia druhu európskeho významu Vertigo angustior.   Druhý a omnoho významnejší kritický bod pre rašelinisko je diaľničný tunel Rojkov, najmä jeho rúra a západný portál. Ten má stáť len asi 80 m juhozápadne od hranice rezervácie v mocných podstráňových sedimentoch (sú náchylné na svahové deformácie a z nich sa do rašeliniska infiltruje veľká časť podzemných vôd plytkého obehu), stavebná oblasť portálu končí iba 20 m od hraníc rezervácie a tunelová rúra má pretínať celé mikropovodie rašeliniska. Takýto masívny zásah do vodných zdrojov a celého povodia rašeliniska preň nesporne predstavuje existenčné ohrozenie. "Štúdia" PEŤKOVEJ, MIKU a kol. (2007) jeho význam bagatelizuje odvolaním sa na zmierňujúce opatrenia (ochranná vodotesná stena, izolácia stien rúry tunela Rojkov), ktoré - okrem toho, že majú pochybnú účinnosť - sa však v dokumentácii k stavebnému povoleniu ani v stavebnom povolení neobjavili. Za takéhoto stavu teda existenčné ohrozenie najstaršieho živého slovenského rašeliniska nielenže trvá, ale sa zostruje. Zostruje ho ten prostý a okato prehliadaný fakt, že pokiaľ má pár desiatok metrov od rašeliniska vyrásť veľká a zložitá stavba, ktorá sa stavia podľa takého "gumového" stavebného povolenia a takými chaotickými spôsobmi, aké predviedol stavebník pri výkope a zásype ryhy (o politických tlakoch ani nevraviac), tak sa tu s najväčšou pravdepodobnosťou skôr či neskôr vyskytnú iné podobné problémy (únik výplachu z vrtu? neutrafená injektáž? medzidepónium zeminy? ...), pre malé rašelinisko osve či kumulatívne osudné.   Uvedené riziká spolu so slovenskými štátnymi orgánmi zjavne podhodnocuje nielen Európska banka pre obnovu a rozvoj (s prísnosťou svojho environmentálneho posudzovania to v tomto prípade určite neprehnala), ale aj Európska komisia. Tá sa už niekoľko mesiacov zaoberá len zmierňujúcimi opatreniami k schválenému variantu a vytesňuje zásadný problém, že "štúdia" PEŤKOVEJ, MIKU a kol. (2007) nie je primeraným posúdením (appropriate assessment) variantov trasy D1 podľa článku 6(3) Smernice o biotopoch. Dobre zvážiteľné a spoľahlivé dôkazy o tom jej predstavitelia berú na ľahkú váhu a nepriznávajú ani existenciu 3 nezávislých expertíz, ktoré im tieto dôkazy priniesli (TOPERCER a kol. 2009, VOLF 2010 a 1 neverejná štúdia).   Dve zovšeobecnenia na záver:   1. Terajší spôsob výstavby diaľnice D1 vyvoláva nielen enormný nárast dlhu verejných financií (čo je však vratná veličina), ale aj ekonómami notoricky ignorovaný neúnosný nárast environmentálneho dlhu, obsahujúceho mnohé nevratné položky typu znehodnotenia unikátneho Rojkovského rašeliniska, ktoré sa nedá ani nahradiť, ani skonštruovať a ani zrekonštruovať.   2. Najdrahšie diaľnice nie sú tie, ktoré sa nepostavia. Najdrahšie diaľnice sú tie, ktoré sa stavajú v chvate, spolitizovane, netransparentne a násilnícky voči prírode i miestnym obyvateľom. Výstavba diaľnice D1 v úseku Turany - Hubová je toho exemplárnym príkladom.   Literatúra BOSÁČKOVÁ E. 1965: Jedna z prvých rezervácií rašelinných biocenóz na Slovensku. Ochr. Přír., Praha, 20: 132-133. BOSÁČKOVÁ E. 1967: Charakteristika vegetačných pomerov Štátnej prírodnej rezervácie Stankovianske rašelinisko. Českoslov. Ochr. Prír., Bratislava, 3: 127-138. HÁBEROVÁ I. a FAJMONOVÁ E. 1995: Rastlinstvo ŠPR Rojkovské rašelinisko. Ochr. Prír., Banská Bystrica, 13: 15-31. HORSÁK M. 2003: Malakozoologický inventarizační výzkum PR Rojkovské rašelinisko. Ochr. Prír., Banská Bystrica, 22: 91-96. KLIKA J. 1934: O rostlinných společenstvech stankovanských travertinů a jejich sukcesi. Rozpravy II. třídy České Akademie, Praha 44/8: 1-11. KLIMENT J., LISICKÁ E., ŠOLTÉS R., BERNÁTOVÁ D., DÍTĚ D., JANIŠOVÁ M., JAROLÍMEK I., KOCHJAROVÁ J., KUBINSKÁ A., KUČERA P., MIŠÍKOVÁ K., OBUCH J., PIŠÚT I., TOPERCER J., UHLÍŘOVÁ J. a ZALIBEROVÁ M. 2008: Príroda Veľkej Fatry. Lišajníky, machorasty, cievnaté rastliny. Vydavateľstvo Univerzity Komenského, Bratislava, 408 s. PEŤKOVÁ E., MIKA Ľ., ADAMEC M., ČUMOVÁ D., SUJOVÁ K., SCHWARZ M., ŠEFFER J., JANÁK M., RIPKA J. a POSPIECHOVÁ O. 2007: Hodnotenie významnosti vplyvov navrhovanej diaľnice D1 Turany - Hubová na územia sústavy NATURA 2000. 46 s.+prílohy, ms. [Štúdia; depon. in: Creative, spol. s r. o. Pezinok]. TOPERCER J., JASÍK M., DÍTĚ D., BERNÁTOVÁ D. a RIDZOŇ J. 2009: Významnosť vplyvov navrhovanej diaľnice D1 Turany - Hubová na druhy, biotopy, územia sústavy Natura 2000 a krajinu. 19 s., ms. Dostupné na internete: &lt;http://www.priateliazeme.sk/cepa/index.php?id=142&amp;level=1&amp;x=3229&gt;. VOLF O. 2010: Dálnice D1 - úsek Turany - Hubová, Slovensko. Posudek na hodnocení vlivů na evropsky významné lokality a ptačí oblasti. 20 s., ms. [Posudok; depon. in: SOS/BirdLife Slovensko Bratislava]. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (64)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            D1 — jednotka degenerácie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Nerušiť, iterujú!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            D1 Turany — Hubová kontra Natura 2000
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Šúľky pálky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Panská vôlenka II.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jano Topercer
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jano Topercer
            
         
        topercer.blog.sme.sk (rss)
         
                        VIP
                             
     
        ekológ (=zoológ v botanickej záhrade:)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    292
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    460
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekoblógia
                        
                     
                                     
                        
                            Qvety kalamity
                        
                     
                                     
                        
                            Eva-Lucia &amp; Filová Žofia
                        
                     
                                     
                        
                            ZOO Politico
                        
                     
                                     
                        
                            Ľudmila Populovna
                        
                     
                                     
                        
                            Aforizmy
                        
                     
                                     
                        
                            Poetree
                        
                     
                                     
                        
                            Iraciol
                        
                     
                                     
                        
                            Mikrozprávky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




