
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Sliacky
                                        &gt;
                Nezaradené
                     
                 Starý otec a vnuk 

        
            
                                    10.4.2010
            o
            9:37
                        (upravené
                10.4.2010
                o
                9:43)
                        |
            Karma článku:
                6.16
            |
            Prečítané 
            808-krát
                    
         
     
         
             

                 
                    Život píše tie najpravdivejšie príbehy.
                 

                 Stretávame sa takmer denne. Posedíme pri poháriku dobrého vínka. Mohol by som napísať, aj v ktorej vinotéke, keby majiteľ nebol žgrloš a zaplatil za reklamu. Ale o to mi v tejto chvíli nejde.       S Vincom (tak ho dnes nazvem, hoci sa volá inak) sme si posedeli aj včera. Reč sa točila o všeličom, najmä o prírode. K tej máme obaja blízko. Politike sa radšej vyhýbam. On - dôchodca - je tvrdý mečiarovec a ficovec. Až na ňom som sa očividne presvedčil, že niektorí ľudia berú Mečiara naozaj ako „otca národa“, že ho doslova žerú (prečo ho už nezožrali?), akoby pred ním bolo Slovensko len čiernou dierou. A Fico? Radšej odbočím na inú tému. To by sme už asi neboli kamaráti.       Nedávno som na blogu uverejnil Vincov zážitok. Len tak, na pobavenie i poučenie. „Čo stojí jedna dierka? Napríklad 24 eur.“ Tak som ho nazval. Nuž čo, pobavili sme sa. Včera mi Vinco vyčítal, že som tam nenapísal meno tej zubárky. - Hohoho! Máš nejaké divné predstavy o novinárčine! - vravím mu. - Dobre, ak chceš urobím si tvoju fotku, zopakuj mi pekne na diktafón, čo si zažil, a potom pôjdem za tou zubárkou opýtať sa na jej názor. - Jáj, to nie! Prečo by to mal robiť? Veď ona urobila chybu. Typické pre dnešného Slováka. Viac k tomu nemám čo dodať.       Prešli sme na inú tému. Vinco má dospelé a úspešné deti. Je na ne prirodzene hrdý. Nezávidím mu, naopak, teší ma, že na staré kolená si môže užívať skromný dôchodok. Aj s tým je spokojný. Veď pred Vianocami dostane ešte niečo navyše. - Ale nebyť Fica, nedostal by som to! - povie tvrdo, presvedčený o neomylnosti a nenahraditeľnosti svojho idolu.       Nedávno Vinco predal dedinský dom. Nepredal len nehnuteľnosť, ale aj svoju veľkú lásku - holuby. Tým sa venoval od detstva. Teraz ich nemá kde chovať. Balkón bytu na sídlisku je na takýto koníček primalý.       Peniaze, ktoré dostal za domček, rozdelil deťom rovnakým dielom. - Ja už predsa nič nepotrebujem, - vraví. - Na ten pohárik vína alebo na pivo mi z toho môjho dôchodku zostane.       Možno by si za tie peniaze mohol ísť liečiť astmu, niekde do Tatier alebo k moru, ale to nepokladá za podstatné. - Ja už nič nepotrebujem, - zopakuje. Tentoraz mu neverím, ale čo už...       Pri ďalšom, vari štvrtom poháriku vína Vinco trošku posmutnel. Spomenul si na Veľkú noc a na vnuka, ešte nedávno pubertiaka, ktorý ho prišiel na skok navštíviť. Ani nie tak preto, aby dedka videl a pozdravil, ale aby sa mu ukázal s novým autiakom. - Neviem, kde na to zobral peniaze! - krúti nechápavo hlavou Vinco, ktorý každú zarobenú korunu (za jeho produktívneho veku bola taká mena) obrátil nie dvakrát, ale vari aj desaťkrát. Do toho terénneho autiaka si nesadol. A vlastne ho vnuk ani doňho príliš neťahal. Až teraz si uvedomil, že s tým nadpriemerným zárobkom jeho detí nie je všetko s kostolným poriadkom, že je síce za ním aj kus poctivej roboty, ale aj kus ľudskej naivity, ba až hlúposti, že možno vysoký zisk firiem ide aj na vrub podobného dôchodcu, ako je on sám, že je za tým aj niečo, čomu sa hovorí úplatok. - Ale taký je život, - dodá a už na túto tému nechce hovoriť. Áno, aj takýto je život. Na Slovensku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Veľryba je hladná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Tak im treba?! ??? !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Alarm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Hanba!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Viete, čo má spoločné mandarínka s „pukaným“ vajcom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Sliacky
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Sliacky
            
         
        sliacky.blog.sme.sk (rss)
         
                                     
     
        59-ročný muž s energiou a dôverčivosťou mladíka.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    249
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1437
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




