

   
   
 So svojimi predkami však, okrem nápadne podobného vzhľadu, zdieľajú aj geneticky vrodené návyky, medzi ktoré neodmysliteľne patrí delenie si svojho územia na laicky povedané rajóny. Vzhľadom k tomu, že mestá pokrývajú pomerne „malú“ plochu, pochopiteľne sa ich vzájomné teritória prekrývajú. 
   
 Podobne ako u svojich vzdialených príbuzných, mačkovitých šeliem, aj ich menšie verzie si svoje územia delia na menšie, no a od toho závisí aj rozdelenie času, vhodného či už na lov alebo párenie. Platí to však najmä o mačkách žijúcich na dedinách či statkoch, kde sa často vyskytujú vo väčšom počte na jednom menšom území, žijúc v skupinkách (zväčša rodinné príbuzenstvo), ktoré sa navzájom veľmi dobre poznajú. Od toho si môžu dokonalým systémom rozdeliť čas na svoje aktivity, tak aby sa navzájom nerušil tento zaujímavý cyklus. 
   
 Mačky, ale platí to najmä o kocúroch, si svoje teritória veľmi dobre strážia. Hlavný rozdiel spočíva v pokrytí územia, zatiaľ čo dospelej mačke stačí na lov územie o rozlohe najviac 120 hektárov, kocúry sú už o čosi náročnejší – boli zaznamenané aj teritória o veľkosti 900 hektárov u jedného kocúra! 
   
   
 Takéto územia si nárokujú najmä tie mačky, ktoré žijú na ulici, bez svojho ľudského majiteľa. Čím lepšie sa o tieto zvieratá staráme, tým ľahšie si zvyknú na nové prostredie – teda na vyhriaty bytík s prísunom čerstvého jedla a vody. Tejto láskyplnej starostlivosti sa mačky zriekajú len veľmi ťažko.  
   
 U kocúrov je najdôležitejším bodom ich vlastného úspechu potencia. Vykastrovaný kocúr je viac mačkou ako pánom svojho územia a pre túlavé mačky nie je vôbec zaujímavý – jednoducho povedané, stáva sa nežiadúcim zaberačom územia pre potenciálne schopnejších kandidátov. Mačky sú prešibané, príjmu len silného jedinca, ktorý nesie dobrý genetický kód pre párenie sa.  
   
 Mačky si zvykajú na domáce prostredie omnoho ľahšie než kocúry, napriek tomu aj oni potrebujú spestrenie inak monotónneho života – jednoducho potrebujú poriadnu nočnú potulku, kde si môžu zaloviť, ako mimoriadne zvedavé tvory preskúmať nové územia a spáriť sa s vhodným kocúrom (orientujú sa väčšinou podľa „ostrieľanosti“ vhodného kandidáta na párenie – najlepšie keď je kocúr dobrý lovec, je zjavné že sa o neho nikto nestará – jazvy a otrhaný kožuch sú toho neklamným znakom). 
   
 Na takýchto potulkách neraz dochádza k tvrdým súbojom, pri kocúroch sa dá hovoriť o súbojoch na život a na smrť, keď porazený v lepšom prípade vyviazne trébars s vypichnutým okom či podobným zranením. Vzťahy medzi mačkami na ulici sú poriadne vyostrené, u domácich mačiek žijúcich v skupinke v jednej domácnosti sa to nedá povedať, aj keď aj tie pri stretnutí s „cudzincom od vedľa“ vedia byť poriadne ostré. 
   
 Jednoducho gény sa nezaprú, mačka si vždy bude chrániť to svoje.  
   
 Neraz dôjde k priamemu súboju aj medzi mačkou a kocúrom – väčšinou v prípade, že chráni svoje potomstvo, alebo nechce byť spárená s nežiadúcim kandidátom. Využívajú pri tom všetku svoju mrštnosť a najmä svoje ostré pazúriky. Ak sa vám podarí sledovať takýto súboj, v mnohom vám to bude pripomínať súboje oveľa väčších šeliem – afrických levíc alebo ázijských tigrov.  
   
 Mačka je navyše veľmi výkonný lovec. Pokiaľ ostáva na ulici, dokáže bez najmenších problémov za pár dní zlikvidovať celú miestnu komunitu hlodavcov. U kocúrov je to iné, tí lovia prevažne zo zábavy, nejednému majiteľovi sa stalo, že kocúr doniesol svoj úlovok ukázať domov. Všetko preto, aby sa pochválil, aký je dobrý. V žiadnom prípade ho ale za to netreba trestať, zaslúži si poriadnu odmenu, ktorú si užije omnoho viac ako obyčajne.  
   
   
   
   
 Ďalším neklamným znakom je značkovanie si svojho územia. Mačkám k tomu stačí otieranie brady o rôzne predmety na okolí, čím zanechajú po sebe stopu.  
   
 Kocúr vystrekuje poriadne dávky hustého moču (niekedy zmiešaného so spermiami). Tento zápach narozdiel od mačiek necítia len oni, ide o nepríjemnú vôňu, ktorá vám určite nebude rozvoniavať. Vďaka tomu ale majú nad svojím územím dokonalú kontrolu, ktorú môžu narušiť len mačky určené na párenie. 
   
 Mačky a kocúry majú rozhodne pestrý život. Pokiaľ sa rozhodnete práve pre toto zvieratko, musíte mať na pamäti, že stále ide predsa len o šelmu, aj keď v malom vydaní. Preto je lepšie nechať zvieratko vykastrovať, potom potrebuje omnoho menej priestoru a je vám viac „oddané.“  
   
 Jednoznačne je ale dobré mačku raz za čas pustiť von – na potulku, kde naberie nové skúsenosti a zážitky, bez ktorých by tieto zvieratká boli poriadne smutné. Navyše sú veľmi verné, označovanie ich za falošné patrí len k poverám. Z vlastných skúseností viem, že náš kocúr chodil každú noc von, no každé ráno sa vždy vrátil naspäť domov, kde sa najedol a poriadne si oddýchol po ťažkej noci. Bolo zaujímavé ho sledovať, viažu sa mi k tomu ešte zaujímavejšie spomienky.  
   
 Mačky sú ideálnym domácim zvieratkom nielen pre deti, na ktoré si veľmi rýchlo zvyknú, ale hlavne pre tých ľudí, ktorí nemajú až toľko času. Rozhodne sú menej náročné než psy či rybičky, dokonca vyžadujú menej starostlivosti ako malé hlodavce. Takže pre vás, ktorí nemáte priveľa času venovať sa svojmu zvieratku, ale nejaké by ste chceli, mačka alebo kocúr je ideálna voľba – určite vás nesklame! 
   

