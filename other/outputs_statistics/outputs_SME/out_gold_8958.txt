

 Makao je špeciálna administratívna oblasť Číny a bola prvou európskou kolóniou v Ázii, ktorá patrila Portugalsku až do roku roku 1999, keď prešla pod čínsku kontrolu a podobne ako Hong Kong si udržala vysoký stupeň autonómie na nasledujucich 50 rokov. 
  
 Foto: Makao (pohľad z vrchu Penha) 
 Ked som pristála na makajskom letisku, na južnom ostrove Taipa, letuška nám po čínsky popriala príjemný pobyt a s maskou na tvári (kvoli vtedajšej hrozbe H1N1) som vykročila krížom cez letiskovú halu. Kyvadlovou dopravou, rozvážajúcou ľudí  z letiska do kasín, som sa dostala do centra. Makao je 21. najnavštevovanejšou krajinou na svete práve kvôli množstvu kasín, spolu ich je až 28! Prvé dojmy boli úžasné, vysvietené mesto, ktoré ako sa zdá nikdy nespí. Tak toto je Makao, mesto nikdy nekonciacich hazardnych hier. Kasíno Lisboa (portugalsky pre Lisabon).  Farebne vysvietené  a predsa nie až tak impozantné ako nové kasíno otvorené v 1997, Grand de Lisboa, hneď vedľa. Zvedavosť mi nedala a s malou dušičkou som vkročila dnu. Tričko, kraťasy, cestovná taška na pleci. Odtiaľto hneď poletím, pomyslela som mysliac pri tom na vyobliekaých gamblerov z amerických filmov. V Ázii to však tak nebeží. Ľudia z každej vrstvy, džíny, ležérna košeľa, cigareta v jednej ruke, žetóny v druhej. Trochu som sa osmelila a podišla k najbližšiemu stolu. Black Jack.  Pri ďalšom páni s whisky hrali poker.  Wow, toľko stolov, toľko automatov, toľko hier! Pritom som ani zďaleka nebola v najväčšom kasíne, ktorým sa Makao môže pýšiť.  Kasíno Venetian je podľa počtu hracích automatov najväčším kasínom na svete a najväčšou budovou v Ázii. Tam som sa dostala až o pár dní neskôr.  Roprestiera sa v časti Taipa, ostrov prepojený s ďalším ostrovom nazvaným Coloane, ktoré oba spolu s poloostrovom Makao patria pod autonómnu oblasť Makao s rozlohou 28 km2. 
  
 Foto: Kasíno Venetian - najväčšie kasíno na svete (v pozadí) 
 Na druhý deň som sa vybrala pozrieť pamiatky. Po rôznych pokusoch dohovoriť sa po portugalsky som otvorila mapu a zašla k chrámu A-Ma, ktorý bol postavený roku 1488. Názov Makao je odvodený práve od názvu tohto chrámu. 
 Jedna z najpompéznejších pamiatok v Makau a zároveň jedna z najväčších kresťanských pamiatok v Ázii, je ruina katedrály Svätého Pavla, po ktorej zostala len fasáda a schodisko. 
  
 Foto: Ruina Katedrály Sv. Pavla 
 Po niekoľkých dňoch som to vzdala s portugalčinou a snažila sa dohovoriť mandarínskou čínštinou (ktorú sa na Taiwane popri iných povinnostiach snažím naučiť), čo však bol taktiež problém spojený s mnohými usmievavými príhodami, pretože ak vám aj rozumejú, odpovedia vám v kantóskej čínštine (oba jazyky používajú rovnaké znaky, výslovnosť sa však veľmi líši), ktorá je spolu s portugalčinou oficiálnym jazykom Makaa. Našťastie všetky nápisy, cestovné poriadky aj názvy ulíc sú dvojjazyčné, kantónske a portugalské.   A tak som sa vydala pozriet do ulíc ako napr. Rua de Coimbra (v meste Coimbra v Portugalsku som študovala), Rua de Lisboa (Lisabon - hlavné mesto Portugalska), ... 
  
 Foto: Rua da Cidade de Coimbra (Ulica mesta Coimbra) 
 Mnohé nápisy v parkoch a názvy ulíc boli, pre mňa, veľmi smiešne. Najviac ma rozosmial názov ulice Rua do peixe salgado (Ulica slanej ryby). 
  
 Foto: Rua do peixe salgado (Ulica slanej ryby) 
 Na ulici ste len málokedy mohli zahliadnúť portugalsky hovoriaceho podnikateľa. Tak predsa sa tu hovorí po portugalsky! Ale bolo to skôr zriedkavé. Keď som to už úplne vzdala, posledný deň, keď som na pošte odosielala pohľadnice s portugalským adresátom, prihovorila sa mi pri nalepovaní známok poštárka ázijského vzhľadu plynulou portugalčinou. Nepredvídajúc takýto zvrat, som s očami otvorenými dokorán od úžasu ledva vykoktala "obrigada" (portugalsky pre ďakujem). Okrem skutočnosti, že portugalčina sa používa už len ako úradný jazyk a v uliciach mi pripadala skôr ako mŕtvy jazyk, samotné Makao by sa kľudne dalo nazvat o Malým Portugalskom, alebo Portugalsko napadnuté Číňanmi, pretože tak to tam vyzerá. Hlavné námestie Largo do Senado, historické európske budovy, katolícke kostoly, rybárske dedinky v Coloane s malými reštauráciami tak typickými pre Portugalsko pripomínajú krajinu na juhu Európy. 
  
 Foto: Ulica v portugalskom štýle 
 V obchode ste na chvíľu aj zabudli, že ste v Ázii: dlhé regály s vínom (v Ázii veľmi netradičné) s názvami ako napr. Casal Garcia, Gazela (moje obľúbené portugalské zelené víno), v chladiacich boloch džúsy Sumol, pivá Super Bock, ... 
 Čínsky vplyv sa však nezaprie najmä na severe poloostrova Makao. Ošarpané staré budovy, pouliční predavači, stánky s čínskym občerstvením (pre Európana nie veľmi lákavé). 
  
 Foto: Čínsky vplyv (na severe Makaa, neďaleko čínskeho hraničného prechodu) 
 Ked vám niekto povie, že na Makao vám stačí jeden deň v rámci návštevy Hong Kongu (ako to mnohí robia pri "visa run"), neverte im . Makao je perfektná spleť Európy a Ázie, miesto kde sa stretávajú dve kultúry. Aj keď súostrovie sa nemôže pochváliť exotickými plážami, na aké sme v juho-východnej Ázii zvyknutí (more je plytké a pláže bahnité), architektúra, pamiatky a malebné rybárske dedinky stoja za to, nehovoriac o kasínach, ktoré sú naozaj na každom kroku, Makao nimi žije. 
 Už ostáva len dodať "saúde" (nazdravie)s vínko v jednej ruke a  "boa sorte" (veľa šťastia) s hracím žetónom v druhej ruke. 
   
   
   
   
 Zaujímavé info &amp; užitočné rady: 
 Makao je územie s najvyššou hustotou zaľudnenia na svete. 
 V Makau sa na rozdiel od zvyšku Číny jazdí na ľavej strane. 
 Makao je rozdelené do regiónov: polostrov Macao, ostrovy Taipa a Coloane. 
 Peniaze: Mena pataca (skratka MOP) ma takmer identicku hodnotu s HK dolár. Plati't sa dá oboma menami, výdavok je vśak zakaždým v patakoch. 1€ sa rovná približne 10 MOP. 
 Víza: Slováci majú bezvízový vstup do krajiny povolený na 90 dní. Pri výstupe z Macaa do iných provincií ČĽR sa vyžaduje čínske vízum. 
 Pláže: Makao má na juhu v Coloane 2 pláže:HacSa a CheocVan. Hac Sa je podľa názoru miestnych lepšia, nám sa však vôbec nepozdávala, posobila tak špinavo, že človek nemal napriek horúcemu počasiu chuť si ani prst na nohe namočiť. Našťastie hneď vedľa je kúpalisko. 
 Jedlo: Zväčša čínske jedlo (ryby, nudle, zelenina akú ste ešte nevideli), pripravte sa na čínsku stravovaciu kultúry (vypľúvanie kosti pred seba na stôl!!!, grganie - ale to ma už ani neprekvapuje). Portugalske jedlo bolo prekvapivo drahé, prezentované ako nejaká špecialita/rarita. Portugalskú pochúťku "pastel de nata" tu v Ázii poznajú ako makauskú (!!!) špecialitu "egg tart". 
  
 Foto: Pastel de nata 
 Lacné ubytovanie:  
 Nenápadný malý škaredý hostel za cca. 8€ na noc asi 2 min chôdze od kasína Lisboa, ktorý je ale zvnútra veľmi čistý a má príjemný personál. 
 Auguster's lodge: 24, Rua Do Dr. Pedro Jose Lobo, Floor 3J, Block 4, Edf. Kamloi,  http://www.sweet-house.de/ 
 Ďalšia možnosť je dvojhviezdičkový hotel neďaleko, v približne podobnej cene: 
 Central: Av. Almeida Ribeiro 264 &amp; 270, 4-7 posch., (+853)28373888, 28314051 
   

