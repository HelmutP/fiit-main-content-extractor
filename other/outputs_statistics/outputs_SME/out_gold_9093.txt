

 Musíte zabudnúť na staré myšlienky, na negatívne sebahodnotenia (možno až sebaodsúdenia). Aj napriek tomu, že ste možno doteraz celé roky o sebe uvažovali len skromne. Preto musíte zabudnúť a vypustiť zo svojho myslenia a slovnej zásoby vety typu: „Ó, to je beznádejné. Som smoliar. Tak na toto nemám. Mohol by som vyzerať aj lepšie. Mohol by som byť múdrejší, veď nemám dokončenú ani odbornú školu.“ Prečo zabudnúť? Pretože, ak si do hlavy púšťate jalový obsah, napríklad uvedené výroky, bulvár či klebety, potom aj vaše myslenie sa časom prispôsobí tejto latke kvality. Bude jalové. 
 Musíte dostať do hlavy nové myšlienky, nové slová, nové pozitívne sebahodnotenie – čiže svoj nový budúci obraz. Musíte dať svojmu mozgu novú potravu, lebo myšlienky vytvárajú realitu. Všetko, čo zažívame, sa deje podľa našich myšlienok. 
 Myšlienky vytvárajú realitu 
 Je to odvážne tvrdenie a mnohým z vás pripadne ako z rozprávky, celkom nereálne. Možno si poviete – nie sme kúzelníci. Predsa nestačí niečo chcieť a hneď to dostať. Veď predsa nevieme čarovať. Možno si aj spomínate, že už v detstve ste chceli veľa vecí a nepodarilo sa vám ich vykúzliť. A aj dnes – možno by ste chceli viac peňazí, stať sa sláv­nym, niečo veľké vytvoriť, možno nemať dlhy. 
 Môže samotné chcenie zmeniť tieto veci? Túto realitu? Ja tvrdím, že väčšinou áno. To, čo v živote máte dnes, bolo vytvorené tým, čo ste si o sebe mysleli v minulosti. Dnes ste takí, akí ste sa naučili byť. Máte to, čomu ste roky verili. To vytvorilo vašu súčasnú životnú situáciu. 
 My zodpovedáme za to, čo máme 
 To, ako zmýšľame, priamo ovplyvňuje to, čím sme. Znie to neuveriteľne pre človeka, ktorý sa na udalosti okolo seba díva, ako na fakty, ktoré sa nedajú zmeniť. No skúste sa na svet okolo seba pozerať inak. Ako na sled udalostí. 
 
 Väčšinu udalosti vytvárate priamo vy. Tým čo robíte, chcete, ako sa rozhodujete. 
 Iné udalosti spoluvytvárajú aj iní ľudia, ale vy ich môžete zmeniť. 
 Sú aj udalosti, na ktoré nemáte priamy vplyv. Na ne však reagujete. A tým ovplyvníte, čo sa bude diať ďalej. 
 Len veľmi málo situácií je takých, ktoré vzniknú a nedajú vám ani možnosť reagovať a ovplyvniť tak ďalšie dianie. 
 
 Mnoho ľudí dnes žije v predsudku, že život je hotová vec. Že to, ako žijeme, alebo kde sa nachádzame, čo máme, s kým žijeme bolo dané len súhrou okolností a že sa to nedá zmeniť.  
 Preto sa bližšie pozrime na myšlienky, ako na produkt mozgu. 
 Vznik nových myšlienok 
 Myslíme na takej úrovni a v takých pojmoch, aké sme v minulosti do svojho mozgu vložili. Výchovou, vzdelaním, praxou v živote, dennou rutinou, skúsenosťami, s kamarátmi na pive, sledovaním televízie, z novín, v škole, od kolegov… Jednoducho v minulosti sa ku nám dostávali názory, príbehy, postoje a ovplyvňovali nás. 
 Človek sa učí opakovaním a napodobňovaním. Z toho, čo sa naučí potom môže zložiť niečo nové. Najskôr do mozgu dostanete slová, obrazy, názory a z toho všetkého sa postupne začínajú skladať vaše nové myšlienky.  
 Dá sa povedať, že to, čo si vpustíme do hlavy je niečo ako potrava pre mozog. Ak radi čítate hlboké myšlienky, počúvate nápady iných, príjemné zážitky, odvážne vízie, aj vaše myslenie bude produkovať podobné obrazy. 
 Jedna kvapka vody kameň nezmení. Dlhodobé kvapkanie vyhĺbi do kameňa dieru. Podobne je to aj s myslením.  
 Aj malé bežné dennodenné zážitky nás formujú. Ak si denne a roky budete opakovať nejaký výrok, tak sa mu postupne prispôsobíte. Naprogramujete týmto opakovaním svoje myslenie. Nuž a ak sa dá niečo naprogramovať, dá sa to zvyčajne aj zmeniť. Preprogramovať, zmeniť svoje myslenie dokážete tým, že si budete opakovane hovoriť a myslieť nové tvrdenia alebo výroky. Také, ktoré budú vaše sebavedomie posilňovať. Pomocou nich budete manipulovať so svojím podvedomím. Tento postup sa nazýva afirmácia, hoci u nás je asi známejšie slovo autosugescia. 
 V zásade však ide o to isté: budete samých seba uisťovať v určitej veci. Keďže však pôjde len o kladné, pozitívne tvrdenia, pôjde o afirmáciu. 
 Opakovaným pripomínaním kladných výrokov sa dá zvýšiť sebavedomie. Možno niekto namietne, že je to len obyčajné vymývanie mozgu. A veruže má pravdu. Ak máte v mozgu balast a neužitočné názory, oplatí sa vám ich odtiaľ veľmi rýchlo vypláchnuť. Vytvoríte tak priestor pre niečo užitočné. 
 Častým opakovaním si vhodných slov sa dá dosiahnuť zmena postojov. Ak niečo budete často opakovať (presviedčať sa, sugerovať), uveríte tomu a začnete sa podľa toho správať. Takto je možné zvýšiť si sebavedomie, motivovať sa, upriamiť svoju pozornosť na niečo žiaduce. Dokážete teda zmeniť vaše myslenie. 
 Ciele afirmácií 
 Už vieme, že afirmáciami ovplyvníme svoj postoj. Nimi ovplyvníme nielen svoje názory, ale aj názory iných na našu osobu. Motivujú nás k lepším výsledkom. I napriek tomu sa vám asi bude zdať čudné, ak si máte dookola opakovať nejaké tvrdenie, napríklad „som vynaliezavý“. Možno si poviete „čo som nejaký zaklínač hadov, alebo šaman, že mám omieľať nejakú mantru?“ No nie je to také primitívne, ako to vyzerá. Veď človek sa učí len opakovaním. 
 
 Opakujeme určitú činnosť, až sa nám začne dariť. 
 Opakovane čítame nejakú odbornú knihu, až sa to naučíme a staneme sa odborníkmi. 
 Opakujeme určité tvrdenia, až im začneme veriť. 
 Opakujú nám reklamy, až ideme a kúpime. 
 Opakujeme si, že sme nemehlá a potom všetko kazíme. 
 
 Cieľom bude postupne zmeniť starú schému vašich myšlienok o sebe, ktorú ste si možno vytvárali niekoľko rokov. Ak sa, napríklad, cítite byť "nemehlo", skúste si často opakovať "som šikovný". Časté opakovanie takého výroku môže spôsobiť, že sa za šikovného postupne začnete považovať. Právom? Neprávom? To je jedno. Prečo je to jedno? 
 Ak veríte, že môžete zvládnuť o niečo viac, tak to aj skúsite. Ak skúsite, je možné, že uspejete. Ak to zvládnete, zvýši sa vaše sebavedomie. O to odvážnejší ďalší cieľ si dáte. A dostanete sa tak do pozitívnej špirály, keď s nárastom sebavedomia si následne dáte aj vyšší osobný cieľ. 
 Opakované, pozitívne tvrdenia samého k sebe môžu zmeniť váš podvedomý postoj sebe k samému. A na základe neho sa začnete inak správať. Sú to však pomalé procesy. Kto to neskúsil, nebude veriť, že to funguje. 
 Ako ste na tom vy? Používate takéto pozitívne výroky?  

