
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lucia Anďalová
                                        &gt;
                Nezaradené
                     
                 Tanky v strede Popradu - PMD 

        
            
                                    31.5.2010
            o
            15:01
                        (upravené
                10.6.2010
                o
                13:11)
                        |
            Karma článku:
                3.43
            |
            Prečítané 
            1229-krát
                    
         
     
         
             

                 
                    Pred popradskou Arénou stojí vetroň, bazén s loďami a obrnený transportér. Pomedzi ne sa prechádzajú sovietski a nemeckí vojaci a obrnení Rimania. Nie, nie sme pri nakrúcaní filmu. Je posledná májová nedeľa a koná sa prvý ročník PMD, Popradského modelárskeho dňa.
                 

                 
OT-64Jozef Anďal
      Žiadne hračky   Na svoje si prišli všetky vekové kategórie. Najmenších okúzlili diaľkovo ovládané modely lodí, vrtuľníkov, áut a tankov. Staršie a zručnejšie deti ich mohli aj vyskúšať. Ôsmaka Petra sme stretli pri riadení RC vrtuľníka. Prišiel aj napriek zlomenine. „Veľmi som sa tešil. Keď mi dali na nohu sadru, bál som sa, že ma mama nepustí, ale nakoniec sme ju s ockom prehovorili." Je nemožné prehliadnuť na zemi stojace lietadlá s niekoľkometrovým rozpätím krídel. Samozrejme, tu sa už nehovorí o hračkách. Cena funkčných modelov sa pohybuje vo vyšších reláciách a to isté platí o kvalite.   Prichádzame k nádherne vypracovaným vláčikovým krajinkám. Deti sa od nich nemohli odtrhnúť po celé  hodiny, najmä od miniatúrnej lesnej železnice pána Fifíka. Rovnako ťažko odchádzali aj starší, ktorí spomínali na detské časy alebo naberali inšpiráciu na zdokonalenie vlastného koľajiska. „Domčeky sme zlepili už doma... Teraz pridávame stromy a trávu," ukazuje nám štrnásťročná Katka na prefabrikovanom podklade. Malá železnica vedúca cez tunel a stanicu nám pomaly rastie pred očami. Cieľom tohto workshopu je ukázať ľuďom, že pekná krajinka sa dá vyrobiť už za jediný deň.   Majstri   Dávno neplatí, že lepenie vystrihovačiek a plastových stavebníc je zábavou len pre deti. Mnohých nikdy neprestalo baviť alebo sa k nemu vracajú na staré kolená. Majstrovské kusy vyžadujú dávku trpezlivosti a improvizácie. Keďže toto umenie je zamerané najmä na detaily, vystavovatelia sú na svoje dielka veľmi hrdí. Každoročná súťaž Plastic Session toto podujatie oživila vďaka množstvu vystavených modelov. Zapojili sa celé rodiny, napríklad Kostkovci-Zelinovci z Dobšinej. Všetci traja si odniesli domov za svoju prácu ocenenie.   Krátky dlhý deň   Od deviatej do sedemnástej sa v atraktívnych priestoroch Arény premleli asi tri tisícky návštevníkov zo širokého okolia. O 14:45 boli vstupenky zlosované v tombole a rozdalo sa 35 atraktívnych cien. Atmosféru dotvárali Vojenský historický klub z Humenného ukážkami uniforiem a zbraní z druhej svetovej vojny.   Hlavný organizátor, Ing. Jozef Anďal bol s výsledkom akcie spokojný. „Prekonalo to všetky moje očakávania. Som veľmi rád, že návštevníkom sa výstava páčila a zabavili sa. Verím, že radi prídu na PMD aj o rok."   O tom, že sa nemýli, svedčia slzičky detí opúšťajúcich Arénu proti svojej vôli v náručí rodičov.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Zombie medzi nami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Otvorený list MUDr. Štefanovi Paulovovi - Pi*a z h*ven sú bez viny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Nové polyfunkčné budovy v Poprade. Pre koho?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Protidrogová výchova na školách – vyhodené peniaze!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Zlatý život študentský 5: Ak nevytŕčaš z davu správne, si nepriateľ
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lucia Anďalová
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lucia Anďalová
            
         
        andalova.blog.sme.sk (rss)
         
                                     
     
        Študujem žurnalistiku ale už sa zotavujem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1680
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Študentský život
                        
                     
                                     
                        
                            Poprad
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            WEBJOURNAL
                                     
                                                                             
                                            Prečo je štát zlý hospodár?
                                     
                                                                             
                                            Apokalypsa v denníku Nový čas
                                     
                                                                             
                                            Láska v čase internetu (toto zabolelo)
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Farby strachu (jediná slovenská hororová antológia)
                                     
                                                                             
                                            Eastlake: Hájili sme hrad
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            P*ča z h*ven
                                     
                                                                             
                                            Ľahká múza
                                     
                                                                             
                                            Theatre des vampires
                                     
                                                                             
                                            Sunn o)))
                                     
                                                                             
                                            Psyclon Nine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            judytha
                                     
                                                                             
                                            cookingchinchillas
                                     
                                                                             
                                            afinabul
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyanide &amp; Happiness :)
                                     
                                                                             
                                            www.dolezite.sk
                                     
                                                                             
                                            www.slovakgothiccommunity.sk
                                     
                                                                             
                                            www.darklyrics.com
                                     
                                                                             
                                            www.cracked.com
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Otvorený list MUDr. Štefanovi Paulovovi - Pi*a z h*ven sú bez viny
                     
                                                         
                       Nové polyfunkčné budovy v Poprade. Pre koho?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




