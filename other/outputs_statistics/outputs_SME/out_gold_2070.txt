

 V Trenčíne sa po preložení železničnej trate a vybudovaní nového železničného mosta vytvorí priestor na prípadnú zástavbu nábrežia. Mesto vypracovalo urbanistickú štúdiu v ktorej približuje svoj zámer s dotknutou lokalitou. Existuje obava, aby celý proces (nie len prípravny, ale aj realizačný) nepripomínal nákupné centrum Aupark, o ktorom sa pred niekoľkými mesiacmi konalo miestne referendum. Preto považujeme za nutnosť, aby bola k takto význámnému zámeru prizvaná aj verejnosť. Pôjde o výraznú zmenu daného územia, ktorá na dlhé desaťročia môže zmeniť panorámu mesta Trenčín. 
  
 Nábrežie Váhu v centre mesta - žltou farbou je vyznačené dotknuté územie. Územie, ktoré je veľké ako celé historické centrum mesta tvoria z podstatnej časti zelené a verejne prístupné plochy (S výnimkou lodenice, kolkárne a parkoviska pri plavárni. Zelená plocha letnej plavárne je oplotená s časovo obmedzeným prístupom.) Autor: Norbert Brázda/Google Earth 
 Diskutovať budú: 
 Ing. Branislav Celler, primátor mesta Trenčín, Ing. arch Matúš Vallo, ateliér Vallo Sadovsky Architects, Ing. arch Marek Guga, ArTUR, Ing. Drahomír Stano, riaditeľ správy CHKO Biele Karpaty, Jaroslav Olah, podnikateľ, Bc. Richard Ščepko, študent 
 Moderátor: Štefan Hríb 
   
 Priebeh diskusie:  
 Úvodná prezentácia územia  História rieky Váh v centre mesta, ukázky územia spred 100 rokov, súčasná situácia, budúcnosť územia. Zámery mesta. Ukážky riešení podobných území v iných mestách.  Bývanie pri rieke - predstavenie projektu Ing. arch Matúš Vallo predstaví štúdiu projektu “Bývanie pri rieke”, ktorá bola impulzom pre zorganizovanie tejto diskusie. Ich štúdia je zverejnená na web stránke Vallo Sadovsky Architects. 
  
 Štúdia obytného komplexu v atraktívnom prostredí pri rieke Váh. Zdroj: vallosadovsky.sk 
   
 Skvelé, zaujímavé, poburujúce alebo nepriateľné? Prečo sa architekti rozhodli pre takúto štúdiu? Môžeme o niekoľko rokov očakávať podobné objekty na novom nábreži? Zdroj: vallosadovsky.sk 
   
 Nábrežie - aké sú záujmy a zámery mesta? Primátor mesta, B. Celler predstaví aktuálne záujmy a zámery mesta v danom území.   Ako by nábrežie mohlo/malo vyzerať? Diskusia o predstavách a názoroch účastníkov na riešenie nábrežia.  Ako by sa o budúcnosti nábrežia malo rozhodovať? Diskusia o hľadaní „verejného záujmu“ a dosahovaní konsenzu laických i odborných názorov pri rozhodovaní o území, ktoré na desiatky rokov ovplyvní vzhľad i život mesta. 
 Súčasťou diskusie bude aj spomínaná prezentácia, kde bude rozsiahlejšou formou prezentovaná daná lokalita. Mesto má tento zámer uvedený na svojej internetovej stránke. Prikladám niekoľko vizualizácií, ktoré mesto vytvorilo vo svojej štúdií. 
  
 Vizualizácia novej Nábrežnej ulice. Pohľad od vyústenia promenádneho mosta smerom na Električnú ulicu. ZDROJ: trencin.sk  
  
 Vizualizácia novej Nábrežnej ulice. Pohľad od vyústenia promenádneho mosta smerom na mestskú plaváreň a štadión. ZDROJ: trencin.sk  
  
 Vizualizácia nábrežia pri mestskej plavárni. V pozadí nový železničný most. ZDROJ: trencin.sk  
  
 Ďalšie možné podoby nábrežia. ZDROJ: trencin.sk 
   
 ZDROJ: www.sohk.sk  
  
 Zastavať, alebo ponechať prírodný charakter? Ak zastavať, tak ako a čím? Ak ponechať nábrežiu prírodný charakter, ako má vyzerať? Je výstavba luxusných bytov verejným záujmom? ZDROJ: trencin.sk 
  
 Vizualizácia nového železničného mosta cez rieku Váh. ZDROJ: trencin.sk  
   
 Na urbanistickej štúdií je vidieť dotknuté územie. Pre lepšie rozlíšenie návrhu, kliknite na obrázok. Zobrazí sa jeho väčší rozmer. Aký máte na takúto zástavbu názor?  
 Koridorizácia železnice sa postupne blíži k mestu Trenčín. Pripravuje sa projekt nábrežia, ktoré má byť atraktívne nie len pre návštevníkov, ale hlavne pre samotných obyvateľov mesta Trenčín. O tom, ako by malo vyzerať budeme diskutovať spolu s ďalšími hosťami a verejnosťou. Myslím, že by sme sa mali zaujímať, aké sú plány mesta s touto lokalitou. Alebo to necháte na "tých" druhých? Ukážme, že nám na tomto meste záleží a nie je nám jedno, ako bude vyzerať o niekoľko rokov. Tešíme sa na Vás! 
   
 Účasť môžete potvrdiť aj na Facebooku. 
   Diskusiu organizujú: Richard Ščepko, Norbert Brázda, Richard Medal 
   

