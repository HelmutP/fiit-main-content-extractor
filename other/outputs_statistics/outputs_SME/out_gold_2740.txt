

 Úplne najväčší nedostatok nášho odvodovo-sociálneho systému je ale marginálne zdanenie vo výške 84%. To znamená, že keď rodina dostáva na sociálnych dávkach povedzme 300€ a otec tejto rodiny sa rozhodne pracovať za 100€ (napríklad, že bude hodinu denne zametať chodník a odhŕňať sneh), celkový čistý príjem rodiny nestúpne na 400€, ale iba na 316€ a rozdiel si nechá štát (36€ sú odvody a o 48€ sa znížia sociálne dávky). 
 Naopak, keď niekto zarába 5.000€ mesačne a zvýši svoj príjem na 5.100€, z prírastku mu zoberie štát len 19€ na dani z príjmu. Inými slovami, štát chudobnému takmer všetko zoberie (84€ zo 100€) a bohatému takmer všetko nechá (81€ zo 100€). 
 Preto navrhujeme (celý program nájdete na www.120napadov.sk) zaviesť Odvodový bonus ako ucelené riešenie spomínaných problémov. Odvodový bonus je garancia životného minima pre každého občana tohto štátu, polovičné odvody a výrazne jednoduchší systém (počet parametrov klesne z 279 na 19). Viac k Odvodovému bonusu nájdete tu. 
 Napriek zrejmým výhodám Odvodového bonus existujú aj kritické hlasy. Celkovo som spísal 30 pripomienok, ktoré sa spolu s vyjadreniami nachádzajú tu. Kto si ich prečíta, zistí, že neexistujú relevantné dôvody Odvodový bonus nezaviesť. 
 Rád by som ale tu priamo reagoval na niekoľko výhrad, ktoré sa objavili v poslednej dobe: 
 
 Odvodový bonus ruší podporu v nezamestnanosti a nemocenské 
 
 Odvodový bonus výrazne zvýši čistý príjem rodiny a kto chce, sa môže poistiť pre prípad nezamestnanosti a práceneschopnosti. Pokiaľ nenájde vhodnú súkromnú poisťovňu, môže tak spraviť v Sociálnej poisťovni, ktorá bude naďalej tieto produkty ponúkať za dnešné sadzby (1,5% a 2,0% zo superhubej mzdy). Rozdiel je v tom, že poistenie bude dobrovoľné. 
   
 
 Odvodový bonus spôsobí deficit v štátnom rozpočte 
 
 Toto tvrdenie jednoducho nie je pravdivé. Prepočty som robil ja, MESA10 a Slovenská akadémia vied. Všetci sme sa dopracovali k neutrálnemu výsledku. Odvodový bonus nezvýši zaťaženie štátneho rozpočtu. Dôvod, prečo sa toto tvrdenie tak dlho drží je ten, že výpočet je pomerne komplikovaný. Nie je to skratka v zmysle "tu zoberiem 2 a tu pridám 2". 
   
 
 Odvodový bonus nie je adresný 
 
 Práve že je a to s absolútnou dôslednosťou. Každý má nárok na určité štátne dávky (na občana, na deti, na matku s dieťaťom do troch rokov, na osamelú matku a na invalida) a tieto dávky sa znižujú s rastúcim príjmom daňovníka. To znamená, že kto nemá príjem, dostane dávky v celom rozsahu a kto má veľmi vysoký príjem, nedostane žiadnu podporu od štátu. Medzi tým je lineárny a pozvoľný (10%-né klesanie) prechod. 
   
 Zavedenie Odvodového bonusu medzi množstvom iných výhod výrazne zvýši čisté príjmy rodiny. Aby sme to vedeli lepšie znázorniť, vyrobili sme mzdové koliesko, na ktorom sa dá v závislosti od príjmu a počtu detí úplne jednoducho odčítať zvýšenie čistých príjmov. Myslím, že bohate stačí ľuďom priblížiť výhody tejto reformy a netreba ich zaťažovať detailmi, prepočtami, marginálnym zdanením a pod. Samozrejme kto má záujem nájde všetko tu a tiež rád zodpoviem akúkoľvek detailnú a odbornú otázku, ktorá sa nenachádza medzi často kladenými otázkami. 
 Kto má záujem o mzdové koliesko, nech si ho prosím objedná tu: http://120napadov.sk/koliesko 

