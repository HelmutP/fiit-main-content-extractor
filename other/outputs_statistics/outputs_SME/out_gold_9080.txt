

 Náboženstvá 
 Emberá a Wounaan nerobia rozdiel medzi fyzickou podstatou človeka a jeho dušou. Veria, že tak ľudia, ako aj zvieratá, rastliny či prírodné javy majú tzv. jai - zlého alebo dobrého ducha. Od nadprirodzených síl, ale aj od kúziel a umenia šamana jaibaná závisí, či zvíťazí zlo alebo dobro. Existujú dvaja stelesnení duchovia, ktorí stoja proti sebe: Ewandama - dobrý a tvoriaci duch, a Tiauru - jeho zlý a škodlivý protivník. Misionári zmenili náboženskú vieru Indiánov pokiaľ ide o vnímanie hriechov, nebies, pekla a odpustenia, ale tradície a zvyky o silách duchov, vždy spojené s pralesom a prírodou, stále pretrvávajú. 
 Náboženské praktiky sa sústreďujú na šikovnosť šamanov, ktorí vďaka znalostiam medicíny, rastlín a ich účinkov a schopnosti vyháňania zlých duchov svojimi totemami, majú úctu a uznanie Indiánov. Určenie jai je rozhodujúce pre stanovenie pôvodu choroby, trápenia alebo nešťastia. Šaman môže kontaktovať jai a pokúsiť sa zmeniť skutočný stav ťažkostí. Dokáže napríklad vyčistiť rieky alebo pôdu od diabolských vplyvov ducha a všetkých ďalších nebezpečenstiev. Nemôže však prísť o žiaden liečiteľský totem, pretože každý je určený pre inú chorobu či bolesť. 
  
  
 Šamani nemajú nijaké špeciálne hierarchické postavenie, sú to obyčajní muži, ktorí majú uznanie vďaka svojim znalostiam prírodných liečiteľských zdrojov a schopnosti komunikovať s duchmi. Nie sú ostatným Indiánom nijako nadradení, nie sú to nijakí panovníci. Ich vedomosti o človeku a prírode vzbudzujú obdiv, rešpekt, dôveru a vďačnosť. 
 Rituály, legendy a aj všetko, v čo Indiáni veria, má pevný vzťah k prírode a prostriedkom, z ktorých bol človek stvorený. Pre nich je prales posvätné miesto, kde sídlia duchovia objavujúci sa každý deň v nekonečnom slede udalostí. Zdravie a choroba, život a smrť, radosť a bolesť sa prelínajú a vytvárajú realitu, kde sa všetko telesné i duševné harmonicky spojí. Dobro a zlo je výsledok rovnováhy medzi oboma svetmi - fyzickým a duševným, ktoré sú jasne oddelené, no pevne spojené. 
  
  
  
 Oslavy a rituály 
 Aj v 21. storočí si Indiáni Emberá a Wounaan dokázali uchovať odkaz svojich predkov, svoje tance a spevy, oslavy a rituály. 
 Jednoduché oslavy sú súčasťou každodenných udalostí, ako je zasvätenie, svadba, pôrod, smrť, ale aj dokončenie zberu či spoločných prác. Muži hrajú na píšťaly alebo malé bubienky, sprevádzajú ženy a deti pri tancoch a piesňach oslovujúcich zvieratá dažďového pralesa a pijú veľa pálenky chicha alebo guarapo. 
 Niektoré príležitosti Indiáni dosiaľ oslavujú tradičnou formou aj cez určitý vývoj vzdelania a uvedomenia si prírodných javov. Ide napríklad o ceremóniu ukončenia dievčenskej puberty a jej zasvätenia do života ženy. 
  
  
  
 Zasvätenie 
 Rituály okolo dospievania prebiehajú u dievčat Emberá vo veku dvanásť až trinásť rokov, teda v čase prvej menštruácie. Keď dostane Indiánka prvýkrát menzes, matka jej ostrihá vlasy a musí sa ukryť, väčšinou vo svojom obydlí v akejsi „izbe" utvorenej z listov palmy, kde je bokom od pohľadov ostatných. Tam zostáva pätnásť dní pod dozorom svojej matky. Počas tohto obdobia nesmie nikoho vidieť a nikto nesmie vidieť ju, pretože pohľad by jej mohol spôsobiť jazvy na tele i na duši. Nemôže vykonávať nijaké práce, ani pre svoju rodinu, a musí iba odpočívať. Nesmie tiež chodiť, hovoriť a pozorovať okolie. Musí zostať ticho zavretá vo svojom „väzení", čím predíde zlým vplyvom a uvedomí si ťarchu ničnerobenia. 
 Dievča dodržuje i prísnu diétu. Môže jesť iba varený banán a guacuco, čo je malá riečna ryba, ktorá je veľmi výživná. Konzumuje ju suchú, nesolenú. Prvých osem dní dievčina vstupuje do novej etapy. Odlúčenie od ostatných jej umožní prechod a ochráni ju. Ôsmy deň môže so sklonenou hlavou opustiť obydlie. Nesmie sa na nikoho pozrieť, ide vykameňovanou cestičkou k rieke. Telo má ozdobené tetovaním jagua. Keď zíde k rieke, musí sa rovnakou cestou vrátiť domov. Potom môže zdvihnúť hlavu a prijať pohľady ostatných bez toho, aby jej to spôsobilo ujmu. 
 Po návrate domov uvarí svoje prvé jedlo z banánov, ale to sa vyhodí ako dôkaz, že sa musí najprv naučiť variť. Potom uvarí druhé jedlo, až to sa môže jesť. Dievča už má dovolené oslovovať iné osoby. Nasledujúcich osem dní sa ešte nesmie zúčastniť na spoločných prácach a tým si naplno uvedomí, aké nepríjemné je zaháľanie a lenivosť. Po pätnástich dňoch sa vráti k bežným každodenným prácam a je už takmer premenená na silnú a tvrdú ženu. Potom už zostáva iba fáza posvätenia pálenkou. 
  
  
  
 Druhá časť zasvätenia prebieha ako všeobecná slávnosť za účasti všetkých príbuzných a známych. Dievčina je potretá jaguou a dá sa jej slávnostne vypiť kukuričná pálenka chicha. Chór žien pri tom okolo nej spieva a tancuje. Obvykle však pod vplyvom chichy skoro zaspí a potom ju ženy umyjú a dajú matke, aby ju obliekla a uložila do pos-​tele. Týmto sa stáva dospelou a pripravenou vstúpiť do manželstva. 
 Príchod menštruácie pre dievčatá Emberá znamená začiatok ich roly v indiánskej spoločnosti. Musia byť silné a pracovité. Tento rituál ich má posilniť, pomôcť im vyvarovať sa zlým vplyvom a uvedomiť si nevhodnosť lenivosti. Zároveň sa učia prijať zákony prírody, ktoré prinášajú menštruáciu. 
 V nasledujúcich rokoch sa žena Emberá vždy, keď má menštruáciu, schová počas ôsmich dní vo svojom obydlí a nevychádza. Kúpe sa až trikrát denne a nesmie pripravovať jedlo, pretože by sa naň mohla preniesť infekcia. Bohužiaľ, týmto pre nás normálnym javom sa žena stáva pre svoje okolie nečistou a zraniteľnou. 
   
 Buďte šťastní, Hirax 
   
   
 Aktivity Hiraxa: 
 Utorok, 22. 6. 2010, Zlaté Moravce, súkromná čítačka  v  reedukačnom zariadení pre mladistvých. Vstup  jeden "hriech" a jeden  úsmev.    Utorok, 22. 6. 2010 o  16:02, Piešťany, Kníhkupectvo MODUL (Vinterova),  Hiraxova čítačka, vstup jeden úsmev.    Pondelok, 28. 6. 2010, Topoľčany, 17:00 hod  Galéria (program pre mladých - spevokol, básne Hirax), 18:00 reštaurácia   Kaiserhof Nám. M. R. Štefánika (čítačka Hirax). 
 Máj-jún 2010: Výstava Hiraxových fotografií z Thajska v martinskej   kaviarničke Kamala. Pozor, nejedná sa o žiadnu "galériu", ale o 12násť  záberov, ktorými som sa snažil vystihnúť túto krajinu. Vstup  jeden  úsmev. 
 Štvrtok, 4. 11. 2010, Svet, Oficiálne vydanie  Hiraxovho štvrtého románu "Nauč ma umierať".   Štvrtok, 4. 11.  2010, 16:37, Bratislava, Svet knihy Bene Libri (v  centre mesta, Obchodná), Prvá Hiraxova čítačka k jeho štvrtému románu  "Nauč ma umierať". Vstup  jeden úsmev.   Streda 10. 11. 2010 o  11:02 hod., Nitra, Krajská knižnica K. Kmeťka. Hirax -  čítačka. Vstup jeden úsmev.  Štvrtok 11. 11. 2011(-1) o 11:11, Šaľa, gymnázium, Hirax - čítačka. Vstup jeden úsmev. 
   

