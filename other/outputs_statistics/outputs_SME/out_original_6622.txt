
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roland Uškovitš
                                        &gt;
                Nezaradené
                     
                 uveriteľno - neuveriteľný príbeh o vlastnej prirodzenosti. 

        
            
                                    16.5.2010
            o
            17:47
                        |
            Karma článku:
                4.32
            |
            Prečítané 
            493-krát
                    
         
     
         
             

                 
                         Zapríčinila to moja nedeľná prechádzka, počas ktorej som si všimol zástup detí kráčajúc pekne v dvojrade z prvého sv. príjimania z kostola. Do oka mi padol chlapec v kaki obleku, ktorý medzi ostatnými ´´čiernoodencami´´ silne vyčnieval z davu. V tú chvíľu som si  uvedomil, že celé moje doterajšie životné správanie ovplyvnili už moji rodičia v detstve. Dosť živo som si v tej chvíli premietol ako mi triedna ziape do ksichtu, že si neželá aby v jej triede nosili chalani náušnicu, a následne si v hlave vychutnal pocit ako sa mohla aj pretrhnúť od zúrivosti, keď to prišiel riešiť do školy môj otec a mal v uchu presne takú istú ako ja. Doteraz si pamätám jeho slová, keď som mu ako stvrtáčik na základnej vysvetloval aké je teraz moderné nosiť náušnicu a nám to triedna nechce dovoliť. Jeho reakcia bola vtedy jasná: ´´Tak si ju dám aj ja a hotovo.´´
                 

                      Historky o prefarbení vlasov v piatej triede a mojich oblečkovských kreáciach počas celej základky sa premietali v mojej hlave ako živý film. A tieto spomienky ma utvrdili v myšlienke že aj keby som vynaložil maximálne úsilie  a bol by som sám odhodlaný chovať sa v rámci tejto spoločnosti tak ako by sa slušilo a patrilo, podľa určitých zásad... Moja prirodzenosť vždy vypláva na povrch.           O fakte že si frajerka počas mojej týždnovej neprítomnosti pripravila pre seba večer tzv. ´´smútiaci´´, v ktorý sa od samého smútku že opäť cestujem s kapelou po slovensku a zarábam nám na živobytie stihla vyspať so svojím bývalým, som vedel už od stredy, ale nerád riešim tieto veci, ako aj akékoľvek iné veci, po telefóne a domov som došiel až vo štvrtok v noci, a to som sa potreboval konečne po celom týždni vyspať. Začal som teda s riešením vzniknutej situácie až v piatok ráno, keď som jej po rannom čajíku oznámil nech si zbalí svoje dve tričečká a vypadne láskavo z môjho brlôžku. Po dobalení poslednej Tesco igelitky to ale na ňu trošička doľahlo a nakoniec sa teda priznala a ja počítajúc si v duchu dva zárezy na svojej ľavej paži počas nášho dvojmesčného spolunažívania som ju  víťazoslávne objal, dodávajúc si sily myšlienkou že predsa len stále o gól vyhrávam. Nasledovalo dvojhodinové ´´udobrovanie´´ po všetkých miestnostiach nášho skromného príbytku z ktorého ma vytrhol až telefonát nášho basáka upozorňujúci ma na desaťminútový interval ktorý mi mal umožniť sprchu, raňajky a pobalenie sa na piatkový koncert v Trenčíne. Stihol som teda aspoň tú sprchu. Silné objatie a bozk na rozlúčku mi dali evidenntne najavo že sila prežitých emócíí v to ráno so mnou ešte večer pekne pozametá ak priam nevymetie.        Napriek tomu, že som si počas hodinovej cesty do Trenčína v sebe reálne priznal že ranná scéna nebola len otázkou mojho jednogólového vedenia, ale že osobu ktorá ma bude v noci s teplou večerou opäť doma čakať skutočne ľúbim, som sile nahromadených emócií vo svojom vnútri už po príchode do klubu nedokázal odolať. Úvodné panáky s Teckerom, naším gitaristom sme stihli ešte pred zvukovkou, a keď nás klávesák tesne pred koncertom zavolal ešte na ´´šlúčika´´ začal som tušiť že...        Počas koncertu po mne pokukovala príjemná vysoká tmavovláska, a v mojom mozgu sa neváhalo oddeliť kúsoček miestočka ktorý popri kontrole mojich 4roch končatín stíhal premietať v mojej hlave scénu z filmu: Trenčín a tmavovovláska, s podtitulom: Po koncerte na šatni. Z udeľovania oskarov za réžiu ma vytrhol až spevákov prejav pred jedinou pomalou vecou nášho koncertu. Rečicky o tom ako sa máme všetci radi, na to sme boli už zvyknutí, ale slovné spojenie ´´Náš čerstvo zaľúbený bubeník´´ okamžite zatrhlo akýkoľvek honorár spomínaného filmu. Tmavovláskyn pohľad mi do konca koncertu ostal odoprený. Po vytriezvovacej polhodinke, ktorá spočívala v pobalení bubnov a ich následnom ponosení do dodávky ma druhá - lepšia polovica môjho JA začala príjemne oslovovať plánom stihnúť posledný rýchlik z Trenčína - smer Žilina, a splnením dopoludňajšieho sľubu spapkať doma pripravenú večeru a zaspinkať s hlavičkou-makovičkou mojej frajerky na hrudi. Posledná cik-pauza s rupsáčikom na chrbte vychádzam z vécka smer únikový východ... ´´A Ty si sa kam vybral bratu?!?! ´´ ...z príjemného pocitu že dnes budem Posluško ma vytrhli Teckerove slová. ´´Neblázni, posledný koncert šnúry a ty chceš tiež zmiznúť?´´ Vedel som že chalani z kapely už poslušne sedia v dodávke smer Bratislava, ale to že Tecker ostal v klube žúrovať, tak to som ani netušil. Bol by som býval opatrnejší, veď som bol naozaj už pevne rozhodnutý stihnúť ten posledný rýchlik. ´´Počúvaj Apa, zasedel som sa s kámoškou odtialto, poď ešte na pivec, spanie je zabezpečené. Asi čarovná kombinácia slov pivec a spanie a dve blondsky čo akurát vychádzali z dámskeho ma v zlomku sekundy presvedčili že z posleného koncertu turné sa naozaj domov nechodí posledným vlakom. Teckerova kámoška patrila do skupiny zaujimavá ale od začiatku nášho rozhovoru pri pivku by sa dala asi najlepšie charakterizovať slovným spojením: nevídane otvorená akýmkoľvek nápadom. Tecker ju poznal samozrejme už lepšie a keď mi pri bare objednávajúc dalšie hruškovice objasnil že túto slečinku ždžobneme dnes večer obaja naraz, som si bol istý že vie o čom hovorí a vyvodil si z toho patričné dôsledky. Tie boli priamo úmerné stavu hruškovicového zápasu Ja vs. Tecker, ktorý nado mnou dôsledkom toho že bol gitarista a predsa len pobaliť si po koncerte gitaru netrvá toľko ako pobaliť bicie, vyhrával 5:1. Boli tu dve možnosti. Buď sústrediť sily na vyrovnanie zápasu alebo zosnovať reálne riešenie ako sa vyhnúť tejto situácii. Po tom ako som takmer triezvo vyhodnotil dôsledky činu ktorý sme mohli v tú noc spáchať, a uvedomil si že obraz Teckerovej bielej riťi by ma prenasledoval na každom nasledujúcom koncerte minimálne mesiac som zvolil možnosť číslo dva. Jediným možným riešením ostával útek z klubu ala Jack Rozparovač a následný nočný stop domov. Keď som beznádejne stál na výpadovke D1ky, utešoval som sa aspoň tým, že predsa len na dnes vyhrala tá Posluškovská stránka môjho Ja. Po hodine, čo okolo mňa prefrčalo asi desať áut dostal však tohtovečerný čistý kúsok mojho svedomia ranu pod pás. Neprijaté hovory od Teckera nenechali na seba dlho čakať.        Keď som si v sobotu na obed kupoval v Hyprenove džúsik na suchoty a rožteky s treskou na žalúdoček ani vo sne by mi nenapadlo že aj snaha poslúchať sa cení. Ocenenie v rámci toho že som noc nakoniec, ani newiem ako, strávil sám v izbe aj v posteli, mi padlo vhod. Vytratil som sa z bytu hneď ako som sa zobudil. Ešte aj tá cesta rýchlikom domov sa nakoniec konala aj keď so 14 hodinovým oneskorením. Jediným mráčikom na modrej oblohe mojej pokojnej cesty domov ostávalo 5 neprijatých hovorov od frajerky. Rozhodnutie vyriešiť tento problém až silným objatím v dverách nášho hniezočka lásky sa však javilo ako najlepšia voľba. Po príchode do Rajeckých Teplíc som neodolal využitiu prestupnej stanice našej krčmy kde som na odvahu a lepšiu zrozumiteľosť mojho smútočného prejavu ktorý sa mal konať po príchode domov zadelil ešte nejaké pivká, hruškovice a šluky. Domov som dorazil po desiatej večer. Neskrývam že to bol zámer. Na žiadnu hlasnú výmenu názorov zo strany mojej polovičky som nemal náladu a nočný kľud predsa treba dodržiavať. Naša uzmierovacia debata sa skončila v krčme pri kofolke slovami: ´´Ty asi nemôžeš byť normálny, pre teba je prirodzené vyčnievať z davu...               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roland Uškovitš 
                                        
                                            Stopom z Paríža za 29 hodín.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Uškovitš 
                                        
                                            Mladé milenky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Uškovitš 
                                        
                                            More zážitkov na ceste slobodou...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Uškovitš 
                                        
                                            Nebeská Dominika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Uškovitš 
                                        
                                            Mám sa fajn.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roland Uškovitš
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roland Uškovitš
            
         
        rolanduskovits.blog.sme.sk (rss)
         
                                     
     
        ...jsem smutnej, malej, citlivej, nevdečnej Ježíš ve znamení Ryb. (Cobain)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    129
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    197
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Áno, nečudujem sa, že žijem
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Tatiana Melasová - Tovar
                                     
                                                                             
                                            Fernando Morais - Mág (životopis Paula Coelha)
                                     
                                                                             
                                            Simone Berteautová - Edith Piaf
                                     
                                                                             
                                            Charles Cross - Težší než nebe... (životopis Curta Cobaina)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Erykah Badu
                                     
                                                                             
                                            60 kíl
                                     
                                                                             
                                            M.Brezovský, O.Rózsa - Hrana
                                     
                                                                             
                                            John Mayer
                                     
                                                                             
                                            Pearl Jam
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tatiana Melasová
                                     
                                                                             
                                            Katarína Mrvečková
                                     
                                                                             
                                            Pavel Hirax Baričák
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nebeská Dominika
                     
                                                         
                       Mám sa fajn.
                     
                                                         
                       Mexická Princezná.
                     
                                                         
                       Predstavy
                     
                                                         
                       Pozri na mňa, moja láska.
                     
                                                         
                       Môj pocit, nazvime ho: život na Slovensku.
                     
                                                         
                       Život je krásny alebo o mojich potulkách po Európe.
                     
                                                         
                       Čierny havran
                     
                                                         
                       Jesenné prelúdium.
                     
                                                         
                       RÁNO
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




