
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Trnovec
                                        &gt;
                Nezaradené
                     
                 Nekrológ 

        
            
                                    24.4.2010
            o
            11:24
                        |
            Karma článku:
                2.66
            |
            Prečítané 
            1018-krát
                    
         
     
         
             

                 
                    Janusz Krupský, ktorý tragicky zomrel 10. apríla tohto roka pri leteckom nešťastí pri Katyni bol významnou osobnosťou demokratickej opozície v Poľsku
                 

                  &lt;!--  /* Font Definitions */  @font-face 	{font-family:"Cambria Math"; 	panose-1:2 4 5 3 5 4 6 3 2 4; 	mso-font-charset:1; 	mso-generic-font-family:roman; 	mso-font-format:other; 	mso-font-pitch:variable; 	mso-font-signature:0 0 0 0 0 0;} @font-face 	{font-family:Calibri; 	panose-1:2 15 5 2 2 2 4 3 2 4; 	mso-font-charset:238; 	mso-generic-font-family:swiss; 	mso-font-pitch:variable; 	mso-font-signature:-1610611985 1073750139 0 0 159 0;}  /* Style Definitions */  p.MsoNormal, li.MsoNormal, div.MsoNormal 	{mso-style-unhide:no; 	mso-style-qformat:yes; 	mso-style-parent:""; 	margin-top:0cm; 	margin-right:0cm; 	margin-bottom:10.0pt; 	margin-left:0cm; 	line-height:115%; 	mso-pagination:widow-orphan; 	font-size:11.0pt; 	font-family:"Calibri","sans-serif"; 	mso-ascii-font-family:Calibri; 	mso-ascii-theme-font:minor-latin; 	mso-fareast-font-family:Calibri; 	mso-fareast-theme-font:minor-latin; 	mso-hansi-font-family:Calibri; 	mso-hansi-theme-font:minor-latin; 	mso-bidi-font-family:"Times New Roman"; 	mso-bidi-theme-font:minor-bidi; 	mso-fareast-language:EN-US;} span.longtext 	{mso-style-name:long_text; 	mso-style-unhide:no;} .MsoChpDefault 	{mso-style-type:export-only; 	mso-default-props:yes; 	mso-ascii-font-family:Calibri; 	mso-ascii-theme-font:minor-latin; 	mso-fareast-font-family:Calibri; 	mso-fareast-theme-font:minor-latin; 	mso-hansi-font-family:Calibri; 	mso-hansi-theme-font:minor-latin; 	mso-bidi-font-family:"Times New Roman"; 	mso-bidi-theme-font:minor-bidi; 	mso-fareast-language:EN-US;} .MsoPapDefault 	{mso-style-type:export-only; 	margin-bottom:10.0pt; 	line-height:115%;} @page Section1 	{size:595.3pt 841.9pt; 	margin:70.85pt 70.85pt 70.85pt 70.85pt; 	mso-header-margin:35.4pt; 	mso-footer-margin:35.4pt; 	mso-paper-source:0;} div.Section1 	{page:Section1;} --&gt;        Klub mnohodetných rodín, združujúci mnohodetné rodiny a rodiny s postihnutým dieťaťom je zakladajúcim členom Európskej konfederácie mnohodetných rodín so sídlom v Španielsku. Našim partnerom v Poľsku je organizácia združujúcou mnohodetné rodiny so zodpovedným rodičovstvom „Trzy Plus", ktorú osobitným spôsobom zasiahla celonárodná tragédia.       Janusz Krupský, ktorý tragicky zomrel 10. apríla tohto roka pri leteckom nešťastí pri Katyni bol významnou osobnosťou demokratickej opozície v Poľsku, tvorca a vydavateľ undergroundového časopisu pre mladých katolíkov "Stretnutia", viceprezident IPN a najnovšie riaditeľom Úradu pre vojnových veteránov a perzekuovaných osôb. Vlastenec, nezlomný muž.   Narodil sa dňa 9. mája 1951 v Lubline, bol historikom, v 70. a 80 rokoch aktivista antikomunistickej opozície. Od roku 1976 členom Klubu katolíckej inteligencie vo Varšave, redaktor nezávislého časopisu "Stretnutia", členom Regionálnej rady Solidarity v Gdansku.   Počas stanného práva bol internovaný od 22. októbra 1982, v neskôr pod stálym dozorom a vystavený represiám zo strany SB (Štátnej bezpečnosti).   V rokoch 1990-1992 bol riaditeľom vydavateľstva Stretnutia, člen redakčnej rady týždenníka Stretnutia, v rokoch 1992-1993 expertom mimoriadnej parlamentnej komisie skúmajúcej vplyv stanného práva, 1993-1995 expertom Komisie pre ústavnú zodpovednosť.   V rokoch 2000 - 2006 pôsobil ako námestník predsedu Inštitútu národnej pamäti, 2006-2010 vedúci Úradu pre vojnových veteránov a perzekuovaných osôb.   Jeho manželka Joanna Puzyna-Krupska je predsedníčkou organizácie združujúcou mnohodetné rodiny v Poľsku „Trzy Plus". Osirelo sedem detí, Piotr, Paweł, Tomko, Łukasz, Jasia, Marysia i Tereska.   Pohreb Janusza Krupského, ktorý tragicky zahynul pri Smolensku, sa bude konať 26. apríla o 14:30 hodín. v kostole sv. Martina na Starom Meste vo Varšave. Pochovaný bude na vojenskom cintoríne vo Varšave.   Česť jeho pamiatke!  Klub mnohodetných rodín, združujúci mnohodetné rodiny a rodiny s postihnutým dieťaťom je zakladajúcim členom Európskej konfederácie mnohodetných rodín so sídlom v Španielsku. Našim partnerom v Poľsku je organizácia združujúcou mnohodetné rodiny so zodpovedným rodičovstvom „Trzy Plus”, ktorú osobitným spôsobom zasiahla celonárodná tragédia.       Janusz Krupský, ktorý tragicky zomrel 10. apríla tohto roka pri leteckom nešťastí pri Katyni bol významnou osobnosťou demokratickej opozície v Poľsku, tvorca a vydavateľ undergroundového časopisu pre mladých katolíkov "Stretnutia", viceprezident IPN a najnovšie riaditeľom Úradu pre vojnových veteránov a perzekuovaných osôb. Vlastenec, nezlomný muž.   Narodil sa dňa 9. mája 1951 v Lubline, bol historikom, v 70. a 80 rokoch aktivista antikomunistickej opozície. Od roku 1976 členom Klubu katolíckej inteligencie vo Varšave, redaktor nezávislého časopisu "Stretnutia", členom Regionálnej rady Solidarity v Gdansku.   Počas stanného práva bol internovaný od 22. októbra 1982, v neskôr pod stálym dozorom a vystavený represiám zo strany SB (Štátnej bezpečnosti).   V rokoch 1990-1992 bol riaditeľom vydavateľstva Stretnutia, člen redakčnej rady týždenníka Stretnutia, v rokoch 1992-1993 expertom mimoriadnej parlamentnej komisie skúmajúcej vplyv stanného práva, 1993-1995 expertom Komisie pre ústavnú zodpovednosť.   V rokoch 2000 - 2006 pôsobil ako námestník predsedu Inštitútu národnej pamäti, 2006-2010 vedúci Úradu pre vojnových veteránov a perzekuovaných osôb.   Jeho manželka Joanna Puzyna-Krupska je predsedníčkou organizácie združujúcou mnohodetné rodiny v Poľsku „Trzy Plus”. Osirelo sedem detí, Piotr, Paweł, Tomko, Łukasz, Jasia, Marysia i Tereska.    Pohreb Janusza Krupského, ktorý tragicky zahynul pri Smolensku, sa bude konať 26. apríla o 14:30 hodín. v kostole sv. Martina na Starom Meste vo Varšave. Pochovaný bude na vojenskom cintoríne vo Varšave.   Česť jeho pamiatke!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Je ešte Rakúsko katolícke?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Nepôjdem voliť! Prečo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Strata zdravého rozumu, alebo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Diera v železnej opone
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Trnovec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Trnovec
            
         
        trnovec.blog.sme.sk (rss)
         
                                     
     
         Som slobodný človek. Sloboda je okrem života najfantastickejší dar, ktorý som dostal. Denne stojím v nemom úžase pred zázrakom života, tohto tajomstva sa neviem nabažiť. Každý deň je DAR, za ktorý nestíham ďakovať. Predseda Klubu mnohodetných rodín, viceprezident FEFAF, člen administratívneho výboru COFACE. Teda písať budem o rodine. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    133
                
                
                    Celková karma
                    
                                                5.58
                    
                
                
                    Priemerná čítanosť
                    749
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Je ešte Rakúsko katolícke?
                     
                                                         
                       Nepôjdem voliť! Prečo?
                     
                                                         
                       Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                     
                                                         
                       Strata zdravého rozumu, alebo?
                     
                                                         
                       Ako trestáme korupciu: Za miliónové tendre podmienka, za 10 kíl ryže 5 rokov natvrdo
                     
                                                         
                       Zmena ústavy je dobrou správou, cena je však vysoká
                     
                                                         
                       EÚ a tradičné hodnoty
                     
                                                         
                       Zajtra hrozí sedem kresiel pre SMER a s ním aj útok Bruselu na posledné zvyšky suverenity
                     
                                                         
                       10 dôvodov, prečo má Slovensko nádej
                     
                                                         
                       Kto je chudobný?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




