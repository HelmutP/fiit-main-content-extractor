
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Karol Kaliský
                                        &gt;
                PRÍRODA
                     
                 Prvomájový pochod 

        
            
                                    4.5.2009
            o
            9:00
                        |
            Karma článku:
                17.07
            |
            Prečítané 
            7595-krát
                    
         
     
         
             

                 
                    Ako deti sme zvykli na 1. mája oslavovať sviatok práce. Nevyberali sme si, chodili sme s červenými pionierskymi šatkami v dlhočizných sprievodoch, mávali zástavkami a kričali rôzne duchaplné heslá. Neskôr, teda presne pred piatimi rokmi, sa pridružil aj ďalší dôvod na oslavu. Slovenská republika vstúpila 1. mája 2004 do Európskej únie. Po vstupe do EÚ sme sa zaviazali ochraňovať a zabezpečiť priaznivý stav európsky významných území ...
                 

                 Dnes si už našťastie môžme všetci vybrať, ako tento deň oslávime. Do prvomájových sprievodov už chodiť nemusíme, a tak sa môžeme rozhodnúť napríklad pre návštevu tých prírodných krás, ktoré sme zahrnuli aj do siete európsky chránených území.  Ja som sa tohtoročný 1. máj rozhodol osláviť v sprievode môjho bratranca pochodom na Veľký bok. Je to turisticky veľmi atraktívny, aj keď trochu nedocenený vrchol v Kráľovohoľskej časti Nízkych Tatier, vzdialený asi jeden a pol hodiny od hlavného hrebeňa. Výstup naň bol pre mňa vždy veľkým zážitkom, spojeným s pobytom v nádhernej prírode národného parku. V posledných rokoch som sa tam však nedostal, turistický chodník bol pre turistov zavretý. Dôvodom mali byť chemické postreky a ťažba v NAPANTe. Tabuľa, naháňajúca strach a zakazujúca vstup turistom, je tam síce ešte aj dnes, ale zákaz je len do 31. decembra 2008. Vstup je teda konečne voľný! Čo iné ako radosť a očakávanie môže človek pocítiť?  Po prvých sto metroch kráčania Malužinskou dolinou sme namiesto očakávanej zakvitnutej lúky zbadali obrovský sklad dreva, kde boli okrem smrekov traktormi naťahané jedle, buky, javory, smrekovce a osiky.    Upútali ma aj veľké kopy smrekového dreva zbaveného kôry, zjavne zo starej kalamity. Ak bol dôvodom na ťažbu týchto smrekov lykožrút, tak potom sa moji kolegovia lesníci oneskorili určite aspoň o dva roky. Pokračovali sme ďalej, no príjemné pocity vystriedala skľúčenosť, bývalé lesné zákutia vystriedali holé svahy s narýpanými približovacími cestami, pripomínajúce skôr kameňolom ako národný park. Celou dolinou nás sprevádzal intenzívny pach bahna, na ktoré sa zmenila kolesami traktorov zničená lesná pôda okolo potoka, cez ktorý sa z každej dolinky približovalo drevo.    Po niekoľkých kilometroch šľapania hore dolinou, obzerajúc si ťažké lesné stroje, vyhýbajúc sa terénnym autám a jednej štvorkolke, sme stretli pána lesníka. Išiel pešo, v jednej ruke niesol vidly, v druhej zhodený paroh z jeleňa, na tvári mal zachmúrený výraz. Vraj tam nemáme čo hľadať, dolina je zavretá! Stále je tu kalamita. Mali by sme vidieť čo sa tu robí cez pracovný týždeň! Dolina je vraj plná robotníkov, nákladné autá behajú hore dole.    Skromne a zahanbene sme pánovi lesníkovi povedali, že žiadny zákaz vstupu v doline nebol, a ak predsa, tak platil do konca minulého roka, a ubezpečili sme ho, že nepôjdeme mimo turistického chodníka. To že by sme tým poškodili prírodu národného parku už nebolo ani potrebné dodávať, z prísnych lesníkových očí sa to dalo krásne čítať.  Urobil som cestou ešte niekoľko záberov tej prírody, kam majú turisti z dôvodu ochrany prírody vstup zakázaný. Malužinská dolina pod Veľkým bokom, územie európskeho významu, územie národného parku, 1.mája 2009, teda presne päť rokov od vstupu do EÚ:       Bratranec sa ma opýtal, či to čo vidí, je valašská kolonizácia ...     Neviem, sám som lesník, ale mne to ako boj s kalamitou nepripadalo. Na kopách dreva bolo sprejmi nastriekané, ktorá firma ho vyťažila. Na veľkých vyťažených plochách bola na niektorých miestach po spádnici uhádzaná haluzina, zelené vetvy smrekov. Neviem o akú kalamitu sa jednalo, podľa vyrúbaných javorov a bukov s typickými zátinkami, ktoré hovorili o tom, že boli zrúbané nastojato, som vylúčil vetrovú kalamitu.  Ostávala teda ešte kalamita lykožrútová. Lykožrúty však osiku ani buky nenapádajú. Možno teda listnáče padli za obeť pri ťažbe napadnutých smrekov. To, čo však bolo pre mňa naozaj šokujúce, bolo zistenie, aké tá kalamita vykazuje tvary!    Priznám sa, že mne to pripadalo, akoby si niekto dolinu rozparceloval na ťažobné celky, ktorých hranicu tvoria umelé línie, siahajúce po hrebene alebo po najbližšiu cestu.         Keď sme cez túto hrôzu prešli a dostali sa na Veľký bok, boli sme znovu ohromení, no tentokrát krásou divokej prírody, kde sa ešte človek nedostal.          Čo dodať na záver? Možno len toľko, aby ste si šli spraviť názor sami, a pýtajte sa. Pýtajte sa prečo vy nemôžete ísť tam, kde bez problémov môžu harvestory, buldozéry, štvorkolky, terénne autá a nákladiaky. Do národného parku, do územia európskeho významu ... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Samozvaní ochranári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            10 rokov po ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            O čom sú (pre nás) Tatry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Neodrezávajte svojim miláčikom hlavy!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Vlčie hory
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Karol Kaliský
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Karol Kaliský
            
         
        kalisky.blog.sme.sk (rss)
         
                        VIP
                             
     
        lesník, trošku aj fotograf prírody (viac na www.wildlife.sk)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                12.48
                    
                
                
                    Priemerná čítanosť
                    4834
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            PRÍRODA
                        
                     
                                     
                        
                            FOTOGRAFIE
                        
                     
                                     
                        
                            NEZARADENÉ
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            V čase krízy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pokorný, Bárta: Něco překrásného se končí
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janci Topercer
                                     
                                                                             
                                            Juro Lukáč
                                     
                                                                             
                                            Mišo Wiezik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.arollafilm.com
                                     
                                                                             
                                            www.wildlife.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                     
                                                         
                       Vlčie hory
                     
                                                         
                       130 vlkov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




