




 
 
  
 
 
 

	 
		 
			
			 
 
 




 
 
  



 Spravodajstvo 
 
 Spr?vy 
 Dom?ce 
 Zahrani?n? 
 Ekonomika 
 Videospravodajstvo 
 Reality 
 Kult?ra 
 Koment?re 
 Shooty 
 
 ?port 
 
 ?port 
 V?sledky 
 
 Slu?by 
 
 Blog 
 Vybrali SME 
 Predpove? po?asia 
 TV Program 
 Teraz 
 English news - Spectator 
 Magyar h?rek - UjSz? 
 


 Tematick? weby 
 
 Porad?a 
 AUTO 
 TECH 
 TV SME 
 ?ena 
 Prim?r (Zdravie) 
 R?movia 
 Vysok? ?koly 
 Cestovanie 
 Dom?cnos? 
 
 Pr?lohy a sekcie denn?ka SME 
 
 Rozhovory 
 TV OKO 
 V?kend 
 



 
Spr?vy
 

 
  



  Vybra? svoj regi?n  
 Z?pad 
 
 Bratislava 
 Levice 
 Nov? Z?mky 
 Nitra 
 Pezinok 
 Senec 
 Topo??any 
 Tren??n 
 Trnava 
 Z?horie 
 
 V?chod 
 
 Korz?r 
 Humenn? 
 Ko?ice 
 Michalovce 
 Poprad 
 Pre?ov 
 Star? ?ubov?a 
 Spi? 
 


 Stred 
 
 Bansk? Bystrica 
 Kysuce 
 Liptov 
 Novohrad 
 Orava 
 Pova?sk? Bystrica 
 Prievidza 
 Turiec 
 Zvolen 
 ?iar 
 ?ilina 
 



 
Regi?ny
 

 
  



 Praktick? slu?by 
 
 Druh? pilier 
 TV Program 
 Artmama 
 Deti 
 Dvaja.sme.sk 
 Fotky 
 Gul? 
 Horoskop 
 Inzer?ty 
 Jazykov? porad?a 
 Nani?mama 
 Napizzu.sk (don?ka pizze) 
 Pr?ca 
 Re?taur?cie 
 Torty od mamy 
 Tla?ov? spr?vy 
 ?ahaj 
 Zlat? fond (aj e-knihy) 
 


 Odborn? recenzie elektroniky 
 
 mobily 
 telev?zory 
 tablety 
 notebooky 
 fotoapar?ty 
 hern? konzoly 
 
 Tret? sektor 
 
 Cena ?t?tu 
 ?akujeme 
 Obce 
 Otvoren? s?dy 
 ?koly 
 Tender 
 Vysok? ?koly 
 



 
Slu?by
 

 
  



 
 Last minute dovolenka 
 Don?ka jedla 
 Reality 
 Z?avy 
 Autobaz?r 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 SME knihy a DVD 
 SME predplatn? 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 



 
Nakupujte
 

 

    

  
 
			 
 
	
	 V?etko zo sveta vedy a techniky 
 
	 











	 
 

 DOMOV 
 Internet 
 Mobil 
 Testy a recenzie 
 Hardv?r 
 Hry 
  
 Vesm?r 
 ?lovek 
 Biol?gia 
  
 Fotogal?rie 
  
 TAHAJ.SME.SK 
 RECENZIE 


 
 SOFTV?R 
 HandyRecovery - obnov? vymazan? s?bory 
 
			 
				 
					 



 Vydan? 12. 5. 2004 o 0:00 Autor: MILAN GIGEL
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 Aj ke? s?bor v  po??ta?i vyma?ete z ko?a, e?te st?le existuje n?dej, ?e sa bude da? obnovi?, najm? ak od jeho vymazania uplynul len kr?tky ?as. 
 Aj ke? s?bor v po??ta?i vyma?ete z ko?a, e?te st?le existuje n?dej, ?e sa bude da? obnovi?, najm? ak od jeho vymazania uplynul len kr?tky ?as. 
 V z?sade plat? pravidlo, ?e ka?d? s?bor je po vymazan? obnovite?n?. Priestor, ktor? obsadzoval na pevnom disku, v?ak nesmie by? prep?san? in?mi ?dajmi. Znamen? to, ?e ak zist?te, ?e ste omylom vymazali nie?o, ?o ste nemali, mus?te okam?ite zastavi? pr?cu na po??ta?i, nesp???a? ?iadne programy a na pevn? disk ni? nenahr?va?. Aj jednoduch? spustenie programu m??e sp?sobi? vytvorenie do?asn?ch s?borov na pevnom disku ?i zv??enie ve?kosti swapu, ?o m??e vymazan? d?ta absol?tne zni?i?. 
 Katastrofe m??ete zabr?ni? rozdelen?m pevn?ho disku na samostatn? oddiely (part?cie), ke? jeden vyhrad?te len pre va?e d?ta a na druhom bude ulo?en? opera?n? syst?m a programy. Naopak, absol?tnou pohromou pre obnovovanie d?t je defragment?cia (upratanie) pevn?ho disku. 
 Na jednoduch? obnovovanie d?t dobre posl??i aj softv?rov? bal?k HandyRecovery vhodn? aj pre t?ch, ktor? s po??ta?mi ve?a sk?senost? nemaj?. Podporuje s?borov? syst?my FAT12, FAT16, FAT32, NTFS a NTFS5, ?o pokr?va cel? ?k?lu opera?n?ch syst?mov pou??van?ch syst?mami Microsoft Windows. 
 Po spusten? treba zvoli? diskov? jednotku, z ktorej maj? by? d?ta obnoven?. Po za?at? anal?zy softv?r prezrie obsah pevn?ho disku a v okne priebe?ne vyzna?? adres?rov? ?trukt?ru vr?tane s?borov, po ktor?ch zostali na disku ?i diskete inform?cie. Vyh?ad?vanie s?borov zaberie nejak? ?as, tak?e na ?pln? zoznam si treba chv??u po?ka?. Ak ?elan? s?bor v adres?rovej ?trukt?re n?jdete, neznamen? to v?dy, ?e je obnovite?n?, m??ete sa v?ak o obnovu aspo? pok?si?. 
 
 
 

 Po zvolen? s?boru alebo cel?ho adres?ra sta?? klikn?? prav?m tla?idlom na jeho n?zov a zvoli? obnovu. Pr?slu?n? d?ta v?ak treba obnovi? na in? diskov? jednotku, na akej sa nach?dzaj? - teda na in? oddiel disku alebo na disketu. Pri obnove na to ist? miesto by sa mohlo sta?, ?e bud? prep?san? in? obnovite?n? s?bory. Ak je obnoven? dokument nepou?ite?n?, netreba ve?a? hlavu. Mnoh? kancel?rske bal?ky a softv?ry si pri pr?ci so s?bormi vytv?raj? ich do?asn? k?pie v adres?ri c:\windows\temp, dobr? je preto poh?ada? aj v ?om. 
 Na z?ver v?ak treba doda? to najd?le?itej?ie - ani ten najinteligentnej?? sp?sob obnovy d?t nedok?e zast?pi? kvalitn? z?lohovanie. 
 (www.handyrecovery.com , 550 kb) 
   
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
  
 
 
S?visiace ?l?nky: 
 
 
 + Ako obnovi? digit?lne fotografie z pam?ovej karty?
 
 
 + Pred?vate star? po??ta? alebo disk? Pozor na d?ta!
 
 
 
 
 
 
  
 
 
 
 

 
 
 
 

 
 
 
 
Top:

24 hod?n
3 dni
7 dn?

 
 
 
 
 
 
 
 
1. 
 ??na buduje nov? ve?k? m?r, chce zabrzdi? p??? 
21?349
 
 
 
2. 
 Uk?zali prv? farebn? sn?mku kom?ty, vyzer? ako uhlie 
15?001
 
 
 
3. 
 
Inteligentn? kufor sa nikdy nestrat?, vyzbieral mili?n dol?rov
 
9?363
 
 
 
4. 
 Zabudnite na dopravn? spr?vy, policajn? auto odhal? mal? ?katu?ka 
5?984
 
 
 
5. 
 Vypnut?ho The Pirate Bay sa ujal konkuren?n? server 
5?744
 
 
 
6. 
 ?ivot funguje aj kilometre pod zemou, bez slnka a kysl?ka 
5?668
 
 
 
7. 
 Slov?ci uk?zali z?klady novej lie?by srdca 
4?663
 
 
 
 
 
 
 
 
 
1. 
 ??na buduje nov? ve?k? m?r, chce zabrzdi? p??? 
40?461
 
 
 
2. 
 
?lovek m??e na Marse zomrie?, let u? za?ali pripravova?
 
33?001
 
 
 
3. 
 Vedci vytvorili nov? druh ?adu, poznala ho len te?ria 
30?052
 
 
 
4. 
 Uk?zali prv? farebn? sn?mku kom?ty, vyzer? ako uhlie 
15?001
 
 
 
5. 
 Google s?ahuje z Ruska v?voj?rov 
14?541
 
 
 
6. 
 P?vod oce?nov zost?va z?hadou, voda nemus? by? z kom?t 
10?038
 
 
 
7. 
 
Inteligentn? kufor sa nikdy nestrat?, vyzbieral mili?n dol?rov
 
9?363
 
 
 
 
 
 
 
 
 
1. 
 ??na buduje nov? ve?k? m?r, chce zabrzdi? p??? 
40?461
 
 
 
2. 
 
?lovek m??e na Marse zomrie?, let u? za?ali pripravova?
 
33?001
 
 
 
3. 
 Vedci vytvorili nov? druh ?adu, poznala ho len te?ria 
30?052
 
 
 
4. 
 ??na uk?zala najten?? smartf?n na svete 
29?722
 
 
 
5. 
 Slov?k prerazil v najlep?om vedeckom magaz?ne, s evol?ciou lebky v Nature 
28?846
 
 
 
6. 
 ?esi dosiahli prevratn? objav, uk?zali dom?ci test na rakovinu prostaty 
27?106
 
 
 
7. 
 Mlad? Slov?k sk??a prerazi? so sol?rnym n?ramkom, zaujme svet? 
26?502
 
 
 
 
 
 
 
 

 
 
 
 
  
 
 
 
 
  
 
 
 
 
 
 



 

 
 
	 
		 
 
 ?al?ie novinky 
 


 
 

 
 
Cukor je pre na?e srdce hor?? ako so?
 
 Cukor zohr?va ?lohu pri vysokom krvnom tlaku a in?ch srdcov?ch chorob?ch. 
 
 
 
 
 




 
 

 
 ?ivot funguje aj kilometre pod zemou, bez slnka a kysl?ka 
 Mikr?by dok?zali pre?i? viac ako dva kilometre pod dnom mora 
 
 
 
 
 


 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 21:30  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v utorok st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
KOMENT?R PETRA SCHUTZA  
Len korup?n? ?kand?ly preferencie Smeru nepotopia
 
 Ak chce opoz?cia vo vo?b?ch zvrhn?? Smer, bez roz??renia reperto?ru si neporad?. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 23:50  
Taliban zabil v ?kole v Pakistane 132 det?
 
 Pakistan a? teraz poriadne zatla?il na islamistov. Odplatou je ?tok na deti vojakov. 
 
 
 
 
 




 
 

 
ZAHRANI?IE  
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi
 
 Na demon?tr?cii proti Orb?novej vl?de bolo v utorok u? len nieko?ko tis?c ?ud?. 
 
 
 
 
 


 
 
 
 
     
         
                             
                    
                        Don?ka
                    
                 
                             
                    
                        Dovolenka
                    
                 
                             
                    
                        Reality
                    
                 
                             
                    
                        Recenzie
                    
                 
                             
                    
                        Re?taur?cie
                    
                 
                             
                    
                        Z?avy
                    
                 
                     
     
     
        
         
         
                                                 
                         
                            
                                 

                                    
                                 
                            
                            Apple MacBook Pro 13 Retina Display (2014)

                             smeSk?re: 95 bodov 
                         
                     
                                     
                         
                            
                                 

                                    
                                 
                            
                            Apple MacBook Pro 13 Retina Display (2013)

                             smeSk?re: 91 bodov 
                         
                     
                                     
         
             Najlep?ie notebooky pod?a odborn?ch hodnoten? 
             
                                     
                        Ultrabooky
                     
                                     
                        Mini notebooky
                     
                                     
                        Hern? notebooky
                     
                                     
                        Z?kladn? notebooky
                     
                             
                            
                     
     
     
 

 



					 
				 
 
	 
	 
 



 



 
 
   
				   
			 
		
		 
		

 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	   
 
 



 

	
	 
Redakcia Kontakty Nap?te n?m Etick? k?dex Pomoc Mapa str?nky
 
	 
		
		
		
		
		
		
		
		
		
		  
	 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

		
	 
 
	


  


 



 

 

 


 











 
 
 
 
 






