

 "Som zvedavá, čo na to povieš." 
 "Nevídalí," hovorím si samozrejme v mysli. "Šalátovka." 
 Ale tá chuť. Tá bola predsa len iná. Sviežejšia, temperamentnejšia, s jemnulinkou trpčinkou a pritom delikátna. Veľmi mi to chutilo. No a keď sa mame v posledných rokoch odchcelo pobehovať po lúke pri našej záhradke a začala si šťaveľ pestovať sama, už mi bolo jasné, že šťaveľový prívarok bude niekedy aj na blogu. Tá chvíľa prišla práve teraz. 
 Potrebujeme: 
 Na 5-6 porcii šťaveľového prívarku aspoň 0,5 kg čerstvého záhradného, alebo lúčneho šťaveľu. Šťaveľ zbierame ďaleko od ciest na lúkach, nie že si pozbierate ten z okolia baráku, ošťatý od domácich miláčikov a plný olova.:) 
 Ďalej potrebujeme 2 dcl mlieka, 1 smotanu na varenie, 4-5 lyžíc hladkej múky (môže byť aj kukuričná či pohánková, bezlepkáči...), 3 strúčiky cesnaku, celé či mleté čierne aj nové korenie, bobkový list, soľ a olej, alebo maslo a lyžicu cukru. Ničím iným nie je potrebné šťaveľový prívarok, či polievku dochucovať. Sú výborné také úplne jednoducho pripravené. 
  
 Čo s tým urobíme? 
 Šťaveľ dobre poumývame. Môžeme ho  dokonca zaliať vodou a nechať odmočiť cez noc. Následne z každého lístočka odstránime tvrdú stopku a šťaveľ pokrájame na menšie kúsky. Cesnak očistíme a nasekáme, alebo pretlačíme. Nakoniec tej roboty ani nebolo toľko veľa. 
 Do vhodného hrnca nalejeme nie príliš veľké množstvo vody, len na prekrytie šťaveľa, pridáme nové aj čierne korenie, bobkáč, cesnak, kopcovitú lyžicu cukru a soľ. Necháme zovrieť a varíme asi 5 minút. 
  
 Kým sa šťaveľ varí, urobíme si bešamel. Na oleji (masle) speníme múku, prilejeme mlieko, premiešame, prehrejeme a odstavíme. 
 Do vzniknutej hustej kaše vylejeme smotanu na varenie. Dobre vyšľaháme, aby neostali hrudky. 
  
 Šťaveľ sa krásne uvaril, 
  
 takže neostáva iné, iba vliať bešamel so smotanou, dobre premiešať, nechať zovrieť a povariť, občas premiešajúc aspoň 10 minút. 
 Prívarok následne zriedime či naopak múkou rozmiešanou vo vode zahustíme podľa potreby. Je málo kyslý? Pridáme trochu octu. Je naopak príliš kyslý? Nevídali, vezmeme pol kávovej lyžičky bikarbonátu sodného, nasypeme do prívarku a premiešame. Zašumí, spení sa a kyslosť stiahne. A je hotovo. 
  
 Ortodoxní mäsožravci! 
 Vezmite nás na milosť a na začiatku varenia si v hrnci urestujte na malé kocky nakrájanú slaninu. Do nej potom pridajte šťaveľ, vodu, koreniny a cesnak. Pokračujte ako v recepte... 
 Na rad prichádza vaječná sadlina. A aby ste neprišli skrátka, urobíme ju dvomi spôsobmi. 
 Potrebujeme: 
 Na každé vajce okolo 3/4 dcl mlieka, zarovnanú lyžicu múky, trochu soli, prípadne čierneho korenia a tuk na potretie nádoby. 
  
 Vajíčka, mlieko, múku, soľ a štipku korenia dobre vyšľaháme. 
  
 Najprv si ukážeme sadlinu pečenú vo vodnom kúpeli v rúre. Srnčí chrbát je na to ako stvorený. Vaječnú zmes vlejeme do dobre maslom či olejom vytretej nádoby, ktorú je vhodné vysypať múkou, tak do 2/3 jej výšky. 
  
 Pekáč vložíme do väčšej vhodnej nádoby s vodou a následne do rúry predhriatej na 200 stupňov. 
  
 Po 15-20 minútach vytiahneme a sadlinu necháme vychladnúť. Spadla vám? Nech si leží. Dobrá bude aj tak. Vyberieme ju lopatkou z pekáča a nakrájame na plátky. 
 Deti, sadlina má okrem použitia na prívarky všestranné použitie. Jej príprava je jednoduchá ako facka, je veľmi chutná a efekt je exkluzívny. Primiešať do nej môžeme všetko od výmyslu sveta, napadajú ma napríklad dusené šampiňóny na cibuľke, kúsky šunky, hrášok, kukurica, pažítka, iné vňate a tak ďalej. Takáto sadlina, nakrájaná na plátky bude chutnou ozdobou akejkoľvek studenej misy či šalátu. Proste veľa muziky za pár šupov... 
  
 Tak a máme tu ďalší spôsob. Tento krát sa hodí skôr okrúhla keramická nádoba. Dobre ju vymastíme a ak sa chceme vyvarovať prilepeniu ku dnu nádoby, tak ho môžeme vystlať krúžkom papiera na pečenie. Naň potom nalejeme vaječnú zmes, 
  
 nádobu vložíme do inej väčšej nádoby s vriacou vodou, 
  
 prikryjeme a varíme 10-15 minút. 
  
 Necháme vychladnúť, nožom sadlinu uvoľníme od stien nádoby, 
  
 vyklopíme a krúžok papiera na pečenie odstránime. 
  
 Nakrájame na takéto pekné plátky. 
 Ďalšie použitie sadliny je v jej pokrájaní na malé kocky a tie sú vynikajúce napríklad v mäsovom či zeleninovom vývare. Čo vy na to? 
  
 Konečne tu máme našu prezentáciu. 
  
 Dobrú chuť. 
  
   
 tenjeho 
   

