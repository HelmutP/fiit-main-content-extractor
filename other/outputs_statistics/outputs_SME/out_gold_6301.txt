

  Na nohách čierne topánky na ktorých nebadať prach veľkomiest, sú vyleštené v súlade so špeciálnou príležitosťou. Inak pôsobia celkom bežne. Šedozelené nohavice vyžehlené na puky, niečo na štýl elegantných ,,kapsáčov“ s čiernym pásom a výraznými vreckami na boku má ušité na mieru. Dvojmo opásaný tenkým špagátom čiastočne pripomína budhistického mnícha. Z praktických príčin takto nahradil opasok, ktorému nečakane odpadla pracka. Nezostávalo nič iné, len sa vynájsť. Kravata s vyrezanými pásikmi, vraj vyrobená zo značkovej gumy Matador. Okuliare s čiernym rámom vytŕčajú z vrecka hrubého saka. Karol Pichler pricestoval z Rio de Janera na otvorenie svojej prvej monografickej výstavy do Nitrianskej galérie.  
   
 V súvislosti s Pichlerovým menom sa často spomína slovenská postmoderna a neokonceptuálne tendencie. Inými slovami, vo svojej tvorbe rád používa ready- made predmety, textové hry, znaky a symboly. Prostredníctvom symbolov a znakov odhaľuje nepovšimnuté veci každodenných dní, poukazuje napríklad aj na zákazy, ktoré neustále zapĺňajú náš verejný priestor. Od zákazu fotografovania môže byť len na krok k zákazu nečinného postávania, usmievania sa, či hľadania kontextov. ,,Zákaz vstupuje do vašej slobody, nech má akýkoľvek level. Či už je väčší alebo menší, vždy vás určitým spôsobom usmerňuje. Snaží sa ľudí pretvoriť na modul či model.“ Pichler nerozlišuje veľkosť zákazov, práve naopak, vo všetkých vidí zhodné posolstvo usmerňovania a obmedzenia osobnej slobody.  
 Nomádske výskumy 
 Kvôli červeným zákazom odišiel v osemdesiatych rokoch študovať do susednej Budapešti, v tých časoch nazývanou aj ,,Kultúrnou Mekkou východného bloku.“ Po prvom nomádskom kroku nasledovali ročné štipendiá v Taliansku a vo  Švajčiarsku, pobyt v Indonézii a vo Francúzsku. Brazíliu však nepovažuje za svoje posledné stanovisko. Túži ešte navštíviť Argentínu a Japonsko. Cestovanie Pichlera napĺňa pocitom slobody a núti ho k neustálej konfrontácii s novým svetom a prostredím. Napokon cesta- symbol slobody sa vyskytuje aj v jeho inštaláciách. Na podklade Wittgensteinovej filozofie ohraničuje vlastné bytie prostredníctvom jazykových znalostí. Ako sám tvrdí: ,,Už prvými slovami, s ktorými človek dokáže komunikovať, sa otvárajú nové možnosti rastu.“  
 Kurátorka Barbora Geržová špecifikuje Karola Pichlera ako autora vracajúceho sa k určitým témam a motívom. Napríklad textové práce zaraďuje skôr k rannej tvorbe, avšak on sám tvrdí, že jazyk je dominantným problémom v jeho živote, čo sa neustále odráža aj v dielach.  
 Slovné hry a ich interaktívne návody však zostali v rámci vernisáži skôr nepovšimnuté. Odstup slovenského pozorovateľa môže byť spôsobený neprekonaním rešpektu samotného diela, alebo jednoducho v nezáujme stať sa účastníkom Pichlerovej hry.  
 Manipulácia moci s jedincom 
 Obsedantnosť jazykom v Pichlerovej osobnej histórii predchádzala téma vzťahu jednotlivca a moci. Túto tematiku môžeme implicitne nasledovať v niekoľkých abstraktných polohách. Príkladom môže ísť ďalekohľad navádzajúci na voyeristické pohľady do protiľahlého okna, no ukrývajúci klamlivé univerzum kaleidoskopu, či igelit pripevnený na stenu sieťou pripínačiek. Pripínačky vytvárajú súmernú sieť, do ktorej je násilím vtesnaný prekypujúci objem igelitu. Ide o paralelu násilného vtláčania formy. ,,V tejto práci ma znervózňuje, že sa nezohnali biele pripínačky, tie sú predsalen rafinovanejšie, nenápadnejšie.“ Biela farba by na podklade steny zanikla a význam nenápadnej manipulácie spoločnosti jedincom by sa mohol priblížiť k dokonalosti.  
   
 Najnovšia Pichlerova videoinštalácia predstavuje trochu inú formu karaoke. Na plátne je premietaná dokonale gýčová exotická krajina, doladená lahodnou oddychovou hudbou. Zobrazovaný text piesne sú však originálne state Michela Foucaulta popisujúce štruktúry moci. ,,Symboly na jednej strane čakajú na dešifrovanie, na strane druhej dávajú návody blízke ľuďom.“  
 Vystavený symbol bielej svastiky vznikol v jednom geometrickom rade z logického krájania a otvárania štvorca. Naša blízka minulosť spôsobila, že tento symbol považujeme za veľmi negatívny znak. Pichler od diváka očakáva, že ho dokáže prečítať nanovo. Navyše netreba zabúdať na kontext, background, v ktorom je symbol zobrazený. ,,Niekedy sa hodnoty symbolu nedokážu prečistiť. To je však častokrát otázka východy a kultúrneho zázemia.“ 
   
 Spoluvystavujúci 
 Výstava predstavuje aj dvoch domácich autorov mladšej generácie Erika Bindera a Stana Masára. S Karolom Pichlerom zdieľajú neokonceptuálne stratégie, hravosť a motív piktogramov. Na záver ešte treba dodať, že výstava je koncipovaná hravo a interaktívne a v neposlednom rade dojem z Binderových a Masárových diel fantasticky dopĺňa celkový dojem.  
  
 Traja králi Erika Bindera 
  
 Stano Masár: Monetove piktogramové lekná.  
 Trvanie výstavy: 6.5.2010- 4.7.2010 
 Miesto: Nitrianska galéria  
   

