
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Gaborčík
                                        &gt;
                -
                     
                 Modlitba imbecilov.../ !?/ 

        
            
                                    8.1.2010
            o
            21:58
                        (upravené
                13.1.2010
                o
                18:25)
                        |
            Karma článku:
                14.67
            |
            Prečítané 
            2882-krát
                    
         
     
         
             

                 
                    Panebože viem, že je to asi „ťažký hriech“ ísť v tejto neistej dobe proti prúdu. A znova ako kedysi.... nepočúvať hlas „spasiteľa“ ľudu. Asi som už starý, hluchý nemoderný - „imbecil.“ Snáď ináč myslím, vidím a počujem, alebo už v právnom štáte nežijem? Len tak, bez opýtania strčiť výbušninu do batožiny nič netušiaceho občana! To sme Číňania? Čo ja viem? Veď to nevie ani kamarát: dement – senior, či infantilný junior.
                 

                 Mám taký šteklivý pocit v žalúdku... ale svedomie mám čisté. Nevolil som ich!“ Úspešných „babrákov“, zlodejov a darebákov. Vládu troch „pijavíc" som nezostavil Ja! Ale mi vládnu, odtiaľ tá moja dezilúzia. Ponovembrová schizofrénia. Vidím „červených arogantných apačov.“ Sú infiltrovaní všade, ešte aj v tzv. sociálnej vláde.  Protagonisti boľševického „raja“ postupne všetko rozkradli a úspešne to taja. Je mi na zvracanie. Vymenili ministrov a bakšiš im ostane. Jasné, nikomu sa nič nestane... Rozum si nekúpiš, no svedomie áno! „Prajem“ vám, ospalí demokrati, súdružské dobré ráno. Vy ste sa z ničoho nepoučili! V akej karme ste doteraz žili? Pýtajú sa „imbecili.“ Začínate už ľudí poriadne „srať“, neviete byť jednotní a na niečom rozumnom sa dojednať. Analýzu svojho pádu beriete ako „vlastizradu“. Sebareflexia aj odvaha sú v keli. Takto ste v politike dozreli ?  Populistovi sa pri vás dobre vodí, ale občanovi to tak či tak len škodí. Kradne, tára, oblbuje. Prečo tento národ mu to toleruje ? Načúva väčšine, preto dobre vie, čo tento národ počuť chce. Náš plebs to nevidí, ešte mu drukuje, veď je presvedčivý. S chudákmi - „hlupákmi“ vie urobiť „divy.“ Demagóg - rozpráva, presviedča a argumentuje, ako pre ten národ nespí a pracuje. Ľudia milujú rozprávky, veria v zázraky. On v percentá. A to je tá zvláštna symbióza nevzdelanca a docenta. Je kladný „hrdina“ - myslí si oklamaná väčšina. Talentovaný manipulátor, raz možno skončí ako “úspešný diktátor.“  Slušným, pracovitým, čestným, tichým, mlčiacim, veriacim aj neveriacim - „imbecilom" nezostáva asi nič iné ako čakať na zázrak. Volebný zázrak r. 2010. Ten bordel sme dovolili a pre zmenu pomerov dodnes nič podstatne nespravili. Ostáva nám asi iba tá modlitba:    „Modlitba imbecilov :“   Pane daj, aby tento národ nebol:    - hluchý, slepý a sprostý, aby mu „tupovoliči“ nedolámali krehké kosti - vedel povedať dosť , klamárom a kadejakým príštipkárom - vytriezvel z rečí populistov a odolával ich sladkým drístom - mal svoju hrdosť a sebavedomie, vedel sa postarať o seba a zarobiť si poctivo na chleba  Pane daj:   - aby sa už ľud prestal báť tých, čo tu otravujú a púšťajú teplý smrad  - aby človek vedel robiť rozumné kompromisy a dal sa najesť aj chudobnému z plnej misy - aby človek vedel, čo je morálka a česť a kedy je už "profit" hriech - aby zlodej a podvodník si nenašiel ku korytu chodník - aby súdy ozaj súdili a o spravodlivosti nie iba blúznili - aby už konečne boli vo funkciách vzdelaní a slušní ľudia a nie tí, čo podvodmi zarábajú a ešte sa aj súdia - aby už konečne skončili tí, ktorí nás prenasledovali a trýznili  /súdruhovia, eštebáci a všetci do červena sfarbení rodáci/ - aby sa opozícia konečne zobudila a nie v bezradnosti hnila - aby táto vládna „perestrojka“ skončila a nie bobky ďalej trúsila - aby sme my nezávideli a nekradli a na duši a cti nechradli - aby nás aspoň vo voľbách „osvietilo“ a bez tých podvodníkov sa nám lepšie žilo       Ďakujeme Ti Pane a dúfame, že pomôžeš nám splniť naše želanie. Títo vládni páni spadli do žumpy morálneho úpadku, no stačili výdatne pohnojiť svoju ružovú záhradku. Sú tým smradom viac, či menej nasiaknutí a to sa nám na nich hnusí. O morálnej a hodnotovej kríze spoločnosti, ako takej, sa dnes čuší, ale sa nemusí! Táto kríza stále je a dlho trvá, mimochodom vznikla ako prvá. Je tu taká skrytá hrozba - ak raz padnú všetky možné zábrany, proletári budú štrngať francúzskymi kľúčami. A pohrozia legálnymi štrajkami.  Pane, nech sa splní vôľa Tvoja, ale prosím zabráň vláde „červeného vola". Nedopusť už policajnú republiku Chavezovho typu. Róm tu bude časom 1:1 robiť na dôchodky: „Vypijeme horký kalich pravej ruskej vodky ?“   P.F. 2010 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Súčasná Matica slovenská-pýcha,či hanba národa?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Prezident nemôže perliť v parlamente.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Aj nás desí zloba p.prezident Ivan "hrozný"!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Mlčanie "ružových" jahniatok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Kto utrie soplíky Ficovi?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Gaborčík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Gaborčík
            
         
        gaborcik.blog.sme.sk (rss)
         
                                     
     
        Spišiak - nie celkom "slepý". Súkromný vetrinárny lekár so záľubou komentovať politické dianie formou článkov resp. básní. Iné záujmy - šport, história,varhany, šachy, príroda.
Obľúbená pieseň: Goraľu, czi či ne žaľ. Pod úbočou...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1814
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Kačani
                                     
                                                                             
                                            Hubeňová
                                     
                                                                             
                                            Jurzyca
                                     
                                                                             
                                            cerveň
                                     
                                                                             
                                            Marec
                                     
                                                                             
                                            Hlina
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




