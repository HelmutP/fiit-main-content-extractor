
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Krajmerová
                                        &gt;
                Súkromné
                     
                 Pritlač ma k múru 

        
            
                                    24.6.2010
            o
            19:22
                        (upravené
                24.6.2010
                o
                19:36)
                        |
            Karma článku:
                7.44
            |
            Prečítané 
            670-krát
                    
         
     
         
             

                 
                    Zamyslene som kráčala od lekára k svojmu autu - keď vtom ku mne prileteli intímne slová roztraseného hlásku: „Pritlač ma k múru, Jožinko môj, veď mám dnes tú slávnu osemdesiatku!" Rýchlo som odfúkla skleslé myšlienky - a rozhliadla sa okolo seba...  
                 

                 
 internet
         Neďaleko staručkej fiatky som zbadala baloniakovo rozkošnú dvojicu. Vetchý deduško sa poobzeral a keď nikoho nezbadal,  vytiahol odkiaľsi  maličké vrecúško - a potom jemne pritisol k dlaždicovému múru garáže svoju krehkú fešandu, ktorá vyzerala úplne ako herecká diva Kačenka Hepburnovie na sklonku svojho života. Jožinko jej zamával vrecúškom okolo tváre a s úsmevom riekol: „Moja zlatá, celú jar som zbieral v našej predzáhradke tvoje obľúbené fialky a nechal ti ich usušiť. To z lásky Božka moja, ozaj z lásky..."       V poslednom čase mi netreba príliš veľa, aby som sa rozcitlivela. O sekundu som začala v prítmí nevábnej garáže pregĺgať slzičky, lebo starenka milo švitorila svojmu princovi, že jej musí dať každú hodinu pusinku a že mu uvarila jeho obľúbený segedín. Rozprávkový deduško jej cinkol  za minútu azda i päť cmučiek a potom jej dvorne otvoril starú fiatku a pomohol do auta...   Ešte som začula chichotavé: „Moja zlatá panička, na čo ty len nemyslíš! Neboj sa, všetko bude, len aby sme to prežili,"  prehodil žartom.        A odfrčali - zrejme do svojho kráľovstva so segedínom, ale najmä plného nežného porozumenia. Spomenula som si, že som ich zbadala pri vedľajšej ambulancii a elegantne upravená Boženka v bielej blúzičke, na golieriku ktorej mala naháčkovaný staromódny lem, zvierala v ruke krabičku s ampulkou na injekciu. Pokiaľ bola vnútri, Jožinko dosť hlasno rozprával susedovi, že tá jeho dostáva raz do mesiaca včeličku, aby sa toľko od tej nešťastnej Parkinsonky netriasla - a vraj jej to pomáha...        Viac som sa nedozvedela, lebo si ma zavolala lekárka, aby mi oznámila, že v jednom uchu mi bude hučať či už s liekmi, alebo bez nich - a pritom sa na mňa ani raz nepozrela, ani ma nikde na vyšetrenie neposlala. Tak som jej oznámila, že  už k nej neprídem a znova pôjdem radšej na akupunktúru. Povýšenecky mi odfrkla: „Len si bežte k šarlatánom!"         A potom som zažila nával emócií dvoch ľudí, ktorí dokázali pri sebe stáť v dobrom i zlom veľa-veľa rokov. Čo už, toho môjho som nikdy neprinútila ani len na čiastočnú dávku staromódnej romantiky Boženky a Jožinka. Tak trošku som im aj závidela, ale na druhej strane im želám, aby im toto krehké šťastíčko vydržalo dlho-predlho...               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Voniaš ako...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Týrané Slovenky, nedajte sa !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Spod plesovej krinolíny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Prírodný detox ľudského organizmu zvládne každý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Rok čínskeho draka má optimistický charakter
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Krajmerová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Krajmerová
            
         
        krajmerova.blog.sme.sk (rss)
         
                                     
     
        Autorka v júli 2012 zomrela. Blog nechávam na jej pamiatku. Dcéra Katka
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    235
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2101
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voniaš ako...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




