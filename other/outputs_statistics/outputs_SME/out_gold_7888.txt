

 Ale... 
 Milá naša príroda. 
 Ja si nemôžem pomôcť. Aj tak by som Ťa chcel veľmi, veľmi, veľmi pekne prosiť, aby si už zastavila ten veľký dážď a nezatápala viac našich ľudí. Prosím za nich preto, lebo som presvedčený, že najviac problémov majú práve tí najbezmocnejší. Tí, ktorí nemajú prostriedky na to, aby sa vedeli proti povodniam účinne chrániť. Tí, ktorí Ti možno škodili najmenej. 
 Za vyslyšanie mojej bezočivej, trúfalej prosby vopred ďakujem.   
   
   
   

