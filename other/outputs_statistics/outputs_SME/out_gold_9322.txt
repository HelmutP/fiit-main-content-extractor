

 Zoberme si to pekne od začiatku. Vzniká nová politická strana. Akú taktiku treba zvoliť, aby získala viac ako 5%, čiže aby prekonala hranicu zvoliteľnosti do parlamentu. Aký marketingový ťah je ten najsprávnejší pre zisk hlasov? Na „trhu" sa vyskytuje veľké množstvo voličov, ktoré sa rozdeľuje napríklad aj na tzv. prvovoličov, kresťansky orientovaných, ľavicovo orientovaných, takisto protipól pravicovo orientovaných, pronárodne orientovaných a naopak menšinovo orientovaných a dajme tomu starších voličov. Vychádza nám z toho veľký galimatiáš a človeka môže rozbolieť hlava z toho ako správne taktizovať, aby bola nová politická strana rešpektovaná a istým spôsobom aby získala svoje miestečko medzi už inými povedzme stálymi stranami na Slovensku. Najviac ohrozeným druhom sú práve starší voliči, ktorí už zažili kadečo... Chceme ich vidieť ako tých „skúsenejších" voličov. Avšak presne tento typ voliča na Slovensku je deprimovaný, nové politické strany nevníma a ani nemá záujem sa dozvedieť, čo vlastne taká nová strana chce priniesť. Je to asi tým, že táto skupina voličov už dávno neverí sľubom politikov a radšej bude voliť staré „osvedčené" značky alebo pre istotu nebude voliť vôbec. Protipólom sú prvovoliči alebo mladšie generácie, ktoré sú optimisticky naladené a skôr ich zaujíma budúcnosť ako minulosť. Nie je lepšie sa orientovať na tento typ voliča? Myslím si, že to nebude stáť toľko námahy a prisviedčacích psychologických manipulácií na dovedenie strany do zdarného „koncozačiatku". Na tento marketingový ťah stavila strana Sloboda a solidarita a vyplatilo sa. Cez internet prehovorili k mladým ľuďom, zvolili témy, ktoré sa týkajú a zaujímajú mladých ľudí (dekriminalizácia marihuany), témy, ktoré sa týkajú určitej vybranej skupiny ľudí (registrované partnerstvá) a mnoho ďalších iných nápadov a oslovení. Práve marketingový ťah, podľa ktorého fungovala volebná kampaň SaS bol úspešný. Dokonca podľa štatistických údajov bola volebná kampaň novej ambicióznej politickej strany tá najsprávnejšie zvolená, keď sa zameriame na všetky nové strany (od roku 1994) a ich počet získaných hlasov v prvých voľbách (u mnohých aj jediných). 
 Nové politické strany, ktoré sa dostali do NR SR: 




 Voľby 


 Politické strany 


 Počet získaných hlasov 


 Podiel platných hlasov v % 




 1998 


 Strana občianskeho porozumenia 
 SOP 


 269 343 


 8,01% 




 2002 


 Aliancia nového občana 
 ANO 


 230 309 


 8.01% 




 2010 


 Sloboda a solidarita 
 SaS 


 307 287 


 12,14% 




 Poznámka: Beriem do úvahy voľby do NR SR od založenia samostatnej Slovenskej republiky, čiže prvé voľby sa konali v roku 1994. Vo voľbách v roku 1994 a 2006 sa do NR SR nedostali žiadne nové politické strany. 
 V tabuľke môžeme vidieť tie politické strany, ktoré boli na politickej scéne nováčikmi a vo voľbách prekročili percento zvoliteľnosti a dostali sa tak do parlamentu. Strana Sloboda a solidarita získala najväčší podiel platných hlasov v percentách a predbehla tak aj „stálice" na politickej scéne.Nechcem sa venovať komparácii týchto politických strán, ale chcem poukázať na niektoré zo sľubov, ktoré ponúkala práve Sloboda a solidarita. Ako som už v úvode spomínala, okolo mňa sa začali po voľbách šíriť vety „Oklamali mladých voličov"..."Nikdy im nesplnia čo hlásali pred voľbami" „ Keď vytvoria koalíciu aj so stranou KDH, tak im tieto sľuby určite neprejdú". V zásade ide o dva body v programe a to: dekriminalizácia marihuany a registrované partnerstvá. Ja osobne si myslím, že pán Sulík a jeho strana nikoho z voličov neoklamal. Zvolili si témy, ktoré zaujímajú mladých ľudí a istú skupinu ľudí. Nikto im to nemôže mať za zlé. Marketing je marketing. Avšak každá strana má svoj volebný program, za ktorým si stojí a samozrejme chce z neho splniť maximum. Tak aj Sloboda a solidarita sa zasadila o naplnenie volebného programu. Z politologického hľadiska je pre stranu veľmi dôležitý počet získaných hlasov a teda percentá, vďaka ktorým môže následne rozhodovať prípadne zostavovať budúcu vládu. Ak získa menšie percento, znamená to, že sa musí viacej prispôsobovať a následne aj menej bodov splní so svojho volebného programu. Ak získa viac hlasov, môže si dovoliť viac rozhodovať a do vládneho programu tak putuje viac z ich volebného programu. Potom samozrejme záleží od spájania sa politických strán do koalície, na samotnej ich dohode a vytvorení vládneho programu. Čím viac politických strán, tým sa zmenšuje počet možných splnených bodov zo svojho daného volebného programu. A presne toto sa stalo aj strane Sloboda a solidarita a aj iným politickým stranám. Lenže skoro každý si zapamätal len dekriminalizáciu marihuany a registrované partnerstvá. Nikto nerozoberá, čo nesplní SDKÚ, KDH, Most - Híd, ktorý z bodov v ich programe vypadne... 
 Na záver dodávam, že nikto nikoho neoklamal, len záleží do budúcej dohody vládnej koalície a zostavení vládneho programu. Ak by tam mali byť celé programy jednotlivých koaličných vlád, tak budúca vláda môže vládnuť aj dvadsať rokov.... 
   
   
   

