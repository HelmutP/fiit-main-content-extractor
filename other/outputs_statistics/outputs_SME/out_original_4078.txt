
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Vyskoč
                                        &gt;
                Ochrana súkromia
                     
                 Dve správy pre ochranu súkromia na Slovensku 

        
            
                                    7.4.2010
            o
            12:17
                        (upravené
                7.4.2010
                o
                11:03)
                        |
            Karma článku:
                7.52
            |
            Prečítané 
            2140-krát
                    
         
     
         
             

                 
                    Prvá správa (dobrá): konečne sa u nás našla politická strana, ktorá do svojho volebného programu zahrnula podporu ochrane súkromia. Žiaľ, druhá správa je menej potešiteľná –
                 

                znenie príslušného bodu programu síce začína pekne – „zachováme ochranu súkromia“, ale zvyšok vety sa odkláňa od bežného chápania problému ochrany súkromia v počítačoch resp. informačných systémoch. Totiž deklarovať, že zachováme ochranu súkromia osobných počítačov v zmysle hesla „Môj počítač, môj hrad“ znamená v podstate to, že na svojom počítači si budem môcť uchovávať čo len chcem (s výnimkou niečoho nelegálneho, samozrejme). To som si myslel aj doteraz. Prečo potom taká formulácia?    Napadajú ma len dva dôvody. Možno autori volebného programu (či aspoň tej formulácie) majú informácie o organizáciach či jednotlivcoch, ktorí systematicky a v značnom rozsahu prenikajú do osobných počítačov občanov Slovenska („hackujú“ ich) len preto, aby zistili čo sa tam nachádza. Tak toto by naozaj bola zlá správa a zmienená strana by si zaslúžila uznanie, že nás chce pred niečím takým chrániť.    Druhá možnosť je, že autori textu nepochopili, že ochrana osobných údajov nie je o tom, čo mám vo svojom počítači, ale o tom, kde všade (mimo kontroly dotknutých osôb) a na aké účely sa spracúvajú osobné údaje občanov, a kto všetko má k nim prístup. Hmmm, takúto interpretáciu – teda že politici majú chaos v chápaní ochrany súkromia/osobných údajov – tiež ťažko označiť za dobrú správu. :-(   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Vyskoč 
                                        
                                            Pripravuje sa eGovernment – budú mu ľudia dôverovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Vyskoč 
                                        
                                            Estónci sa dočkali prvého spochybnenia internetového hlasovania
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Vyskoč 
                                        
                                            Poučné pripomienkové konanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Vyskoč 
                                        
                                            Mágia dňa ochrany osobných údajov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Vyskoč 
                                        
                                            Deň ochrany osobných údajov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Vyskoč
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Vyskoč
            
         
        vyskoc.blog.sme.sk (rss)
         
                                     
     
        Živí sa poradenstvom v oblasti bezpečnosti informačných systémov. Pravidelný prispievateľ do patavedeckých seminárov v Bratislave. Prednáša (informačnú bezpečnosť, čo iné) na Fakulte managementu UK a na Fakulte informatiky PEVŠ v Bratislave. Ďalšie informácie o ňom možno nájsť na www.vaf.sk/tim.htm.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    166
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2978
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Bezpečnosť
                        
                     
                                     
                        
                            Ochrana súkromia
                        
                     
                                     
                        
                            Pataveda
                        
                     
                                     
                        
                            Vzdelávanie a výskum
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




