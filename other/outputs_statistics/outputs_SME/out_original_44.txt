
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daša Alexyová
                                        &gt;
                Súkromné
                     
                 Rozprávkova svadba v rozprávkový deň 

        
            
                                    14.7.2007
            o
            9:56
                        |
            Karma článku:
                10.25
            |
            Prečítané 
            17689-krát
                    
         
     
         
             

                 
                    Aby sme ako v rozprávke žili spolu až kým neumrieme sme si naplánovali rozprávkovú svadbu a nakoniec nám to dokonca vyšlo aj v rozprávkový deň - nevesta bola ako z rozprávky (teda ja hehe), ženích ešte krajší, svadobčanov prišlo veľa presne ako bolo naplánované a tiež boli všetci ako z rozprávky - jedli, pili a zabávali sa až do rána bieleho, počasie bolo úžasné, kostol bol ako z rozprávky, Oravský Háj kde sa konala svadba bol su..su..super a kapela hrala viac než úžasne. Ale keďže aj v rozprávke bývajú aj zlé a neprajné osoby a nie vždy všetko dobre dopadne, tak aj my sme na svadbe mali pár zádrheľov, hlavne nášho svadobného agenta, ktorý to až tak veľmi nezvládal ale hlavne že ako v rozpávke všetko nakoniec dobre dopadlo a my sme šťastní už týždňoví manželia.
                 

                 
  
    Nášho svadobného agenta sme najali hlavne preto, že žijeme v Singapure a naozaj sme nemali čas si našu svadbu organizovať. Nanešťastie sme si vyberali podľa web stránok, keďže sme osobne žiadneho nepoznali a nanešťastie sme si opäť raz dobre nevybrali. Zaplatili sme síce rozprávkovo ale služby až také rozprávkové neboli. Dúfam len, že si to naši svadobčania neuvedomovali, iba my. Paradoxne teda, svadobný agent Vám má stresy na svadbe eliminovať, ten náš, nám ich pridával. Napriek tomu si myslím, že mať agenta je skvelý nápad, len by sme si do budúcnosti asi vybrali menšiu firmu, pre ktorú by naša zákazka bola jedna z väčších a tak by sa nám snáď naozaj kráľovsky venovali. Naša "kráľovská cateringová spoločnosť", ktorú sme si najali síce varila skvele, ale chýbali jej veľmi základné organizačné, koordinačné a komunikačné schopnosti. Ak by sme nezorganizovali v živote stovku podobných akcií, tak by sme si to možno nevšimli, v každom prípade si treba pri výbere dávať väčší pozor - skvelá prezentácia, super webka, referencie (neoverené) a vysoké ceny nie sú zárukou dobrých služieb (čo sme teda mohli vedieť, žiaľ sme až príliš dôverovali a málo overovali).     Našu svadbu sme teda začali pripravovať už v novembri minulého roka, kedy už veľa sitov bolo na naše cieľové dátumy obsadených. Keďže sme ale chceli svadbu v stane a niekde vo voľnej prírode (aby sme mohli byť divokí a netrápiť sa obmedzeniami hotelov), tak sme si nakoniec vybrali Oravský Háj a Bojnice - obe miesta vyzerali rozprávkovo a nakoniec sme sa tiež dosť paradoxne rozhodli pre Oravský Háj, pretože agent naviazaný na tento site vyzeral mať viac skúsenosti a lepšie odpovede na naše otázky :o).     Pravidelne sa nás všetci pýtali, prečo sme sa rozhodli mať svadbu na Orave, keď sme obaja z Popradu (dokonca aj pán farár) a ja som už na to vymyslela kopec odpovedí typu - aby sme nahnevali všetkých a aj rodina z východu a aj kamaráti z Bratislavy to mali na svadbu ďaleko. V skutočnosti sme sa nad tým s Petrom nikdy nezamýšľali, vedeli sme aké miesto hľadáme a kedže sme také ani v Poprade ani v Tatrách nenašli, neobmedzovali sme sa a hľadali sme po Slovensku. Tiež sme to chceli mať ďalej od rodiny a domova, aby sa rodina príliš neangažovala do organizácie svadby, aby to bolo všetko len a len podľa našich predstáv a aby sa eliminoval stres a hádky v rodine. To sa aj super podarilo - svadba bola až na pár detailov aj pre naše rodiny úplným prekvapením a bez veľkých stresov. Stresy spôsobila len farba šiat, kabeliek a topánok :o) a diéta pred svadbou.            Pred svadbou som si povedala, že sa do toho tiež sama miešať veľmi nebudem a nechám všetko na agentovi, keď ho už tak platíme. Tiež som nechcela byť vystresovanou nevestou a hlavne som v sebe chcela potlačiť pud všetko organizovať, keďže som maximálne organizačný typ. Keď sa nás ale agent ani v marci nepýtal na detaily a predstavy o svadbe, začala som byť trochu nervózna a začala som sa pýtať ja. Asi som dobre urobila, lebo agent by sa ma veru do svadby nič okrem toho o ktorej je kostol a koľko bude ľudí na svadbe a ich mená, asi nič iné neopýtal. Asi by sme teda nakoniec ostali bez chladiaceho boxu a koláče by sa nám roztopili, vázy na kvety by sme nikde nezohnali, kostol by ostal neozdobený, naši hostia by nemali čo piť a pár ďalších zaujímavosti. Nakoniec sme teda museli do svadby zatiahnúť aj nášho svadobného agenta - Peťovu sestru, pre ktorú to síce bola prvá svadba, ktorú organizovala, ale myslela na viac detailov, ako náš agent a spolu s ďalšími ľuďmi z našich rodín svadbu zachránila. Srandovne, Peter pred svadbou stresoval viac ako ja a dokonca obidve  naše rodiny dostali podrobnú inštruktáž deň pred svadbou - minútu po minúte, aby vedeli, kde stoja, čo hovoria atď. Bolo to veľmi milé a nakoniec aj veľmi potrebné.     Najväčší problém s našim agentom bol, že nám všetko možné nasľuboval a nakoniec na to buď zabudol, nikomu nič nepovedal, alebo si to vysvetlil a zrealizoval dosť po svojom. Nuž nevedel, s kým má dočinenia. Keď nám po svadbe hovoril, že to bola klasická svadba a žiadne stresy by neboli, keby Peťova a moja sestra nestresovali, skoro ma porazilo. Nasľuboval nám svadbu na mieru podľa našich predstáv ale nakoniec realizoval "klasickú" svadbu, kde je úplne bežné, že podávanie večere trvá 2 hodiny, len preto, že on to volá banket. Ak by nám to povedal, keď sa nám predával, len veľmi ťažko by sme si ho vybrali. Ale dosť o agentovi, ktorého nik nepočúval a nerešpektoval. Našťastie sme to dosť skoro zistili a nahradili jeho našimi komunikačnými osobami a všetko dobre dopadlo.      Site - Oravský Háj - bol naozaj skvelý na svadbu v stane, žiaľ penzión nemá kapacitu na catering nad 50 osôb a preto je nevyhnutné mať vlastnú cateringovú spoločnosť. Na Háji bolo podľa nášho agenta ževraj veľa komunikačných problémov s manažmentom, ktorý mu robil napriek a všetko čo sľúbili, urobili presne naopak alebo neurobil. Nevieme, čo je pravda, podľa nás, ho tam len nemajú v láske a tak sa tak k nemu správajú. Určite by sme miesto odporučili, len si dajte pozor, aby všetko odsúhlasili všetci ľudia z manažmentu Oravského Hája (teda asi 8 šéfov).             Kostol - Brezovica - kostolík bol skvelý a prekrásny, naozaj sme si veľmi dobre vybrali. Trošku nám to tam skomplikoval fakt, že práve vymenili farára a tak nás nakoniec sobášil niekto iný, ako ten s ktorým sme sa na všetkom dohodli a nový pán farár o mnohých veciach nevedel, ale aj tak sa nám obrad páčil, Peter dostal dišpenz od biskupa v deň svadby a tak všetko nakoniec dobre dopadlo a vzali sme sa tak, že sa už nebudeme môcť rozísť :o)             Cateringová spoločnosť - xxx xxxx Catering s.r.o. - za catering sme zaplatili 1500 SKK na hlavu a mali sme jedla habadej - naozaj až strašne veľa. Jedlo bolo super, všetkým chutilo, teda aspoň myslím, lebo ja som hlavné jedlo zjedla od hladu tak rýchlo, že ani neviem ako chutilo a viac som už jesť nestíhala. Najlepšia bola odozva na pečené prasiatko na polnoc - po tom sa len tak zaprášilo a potom ešte na netradičnú ovocnú misu s čokoládovým fondu. Kombinácia hlavná večera vo forme banket, potom teplý a studený bufet a nakoniec pečené prasa na polnoc bol dobrý nápad, len bolo veľa veľa jedla.     Stan bol dodaný spoločnosťou z Trenčína a výzdoba bola pripravená agentom. Všetko super.                      Koláče a torty - všetko bolo pečené v Námestove pánom Gerekom (na odporúčanie kamarátov) a ako sme sa neskôr dozvedeli je najlepší na Orave a naozaj jeho torty aj koláče boli skvelé. Pán Gerek aj jeho dcéra boli veľmi milí a ochotní, všetko ako má byť. Len sme mali príliš veľa toriet a koláčov. Za koláče sme nakoniec zaplatili viac ako za alkohol, čo je na nás dosť absurdné :o) a tak odporúčame naozaj max. koláče nevyhnutné na výslužky (15 - 30ks a 1/4 torty na výslužku, rodine dávali naši asi 50 koláčov a tortu ale to bolo na celú rodinu nie na človeka alebo na pár ako my) a max. 5 koláčov na hlavu na oslavu so svadobnou tortou.            Alkohol - na ten sme tiež mali šťastie, dlho sme rozmýšľali koľko, aby nebolo ani veľa a ani málo a nakoniec nám p. Kormanová z veľkoskladu v Trstenej, dala alkohol na dodací list, aby sme všetko navyše mohli vrátiť, čo sme veľmi ocenili, ale nakoniec sme nič nevracali, všetko sa do bodky vypilo :o). Plánovanie alkoholu (tvrdého je naozaj ťažké) - my sme plánovali špeciálne každý jeden stôl hostí na svadbe, podľa toho čo sme vedeli, že radi pijú a tak sme nakoniec na 95 hostí (aj s hudobnou skupinou a fotografmi) mali 9 x fernet citrus, 9 x vodka absolut, 6 x borovička, 6 x spišská slifka, 3 x gin beafeater, 3 x original brandy, 1 x malibu, 1 x baileys. Okrem toho sme ako svadobný dar od Stanky z Trebišova dostali 25 listrov tokajského vína (všetkým super chutilo), ďalší dar bolo biele a červené víno z firmy Vinkova od kámošov z Hlohovca (na výslužky 40 litrov a 20 litrov na pitie počas svadby) a ešte sme si kúpili sud piva (Krušovice 10) z Námestova - až na pár fliaš vína, padlo všetko a bolo nám veselo :o). Ja som pila nakoniec jedno cez druhé, čo bolo po ruke a vôbec som sa neopila, ako sa niektorí pred svadbou obávali :o).            Hudobná skupina - Dobrovar zo Zvolena - najlepšie rozhodnutie, aké sme mohli urobiť. Bolo to na odporúčanie kamarátky, čo žije tu v Singapure a ktorej brat robí manažéra Dobrovaru - inak neznámej skupinky, na ktorú by sme sa teda inak nedostali. Hudba bol náš najväčší problém. Peter je muzikant a nemá rád, keď niekto hrá a spieva zle a k tomu má aj špecifickú chuť na hudbu - tympádom, žiadne svadobné duo s elektrickým klavírom a hroznými basami nemohlo byť a ani žiadna cimbalka, lebo to Peter nemá rád. Vypočuli sme si pred svadbou milión mp3jek slovenských skupín a dopadlo to dosť zle. Nakoniec to vyzeralo tak, že aj Oravská cigánska skupina bude lepšia ako tie čo sme počuli, prípadne len DJ...na naše naozaj veľké šťastie sme natrafili na Dobrovar (ktorý sme pred svadbou nepočuli hrať), len sme verili odporúčeniu a nezmýlili sme sa. Hrali skvele až do rána, odchádzali naozaj poslední a to všetko za velice rozumnú cenu 10,000 SKK plus doprava na Oravu. Všetkým odporúčame - hrali to, čo sa ľuďom páčilo cez Olympik, slovenskú klasiku, Beatles, no proste super zábava pre rôzne vekové skupiny. Na svadbe sme mali aj DJa, nakoniec zbytočne, ale aspoň vypĺňal prestávky skupiny.                  Oblečenie nevesty a ženícha - všetko na mieru ušité v Singapure (aj moje topánky :o)) - maximálna spokojnosť. Moje šaty boli presne podľa mojich predstáv - žiadna klasika s ťažkou sukňou a metrovou vlečkou - chcela som niečo jednoduché, v čom budem môcť lietať sem a tam a veľa tancovať a čo na sebe nebudem ani cítiť a presne to som dostala (za cenu podobnú prenájmu svadobných šiat v Bratislave). Peter bol tiež veľmi spokojný, hoci si len málokto všimol jeho oblečenie, podľa mňa bol fešák :o), mal vestu aj kravatu z takej látky ako ja šaty a všetko mu super padlo. Mňa na začiatku trápil a stresoval len môj závoj, ktorý som si nevedela pripnúť, ani moje družičky, ale nakoniec som sa na to vykašľala.             Make up a vlasy - opäť na odporúčanie (pána z diskusného fóra mesta Trstená) a super - Janka a Ivonka z Trstenej. Na to, že som mala prvú skúšku účesu a make upu len v piatok pred svadbou, dopadlo všetko viac než super. Ja a moje 3 družičky sme sa sa krášlili v Jankinom kaderníckom salóne každá skoro dve hodiny v sobotu ráno, zatiaľ čo Peter a ostatní sa kúpali v bazéne a vírivke na Oravskom Háji. Make up som chcela veľmi ľahký a prirodzený a vlasy tiež čo najjednoduchšie a s výsledkom som bola spokojná. Vlasy vydržali veľa náročných kôl tancovania a stále držali formu, takže niet čo dodať. Odporúčame.            Fotograf a DVD - fotografov sme mali spústu, fotil náš Willie zo Singapuru, ktorého sme si najali v Singapure a poznali sme ho, lebo už pre nás fotil a s výsledkami (skoro 1500 fotiek) sme strašne spokojní. Okrem toho fotil pán fotograf od nášho agenta a natáčal p. Nakacka z Trstenej. Výsledky sme ešte nevideli, ale všetko prebehlo OK, tak hádam, aj naše fotky a DVD nebudú veľmi tradičné a gýčové ale veľmi netradičné a veselé. Počasie a okolie na fotenie boli fakt úžasné.           a tu je náš fotograf Willie v akcii s nevestou :o)            Doprava - do kostola aj z kostola sme išli na super koči s kôňmi. Trošku to natriasalo a našim hosťom sa za nami veľmi ísť pešo nechcelo, ale aj tak to bolo super a hlavne fotky stoja za to. Auto by sa do toho prostredia ani veľmi nehodilo, takže sme strašne šťastní, že nám počasie tak skvele vyšlo.             Svadobný program - žiaden nebol :o), nechceli sme veľa tradičných zvykov a obyčajov a ani hier, bolo len uvítanie s rozbíjaním pohárov a zametaním, Peter ma nosil na rukách cez most do stanu :o), krmili sme sa polievkou, otec a svedok mali príhovory a potom sa už len tancovalo, jedlo a pilo a na polnoc sme mali neplánovaný únos ženícha (vďaka našim super kamarátom) a ja som si manžela mala vykúpiť spevom (čo bol dosť trest, lebo spievať neviem) ale zvládla som to - spievala som Ani tak nebude, ani tak nesmie byť, aby tí Slováci nemali za čo piť (fakt neviem, prečo ma to napadlo) a potom som ešte spievala Macejka (to ma napadlo preto, že som to často "spievala" na Srí Lanke :o))...no a potom sme ešte o polnci krájali tortu, tancovali na pesničku čo som pre Peťa dala skomponovať chlapcom v base a sama do nej narozprávala slová (Volala sa TOGETHER FOREVER) a ešte som hádzala kyticu a potom sme tancovali redový tanec za peniažky. Neplánované boli aj moje tri únosy :o). Každý ma chcel unášať, tak som si to užívala. Najprv ma hlavný svedok poklepal po pleci, že poď von a odtiaľ sme išli do baru a dali si rundu, kým nás Peter našiel. O tomto únose snáď ani nikto nevedel :o) Druhýkrát ma chcel uniesť ujo počas redového tanca, ale prekazil mu to bratranec, ktorý mal naplánovaný oficiálny únos, ktorý sa teda uskutočnil asi o jednej ráno a pri ktorom som sa teda nautekala, lebo nosiť ma nechcel :o). Potom ma Peter vykúpil a mohla som sa ísť prezliesť a hlavne prezuť, lebo mi nohy z tanca už krvácali.          Kŕmenie sa ...          a mladá pani Bohmová s mamičkou (už prezlečené)            Dary - sme si vypýtali finančné a aj sme dostali, lebo nič iné by sme asi do Singapuru nezvládli odniesť. Okrem toho sme ešte dostali taký super netradičný dar - víno v puške, takej veľkej priesvitnej. A ostatné super dary - ako pre nás.       No a už len treba dodať, že sme tancovali do piatej rána a potom už len bola svadobná "noc" ako má byť a o desiatej sme už vstávali na raňajky. Trošku ma prekvapilo, že kamaráti išli spať trochu skoro na môj vkus, ale rodina sa bavila skvele až do rána (aj Stanka, Peťo a Dodo :o)).     Ráno sme sa spolu naraňajkovali a bolo po všetkom, rozdali sa výslužky a hostia odišli domov. My sme ešte ostali jednu noc, aby sme v pondelok vrátili prázdny sud od piva a prepravky z koláčov a išli sme sa domov vyspať :o). Sme strašne radi, že to máme za sebou a už sa tešíme na manželstvo.     Každý sa ma teraz pýta, či sa cítim inak, keď sme po toľkých rokoch zrazu manželia.  Pravdupovediac áno, tak zodpovednejšie :o) ale o tom neskôr, keď bude viac skúseností.           Všetkým, ktorí prišli na našu svadbu aj tým, ktorí sa pričinili k našej rozprávkovej svadbe ĎAKUJEME.                  p.s: Som zabudla napísať, že mi družičky zorganizovali aj super rozlúčku so slobodou v stredu pred svadbou :o) Po tej ma veru hlava viac bolela ako po svadbe - len striptér Ken chýbal, inak všetko bolo.     p.s2: Fotky zo svadby už čoskoro budú všetky na www.bohmovci.com                                                   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daša Alexyová 
                                        
                                            Napady napady same napady
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daša Alexyová 
                                        
                                            Together Forever
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daša Alexyová 
                                        
                                            Ludia su odrazom krajiny a krajina je odrazom ludi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daša Alexyová 
                                        
                                            Nase svadobne oznamko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daša Alexyová 
                                        
                                            Predsvadobna materska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daša Alexyová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daša Alexyová
            
         
        alexyova.blog.sme.sk (rss)
         
                                     
     
        Som Slovenka (rozumej národnosť a nie pohlavie), ktorá je hrdá na to, že je Slovenka a ktorá rada chodí kade tade po svete, aby mohla o Slovensku a Slovákoch ľuďom hovoriť.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    45
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Singapur
                        
                     
                                     
                        
                            Malajzia
                        
                     
                                     
                        
                            NextLogic
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Asia Business Services Pte Ltd
                                     
                                                                             
                                            O koučingu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Your Coach In a Book (Hargrove)
                                     
                                                                             
                                            TurboCoach (Brian Tracy)
                                     
                                                                             
                                            Eat the frog (Brian Tracy)
                                     
                                                                             
                                            A whole new mind (Daniel Pink)
                                     
                                                                             
                                            The road less travelled
                                     
                                                                             
                                            Why we want you to be rich
                                     
                                                                             
                                            The World is Flat
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Petrov NextLogic Blog
                                     
                                                                             
                                            In touch with Dasha
                                     
                                                                             
                                            Marián Minárik blog
                                     
                                                                             
                                            NextLogic story
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Coaching in Slovakia
                                     
                                                                             
                                            Asia Business Services
                                     
                                                                             
                                            SBS - Small Business Solution
                                     
                                                                             
                                            NextLogic
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




