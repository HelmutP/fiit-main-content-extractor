

 S príchodom jari sa najmä mužom začína trápenie. Vonku je síce ešte sneh, ale organizmus už je nastavený na jar. Začnú sa ozývať žalúdočné a dvanástnikové vredy. Je to choroba, ktorá si nevyberá, majú ju prezidenti, podnikatelia, speváci, učitelia, novinári či vodiči tirákov. Nedávno ničila prezidenta Jeľcina či Mariána Vargu. V priebehu jediného roka sa na Slovensku vredy objavia asi u stotisíc ľudí. Mnohí to však ani len netušia. Štatistiky hovoria, že na sto diagnostikovaných pacientov ďalších 50 nevie, že vredy má. Muži sú postihnutí štyrikrát častejšie ako ženy, ktoré chránia hormóny, tie zas majú vážnejšie komplikácie. 
 Prečo práve na jar? 
 Príčin je viac, nie všetky sú dostatočne vysvetlené, ale spúšťačom bolesti vredov môže byť napríklad aj to, že koncom zimy hltáme viac liekov proti bolesti a chrípke. Podľa čínskych lekárov silné energetické prebúdzanie prírody pôsobí aj na človeka, na jar aktívnejšie pracuje žlč a pečeň, ktorá filtruje potrebné látky a nepotrebné vylučuje z tela von. Z toho pramenia aj väčšie bolesti. Vredy si však vyrábame svojím životným štýlom. 
 Ľudia 21. storočia sú čoraz podráždenejší, náladovejší, prepracovanejší, ráno im nechutí, bolí ich brucho. K tomu dlhodobý stres, strachy a úzkosti. Ešte pred dvadsiatimi rokmi by to boli zreteľné príznaky nervového zrútenia, hovoria psychológovia. 
 Dnes na to nemáme čas. Žijeme pod obrovským tlakom, a tak tieto varovné signály ignorujeme a hltáme tabletky proti bolesti. Stres a analgetiká sú jedinečnou kombináciou, aby sme si vyrobili žalúdočné vredy. "Kto má predispozíciu, môže si vredy uhnať nadmerným užívaním analgetík už za niekoľko týždňov," hovorí internista Andrej Púchovský. 
 Prečo sa niekomu vytvorí vred a inému nie? 
 To je otázka, ktorú si kladú nielen tí, čo s ním majú trápenie, ale aj odborníci-gastroenterológovia. Aj keď sú známe hlavné riziká, nejasností je stále dosť. "V poslednom období sa za významný rizikový faktor považuje infekcia mikroorganizmom Helicobacter pylori. Dôležitú úlohu má však aj predispozícia, lebo 60 až 80 percent populácie u nás má vo svojom žalúdku mikroorganizmy Helicobactera pylori, no len asi desať percent dostane vredy," hovorí MUDr. A. Bátovský. 
 Ako spoznať, že máme vredy? 
 Pacienti s vredom dvanástnika mávajú bolesti v noci, nalačno. Bývajú často nočnými návštevníkmi chladničky. Už po pár hltoch potravy či dúškoch mlieka pociťujú úľavu. Tí, čo majú vredy na žalúdku, majú zase hneď po najedení bolesti, a tak sa zo strachu boja jesť, strácajú hmotnosť, chradnú. 
 Ak sa bolesť stane trvalou, prípadne vyžaruje do chrbta, je to signál, že môže ísť o komplikáciu a treba vyhľadať lekára. Hlboký vred môže poškodiť cievu a pacient začne krvácať, vred môže dokonca stenu tráviacej rúry aj prederaviť. Tí, čo vred majú, vedia, že je to veľmi bolestivá choroba, ktorá môže ohroziť dokonca aj život. Dobrá správa je: dá sa veľmi dobre liečiť. EVA HRDINOVÁ 
 Rizikové faktory vzniku žalúdočných vredov 
 
 mužské pohlavie (vyššia tvorba žalúdkovej kyseliny) 
 ľudia s krvnou skupinou 0 
 nadmerné pitie alkoholu, čierna káva nalačno 
 fajčenie 
 nadmerné užívanie niektorých liekov (acylpyríny, brufeny, analgetiká) 
 nepravidelná, studená alebo príliš horúca strava 
 
 Kedy vyhľadať lekára ?  
 
 ak tráviace ťažkosti pretrvávajú dlhšie ako 10 dní 
 ak je sťažené alebo bolestivé hltanie potravy alebo tekutiny, plus pálenie v pažeráku 
 ak je bolesť v nadbrušku plus pocit návratu kyslého, horkého obsahu do pažeráka 
 ak pretrváva bolesť brucha (kolikovitá alebo stála, denná či nočná bolesť) 
 pri napínaní na vracanie, vracaní (najmä ak je vo zvratkoch krv) 
 ak máte nadmernú plynatosť, pocit sťaženého trávenia po jedle či nalačno 
 viac ako 10 dní trvajúca zápcha alebo hnačka 
 akákoľvek nápadná zmena telesnej váhy, najmä nevysvetliteľné chudnutie 
 strata chuti do jedla, celková slabosť spojená s tráviacimi ťažkosťami plus zistenie akejkoľvek hrčky alebo na dotyk bolestivého miesta v bruchu 
 nevysvetliteľný nadmerný rast obvodu brucha spojený najmä s opuchmi členkov 
 
 Vyšetrenie tráviaceho traktu? Stačí zhltnúť kameru 
 Ešte pred dvadsiatimi rokmi to bola utópia, dnes skutočnosť, nie síce bežná ale dostupná. Aj na Slovensko sa už dostala úplne nová metóda na vyšetrenie tráviaceho traktu. Pacient zhltne kapsulu, veľkú ako bežná tabletka, v ktorej je zabudovaná miniatúrna kamera. Prehltnutá kamera prechádza tráviacou sústavou a pritom neustále monitoruje okolie, ktorého obraz sa prenáša von vďaka sondám priloženým na tele pacienta. Lekári môžu snímky vyhodnotiť po ôsmich hodinách po prehltnutí. Nová vyšetrovacia metóda je pre pacienta jednoduchá a úplne bezbolestná na rozdiel od doposiaľ zaužívaných vyšetrovacích sond, ktoré sa zavádzajú do žalúdka. Metóda bola od svojho zavedenia už zdokonalená, už je na svete kapsula druhej generácie, ale oproti klasicej endoskopii má podľa našich lekárov stále veľkú nevýhodu, nedá sa pri nej odobrať biopsia z tráviaceho traktu. 
 Pomáha na stanovenie diagnózy najmä pri problémoch s tenkým črevom. Kapsula napokon opustí telo prirodzenou cestou. Na Slovensku sa zatiaľ s pomocou českých kolegov vyšetrilo pokusne len zopár pacientov. 
 Táto malá kamera v podobe kapsuly je zatiaľ na jedinom pracovisku, v súkromnej gastroenterologickej ambulancii na bratislavských Kramároch. Zdravotná poisťovňa ho neprepláca, toto vyšetrenie stojí približne 20 tisíc. (uj) 
 Kapusta lieči chorý žalúdokUž starí Rimania považovali kapustu za účinný všeliek, lebo jej listy hoja žalúdočné vredy, uľahčujú trávenie, pôsobia proti bolesti zubov a priaznivo ovplyvňujú činnosť pečene. 
 Čerstvá hlávka má mimoriadne vysoký obsah vitamínu C - asi 45 miligramov na 100 gramov hmoty, v kvasenej kapuste sa jeho obsah znižuje na 50 percent. Vláknina, ktorá zasa rýchlo prechádza zažívacím traktom, spolu s účinnými olejmi kapustových listov je prírodnou prevenciou proti vzniku rakoviny čriev a navyše prirodzenou cestou odvádza jedy z tela. 
 Muži, ktorí jedia kapustu aspoň raz týždenne, majú len 30-percentné riziko vzniku rakoviny v porovnaní s mužmi, ktorí jedia kapustu zriedkavo. Doslova balzamom na žalúdočné vredy je podľa MUDr. I. Bukovského šťava zo surovej kapusty. (uj) 
   

