

 
 
 
 
 
Teatrológ
Dr. Anton Kret: "Lazorík by chcel v televízii folklórne filmy na večer,
ale - kto by sa na ne pozeral?!"
 
 
Nuž - niektoré
sú priam nádherné a figľovné (napr. Ťapákov film Z chlapčeka vojak, Mlackoši na
panštine z Krivian), ale - bolo by ich treba hádam uvádzať s príhovorom, či
titulkami: 
 
 
Slováci,
a chcete byť ešte Slovákmi? Ano? Ale, preboha, podľa čoho? 
 
 
Iba
podľa Don Carlosa v SND, podľa hlúpych, násilných grimás Andy Krausza v
televíznych Susedoch? Podľa popu, rocku a anglometál skupín?! 
 
 
Raz
mi dokonca i náš básnický bard Milan Rúfus napísal: Nesúhlasím s vami, že sa
zastávate nárečia...
 
 
Šéfredaktor
bratislavského týždenníka Renesancia Nicholson /matičiar!/: Nárečie - to nie je
kultúra, ale zaostalosť! 
 
 
Riaditeľ
Matice slovenskej Markuš afrikanológovi z Ghany, žiaľ, už nebohému, Ing.
Koremovi odoprel vydať knižku o jeho drienovskom šarišskom nárečí... 
 
 
A
ja, preboha, čo si mám o tých "názoroch" myslieť, keď - ako som bol
spomínal -  pri písaní ľudových knižiek
viac ako štvrtinu nárečových slov, výrazov neviem adekvátne preložiť do
spisovnej slovenčiny, nech je akokoľvek výrazovo bohatá. 
 
 
Ale
dnes takto premýšľajú nielen tí vymenovaní, ale všetká naša vplyvná "inteligencia"
i so štátnymi predstaviteľmi. 
 
 
Národu nemá kto pripomenúť, že i v neviemakom pokroku a technike sa človek od zvieraťa
má odlišovať nielen inším biologickým citom, ale predovšetkým s vážením si
pamiatky, úctou a vedomím, že i nárečia sú najpreukaznejším dôkazom princípu
stvorenstva v prírodnej rozmanitosti. 
 
 
Na
také špecifické životnomúdre vzdelanie však nemáme ani len celoslovenský ľudový
vlastivedný časopis, platformu, kde by sa bolo možné prieť, diskutovať a tieto
skutočnosti pripomínať (múzeá deponujú len históriu a materiálnu kultúru). 
 
 
Naši
mladí a deti v médiách nemajú nijakú reláciu o dedovizni, ba práve naopak -v
bývalom rozhlasovom Rádiošpunte z Košíc sa spievali len veľkomestské hity,
nikdy nezaznela zmienka o ľudovej tvorbe, o nárečí, ľudové rozprávky sa
rozprávajú len spisovne, akoby to tak boli už i naši pradedovia rozprávali. 
 
 
V
odrodzovaní malých ďalej pokračuje rozhlasová detská Balabala, v STV všelijaké
superkvízy junior, filmy a seriály na nedeľné rána. 
 
 
Len
svetovosť má zaručiť súťaž o Cenu Dunaja a sprimitívnenie národa majú dokonať
"zábavné" televízne relácie, ktorých je také množstvo, že je zbytočné ich tu všetky
menovať. 
 
 
Máme
svetových kultúrnych velikánov a je treba ich poznať, ale doslovne -  utajujeme pred deťmi, že tu žili AJ ICH
veľkí a kultúrou bohatí predkovia. A to je ďalšia z mojich žalôb...
 
 
 
 
 
 
 

