

 
 Prvá Dzuridnova vláda. Viacerí ministri z nej museli odísť pre rôzne kauzy, nikto z nich však za ne nebol postihnutý inak než demisiou. Aj druhá Dzurindova vláda už má za sebou niekoľko afér a dva odchody ministrov, tí však museli odísť len preto, lebo odmietli slepo poslúchať stranícke príkazy. FOTO – ARCHÍV TASR 
     
 Na Slovensku máme niekoľko veľkých káuz, ktorých aktérmi sú politici. Trestná zodpovednosť je na nich krátka, napriek tomu, že pripravili tento štát svojimi rozhodnutiami o miliardy korún. 
 Transparency Internacional Slovensko v utorok zverejnila, že hoci vláda už tri roky hlása boj proti korupcii, jej vnímanie sa u nás nezmenilo. „Situácia je o to horšia, že podozrenia o podplácaní sa objavujú na najvyšších miestach.“ Vnímanie korupcie na Slovensku je na úrovni Kolumbie, Peru či Salvádora. Pavol Nechala o tom hovorí: „Podľa politikov sa boj proti korupcii má týkať každého, len nie ich.“ 
 Keď v súkromnej spoločnosti zamestnanec urobí krok, ktorý poškodí firmu, v lepšom prípade dostane výpoveď, v horšom ho čaká obvinenie z úmyselného poškodzovania zvereného majetku a trestné stíhanie. Ak sa zle rozhodne minister alebo predseda vlády, nesie maximálne politickú zodpovednosť, ktorá sa rovná dobrovoľnému alebo nútenému odchodu z funkcie. 
 Hoci má podľa politológa Grigorija Mesežnikova po preukázaní kauzy nastúpiť trestné právo, vo väčšine prípadov sa tak nedeje. Prípad, že by štát zažaloval ministra za to, že mu spôsobil škodu, neexistuje a občan to urobiť nemôže, aj keď jeho vinou má v peňaženke menej peňazí. 
 „Môže ho žalovať iba štát. Zákon o správe majetku štátu hovorí, kto koná za štát v prípade spôsobenia škody. Obyčajne je to ministerstvo,“ hovorí riaditeľ Úradu špeciálneho prokurátora Ján Bernáth. 
 Ministerstvo zatiaľ nikoho, kto musel odísť z jeho radov, nezažalovalo. Ministri sa dokonca po strate postu v úrade zvyčajne vracajú do poslaneckých lavíc, poberajú príjemných päťdesiattisíc korún čistého a ak by náhodou niekto voči nim tvrdšie vystupoval, odvolávajú sa na stredovekú imunitu. V týchto prípadoch nie sú naplnené ani len slová Mesežnikova: „Politická a morálna zodpovednosť nestačí.“ 
 A tak za kauzy, ktoré traumatizujú slovenskú spoločnosť, nie je zvyčajne nik zodpovedný za vlády Mikuláša Dzurindu rovnako, ako za čias jeho predchodcu. Vladimír Palko v minulom volebnom období na otázku, či je aspoň polovica koalície neskorumpovaná, odvetil: „Tých, čo neberú úplatky, je rozhodne menej.“ 
 Krach Devín banky 
 O čom bola kauza: dôvodom jej krachu v roku 2001 boli nedostatky v hospodárení a nedostatočné pokrývanie rizík zo zlých úverov. Napriek zlej situácii v banke jej štát požičal 60 miliónov dolárov, ktoré mala vrátiť v deblokácii, čo sa jej však nepodarilo. NBS zistila, že japonská spoločnosť, ktorá chcela vstúpiť do banky a zachrániť ju, predložila falošné doklady. 
 Hlavní aktéri: ministerka financií Brigita Schmögnerová, vicepremiér pre ekonomiku Ivan Mikloš, guvernér NBS Marián Jusko. 
 Politická zodpovednosť: Brigita Schmögnerová, Ivan Mikloš, Marián Jusko. 
 Personálny dôsledok kauzy: žiadny, Brigita Schmögnerová z funkcie odišla z politických dôvodov. 
 Čo tvrdil aktér kauzy:  Marián Jusko: „Ja si myslím, že od začiatku môjho pôsobenia vo funkcii guvernéra som robil všetko preto, aby sa Devín banka zachránila. Napriek tomu, že ten záchranný plán nevyšiel, si myslím, že to bolo vtedy správne rozhodnutie.“ 
 Výsledok preverovania: podľa guvernéra NBS Mariána Juska boli dvaja riaditelia odvolaní, ďalší štyria vedúci oddelení boli zbavení funkcií a nahradili ich noví ľudia. Dvaja funkcionári z bankového dohľadu – vrchný riaditeľ a riaditeľ odboru – dostali dekréty len na obmedzené obdobie. Vyšetrovanie stále prebieha, obvinených je zatiaľ asi desať osôb a jeden Japonec. V súvislosti s tunelovaním Devín banky bolo po vyhlásení konkurzu zavraždených niekoľko osôb. 
 Tender na výber poradcu predaja Slovenských telekomunikácií 
 O čom bola kauza: podmienky tendra z roku 1999 porušovali Obchodný zákonník. Zvýhodňovali jednu spoločnosť pred druhou. 
 Hlavný aktér: minister dopravy Palacka. 
 Politická zodpovednosť: Gabriel Palacka a štátny tajomník František Kurej. 
 Personálny dôsledok kauzy: obaja museli odstúpiť z funkcií. 
 Čo tvrdil aktér kauzy:  Gabriel Palacka o Kurejovi: „Môžete ako vodič prejsť cez križovatku na červenú, občas sa to kdekomu stane a je to porušenie zákona o cestnej premávke. Pokiaľ sa to stane niekde v noci, kde absolútne široko-ďaleko nie je ani živej duše, tak je to porušenie zákona, ale zjavne ste nespôsobili žiadnu škodu nikomu.“ 
 Výsledok preverovania: Kontrola Úradu vlády zverejnila, že za nedostatky a viacnásobné porušenie zákona nesie osobnú zodpovednosť František Kurej. Škoda nebola zosobnená žiadnemu úradníkovi. 
 Tender na mobilného operátora 
 O čom bola kauza: minister dopravy Gabriel Palacka zrušil v roku 1999 tender na tretieho mobilného operátora. Išlo o podozrenie z korupčného správania. 
 Hlavný aktér: Gabriel Palacka. 
 Politická zodpovednosť: Gabriel Palacka a štátny úradník František Kurej. 
 Personálny dôsledok kauzy: obaja museli odstúpiť z funkcií. 
 Čo tvrdil aktér kauzy:  Palacka: „Úvahy o klientelizme a korupcii sa ukázali ako nepodložené.“ 
 Výsledok preverovania: v tejto veci sa vôbec neviedlo vyšetrovanie. 
 Eurofondy 
 O čom bola kauza: išlo o podozrenie z korupčného správania pri používaní finančných prostriedkov z EÚ v roku 2001. 
 Hlavní aktéri: riaditeľ zahraničnej pomoci Úradu vlády Roland Tóth a vicepremiér Pavol Hamžík. 
 Politická zodpovednosť: Pavol Hamžík. 
 Personálny dôsledok kauzy: obaja museli odísť z funkcií. 
 Čo tvrdil aktér kauzy:  Tóth: „Ja nevidím žiadnu kauzu, pretože nikto neobjavil spreneveru jediného eura. Nechápem to.“ 
 Výsledok preverovania: prípad bol odložený 23. apríla 2003 s tým, že nedošlo k spáchaniu trestného činu. 
 Návrat akcií Nafty Gbely 
 O čom bola kauza: prezident FNM Ľudovít Kaník mal v roku 1999 poškodzovať záujem štátu v prospech súkromnej spoločnosti. 
 Hlavní aktéri: Ľudovít Kaník verzus premiér Mikuláš Dzurinda a minister hospodárstva Ľudovít Černák. 
 Politická zodpovednosť: Ľudovít Kaník. 
 Personálny dôsledok kauzy: Kaník bol odvolaný z funkcie. 
 Čo tvrdil aktér kauzy:  Kaník: „Ukázalo sa, kto hovoril pravdu a kto klamal.“ 
 Výsledok preverovania: policajné vyšetrovanie predstaviteľov FNM SR v kauze Nafta Gbely je odložené. 
 Skrytý predaj Priemyselnej banky 
 O čom bola kauza: zlá koordinácia krokov ministerstva hospodárstva a ministerstva financií v roku 1999 znamenala, že banku ovládli finančné skupiny. Tie zároveň ovládli akcie VSŽ, ktoré banka vlastnila. 
 Hlavní aktéri: minister hospodárstva Ľudovít Černák a vicepremiér pre ekonomiku Ivan Mikloš. 
 Politická zodpovednosť: Ľudovít Černák 
 Personálny dôsledok kauzy: aj vďaka kauze PB musel Černák odísť z postu. 
 Čo tvrdil aktér kauzy:  Ivan Mikloš: „Mieru zodpovednosti nie je možné zistiť vzhľadom na to, že tvrdenia týchto inštitúcií stoja proti sebe.“ 
 Výsledok preverovania: polícia nevyšetruje žiadne politické pochybenia, iba škody spôsobené zlou úverovou politikou banky. 
 Tunelovanie Štátnych hmotných rezerv 
 O čom bola kauza: v roku 2000 na verejnosť prenikla tajná správa SIS, ktorá hovorila o podozrení z tunelovania a závažných ekonomických machinácií tohto štátneho podniku. 
 Hlavní aktéri: podpredseda správy Štefan Soták a predseda Ján Odzgan, informácia unikla z ministerstva pôdohospodárstva. 
 Personálny dôsledok kauzy: obaja odstúpili zo svojich funkcií, minister pôdohospodárstva Koncoš nevyvodil žiadne dôsledky. 
 Čo povedali aktéri kauzy:  Soták: „Akákoľvek obchodná činnosť SŠHR je v kompetencii predsedu.“ Odzgan: „Považujem to za kampaň proti Správe štátnych hmotných rezerv a proti sebe, rozhodol som sa odstúpiť.“ 
 Výsledok preverovania: vyšetrovanie Najvyššieho kontrolného úradu obvinenia nepotvrdilo. Polícia v súvislosti so spomenutými vedúcimi predstaviteľmi neviedla žiadne vyšetrovanie. Stíhanie vo veci prezradenia tajnej informácie vyšetrovateľ prerušil, pretože sa mu nepodarilo preukázať, že skutok spáchala konkrétna osoba. 
 Kauza Slovenské elektrárne 
 O čom bola kauza: pochybnosti v kauze deblokačného tendra na splácanie ruského dlhu, vývoz jadrového paliva, boj záujmových energetických skupín v roku 1999 a na začiatku roka 2000. 
 Hlavní aktéri: riaditeľ elektrární Štefan Košovan, minister hospodárstva Ľubomír Harach. 
 Dôsledok kauzy: odvolanie Štefana Košovana. 
 Čo povedali aktéri kauzy:  Košovan: „Jednoznačne sme pravdepodobne narazili na záujmové skupiny.“ 
 Výsledok preverovania: pravdepodobne sa nič nevyšetrovalo. 
 Tender na ľahké motorové vlaky 
 O čom bola kauza: tender na dodávku ľahkých motorových vlakov v roku 2002, korupčné správanie štátnych úradníkov. 
 Hlavný aktér: Jozef Macejko. 
 Politická zodpovednosť: Jozef Macejko. 
 Dôsledok kauzy: odvolanie Jozefa Macejka. 
 Čo tvrdil aktér kauzy:  Peter Kresánek: „Gabo (zrejme Palacka pozn. red.) berie vec alternatívne podľa faktu, že pozadie je pre stranu rovnaké.“ 
 Výsledok preverovania: exminister dopravy Jozef Macejko, štátny tajomník rezortu dopravy Michal Balogh a vedúci úradu ministerstva Peter Klučka čelia obvineniu z trestného činu zneužitia informácií v obchodnom styku. Pre pokus o korupciu šéfa výberovej komisie Miroslava Dzurindu obvinili bývalého federálneho ministra zahraničného obchodu Jozefa Bakšaya. Úrad špeciálneho prokurátora stíha istého muža, ktorý sa nemalým úplatkom pokúsil zastaviť vyšetrovanie exministra Jozefa Macejka. 
 Kurzová strata pri predaji SPP 
 O čom bola kauza: pri podpise zmluvy o predaji SPP v roku 2002 vraj Ivan Mikloš zabudol uviesť, že kupujúca strana zaplatí v slovenských korunách takú sumu, aká bola v deň podpisu zmluvy. Štát tak prišiel podľa samotného Mikloša o 7,7 miliardy korún. 
 Hlavný aktér: Ivan Mikloš. 
 Politická zodpovednosť: Ivan Mikloš. 
 Dôsledok kauzy: strata niekoľkých miliárd korún pre štátny rozpočet. 
 Čo tvrdil aktér kauzy:  Mikloš: „Neviem o jedinej transformujúcej sa krajine, kde by sa poisťovalo kurzové riziko.“ 
 Výsledok preverovania: podľa hovorcu ministerstva vnútra Stanislava Rybana na odbore vyšetrovania závažnej trestnej činnosti na ničom takom nepracovali. 
 Odvolanie šéfa NBÚ Jána Mojžiša 
 O čom bola kauza: riaditeľ NBÚ z neznámych príčin totálne stratil dôveru premiéra. 
 Hlavný aktér: riaditeľ NBÚ Ján Mojžiš, premiér Mikuláš Dzurinda a minister obrany Ivan Šimko. 
 Politická zodpovednosť: Mikuláš Dzurinda. 
 Personálny dôsledok kauzy: Ján Mojžiš bol odvolaný z funkcie, rovnako ako Ivan Šimko, ktorý hlasoval proti jeho odvolaniu. 
 Čo tvrdili aktéri kauzy:  Dzurinda o dôvodoch odvolania Mojžiša: „Totálna strata dôvery.“  Mojžiš. „Problém medzi mnou a premiérom vznikol na prelome januára a februára v roku 2002. Bolo to v čase, keď NBÚ nemal dostatok peňazí. Premiér prišiel na NBÚ osobne rokovať o výške zvýšenia rozpočtu. Po oficiálnej časti si vyžiadal rokovanie medzi štyrmi očami, na ktorom ma požiadal, aby NBÚ zmenil stanovisko v kauze Govnet (projektu na vybudovanie vládnej informačnej siete – pozn. red.).“ 
 Výsledok preverovania: Mikuláš Dzurinda nepredložil žiadne dôkazy, ktoré by usvedčovali Jána Mojžiša z nečestnej činnosti. Tie, ktoré ukázal koaličným partnerom, boli podľa KDH a SMK nedostatočné. 

