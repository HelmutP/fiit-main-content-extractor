
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Erik Kriššák
                                        &gt;
                Poézia
                     
                 Solidarita 

        
            
                                    8.4.2010
            o
            9:48
                        (upravené
                8.4.2010
                o
                16:45)
                        |
            Karma článku:
                10.84
            |
            Prečítané 
            470-krát
                    
         
     
         
             

                 
                    Niečo mi hovorí, že toto nebude práve obľúbená báseň. Je hlavne výčitkou (predovšetkým mne, ale dôjde aj na ostatných).
                 

                 Solidarita    Pokým hluchý naťahuje uši  za zvukmi nemého, čo kýcha,   a zakrýva si rukou oči  proti svetlu, ktoré si  nedokáže predstaviť slepý,  my sa snažíme ich nevnímať.  Predčasne ukončená bytosť  bez končatín kynie aspoň hlavou  oblakom, z ktorých sa k nám  znáša dážď a všetkých  nás presviedča o tom,  že sme ešte nestratili  posledný kúsok citu.      Chlad nás strasie  z piedestálov a dáva  nám na výber.  Skryjeme sa sami,   pred všetkými  alebo pomôžeme  aspoň niektorým  zo zjavených  neviditeľných.      A možno schytíme  studenú pištoľ,  ktorá páli v dlani  a rovno ich postrieľame.      Pre ich vlastné dobro.      A aj pre naše,  čo nám majú čo  kriviť zrak,  drhnúť sluch,  zavadzať...      Ak ste, milí čitatelia,  pri čítaní  necítili nič,  vaša ľahostajnosť  predstavuje  strašnejšiu zbraň,  než je zmienená pištoľ.      Ak ste však pocítili  kvapky dažďa,  máte šancu stať sa dáždnikom  pre tých, ktorých bičujú  viac ako Vás.      Alebo strieľajte, ale  sa už konečne prejavte!      Nech vieme, na čom sme.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (42)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Zaklínač: Búrková sezóna – dvakrát do tej istej rieky nevstúpiš! Ale čoby!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Novembrová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Vtedy na Východe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Dôkaz
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Pod nebesami báseň nezaniká
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Erik Kriššák
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Milujú sa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Erik Kriššák
            
         
        erik.blog.sme.sk (rss)
         
                                     
     
         Fanúšik komiksov, literatúry a hudby. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    458
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    353
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Texty
                        
                     
                                     
                        
                            Blogové venovania
                        
                     
                                     
                        
                            Básnické grify v praxi
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Pre deti
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O hudbe
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            Komiks
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Guy Gavriel Kay - Pod nebesy
                                     
                                                                             
                                            Fernando Báez - Obecné dějiny ničení knih
                                     
                                                                             
                                            Augusto Croce - Italian Prog
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Warhorse - Ritual
                                     
                                                                             
                                            Genesis - Selling England by the Pound
                                     
                                                                             
                                            Beggar's Opera - Raymond's Road
                                     
                                                                             
                                            Premiata Forneria Marconi - Photos of ghosts
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Branislav Haspra
                                     
                                                                             
                                            Lívia Hlavačková - pitva
                                     
                                                                             
                                            Silvia Lužáková
                                     
                                                                             
                                            Saša Grodecká
                                     
                                                                             
                                            Patrícia Šilleová
                                     
                                                                             
                                            Marek Sopko
                                     
                                                                             
                                            Martin Hajnala
                                     
                                                                             
                                            Jano Topercer
                                     
                                                                             
                                            Jaromír Novak
                                     
                                                                             
                                            Ema Oriňáková
                                     
                                                                             
                                            Vitalia Bella
                                     
                                                                             
                                            Janka Bernáthová
                                     
                                                                             
                                            Dana Janebová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Žalmani na Facebooku
                                     
                                                                             
                                            Žalman Brothers Band
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




