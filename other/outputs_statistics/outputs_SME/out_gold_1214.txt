


 
Architektúra je to
socialistická, takže hneď pri vchode máš pocit, že si dostal odborársku
poukážku od súdruhov z kádrového oddelenia ako odmenu za prekročenie plánu
päťročnice závodu o 100 %.  Najprv ťa tu prelustrujú 2 gigantickí čierni
newfoundlandi, ktorí pri veľkosti chaty pôsobia ako yorkshiry. Pomaličky sme pri
vchode dopíjali fľašku Havana Club, schovanú dovtedy v igelitke v záveji pri
lanovke. Celkom dobrý nápad jedného snowboardistu z partie. Pri
snowboardistovi ti nenapadne sa čudovať, že poležkáva na snehu, tieto „tulene“
si to môžu dovoliť... a nenapadni ti, že kvôli nejakej igelitke...
 
 
Keď sme uvideli veľkú
skupinu lyžiarov /asi lyžiarsky zájazd/ ako sa blíži od lanovky ku chate, rýchlo sme dopili a utekali
dovnútra, aby sme sa konečne po lyžovaní ubytovali. 
 
 
Po otvorení dverí do
lyžiarne ma ovalilo veľké množstvo pivných sudov, kde nebolo voľné miesto čo
len na jedny lyže, architekt asi nepočítal pri toľkých ľuďoch so smädom
lyžiarov „v ére komunizmu“. 
 
 
Jedna z našich izieb
je „osmička“ s poschodovými posteľami a dvojka so
sociálnym zariadením, celkom čistým. Obe majú dvere, ktoré sa samé otvárajú,
dokonca jedny aj pri zamknutí, alebo keď otvoríš okno. Dvere na osmičke však
v konečnom dôsledku nakoniec nevydržali nápor otváraní, pochopiteľne
to vzdali a rozsypali sa. Jednému z nás sa však podaril husársky
kúsok a dvere spojil.
 
 
Celá chata vrela davmi
jačiacich študentiek, behajúce v letných tielkach po chate
a skákajúce po posteliach nad vami v izbe. To, že nepatrili do éry
normalizácie prezrádzal len piercing
a spomenuté tielka. Ich spolužiaci, už mierne zarastení, púšťali po
chodbách rap. Dúfali sme, že ranný budíček „si neodnesú“ aj naše
izby. 
 
 
Dole v jedálni to
vrelo rovnako. Mega-jedáleň socialistického typu bola plná skupiniek
s gitarami, rodín s deťmi, i starších návštevníkov hôr, tiež
lyžiarskych zájazdov od ZDŠ po VŠ. Do toho všetkého behali psy klientov chaty naháňajúcimi
nemeckú vlčicu asi domácich, ktorá práve „dohárala“,
čo štekajúco vysvetľovala všetkým psom. Jej pach tomu však nenasvedčoval. 
 
 
Večera bola skvelá !
Bravčové pečené s kapustou chutilo výborne a napriek tvojmu pocitu,
že v jedálni večeria asi celé Slovensko, obsluha bola rýchla
a nečakalo sa ani na príbory ako minule (nemáte lyžice ? Veď títo vedľa
práve jedia polievku, vyčkajte!).
 
 
Zlatým klincom večera bol
vydarený koncert Ďura Buriana, Valihoru za bicími, Juraja Grigláka za basou,
Marcela Palondera za mikrofónom, a skvelým klávesákom....  Koncert
v bare v suteréne sa rýchlo zmenil na jeden veľký žúr, kde skoro
všetci tancovali a pálenô tieklo potokom.
 
 
Videla som tu dokonca
jedného lyžiara, ktorého som stretávala v ten deň na všetkých zjazdovkách,
pravda, hlavne v bufete. Na koncerte mával nad hlavou čiernym tričkom
s nápisom POLICIA, a zrazu skončilo na činelách bicích Valihoru.
Tomu to vôbec neprekážalo a ani brvou nepohol a mastil do bicích ďalej.Týpek sa potom pridal k ďalším 3 do pol pása vyzlečeným (asi kolegom) v bare,
ale on jediný vyzeral z nich ako YETTI, bol neuveriteľne celý chlpatý (videla som ho len od chrbta), čím celkom zapadol do prostredia
horskej chaty.
 
 
Celkom vtipná atmoška
bola v ten deň aj na zjazdovke. Lanovka už dlhšie stála, tak sme v hmle ako mlieko lyžovali
na krátkej zjazdovke Kôprovka. Keď sme vyšli vlekom hore, ozýval sa na svahu hlučný
popevok „...Kerá nemá frajíra, kerá nemá frajíra...“ Úsmev ti
nezmizol z tváre, ani potom, keď si tohto vtipného lyžiara nezaregistroval
ani na tretí krát, čo furt spieval, niekde na kopci. Milý lyžiar totižto takto zúfalo vyspevoval hore „v
oblakoch“ na lanovke, ktorá sa nehýbala už dobrú pol hodinu. V ten deň
nešli ďalšie 2 vleky, tak sme si to nadávajúc vyšľapali na neupravované Zadné
Dereše. Tu v bufete fičal The Stranglers a (The) Polemic. Dobre sa pri tom popíjal čaj s rumom. Inak, stromy akoby namočené do snehovej šľahačky  s priesvitnými cencúľmi vyzerali ako na Bielej púšti v Egypte, ale možno to
spôsobili tie čaje. 
 
 
            Akokoľvek, v nedeľu
nás čakal prekrásny slnečný deň. Možno aj preto zrazu išli vleky tak, ako mali
ísť. Na „Koske“ sa tá stará dobrá atmoška nevytratila. Čakáš, že
stretneš „Anděla na horách“ v koži Marvana, a v módnom svetríku
nórskych vzorov aktuálnom aj po päťdesiatich rokoch natáčania tohto filmu na Slovensku.

 
 
            Počula som,
že chata Kosodrevina je vo vlastníctve J&amp;T (dúfam, že sa mýlim) a vedúci ju má len v
prenájme. Ak je to pravda, už teraz sa modlím, aby z nej raz nebol ďalší
nepodarený snobský projekt „sedlákov“ J&amp;T typu Lomničák a (ako Petržalka vyzerajúci) trápny River
Park.
 

