

 
 FOTO – ČTK 
 
 Parlament schválil zákon len nedávno, ale jeden z jeho predkladateľov už v liste požiadal policajného riaditeľa, aby s pokutami ešte počkalaBRATISLAVA – Zákon o povinnom používaní zimných pneumatík parlament schválil iba prednedávnom. Jeden z jeho predkladateľov už v liste stihol požiadať policajného riaditeľa, aby polícia vodičov za jeho nedodržiavanie netrestala. 
 „Aby sa nestalo, že 16. novembra vyjdú policajné hliadky do ulíc a budú mať Vianoce,“ hovorí autor listu, poslanec Jirko Malchárek. Myslí si, že je dobré, ak bude zákon účinný aj bez sankcií. Ľudia by si na povinné používanie zimných pneumatík mali počas prechodného obdobia zvyknúť a po čase sa pre nich podľa neho stane samozrejmosťou. „Mne nejde o to, aby boli vodiči trestaní, ale o to, aby to brali ako samozrejmú záležitosť,“ povedal. 
 Policajný riaditeľ Anton Kulich je na dovolenke, k listu sa ešte oficiálne nevyjadril. „Keď sa vráti, určite sa bude listom pána Malchárka vážne zaoberať,“ vyhlásil policajný hovorca Jaroslav Sahul. Očakáva, že jeho šéf si dá vypracovať odborné stanoviská od špecializovaných útvarov polície a potom sa rozhodne. 
 Ak by Kulich s Malchárkom súhlasil, nebol by to prvý podobný prípad. Už v minulosti chvíľu trvalo, kým policajti začali po prijatí zákona pokutovať vodičov, ktorí v zimných mesiacoch nesvietili. Malchárek tvrdí, že oba prípady sú rovnaké. 
 Neobáva sa, že zákon bez trestu stratí význam? „Nie je možné ľudí stále podceňovať, že sú ochotní urobiť niečo v súlade so zákonom, len keď ich čaká tvrdá sankcia,“ myslí si Malchárek. Dostatočným trestom má byť aj dohovor od policajtov. 
 Pripustil, že zákon môže niekomu spôsobiť komplikácie. Nevie, ako sú pripravení dodávatelia, problémy môžu vzniknúť so skladovaním veľkého počtu pneumatík. Zvýšené náklady nepovažuje za prekážku. „Proti bezpečnosti nie je finančný argument významný,“ tvrdí Malchárek. 
 (rp) 
 Znenie zákona 
 § 31a – Osobitosti cestnej premávky v zimnom období 
 § Na idúcom motorovom vozidle s výnimkou motocyklov a zvláštnych motorových vozidiel sa v čase od 15. novembra do 15. marca musia používať pneumatiky so zimným druhom dezénu s označením M+S, M/S alebo MS alebo terénnym druhom dezénu. Podrobnosti o osobitostiach cestnej premávky v zimnom období ustanoví všeobecne záväzný právny predpis, ktorý vydá Ministerstvo vnútra Slovenskej republiky. 
 Argumenty predajcov pneumatík 
 
 V zime vzniká veľmi veľa kolízií vinou nevhodných pneumatík. Celá sada zimných pneumatík je zvyčajne lacnejšia ako oprava hoci aj po malej nehode. Vyššiu bezpečnosť nie je možné vyčísliť. 
 Moderné zimné pneumatiky sú dostatočne komfortné. Vďaka vyspelým technológiám sú rovnako tiché ako letné pneumatiky. 
 Neopotrebúvajú sa rýchlejšie ako letné. 
 Oproti letným pneumatikám majú v zime neporovnateľne vyššiu účinnosť. Ide hlavne o rozhodujúce vlastnosti, ako je brzdný účinok, akcelerácia, stabilita v zákrutách. 
 Sú účinné na mokrej vozovke. Zima nie je iba sneh a ľad, ale aj voda, dážď, polosucho, sucho, námraza, kolísanie teplôt. Na toto všetko sú zimné pneumatiky stavané. Uplatnia sa päť – šesť mesiacov v roku a niekedy aj dlhšie. Použitie sa odporúča, keď teplota klesá pod + 7 oC. 
 V decembri 1999 uskutočnil Autoklub ČR porovnávacie testy letných a zimných pneumatík. Auto s letnými pneumatikami malo brzdnú dráhu z rýchlosti 40 kilometrov za hodinu na nulu 22,5 metra, rovnaké auto so zimnými pneumatikami 17,6 metra. (zdroj: www.mikona.sk) 
 
 Koľko stojí výmena pneumatík 
 Odmontovanie a namontovanie z auta – od 25 korún za kus 
 Stiahnutie plášťa z disku a natiahnutie nového – od 35 korún za kus 
 Vyváženie kolesa – od 40 korún za kus (v niektorých servisoch osobitne účtujú za premeranie na vyvažovacom prístroji a osobitne za každý kus oloveného závažia, ktoré sa používa na vyvažovanie) 
 Spolu – od 100 korún za kus (za štyri kusy od 400 korún) 
 Cena sa líši podľa polohy pneuservisu a značky. V drahších značkových stojí výmena aj 800 korún, ale často ponúkajú akcie. 
 Pneumatiky na Škodu Fabia 
 Zimné pneumatiky – od 2 300 korún za kus 
 Niektorí vodiči si okrem pneumatík kupujú na zimu aj disky a vymieňajú celé kolesá. Jeden disk na Fabiu stojí od 1 400 korún. 
 Majitelia Fabií tak v novembri môžu zaplatiť aj pätnásťtisíc a viac. 
 V predaji sú aj protektorované pneumatiky (už raz zodraté, na ktoré sa znova nanesie dezén) a stoja zhruba o 40 % menej ako nové. V prípade protektorov ide len o východisko z núdze. (rf) 

