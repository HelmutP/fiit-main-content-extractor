

  
  
 
  Hm.....prečo vlastne o tomto vôbec písať... Ale predsa lepšie písať ako nič... Začal by som asi tým suchým vtipom, na ktorom sa veľká časť ženskej populácie vytočiť a potvrdiť, že nemá často zmysel pre humor... Teraz už ozaj viem, že pri týchto riadkoch, som pichol do osieho hniezda.. 
 Často som si kládol tú ošúchanú otázku, „je lepšie byť ženou, alebo mužom?". Viem že odpoveď na ňu by bola asi taká „toto je neutrálna záležitosť, pretože obe pohlavia sa navzájom geniálne dopĺňajú" ale je to skutočne tak...? 
 Zoberme si jednoduchý príklad, z morfologického hľadiska bol muž už od útleho veku logicky považovaný za príslušníka „silnejšieho" pohlavia ľudského rodu. Chlapca sa nikdy nikto nepýtal a vložil sa mu do ruky hračkársky samopal, dievča si zasa intuitívne vybralo za objekt záujmu bábiku. Klásť si otázku, prečo je to tak a popri tom si dať na to iba strohú odpoveď na spôsob „veď chlapci sú silnejší a drsnejší od prírody..." nie je podľa mňa úplnou pravdou. Ak by tak bolo, potom by som intuitívne zasa použil protiotázku, „prečo sú neskôr jedinci mužského pohlavia zženštení a naopak?" Podotýkam, že nenarážam teraz na problém, s rovnakou sexuálnou orientáciou. Zoberme si teraz príklad z bežného života. Žena plne vyťažená príde z práce domov a čaká ju ten známy výjav z bežného života, ktorý sa až chorobne a šablónovite prehadzuje na stránkach feministických časopisov...a to je, keď nájde muža (ak už prišiel z práce skôr ako ona, v pohodlnej polohe s vyloženými nohami, pozerajúc futbalový zápas (ak dávajú..) pričom v jednej ruke drží ovládač a v druhej urputne zviera pivovú fľašu... Tieto články, ako by im nestačilo na dôraz ešte podotknú, že táto žena, nazvime ju"chudera", kým sa dostane k mužovi, musí sa popredierať  cez haldu špinavých ponožiek, rozhádzaných po miestnosti a ak ju neodradí ani halda neumytého riadu po letmom nakuknutí do kuchyne, si pri ňom iba skromne a polohlasne povzdychne... 
 Ten istý „druh" muža, na druhý deň je pozvaný kamarátmi  na panskú jazdu, na ktorú je nahodený ako filmová hviezda.. To ale je už pre ženu absolútny vrchol a často s pocitu frustrácie mu následne vytkne, že ona je práve tou, ktorá okrem polovičného príjmu, ktorý dáva do spoločného rozpočtu, sa ešte stará skoro o celú domácnosť. Muž jej ale následne vysvetľuje, že ťažko drie a prečo by si z času na čas nemohol dopriať trošku času aj pre seba. Žena je zasa tá, ktorá si to vysvetľuje po svojom a jej vnútorná veta často znie „ myslela som si, že najlepšie sa cíti so mnou..." Hádka, konflikt, alebo ako sa to nazve, je na svete... 
 Teraz prevráťme list a predstavme si muža v pozícii, keď sa vráti domov z práce, zájde na nákup ak treba, dokonca vyžehlí (ak to aspoň trošku ovláda), nahádže veci do práčky a ku podivu tento „druh" muža, vie odlíšiť aj čo je farebné a biele....veď nie je šimpanz... a bez problémov vynesie smeti, umyje riad, ponožky má na mieste a rád si pozrie aj telku a prečo nie.. veď je tiež iba človek. Tento typ muža vie dokonca narátať nielen do desať, ale dokonca aj do sto... No iróniou je, že práve žena je tá, ktorá vytvorila jednotný mýtus o mužovi-povaľačovi, neschopákovi, ktorý smrdí od cibule a piva, mužovi, ktorý nemá mimo nasávania a TV, žiadne záľuby, nemá tak isto ani estetické cítenie, nemá empatiu, nemá skoro žiadne atribúty z toho, čo má žena... je to proste úbožiak, ktorý je odkázaný iba na neúspešné korigovanie a následné zlyhanie vlastného libida, odkázaný iba na hrubosť, sex a všetko to povrchné z povrchného... jednoducho „klasika muž"... 
 Teraz položím otázku a čo „klasická žena" existuje pre muža aj taká? Existuje mýtus o žene z pozície muža? To je dobrá otázka a ja sa pokúsim teraz nad ňou zamyslieť. V spoločnosti sa etabluje žena ako niekto, kto dotvára šarm, harmóniu, krásu a jemnosť zároveň, čomu v žiadnom prípade nenamietam a je to pre muža nádherný fakt. Pokiaľ......... pokiaľ si táto „klasická žena lepšie prepojenými hemisférami  jednoducho nepovie „čo by sa z toho že som žena, ešte dalo vyťažiť...?" a to myslím doslovne. Potom ani vtip, že „ženu ani kvetinou, iba kvetináčom..." ju nerozhádže a ide si k baru popýtať ďalší drink. Má popri tom veľa „nástrojov" ako tohto čiastočného robota s pytlíkom medzi nohami zamestnať vo svoj prospech... 
 PODNIK: 
 
 Žena nemá problém požiadať muža odrink 
 Žena nemá problém sodvozom 
 Žena (väčšinou) nemá problém snadviazaním kontaktu, na rozdiel od muža (výnimka mužov splnou peňaženkou) 
 Žena často nemusí zaplatiť ani cent ato nie len pri povestnom prvom stretnutí 
 Žena má automaticky grátis vstup 
 Práve pre ženy sú ušité rozličné akcie tipu „Mojito za polovicu pre dámu..." apod. 
 Žena ostáva často aj po záverečnej, chlapa vyhodia 
 Žena tancuje aj so ženou anik jej nepovie „lezba" 
 Jej peňaženka často ostáva nedotknutá... 
 
   
   
 Toto je iba zopár príkladov, každopádne nechcem byť voči nežnému pokoleniu zaujatý, skôr je to iba môj strohý, pre podaktorých stupídny ale úprimný názor. Aj napriek tomu si myslím, že ženy sú súčasť mužského života a robia svet krajším, takže rozhodne netvrdím, že muž, alebo žena je dôležitejšia. Moja reč na záver, muž a žena sa budú krásne dopĺňať, krásne nechápať, krásne súperiť medzi sebou, ale k konečnom dôsledku založia spolu rodinku a šmitec. 
 

