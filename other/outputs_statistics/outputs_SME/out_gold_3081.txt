

 „Volala som sa Salmonová, ako tá ryba, losos. Krstné meno Susie. Mala som štrnásť rokov, keď ma 6.decembra 1973 zavraždili...“  
 Tak sa začína kniha „Pevné puto“. Susie rozpráva o tom, ako vyrastala a ako žila predtým, ako ju zavraždil na ceste zo školy psychopatický sused. 
 „Môj vrah bol zo susedstva. Mame sa páčili jeho kvetinové záhony a otec sa s ním raz rozprával o hnojení...“ 
 Necháva nás nahliadnuť do neba, kde sa teraz jej duša nachádza, no hlavne opisuje život tam dolu. Ako si jej spolužiaci odovzdávajú chýry o jej zmiznutí, jej rodina sa drží nádeje, že ju nájdu, jej vrah sa snaží zahladiť stopy a neodhalený sa chystá vraždiť opäť. Mesiace a roky však plynú a vzťah jej rodičov je v troskách, jej braček nechápe, čo sa stalo a sestra sa rozhodne byť oceľovo tvrdá... 
 „Pán Harvey vravel, že ma nezdrží, a tak som šla za ním do poľa, kde už kukurica nebola polámaná, lebo tadiaľ nikto nechodil. Všimla som si, že pán Harvey sa na mňa čudne pozerá...“ 
 Autorka majstrovsky vystihla kontrast drsných zobrazení, udalostí a optimistického nadhľadu Susie, ktorá všetko popisuje ešte detskými očami. Rozpráva sviežo, oduševnene, túži ovplyvniť beh vecí tam dolu, no nedarí sa jej to. Díva sa na spomienkovú bohoslužbu aj neúspešné vyšetrovanie, keďže jej telo sa nikde nenašlo...iba odrezaný lakeť priniesol v zuboch susedovie pes. 
 „Bránila som sa...ale aj tak som sa čoskoro ocitla na dlážke a on na mne, zadychčaný a spotený; v zápale bitky stratil okuliare...Bola som vtedy taká živá!“ 
 Nádherné sú opisy Susie a jej puta s blízkymi...keď bola vo vzduchu okolo nich; keď bola v studených ránach, ktoré trávievali spolu; keď bola chvíľkou samoty, do ktorej unikali... To puto bolo natoľko pevné, že aj keď ho ktosi násilne a kruto preťal, nedalo sa len tak oddeliť. Ani zo strany blízkych, ani zo strany Susie. A v tom je nádej tejto knihy. Že aj keď nás blízki opustia, sú niekde na onom svete, kde je im už dobre... 
 „Vedela som, že ma zabije. Len som si neuvedomila, že už zomieram...Natiahol sa a rukou zašmátral kdesi nad svojou hlavou, vo výklenku, kde mal veci na holenie. Keď ju stiahol, držal v nej nôž. Vybral mi čiapku z úst. - Povedz, že ma ľúbiš, - prikázal mi. Povedala som. Nežne. Aj tak prišiel koniec." 
 Prečítajte si ďalšiu kapitolu z knihy Pevné puto. 
   

