
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peťo Varga
                                        &gt;
                Nezaradené
                     
                 Iný 

        
            
                                    21.3.2010
            o
            19:18
                        |
            Karma článku:
                4.81
            |
            Prečítané 
            667-krát
                    
         
     
         
             

                 
                    V dnešnej dobe demokracie, voľnosti a slobody môže ktokoľvek robiť takmer čokoľvek. Doslova. Mám pocit, že ľudia skúšajú, robia, páchajú. Ale musí to robiť každý?
                 

                      Rozmýšľal som, čo môže človek v 20. rokoch študujúci na vysokej škole robiť. Čomu sa venovať. Študenti počas prednáškového obdobia žijú. Hovorí sa, že tento život je najkrajšou časťou v živote človeka. Doby študentské. Čo to však obsahuje dnes? Diskotéky, alkohol, cigarety, možno nejaké mäkké drogy a opäť dookola. Človek skúsi všetko, ale po čase ho prestane baviť ten cyklus.   Teraz budem písať o sebe, o mojich pocitoch z tohto všetkého. Áno, človek si užíva, no ak nie už v tej miere, že furt behá von, samé krúžkovice, katedrovice, bytovice, chodbovice, izbovice... tak je považovaný za iného. Prednedávnom mi bolo povedané, že na to, koľko mám rokov si neužívam život tak, ako by som mal. Vraj sa zamýšľam a riešim veci, ako starý 40- ročný chlap. Pýtam sa prečo je to tak? Prečo som iný? Niekedy by som chcel byť ako ostatní. Žiť proste jednoduchým študentským životom, no furt mi niečo vo mne hovorí, rob aj niečo navyše. Prestal som navštevovať v takej miere diskotéky, kluby a podobne a zameral som sa na štúdium. Nie, že by som bol kocka, môj priemer vôbec nepatrí medzi najlepšie na univerzite, ale rozmýšľal som, čo by som chcel v živote vedieť. Pri otázke na mojich spolužiakov mi zásadne odpovedajú, že oni ešte nevedia. Ja mám akú – tú predstavu, čo ďalej, čo by sa dalo, čo by som chcel. V mojom veku asi nikto nerobí to čo ja, nechcem sa teraz chváliť, určite nie, ale robím veci, ktoré ľudia v mojom veku asi nie. Dostal som možnosť byť na pozícií manažéra jedného kultúrneho telesa. Prečo nevyskúšať takúto prácu. Nie je to platené, ale prečo nevyskúšať organizovanie a manažovanie niečoho takého a keď to ešte človeka aj baví... Niekedy mám chuť meniť tento svet, nepáči sa mi, ako všetko funguje, ako to je. Študujem ekológiu a už v minulosti som pracoval na rôznych eko-projektoch a rozhodol som sa popracovať na nich aj teraz. Myslím si, že takéto práce človeka posúvajú, preto aj na vysokej škole pracujem na vedeckej práci. Čo z toho vyplýva? Som iný. Prečo? Lebo robím to čo ma baví, potom som odcudzovaný a niekedy sa cítim ako opustený topoľ niekde v poli. Potom nastupujú depresie a otázka na samého seba, je dobré čo robíš, nemal by si niekedy zapadnúť s davom? Ak sa nebavím s ľuďmi o tom, kto kde chlastal a s kým spal, ale hovorím o politike, kultúre či ekonomike som opäť považovaný za iného. Vraj neadekvátne témy môjmu veku. To je zlé, že poviem, čo si myslím o politickej situácii v štáte v ktorom žijem? Mám na to právo, nie? Potom človek naozaj upadne do brutálnej depresie a chcel by zmeniť celý svoj život, celého seba.   Zatiaľ som v živote stretol málo ľudí, ktorí by boli ako ja... asi som nejaké indivíduum... proste iný.   Takže čo z toho vyplýva? Že sa asi pôjdem riadne opiť, vyspať sa s niekým a vtedy asi získam opäť medzi mojimi vrstovníkmi dobré a normálne postavenie, nie nálepku čudáka.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Varga 
                                        
                                            Separovanie odpadu alebo ako na to?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Varga 
                                        
                                            O pomoci
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Varga 
                                        
                                            Práca navnivoč?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peťo Varga
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peťo Varga
            
         
        petovarga.blog.sme.sk (rss)
         
                                     
     
        Mladý človek žijúci na Slovensku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    750
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




