
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Banáš
                                        &gt;
                Úvaha
                     
                 Vďaka, za každú krásnu hokejovú chvíľu chlapci! 

        
            
                                    16.5.2010
            o
            20:45
                        (upravené
                16.5.2010
                o
                20:38)
                        |
            Karma článku:
                2.02
            |
            Prečítané 
            2585-krát
                    
         
     
         
             

                 
                    Každý rok v tieto jarné dni prepadne celé Slovensko horúčke. Pýtate sa akej? No predsa tej, ktorú spôsobuje hokej. Ten medzinárodný. Majstrovstvá sveta v ľadovom hokeji, vrchol celej sezóny a to, čoho sa celý rok každý správny hokejový fanúšik nevie dočkať.
                 

                     Azda najväčším meradlom športového úspechu je záujem fanúšikov. Majstrovstvá sveta v ľadovom hokeji sú neodmysliteľnou tradíciou spätou práve s týmto športom. Hokejový sviatok, na ktorom sa rok čo rok stretáva šestnástka najlepších hokejových krajín bojujúcich o titul najlepšieho reprezentačného mužstva na svete.       Do povedomia slovenských fanúšikov sa MS v hokeji dostali predovšetkým vďaka našim najväčším úspechom, zakončených medailovými umiestneniami v rokoch 2000, 2002 a 2003.       Najlepšia generácia našich hráčov dosiahla až na hokejový trón a navždy sa zapísala nielen do histórie, ale aj do sŕdc a povedomia každého Slováka.        Po týchto mimoriadnych úspechoch sme ale čakali dlho. Slovensko znovu žilo hokejom, no dôvodov na radosť nebolo ani zďaleka veľa. Začiatkom roka sa však na vancouverskej zimnej olympiáde stretla snáď najsilnejšia zostava, akú mohlo Slovensko na vrcholové podujatie poskladať a málo chýbalo k tomu, aby sme znovu prežívali tie neopísateľné pocity radosťi, čo pri predošlých úspechoch.                         Pred týmito majstrovstvami sveta konajúcimi sa v Nemecku bolo celkom jasné, že náš tím bude musieť prejsť generačnou výmenou. Úspešného trénera Jána Filca nahradil na našej lavičke kanadský odborník Glen Hanlon, no a jemu sa podarilo počas krátkej prípravy poskladať partiu mladých hráčov a tých, ktorí nikdy v reprezentácii nedostali dostatok priestoru.            Tomuto výberu nikto neveril. Nominácia sa mnohým „akože-fanúšikom“ zdala prislabá. Premládnutý výber sa však nenechal zahanbiť a všetkých prekvapil už v prvom stretnutí, proti hviezdami nabitému výberu Ruska. Hoci sme nakoniec prehrali 1:3, je to prehra za ktorú sa ani jeden člen slovenského mužstva nemusí hanbiť.    V ďalších zápasoch naši ukázali nezlomnú vôľu a otočili stretnutie s Bieloruskom, či pomerne hladko zdolali Kazachstan po výsledku 5:1.       V prvom zápase osemfinálovej skupiny však naši narazili na veľké hokejové prekvapenie z Dánska. Dáni v predchádzajúcich zápasoch šokovali Fínov aj USA, no a hoci boli na papieri proti nám jednoznačným outsiderom, zápas sa vyvinul celkom inak.   Po prvej tretine na svetelnej tabuli svietil hrozivý stav 0:6. Šok, chladná sprcha, tvrdá facka pre nás, ktorej by nikto predtým neveril. Odrazu bolo sebavedomie našich hráčov, ale dovolím si tvrdiť že hlavne fanúšikov, zrazené poriadne k zemi.            Priznám sa, že patrím k tým, ktorí tomuto výberu moc neverili. Však nemôžeme vypadnúť, je jedno či prehráme všetko, hlavné je, že priestor dostali mladí, ktorí sa potrebujú rozohrať. Napokon sa však ukázalo, že aj tento zdanlivo neznámy tím dokáže zahrať vyrovnané zápasy na najvyššej úrovni. A keďže sme my národ slovenský natoľko zvláštny, hneď potom ako naši začali predvádzať dobré výkony, sebavedomie bolo vystrelené do nadpozemských výšin a každý začal mať hubu plnú rečí. A akonáhle naši dostali na hubu od toho „Legolandu čo nevie čo je to hokej“ tak sa začalo na nich odvšadiaľ kydať.        „Doprdele, to snáď neni možné.“       Citujem slová nášho kapitána Richarda Lintnera, ktoré vyslovil po skončení prvej, hrôzostrašnej tretiny súboja s Dánskom do televíznych kamier. Tieto slová sa potom akousi záhadnou cenzúrou na internet už nedostali, namiesto toho internetové diskusie zaplavili ešte horšie bludy presne od tých ľudí, ktorí len deň predtým vynášali celú repre s Glenom Hanlonom na čele k nebesiam. Vedia tí ľudia čo vôbec chcú? Potom to doprajem radšej aj Dánom, ktorí sa narozdiel od našich hokejom vedeli baviť aj keď prehrávali. Doprdele s nimi, alebo s nami? Toto fakt nie je možné.           Jedna prehra, v ktorej sme podali oduševnený výkon. Jedno skutočne pekné víťazstvo, potom jeden zápas s dobrým koncom, avšak poriadne odfláknutý. A teraz prehra, akú sme ešte nezažili.       Taká je bilancia našich na tohtoročných majstrovstvách. Stále nás však čakajú dve stretnutia, najprv silní Fíni a potom domáci Nemci, hnaní vpred svojimi vernými fanúšikmi.    V podstate je jedno, či tieto dva zápasy prehráme. Záleží na tom, akým spôsobom ich prehráme. Alebo naši znovu prekvapia, znovu budú pre každého najlepší a potom dostaneme na hubu? Doprajem tomuto tímu len to najlepšie. Nech idú pekne krôčik po krôčku za svojím snom, o ktorom sa im snáď ani len nesnívalo.   Nepochybujem o tom, že na to majú. No ak sa im predsa nepodarí splniť to, nič sa nedeje. Stále sa len vo veľkom hokejovom svete obzerajú, stále len hľadajú svoju cestičku. A niekedy stačí skutočne málo, len kúsok srdiečka a nezlomnej vôle.       Trikrát sme to nečakali, trikrát sa to podarilo. Tak prečo nie teraz?               Áno, znovu sa budem opakovať. Stačí iba málo – nájsť správnu dávku optimizmu, ale nestrácať pritom triezvy úsudok. Len je veľká škoda, že my Slováci to zatiaľ nevieme.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Ako za starých časov 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Dva póly filmového roka 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 2
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Banáš
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Banáš
            
         
        milanbanas.blog.sme.sk (rss)
         
                                     
     
        Ak chceš vedieť aký som - spoznaj ma. Ak chceš vedieť ako píšem - čítaj ma :) 
Čo viac dodať? Som začínajúci autor z Liptovského Mikuláša.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    129
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1458
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Liptovský Mikuláš
                        
                     
                                     
                        
                            Úvaha
                        
                     
                                     
                        
                            The Game
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Vyjednávač
                        
                     
                                     
                        
                            Záhady
                        
                     
                                     
                        
                            Legendy
                        
                     
                                     
                        
                            Poviedka
                        
                     
                                     
                        
                            Spektrum
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Luceno J. - Háv klamu
                                     
                                                                             
                                            Barry D. - Veľké trampoty
                                     
                                                                             
                                            Keel J. - Mothman Prophecies
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lady GaGa
                                     
                                                                             
                                            Depeche Mode
                                     
                                                                             
                                            Hudba z prelomu 80 a 90 rokov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Sčobák
                                     
                                                                             
                                            Peter Bombara
                                     
                                                                             
                                            Pavol Baláž
                                     
                                                                             
                                            Juraj Lisický
                                     
                                                                             
                                            Dominika Handzušová
                                     
                                                                             
                                            Roman Slušný
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Supermusic
                                     
                                                                             
                                            Gorilla.cz
                                     
                                                                             
                                            Hokejportál
                                     
                                                                             
                                            Facebook
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




