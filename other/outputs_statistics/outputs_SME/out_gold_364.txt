

 Čaj predstavuje pohodu a uvoľnenie a patrí najmä k dlhým chladným večerom. Pitie čaju je dlhodobou tradíciou. Prvé zvyky, obrady a nájdené pamiatky pochádzajú z Číny. Preslávený je tiež pojem čajový  obrad, ktorý sa spája najmä s japonskou kultúrou. 
 Každá krajina má špecifické prvky v pití čaju. Napríklad v Anglicku čierny indický čaj predbehol v obľúbenosti zelený čaj, Američania zase po vyhlásení nezávislosti prestali piť čaj s mliekom a pridávajú do neho kus ľadu. Pre Maroko je typický zelený  čaj s mätovými listami a cukrom. A aj u nás sa pitie čaju teší obľube. Čoraz viac ľudí vyhľadáva príjemné posedenie v pribúdajúcich bratislavských čajovniach. Hoci majú všetky spoločnú širokú ponuky sypaných čajov a iných pochutín, každá má svoju vlastnú, jedinečnú atmosféru a  špeciálne recepty. 
 Čajovňa Michalský dvor, Michalská 3 
 Otváracie hodiny: pondelok - nedeľa 14.00 - 21.00 
 Útulná podzemná čajovňa v centre Starého Mesta láka svojich návštevníkov najmä vďaka príjemnej atmosfére. Interiér zdobia originálne kresby výtvarníka Fera Liptáka, záujemcovia môžu  v susediacej galérii navštíviť aj výstavy rôznych umelcov. V podzemnej čajovni je približne 40 miest. Čajovňa ponúka viac ako 50 čajov rôznych druhov. Podávajú sa tu v originálnych čajových servisoch od Štefana Janca. Návštevníci si môžu ktorýkoľvek z ponúkaných čajov tiež zakúpiť a doma si tak  vychutnať atmosféru pravej čajovne. 
 Ako pripraviť zelený čaj - Pai Mu Tan 
 Tento zelený čaj pochádza z juhovýchodnej čínskej pobrežnej provincie Fuijan. Pripravuje sa zo sušených nerozvinutých púčikov, ktoré majú bielostriebristú farbu. Zalieva sa 70-stupňovou vodou a môže sa nechať vylúhovať iba dve až tri  minúty, aby príliš nezhorkol. Zelený čaj sa má piť  horúci alebo teplý, aby si zachoval svoje blahodarné účinky, známa je tiež veta: „Horúci zelený čaj je liek, studený jed." Veľké množstvá zeleného čaju pijú aj obyvatelia ázijských krajín a práve v tom vidia mnohí príčinu ich dlhovekosti.  (oba)	 
 Čajovňa v podzemí, Ventúrska ulica 
 Otváracie hodiny: pondelok - nedeľa 13.00 - 23.00 
 Voňavá čajovňa v podzemí existuje od roka 2001 a len dva týždne sídli v Zichyho paláci na Ventúrskej. Čajovňa ponúka viac ako 100 druhov čajov z celého sveta, pripravených podľa tradičných  receptúr. Na výber sú zelené, polozelené, čierne, biele čaje, aj nečaje, čajové špeciality, ľadové čajové špeciality, ovocné a slovenské bylinné čaje. Každý nápoj sa podáva v originálnych kanvičkách. Okrem duši lahodiacemu nápoju sa tu dajú zakúpiť aj keramické súpravy čajového  riadu a hrnčeky, ako aj sypané čaje. 	 (dot) 
 Ako pripraviť Pu-Erh 
 Pu-Erh je silný, tmavý alebo niekoľkokrát fermentovaný čaj. Má veľmi dobré detoxikačné vlastnosti, upokojuje žalúdok a prečisťuje krv, vďaka čomu sa ľudovo nazýva požierač tukov. Podobne ako víno, aj Pu-Erh rokmi zreje. Pripravuje sa tak, že po  zaliatí vriacu vodou sa 10 - 15 minút lúhuje nad sviečkou. Potom sa nalieva. 
 Čajovňa Pohoda, Laurinská 1 
 (hlavný vchod je z Radničnej ulice) 
 Otváracie hodiny: pondelok - piatok 9.00 - 22.00, sobota a nedeľa 10.00 - 22.00 
 V Čajovni Pohoda je k dispozícii viac ako dvesto druhov rôznych čajov a zákusky. Interiér je ladený do  optimistických oranžovožltých tónov, príjemnú atmosféru dotvárajú stoličky v pastelových farbách. V priestoroch s kapacitou sedemdesiat ľudí si návštevníci môžu zahrať šach alebo obdivovať vystavené vodné fajky, ktoré však slúžia len ako dekorácia. 
 Z čajov zaujme argentínske maté, ktorý má  aj vďaka veľkému množstvu kofeínu povzbudivý účinok. Kofeínu je v ňom dokonca viac ako v káve. Tradične sa maté podáva v tzv. kabasse (nádobka z vysušenej tekvice) a pije sa horúci pomocou kovovej slamky - bombilly. 
 Ako pripraviť ajurvédske čaje 
 Varia sa s mliekom a medom presne „védskych" dvadsať minút. V čajovni  majú štyri druhy s názvami Relax, Pekný sen, Vitálna žena a Aktívny muž. Ak chcete mať doma aktívneho muža, pripravte si škoricu, klinček, zázvor, kardamón, fenikel, Damianove lístočky, kakaovú kôru, svätojánky chlebíček, žeň-šeň a čierne korenie. 		 (mad)
 
 Čajovňa Lilith, Živnostenská ulica 
 Otváracie hodiny: pondelok - piatok 10.00 - 22.00, sobota 14.00 - 22.00, nedeľa 15.00 - 22.00 
 Čajovňa s klubom Lilith je otvorená niekoľko mesiacov, napriek tomu si získala svojich návštevníkov najmä vyhovujúcou polohou v centre mesta. Čajovňa je situovaná v úzkej uličke medzi Vysokou a Kollárovým  námestím. Hoci sa nachádza na dvoch poschodiach, väčšina návštevníkov uprednostňuje atmosféru v podzemí. Interiér je v pozitívnych, svetlých farbách, steny ozvláštňuje  napríklad zaujímavý odev alebo rôzne dekorácie. Celková kapacita podniku je približne 60 miest.  Podnik ponúka 45 druhov rôznych sypaných čajov.  Špecialitou podniku je tiež napríklad arabský chlieb, pripravovaný priamo v čajovni. Návštevníci si nad voňavým čajom môžu zahrať šach alebo v kocky. Večery často spestrujú vystúpenia brušnej tanečnice, výstavy obrazov alebo fotografií.	  (oba) 
 Ako pripraviť Yogi čaj 
 Tento čaj si v podniku Lilith  vyrábajú sami. Čaj sa skladá  z kardamónu, klinčekov, škorice, čierneho korenia a zázvoru. Všetky ingrediencie sa zomelú na mlynčeku a dajú sa variť s dvomi decilitrami vody. Po zovretí sa pridá polievková lyžica cukru a čierny čaj z Assamu. Po jednej, dvoch minútach sa musia pridať ešte dva decilitre mlieka. Výsledkom je Yogi čaj, ktorý má  sladko-korenistú chuť. 
 Čajovňa Kelion, Líščie údolie 138 
 Otváracie hodiny: utorok - nedeľa 16.00 - 22.00 
 Malá čajovňa s kapacitou 45 osôb, ktorej nenápadný vchod musia ľudia v Líščom údolí hľadať trochu dlhšie, má v ponuke 75 druhov čajov. Môže sa  popýšiť aj  čínskym čajom pod názvom Tygrí prameň. 
 Okrem čajov predávajú aj sladké bábovky a toasty. Toastový rekord doposiaľ držia dvaja zákazníci, ktorí za jedno posedenie dokázali skonzumovať až dvadsaťjeden zapekaných pochúťok. 
 Interiér Kelionu tvorí viacero menších miestností a aj teraz v zime sa dá  posedieť na presklenej terase. Pri tuhých mrazoch hostí zohreje funkčná piecka (na snímke) a zamilované páry si môžu horúci nápoj naliať aj zo špeciálnych kanvičiek pre dvoch. 
 Ako pripraviť Gyokuro 
 Jeden z najdrahších čajov sveta pochádza z Japonska a volá sa Gyokuro. Kríky sú pri dozrievaní zatienené slamenými  rohožami a jeho lístky sa zbierajú ručne. Japonci ho pijú len pri slávnostných príležitostiach. Čaj má povzbudzujúci účinok a obsahuje množstvo minerálov. 	 (mad) 
 Čajovňa My a mama, Škultétyho 5 
 Otváracie hodiny: pondelok - nedeľa 16.00 - 22.00 
 Útulná obývačková čajovňa pre 40  ľudí My a mama sa nachádza v budove Radošinského naivného divadla. Ako na návšteve alebo doma, vo vstupnej miestnosti si návštevníci vyzúvajú topánky. V čajovni je na výber asi 80 druhov čajov, medzi nimi juhoafrické, juhoamerické, ajurvédske a aj zopár vlastných receptov. Súčasťou čajovne je aj predajňa. 	 (dot) 
 Ako  pripraviť Tie Guan Yin 
 Tie Guan Yin z triedy Oolong je jeden z najslávnejších čínskych čajov, v preklade znamená Železná bohyňa milosrdenstva. Patrí pod polozelené čaje, ľahko chutí po karamele a vonia sviežou orchideou.  Lístky sa zalievajú horúcou vodou, ktorá má maximálne 80 stupňov, nechá sa 4 - 5 minút  vylúhovať. Nie je potrebné ho sladiť. Pre dokonalý pôžitok z čaju sa odporúča používať špeciálnu čínsku keramiku  polokameninu Yixing. Nie je glazovaná, a pritom neprepúšťa vodu. Využíva sa pri čínskom a taiwanskom čajovom obrade Kung Fu Cha. Konvička sa tradične používa iba na jeden druh, po ktorom po čase začne  voňať. 
   

