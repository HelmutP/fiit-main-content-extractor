
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Iveta Durilová
                                        &gt;
                O mne
                     
                 Moja internetová samovražda 

        
            
                                    15.6.2010
            o
            8:37
                        (upravené
                24.10.2014
                o
                19:57)
                        |
            Karma článku:
                9.35
            |
            Prečítané 
            3831-krát
                    
         
     
         
             

                 
                    Začalo to nenápadne. Príchod počítačov do našich každodenných životov, založenie prvej emailovej adresy, prvé chatovanie na chatoch, ktoré v súčasnosti už ani neexistujú. Xchat, pokec, msn, icq, skype, twitter, facebook... Zriadenie druhej, serióznej emailovej adresy pre formálne účely v tvare meno.priezvisko@...com. Založenie sekundárneho nicku na pokeci, veď človek sa potrebuje sem - tam vykecať aj anonymne, bez fotky, ale zato s dokonalým profilom 174 centimetrovej blondínky s mušou váhou, študujúcou minimálne právo, resp. ekonómiu, ktorá vo voľnom čase ešte stíha pracovať, venovať sa jachtingu a bez nároku na honorár pracuje s postihnutými deťmi. (Rozumej zistiť, ako sa má bývalý bez toho, aby on zistil, že si to o ňom zisťujem). 
                 

                 Popridávanie si všetkých bývalých, súčasných a budúcich priateľov, spolužiakov a kolegov do priateľov. Písanie si statusov, ich komentovanie, chatovanie aj s tými, na ktorých momentálne nemám chuť, pridávanie fotiek a ich okomentovanie, nachádzanie vlastných fotiek na sieti, ktoré mali ostať zabudnuté, prezeranie albumov, statusov, profilov, diskusií, skupín atď., čakanie NA NEHO, kým bude online...   „Heading to Thassos / Smerujúc na Thassos“ - znel môj status na všetkých fórach. More, teplo, slnko, piesok... a internet za 3 eurá na hodinu. Čo však človek neurobí pre priateľov a hlavne pre lásku. Ležiac na pláži som rozmýšľala o tom, aký status si napíšem, akú fotku spraviť, aby sa dobre vynímala v profile, aký komentár napíšem... Spočiatku som si myslela, že to grécke slnko mi nerobí dobre. Ale počas prázdnin, keď bolo voľného času viac ako som chcela, to začalo znova. Ráno, prebudenie, toaleta, raňajky, zapnutie pc. Skontrolovanie mailových schránok, statusov, internetových fór, priateľov, fotiek, albumov, „komentov“... Veď čo ak sa za tých 8 hodín, čo som spala, stalo niečo významné? Tu sa ozval kamoš, kamarátka zo zahraničia bola po dlhšom čase online, spolužiak pridal fotky zo včerajšej akcie, Jana má zase depresiu, Maja rieši rozchod s Lacim, spolužiačka potrebuje materiály na skúšku, Katka odlieta do USA...   Najprv padol za obeť sekundárny nick na pokeci. Je predsa škaredé, keď človek zavádza ostatných. Potom neformálny mail – načo mi je? Msn, icq – veď ostávam na ostatných fórach. Pri pokeci som takmer vyronila slzičku – koľko hodín písania o všetkom možnom aj nemožnom a koľko premrhaného života, ktorý som mohla využiť trebárs na tú dobrovoľnícku prácu s postihnutými deťmi. Smska typu „Diky ze si si ma vyhodila z priatelov“... Skype som používala výhradne na komunikáciu zo zahraničia a keďže sedím poctivo na zadku doma už niekoľko mesiacov – takisto nevyužívam. Facebook – kapitola sama o sebe. Fotky, albumy, ale predovšetkým priatelia z celého sveta, s ktorými som strávila tie najbezstarostnejšie mesiace počas študijného pobytu v zahraničí... Janko sa uci na statnice, Janko je zufaly, Janko to zajtra dá! Janko je INZINIER!!!!! Ing. Janko to vecer ide zapit, Ing. Janko to zapija, Ing. Janko sa nechce zajtra rano zobudit..., Ingg. Janko je po obravskej opici, ale supier bolo, Janko objima zachodovu misu... Janko si umyva zuby, aj s dentalnou nitou. Nastastie ma iba 28 zubov...   Ing. Janko to rozhodol za mňa. Hromadný mail priateľom, že ruším kontá, ostávam na maily (kde je možný aj chat) a môžu mi kedykoľvek zavolať, resp. stretnúť sa osobne. Tak!   Už niekoľko mesiacov mám pokoj. Zapnem si internet, prezriem maily, kde nič – tam nič a môžem vypnúť počítač.   Ale – ušila som si na seba búdu: zriadila som si vlastný blog. Prvý uverejnený článok, prvé reakcie... karma 6,13? Ešte neviem, čo to je, ale znie to dobre. Sledujem nových autorov, prezerám si ich články, fotky, komentáre, diskusie, zoznamy rubrík, čítam najlepších autorov, píšem ďalší článok...   Žeby moja internetová samovražda neskončila úspešne?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Durilová 
                                        
                                            Kroky k dospelosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Durilová 
                                        
                                            50 eurovník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Durilová 
                                        
                                            Starostlivá mama
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Durilová 
                                        
                                            Ella
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Durilová 
                                        
                                            Úroveň služieb na Slovensku alebo zázraky sa ešte dejú
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Iveta Durilová
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Iveta Durilová
            
         
        durilova.blog.sme.sk (rss)
         
                                     
     
         ...že vraj keď Google niečo nevie, tak sa opýta mňa... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2134
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            O iných
                        
                     
                                     
                        
                            O mne
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




