
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s medveďom - Sviečková na smotane 

        
            
                                    6.1.2008
            o
            12:00
                        |
            Karma článku:
                10.50
            |
            Prečítané 
            101015-krát
                    
         
     
         
             

                 
                    Milí moji.:)  Tento môj kuchársky výtvor pochádza ešte z medzisviatkového obdobia. Je ostatne fakt, že už dlhodobo plánujem uvariť, nafotiť a dať na blog dosť dobrú staročeskú sviečkovú na smotane.  Nechcem sa nijako dotknúť našich maminiek, veď ostatne ich sviečková býva skvelá, ale varenie hovädzieho mäsa vo vode s koreňovou zeleninou jednoducho neposkytne tomuto skvostnému jedlu rozmer, ktorý mu prináleží.
                 

                  Receptov na sviečkovú máme na nete bez počet. Používajú sa v nich borievky, červené víno, či rôzdne bylinky. Ja som však, čo sa týka tradičných jedál tak trochu 'skostnatený'. Uprednostňujem klasiku.     Potrebujeme:     1,5 kg falošnej sviečkovice, alebo rovnaké množstvo pekného hovädzieho stehna. Dôvod, pre ktorý nepoužívame pravú sviečkovú je snáď každému jasný. Ostatne nádherná falošná sviečková na našom zábere  stála tiež slušnú sumičku. Ďalej potrebujeme cca 10-15 dkg peknej prerastenej údenej slaniny, 2-3 mrkvy, 2 petržleny, poriadny kúsok zeleru (...hnevám sa, keď sa táto skvelá zelenina ignoruje:)), menšiu cibuľu, olej, ocot, sladkú šľahačkovú, ale aj kyslú smotanu, 2-3 lyžice hladkej múky a 3-4 lyžice kryštálového cukru. Ďalej potrebujeme zaváraný brusnicový kompót, alebo džem.     Ochucovať  budeme celým novým korením, bobkovým listom, mletým čiernym korením, vegetou a plnotučnou horčicou, prípadne cukrom a samozrejme vyššie uvedeným octom. Napriek menšej bohatosti korenín môžeme docieliť   skvelú chuť a vôňu, veď ostatné použité ingrediencie sú exkluzívne.          Najprv sa budeme venovať mäsu. Je potrebné prešpikovať ho slaninou. Nie je to žiadne strigônstvo, ako by sa mohlo zdať. Zabudnite však na teórie o tom, že mäso treba prepichnúť špikovacím nožom a popri ňom vtlačiť špikovanú ingredienciu. Najprv slaninu zbavíme kože a nakrájame ju na dlhšie hranolčeky 3/4 cm x 3/4cm na hrúbku a dlhé na výšku slaniny. Škoda, že som neurobil záber.  A vezmeme proste menší nôž, prepichneme mäso poriadne do hĺbky a ešte ním pohýbeme zo strany na stranu, vytiahneme ho a ukazovákom otvor rozšírime. Potom do otvoru vtlačíme hranolček slaniny. Diletantské, ale účinné.          Následne očistíme koreňovú zeleninu, cibuľu a všetko nakrájame na väčšie kocky. Prešpikované mäso zo všetkých strán potrieme horčicou. Na panvici zohrejeme olej na vysoký stupeň a mäso zo všetkých strán prudko opražíme. Bude nám to prskať, preto si vyberme vhodne dlhé nástroje k prevracaniu mäsa. Nemali by sme ho však prepichovať.          Následne mäso posolíme zo všetkých strán, položíme ho do pekáča a zalejeme výpekom z panvice.            Tak a prichádza na rad karamel:     Vezmeme  šikovnú rajničku a položíme ju na slabší plameň. Nasypeme 3-4 lyžice kryštálového cukru a miešame do zhnednutia. Pozor, karamel by nám nemal zhorieť. Mal by ostať priezračný.  Následne vlejeme 0,5l vody.  Bude to syčať a prskať, ale nám to nevadí. Sme odvážni a neohrození.:) Necháme skaramelizovaný cukor  vo vode na plameni rozpustiť.     Ak sa bojíme, že sa nám karamel spáli, je vhodnejšie hneď na začiatku do cukru pridať 0,5 dcl vody. Proces sa predĺži, ale karamel nezhorí.               Výpekom zaliate mäso v pekáči obložíme s na kocky nakrájanou koreňovou zeleninou, pridáme na dve polovice prekrojenú očistenú cibuľu, zo 5 bobkových listov, 8-10 gulôčok nového korenia, všetko dobre posypeme vegetou (podporuje chuť koreňovej zeleniny) a soľou. Zalejeme karamelovou vodou, dvomi lyžicami octu a prikryjeme. Ak nemáme druhú časť pekáča, tak použijeme alobal.          Zakrytý pekáč vložíme do silne vyhriatej rúry a po 20 minútach pečenia plameň stiahneme, pri plynových rúrach na polovicu, pri elektrických ešte menej.  Necháme hodinu dusiť a potom len prekontrolujeme, či sa mäso neprilepilo k pekáču, prípadne prilejeme dostatok vody. Znovu zakryjeme a pomaly dusíme ďalšiu 1 až 1,5 hodiny.     Výsledok je nižšie.          Koreňovú zeleninu a výpek vlejeme do väčšieho hrnca. Odstránime pri tom 'bobkáč' aj celé nové korenie. Samozrejme môžeme zeleninu pretlačiť cez sitko, ale ponorný mixér je super. Všetko spolu rozmixujeme.          Následne vlejeme do zeleninovej zmesi asi 1l horúcej vody a omáčku necháme prevariť.     V tom čase si namiešame zátrepku. Do zmesi šľahačkovej a kyslej smotany nasypeme 2-3 lyžice hladkej múky, poriadne rozmiešame a vlejeme ju do omáčky. Zároveň skvelé, fantastické a hlavne krehké slaninkou prešpikované mäso nakrájame na tenké plátky.          Zátrepku vlejeme do omáčky a necháme aspoň 10 minút prevariť. Omáčku dochutíme podľa potreby soľou, cukrom, octom, mletým čiernym korením a horčicou. Správny pomer už sa naučiť nedá... Je to individuálne.          Niekto nechá nakrájané mäso vonku, mimo omáčky, ale ja po dokončení varu a dochutení omáčky mäso vraciam späť. Nechcem aby zbytočne vysychalo.     Tak a nižšie je prezentácia. Na tanier dáme zohriate a nadýchané kysnuté knedle. Môžu byť len čisto kysnuté, alebo žemľové kysnuté knedle. Zalejeme skvelou omáčkou, pridáme na plátky nakrájané mäso a naň brusnicový kompót.         Naša milá návšteva bola nadšená. A všetky lichôtky a chvály som si skvele  užil. A bolo ich neúrekom...      Dobrú chuť.     tenjeho        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (84)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




