

 Miestne rekreačné stredisko je známe aj ako osada Byšta-Kúpele a od obce je vzdialené asi 1,5 km. Vybudované bolo niekedy v 60.-tych alebo 70.-tych rokoch 20. storočia ako areál určený pre všestrannú letnú rekreáciu. Je priam ukážkovým príkladom stereotypnej predstavy o tom, ako má vyzerať rekreačné stredisko miestneho alebo regionálneho významu postavené za čias socializmu.  Byštianske kúpele majú kvôli ére svojho vzniku ďaleko od romantických vidieckych kúpeľov z čias c.-k. monarchie alebo prvej republiky. Ich charakter je neúprosne súčasnejší, zemitejší... a tak trocha trampskejší... :-) Navyše, sú doslova mrňavé - tvoria ich v podstate len dva liečebné domy. Čo sa niekdajšieho liečebného zamerania týka - liečili sa tu žalúdočné, reumatické a cievne ochorenia a ischias. Kúra pozostávala z využívania slano-zásaditých minerálnych prameňov na vaňové kúpele. 
 Zvyšok pomerne rozľahlého areálu zaberá motorest ("vstupná brána" s pustnúcim parkoviskom), termálne kúpalisko s dvomi-tromi bazénmi (zďaleka najväčšia atrakcia), súkromné aj podnikové chaty, malý hotel so superoriginálnym názvom Minerál, rôzne bufety a stánky, parková zeleň... a obecný rybník. :-) Vznikol prehradením Byštianskeho potoka (prítoku riečky Roňava) a má rozlohu približne 4 ha. Je nemožné si ho nevšimnúť už z diaľky počas cesty k areálu : 
  
 Popoludnie, Slnko neúprosne praží, tráva a stromy šumia... 
  
 ...na poli usilovne vrčí a rapoce kombajn... 
  
 ...a kovové "mólo" s vodomerom a regulátorom prietoku naďalej ticho oxiduje... 
 Prechádzka po hrádzi : 
  
  
  
   
 Červená bodka na hladine je výstražná bója pre plavcov : 
  
 Obligátne rozložené stany výletníkov : 
  
  
 Samotná Byšta učupená pod horským masívom : 
  
   
 Tŕstie a hladina sa spolu ticho vlnia do rytmu : 
  
 Jedinou doteraz fungujúcou verejnou budovou v areáli je kríženec reštiky a motorestu : 
  
  
 Meno motorestu si síce nepamätám (má vôbec nejaké ?), ale ponuka je solídna, takže určite neodídete odtiaľ hladní, smädní alebo priotrávení... :-) 
  
 Terasa... 
  
 ...je zastrešená a využívaná len z polovice... 
  
  
  
 Pomaly korodujúca tabuľa s orientačným plánom. 
  
 Zlaté časy sú preč... Bufet Oáza však pôsobí, akoby ho zavreli len včera... 
   
 Nechcene tragikomicky pôsobí aj vstupná tabuľa kedysi preplneného kúpaliska... 
  
 Pri pohľade na spustnuté časti areálu, na ktoré akoby človek zabudol a začala sa ich znova zmocňovať príroda, ma nečakane napadá jedna výstižná asociácia : Pripjať...  
  
 Okolitá "promenáda" a dlažba bazéna začínajú pripomínať menšiu lúčku... Neďaleko odtiaľ je menší a novší bazén, ktorý som nefotil. Má bezchybnú zámkovú dlažbu a nerezové zábradlia pre vstup. Oboje  úplne nové a nikdy nepoužité... 
 Čo sa vlastne s celým strediskom stalo ? Nuž, je to dlhý príbeh, ale podobný všetkým ostatným príbehom o krachu malých dovolenkových zariadení v 90.-tych rokoch. Za úpadok a uzavretie väčšiny areálu mohlo viacero faktorov : Veľké zadĺženie sa obce začiatkom 90. rokov, neustále odkupovanie strediska do nových rúk a neochota nových vlastníkov doriešiť vzniknuté problémy a ďalej využívať nadobudnutý majetok. Nechce sa mi to ďalej rozpitvávať (aj tak ma to nezaujíma a ani nie som miestny) - o celej kauze si môžete prečítať v tomto novinovom článku. 
 Nech už bol osud väčšiny areálu akýkoľvek, chatám a chatárom sa tu naďalej bez problémov darí : 
  
  
  
 Marča... :-) 
  
 Niektoré chaty boli v posledných rokoch renovované a upravované ich majiteľmi... 
  
  
  
 ...a niekde vyrástli aj celkom nové chaty a víkendové chalúpky : 
   
 A každý rok pribudne aspoň jedna... Byštianske kúpele snáď prežijú aspoň ako chatová osada... :-) 
  
 Ako som sa tak poberal späť do dediny, padla mi do oka táto jarabina, po maďarsky berkenye (vysl. "berkeňe"). Po pár rokoch nezáujmu "zdivela". Ako všetko ostatné naokolo...  
 ---- 
 Fotografie : (C) Peter Molnár - august 2008 
 Nejaké dodatočné informácie o Kúpeľoch Byšta nájdete aj tu, tu a tu. 
 Vyzerá to tak, že už aj majitelia motorestu začínajú uvažovať o jeho predaji. 
   

