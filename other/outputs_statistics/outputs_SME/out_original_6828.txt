
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kobelák
                                        &gt;
                Nezaradené
                     
                 Tri príbehy z pôrodnice 

        
            
                                    19.5.2010
            o
            17:43
                        (upravené
                22.5.2010
                o
                20:58)
                        |
            Karma článku:
                21.96
            |
            Prečítané 
            9538-krát
                    
         
     
         
             

                 
                    Pacient: Dať, či nedať = byť, či nebyť (opatrený a zdravý). Lekár: Brať, či nebrať = mať, či nemať (primeraný štandart).
                 

                 Hlavným hrdinom nasledujúcich príbehov je obálka, ktorá by v normálne fungujúcej spoločnosti nesmela dostať ani tú najmenšiu, najlepšie žiadnu úlohu. Žiaľ, stav nášho nezreformovateľného zdravotníctva, zdá sa však, so šedou ekonomikou priam počíta: nedokáže zaistiť každému pacientovi náležitú starostlivosť a nemá na adekvátne platy zdravotníckeho personálu.   Jedným z hlavných dôvodov kolovania obálok v tomto systéme, okrem vyššie uvedeného, je strach. Strach o svoje zdravie, strach o svojich najbližších, strach, že sa odkázanému nedostane náležitej starostlivosti a bezodkladnej pomoci, najmä pri diagnózach a stavoch, kde zaváhanie z akéhokoľvek dôvodu by mohlo mať fatálne následky. Ďalším dôvodom je to, že niektorí lekári si, či už priamo alebo sofistikovanejšie, "sponzorské" vyžadujú. Nie každý lekár má morálne zábrany a dokáže sa uspokojiť s tým, čo má, najmä, ak sa nemusí  pri takomto vedľajšom príjme ani veľmi namáhať. Chce to len silný žalúdok. Neposledným, spoločensky pri súčasnom stave zdravotníctva, akceptovateteľným dôvodom je vďačnosť.  Vďačnosť za ľudský prístup, za príkladnú starostlivosť, za kvalitný odborný výkon zdravotníkov, za záchranu života.       Príbeh prvý: Date et dabitur vobis   No, mamička, vyzerá to tak, že potom, čo spolu privedieme na svet vaše dieťatko, obaja pôjdeme na dovolenku. Vy na materskú a ja si tiež oddýchnem od tohoto blázinca, aj keď náš ústav sa volá pôrodnica - povedal lekár mojej dcére, keď jej určil dátum pôrodu prvého dieťaťa. Trošku sa prerátal, dcéra vypočítaný termín  pretiahla. Keď sa dostala do nemocnice, pán doktor už na tej dovolenke bol, predbehol ju. Na dcérino požiadanie však k pôrodu prišiel, bol dostihnuteľný. Našťastie. Pôrod neprebiehal tak, ako by mal, niečo nebolo úplne v poriadku. Keď dcére zrazu prestali kontrakcie, pán doktor vykázal zo sály budúceho otecka (ten sa veľmi tešil, že bude v tom krásnom okamihu so svojimi, že to zdokumentuje, no o to bol radšej, keď už šťastne bolo po všetkom) a rozhodol sa ihneď riešiť pôrod chirurgicky. Ukázalo sa, že sa rozhodol správne a včas. Dieťatko bolo zamotané v pupočnej šnúre. Všetko dobre dopadlo, vďaka kvalitnému odbornému zásahu lekára sa dcéra aj vnučka vyhli možným komplikáciám. Keď dcéra vzala do úvahy aj dôslednú a nepretržitú starostlivosť lekára o dieťa i o ňu aj počas tehotenstva, rozhodla sa ho odmeniť.  Keďže veľa toho dostala, nemala problémy s tým, že tiež niečo dala.       Príbeh druhý: Nomen est omen   Keď sme sa dozvedeli (telefonicky), že budeme starými rodičmi, mladí nám chceli  našu radosť ešte znásobiť: keď prídu do Košíc, prvýkrát nám ukážu vnučku ešte v brušku našej dcéry pomocou 3D vyšetrenia.   Budúca babka v predstihu vybavila u lekára, ako sa neskôr ukázalo s výstižným menom, potrebné vyšetrenie. Natešená babka, spolu s mladými, sa v dohodnutý čas (vo večerných hodinách) dostavila do pôrodnice fakultnej nemocnice. Počas poldruhej hodiny čakania na lekára, prišli na podobné vyšetrenie ďalšie dve budúce mamičky. Ten svoje meškanie zahral do autu inými povinnosťami. Počas vyšetrenia sa žoviálny pán doktor prítomných okrem iného povypytoval aj na ich zamestnanie a hmotné zázemie. Keď sa dozvedel, že budúca babka je podnikateľka, pravdepodobne podľa toho stanovil cenu vyšetrenia. Keďže suma bola 5x vyššia, než akú dcéra zaplatila za totožné vyšetrenie u svojho gynekológa, požiadala lekára o vystavenie potvrdenia. Nato, v súlade so svojim menom, sa pán doktor rozprávka.mi snažil prítomných presvedčiť, že v takúto neskorú hodinu už tu nik nie je, nemá sa ako k bločkom dostať, teda ho nemôže vystaviť. Urazený pán doktor si ešte neodpustil uštipačnú poznámku na adresu budúcej babky, že si nevie predstaviť, ako by si to vyšetrenie mohla dať do nákladov. To nemá chybu. Tak pán doktor v štátnej nemocnici, na štátnych prístrojoch zarába do svojho súkromného vrecka. Zneužíva neopakovateľnosť chvíle prvého pohľadu na ešte nenarodené dieťa, počíta s tým, že človek v tej radosti, eufórii, bude súhlasiť s cenou vyšetrenia a prehliadne nevydanie účtenky. Bez svedkov, vo večerných hodinách zarobí za dva-tri dni viac, ako v službe za celý mesiac. Má to dobre vymyslené, ale aj rozprávka raz narazí a vezme za koniec.       Príbeh tretí: Error corrigitur, hic et nunc   Na pôrodníckom oddelení nemocnice čaká na prepustenie mladá rómka, prvorodička. Leží na štvorke medzi bielymi. Sama sa sem pýtala, nechcela byť medzi špinavými rumungurkami, ona ja olašská Cigánka,  zdôrazňovala. Nevie sa dočkať, kedy už pre ňu a malého prídu. Každú chvíľu, už od rána, do izby nakukuje lekár, ktorý bol pri jej pôrode, ale od vtedy sa tu neukázal. Možno ešte netrpezlivejšie ako prvorodička čaká na príchod jej príbuzných. Nechce zmeškať svoju odmenu. Na nemocničnom dvore zaparkuje mercedes, za ním niekoľko áut podobných značiek. Rómka sa poteší, doktor vybehne príchodzím oproti a začne im vykladať, že matka aj dieťa sú v poriadku a bolo o nich dobre postarané. Tí si ho veľmi nevšímajú, nahrnú sa do izby a všetci sa venujú malému (možno budúcemu vajdovi). Pri prvorodičke sa zastaví iba jej matka, pomáha jej baliť. Keďže doktorovi nikto nevenuje pozornosť, ten sa upriami na hlavu rodiny a upozorňuje ho na svoje zásluhy pri v podstate bezproblémovom pôrode. Hlava rodiny kývne na zaťa, novopečeného otca, ten vytiahne z aktovky bonboniéru a podá ju lekárovi. Doktor neskrýva v tvári svoje sklamanie, nakoniec sa neovládne a upozorní otca dieťaťa, že tu to nechodí tak, ako keď príde k lekárovi s nádchou, tu predsa ide o život, tu sa nosí obálka. Hlava rodiny zasiahne: prepáčte, pán doktor, my sme len hlúpi Cigáni, hneď to napravím, poradte, koľko dať do tej obálky. Mohli by to byť tri stovky, ukľudnil sa doktor. No vidíte, vy ste študovaný, vy ste doktor, vy to viete najlepšie, kontruje hlava rodiny. Vypýta si späť bonbonieru, otvorí ju, je v nej obálka. Vyberie z nej zväzok bankoviek, odpočíta z neho tri stovky, vráti ich do obálky a podá ju spolu s bonbonierou lekárovi. Dúfam, že teraz je to už v poriadku, dodá. Doktor najprv skamenie, potom zozelenie a potom v purpurovej fáze vyletí z miestnosti. Všetky mladé mamičky, schované pod prikrývkami, sa zadúšajú od smiechu.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (69)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Nad Tatrou sa blýska, nie je pôvodná slovenská hymna!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Bezzubý sex :)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Ak máte deti, tak poznáte odpoveď
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            A čo vy na to, pán Márai: o vlasti a štáte
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            A čo vy na to, pán Márai: o  nás a našich ženách v ich sviatok
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kobelák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kobelák
            
         
        kobelak.blog.sme.sk (rss)
         
                        VIP
                             
     
        Väčšinou som v menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5149
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            NEGEV
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nad Tatrou sa blýska, nie je pôvodná slovenská hymna!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




