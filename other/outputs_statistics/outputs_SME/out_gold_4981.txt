

    
 Dnes už viem, 
 všetko je ako má byť, 
 vonku nám jar rozkvitá 
 a našou ostrou túžbou žiť 
 je do sveta nádej vyrytá. 
   
 Myslím, 
 je to asi všetko správne 
 a veci dávno dávne 
 nám pripomína už len fotka v múre zabitá. 
   
 Pamätám sa na tú chvíľu, 
 kedy dával si mi silu 
 žiť bez omylu. 
   
 Nám sa však rozchádzajú svety. 
 Nesedia hlásky, líšia sa slová, bijú sa vety. 
   
 No dnes už viem, 
 všetko je ako má byť. 
 Čas začína odznova. 
 Ty sám a ja sama musíme ísť 
 a ja konečne žijem doslova. 
   
 Verím, 
 dobre je to tak, 
 z nášho vzťahu síce ostal vrak, 
 no posádka po záchrane užíva si krásy tropického ostrova. 
   
 Pamätám sa na tú chíľu, 
 kedy dával si mi silu 
 žiť bez omylu. 
   
 Nám sa však rozchádzajú svety, 
 Už nie je chvíľ, v ktorých by sme sa stretli... 
 Ale predsa.... 
   
 Všetko je ako má byť. 
   

