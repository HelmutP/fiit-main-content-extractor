

 K výsledkom volieb v ČR som sa vyjadril vo svojom predchádzajúcom článku. Aj napriek tomu, že formálny víťaz volieb ČSSD na čele s novým predsedom Bohuslavom Sobotkom žiada prezidenta V. Klausa o pokus zostaviť vládu, nemá reálnu šancu. Keď sa mu to nepodarí, ministerstvá obsadí stredopravá vláda troch strán na čele s ODS. 
 Moja predpoveď rozdelenia sedemnástich ministerstiev a premiéra je nasledujúca: 
 Česká vláda bude v pomernom zložení 7-6-4, čo mi vychádza aj z výsledkov volieb a percent jednotlivých strán. (20/16/10) 
   
 1. Premiér- ODS- Petr Nečas 
 - aj strany potvrdili, že toto je bez diskusie 
   
 2. Minister zahraničných vecí- TOP 09- Karel Schwarzenberg 
 - hoci ODS bude pravdepodobne chcieť presadiť Petra Vondru 
   
 3. Minister obrany- ODS- Petr Vondra 
 - keď nezíska kreslo ministra zahraničia 
   
 4. Minister financií- TOP 09- Miroslav Kalousek 
 - ODS má záujem zjednotiť post ministra financií s postom premiéra, o toto ministerstvo očakávam spor 
 - možný kandidát ODS je Martin Kocourek 
   
 5. Minister pre európske záležitosti- ??? 
 - netrúfam si odhadnúť, pravdepodobne však z ODS 
   
 6. Minister vnútra- VV- Veci verejné majú o toto miesto evidentný záujem, avšak netuším koho na toto miesto budú chcieť dosadiť 
 - s obsadením tohto kresla zo strany VV nebudú súhlasiť obe zvyšné strany,  opäť možným kandidátom Petr Vondra z ODS 
 - VV majú záujem presadiť zjednotenie ministerstva vnútra a obrany 
   
 7. Minister priemyslu a obchodu- TOP 09- Jaromír Drábek 
   
 8. Minister spravodlivosti- TOP 09- Vlasta Parkanová 
 - napriek niektorým zaujímavým počinom (naspievanie CD pre G.W. Busha, ako oslavu amerického radaru v čechách) je to pre tento post favorit 
   
 9. Minister sociálnych vecí- ODS- Miroslava Němcová 
 - alebo Daniela Filipiová 
   
 10. Minister dopravy- ODS- Petr Bendl 
   
 11. Minister poľnohospodárstva- ODS- Pavel Drobil 
 - možné presadenie spojenia ministerstiev ŽP, poľnohospodárstva a miestneho rozvoja, v prípade že by sa tak stalo, predpokladám na tomto mieste Miroslava Kalouska (keď sa nestane ministrom financií) 
   
 12. Minister zdravotníctva- VV- Martin Jan Stránský 
 - odborník, žiadna strana nebude mať námietky 
   
 13. Minister školstva, mládeže a telovýchovy- ODS- Jiří Pospíšil 
   
 14. Minister životného prostredia- ODS- David Vodrážka 
 - možnosť zjednotenia 
   
 15. Minister kultúry- VV- Radek John 
 - Radek John je publicista a spisovateľ 
   
 16. Minister pre miestny rozvoj- TOP 09- Karel Tureček 
 - možnosť zjednotenia 
   
 17. Ministerstvo pre ľudské práva- VV-  ??? 
 - predpokladám toto ministerstvo v rukách VV , netrúfam si však tipovať presnú osobu 
   
 18.Minister a predseda Legislatívnej rady vlády- ??? 
 - je to poradný orgán vlády v oblasti legislatívnej činnosti 
   
 Hoci má táto budúca trojkoalícia množstvo programových prienikov, očakávam veľký boj a tvrdé rokovania o kreslá, najmä financií, zahraničia a vnútra. 
 Tieto riadky sú len môj odhad. Môže to celé dopadnúť úplne inak, keďže za scénu nevidíme a len jednotliví predstavitelia rokujúcich strán budú vedieť, kto sa prečo ako rozhodol. 

