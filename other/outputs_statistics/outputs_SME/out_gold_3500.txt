

 SRDCE  
   
   
 A čo teraz,  
 Opýtaj sa srdce samo seba. 
   
 Nájdi si vhodný argument,  
 Ako vysvetliť  
 Pôvod hier s ohňom,  
 S naivným chladným  
 Počasím v myšlienkach. 
   
   
 Tak, čo vieš už odpoveď,  
 Máš logiku,  
 Alebo zahráš citlivku,  
 Ako vždy, keď horia  
 Aj posledné stromy  
 V nehostinnej intimite  
 Lesíkov. 
   
   
 Nechaj ma hádať,  
 Opäť mozog  
   
 Zvíťazí ?! 
   
 MOŽNO UŽ NIE SI  
   
   
 
    

 Si opustená,  
 Plačeš na vtáčom  
 Krídle. 
   
 Slzy máš sladké  
 A rany sú neživé. 
   
   
 Nevnímaš svet,  
 Ani okoloidúce  
 Svetlo.  
 Ani chladné teplo. 
   
   
 Si odovzdaná  
 Vydaná napospas  
 Osudu. 
   
   
 Možno už nie si.  
   
 LOŽ 
   
   
 Upadáš,  
 Si odhodená socha bez hlavy,  
 Nenamaľovali ti ani oči maliari,  
 Klesla si na dno. 
   
   
 Škoda rečí, 
 Sviečka ti horí, ale nemá  
 Knôt. 
   
   
 Ale čo ťa urážam,  
 Teba to neraní ani nezlomí,  
 Dávno si v mysliach  
 Zanikla. 
   
   
 Mávam Ti z diaľky,  
 Zo stanice pravdy právd.  
 Zbohom egoistka. 
   
   
 Rečou som odkopol  
   
 Lož. 
   
 LOTÉRIA S CHARIZMOU  
   
   
 Vyhral som lotériu,  
 Mizéria skončila,  
 Paráda,  
 Ľahnem si na chrbát,  
 Hlavou do trávy 
 A začnem počúvať, 
   
   
 Všetkých, čo majú  
 Problémy  
 A hádzať na nich  
 Pohľady  
 Novodobého ničomníka  
 S charizmou v ušiach. 
   
   
   
   
   
   
   
   

   
 
   

