

 Bože, nadovšetko milujem Slovensko. Milujem túto krajinu, históriu  aj ľudí. Dal by som za to všetko, aj vlastnú dušu. Je mi veľmi smutno, keď vidím kam smerujeme. Naša krajina je  plná nenávisti, agresivity a predsudkov. 
 Bože, ja viem, že veľa ľudí na Slovensku sa správa pokrytecky a neľudsky. Viem, že pomaly spejeme do záhuby.  Viem, že nádej sa pomaly vzďaľuje.  Viem, že na toto obdobie v budúcnosti nebudeme pyšní. Aj tak sa nevzdávam a ďalej dúfam. 
 Bože, prosím nedopusť, aby to ďalej pokračovalo. Nedopusť, aby nám opäť vládli zlodeji, grázli a klamári. Nedopusť, aby sa  raz naše deti museli hanbiť za svojich rodičov. 
 Bože, ja viem, že si to ľudia na Slovensku neuvedomujú. Väčšina z nich je hlboko vo svojom vnútri dobrá a spravodlivá.  Títo ľudia sú len v zajatí klamstiev a nenávisti. 
 Bože, nežiadam pre Slovensko pohodlný život a ani trináste dôchodky. Spravodlivý a poctivý človek sa dokáže aj nešťastím úspešne prebiť.  Prosím ťa len o jedno, dopraj nám  krajinu, kde bude vládnuť právo , spravodlivosť a úprimnosť. Miesto, kde si každý bude  môcť hľadať vlastné šťastie... 
   

