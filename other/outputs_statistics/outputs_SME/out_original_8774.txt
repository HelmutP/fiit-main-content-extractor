
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                Povodne
                     
                 Príčina bleskovej povodne na rieke Poprad 

        
            
                                    14.6.2010
            o
            9:15
                        (upravené
                10.7.2010
                o
                23:46)
                        |
            Karma článku:
                14.68
            |
            Prečítané 
            5664-krát
                    
         
     
         
             

                 
                    Pred týždňom sa prehnala kastrofálna povodňová vlna údolím rieky Poprad. Povodňová vlna zaliala Kežmarok, Podolínec, Starú Ľubovňu a množstvo ďalších obcí. Podľa predbežných analýz najvýdatnejšie zrážky sa vyliali do prítokov riečky Ľubica a Jakubovianky, prameniace v Levočských vrchoch. Preto som sa v nedeľu vybral s priateľom Štefanom Vaľom zdiagnostikovať príčinu, čo sa to vlastne stalo a prečo. Vybrali sme si Ľubické údolie a ponúkam Vám smutný pohľad na stav poškodenej krajiny. Hlavná diagnóza: Cesty sa stali počas povodne privádzačmi dažďovej s halúzami a inou biomasou do údolí. Na podmytých stromoch sa splachovaná biomasa zachytávala, upchávala ich a vytvárala malé vodné nádrže pozdlž celého toku. Po ich naplnení sa bumerangové hrádze trhali a spustili katatrostrofálnu povodeň. V údolí Jakubianky sme neboli, avšak je možné konštatovať, že diagnóza je tá istá. Paradoxne porasty rozsiahlych holorubov spôsobené kalamitou z novembra 2004 neboli príčinou povodne.
                 

                 Lesné cesty a svažnice sú privádzačmi dažďovej vody do údolí                     Poškodené porasty po vyťažení dreva nie sú problémové, ak na nich nie sú cesty, či približovacie linky         Každá cesta do údoli je privádzačom dažďovej vody a eroduje ju         Poškodené okraje lesnej cesty koncentrovanou dažďovou vodou      Zlý stav svažnic                                 Vybudovaný priepust nestílal odvádzať dažďovú vodu, ktorá si vytvárala vlastné korytá         Koncentrovanej dažďovej vode neodolí ani spevnená cesta            Stopy po mechanizmoch sa včase dažďa stávajú jarkami      Približovacia linka po vyťažení štyroch strmovov sa stáva tiež zberačom dažďovej vody                  Z najnižšieho bodu svážnice sa prevalila koncentrovaná dažďová voda po svahu a cez porast vytiekla na lesnú cestu               Most sa stal tiež haťou s vytvorením nádrže         Pretrhnutá bumerangová hrádza               Nepochopil som podstatu tobogánu v lesnom prostredí, avšak pochopil som, že rýchla koncentácia pretekajúcej vody cez neho zmietla všetko až na najbližší most, ktorý sa upchal a vytvoril veľké jazero, následne sa pretrhol a spustil ešte väčšiu povodeň.                        Prvý dom v Ľubici, ktorý to schytal.      Ostatné obrázky so zaliateho Kežmarku, Starej Ľubovni poznáte.   PS: Takže príčinou povodne v rieke Poprad je naše nezodpovedné správanie o naše vychodené "chodníčky" v prostredí z ktorého ťažíme zdroje pre naše živobytie. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




