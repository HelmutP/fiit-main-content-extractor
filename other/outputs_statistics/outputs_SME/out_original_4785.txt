
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľuboš Dobrovič
                                        &gt;
                Nezaradené
                     
                 Život a smrť legendy F1 

        
            
                                    18.4.2010
            o
            15:10
                        |
            Karma článku:
                3.65
            |
            Prečítané 
            652-krát
                    
         
     
         
             

                 
                    "Budem jazdiť naplno celý čas... Milujem pretekanie!" - tak ako zneje výrok Gillesa Villeneuva - tak žil a pretekal. Jazdec, ktorý sa nebál rizika a agresívnej jazdy na okruhu za akéhokoľvek počasia, no v súkromnom živote ako to povedal Niki Lauda : "...mimo okruhu bol veľmi citlivý a láskavý. Toto z neho robilo unikátnu ľudskú bytosť."  
                 

                 
  
   Cesta k motošportu       Gilles Villeneuve sa narodil 18. januára 1950 v kanadskom Quebecu. Vzťah k motorom bol viditeľný už od útleho detstva. Ako malé dieťa sa po otcovej stavbe preháňal na nákladnom vozidle. Jeho otec sa o jeho nebojácnosti za volantom "presvedčil" aj vtedy keď mu Gilles vo svojich 15 rokoch zdemoloval Pontiac.       Skútre a Formula Atlantic       Začiatky jeho pretekárskej kariéry sa neviažu k pretekárskym autám, motorkám alebo motokáram ako je u profesionálnych jazdcov F1 zvykom, ale Gilles začínal svoju kariéru na snežných skútroch. Ako sám Gilles neskôr tvrdil, jazda na skútroch bola veľmi náročná, pretože všade bolo veľa rozvíreného snehu a jazdilo sa zle hlavne pre sťaženú viditeľnosť. Táto skúsenosť z neho urobila geniálneho jazdca v daždi. V roku 1973 vyhral práve v pretekoch snežných skútrov celú Svetovú sériu.   V roku 1976 Gilles dominoval v sérii Formula Atlantic s tímom Ecurie Canada. V sérii vyhral 8 z 9 pretekov, pričom na jeden pretek nemohol nastúpiť pre finančné problémy v tíme. No skúseným očiam z Formuly 1 po takejto sezóne uniknúť nemohol.       Začiaky vo Formule 1       Po skvelej sezóne vo Formule Atlantic si Gillesa vyhliadli v McLarene a ponúkli mu pozíciu tretieho jazdca. Prvú možnosť štartovať na veľkej cene dostal vo Veľkej Británii na Silverstone v roku 1977. Na tejto veľkej cene štartoval z 9. pozície a skončil na 11. mieste. V jeho prvej sezóne v F1 to bol jediný štart. V konkurencii s jazdcami ako bol James Hunt a Jochen Mass to mal veľmi tažké, no aj napriek tomu si vytvoril veľmi dobrú reputáciu.   Kedže pre spory so sponzormi nemal Gilles istý štart v sezóne 1978, v auguste 1977 si ho vyhliadol Enzo Ferrari, ktorý ho prirovnal k Taziovi Nuvolarihovi. Jeho kariéra naštartovala po odlúčení Enza Ferrariho s Nikim Laudom. Tento spor otvoril Gillesovi cestu k Ferrari a k pravidelným štartom vo veľkých cenách.       Krátka a úspešná kariéra vo Ferrari       Prvú veľkú cenu za Ferrari odjazdil v októbri 1977 pred domácim publikom v Kanade. Pretek nedokončil pre šmyk na olejovej škvrne. Čierny deň v jeho kariére bola veľká cena v Japonsku, kde po kolízii jeho neovládateľný monopost usmrtil blízko stojacich divákov. Gilles vyhral iba šesť krát. Prvé víťazstvo prišlo na domácej trati v Kanade v roku 1978. O rok neskôr prišlo najlepšie umiestnenie medzi jazdcami. Vybojoval si v celkovom poradí 2. miesto za svojim tímovým kolegom Jodym Scheckterom.       Najpamätnejším okamihom v jeho kariére je určite veľká cena Francúzska 1979. Túto Veľkú cenu nevyhral, no to čo predvádzal by sa dalo nazvať genialitou. V tejto veľkej cene bojoval so slabým monopostom proti dvom Renaultom s turbomotormi. Preteky vyhral Jean-Pierre Jabouille a o druhé miesto viedol Gilles tuhý boj s René Arnouxom. Posledné tri kolá preteku Gilles a René zvádzali tuhé súboje o druhú pozíciu, kde sa niekoľkokrát vzájomne predbehli.   Ďalším husárskym kúskom ktorý Gilles predviedol bola kvalifikácia na okruhu Watkins Glen v roku 1979 kde celú kvalifikáciu vyhral o neuveriteľných 11 sekúnd. V hustom daždi hnal svoj monopost naplno. Ukázalo sa, že Gilles bol bláznom za volantom a vždy šiel naplno.       Tragická smrť v Belgicku v roku 1982       O Gillesovi bolo známe že jazdí na hranici možného a aj to, že sa často vystavuje neuveriteľnému riziku. Na veľkú cenu Belgicka 1982 bol Gilles veľmi motivovaný.Už na začiatku kvalifikácie bolo vidno, že monopost nešetrí a že si ide za najlepším časom. V kvalifikácii sa však dostal za omnoho pomalší monopost tímu March pilotovaný Jochenom Massom, ktorý sa mu vyhýbal vpravo. Gilles to neočakával. V rýchlosti 225 km/h Gilles narazil do pomalšieho monopostu. V tej chvíli sa Gillesov monopost vymrštil do vzduchu a po niekoľkých saltách narazil znova do vozovky. V tomto momente ho vyhodilo z monopostu a Gilles letel vzduchom vyše 50 metrov. Jeho telo dopadlo k okraji vozovky. Po následnom resuscitovaní ho odviezli do miestnej nemocnice. Aj napriek snahe lekárov sa život legendárneho a charizmatického kanaďana zachrániť nepodarilo. Gilles Villeneuve zomrel o 21:21 v miestnej nemocnici. 



    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľuboš Dobrovič 
                                        
                                            Podkarpatská Rus - I. časť - národno-emancipačné snahy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľuboš Dobrovič 
                                        
                                            Eutanázia – právo na dôstojnú smrť alebo zabitie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľuboš Dobrovič 
                                        
                                            Islamská revolúcia inak...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľuboš Dobrovič 
                                        
                                            Ius murmurandi alebo Čo sa stalo s blog.sme.sk?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľuboš Dobrovič 
                                        
                                            Quo vadis Slovenská republika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľuboš Dobrovič
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľuboš Dobrovič
            
         
        lubosdobrovic.blog.sme.sk (rss)
         
                                     
     
        Človek s vlastnými názormi určenými tým, ktorí ich vedia zhodnotiť.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1062
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Paul Hoffman - Ľavá ruka Boha
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog Jána Lazoríka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Podkarpatská Rus - I. časť - národno-emancipačné snahy
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




