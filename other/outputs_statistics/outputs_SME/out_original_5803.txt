
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Piršč
                                        &gt;
                Nezaradené
                     
                 Oddaná láska 

        
            
                                    4.5.2010
            o
            9:53
                        (upravené
                20.3.2012
                o
                12:13)
                        |
            Karma článku:
                11.73
            |
            Prečítané 
            1325-krát
                    
         
     
         
             

                 
                    „Keď som sa dozvedel, koľko dolárov zarobím na hodinu za ponúkanú prácu, rýchlo som si to v hlave vynásobil momentálnym kurzom našej meny, hlava sa mi zatočila z tej cifry, okamžite som súhlasil s pracovnými podmienkami a podpísal dohodu so sprostredkovateľskou agentúrou na rok s možnosťou predlženia o ďalší rok.
                 

                     Hneď večer som zavolal manželke domov, oznámil som jej radostnú novinu, že som konečne po dvoch rokoch v cudzine chytil poriadny flek, že už sa nemusí obávať o našu budúcnosť, budem jej mesačne posielať ešte viac peňazí, aby mala dostatok na vedenie domácnosti, na výchovu našej osemročnej dcéry, že už nemusí hľadať nejakú podradnú, slabo platenú prácu u nás doma, že chcem, aby pocítila aj na tú diaľku, ako ju mám rád, že sa viem nadpriemerne postarať o svoju rodinu a zabezpečiť jej a našej dcérke lepší a bohatší život.   Po vystriedaní rôznych profesií, od maliara izieb až po umývača okien, stal som sa nakoniec opatrovateľom - a carer. Opatroval som deväťdesiatročného bývalého profesora vysokej školy v jednom severoamerickom mestečku na východnom pobreží USA. Starý pán už bol na invalidnom vozíku, nemal žiadnu rodinu, prežil svoju manželku aj svojho jediného syna, ktorý sa neoženil a nemal žiadnych potomkov. Starký bol z toho dosť smutný, ale nie zúfalý. Býval som v jeho staromódne zariadenom rodinnom dome, plnom starožitností a umeleckých kníh, kde som dostal vlastnú izbu. Mal som voľný prístup po celom dome a okrem domu staral som sa o trávnik v záhrade, o profesorove historické auto, ale samozrejme hlavnou náplňou mojej práce bola starostlivosť o profesora ako o pacienta - lieky, nákupy, príprava stravy, dohľad a zabezpečenie jeho základných životných funkcií.   Po čase si ma starý pán veľmi obľúbil, pomáhal som mu nad rámec svojich povinnosti, masíroval som mu ruky, nohy, cvičil s ním v posteli jednoduché cviky. On mi na oplátku pomáhal zdokonaľovať sa v angličtine, zasväcoval ma do histórie umenia, do predmetu, ktorý kedysi vyučoval, a o ktorom rozprával s takým nadšením, že som ho počúval s otvorenými ušami a očami. Často som vyberal z jeho bohatej knižnice farebné publikácie rôznych svetových maliarov, rozoberali sme spolu detaily tisícky obrazov, naučil ma rozlišovať umelecké smery, dal mi základy estetiky nielen vo výtvarnom, ale aj úžitkovom umení, v starožitnostiach, v sochárstve a v architektúre.   Postupom času mi starý profesor tak dôveroval, že ma posielal autom do banky, vybavoval som mu účty, jeho susedia mi každé ráno zdravili, a keď som aj im pomohol s drobnými domácimi opravami, oslovovali ma po mene, volali ma k sebe na návštevu, jedným slovom, stal som sa v cudzom svete váženým mladým mužom, ktorého prijalo medzi seba najbližšie osadenstvo.   Zdravotný stav pána profesora sa tak zlepšil, že s mojou pomocou začal chodiť na krátke vzdialenosti po svojich nohách, v slnečných dňoch som ho vozil autom k moru, bol so mnou nadmieru spokojný a po roku si na mňa tak zvykol, že ma bral úplne ako svojho syna.   Manželke a dcérke som telefonoval minimálne dvakrát týždenne. Hovory trvali niekoľko hodín a starký, keď vybadal, že sa blíži deň, keď sa vrátim domov k milovanej žene, jedného dňa si ma s vážnou tvárou zavolal k sebe a pokojným hlasom mi navrhol, aby sa moja manželka a dcéra presťahovali k nám, do jeho domu, do USA, že on nemá dedičov, a že celý svoj majetok: dom, auto, úspory prepíše na mňa, ak ho dochováme do smrti.   Bola to šokujúca správa, profesor bol veľmi bohatý človek - znelo to ako z rozprávky - o takomto živote sa mi pred troma rokmi, keď som do USA utiekol z nášho úradu práce, ani nesnilo. Rozradostený som okamžite volal manželke, vysvetlil som jej, aké máme neuveriteľné šťastie, že môžeme všetci spolu bývať v nádhernom rodinnom dome v USA, v malom mestečku, rozlohou podobnom ako to naše na Slovensku, ale oveľa krajšom a čistejšom, nech chytro začne vybavovať víza a pripravovať sa na nový, šťastnejší život. Jej kategorická odpoveď ma takmer omráčila: ´Ja do žiadnej Ameriky bývať nepôjdem!´   Dopadol na mňa nesmierny smútok. Všetko, čo som posledné roky robil, robil som pre ňu a našu dcérku. Ľúbil som obe nadovšetko, nevedel som si predstaviť, že by som v USA zostal sám bez svojej rodiny, hoci finančne zabezpečený - nebol by som šťastný. Ale nemohol som ju nútiť k takémuto závažnému kroku. V ďalších dňoch sme po telefóne opäť rozoberali náš spoločný osud, zatvrdlo trvala na svojom, chcela naďalej žiť, kde si zvykla, v malom rodnom mestečku na východnom Slovensku.   Bolo to pre mňa ťažké rozhodovanie. Nakoniec som sa rozhodol ukončiť načas pracovný pomer v USA a odcestoval som domov, dúfajúc, že ju možno dajako prehovorím k zmene postoja.   Privítanie nebolo také vrelé, aké som si predstavoval. Videla sa mi akási zmenená, odcudzená, pripisoval som to našej dlhej odlučiteľnosti. Svojimi ženskými zbraňami dosiahla, že sme sa onedlho prestali baviť o vysťahovaní, naopak, začali sme s modernizáciou nášho dvojizbového bytu. Výmena jadra, okien, podláh, vstavané skrine, podhľady - financoval som to všetko sám, náklady pohltili väčšiu časť mojich bočných úspor.   Tri a pol mesiaca po mojom návrate z USA, keď som konečne dokončil rekonštrukciu bytu, sa jedného večera manželka nevrátila domov. Na druhý deň mi telefonicky stroho oznámila, že sa chce dať so mnou rozviesť! Že už sa nedokáže pretvarovať, že už o tom vie takmer celé mesto, len ja nie, že už tri roky žije s iným mužom, podnikateľom, ktorý obchoduje s paletami, u ktorého býva, a s ktorým si rozumie vo všetkom. V rozvodovom konaní bude žiadať do opatery dcéru, plus alimenty a polovičku nášho spoločného nehnuteľného majetku.   Chytil som sa za vlasy, hlava sa mi zatočila od nešťastia. Snažil som sa dovolať do USA k múdremu pánu profesorovi, požiadať ho o radu - čo robiť? Môžem sa vrátiť? Začal sa mi rúcať svet. Nikto nedvíhal telefón. Zavolal som jeho susedovi, ktorý mi so smutným hlasom oznámil, že starý pán profesor už bohužiaľ nežije, pred troma týždňami zomrel - jeho dom a majetok sú momentálne v dražbe."     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Šéfova dcéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 2.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 1.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Papierová prilba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Silvester 1988
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Piršč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Piršč
            
         
        pirsc.blog.sme.sk (rss)
         
                                     
     
        Foto: 10.9.2012









 
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bieda a charakter
                     
                                                         
                       Kvetografie
                     
                                                         
                       Led Zeppelin - Celebration Day
                     
                                                         
                       Racheli
                     
                                                         
                       Krása sveta
                     
                                                         
                       O Slovensku, ktoré nik nechcel
                     
                                                         
                       Splynutie ticha
                     
                                                         
                       spomienkovy
                     
                                                         
                       Ďakujem Ti, Vierka
                     
                                                         
                       Emily Dickinsonová: Srdce chce najskôr slasť, potom - zomrieť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




