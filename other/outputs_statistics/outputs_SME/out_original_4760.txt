
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Fusek
                                        &gt;
                RETRO v Ružinove
                     
                 RETRO (Ružinov): Obyvatelia na prokuratúre, magistrát zatiaľ nie 

        
            
                                    17.4.2010
            o
            23:38
                        (upravené
                22.4.2010
                o
                23:35)
                        |
            Karma článku:
                3.18
            |
            Prečítané 
            544-krát
                    
         
     
         
             

                 
                    Magistrát na čele s primátorom Ďurkovským odkazovali do médií, že ich odpor voči navyšovaniu RETRO rozhodnutím Krajského stavebného úradu nekončí. Mesto zvažovalo brániť sa ministerstve či súde. Medzitým viacerý občania už podali podnet na prokuratúru na preskúmanie rozhodovania miestneho aj krajského stavebného úradu. Na čo čaká magistrát?
                 

                 ČO NA TO KRAJSKÝ STAVEBNÝ ÚRAD   Občania sa obrátili na Krajský stavebný úrad z odvolaním proti rozhodnutiu ružinovské stavbeného úradu na čele so starostom Drozdom, ktorý schválil navýšenia RETRO v Ružinove. Občania namietali voči postupu schvaľovania nielen dodatočných poschodí, ale aj zmeny funkcie vďaka vypusteniu domovu dôchodcov (viď. linku č.1). Podobne sa odovolalo mesto najmä v súvislosti z rozsahom zmien (viď. linku č.3), ktorý nespadá pod režim zmeny stavby pred dokončením.   Odpoveď KSÚ bohužial len opisuje proces, nerieši obsahom podnetov, ktoré boli omeškané (viď. linku č.4-6). Preto ani nevysvetľuje, čo bol ten chýbajúci oprávnený dôvod pre povolenie zmeny stavby pred jej dokončením. Tento argument nevedel uviesť ani starosta Drozd ako predseda MSÚ a KSÚ sem svetlo tiež nevznieslo.   Toto vrhá negatívne svetlo nielen na MSÚ na čele so starostom Ružinova ale ja na KSÚ, preto sa tým bude zaoberať prokuratúra (viď. linku č. 7). Otázkou ostáva, prečo otála magistrát na čele s primátorom Ďurkovským, ktorý rovnako dostal nepotešivú odpoveď z KSÚ (viď. linku č.8-11).       STAROSTA DROZD JE SPOKOJNÝ, RUŽINOV ASI NIE   Pár chvíľ po zverejnení rozhodnutia KSÚ starosta Ružinova Drozd opäť zanietene obhajoval svoj postup, ktorý vraj potvrdilo KSÚ. A tak teda dobre urobil, lebo vlastne odvrátil hrozbu žaloby investora voči Ružinovu. Ten istý dobrý investor sa údajne zaviazal za intenzívnej zásluhy pána starostu mimo iného k investícii do parku v prípade navýšenia RETRO. Samozrejme RETRO sa už veselo navyšuje, ale o revitalizácii ani slychu. Išlo opäť len o RETRO marketing pre nalákanie budúcich kupcov, alebo prečo sa už 1/2 roka čaká od schválenia na vybudovanie tohto parku hlavne pre ostatných Ružinovčanov? Páni a čo vlastne hovorí zmluva či vôbec existuje, aby sa na všetko nezabudlo?   Takto sú vlastne všetci spokojný: Úbohý starosta musel súhlasiť s navýšením - zachránil Ružinov - v čom mu za pravdu dal aj KSÚ, a teda je to hrdina. Investor RETRO síce vie, že na jeho pozemok dnes patria 4 poschodia, ale len on veselo navyšuje na 25. poschodí, však MSÚ aj KSÚ mu to dovolili - tak prečo by mal konať morálne? A občan, obyvateľ Ružinova? Ale však o toho tu predsa očividne nejde.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Fusek 
                                        
                                            Ružinov: Protihlukové VZN "zákaz stavby cez víkend" Ťa potrebuje!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Fusek 
                                        
                                            RETRO (Ružinov): Prokurátor koná, Ďurkovský voličov ignoruje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Fusek 
                                        
                                            Retro (Ružinov): Starostov vlastný gól Ružinovu musia hasiť ostatní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Fusek 
                                        
                                            RETRO (Ružinov): Občania sa posťažovali starostovi ale čo ďalej?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Fusek 
                                        
                                            RETRO (Ružinov): Stovka ľudí chcela vidieť starostovi do očí
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Fusek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Fusek
            
         
        fusek.blog.sme.sk (rss)
         
                                     
     
        Nie je mi jedno a chcem veci napravit. A to aj vlastnymi rukami, pre svoje deti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1240
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť a politika
                        
                     
                                     
                        
                            RETRO v Ružinove
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




