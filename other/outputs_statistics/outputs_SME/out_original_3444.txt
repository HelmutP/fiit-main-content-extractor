
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Turanský
                                        &gt;
                Brazília
                     
                 ...Hoten...Toten... 

        
            
                                    27.3.2010
            o
            19:11
                        (upravené
                27.3.2010
                o
                23:23)
                        |
            Karma článku:
                8.50
            |
            Prečítané 
            968-krát
                    
         
     
         
             

                 
                    Aj John Smith z rozprávky Pocahontas išiel do zasnenej zeme Indiánov bez akejkoľvek znalosti miestneho jazyka. A predsa sa mu podarilo aklimatizovať sa a dokonca sa s miestnymi spriateliť. Tak isto som aj ja išiel do vzdialenej zeme bez toho, aby som jazyk čo len trochu ovládal a tak isto sa mi podarilo začleniť sa...aj keď zo začiatku to bolo sprevádzané mnohými komickými situáciami...jazyk vie byť zradný...
                 

                     „Dáš si dezert?“ – opýtala sa ma počas prvého mesiaca môjho pobytu hosťovská mama po obede.   „Áno, susedu by som si dal.“ – odpovedal som jej s vážnym výrazom v tvári.   (sobrimesa – dezert, vizinha – suseda )... neviem prečo sa mi to vtedy zdalo podobné..:)       „Aké je typické jedlo na Slovensku?“ – vyzvedala hosťovská sestra.   „Varíme veľa polievok.“ – hovorím.   „Aké napríklad?“   „Veľa, ja mám veľmi rád polievku z očí.“ – vysvetľoval som lámanou portugalčinou.   Sestra takmer dostala infarkt..   (ovo – vajce, olho – oko)       Moja mama mi rozprávala pár podobných historiek, ktoré zažili s brazílskou študentkou  u nás na Slovensku.   Bruna (tak sa volá Brazílčanka) išla počas hokejového zápasu v Martine na toaletu a vydala sa smerom k dverám s nápisom PÁNI. V poslednej chvíli ju moja mama zdrapila zozadu za kabát a pýta sa jej : „Preboha, kam ideš?“   „Na záchod.“ – odpovedala prekvapená.   „Však to je pre mužov.“ – vysvetľovala jej mama.   „Je tam napísané PÁNI? Je. Som pani ? Som. Idem.“ – odpovedala Bruna.   Hľa, čo môže spôsobiť jeden obyčajný slovenský dĺžeň!       Mesiac po príchode na Slovensko bola Bruna v obchode a predavačka sa jej pýta: „Máte drobné slečna ?“ A Bruna jej na to odpovedala : „Počkaj, počkaj...“  Predavačka vyvalila na ňu oči a mama musela vysvetlovať, že je to výmenná študentka z Brazílie, že nevie ešte dobre po slovensky a zatiaľ vykaniu nerozumie. Nerozumie dodnes..akurát, že sa to akosi intuitívne naučila.       Zo začiatku si Bruna lepila po izbe papierky so slovíčkami, aby sa ich ľahšie naučila. Išli niekam s mojou rodinou autom a na lúke sa pásli kone, na čo Bruna hovorí:  „Aha, aký krásny kôš!“.. potom moji rodičia pochopili, že si zafixovala ô na základe slova kôš..mala taký jeden v izbe a na ňom nalepený papierik..kôš, kôň..aké podobné...:)       Môj zákerný mladší brat jej to nie veľmi uľahčoval a s vážnou tvárou ju naučil, že rožok sa povie rožek, zemiak je zemák a nanuk je nanak. Bruna to potom povedala v škole, kde sa jej dosť smiali. Dnes to našťastie berie s humorom :).       Druhá výmenná študentka, ktorá momentálne býva v mojej  rodine je z Austrálie.  Volá sa Leah. Moji rodičia ju počas lyžovačky učili ako povedať po slovensky časti oblečenia. „Leah, toto sú nohavice, rukavice..lebo ruka, noha..vieš.. a eye sa povie oko ,takže glasses povieme....“.. A skôr ako moja mama stihla dopovedať, že sú to okuliare, Leah víťazoslávne a  sebavedome zahlásila : „Okavice...“ Myslím že nie je čo dodať..:)       Učiť sa cudzie jazyky priamo na mieste činu môže byť niekedy veľmi zradné..ale v každom prípade veľmi zábavné..:)         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Nie všetko "značkové" je naozaj "značkové" alebo o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Posledný večer
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Amazónia - Tajomná a očarujúca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Komunisti ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Z blata do kaluže
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Turanský
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Turanský
            
         
        tomasturansky.blog.sme.sk (rss)
         
                                     
     
        Som študent, bývalý výmenný, ktorý si absolvoval rok v Brazílii a teraz si zvyká znovu na realitu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1528
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Brazília
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Andrzej Sapkowski - Boží Bojovníci
                                     
                                                                             
                                            Roald Dahl - Neuveriteľné Príbehy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Led Zeppelin
                                     
                                                                             
                                            Sex Pistols
                                     
                                                                             
                                            Ramones
                                     
                                                                             
                                            Rádio Expres
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




