
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marcel Páleš
                                        &gt;
                Cesta za zmyslom života
                     
                 Každý deň rozohrávaš nový zápas 

        
            
                                    14.4.2010
            o
            15:26
                        (upravené
                15.4.2010
                o
                19:23)
                        |
            Karma článku:
                9.89
            |
            Prečítané 
            984-krát
                    
         
     
         
             

                 
                    Občas mi zo seba chodí nevoľno. Áno - občas, keď sa tak na seba pozerám a pozorujem sa, najmä to, ako nakladám so vzácnym časom, mám chuť zo seba vyzvracať všetko to, čo by som zo svojho života najradšej ihneď vyhodil. Presne, ako keď sa vám do žalúdka dostane niečo, čo tam nepatrí a spraví vám v ňom menšiu šarapatu. Musíte to dať von - možno sa vyprázdniť a začať znova. Miestami mi býva od toho duševného žalúdka zle aj celé dni. Je to ťažký súboj.
                 

                 
  
   Každý deň náš život predstavuje nový zápas. Presne ako pri futbale či hokeji, začíname od stavu 0:0. Myslím, že u nejedného z nás sa každodenný zápas za tohto stavu obvykle aj končí. Sú totiž dni, ktoré neznamenajú pre náš život žiaden prínos – nezablysne sa v nich nič výnimočné, nič zaujímavé, jednoducho nič, čo by stálo za zmienku. Zdá sa, že v takéto či tie horšie dni sa podobáme tomu číslu, ktoré figuruje na obidvoch stranách počiatočného stavu. Nule.    Podobne ako vo futbalovom zápase i v tom našom padajú góly. Keď sa vám niečo podarí, niečo, čo vás obvykle pozdvihne, poprípade to pomôže niekomu inému a vy máte zo seba dobrý pocit, je to akýsi gól. Na vašom konte sa objaví namiesto nuly jednotka. Vyhrávate. Takto tých gólov môžete každý deň nastrieľať niekoľko.    Avšak všetko má obvykle aj druhú stranu. Aj skóre má ľavú a pravú stranu, oddelené dvojbodkou. Pravá strana zaznamenáva góly do vašej siete. Napríklad, keď spôsobíte svojim hlúpym konaním niekomu bolesť, keď zahodíte dobrú šancu, keď spravíte niečo chybné – pokazíte prihrávku, ktorú vám niekto krásne načasoval. Je pravdou, že určité veci je správne najprv spraviť nesprávne (aby sa človek poučil). No ale na chybách sa učíme a nemôžeme tie nahrávky kaziť donekonečna. Teda pribúda gólov aj na ľavej strane.    Ak ste napríklad cez deň pomohli priateľovi, pomohli starej pani s nákupom, vytvorili nejakú krásnu vec a odviedli skvelú prácu – vyhrávate 4:0. No, ak sa vám v ďalší deň podarilo bezohľadným konaním zarmútiť iného priateľa, z ľahostajnosti ste nechali povinnosti nevyriešené, vďaka lenivosti ste nič poriadne neurobili a dokonca ste podľahli lacnému pokušeniu – vaše skóre vykazuje evidentnú prehru 0:4.   Váš tím má totiž svojich hráčov, proti ktorým stoja hráči súpera. Na vašej strane hrá napríklad usilovnosť, láska, bojovnosť, vytrvalosť, činorodosť alebo viera, no proti nim stoja zdatní súperi ako lenivosť, ľahostajnosť, pýcha a im podobní. Je to každopádne neraz náročný súboj. Často prehratý. No tak, ako na sebe pracujú športovci a neskôr dosahujú skvelé výkony, tak aj ľudia môžu na sebe zapracovať a vypracovať si svojich „hráčov“ na lepšiu úroveň. Potom napríklad náš zadák s menom Usilovnosť nemá problém ustrážiť súpera prezývaného Lenivosť. Pomáha tak útočníčke Činorodosti, ktorá môže ľahko skórovať.    Neznášam pocit, keď si líham do postele a cítim sa prehrato. Vtedy mi je jasné, že mnoho šancí z daného dňa som premrhal a zahodil, že niektoré prihrávky som úplne pokazil. Teším sa ale, lebo po krátkej pauze v podobe spánku sa začína nový zápas – opäť od nuly –, no a v ňom môžem zažiariť. Je pravda, že na akýkoľvek tím prídu strelecké suchoty, niekedy aj na pár týždňov, ale to neznamená, že sa daný tím má nechať potopiť. Kdeže!   Každý deň predstavuje novú nádej, nové šance, nové životné prihrávky a nové skúsenosti. Pomyslenie na víťazstvo nás môže vyburcovať, aby sme podali zodpovedný, poprípade kvalitný výkon. Želám vám čo najviac víťazných zápasov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Je trápne byť citlivým človekom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Veľkým svetom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Obyčajné objatie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Ra-ta-ta-ta-ta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Máte v sebe nainštalovaný balík láskavosti?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marcel Páleš
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marcel Páleš
            
         
        marcelpales.blog.sme.sk (rss)
         
                                     
     
        Okrem mnohého iného ma zaujímajú diskusie o zmysle života, rád presahujem hranice priemernosti, milujem čítanie kníh a tiež čítanie ľudí. Keďže rád nachádzam neobyčajné v obyčajnom, nájdete ma často strateného v pozorovaní kolobehu života. Soľou môjho života je umenie, korením priatelia a najsilnejšou prísadou nad všetko vyvýšená láska. Spomedzi priateľov si najviac cením Boha, preto mastretnete aj v božích chrámoch, kde vyhľadávam pokoj v duši.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    154
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1065
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Živý pozdrav z Márnice
                        
                     
                                     
                        
                            Idúc ulicou
                        
                     
                                     
                        
                            Cesta za zmyslom života
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Slová nad zrkadlom
                        
                     
                                     
                        
                            Boh
                        
                     
                                     
                        
                            Nočná návšteva
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Beatnici + absurdné divadlo
                                     
                                                                             
                                            J.P.Sartre, Ionesco
                                     
                                                                             
                                            Franz Kafka
                                     
                                                                             
                                            Haruki Murakami
                                     
                                                                             
                                            Mario Puzo
                                     
                                                                             
                                            Dan Brown, D. Dán
                                     
                                                                             
                                            Paolo Coelho
                                     
                                                                             
                                            Maxim E.Matkin
                                     
                                                                             
                                            Stephen King
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            relaxačné napr. Jaro Filip - Meditation for Piano
                                     
                                                                             
                                            Hip-hop najmä SK scénu
                                     
                                                                             
                                            Rieka života a podobné
                                     
                                                                             
                                            cigánske, napr: Kmeťoband, Diabolské husle
                                     
                                                                             
                                            trocha popu, rocku, punku, romantiky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Brnčová Kristínka
                                     
                                                                             
                                            Janka Dratvová
                                     
                                                                             
                                            Danielka Nemčoková
                                     
                                                                             
                                            Danka Janebová
                                     
                                                                             
                                            Janka Regulyová
                                     
                                                                             
                                            Miška Urbanová
                                     
                                                                             
                                            Phillipa Marco
                                     
                                                                             
                                            Lienka Jamborová
                                     
                                                                             
                                            Pietruchova Oľga
                                     
                                                                             
                                            Peťuška Debnárová
                                     
                                                                             
                                            Čínske dievča-Chen Lidka Liang &amp;#26753;&amp;#26216;
                                     
                                                                             
                                            Kika Hatarova
                                     
                                                                             
                                            AnezaHviezdna
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            LumpArt - moderné umenie
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




