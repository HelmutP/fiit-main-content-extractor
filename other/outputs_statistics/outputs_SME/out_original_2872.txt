
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Tralich
                                        &gt;
                Filozofia slobody
                     
                 Môj jazyk štátu nepatrí 

        
            
                                    18.3.2010
            o
            20:50
                        (upravené
                19.3.2010
                o
                20:02)
                        |
            Karma článku:
                4.31
            |
            Prečítané 
            891-krát
                    
         
     
         
             

                 
                    Žiaci a učitelia v školách s vyučovacím jazykom maďarským by sa mali pripraviť na zvýšený počet inšpekcií na hodinách slovenčiny. Taká je odpoveď ministra školstva za SNS Jána Mikolaja na víkendové slová maďarského prezidenta Lászlóa Sólyoma, podľa ktorého by sa Maďari v Srbsku, Rumunsku a na Slovensku mali učiť štátny jazyk ako cudzí jazyk.
                 

                 
  
   ...mali učiť štátny jazyk ako cudzí jazyk, povedal jeden politik. A druhý politik ako reakciu predloží návrh programu na zvýšenie kvality vyučovania štátneho jazyka v školách s vyučovacím jazykom národnostných menšín. Kde okrem iného nariaďuje inšpekcie na hodinách slovenčiny, na ich základe sa potom má „inovovať štátny vzdelávací program".    „Štátny jazyk“   Neviem odkedy štátu narástol jazyk, keďže štát je len abstraktný pojem a keď chcem ísť niečo vybaviť ohľadom oficialít, nejdem k štátu, ale na úrad. Preto poznám skôr úradný jazyk. A podľa tohto označenia má byť používaný len na úradoch.   Štát mi nemá právo kárať ako sa ja budem rozprávať s mojimi zamestnancami či zákazníkmi alebo v akej reči budem viesť písomný styk s kým len chcem, či to je už inzerát, reklama alebo výveska na mojom majetku či na mojom pozemku či na majetku toho, s kým som sa na tom dohodol. Prečo mi páni politici zasahujete do môjho majetku? Štát som na toto konanie žiadnym spôsobom nesplnomocnil. Môj majetok som vám nedal a ani len neprepožičal.    Vyučovania štátneho jazyka a štátny vzdelávací program   Chcem sa vás, drahí politici, taktiež opýtať, prečo chcete rozhodovať aký jazyk budem ovládať ja či moje deti? Nepamätám si, že by som vám podpisoval nado mnou a mojim potomstvom plnú moc rozhodovania nad mojím životom, resp. nad mojim telom. Alebo chcete vyhlásiť, že som vašim majetkom? Lebo tak sa ku mne a mojim deťom správate. Sme novodobí otroci vášho štátneho systému?        A poslednou otázkou na vás bude: Prečo používate násilné výpalné, ktoré VY nazývate DANE a ODVODY na vytváranie centrálne riadeného hospodárstva vo výučbe? (A keby len tam, ale teraz sa venujem iba vzdelávaciemu systému.)   Nechajte na mne kam dám svoje deti, resp. kam budú ony chcieť ísť. Nechajte na mne a mojich deťoch čo si vyberieme za odbor a čo sa v rámci toho odboru budeme učiť. My si chceme slobodne vybrať medzi školami, ktoré si budú svoje vzdelávacie programy tvoriť podľa našej aktuálnej potreby a dopytu a nie podľa vašich momentálnych nálad, emócií či mocenských rozmarov a lobistických nátlakov a záujmov.   A preto...   Neokrádajte nás na našom majetku a netvorte systém postavený na násilí (výpalné - dane) a na tom násilí vybudované inštitúcie (štátne školy - platené z daní). Sme len obyčajní ľudia a nevieme konkurovať tým silným lobistom čo za vami stoja a denno-denne lobujú u vás za zákony, ktoré sú pre nich prijateľné a zvýhodňujú ich oproti nám. Nechceme štátom sponzorovanú pozitívnu diskrimináciu.   A perlička na záver: „Premiér Róbert Fico už v pondelok označil Sólyomove slová za „zákerný útok" a „rozbíjanie štátnosti". - Pán Fico, kedy som vám ja dal legitimitu nado mnou vládnuť? Vám, alebo ktorýmkoľvek vašim predchodcom?   Môj život, telo a majetok štátu nepatrí...či? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Tralich 
                                        
                                            Slováci, rozmýšľajte, komu slúžia tí, ktorým nevadí rozkrádanie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Tralich 
                                        
                                            SNS (Slovak National Shame)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Tralich 
                                        
                                            Mikuš využíva peniaze kraja na sebapropagáciu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Tralich
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Tralich
            
         
        tralich.blog.sme.sk (rss)
         
                                     
     
         http://c4fs.wordpress.com/  

    
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1147
                
            
        
     

    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cena štátu - projekt INESS
                                     
                                                                             
                                            Ako Vás môže štát alebo samospráva pripraviť o majetok?
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




