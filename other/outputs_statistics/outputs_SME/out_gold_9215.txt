
 Ráno som vstala s pocitom, že musím odísť niekam ďaleko, niekam kde ma nebudeš hľadať. Aby som sa vyhla bolesti a všetkých tých emócií, ktoré rozožierali moje vnútro. Po špičkách som si pobalila pár vecí, obula som sa a zatvorila dvere od nášho bytu.
Nebol si doma.
Nevedel si, že o tom viem. Tým som si bola istá. 
Naštartovala som auto, pršalo. V rádiu mi Bono spieval " Beautiful day " a ja som si len vzdychla. 
Neviem ako dlho som sa len tak bezcielne túlala, s dažďom, čo mi pripomínal, že plačem niekde tam kde začína srdce. 
Viem len, že keď mi zazvonil telefón a volal si - nezdvihla som. 
Lebo som nemohla..
Lebo som pred tebou utekala..
Lebo som nemohla zniesť, že to srdce v hrudi, na ktorej som lehávala - patrí niekomu inému.

Patrilo Dáši - mojej najlepšej kamoške.

 
