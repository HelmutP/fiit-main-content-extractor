

 Ak ste s prijímačkami na vysokú školu neuspeli máte viacero možností. Môžete si podať prihlášku na ďalšiu vysokú. Vybrať si môžete spomedzi tých škôl, ktoré robia ešte aj druhé kolo prijímacích pohovorov. Termín na podanie prihlášok je väčšinou do konca júla, prijímačky druhého kola sa konajú v auguste. 

 Ak máte veľkú chuť študovať a dokázali by ste dať dokopy niekoľko desiatok tisíc korún, môžete sa hlásiť aj na súkromné vysoké školy. Na Slovensku funguje v sektore súkromných vysokých škôl zatiaľ iba jedna škola - City University, v Čechách je už ponuka širšia. Súkromné vysoké školy ponúkajú študijné programy, ktoré sú z 58 percent orientované na ekonomiku,  ďalej na umelecké odbory, právo a informatiku.  

   

