
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Annie Gáborová
                                        &gt;
                Z hlbín duše
                     
                 Dotyk šťastia 

        
            
                                    11.6.2010
            o
            20:36
                        (upravené
                11.6.2010
                o
                22:26)
                        |
            Karma článku:
                3.22
            |
            Prečítané 
            239-krát
                    
         
     
         
             

                 
                    Toľko dotykov.  Duša sa raduje.  Poskakuje.  Steny sú jej malé.
                 

                 
  
   Dotyk náhody - nečakaný, výnimočný, dar stadiaľ - od Neho.  Dotyk tepla - z horúceho vzduchu, z ich prítomnosti, z tej trochy najlepšieho zlata v pohári.  Dotyk hudby - živelný, energický, iný. Rozkmital to vnútro, čo rado tancuje.  Dotyk tmy - skrývajúci a odhaľujúci. Aj komárie krídla dotkli sa. Už nevadí.  Dotyk vône - lipového kvetu, keď svet stál a ja som sa točila, pod ním. Donekonečna. Aj suchá tráva z polí zavoňala.  Dotyk kvapiek - studených, farebných, pokojných, hravých. Nechali sme ich rásť.   Toľko dotykov.  Duša sa raduje.  Poskakuje.  Steny sú jej malé.  Dotyk spokojnosti - so životom.  Dotyk šťastia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Annie Gáborová 
                                        
                                            (Ne)nechať ísť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Annie Gáborová 
                                        
                                            Dokonalé zatmenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Annie Gáborová 
                                        
                                            Len kúsok viery?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Annie Gáborová 
                                        
                                            Kroky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Annie Gáborová 
                                        
                                            Be free!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Annie Gáborová
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Do mňa sa omáčaj
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Nežná
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Annie Gáborová
            
         
        anniegaborova.blog.sme.sk (rss)
         
                                     
     
        To live is to love. Everything else is just details. (P. Coelho)
.................................................
Put a smile on your face and show the world you can't be beaten. 
..............................................
Šťastie musí človek nájsť predovšetkým v sebe. Radovať sa z maličkostí a všetko na tomto svete prijať a priať si ŽIŤ život so všetkým ... prijímať porážky a ísť ďalej ... so srdcom na dlani ... s vierou, láskou a nádejou.
.............................................
Možno, že najdôležitejšou časťou reality sú naše sny. (Kosorin)
..............................................
"Mám veľmi jednoduchý vkus. Uspokojím sa vždy s tým najlepším." (O. Wilde)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    231
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z hlbín duše
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            P. Coelho
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Ed Sheeran - Kiss Me
                                     
                                                                             
                                            The Swell Season - The Moon
                                     
                                                                             
                                            Edyta Bartosiewicz - Ostatni
                                     
                                                                             
                                            Braille - Lisa Hannigan
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       (Ne)nechať ísť
                     
                                                         
                       Len kúsok viery?
                     
                                                         
                       Dokonalé zatmenie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




