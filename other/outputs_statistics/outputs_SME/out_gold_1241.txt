

 
 









Na snímke prezentácia tretieho záchranného vrtuľníka Agusta A109K2 vo Vajnoroch 31. marca 2004. Tento stroj, ktorý v záchrannej úprave umožňuje prepravu jedného ležiaceho pacienta a troch členov zdravotníckeho personálu, spĺňa všetky európske normy leteckej prevádzky pre potreby leteckej záchrannej služby. Vrtuľník dosahuje rýchlosť vyše 280 km/h a má dolet 800 km. 
Foto: Vladimír Benko - TASR 



POPRAD 29. novembra (SITA) - Počas zimnej sezóny pribudne do
leteckého parku spoločnosti Air Transport Europe (ATE)
Poprad štvrtý vrtuľník Agusta 109 K2, ktorý nasadia pre
potreby Leteckej záchrannej služby v Banskej Bystrici.
Vrtuľníky rovnakého typu už pracujú v strediskách Poprad,
Bratislava a Košice. Ide o vrtuľníky spĺňajúce najprísnejšie
parametre požadované Európskou úniou. Nový vrtuľník by mal
na Slovensko priletieť po 15. decembri. Po zaškolení
personálu ho ATE nasadí pre potreby leteckej záchrany už v
januári 2005, informoval na tlačovej besede v Poprade
riaditeľ ATE Milan Hoholík. Záchrana vrtuľníkom je plne
hradená zo zdravotného poistenia. Tiesňové číslo Leteckej
záchrannej služby zo všetkých sietí na celom území Slovenska
je 18155. 
Aj pred tohtoročnou zimnou sezónou si môžu návštevníci hôr,
ale aj ostatní záujemcovia kúpiť Kartu darcu v hodnote 200
Sk. Držiteľ karty má nárok na bezplatnú službu LZS alebo
Horskej záchrannej služby pri náhlom zhoršení zdravia alebo
pri úraze, aj keď takýto zásah nebol indikovaný zdravotnou
službou, ako aj na prevoz vrtuľníkom do najbližšieho
vhodného zariadenia pod dohľadom lekára, ak si to bude
vyžadovať zdravotný stav. Od augusta 2003, keď LZS ATE v
spolupráci s HZS zaviedla Kartu darcu si ju kúpilo 799
záujemcov. 
Počas letnej sezóny, od júna do októbra, zasahovala Letecká
záchranná služba Air Transport Europe v 281 prípadoch na
celom Slovensku. Najviac zásahov (135 letov) uskutočnilo
stredisko v Poprade, pod ktoré patria Slovenský raj, Západné
a Vysoké Tatry. Počas leta nalietali záchranári 351 letových
hodín. Počet zásahov kopíroval návštevnosť, ktorá bola
nižšia ako v minulých rokoch, uviedol hovorca LZS ATE Pavol
Svetoň. 
Spoločnosť ATE sa rozhodla poskytnúť leteckú techniku na
likvidáciu škôd v Tatrách. "Nie je nám ľahostajné, čo sa tu
stalo a takýmto spôsobom chceme prispieť k obnove Tatier,"
uviedol M. Hoholík. Už krátko po víchrici poskytla firma ATE
Štátnym lesom bezplatne vrtuľník na obhliadku a zmapovanie
kalamitou postihnutého lesa. Letecké práce v hodnote 5 mil.
Sk budú robiť na základe požiadaviek Štátnych lesov a Správy
TANAP-u. Vrtuľníky môžu robiť postreky, ťažbu dreva,
mapovanie terénu a uvoľňovanie ciest všade tam, kde sa iná
technika nemôže dostať. ATE má k dispozícii tri vrtuľníky
Agusta 109 K2, jeden vrtuľník Mi-8, štyri vrtuľníky Mi-2 a jeden vrtuľník
Kanja. Letecká záchranná služba nebude počas týchto prác
žiadnym spôsobom obmedzená.

