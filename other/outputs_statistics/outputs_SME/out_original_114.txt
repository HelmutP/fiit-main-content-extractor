




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 ?ampansk? 
 
 Vydan? 5. 8. 2003 o 0:00 Autor: ZUZANA O?EN??OV?
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 FOTO - ARCH?V 
 

 Svadby, krsty det? aj lod?, tajn? stretnutia milencov, automobilov? preteky, orgie, obchody v?etk?ho druhu - tam v?ade do ?a?? na stopk?ch, bakelitov?ch poh?rikov aj pupkov zh?ralcov pr?di ?tekliv? mok. Ako pri v?etk?ch dobr?ch veciach v ?ivote, aj prv? ochutnanie lahodn?ho ?ampansk?ho zvykne sprev?dza? hork? sklamanie. Ak nov??ik doteraz pil len osladen? s?ten? pav?na falo?ne naz?van? ?ampansk?m a pred ochutnan?m toho ozajstn?ho zlupol dobre pocukren? z?kusok, vyv?en? such? chu? a korenist? ar?ma sa mu zare?? do jazyka ako britva. Ani nezac?ti, ?e pr?ve ochutnal najtemperamentnej?ie z v?n na tejto plan?te, vyn?lez poloslep?ho mn?cha od benedikt?nov - ?ampansk?. Dom P?rignon objavil zlatonosn? silu bubliniek okolo 4. augusta 1693, pred 310 rokmi. 
 Nikto presne nevie, kedy pre?iel ?udsk?m hrdlom prv? hlt ?umiv?ho v?na. Ist? v?ak je, ?e sa v Anglicku objavilo e?te sk?r, ne? ho pod?a legendy objavil v?myseln?cky mn?ch Dom P?rignon z benedikt?nskeho op?tstva z Hautvillers. 
 T?to legenda potvrdzuje, ?e omamn? ?ampansk?, pod vplyvom ktor?ho sa ?udia odd?vna p???ali do najroztopa?nej??ch k?skov, vymyslel mu? posv?ten? mn?skym s?ubom striedmosti. 
 
 
 

 Ako sa teda ?umiv? v?no dostalo do Anglicka? Svetov? encyklop?dia v?n pon?ka nasleduj?ce vysvetlenie: Ke? v Champagne pri?la zima, v?no v sudoch prestalo pracova?. Stredovek? vin?ri si to spo?iatku mylne vysvet?ovali ako d?kaz ukon?enia kvasenia. Driemaj?ce v?no previezli n?morn?ci do Anglicka. Angli?ania na rozdiel od Franc?zov u? v tom ?ase v?no f?a?ovali. V pivniciach hostincov na Britsk?ch ostrovoch sa v?no zmenami teploty prebudilo a nepozorovane v ?om pokra?ovala alchymistick? premena. Ke? potom prekvapen?mu Angli?anovi vystrelila z?tka z f?a?e a obila kus stropu, netu?il, ?e v ruk?ch dr?? nie?o, ?o v bud?cnosti bude zn?mkou luxusu, a e?te k tomu franc?zskou. 
 Pod?a in?ch zdrojov sa prv? ?ampansk? objavilo u? o storo?ie sk?r, ne? sa Dom P?rignon narodil, a to v roku 1531 vo vzdialenom benedikt?nskom op?tstve v Limoux na juhu Franc?zska. Historici v?na sa domnievaj?, ?e Dom P?rignon sa od kolegov z Limoux mohol in?pirova? a ich pozorovania pou?i? ako z?klad pre svoju met?du. 
 Dom P?rignon skr?val pod mn?skou kapuc?ou hlavu ?pekulanta. Najprv experimentoval s mie?an?m r?znych odr?d hrozna vo v?nnom mu?te a vymyslel techniku sce?ovania v?n. Prav? ?ampansk? vznik? zo spojenia odr?d Chardonnay, Pinot Noir a Pinot Meunier. Cez svoj zvl?tny lis dok?zal Dom P?rignon dosta? aj z modr?ch bob?? ??ry bezfarebn? mu?t. 
 Znovuobjavil pre Franc?zov korkov? ?tuple, ktor? po vp?de Rimanov z pivn?c vymizli, a vymyslel z?klady procesu, ktor? dnes nesie honosn? n?zov ?m?thode champenoise?. 
 V?robu ?ampansk?ho sprev?dza rad zdanlivo nezmyseln?ch ?innost? a t??dne tich?ho ?akania pri dubov?ch sudoch. Na za?iatku s? l?ny vini?ov z oblasti Champaigne, na ktor? tam narazili u? Rimania. Ke? prvotriednym mu?tom, zvan?m ?cuv?e?, prebehne kvasenie, vznik? ?z?kladn? such? v?no. Aby z neho nie?o bolo, mus? by? ?vn?torne nevyrovnan?, ma? potenci?l pre chemick? dr?mu, ktorej v?sledky po mesiacoch trpezliv?ho ?akania vyb??ia na jazyku v podobe rafinovan?ch buketov. 
 V z?kladnom v?ne prebieha proces zvan? jabl?no-mlie?na ferment?cia. S pridan?m cukrom a v?nnymi kvasinkami ho potom vin?ri odnes? do najhlb??ch pivn?c v oblasti, ktor? do kriedov?ch ?tesov z usaden?n d?vneho morsk?ho dna vyh?bili e?te r?mski dobyvatelia. 
 ??m pomal?ie bude v?no pracova?, t?m bude vz?cnej?ie. Po nieko?k?ch t??d?och sa f?a?e premiestnia do zo?ikmen?ch stojanov. Nasleduj?cich osem t??d?ov prich?dzaj? k f?a?iam tich? slu?obn?ci a po tro?ke f?a?e potriasaj?, aby posunuli sedimenty k hrdlu f?a?e. Prav? ?ampansk? potom neru?ene drieme jeden a? tri roky. 
 Nasleduje opatrn? odstr?nenie sedimentov, do moku padne kvapka brandy a vin?r mu ritu?lne nasad? korkov? klob?k s ko??kom z dr?tu. E?te jeden-dva roky odpo?inku v pivnici a vyn?lez mn?cha m??e s nami za?a? robi? nekres?ansk? z?zraky. 
 Dom P?rignon by si mohol da? patentova? e?te jeden vyn?lez. Na distrib?ciu svojho ?umiv?ho v?na pou??val odvtedy mnohokr?t ?spe?n? marketingov? met?du nedosiahnute?nosti. Znie: ??ampansk?ho m? by? v?dy m?lo.? 
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? 16:00  Rube? kolabuje, rusk? vl?da m? mimoriadne rokova? 
 Jeden dol?r st?l v utorok poobede u? viac ako 78 rub?ov, razantn? zv??enie ?rokov nepomohlo. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 18:45  
Taliban zabil v ?kole v Pakistane vy?e sto ?ud?, v??inou deti
 
 Militanti vtrhli do vojenskej ?koly v policajn?ch rovno?at?ch. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  Cynick? obluda: Medzi?asom na opustenom ostrove 
 Stroskotanci maj? ve?k? ??astie, ?e s? dvaja. Lacn? vtipy na ?rovni ich autora. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  
Zav?dzaj?ca produktivita pr?ce
 
 Priemern? hodnoty s? pekn?m ?dajom, ale zl?m n?vodom pre hospod?rske politiky. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






