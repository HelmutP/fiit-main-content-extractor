
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabo Németh
                                        &gt;
                Súkromné
                     
                 Nezodpovednosť ľudi nepozná hranice 

        
            
                                    8.4.2010
            o
            8:13
                        (upravené
                9.4.2010
                o
                21:47)
                        |
            Karma článku:
                5.72
            |
            Prečítané 
            449-krát
                    
         
     
         
             

                 
                    Asi tak  by sa dala definovať skutočnosť a realita dnešného dňa, keď som sa vracal domov zo železničnej stanice.
                 

                 Chcem vravieť o skupinke ľudí, ktorých nazývame psíčkari. Je krásne mať psa starať sa o neho, ale ak majiteľ je nezodpovedný, ako som to videl cestou domov, okamžite sa mení názor na samotného psíčkara. To, čo som videl sa v našom meste stáva dosť často. Len ma zarazila skutočnosť, že danú osobu  dobre poznám, ale pekne po poriadku. Psíčkárka z vedľajšieho bloku na sídlisku, kde bývam šla venčiť svojho psa. Nebolo by na tom nič, ale stála predo mnou na pár metrov, pozná ma dobre, lenže sa tvárila, že som pre ňu vzduch. Príčinou zmeny jej správania som pochopil za niekoľko minút.   Pes si spravil potrebu a hovienko pekne krásne si len tak lebedilo v tráve. Pani sa obzerala, či niekto ju nevidí, bola si vedomá, že koná zle. A  v negatívnom konaní pokračovala, akoby sa nechumelilo. Psí výkal nechala v tráve, obzrela sa ešte raz a tvárila sa, keď ja nevidim nikoho, ani on nevidia mňa. Tak naše mesto vďaka psíčkárov smrdí od základu. Vďaka takýmto ľudom, ako bola dnes ráno pani so psom. Táto nezodpovednosť už hraničí s ľudskou bezcharakternosťou a stojí na zamyslenie.   Naše mesto potom smrdí, až človeku sa dvíha žalúdok. A nie len to je problém, ktorí psíčkari spôsobujú. Bez vodítka, bez náhubku púšťajú svojich psov po meste, zatiaľ, čo oni sa s priateľmi veselo rozprávajú. Ani ich nenapadne mať psa pri sebe. Veď to ani nie je dôležité. Je potrebné sa porozprávať, vychytať všetky pletky v meste, aby sme boli informovaní o všetkom. Prečo to spomínam?   Stala sa mi nemilá skutočnosť so psom, kedy je majiteľka sedela na lavičke  rozprávala sa so susedou. Zatiaľ jej pes / musím podotknúť bol to bernandín  ešte šteňa, ale dosť veľké  na to, aby ublížilo človeku./ Kračal som na chodníku a pes pribehol ku mne,  vyskočil na mňa, pravú labu dal na moje plece a hlavu na krk. Zamrzol som, ani som nedýchal, počítal iba sekundy, ktoré mi zostávali. Predsa som stihol skríknuť na majiteľku psa, ktorá sa veselo rozprávala. Jej odpoveď bola, až zarážajúca. - Veď sa chce s tebou iba hrať -.   Na koniec predsa len psa upozornila. Čo si pamätám,  v tom strašnom šoku som jej na to odpovedal. -Tak nech sa hrá s vami pani - Ani si nedokážem predstaviť, čo by sa stalo, keby pes mi hryzol do krku, alebo mi inak ublížil. Až hrôza pomyslieť. Odvtedy mám rešpekt  pred každým psom a nehanbím sa priznať i strach. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky a čas
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Plač kvetov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Nočná návšteva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Dušičková
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabo Németh
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabo Németh
            
         
        gabonemeth.blog.sme.sk (rss)
         
                                     
     
         profesionálny knihovník, básnik, spisovateľ, od roku 2006 člen Spolku slovenských spisovateľov. Vydal básnické zbierky: Za súmraku, Slnko nad básňou, Rodinný album. Venuje sa duchovnej poézii. Chce byť pokračovateľom katolíckej moderny. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    917
                
                
                    Celková karma
                    
                                                2.76
                    
                
                
                    Priemerná čítanosť
                    335
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            denná čítannosť
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            A300
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            len a len dobré knihy
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                             
                                     
                                                                             
                                            denník Sme všetky články
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Skôr než zaspím od S.J. Watson
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Queen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            blog spisovateľky Miroslavy Varáčkovej
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetky dobré a kvalitné blogy na Sme.sk
                                     
                                                                             
                                            facebook
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Muž z kríža
                     
                                                         
                       Spomienky a čas
                     
                                                         
                       Jesenné dotyky
                     
                                                         
                       Plač kvetov
                     
                                                         
                       Nočná návšteva
                     
                                                         
                       Spomienky
                     
                                                         
                       Na prechádzke
                     
                                                         
                       Dušičková
                     
                                                         
                       Ranný obraz
                     
                                                         
                       Po stopách vlastného príbehu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




