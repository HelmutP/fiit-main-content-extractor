
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Veselý
                                        &gt;
                Životný štýl
                     
                 Jack Canfield bol v Bratislave 

        
            
                                    28.6.2010
            o
            7:01
                        (upravené
                13.8.2010
                o
                10:44)
                        |
            Karma článku:
                9.52
            |
            Prečítané 
            2869-krát
                    
         
     
         
             

                 
                    V sobotu 26. 6. 2010 sa v Bratislave objavil Jack Canfield a na celodennom seminári preberal tajomstvá úspechu. Prinášam vám z tejto akcie 4 fotky a pár myšlienok na ilustráciu.
                 

                 Popravde - žiadne z tajomstiev úspechu v skutočnosti už pár storočí nie je tajomstvom. Úspech skôr vzniká ako séria zákonitostí. Vyplývajú z psychologických princípov, strategického plánovania, vytrvalosti a ďalších ingrediencií. A značne to celé ovplyvňuje aj to, aké má človek sebavedomie.    Základným kameňom úspechu je potreba prekonať pohodlie, strach, byť trpezlivý, odložiť odmenu na neskôr, investovať trpezlivosť, pozornosť... A to sa málokomu chce. Preto nezaškodí občas si pripomenúť to, na čo je tak príjemné zabúdať. No a v sobotu na to bolo veľa príležitostí.       Pohľad na sálu, v Inchebe, počas seminára Jacka Canfielda. Húfiky kamier dávali tušiť, že so záznamom z tejto akcie sa asi ešte budeme mať možnosť časom stretnúť.      Jack Canfield na pódiu.       Jack Canfield s jedným z dvoch tlmočníkov (tlmočili fakt excelentne).      No a jeden portrétik som cvakol aj zblízka. Ktovie, aké afirmácie si ten deň ráno dal...   Zážitky, či informácie z tak dlhého semináru, ktorý bol tak trocha aj tréningom je dosť ťažké tlmočiť a tak na ilustráciu spomeniem aspoň niekoľko z myšlienok. Koho by to zaujímalo podrobnejšie, určite neublíži ak si kúpite knihu Jacka Canfielda Pravidlá úspechu. Takže sľúbené príklady.   Napríklad prejdite si dnes svoj dom, pracovisko, alebo záhradu s papierom v ruke a zapisujte si, čo sa vám nepáči, čo vás možno dokonca dráždi. Možno je to neporiadok, možno vŕzgajúce dvere, alebo vám niekde nesvieti žiarovka. Spíšte to a okamžite vyriešte všetky tieto drobnosti. Upracte, opravte veci, ktoré nefungujú. Ľudia často majú v neporiadku mnoho maličkostí a to ich programuje na neúspech. Ak nie ste schopní vyriešiť drobnosti, ako by ste si verili, že dokážete vyriešiť veľké úlohy? Čo je horšie - nevyriešené malé problémy roztrieštia vašu pozornosť a vy sa nesústredíte na dôležité veci.    Nešlo o prednášku, ale o praktické cvičenia od relaxačných techník až po strategické plánovanie. Podľa návodu ste si so susedmi mohli navzájom klásť otázky a dávať na ne ťažké odpovede. Niekedy sú také otázky rafinovane nepríjemné. Skúste si to:     Dráždi vás niečo v živote? Je niečo, čo vás zaťažuje?   Akým spôsobom ste sa do tej situácie dostali?   Nechávate tú situáciu pretrvávať? Prečo?   Boli by ste radšej v inej situácii? V akej?   Čo podniknete pre to, aby ste v budúcnosti boli v situácii v akej chcete byť?   Kedy presne začnete pracovať na dosiahnutí zmeny?     Podobné (vlastne samozrejmé) otázky človeku často až vyrazia dych. Neraz sme v ťažkostiach v ktorých zotrvávame aj roky len preto, že je pohodlnejšie nechať veci tak ako sú. A prečo pohodlnejšie? Pretože sa bojíme - zmena je neistota.   Peknou technikou zo záveru seminára Jacka Canfielda bola aj metóda spätnej väzby. Spýtajte sa známych niečo v tomto zmysle:     Ak by ste mali hodnotiť ... (naše kamarátstvo, náš obchodný vzťah, dnešný obed...) na stupnici od 1 do 10, kedy 10 je najlepšie. Ako by ste to hodnotili?   Ak je odpoveď menej ako 10, dáte doplnkovú otázku: Čo môžem nabudúce spraviť, aby to bolo za 10?     Bolo príjemným zážitkom zhruba 9 hodín venovať sa práci na sebazdokonaleni v príjemnej atmosfére.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Test: Okuliare na ochranu krčnej chrbtice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako nepribrať cez sviatky?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Kniha o chudnutí zadarmo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako sa pečie domáci chlieb - video
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Chudnutie a chôdza - video
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Veselý
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Veselý
            
         
        vesely.blog.sme.sk (rss)
         
                        VIP
                             
     
         Píšem na témy chudnutie, práca, opatrovanie a občas posielam fotky z výletov. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    412
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5070
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Videoseriál chudnutie
                        
                     
                                     
                        
                            O chudnutí krátko, stručne
                        
                     
                                     
                        
                            Chudnutie v sebaobrane
                        
                     
                                     
                        
                            Recepty na varenie
                        
                     
                                     
                        
                            Odpočinkové články
                        
                     
                                     
                        
                            Firefox
                        
                     
                                     
                        
                            Recenzie kníh
                        
                     
                                     
                        
                            Prikázania pre úspešný web
                        
                     
                                     
                        
                            O písaní blogov
                        
                     
                                     
                        
                            The Bat! - Program na e-maily
                        
                     
                                     
                        
                            Iné omrvinky
                        
                     
                                     
                        
                            Hudobná skrinka
                        
                     
                                     
                        
                            Životný štýl
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Dukanova diéta
                                     
                                                                             
                                            Turistika
                                     
                                                                             
                                            Energetická hodnota potravín
                                     
                                                                             
                                            Reality
                                     
                                                                             
                                            Kalkulačka BMI a obezity
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Web o opatrovaní a pomoci
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Glykemický index
                                     
                                                                             
                                            Kam na výlet
                                     
                                                                             
                                            Recepty na varenie
                                     
                                                                             
                                            Najlepšie probiotiká
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




