
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Bastl
                                        &gt;
                nezaradené
                     
                 Moja prvá operácia srdca 

        
            
                                    14.5.2010
            o
            16:00
                        (upravené
                14.5.2010
                o
                21:14)
                        |
            Karma článku:
                9.79
            |
            Prečítané 
            2564-krát
                    
         
     
         
             

                 
                    Ústav kardiovaskulárnych chorôb na Partizánskej ulici v Bratislave bol v roku 1980 nové  vrcholové pracovisko s celoslovenskou pôsobnosťou vytvorené pre prevenciu a liečbu srdcovo- cievnych ochorení. Pracovali v ňom erudovaní slovenskí odborníci so zahraničnými skú­se­nosťami z oblasti kardiológie a kardiochirurgie. Ústav bol lokalizovaný v pamiatkovo chránenej budove, ktorá po  dobudovaní v roku 1914 bola útulkom Matiek-diakonisiek, neskôr v  dvadsiatych rokoch Evanjelickou všeobecnou nemocnicou.
                 

                 
Vstupná brána do bývalého Ústavu kardiovaskulárnych chorôb v BratislaveJozef Bastl
   Koncom novembra 1980, po tom, ako som absolvoval všetky potrebné  vyšetrenia, som bol opätovne hospitalizovaný na tomto Ústave za účelom  operačného výkonu na srdci. Išlo o Aorto-koronárny by-passe, čo v  preklade znamená obídenie upchanej cievy na srdci našitím vlastného  implantátu z cievy predkolenia. Operácia sa vykonáva pri otvorenom hrudníku, na zastavenom srdci a pri mimotelovom obehu krvi.   Ležal som spolu s ďalšími troma pacientmi na treťom poschodí nemocnice, kde sa nachá­dzali operačné sály. Na tomto poschodí bolo aj detské oddelenie. A práve deti, ktoré k nám často chodili na návštevu, udržiavali na našej izbe optimistickú predoperačnú náladu. Dosť sme sa s nimi nasmiali. Nikto z nás ale nevedel, do čoho ide. Napriek tomu som veril, že mi lekári pomôžu zbaviť sa neustálych bolestivých ischemických záchvatov a nízkej tolerancie námahy. Snažil som sa preniesť svoju vieru aj na spolupacientov, ktorí sa borili s podobnými problémami.      Operovali ma 4. novembra 1980. Ešte na izbe som dostal pred medikáciu, rozlúčil som sa s kamarátmi a ako prvý som odišiel so zdravotníkmi na chodbu. Mal som čakať na lavici pred dverami operačnej sály. O niekoľko minút som v sede zaspal. Iba matne si pamätám, ako ma prekladali na operačný stôl. Potom som mal chvíľu pocit, akoby moje telo rotujúc vo vodo­rov­nej po­lohe letelo hore dlhým tunelom, na konci ktorého som videl silné biele svetlo.   Keď som sa prebral z narkózy, dýchal so mnou prístroj. Veľmi mi to prekážalo, pretože som dýchal svojím rytmom, ktoré nebolo totožné s rytmom dýchacieho prístroja. Pri lôžku sedela mladá lekárka, ktorej som naznačoval, aby mi tú trubku vybrali z úst. Nerozumela mi, a tak mi podala papier a ceruzku, aby som napísal, čo chcem. Keď som napísal na papier svoju prosbu, odpovedala: „Ešte nemôžeme, lebo sme to skúsili a prestali ste nám dýchať." Nado­po­vaný sedatívami v infúziách som opäť na dlhší čas zaspal.   Nepamätám si, ako dlho som ležal v malej pooperačnej miestnosti. Keď som sa však vrátil na izbu medzi svojich spolupacientov, snažil som sa, napriek pooperačným ťažkostiam, udržať si predoperačný optimizmus. Verím, že to premohlo prekonať strach ostatných z toho, čo ich ešte len čakalo a ja už som to mal za sebou.   Obdivuhodne rýchlo som sa zotavoval, Najskôr som skúšal chodiť iba po rovnej chodbe, ale týždeň po operácii som už chodil po schodoch. Tešil som sa z toho, že ťažkosti, ktoré som mal pred operáciou, sa už neobjavovali. To ma utvrdzovalo v tom, že operácia sa podarila. Spomínam si, že krátko po operácii som na schodoch stretol dvoch profesorov, ktorí sa ma opýtali, ako mi to ide. Keď som povedal, že dobre, iba sa na seba pozreli a z výrazu ich tvárí vyžarovala spokojnosť.   Po operácii som sa vrátil do pracovného prostredia. Vykonával som dosť náročnú prácu a bol som aj k sebe rovnako náročný. Vždy som sa dokázal extrémne vložiť do každej veci a chcel som ju dotiahnuť do konca za každú cenu. Všade som chodil pešo. Najradšej som mal schody. Chodil som po nich, kde sa len dalo. Pravidelne som chodieval na liečenia, kde som veľa a rád tancoval. Priatelia mi často hovorili, že to preháňam. Radili mi, aby som ubral, lebo raz zostanem na parkete. Dnes ale viem, že práve tanec, ktorý som celý život obľu­bo­val, mi veľmi pomohol získať späť stratenú kondíciu, ale aj sebadôveru, ktorá je pri každom ochorení veľmi dôležitá. Mal som obrovské šťastie, že som sa z roboty vždy vracal domov k usporiadanej rodine, kde som mal vytvorené dobré zázemie.   Vynikajúcim slovenským lekárom, internistom, kardiológom a kardiochirurgom, ktorí pracovali v tom čase na Ústave kardiovaskulárnych chorôb v Bratislave, vďačím za to, že mi svojou svedomitou prácou umožnili prežiť dlhých 19 rokov plnohodnotného života. Cieva našitá z nohy na srdce dokázala po celý čas bezchybne premiestňovať okysličenú krv na postihnuté miesto. Potom sa mi ale vrátili podobné zdravotné ťažkosti, aké som mal pred operáciou. Pokračovanie nabudúce....     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Cestný pirát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Radosť z úrody
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Aj seniori to dokážu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Moje prvé pristátie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Návrat do detstva
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Bastl
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Bastl
            
         
        bastl.blog.sme.sk (rss)
         
                                     
     
         Vyrastal som vo viacčlennej rodine v malej dedinke neďaleko Mladej Boleslavy. Vyučil som sa najskôr za baníka, neskôr za automechanika. Vojenskú základnú službu som vykonával neďaleko Bratislavy. Tu som sa aj oženil a založil svoju rodinu. Pracoval som na viacerých postoch vrátane verejnej funkcie. Najdlhšie, takmer tridsať rokov, som pracoval v Československom neskôr v Slovenskom rozhlase ako vedúci dopravný referent. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1405
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            súkromná
                        
                     
                                     
                        
                            nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       3. Na pohraničnej rote
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




