
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrea Fecková
                                        &gt;
                Nezaradené
                     
                 Barbarská noc 

        
            
                                    15.4.2010
            o
            18:17
                        (upravené
                15.4.2010
                o
                18:28)
                        |
            Karma článku:
                9.14
            |
            Prečítané 
            1227-krát
                    
         
     
         
             

                 
                    Áno, veľa sa zmenilo, ale ľudia zostali rovnakí, ibaže teraz vieme lepšie, kto je kto. Kto je slušný, bol slušný vždy. Kto bol verný, je verný aj teraz. Kto sa točí s vetrom, točil sa s vetrom aj skôr. Kto myslí, že teraz prišla jeho chvíľa, myslel vždy len na seba. Nikto sa nestáva prebehlíkom, kto ním nebol skôr a vždy. Kto mení vieru, nemal žiadnu. Človeka neprerobíš, len sa ti vyfarbí.
                 

                 Včera večer som sa stretla s českým kardinálom Vlkom. Hovoril mi o tom, ako chodil do Roháčov na túry. Niekoľko minút predtým však vystúpil pred plnou sálou na pôde Slovenskej ambasády pri Svätej Stolici v Ríme, kde povedal: „Minulosť by nám mala byť mementom, aby sme si dnes volili inú cestu." Kardinál hovoril o Barbarskej noci v 1950-tom.   „Čakal som obrátený k stene s putami na rukách. Potom ma vzali do miestnosti, kde mi vzali všetky veci. Odtiaľ do klietky zo skla, kde som sa musel úplne vyzliecť a robiť drepy."   Ak by toto bola výpoveď spred 60-tich rokov, možno by nás ani veľmi neprekvapila. Veď o Barbarskej noci sme už toľko počuli. Tieto slová však povedal Remigiusz P. Górski.   Jeden z rehoľníkov v bratislavskom kláštore, ktorý donedávna v kauze františkáni čelil obvineniu zo sexuálneho zneužívania  dvoch neplnoletých chlapcov. Nechýbalo násilné vniknutie do kláštora ani odvlečenie. Františkáni prežívali barbarské mesiace. Až dovtedy, kým nevyšla pravda na povrch.   Človek by si myslel, že to, čo sa dialo v 50-tych rokoch, tá mašinéria zla, že sa to už nikdy nevráti. Ale kdeže! Netreba chodiť ďaleko, aby sme zistili, že opak je pravdou.   Skladám klobúk pred františkánmi, ktorých aj vďaka našim bulvárnym médiám a nepravdivej kauze pozná celé Slovensko. Po včerajšku, kedy som videla film Vymývanie mozgov, im zvlášť ďakujem - za svedectvo viery a vytrvalosť, a za ich odvahu a silu čeliť ešte stále tak hlboko zakorenenému komunistickému mysleniu a praktikám v našej slovenskej spoločnosti.   Darmo, Karel Čapek mal pravdu, keď povedal: „Človeka neprerobíš, len sa ti vyfarbí!" 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (48)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrea Fecková 
                                        
                                            Mladić
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrea Fecková 
                                        
                                            Blahorečenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrea Fecková 
                                        
                                            Betkina modlitba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrea Fecková 
                                        
                                            Pena vo vani
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrea Fecková 
                                        
                                            Ukradnuté šťastie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrea Fecková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrea Fecková
            
         
        feckova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Normálne vysoké podpätky nenosím. Ale keď treba, dám si.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    97
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1797
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek Gajdoš
                                     
                                                                             
                                            Roman Daniel Baranek
                                     
                                                                             
                                            Magda Kotulova
                                     
                                                                             
                                            Samo Marec
                                     
                                                                             
                                            Pavol Biely
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Mika Polohova
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




