

 
 FOTO – ČTK 
 

 Louisa Joy Brownová sa narodila ako modrooké dievčatko s plavými vlasmi. Vážila niečo viac ako 2,5 kilogramu, teda úplne normálne. A norme sa nevymykal ani pôrod cisárskym rezom. Napriek tomu to bol hviezdny okamih modernej medicíny. Louise totiž vzišla z úspešného oplodnenia matkinho vajíčka otcovou spermiou mimo tela matky. Stala sa prvým „dieťaťom zo skúmavky“. Na svet prišla v britskom Manchesteri presne pred 25 rokmi - 25. júla 1978. 
 Jej rodičia Lesley a John Brownovci pochádzali z Bristolu. Deväť rokov sa márne pokúšali počať dieťa. Lesley mala upchaté vajíčkovody. Putovala od jedného lekára k druhému. 
 Napokon ju v roku 1976 odporučili k doktorovi Patrickovi Steptoeovi, gynekológovi v Oldham General Hospital. Spolu s fyziológom a embryológom Robertom Edwardsom z Cambridge University vtedy už desaťročie hľadali riešenie takýchto stavov. 
 Vedeli, ako oplodniť vajíčko mimo ženinho tela. Stále sa im však nedarilo prekonať problémy po premiestnení vzniknutého embrya do maternice. Všetkých 80 pokusov umelého oplodnenia sa skončilo do niekoľkých týždňov. S takýmito šancami Lesley 10. novembra 1977 podstúpila nový experiment. 
 Steptoe jej odobral z vaječníka vajíčko. Edwards ho pridal k Johnovým spermiám. Oplodnené vajíčko premiestnil do živného roztoku. Predtým umelo oplodnené vajíčka vkladali do maternice v okamihu, keď sa rozdelili na 64 buniek - teda po štyroch či piatich dňoch. Tentoraz sa rozhodli implantovať ho už po dva a pol dni. Bingo! 
 Lesley úspešne absolvovala prvé týždne tehotenstva. Embryo sa jej pevne zahniezdilo na stene maternice. A potom bez väčších komplikácií mesiac za mesiacom rástlo. Lekári sledovali vývoj dieťaťa ultrazvukom. 
 Deväť dní pred očakávaným dátumom pôrodu Lesley prudko stúpol tlak. Steptoe s Edwardsom sa pre istotu rozhodli priviesť dieťatko na svet cisárskym rezom. 
 Oplodnenie „v skúmavke“ je dnes rutina ako súčasť takzvanej asistovanej reprodukcie. Každý rok sa milióny párov na celom svete pokúšajú počať dieťa. Mnohým sa to dlhodobo nedarí. Príčin neplodnosti je veľa, na strane ženy i muža. A nie vždy medicína pozná riešenie. Vďaka oplodneniu „v skúmavke“ sa dodnes na celom svete narodilo vyše milióna detí. 
 V Československu ho ako prví úspešne uskutočnili lekári z Fakultnej nemocnice v Brne v roku 1982. 
 Samotný zákrok je stále zložitý, prenos embrya sa podarí asi v tretine prípadov. Metodika sa však z roka na rok zdokonaľuje a prispôsobuje rôznym typom neplodnosti. Prevláda trend znižovať počet naraz prenesených embryí, ideálne na jedno, aby sa znížilo riziko nadmernej záťaže matky a detí pri viacnásobnom tehotenstve. 
 Z asistovanej reprodukcie sa v rozvinutých krajinách stal lukratívny medicínsky priemysel. Vyskytli sa štúdie poukazujúce na zvýšený výskyt defektov a vážnych chorôb umelo počatých detí. Iní odborníci im oponujú. No riziká neslobodno podceňovať. 
 Najmä pri takzvanej intracytoplazmatickej injekcii spermie do vaječnej bunky - „násilnom“ vtlačení spermie do vajíčka. Táto v roku 1992 náhodne objavená metóda prekvapivo funguje a vajíčko sa potom riadne delí. Prekonáva ďalšie príčiny neplodnosti, najmä mužskej. 
 Zvolí však vždy lekár na oplodnenie vajíčka najschopnejšiu spermiu, takže nahradí prírodný výber? 
 Imperatívom je, aby narodené deti boli zdravé. V tejto chvíli je fakt, že drvivá väčšina detí počatých umelým oplodnením (tým či oným konkrétnym postupom) taká je - fyzicky aj psychicky. 
 Deti „zo skúmavky“ dosiaľ obklopuje ovzdušie etickej kontroverznosti. Mnohí nábožensky orientovaní ľudia totiž vnímajú prirodzené splynutie vajíčka so spermiou ako nadprirodzený akt. Lekári sa podľa nich pri umelom oplodnení „hrajú na Boha“. 
 Nie všetci kresťania s tým majú problém. Tolerantné sú osobitne mnohé protestantské cirkvi. Podporu pre to našli v Písme. Zhruba to isté platí o judaizme. Veď existenciálna tragédia roky neplodných rodičov musí pri troche empatie vzbudiť hlbokú účasť. Ide o biologickú komplikáciu. A v jadre ľudskej prirodzenosti je nenechať to tak. 
 Inšpiráciu možno čerpať aj z iného kultúrneho okruhu. Konkrétne od staročínskeho mysliteľa Konfucia: „Vždy treba byť predovšetkým ľudský.“ 
 Louise Brownová na oslave dvadsaťpäťky iste ocení, že pri jej zrode stáli ľudia, ktorí sa v myslení i konaní riadili viac duchom ako kanonizovanou literou morálky.	 

