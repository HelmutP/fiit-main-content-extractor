

 
1)      Lebo je v nich dostatočná pridaná hodnota informácií – ten človek prečítal snáď všetky možné ideologické publikácie (minimálne tie z jemu sympatických smerov) 
 
 
2)      Lebo mám obavy z extrémistických excesov tohto radikálneho konzervatívca 
 
 
 Ponúkam pesimistický a optimistický pohľad na túto výraznú postavu ideologických bojov. 
 
 
Pesimistický pohľad 
 
 
Lukáš Krivošík je typ publicistu, ktorý má výnimočný talent na polarizáciu a rozdúchavanie primitívnych emócií či už u svojich priaznivcov, alebo odporcov. Sám napríklad neváha používať na adresu  homosexuálov pejoratívne označenie “sodomiti” a na adresu moslimov vyťahovať pod pásové tvrdenia na ktoré nemá relevantný zdroj. 
 
 
 Ku príkladu o Švédsku píše:“…nárast počtu znásilnení sa časovo zhoduje s nástupom prisťahovaleckých vĺn a páchateľmi aj naozaj sú väčšinou mladí muži z moslimských krajín,”    
 
 
V diskusii vyšlo najavo, že Krivošík nemá o čo toto svoje tvrdenie oprieť ( štatistiky sledujúce kriminalitu imigrantov nie sú k dispozícii) a teda je len jeho dojmom a nie faktom. Je smutné, že si neuvedomuje aké xenofóbne dopady môže mať takéto prehnané a neoverené tvrdenie.    
 
 
 Opatrnosť voči imigrantom je na mieste. O čo ale vlastne Krivošíkovi ide? Nechce sem pustiť tých čo si tu nájdu prácu? Príživníkov tu nechcem ani ja a je mi jedno odkiaľ sú. Nápodobe tých čo poškodzujú iných. Avšak ani dotovaná súťaž v rodení detí nevyrieši problém s nedostatkom kvalifikovanej pracovnej sily.      
 
 
 Zaujímavé sú aj iné Krivošíkové názory. Nepáčia sa mu demokratické rozhodnutia väčšiny a tak agituje za daňovú demokraciu. Čím viac zaplatených daní, tým väčšia váha volebného hlasu! 
 
 
Hmm…  
 
 
Vyzdvihuje aj španielskych dobyvatelov Cortesa a Pizzara. 
 
 
 “  Portugalskí objavitelia a španielski conquistadori v mnohých ohľadoch reprezentujú to najveľkolepejšie z Európy. Ich odhodlanie objavovať nové svety a zapĺňať biele miesta na mapách poznania je niečo, čo môže inšpirovať každého moderného vedca. Pripomínajú nám, že objavovanie je dobrodružstvo.  …Ich nekompromisná viera v Boha a presvedčenie o hodnote vlastného civilizačného dedičstva zas môžu inšpirovať konzervatívnych politikov, ktorí sa cítia osamelí v relativistickej dobe, ktorej chýba rytierskosť i vznešenosť.”  
 
 
Takže im išlo o zapĺňanie bielych miest na mape a nie o zlato a slávu?    
 
 
Krivošík si za nepriateľa vybral ľavicový liberalizmus. Je v skutku priekopníkom tohto nezmyselného slovného spojenia. V preklade sa jedná o sociálnu demokraciu  - v Európe sa  totiž ľavičiari zväčša nenazývajú liberálmi. Akoby aj mohli keď potláčajú ekonomickú slobodu? Keď zadáme do prehliadača spojenie “ľavicový liberalizmus” a pozrieme si príslušné odkazy, získame dojem akoby sa k tejto ideológii nikto nehlásil a všetci proti nej iba bojovali. Reálne sa jedná o marginálnu skupinu v tieni ľavicového populizmu R. Fica. 
 
 
Jedna z najpodstatnejších vecí čo mi vadí na Krivošíkovi, je jeho neférová taktika ideologického boja. V jeho článkoch prezentuje názorový protipól prostredníctvom primitívnych a sparodovaných šplechov (neraz z anonymných internetových diskusií), zatiaľ čo svoje názory podopiera citáciami myšlienkových velikánov z odbornej literatúry. Podľa môjho názoru to vo viacerých prípadoch spĺňa definíciu falošného argumentu “strawman fallacy“ (buduje si slameného panáka na boxovanie). 
 
 
Osobne by som rád v jeho článkoch a komentároch videl častejšie citácie jeho ideologických sokov a nie výber z toho najväčšieho primitivizmu čo internet ponúka.  
 
 
Optimistický pohľad  
 
 
Napriek všetkému uvedenému vyššie, považujem články tohto autora za hodné čítania. Je pochopiteľné, že pri ich neuveriteľnej kvantite sa vyskytnú aj šliapnutia mimo či prestrelenia.     
 
 
Čas ukáže, či sa dokáže Krivošík učiť na vlastných chybách, alebo ich bude opakovať a vytláčať zo svojho zorného pola. V trhových otázkach by mohol posunúť mnohých kresťanských demokratov doprava. Jeho konzervativizmus je síce na prvý pohľad radikálny, ale stále skôr v pozícii obrany status quo, ako prenášania konzervatívnej morálky na ostatných prostredníctvom štátu.    
 
 
Otázku z nadpisu článku by som teda nechal nezodpovedanú a otvorenú. Možno sú aj závažnejšie. V dobe keď ľavicový populizmus valcuje, je lepšie hľadať spojencov ako ďalších nepriateľov. Slovenská pravica je nekonzistentná a s nedostatkom osobností. Načo opravovať vŕzgajúce dvere, keď celý dom horí?       
 
 
 
 

