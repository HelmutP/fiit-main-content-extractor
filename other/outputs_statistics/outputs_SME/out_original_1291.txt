
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Prvý pokus
                                        &gt;
                Súkromné
                     
                 Voličský preukaz ako odysea 

        
            
                                    17.3.2009
            o
            10:37
                        |
            Karma článku:
                6.68
            |
            Prečítané 
            2266-krát
                    
         
     
         
             

                 
                    Tak som si teda bola vyzdvihnúť voličský preukaz. V čase konania prezidentských volieb sa budem vracať z dovolenky a s nádejou, že cestou uvidím volebnú miestnosť, by som rada dala hlas tomu kanidátovi, ktorý je v mojich očiach najmenej zdiskreditovaný a má šancu reprezentovať moju krajinu slušne, vecne a kultivovane. Neviem, či by som sa až takto občiansky angažovala /lebo nemám pocit, že by sa víťazi doterajších prezidentských volieb angažovali tak, aby som sa za nich ako občan nemusela hanbiť/, keby som nemala v rodine prvovoliča. Pre toho je možnosť ísť po prvý raz voliť predsa len čímsi výnimočným. Povzbudilo ma aj vyhlásenie pani Lívie Škultétyovej z Ústrednej volebnej komisie v jednej televízii, že vyzdvihnúť voličský preukaz môže bez problémov aj príbuzný.
                 

                 Nuž, nebolo to až také jednoduché. Ani nie tak pre mňa, ako pre úradníkov miestneho úradu. Samozrejme, že nestačí to, že som matka svojej mimo trvalého bydliska študujúcej dcéry či syna. Samozrejme, že k tomu treba mať nimi podpísané splnomocnenie. To mi, ako ináč, musia poslať alebo inak doručiť, alebo... Alebo si ho jednoducho vyrobím. Aj priamo na  miestnom úrade. Je tu ešte ďalší spôsob získania vlastného voličského preukazu. Moje dietky by napísali na miestny úrad žiadosť a ten by im, samozrejme s nejakými nákladmi na prácu, balné a poštovné, voličský preukaz poslal. Lepší prípad - stačí aj elektronická žiadosť. Pravda tam, kde už majú počítače a dopracovali sa aj k internetu. Možno by ste sa divili, koľko obecných úradov by sa prihlásilo, že toto veru nie je vôbec  jednoduchý prostriedok komunikácie ešte pre mnoho našich oblastí.   Proti môjmu splnomocneniu pani úradnička neprotestovala - ušetrila som jej prácu a úrady náklady na poštovné. Práve naopak - ona mala inštrukcie, aby robila tak, aby boli všetci spokojní. V zákone č. 46/1999 o spôsobe voľby prezidenta SR sa na margo voličských preukazov píše len toto: Oprávnenému voličovi, ktorý nebude môcť voliť vo volebnom okrsku, v ktorom je zapísaný do zoznamu oprávnených voličov, vydá obec po vyhlásení voľby na jeho žiadosť voličský preukaz a zo zoznamu oprávnených voličov ho vyčiarkne. Vyčiarknutie platí iba na čas vykonania voľby prostredníctvom voličského preukazu.   Manuál, ako sa celý tento zákonný  proces zrealizuje, mal ten môj úrad vypracovaný vlastný. Hru na splnomocnenie považujem za absolútne zbytočnú. Ak mi aj preukaz vydajú, voliť predsa môžem ísť len s platným občianskym preukazom. To iste platí aj pre tých, ktorým som ten preukaz vyzdvihla. Nikto iný s nim voliť ísť nemôže a nikto nemôže ísť voliť ani bez občianskeho preukazu. Vďaka bohu, naša pani úradnička bola rozumne uvažujúca žena, ktorá si o /ne/dokonalosti prezidentského volebného zákona myslí svoje /zhruba to isté, čo ja/ a o vyhláseniach členov Ústrednej volebnej komisie tiež. Hovorí, že „s voličmi sa mordujú tí najnižší úradníci. Tí z ústrednej komisie tiež chcú v očiach verejnosti vyzerať čo najlepšie." Presne tak, ako kandidáti na prezidenta.   Tí hovoria o tom, že súčasný zákon je zlý. Pre nich, lebo obmedzuje predvolebnú kampaň na dva týždne prd voľbami, čo je podľa nich málo na to, aby nás presvedčili o svojich kvalitách. No, niektorí nás o svojich nekvalitách presviedčajú už tak dlho, že tie dva týždne nás už v ich prospech nezlomia. Aniektorým sú dva týždne na to, aby sa zhovadili až dosť.  A tak hovoria o tom, že hneď po voľbách budú iniciovať zmenu tohto zákona. Primárna príčina zmeny sa však týka kandidátov, nie voličov. Nuž neviem, ja by som sa skôr dala presvedčiť zámerom /nie sľubom/, že „budeme iniciovať zmenu zákona tak, aby s voľbami nemali problémy ľudia, ktorí nežijú v mieste svojho trvalého bydliska." Ponúkam im niekoľko argumentov /do tej krátkej predvolebnej kampane/:   „Som rád, že mám prácu aj 400 km mimo domu a smenu kvôli voľbám nevynechám, dovolenku si nevyberiem, lebo za mnou je mnoho takých, ktorí v pohode oželejú svoju občiansku povinnosť a radšej pôjdu do práce."   „Študujem a náklady na cestu sú pre mňa náklady."   „Pracujem v zahraničí, chcem sa  raz vrátiť domov a rád by som dal hlas svojmu kandidátovi, aby som sa naozaj chcel vrátiť domov, ale za hranicami sa slovenský prezident voliť nedá."     autorka: PhDr. Anna Pančurová 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Stačí tak málo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Moje skúsenosti s poistením liečebných nákladov v jednej poistovni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Demonstrace zahájená objetím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Referendum o rodine z pohľadu kresťanského libertariána
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Kam zmizli z Malého Ríma všetci kresťania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Prvý pokus
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Prvý pokus
            
         
        pokus.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ak chcete zverejniť svoj text na tomto blogu, pošlite nám ho e-mailom vo worde na adresu matus.paculik(at)smeonline.sk. Nezabudnite na predmet PRVÝ POKUS. 
 
Výber textov je v právomoci redakcie, ktorá má právo zmeniť autorom navrhnutý titulok. Väčšinu článkov zverejňujeme, ale vyhradzujeme si právo ktorýkoľvek odmietnuť aj bez udania dôvodu, najmä ak sa autor nepodpíše celým menom. 
 
Vždy uveďte aj vašu poštovú adresu a telefónne číslo. Tieto dva údaje nezverejníme, ale musíme ich mať k dispozícii kvôli overeniu identity alebo pre prípad, že sa váš článok rozhodneme honorovať. 
 
Ak meno a priezvisko priamo v článku nechcete uviesť, prosíme, napíšte nám dôvod. Za istých dôležitých okolností tieto údaje nepublikujeme, redakcia ich však vždy musí poznať. 
 
Zaradenie textu zväčša trvá niekoľko dní. Na tomto blogu nezverejňujeme poéziu ani prózu. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    713
                
                
                    Celková karma
                    
                                                6.49
                    
                
                
                    Priemerná čítanosť
                    5054
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovenské zdravotníctvo
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       1.5 metra na bratislavských chodníkoch
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Prečo ma Kyjev rozplakal..
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




