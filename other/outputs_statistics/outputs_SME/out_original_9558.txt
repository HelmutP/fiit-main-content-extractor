
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Vrabec
                                        &gt;
                Nezaradené
                     
                 Prečo? 

        
            
                                    24.6.2010
            o
            21:45
                        (upravené
                9.2.2011
                o
                22:47)
                        |
            Karma článku:
                4.09
            |
            Prečítané 
            671-krát
                    
         
     
         
             

                 
                    Stojím osamelo na vrchole kopca a všeobjímajúcim pohľadom sa dívam vôkol seba, do nesmierne hlbokých, farebných diaľok. Zbožňujem ten široký rozhľad ako aj hrdý pocit, že som súčasťou tohto jedinečného pohľadu. Vydržím takto stáť celé veky a snívať o živote, Žiarivom kotúči, energii. Viem, že som potrebný a dôležitý, že patrím do večného, prirodzeného kolobehu bytia. Zároveň však viem, že už dávno nie je všetko v poriadku. Čím ďalej, tým viac vnímam kŕče v mojom tele ... ale najbolestnejší je pocit, ako sa okolo mňa rozširuje samota.
                 

                 V minulosti nás bolo mnoho z môjho druhu, v dávnych časoch nám patrila celá planéta. Patrila nie vo význame majetníckeho vlastnenia - poznali sme, že sme tu za určitým účelom, z nejakého daného, dôležitého dôvodu. Okrem toho, neboli sme tu sami. Žili sme a delili sa o život v pokojnej symbióze s inými bytosťami, ktorým Veľká matka dávala takisto všetko potrebné pre telo i dušu a ktoré boli pre ňu rovnako dôležité a nepostrádateľné. Pomáhali sme si navzájom, naše životy a činnosti neboli bezcieľne, z vývoja a výsledkov našich životných ciest mali úžitok ostatní členovia spoločnosti .... tak isto, ako som ja potreboval život iných druhov a bytostí, potrebovali oni môj. Všetko prebiehalo v zákonitom kolobehu - z prostredia sme dostávali všetko pre život, v nebeskom mieri sme rozširovali svoje rady, navzájom na seba vplývali a po našom predurčenom odchode na Nový začiatok bolo všetko z nás - duša, telo, nadobudnuté vedomosti poskytnuté späť tej, ktorá nám umožnila náš rozvoj, aby nás teraz znova pretvorila a poslala ďalej. Všetky tvory, vrátane môjho druhu, obohacujúce si navzájom svoje bytie a zároveň obohacujúce Najvyššiu bytosť, patrili síce k rôznym spoločenstvám, žili rôznymi spôsobmi, no pre všetky bol mier posvätný a jeho narušenie nemysliteľné. Vlastne, v tých časoch sme si ani poriadne neuvedomovali význam slova mier, keďže sme nepoznali svár, nenávisť, vojnu. Nepoznali, až kým ......   Stojím osamelý vo svojich spomienkach a slastne sa kníšem, keď sa do mňa zaprie všadeprítomný vietor. Vnímam každý jeho jemný pohyb, svojím širokým telom sa mu oddávam a psychoneurónovými spojeniami vnímam rovnaké pocity šťastia svojich druhov, ktorých tu po Poslednom exode, keď duše mnohých museli „vďaka" násilnému odchodu „vykročiť" v ústrety Novému začiatku, neostalo veľa. Nový začiatok ... znie to prívetivo, pokojne, no takým sa Nový začiatok stane iba v prípade predurčeného odchodu, v slávnostnom zmierení duše a tela.   Veľakrát som sa zamýšľal a pýtal, ako dokážu votrelci (takto sme medzi sebou pomenovali novoprišelcov), ktorí sa objavili pred celými vekmi, spôsobovať toľko utrpenia. Nik nevedel povedať, odkiaľ sa vzali, skade prišli. Spočiatku sme ich existenciu vítali a aj keď sme detailne nechápali plány Nebeského bytia, ovládalo nás hlboké presvedčenie, že ich život má svoje opodstatnenie. Nebolo ich mnoho, len niektorým z nás sa „pošťastilo" ich uzrieť. Spočiatku žili votrelci medzi nimi a vedeli sa s nami aj dorozumieť, ale ako sa čas míňal a roky utekali, votrelci stratili schopnosť duše rozumieť iným bytostiam, vcítiť sa do ich pocitov a túžob. Začali si vytvárať vlastné spoločenstvá a izolovali sa od nás. Bez ohľadu na ostatných si začali pretvárať prostredie k svojmu obrazu. Spozorneli sme. Komunikovali sme len o nich, počúvali tých, ktorí ich už videli a mohli opísať tie čudné stvorenia. Všetky existencie, ktoré som dovtedy videl či vnímal, boli úplne iné. Nešlo ani tak o vonkajší fyzický vzhľad, ako skôr o spôsob žitia - na rozdiel od nás si brali od Najvyššej bytosti oveľa, oveľa viac ...  a postupne ich pribúdalo stále viac a viac. Brali si viac, ako potrebovali na prežitie, viac, ako bolo únosné, brali si na úkor ostatných živých bytostí, ktoré akoby ani neexistovali. Nezáležalo im na nikom a ničom okrem seba, nezáležalo im na tom, že „vďaka" ich chamtivosti, sebeckosti a bezohľadnosti museli všetky pôvodné formy života meniť svoj spôsob bytia, ak nechceli predčasne odísť na Nový začiatok. Táto modifikácia spôsobu existencie okrem iného znamenala aj zmenu správania sa voči votrelcom, inými slovami, začali sme sa voči nim brániť. Aj keď sme nikdy nepoužili obranu spôsobom, ktorý by votrelcom fyzicky či psychicky uškodil, títo to vo svojom slepej nadradenosti aj tak považovali za útok a ešte vystupňovali tlak na drahú Najvyššiu bytosť. Postupovali systematicky s cieľom každého, kto im bude stáť v ceste, bez milosti zbaviť bytia. Moji priatelia zomierali v strašných bolestiach, padali jeden za druhým. Votrelci prinášali nám, pôvodným obyvateľom skazu, bolesť a nárek.                                                                                                                                               Prvýkrát som v duši pocítil bolestné kŕče, keď poslali na Nový začiatok celú oblasť Smaragdového kraja. Utrpenie nešťastných tvorov, nedobrovoľne opúšťajúcich tento svet sa šírilo po celej planéte. Nechápali sme to, nevedeli, čo robiť ... dovtedy votrelci likvidovali jednotlivcov, no teraz sa jednalo o nesmierne veľké a bohaté spoločenstvo rozmanitých organizmov, ktoré v agónii vyslali dych berúci a srdcervúci výkrik hroznej bolesti. Všetci sme si uvedomovali tú strašnú stratu životnej energie. Od tohto nešťastia sa to opakovalo v nepravidelných intervaloch, medzi ktorými sa neustále skracovali obdobia pokoja. Exodus striedal exodus. Veľká matka nám postupne zverila rôzne tajomstvá, vidiac, že nemá na konanie votrelcov dosah. Bola milosrdná, keby chcela, mohla ich zmietnuť jediným slovkom. Ale ako v našom, tak ani v jej konaní nikdy nepanovala zášť, nenávisť či pomsta, mohli sme sa len spoločne brániť. Naša obrana však nebola nič platná, nebolo pre nás mysliteľné, aby sme niekomu ublížili. Naša pani nám vysvetlila vznik a zámer života, naše poslanie a úlohy, ktoré plníme. S údivom sme počúvali, ako nám hovorí, že votrelci si sami ubližujú, pretože ich život je nemysliteľný bez nášho, bez životov všetkých stvorení. V bezhlavom šírení svojich bezútešných životov prinášajúcich len spúšť, si privolávali vlastný koniec. Ničili nás a pritom našou jedinou túžbou vlastne bolo svojím bytím slúžiť iným tvorom, áno, aj ničiteľom. Irónia osudu?                                                                                                                                  Avšak nie všetci votrelci boli rovnakí, niektorí pochopili plán Veľkej matky a snažili sa zachrániť, čo sa dalo ... bolo ich ale veľmi málo, ostatní ich druhu ich nepočúvali, vysmiali ich a vyhnali zo svojich domovov. Nepochopení vyhnanci zmierení s budúcim osudom prišli žiť späť medzi nás. V našom kraji som ostal na kopci sám. Prišli aj ku mne, naučil som ich, ako sa dá nažívať v pokoji, v obojstranne prospešnom spolužití a harmónii. Naučil som ich, ako s nami, pôvodnými bytosťami, majú komunikovať. Dlhé hodiny pri mne presedeli, zverovali mi svoje tajomstvá. Tienil som im, keď Žiarivý kotúč pálil ich oči, kryl pred kvapkami, keď padali z Nebeskej brány. Cítil som naplnenie, že môžem pomáhať, že som prospešný pre iných. Boli mi za to vďační. Ale táto idylka nemohla trvať večne, to sme vedeli všetci. Zlé správy sa šírili rýchlo, našimi spojeniami sme cítili ukrutné trápenie, ktoré sa blížilo závratnou rýchlosťou. Votrelci, ktorí nepochopili život samotný, vysali svoje obývané prostredie a presúvali sa tam, kde bolo všetkého dostatok, neuvedomujúc si, že sa pomaly, ale isto presúvajú aj do záhuby.   Stojím na kopci a odovzdane čakám. Vetrík sa so mnou pohráva a ja mu ďakujem, za príjemné chvíle. Za tie roky sa toho veľa zmenilo. Veľa z ľudí, ktorých som chránil, už prešli bránou Nového začiatku, ich rozpadnuté telá odpočívajú v zemi pri mojich nohách. Živých, ktorí pochopili, ostalo málo, vyprahnutý svet sa nevedel postarať o ich potomkov, nuž pomaly vymierali. Pomaly vymierali aj ostatné nevinné stvorenia, ktoré ak nezahynuli rukou votrelca, neuniesli pocity hrôzy a nešťastia, šíriace sa z miest bolestí. Pocity, ktoré som vnímal aj teraz, pocity, ktoré sa, vysielané z umierajúcich bytostí Veľkej matky, znásobovali. Znásobovali sa a silneli. Blížia sa - čím viac je ľudí, tým viac „potrebujú" k životu. Z ľuďmi nepovšimnutého, ojedinelého ostrovčeka života v susednom kraji som dostal správu, že sú na ceste. Sem. Na kopec. Prečo?   Bojím sa. Neviem myslieť na nič iné, ako na to, čo bude. Zaspím.   Budím sa so zlým pocitom. Podomnou stoja votrelci. Vtom zaznie zlovestný zvuk. Články ostrej reťaze sa mi zarezávajú do živej kôry a duše. Bolí to, STRAŠNE TO BOLÍ  ....   V mojom JA prebleskne otázka, na ktorú nikdy nebudem vedieť odpovedať - PREČO???     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Nešťastie ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Mňam a mľask ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Dokonalý svet ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Podpultový tovar?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Pravda
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Vrabec
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Vrabec
            
         
        vrabec.blog.sme.sk (rss)
         
                                     
     
         Som tu na návšteve ... asi ako dieťa - pozorovateľ, aspoň sa o to stále snažím. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    450
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisník "športovca" ... :o)
                        
                     
                                     
                        
                            V MHD
                        
                     
                                     
                        
                            Láska
                        
                     
                                     
                        
                            Oplatí sa vidieť
                        
                     
                                     
                        
                            Čo ma teší ...
                        
                     
                                     
                        
                            Medzi ľuďmi
                        
                     
                                     
                        
                            Čo ma se... srdí ....
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ebola a iné vírusové ochorenia, ktoré strašia našu civilizáciu.
                     
                                                         
                       Prečo milujem svoju ženu?
                     
                                                         
                       Dobré časy, časť prvá: Kanab
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




