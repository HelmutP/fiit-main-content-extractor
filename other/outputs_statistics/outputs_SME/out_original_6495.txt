
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tibor Javor
                                        &gt;
                Zamyslenia
                     
                 Fatima a prekladový slovník 

        
            
                                    14.5.2010
            o
            13:46
                        (upravené
                12.11.2010
                o
                12:36)
                        |
            Karma článku:
                5.50
            |
            Prečítané 
            1152-krát
                    
         
     
         
             

                 
                    „Azízi, svetlo!“ Veta, po ktorej nasledovalo pre diváka šokujúce poodhalenie podstaty v pozadí príbehu. Áno, hovorím o úvode známeho kultového filmu „Piaty element“. Človek by mohol povrchne povedať „brak“. Tak isto by mohol povedať „brak“ na Fatimu, Garabandal, Litmanovú a či Dechtice. Fatimské proroctvá.  Záhadné a fascinujúce. Bolo o nich popísané mnoho, až nakoniec dostali jedno „oficiálne vysvetlenie“.
                 

                 Prečo práve deti. Deti majú pootvorené dvere do informačných oblastí, ktoré si ale príchodom dospelej, všetko racionálne hodnotiacej mysle, zavrú. Škoda. A ešte väčšia škoda je, že je v cirkvi málokto, kto by mal odvahu ich to znova naučiť. Čo keby predsa uvideli za tou bránou niečo, čo nie je kompatibilné s dnešnými doktrínami? Väčšina takých učiteľov totiž neslávne skončila, aj ten najslávnejší, ktorého meno nemáme brať nadarmo. On „maličkých“ dával za vzor...          Pred časom, keď som bol „namotaný“ na zážitky v stave „zmeneného vedomia“, či už pod vplyvom Josého Silvy, alebo Milana Rýzla, zažil som „pootvorení dverí“ viac. Bolo to vzrušujúce, priam ako droga. Pri zemi ma však neustále držali C. G. Jung a onen, vyššie spomínaný učiteľ. To ma priviedlo k akémusi odosobnenému pozorovaniu, ako to vlastne vnímam. Musím povedať, že takéto pozorovanie je veľmi zaujímavé.  „Pozorovanie“, ktoré sa v cirkevnom žargóne nazýva „kontemplácia“, treba zásadne odlišovať od aktívnej meditácie, ktorá môže byť pre neznalého nebezpečná, pretože človek môže podľahnúť záujmom svojho „ega“. Ale ani pri tej kontemplácii to nie je jednoduché.  V prvom rade treba rozlíšiť, z ktorej úrovne daná informácia pochádza. Niekedy to môže byť rácio, inokedy nevedomie, alebo nevedomie kolektívne, s obľubou nazývané „akáša“. Iste, daná informácia prichádza niekedy aj odinakiaľ. Bolo o tom popísané veľa, svätým Pavlom počínajúc a Grofom, Steinerom či Evelyn Underhill končiac, a dokonca i dnešná kozmológia a moderná fyzika naznačujú, že realita vesmíru a bytia v ňom je ukotvená v priestoroch s podstatne viac rozmermi, ako sme schopní aj najlepšími senzormi vnímať a merať. Všetci autori ale narážajú na jeden problém. A tým je, tak ako som si to ja súkromne nazval, „prekladový slovník“...       Ako vlastne človek vníma posolstvá „odtiaľ“? A kto má to vyhradené právo byť „vidúcim“ ako tí fatimskí pastierikovia? Toto sú otázky, ktorými sa cirkev, a nielen ona, zaoberá stáročia. Tá druhá otázka má veľmi jednoduchú odpoveď: Všetci. Problém je, že si to neuvedomujeme a takéto vnímanie dokonca potláčame. V dnešnom racionálnom svete sa síce niečo hovorí o genialite, intuiícii, citu pre hru, či biznis, alebo o „kopnutí múzou“. Geniálnych ľudí však často považujeme za bláznov. Možnože žijú akoby v dvoch svetoch a nerozlišujú odkiaľ ich myšlienky prichádzajú. A pod vplyvom okolia ich to vlastne ani nemá prečo zaujímať. I keď deti a blázni hovoria pravdu. Podstatné však je, že je to pravda jedine ich.       Áno, je to jedine ich pravda, teda v tom zmysle, čo videli a počuli.  Akákoľvek informácia, vstupujúca odkiaľkoľvek do ľudskej mysle je interpretovaná ľudským mozgom do jemu známej formy. Mozog, dalo by sa povedať, pracuje podobne ako procesor počítača, ktorý rozumie iba svojej "reči". Informácia je v rozdielnych typoch procesora interpretovaná rozdielnou postupnosťou binárneho kódu, ktorému sme procesor takpovediac naučili. Výchova, historická epocha a sociokultúrne prostredie formuje našu reflexiu reality. Tú istú informáciu bude jeden mozog považovať za pristátie mimozemšťanov a iný za zjavenie Panny Márie. Podľa toho, čo má vo svojom mozgu a vo svojom nevedomí naprogramované. Kto sa však trocha zaoberal interpretáciou svojich vlastných mimovoľných myšlienok alebo snových obrazov vie, že sa nezaobíde bez vytvorenia akéhosi svojho osobného "prekladového slovníka". Najlepšie pomocou spätnej analýzy. Žiaľ pastierikovia z Fatimy si takýto slovník netvorili. Popísali to, čo videli.        Dlho som si myslel, že onen „prekladový slovník“ je iba môj výmysel. Až nedávno som si na stránke jedného slovenského hnutia prečítal teologický komentár k fatimským zjaveniam od Josepha Ratzingera. Bol som úprimne prekvapený a moje analfabetické vedomosti o tom, ako chápe katolícka cirkev mystiku a zjavenia sa mi aspoň o trošku prehĺbili. A možnože som aspoň trocha pochopil o čo vlastne v Fatime šlo, bez všetkých pribalených kudrliniek o Rusku a podobne. Dokonca si myslím, že interpretácia, najmä tretieho tajomstva, môže byť z pohľadu poznania dnešnej situácie v cirkvi úplne iná, hovoriaca o podstatne vážnejších súvislostiach ako o jednom atentáte. Odlišná od známej interpretácie oficiálnej.              Muž v bielom a pokánie. Pochopili sme tieto dve veci a súvislosť medzi nimi? Muž v bielom kráčajúci cez rozvaliny, prekračujúci mŕtvoly a končiaci sám pod útokmi zo všetkých strán. Ale najmä Ratzingerom zdôraznené trojnásobné slová anjela: „Pokánie, pokánie, pokánie“. Pokánie nie je nič iné ako vnútorná zmena zmýšľania. A komu patrí táto výzva?       Muž v bielom a zmena zmýšľania. Dva veľmi silné archetypy. Dva nadčasové archetypy. Ponad stáročia, akoby nepoznajúc čas, sú aktuálne tak ako pred dvomi tisícročiami.       „Čas nie je dôležitý. Dôležitá je láska“, sú opäť slová z filmu „Piaty element“...                                             foto: (C) Tibor Javor 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Voľne podľa NY Times: Mýty o ruských oligarchoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Biele kone sú na Slovensku legálne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Autobusovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Polemika s Miroslavom Kocúrom o zdiskreditovanej cirkvi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Ad SME: Značka umiera posledná
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tibor Javor
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tibor Javor
            
         
        tiborjavor.blog.sme.sk (rss)
         
                                     
     
         Elektrotechnický inžinier a amatérsky fotograf. Občas s iným pohľadom na tradičné hodnoty. Spoznáte ma aj pod nickom "commandcom". 

 Nejaké moje pesničky nájdete tu: 

 https://www.youtube.com/channel/UC7GEmXuGedNznBj2SzSeL1g/videos 

 a fotografie tu:  

 http://fotky.sme.sk/fotograf/7406/commandcom 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    310
                
                
                    Celková karma
                    
                                                7.64
                    
                
                
                    Priemerná čítanosť
                    2002
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Všeličo
                        
                     
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Potulky
                        
                     
                                     
                        
                            Sally White
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Uznávať Pentaboys?
                     
                                                         
                       Oranžové nafúknuté mŕtvoly v špinavom Dunaji alebo Trans Danube Swim
                     
                                                         
                       Aj ja som teda homofób?
                     
                                                         
                       Rozhovor kresťana s Bohom
                     
                                                         
                       Aj ja som NASRANI
                     
                                                         
                       Rok s mužom v manželstve
                     
                                                         
                       Sedembolestná a ibalgin. A slovenský národ.
                     
                                                         
                       Odpustenie pre minulosť a dôvera do budúcnosti
                     
                                                         
                       Marián Kuffa o zvieratách
                     
                                                         
                       Ak by som bol gay, robil by som lepšie kampane?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




