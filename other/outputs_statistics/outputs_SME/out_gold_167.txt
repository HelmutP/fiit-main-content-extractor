

 
Potrebujeme 3 základné zložky: lístkové cesto, mrazené, nemrazené, menšie, väčšie..., múka na vyvaľkanie, valček a zvyšok, čo kuchyňa dá...
 
 
Základný postup: Zapneme rúru (teplotu znížime po vložení plechu), rozdelíme rozmrazené cesto (väčšie cesto/400 g je ideálne na veľký plech, na menší stačí aj 250 g) na dve časti a spravidla na hladkej múke vyvaľkáme prvú polovicu. (Opatrne, zásada - nikdy nemiesime, len prekladáme na seba jednotlivé vrstvy tak, aby sa nepokazila lístková štruktúra.) Občasný nedostatok ma inšpiroval k použitiu iných (dnes už osvedčených) možností: grahamová múka
- zaujímavým spôsobom ozvláštnila
vzhľad i chuť pri tyčinkách i závine. Pri sladkých obmenách ju môže nahradiť
aj strúhaný kokos, v menšom množstve zmes hladkej múky a nemletého maku (vyzerá efektne).
 
 
1. 
Celý plech: najjednoduchšie a najrýchlejšie pre hladošov. Prvú polovicu cesta rozvaľkáme a dáme na plech (používam papier na pečenie). Naň rovnomerne rozvrstvíme plnku a prekryjeme druhou vrstvou rozvaľkaného cesta. Povrch potrieme rozšľahaným vajíčkom/bielkom (ale napr. olivovým olejom, kečupom, cukrovou vodou, roztopeným maslom...), niečím posypeme (bod 6), popicháme vidličkou (kvôli úniku pary, ktorý by v opačnom prípade znerovnomernil povrch), dáme piecť... Ak podávame ešte teplé, tak ihneď po vyložení opatrne pokrájame na želané tvary (napr. aj trojuholníky), ak sa neponáhľame, krájame až po vychladnutí...
 
 

Plnka: podľa toho, či jedlo má predovšetkým zasýtiť - sladké, slané plnky, mäsové, pre vegetariánov. Mojich pár tipov: zakáľačková kaša (podobná jaternicovej), plnka z mletého mäsa (dá nahradiť aj sójovým mäsom, nadrobno posekanými párkami), zeleninová, zeleninovo/mäsová plnka - napr. kombinácia mäso-kyslá kapusta, tekvica (pozor, aby zelenina nepúšťala veľa šťavy),  tvaroh, mak, orechy... a geburkina jablková
- jablká bez jadierok (so
šupkou), trocha masla, cukor, strúhanka (asi 1:1), škorica, citrónová
šťava, vanilkový cukor (pár minút v mixéri) - jablčník spolu s chladným mliekom je večerou pre celú rodinu. Ak ho chceme vyšperkovať, spodnú vrstvu cesta potrieme roztopeným maslom, a veľmi hustou vrstvou posekaných orechov, hrozienka... a vrchné cesto potrieme a posypeme cukrom, škoricou a orechami. 
 
 
2. Závin: je u nás doma trochu iný ako klasická štrúdľa (na rozvaľkané cesto dáme plnku, zrolujeme a preložíme na plech). Závin je špecifický tým, že cesto plnku len obalí a je všade chrumkavé - a teda aj dobre upečené. Ako na to? Jednoducho... Dva záviny vyšli z jedného cesta a o tejto "špeci" plnke, až na konci (cesto bolo vyvaľkané na grahamovej múke): 
 
 
 
 
 
  
 
 
 
3. Pizza: rozvaľkáme cesto, rozkrojíme na požadované veľkosti, potrieme napr. kečupom (zmesou olivového oleja s bylinkami a cesnakom), naložíme, čo chladnička poskytuje (zvyšky mäsa, údenín, zeleniny, syry) a dáme zapiecť...
 
 
 
 
 
4. Mini-hot-dogy: rozvaľkané cesto rozkrájame na také štvorce, aby z nich párok vykúkal (ak treba, tak ich olúpeme, rozkrojíme - dlhšie aj na tri časti a z oboch strán nakrojíme ako špekáčiky na opekanie). Cesto natrieme kečupom (netreba) a od rohu zrolujeme párky, ktoré poukladáme na plech, potrieme a dáme zapiecť...
 
 
 
 
 

5. Ešte menšie tvary - šatôčky: práca s už vyvaľkaným lístkovým cestom je ideálna aj pre deti. Posýpanie, natieranie vajíčkom, skladanie šatôčiek, aj po jedenie... Podstatou tohto spôsobu spracovania je, že sa zahráme, výsledkom je trochu pomalšia konzumácia, prípadne pohostenie návštevy, ak stihne prísť do času vykladania plechu... 
 
 
 ..  ..  ..   .. 
 
 

 
 
To, že sa niektoré rozchádzajú? Neprekáža, aspoň lákajú chutnou plnkou...
 
 
6. Tyčinky a iné minitvary: Návšteva? Z prvej polovice cesta budú slané tyčinky, z druhej sladké osie minihniezda. Len za pár minút. A tak úžasne chrumkavé a rozplývajúce sa na jazyku... Rozvaľkané cesto najprv potrieme a dochutíme, až potom krájame a prenášame na plech (ktovie, možno by fungovalo aj inak -  prenesený plát na plechu dochutiť a tam len nakrájať, to mi napadlo len teraz)... 
 
 
Povrch cesta: klasickým spôsobom sa cesto potrie rozšľahaným vajíčkom alebo len bielkom a na slané tyčinky  sa sype (vraj skvelé k vínu): soľ, rasca. A teraz naše obľúbené variácie:  rasca, soľ (+ mak, nemletý!),  rasca, soľ, oregano, mletá paprika,  soľ, kari korenie,  zmes - korenie na pizzu, strúhaný syr... a na sladko: škorica, cukor, mak, mleté orechy, kryštálový cukor, farebné posýpky na tortu... keď je cesto takto pripravené, nakrájame tyčinky, no môžeme aj opatrne zrolovať a krájať minikolieska, ktoré potom treba ešte potrieť. Takto (vpravo dole je už jedno osie mini-hniezdo upečené - na porovnanie :-) ): 
 
 
  
 
 
 .  . 
 
 

 
 
Aha, ako dressing sa k sladkým tyčinkám dá použiť aj kyslá smotana alebo jogurt... 
 
 
A na záver, náš "špeci" objav na makovú náplň (dostať už zomletý mak) - dáme do misky, zamiešame so stanoveným množstvo mlieka a cukru, pridáme asi lyžičku škorice, dve lyžice pikantného džemu a jednu čokoládovú zmes - nugetu. Zo zmesi (ak nedáme odjedať), vyjdú dva menšie makové záviny, ako na obrázku hore...
 
 

Všetky tieto podoby lístkového cesta sme robili spolu s deťmi. Ide to, i ten trochu väčší neporiadok v kuchyni stojí za výsledok...
 
 
DOPLNOK 19. 2. 2008 
 
 
"Plnka sa roztiekla a nevyšlo to" bola reakcia, z ktorej som zostala smutná, tak som sa rozhodla skúsiť to trochu inakšie - dnes sme robili závin opäť a vymysleli sme si takmer tekutú plnku a presné miery. Cesto-400 g, piškóty-250 g, čokoládovo-orieškový krém-400 g, vajíčko, múka na pomúčenie a škorica. Variant prvý - piškóty naukladáme a zalejeme čokoládou (dosť suché v tejto podobe), druhý variant je chutnejší - valčekom rozdrvíme piškóty a s krémom, ktorý sme zohriali v mikrovlnke zmiešame. Pozor na prekladanie - prekrývame vrstvu cez vrstvu tak, aby plnka "nevykúkala" .-) Čas prípravy od 16.54 do 18.16 - vrátane umytia riadu, fotenia atď. (na fotkách je čas bez bodky).
 
 
   
 
 
   
 
 
 .   .   .  .  
 

