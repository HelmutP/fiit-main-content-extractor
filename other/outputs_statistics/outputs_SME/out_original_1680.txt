
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivo Nesrovnal
                                        &gt;
                Nezaradené
                     
                 Oprava bratislavských ciest skončila - cyklisti zabudnite. 

        
            
                                    11.9.2009
            o
            0:23
                        |
            Karma článku:
                15.60
            |
            Prečítané 
            11278-krát
                    
         
     
         
             

                 
                    Keď sa na jar v tlači objavili prvé správy o rekonštrukcii bratislavských ciest- Ružinovskej, Záhradníckej, Šancovej, Mickiewiczovej a ďalších, pomyslel som si: konečne! Už bolo fakt na čase. Niektoré z nich pripomínali skôr poľné cesty, než dopravné tepny  hlavného mesta. Čítal som o smelých plánoch na modernizáciu bratislavských dopravných ťahov,  o európskych vzoroch, o moderných technológiách a zvyšovaní životnej úrovne. V duchu som videl cyklistami zaplnené viedenské, mníchovské, budapeštianske ulice a tešil sa, že konečne aj my, v dezolátnej Bratislave, budeme mať niečo pre radosť a slobodu občanov a nie iba pre zisk developerov, alebo turistov.
                 

                  Dnes, po dokončení a spojazdnení zrekonštruovaných ulíc nezostáva než skonštatovať: zase sa na nás, cyklistov, vykašlali. Majú nás na háku. Kto čakal, že Magistrát, alebo miestne úrady, lepšie povedané pán primátor Ďurkovský, alebo starostovia Ružinova a Starého mesta páni Drozd a Petrek, urobia niečo pre nás, bratislavčanov, že vedia kam sa uberá vývoj moderného mestského priestoru, čo je to sloboda pohybu v meste, kvalita života, ako sa vytvára autentická, životaschopná obec a rozmanitá, autonómna, mestská komunita, musel zažiť trpké sklamanie. Nula bodov. Nič, absolútne nič. Po nákladnej –a dobrej- rekonštrukcii hlavných ciest, nemá Bratislava jeden jediný pás pre cyklistov! Jedinú trasu, kadiaľ by sa cyklisti po vozovke dostali do a z mesta. Ako to nazvať? Arogancia? Hlúposť? Ľahostajnosť? Nech si odpovie každý sám.   Pritom široké a rovné vozovky, ako Ružinovská, či Záhradnícka, priam volajú po pásoch pre cyklistov, ktorí by sa pohodlne a slobodne dostali do mesta a späť. Študenti na univerzity, ľudia do práce, do obchodov, dôchodcovia do záhrady, na trh a rekreační cyklisti na hrádzu, do Karpát, alebo len tak. Hocikto, hocikam. Bez áut, autobusov, smradu, dymu, hluku. A to všetko skoro zadarmo, iba trochu farby. Nie, Bratislava na Vás kašle, majte nás radi. Máme iné starosti, ako starať sa o kvalitu Vášho života. To je evidentný odkaz samospráv.    Stovky cyklistov, ktorí týmito priestormi denne jazdia, sú aj naďalej odkázaní na kľučkovanie po chodníkoch, medzi chodcami, kočíkmi, psami a odstavenými SUV s dymovými oknami. Inak sa nikam nedostanete. Nehovoriac o štátnych a obecných inštitúciách, úradoch a stredných a hlavne vysokých školách, kde nenájdete jediný stojan na bicykel. To je určite rarita medzi európskymi hlavnými mestami. A stačí sa iba prejsť po meste, ľudia na bicykloch jazdia a je ich/nás stále viac a  viac. Stavím sa, že aj v  Tirane majú cyklisti lepšie podmienky, ako v „krásavici na Dunaji“, viedenskom TwinCity, Bratislave.    Naši komunálni poslanci a zamestnanci samospráv, primátor a starostovia, ešte nepochopili fakt, že rozvoj autonómnej, alternatívnej dopravy je priamo úmerný úrovni demokracie spoločnosti. Žijú vo svete audín s dymovými sklami, rozmer auta sa rovná významu jeho vlastníka. Ostatní sú bedač. Nerozumejú konceptu, že individuálna, čistá, ekologická a neautomobilová doprava umocňuje pocit nezávislosti, slobody, pohody, skrátka- ten v Bratislave neexistujúci koncept- kvality života. A to nehovorím o zdravotnom hľadisku pohybu, či o odľahčení  katastrofálnej bratislavskej doprave. Asi nežijú v 21 storočí. Je to o to absurdnejšie, že títo ľudia za naše peniaze cestujú na zahraničné poznávacie študijné cesty a prijímajú zahraničné návštevy. Čo tam študujú? A o čom sa s nimi rozprávajú?    Veď stačí vyjsť toť za humná, do prvej rakúskej dediny. Pomaly niet bratislavčana, ktorý by nebol na bicykli pri Neziderskom jazere. Všetci vieme ako to tam vyzerá, určite aj primátor. A všetci tým rakušanom závidíme a ochkáme, ach, aké krásne trasy, a tie vinohrady (ničenie bratislavských viníc je ďalšia téma), ach, prečo to nie je aj u nás? Prečo? A keď na to máme jedinečnú príležitosť, naše samosprávy postupujú amatérsky, slepo, bez invencie, tak ako sa to naučili, keď budovali mesto Mieru, „krásavicu na Dunaji“. A tak to budú robiť stále, donekonečna, resp. dokedy tam budú.    Pritom, nie žeby naše samosprávy nemali dostatok kreatívnych a navýsosť moderných nápadov, ako nám vylepšiť život a spraviť z  Bratislavy jedno vľúdne, príjemné miesto na život. Danube Aréna za pár virtuálnych miliárd, plaváreň za 200 miliónov, hokejový a fotbalový štadión v centre mesta za ďalších pár miliárd (prídu fanúšikovia na bicykli?), paneláky namiesto štadiónu Interu, mediálny komplex na najkrajšom dunajskom nábreží, nákladná letecká doprava, ďalšie a ďalšie mrakodrapy na Chalúpkovej, Mlynských Nivách, „polyfunkčné“ objekty vo vinohradoch... Nápadov je dosť a dosť. Kto by sa zapodieval takými hlúposťami, ako sú cyklistické pásy, verejné priestory, námestia, parky, zeleň, čistota, športoviská. Z toho úplatky od developerov veru nie sú. A bez úplatkov, v Bratislave, starec...    A tak turisti, prichádzajúci po dunajskej cyklotrase, z perfektných rakúskych cyklistických trás, a s nimi posledných pár bratislavčanov, ktorým ešte záleží na tomto meste, budú nemo stáť pred novou krásavicou na Dunaji, týmto Kuala Lumpurom strednej Európy a smutne sa pozerať na stredom mesta sa valiace rieky áut, na naduté provinčné mesto, plné amatérizmu a  priemernosti, pamätník našej malosti a chamtivosti.   Nuž, pán primátor, páni starostovia, ďakujeme. Nejako sa s tými bicyklami ešte snáď pretlčieme.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (116)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Nesrovnal 
                                        
                                            Ďakujem, pán Kiska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Nesrovnal 
                                        
                                            Môj poslanecký odpočet 2010-2014
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Nesrovnal 
                                        
                                            Bratislavčania majú oddnes jasnú voľbu: páni Milanovia alebo nová generácia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ivo Nesrovnal 
                                        
                                            Bratislava má mať Hlavnú stanicu, nie Hlavnú šťanicu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Nesrovnal 
                                        
                                            Páni Milanovia, mňa nezastavíte
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivo Nesrovnal
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivo Nesrovnal
            
         
        nesrovnal.blog.sme.sk (rss)
         
                                     
     
         Bratislavčan, právnik a komunálny politik. Nezávislý kandidát na primátora Bratislavy. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                8.03
                    
                
                
                    Priemerná čítanosť
                    3711
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bratislava má mať Hlavnú stanicu, nie Hlavnú šťanicu
                     
                                                         
                       Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                     
                                                         
                       Bratislava a Praha - dve metropoly, 25 rokov po (revolúcii)
                     
                                                         
                       Oddnes sa na Železnej studienke prestáva ťažiť
                     
                                                         
                       S Radom prichádza do éteru nová telenovela
                     
                                                         
                       Bratislava chce opäť zlegalizovať 400 billbordov
                     
                                                         
                       Hlavná stanica ako výsledok bezradnosti magistrátu
                     
                                                         
                       Ftáčnikov vzorec
                     
                                                         
                       Volebný debakel SMERU-SD prebral aj primátora Ftáčnika
                     
                                                         
                       Bratislava nie je Slovensko, už zase
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




