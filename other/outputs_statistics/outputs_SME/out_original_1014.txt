
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Keď hovorkyňa SMERu káže novinárom o etike 

        
            
                                    21.11.2008
            o
            14:46
                        |
            Karma článku:
                15.25
            |
            Prečítané 
            18448-krát
                    
         
     
         
             

                 
                    Väčšina jej kritiky médií je od veci
                 

                 
Slovo "šašo" je absolútne nemiestne.stv.sk
    Hovorkyňa SMERu Katarína Kližanová Rýsová (KKR) v stredu opäť raz vyzvala slovenské médiá k dodržiavaniu základných pravidiel novinárskej etiky. Podľa nej je strana SMER „znechutená správaním sa niektorých slovenských médií.“ Sťažuje sa oprávnene?     1.    Šašo      KKR: Denník Sme sa v stredu 19.11.2008 opäť zaskvel komentárom Petra Schutza pod názvom „Salva k hranatému výročiu“, kde si okrem iného komentátor dovolil nazvať jedného z troch najvyšších ústavných činiteľov „šašom“, čo už podľa nás prekračuje akékoľvek hranice slušnosti.       Schutz napísal:    Síce ako fraška, ale obrázky z dejín sa zvyknú vracať, ak im nepostavíme hrádze včas, ukázal s veľkou silou víťazný galop Fica (Gašparoviča, Pašku) po slovenských televíziách. Ba dokonca: Za boľševika sme toho videli veľa, ale Husáka so Štrougalom a ešte tretím šašom na hodinu v jednom štúdiu ani raz. Oni to ani nepotrebovali, všetky „médiá“ ovládali.     Mne z toho dosť jasne vyplýva, že šašom bol označený jeden z bývalých komunistických šéfov parlamentu z Husákovej éry, nie niekoho zo súčasnosti. Ozaj neslýchaná trúfalosť tak písať o predstaviteľovi bývalého režimu...      Ako si všimli viacerí, sťažnosť Smeru vonia pokrytectvom. Fico novinárom vulgárne nadáva bežne. Fico mlčal, keď vedľa neho na tlačovke kolega Slota označil uhorského kráľa Štefana (navyše pre katolíkov svätého a pre susedný štát morálny symbol) za šaša.       A Fico rovnako mlčal, keď člen vedenia jeho strany a podpredseda parlamentu Číž nazval  v parlamente šašom opozičného poslanca (sám Číž sa neskôr ospravedlnil).     ;    2.    Kde býva Fico    KKR: Denník Pravda priniesol bombastickú informáciu, že premiér Robert Fico býva v súčasnosti v byte na ulici Čmeľovec, na ktorý má podľa zákona o platových podmienkach ústavných činiteľov nárok. Dokonca byt s väčšou rozlohou mal k dispozícii, a podľa rovnakého zákona, aj bývalý premiér Mikuláš Dzurinda, len v inej lokalite v centre mesta. To ale mienkotvorný denník spomenul len tak na okraj jednou vetou a pokračoval v informáciách o majetku premiéra Roberta Fica, nezabúdajúc pritom opätovne pripomenúť byt od firmy Ikores a vinicu a to napriek tomu, že tieto záležitosti Robert Fico už niekoľkokrát verejnosti vysvetlil. Ale podľa hesla stokrát opakovaná lož sa stáva pravdou, sa denník Pravda snaží neustále navodiť dojem akýchsi pochybností v nadobudnutí tohto majetku.    Aké výhody v úrade požíva premiér „sociálnej“ vlády je úplne legitímna téma. Byt od Ikoresu či skoro zadarmo kúpená vinica sú tiež príbehy na pripomenutie, aj podľa mňa neboli dostatočne objasnené. Pravda píše  o „otáznikoch“ pri týchto kúpach, čo je trefné. Že Fico niečo vysvetľoval neznamená, že vieme o prípadoch pravdu.     3.    Ficova účasť v TV diskusiách    KKR: Rovnako by sme radi poukázali na absolútne absurdné obvinenia zo strany denníka Sme, ktorý sa pravidelne „odbavuje“ na svojej obľúbenej téme – účasť premiéra Roberta Fica v diskusných reláciách. Tak len na margo tejto témy by sme veľmi radi podotkli, že prím v účastiach v diskusných reláciách, ktoré vysielajú slovenské televízne stanice, hrajú od začiatku roka 2008 – Pál Csáky (SMK) – devätnásť účastí - Pavol Hrušovský (KDH) a Ivan Mikloš (SDKÚ) – po sedemnásť účastí – Vladimír Mečiar (HZDS) a Boris Zala (SMER-SD) – šestnásť účastí – Daniel Lipšic (KDH) – pätnásť účastí – a Robert Fico, ktorý od začiatku tohto roka navštívil diskusné relácie dvanásťkrát. Pripomíname, že túto štatistiku uverejnil internetový portál medialne.etrend.sk.    Ukážkový príklad manipulácie KKR. SME a iné médiá nepíšu o tom, že Fico je TV diskusiách najčastejšie, ale že je tam bez politickej opozície. Navyše, KKR sa snaží navodiť citovaním časti štatistík dojem, že opozície je v médiách viac ako koalície. Lenže cituje len vrchol tabuľky. Keby za tento rok sčítala všetkých, vyšlo by jej, že v TV diskusiách ľudia koalície celkovo vystupovali 194krát, kým z opozície celkovo 182krát (štyri vystúpenia Gašparoviča som nezarátal nikde).    4.    Neoznačené konflikty záujmov analytikov    KKR:...rozhovor s ekonómom Eugenom Jurzycom o otváraní druhého piliera, kde autorka rozhovoru síce o pánovi Jurzycovi prináša pár informácií na dokreslenie jeho kompetentnosti hodnotiť druhý pilier, ale akosi pozabudla, asi naozaj len náhodne, na fakt, že spomínaný pán pôsobil aj ako neplatený člen poradných orgánov ministra financií Ivana Mikloša a ministerky práce Ivety Radičovej. V tomto prípade teda ide naozaj o názor „nezávislého“ ekonóma.   To už ani nechceme spomínať, ako slovenské médiá rady citujú „nezávislých“ ekonómov a analytikov, ktorí veľmi ochotne hodnotia ekonomické kroky tejto vlády na čele so stranou SMER – sociálna demokracia. Medzi týmito „nezávislými“ ekonómami dominujú už okrem spomínaného Eugena Jurzycu aj Peter Pažitný – bývalý hlavný poradca ministra zdravotníctva Rudolfa Zajaca; Richard Sulík – bývalý poradca ministra financií Ivana Mikloša a zakladateľ novej politickej strany Sloboda a Solidarita; Jozef Mihál, ktorý sa prihlásil k strane Richarda Sulíka; Vladimír Tvaroška – bývalý štátny tajomník ministerstva financií za ministra Ivana Mikloša alebo Ivan Švejna – člen prestížnej Mont-Pélerinskej spoločnosti, ktorá združuje najvýznamnejších liberálov z celého sveta a ktorý je iniciátorom zavedenia druhého piliera na Slovensku.       KKR osobne o konfliktoch záujmov niečo vie, keďže ako novinárka zároveň brala peniaze za prácu asistentky u poslanca Smeru, a nevidela v tom nič problematické, veď bola „vždy pravicovo zameraná“ a schvaľovala Dzurindove reformy. Preto dnes, logicky, robí hovorkyňu Smeru.     Ale áno, súhlasím, médiá by mali verejnosti naznačiť konflikty záujmov u analytikov, ktorých citujú. Naše médiá to podľa mňa robia správne väčšinou, aj keď nie celkom na 100%. Napríklad v tej tohtotýždňovej debate  TV Joj urobil Daniel Krajcer chybu, keď pri Ivanovi Švejnovi neuviedol jeho spoluautorstvo slovenskej dôchodkovej reformy, o ktorej sa diskutovalo. Ale tam, kde sa Švejna vyjadroval k americkým voľbám či finančnej kríze, to potrebné nie je, stačí uviesť jeho Hayekovu nadáciu, ako to médiá aj spravili.     KKR u niektorých ľudí vyslovene zavádza. Prešiel som si posledný mesiac citácií Vladimíra Tvarošku – nikde nebol označený za nezávislého ekonóma -  novinári vždy uviedli, že je to bývalý štátny tajomník ministerstva financií.  Alebo Jurzyca  - inak môj šéf v INEKO – bol neplateným poradcom nielen u Mikloša, ale aj Schustera a u Magvašiho. Aj ten konflikt je tam už otázny – politici sa neplatených poradcov pýtajú na ich názory, nijak si ich nezaväzujú, na rozdiel od platených funkcií. Podobne KKR pri Sulíkovi určite len náhodou nespomenula, že bol poradcom nielen u Mikloša, ale aj u Počiatka...       5. Údajné chyby SME     KKR: Pre detailné dokreslenie toho, ako sa redaktori a komentátori spomínaných denníkov neštítia narábať s absolútne nepravdivými informáciami, ktoré ale prezentujú ako pravdivé a overené, uvádzame pár konkrétnych príkladov z posledných dní: komentátor denníka Sme Peter Schutz a jeho „excelentne overená“ informácia o klimatizovanom tuneli na Úrade vlády SR za 30 miliónov korún (Fico reisen, 27.10.2008) alebo rovnako „super bomba pravdivá“ informácia od redaktorov denníka Sme Miroslava Kerna a Petra Morvaya o tom, že premiér Robert Fico odmietol rokovať so svojím maďarským partnerom Férencom Gyurcsányom v maďarskom Komárome (Fico vraj nechcel ísť cez most za Dunaj, 13.11.2008). Nehovoriac o tom, že spomínaní redaktori uvedenú informáciu napísali aj napriek tomu, že tlačový odbor Úradu vlády SR jej pravdivosť ešte deň pred uverejnením článku vyvrátil.   V obidvoch prípadoch išlo zjavne o uverejnenie nepravdivých informácií, pričom v prvom prípade bol denník Sme „ochotný“ uverejniť kratučkú opravu po viac ako týždňovom bojkote zo strany Úradu vlády SR a strany SMER - sociálna demokracia v podobe neodpovedania na otázky redaktorov tohto denníka. V druhom prípade sa denník Sme dodnes neospravedlnil.       Schutz chyby priznal hneď na druhý deň na sme.sk, ale je pravda, že oprava v tlačovej verzii prišla zbytočne neskoro. Článok SME o schôdzke premiérov citoval aj názor nášho úradu vlády, aj zákulisné informácie maďarských médií. Nevidím dôvod, aby novinári brali informácie Úradu vlády za automaticky pravdivé. Ale samozrejme dá sa diskutovať o tom, ako  a či vôbec zverejňovať takéto neisté informácie. Ale KKR zavádza, lebo redaktori SME nič nevyhlasovali za pravdivé, už aj v titulku bolo vraj a v článku presne uvedený zdroj ako aj názor úradu vlády.     Celkovo vzaté, Smer ponúka takmer úplne nemiestnu kritiku médií.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (76)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




