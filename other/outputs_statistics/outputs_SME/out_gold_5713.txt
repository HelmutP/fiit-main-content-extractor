

 Zamyslel som sa nad tým a prišiel som k záveru, že opäť SME sa ukázalo, aké v skutočnosti je. 
 Vraj som podnecoval neznášanlivosť voči homosexuálom. A pritom som v článku hovoril aj o úcte k nim, tak ako učí Katolícka Cirkev: „Nemalý počet mužov a žien má hlboko zakorenené homosexuálne sklony. Táto objektívne nezriadená náklonnosť je pre väčšinu z nich skúškou. Treba ich prijímať s úctou, súcitom a jemnocitom a vyhýbať sa akémukoľvek náznaku nespravodlivej diskriminácie voči nim. Aj tieto osoby sú povolané plniť vo svojom živote Božiu vôľu a, ak sú kresťanmi, spájať s Pánovou obetou na kríži ťažkosti, s ktorými sa môžu stretnúť v dôsledku svojho stavu" (Katechizmus Katolíckej cirkvi, 2358).  Teda neviem, ako administrátor čítal tento môj článok, lebo by pochopil, že som nešíril neznášanlivosť voči tejto skupine, len som vyjadril pocit nebezpečenstva, ktoré nášmu národu hrozí. 
 V závere informácii od administrátora je zároveň aj to, že ma prosia, aby som podobné články už nedával na titulku blogu sme.sk. 
 A napr. ten plánovaný pochod homosexuálov neohrozuje naše rodiny? Stačí si prečítať, ako dopadli takéto pochody v iných krajinách a budeme zhrození. 
 Keď niekto chce vyjadriť na verejnosti neprijateľné názory, a média mu to umožňujú, tak prečo ja by som v demokratickej a kresťanskej krajine nemohol na blogu vyjadriť takýto svoj názor, čo tu vždy bolo prijateľné, že rodina je základnou bunkou spoločnosti a normálne manželstvá sú medzi mužom a ženou. 
 (RODINA: muž a žena, dieťa). 
   

