
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Rusina
                                        &gt;
                Camino Santiago de Compostela
                     
                 1. etapa do Compostely alebo ako na mňa trúbili autá 

        
            
                                    6.4.2010
            o
            19:50
                        (upravené
                6.4.2010
                o
                20:56)
                        |
            Karma článku:
                10.45
            |
            Prečítané 
            2592-krát
                    
         
     
         
             

                 
                    Je piatok, 26. marca. Nepodarilo sa mi vstať o šiestej ako som si včera  zaumienil. Vstávam až o pol ôsmej. Raňajkujem dva pomaranče, lúčim sa s  spolubydlom Karolom a bez zdržaní opúšťam izbu. Cestou robím nákup v  mojom obvyklom supermarkete: chleba, 750g dvojfarebná nutela, 100  gramovú čokoláda a dve 1,5 litrové nesýtené minerálne vody. Obsluha tu  býva celkom milá. Pri pokladni mi po nablokovaní predavač želá „Bom  Viage“.
                 

                 Nákup si balím do batohu na nábreží rieky Douro, neďaleko posledného mostu od oceánu.   Odtiaľ šlapem 3640 krokov na autobusovú zástvaku Mercado Bolhão. Bus do  Emersinde, okrajovej časti mesta, mi ide o 9:30. Čakanie si vypĺňam  druhými raňajkami: pečivo so zapečenou šunkou. Som predsa na ceste. Po  vlastných teda začínam od Emersinde o 10:10. Dve hodiny sklz, budem mať  čo robiť aby som to dohnal. Je príjemne slnečno, ale bo oblohe putujú  podozrivé oblaky a skutočne o hodinu sa spúšťa dážď. Prezúvam sa do  dažďovej varianty obutia. Dáždnik je pomorne široký. Spoľahlivo kryje  mňa aj batoh pred návalmi dažďa, len gate odspodu trocha vlhnú. Som plný  sily, krok je svižný. Kráčam po hlavnej ceste N105. Cestou čítam tabule  Alfena, Existo, Agua Longa, Lamelas. Na mape, ktorá v nepremokavom  obale na lanku visí  z popruhu vpredu, mám teda viac obcí. Značenie na  ceste ma teda vôbec neuspokojuje, rád by som totiž vedel, kde sa vlastne  nachádzam, či som zle neodbočil, alebo jednoducho ako ďaleko som sa už  dostal. Občas sa cesta zúži a musím putovať v jarku, v ktorom samozrejme  putuje aj voda. Autá z času na čas po mne vytrubujú.    Stále prší, niekedy aj celkom silno, len občas si to dá na okamih  prestávku. Ja naopak šlapem bez prestávky, akurát pred mestečkom San  Tirso ma dobieha hlad a prvá únava. Po výpadovke vchádzam do mesta. Prvé  gastronomické zariadenie je dáky Snack Bar. Bez zbytočného uvažovania  vchádzam a skladám sa pri jednom stole. Sú dve hodiny poobede. Časník sa  ma pýta, čo si dám. Pýtam sa na ceny. Navrhuje mi ekonomické menu, je  to dáka ryba. Súhlasím. Pýtam si ešte minerálku na pitie. On sa pýta,  kam mám namierené. Ja že, Santiago de Compostela. Prinesie mi opražené  štyri ryby so zemiakmi, zeleninovou omáčkou a ešte k tomu chleba. Riadna  porcia, ale vôbec nemám problém ju zjesť. Keď to odnáša, pýta sa, či si  dám ešte zákusok. Šetrím eurá, teda vravím, že som plný. Je veľmi  usmievavý a milý. Chvíľu sedím a študujem mapu, ale nechcem sa  zdržiavať, tak idem platiť k baru. Čašník vraví, že aby som ešte minútku  počkal. No tak teda dobre. Z kuchyne mi po chvíle prináša mandarínku a  čosi v alobale. Naznačuje mi, že platiť nebudem. Spolu s ostatnými  časníkmi a kuchárkami, ktoré vykukli z kuchyne, mi želajú „Bom Viage“ a  gestami hovoria, nech sa za nich v Santiagu pomodlím. Samozrejme, že  áno. „Muito obrigado,“ zaďakujem a odchádzam.   Tento úžasný obed trval len pol hodiny. Nechcel som sa dlho  zdržiavať, ešte mám kus cesty pred sebou. Prechádzam mestom, tu ma  zdržuje sochársky parčík pri mestskej radnici. Jednoducho to nemôžem zo  len tak odignorovať - študijná deformácia.      Pred mostom cez rieku ešte stretám kláštor San Bento.     Tentokrát  už moje kroky nevedú po hlavnej ceste, ale cez dediny. Fakt je to tu  zle značené. Vlastne obce tu majú podobu dlhých slížov pozdĺž cesty,  ktoré už vzájomne zrástli. Len kde tu sa objaví smerovník, či tabula.  Nedarí sa mi ideálne navigovať a robím kľučky: Bolas, Areias, Avidos,  Landim, Seide-São Miguel, Requião, Montinho... Počasie stále upršané,  len občas sa ukáže slnko. Šlapanie ma už poriadne unavuje, ale nerobím  žiadne prestávky. Snažím sa dobehnúť sklz, čo som si vyrobil neskorým  budíčkom. Niektoré úseky prekonávam ružencom, na iných dopĺňam energiu  ráno kúpenou čokoládou. Autá na mňa trúbia aj tu, ale vďaka ich nižšej  rýchlosti si stíham všimnúť aj povzbudivé gestá ich posádky. Trúbením  vyjadrujú sympatie pútnikom.    K večeru sa dostávam do obce Vale-São Cosme. Nachádza sa v celkom  malebnom údolí. Zaumienil som si aj napriek značnej únave ešte  to  potiahnuť do horského priesmyku, ktorým sa uzatvára údolie. Bude sa tam  ľahko hľadať miesto na prespanie, keďže je to tam pekne zalesnené a  súčasne aj rovnejší terén. Cestou sa strategicky zastavujem v Snack  Bare, kde si kupujem pečivo do ruky a plním fľaše vodou.     V priesmyku som o 19:30, čiže za tmy. Takto sa ťažko hľadá miesto na  spanie. Vyberám si svoju čistinku, napínam igelitový rukáv medzi dvoma  stromami a všetko do neho napchám. Horko-ťažko sa do neho zmestím ja.  Predpokladal som, že to bude o čosi pohodlnejšie. Účel to však spĺňa,  keďže vonku poprchá a ja som ako tak v suchu (zvnútra sa trocha  kondenzuje vlhkosť). V spacáku jem z alobalu, čo mi v San Tirso  nadelili, parádny kus rezňa. Aj napriek nepohodli v igelitovom rukáve a  tomu, že neďaleko je kostolná veža, ktorá pre celé údolie hlasno  oznamuje každú pol hodinu krátkou zvonkohrou a celú hodinu dlhou  zvonkohrou a navyše odbíjaním, zaspávam rýchlo. Veď som spravil vyše  4700 krokov, čo je okolo 35 kilometrov. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            10. etapa do Compostely alebo kto nemá v hlave, ten má v nohách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            9. etapa do Compostely alebo ako som sa neskoro v noci doplazil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            8. etapa do Compostely alebo pôstne putovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            7. etapa do Compostely alebo posledná večera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            6. etapa do Compostely alebo osud premočených smradlavých ponožiek
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Rusina
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Rusina
            
         
        martinrusina.blog.sme.sk (rss)
         
                                     
     
        Momentálne erazmus študent v Porte (Portugalsko)na fakulte Belas Artes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2020
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Camino Santiago de Compostela
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




