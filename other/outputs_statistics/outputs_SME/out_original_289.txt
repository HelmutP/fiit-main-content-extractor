
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Tutková
                                        &gt;
                Súkromné
                     
                 Paľba úplne vedľa 

        
            
                                    12.11.2007
            o
            11:25
                        |
            Karma článku:
                12.11
            |
            Prečítané 
            4362-krát
                    
         
     
         
             

                 
                    Moja reakcia na “objektivitu a vyváženosť" relácie Paľba (5.11.2007) redaktorovi Róbertovi Adamcovi v týchto bodoch:    1. Umelé prerušenie zdravého rozumu  2. Jednostrannosť kampane  3. Možné následky zákazu interrupcií  4. Logika plánovačov rodičovstva  5. Odhady nelegálnych potratov  6. Netransparentnosť kampane  7. Zavádzanie  8. Registrácia CBR  9. Objektivita  10. Medzinárodná spolupráca CBR  11. Vedecké argumenty  12. Pokles počtu interrupcií  13. Hormonálna antikoncepcia  14. Prevencia vs represia  15. Test otvorenosti
                 

                1. Umelé prerušenie zdravého rozumu    Pán Fejér uviedol v upútavke, že relácia sa bude venovať umelému prerušeniu zdravého rozumu... Paľba bola použitá na onálepkovanie kampane, že je v rozpore so zdravým rozumom, čo nijako neprekvapuje, keďže tu boli snahy zo strany Spoločnosti pre plánované rodičovstvo (SPR) označiť kampaň za neetickú prostredníctvom neetického rozhodnutia Rady pre reklamu a rozniesť to po médiách. Etika a zdravý rozum podľa SPR a ich prisluhovačov kážu považovať interrupciu za najzákladnejšie ľudské právo... Teda vopred bolo jasne naznačené, o čo sa relácia pokúsi a pán Adamec to len potvrdil svojími priznaniami na blogu, že chcel ukázať negatívnu stránku kampane, za čo považuje jej jednostrannosť...     2. Jednostrannosť kampane    Predsa rôzne kampane prinášajú rôzny pohľad na skutočnosti. Kampaň Právo na život prináša pohľad z dvoch strán - zo strany nenarodeného dieťaťa ako aj ženy, ktorá je druhou obeťou potratu, čo zdôrazňujeme počas celej kampane. Jednostranne podáva problém interrupcií práve SPR - len z pohľadu ženy, ktorá môže s počatým dieťaťom robiť čokoľvek, pretože ich medzinárodná štruktúra, prevádzkujúca najväčšiu sieť potratových kliník, má záujem, aby jej krvavý biznis ostal legálny. K tomuto názoru ste sa jednostranne priklonil i Vy, alebo žeby ste sa už vopred rozhodol prezentovať tento jednostranný názor Medzinárodnej federácie pre plánované rodičovstvo?    3. Možné následky zákazu interrupcií    Vskutku, skvelá reklama pre SPR zdarma, presne podľa scenáru pani Pietruchovej. Možné následky zákazu interrupcií sú načrtnuté veľmi jednostranne - len zo strany SPR. Nedali ste priestor môjmu argumentu, aké sú možné následky zákazu potratov z nášho pohľadu, čo som Vám dlhší čas vysvetľovala v rozhovore. Podaril sa Vám však úplný opak - úspešne ste predviedol jednu stranu mince - SPR, predstavil všetky mýty, ktoré tu šíri SPR a nenačrtli ste podstatné následky zákazu interrupcií, okrem tých prezentovaných SPR. Vraj naša kampaň zatajuje možné následky zákazu interrupcií... Jediný, kto zatajuje následky zákazu interrupcií, sú práve Plánovači rodičovstva a ich prisluhovači ako Vy, pán Adamec. Následkom zákazu by bolo to, že veľa žien nepodstúpi interrupciu a tak ony ako aj ich okolie budú hľadať iné riešenia. Veľa ľudských životom detí sa zachráni, ako i žien. Zdravie, vzťahy i životy žien sú často postihnuté týmto “slobodným” rozhodnutím. Nezatajujeme, že vždy sa nájdu ľudia, ktorí sa odvážia porušovať zákony, to však neznamená, že musíme legalizovať čokoľvek, čo je zakázané. Dôsledky potratov, ktoré SPR zamĺča je post-aborčný syndróm, spôsobujúce fyzické i psychické poškodenie žien.    4. Logika plánovačov rodičovstva    Váš komentár bol znova jednostranný, keď ste odignoroval môj protiargument, že ženy zomierajú aj na legálne potraty. Naša kampaň je proti legálnym ako i nelegálnym potratom. Podľa logiky SPR a Vašej by sme mali legalizovať krádeže, pretože sa zlodej pri vlámaní môže zraniť. Podľa Klimentovej logiky, treba legalizovať aj znásilnenia, vraždy, či domáce násilie, keďže sa dejú nelegálne a i napriek zákazu. Podľa Pietruchovej logiky, by sme mali legalizovať pedofíliu, ak by ju Holandsko (kde strana založená obvineným pedofilom sa o to usiluje) legalizovalo, lebo veď slovenskí pedofili budú predsa cestovať do Holandska... Radšej im to legalizujme doma. Naša kampaň je proti násiliu, ktoré sme videli na bilboardoch po Slovensku, čo však SPR propaguje ako najzákladnejšie ľudské právo!    5. Odhady nelegálnych potratov    Propotratové organizácie, akými je aj Svetová zdravotnícka organizácia, uvádzajú len odhady, keďže neexistuje oficiálna štatistika nelegálnych potratov. Tie sú často preháňané práve zástancami potratov. Keby som počula Váš komentár k štatistike SZO o ženách zomierajúcich na nelegálne potraty, pridala by som túto štatistiku OSN - takmer 50 miliónov potratov ročne! Porovnajte si to so 40 mil. obetí 2. svetovej vojny, či 100 miliónmi obetí komunizmu. Dnešná ideológia má najväčší počet obetí na nevinných deťoch ako aj ich matkách.  Je propagovaná ako ľudské právo a financovaná vládami a medzinárodnými inštitúciami vyspelých krajín.    6. Netransparentnosť kampane    Mne ste tvrdil, že reportáž chcete nechať otvorenú a prezentovať zhrnutie ku kampani... Ak by Vás to skutočne zaujímalo, tak by ste uverejnil moje vysvetlenia a pýtal sa na iné, ale Váš cieľ bolo vystrihať, čo sa Vám hodilo do scenáru podľa Pietruchovej. Hovoril ste mi pravdu, ak teraz priznávate, že ste zaujal stanovisko o netransparentnosti, zavádzaní a čierno-bielej optike kampane...? V čom je kampaň netransparentná? Že ju spolufinancovali desiatky jednotlivcov a niekoľko firiem? A ak vám poviem ich mená, hoc všetky nepoznám, tak aj ich znemožníte za čierno-bielu optiku? Ľudia majú právo na súkromie (nielen ženy) a teda poskytnúť peniaze bez toho, aby sa to zverejňovalo. Naša kampaň nie je politická strana, aby mala povinnosť zverejňovať zoznam donorov.     7. Zavádzanie    “Zavádzanie” je jedno z obvinení zo strany SPR, ktoré už niekoľko týždňov prezentovali a vy ste ich nekriticky prebrali. Naša kampaň nijako nezavádza. Od začiatku sme otvorene hovorili, že náš bilboard zobrazuje 11. týždňové dieťa a že sme za absolútny zákaz potratov, nie iba tých na žiadosť. Všetky média to tak prezentovali. Na Slovensku sú potraty dovolené až do pôrodu v prípade vážneho poškodenia plodu, či ohrozenia života matky. Do 24. týžňa je interrupcia legálna z dôvodu genetickej vady plodu. Tu treba povedať, že aj na Slovensku sú potraty nelegálne, a to sú potraty na žiadosť v 2. a 3. trimestri. Mimochodom aké sú reálne dôsledky? Vieme o nejakých prípadoch nelegálnych interrupcií, či úmrtiach žien pre nelegálne interrupcie? Je zjavné, že ľudia dodržujú zákon a to je jedna z demytologizácií interrupcií. Nezavádza náhodou SPR? Na potrat je možné ísť bez akéhokoľvek dôvodu do 12. týždňa tehotenstva, čím vyhláška prakticky dáva zelenú zabíjaniu detí do 10. týždňa a 6. dňa (výpočet ku koncu týždňa), čo znamená, že dieťa je takmer tak veľké ako plod na našom bilboarde. Ultrazvuk pri výpočte veku nie je vždy úplne presný. Na našom webe si môžete pozrieť aj mladšie plody, ktoré majú rovnako ľudskú podobu. Dôvod, prečo sme nevybrali z archívu mladší plod bolo to, že na fotkách z archívu CBR sú všetky tieto roztrhané na franforce. Toto by nám neprešlo v agentúrach.    8. Registrácia CBR    To, že CBR bolo zaregistrované až 31. 8. neznamená, že nemohlo vykonávať činnosť predtým ako združenie občanov. Niekoľko mesiacov predtým sme začali fundraising na kampaň, zároveň sme pripravili stanovy a predložili ich na registráciu MV SR počas letných mesiacov, čo tiež trvalo niekoľko týždňov. Ak by ste sa ma na to opýtal, rada Vám to vysvetlím, lenže Vás pravda nezaujímala. Zámerne zakrývate fakt, že Pastor Bonus funguje a je zaregistrovaný od roku 2005.    9. Objektivita    Tvrdíte, že ste “oslovil najmä ľudí, ktorí majú na problém interrupcií iný názor.” Zisťujem, že panujú aj iné názory na to, čo je objektivita. Podľa Vás stačí dať posledné slovo najväčšiemu oponentovi kampane. Iný názor na kampaň Vás nezaujíma. Objektivita však vyžaduje, aby ste oslovili ľuďmi z rôznymi reakciami na udalosť, akou bolo spustenie našej kampane. Je to práve nepochopenie pojmu objektivita, v čom profesionálne zlyhávate. Objektivita iste nie je to, že vystriháte vyhlásenia, ktoré Vám zapadnú do scenára objednaného SPR a nedáte žiadnu možnosť reagovať na ich vyhlásenia. Nehovoriac, že ste ma pri dohadovaní rozhovoru podviedli. Žiaden z mojich hlavných argumentov, ktoré ste ma uistil, že zverejníte, sa v reportáži neobjavili.     10. Medzinárodná spolupráca    Nezakrývam spoluprácu s oboma pánmi, ktorých spomínate. Tiež som špecifikovala, o akú spoluprácu išlo, či ide. Od začiatku otvorene hovorím o tom, že pre kampaň sme od CBR v Kalifornii dostali len video a fotodokumentáciu. Možno Vám to príde neuveriteľné, ale dotácie pobočkám nie sú štýlom CBR. Nielen Bruno Quintavalle sa chcel prísť pozrieť na kampaň a podeliť sa o skúsenosti, ale sme v kontakte aj s inými prolifermi v Európe a niektorí nás tiež navštívili počas kampane.    11. Vedecké argumenty    Nechcete veriť našim vyhláseniam ohľadom financovania, rovnako ako nechcete veriť iné fakty, či štatistické alebo vedecké, napr. že život človeka začína počatím. Presne postoj, či stratégia SPR:  nebavme sa o vedeckých argumentoch, keďže je to hlavný argument kampane Právo na život, o ktorý sa opierajú organizátori kampane. Tejto téme ste nechcel dať priestor zámerne.  Vhodne sa skrývate za to, že konsenzus neexistuje, čo je však práve pseudoargumentácia SPR. Vždy sa nájdu ľudia, ktorí majú problém akceptovať fakty, aj vedecké. To však neznamená, že vedecká argumentácia je irelevantná. V diskusii o potratoch má najviac náboženskej terminológie v ústach práve pani Pietruchová. V súvislosti so skrývaním sa dá poukázať iba na Vás, pretože zámerne skrývate fakty o prenatálnom vývoji, ktoré Vám prezentovali organizátori. Fantasticky ste znova zvládol prebrať nálepky SPR, ešteže ste nedodal ich blud o ochrane gamét...    12. Pokles počtu interrupcií    Ohľadom poklesu interrupcií v SR Vám zjavne nešlo o skúmanie faktorov, totižto ste si vystačil s názorom SPR, že hlavným faktorom je zásluha SPR, že tu propagujú antikoncepciu. Môj protiargument je, že počet potratov sa znižuje vďaka prolife osvete. Porovnajte to napríklad s Britániou, kde táto klíma pre prolife hnutie neexistuje a kde oveľa dlhšie pôsobí Plánované rodičovstvo. V tejto krajine, kde je miera používania antikoncepcie asi najvyššia v Európe, je distribuovaná mladým zdarma (teda za peniaze daňových poplatníkov) a sexuálna výchova sa učí už desaťročia, počet potratov neustále stúpa, za posledný rok o 4 percenta. Metódy plánovačov rodičovstva sú vskutku “úspešné”, pretože potratové kliniky na tom zarábajú, hoc sú registrované ako charities. O Holandsku, často prezentovanom SPR ako ďalšia “success story”, napíšem viac na mojom blogu. Jediné, čo je v praxi overiteľné ako ich úspešná praktika, je že Plánované rodičovstvo (IPPF) dobre zarába a funguje ako marketingová agentúra pre farmaceutické firmy.     13. Hormonálna antikoncepcia    Váš príspevok ohľadom hormonálnej antikoncepcie Vás usvedčuje z ignorancie a slepej dôvere SPR. Prečítajte si niečo o núdzovej hormonálnej antikoncepcii, ktorú propagujú a garantujú plánovači rodičovstva. Zahmlievajúce “pôsobenie na endometriu maternice” znamená práve to, že už oplodnené vajíčko sa neuhniezdi a preto sa po 5-6 dňoch života vyplaví z maternice a zahynie. Tiež si naštudujte viac o tomto mechanizme (popri primárnom, ktorý bráni ovulácii, či sekundárnom, keď sa zhusťuje hlien v kŕčku maternice, aby sa spermia nedostala do styku s vajíčkom) v prípade väčšiny hormonálnej antikoncepcie a nespoliehajte sa slepo na mýty SPR. Existuje evidencia, že jedným z možných efektov hormonálnej antikoncepcie je práve bránenie uhniezdenia vajíčka, čo sa úmyselne zakrýva. Toto je chemický spôsob ničenia počatých detí. Naozaj ste našli toho "najobjektívnejšieho" gynekológa, aby sa vyjadril k tomu, čo som povedala...    14. Prevencia vs represia    Ako preventista navrhujete teda zrušenie zákazu vraždenia, krádeže atď., lebo sú represívne? Skúsme to iba s prevenciou? Žiaľ, napriek prevencii mnohí sa SLOBODNE rozhodnú kradnúť a vraždiť. Zákony nie sú pre čnostných, ale pre tých, ktorí sú práve v komplikovanej situácii, pokúšaní, labilní v princípoch, sebeckí atď. Potom zrušme právnu ochranu aj dospelých, legalizujme vraždy, lebo zákaz je represiou voči potenciálnym vrahom. Vám nielenže chýba fantázia, ale i myslenie mimo mainstream a kritické uvažovanie nad tým, čo automaticky preberáte od SPR. Nikto nespochybňuje, že istý zlomok jednotlivcov bude porušovať zákon. Zákaz však mnohých od neetickej činnosti odradí, čo však úplne ignorujete.    15. Test otvorenosti    Ak naozaj hľadáte pravdu, chcete odkrývať mýty a klamstvá, posvieťte si na plánovačov rodičovstva. To by asi Paľba musela zamestnať nejakého iného novinára, ktorý by sa lepšie vedel odosobniť od svojich názorov a šiel do hĺbky. S Vašim šéfom publicistiky, ktorý si rovnako myslí, že to bolo objektívne, by ste však vyrobil asi len ďalšiu bezplatnú reklamu pre SPR a podporil tak potratový a antikoncepčný priemysel, ktorý už aj tak dnes dobre zarába - na zničenom zdraví a životoch žien a ich detí. Vaša reportáž na objednávku splnila svoj účel a SPR Vám je iste zaviazaná. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Cirkev učila „darwinizmus“ dávno pred Darwinom!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Do nového roka: Viac bezpečnej lásky!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Sme provokatívni – prinášame pohľad, ktorý je nepohodlný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Keď sa darovanie orgánov stáva vraždou...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Randenie nielen pre fundamentalistov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Tutková
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Tutková
            
         
        tutkova.blog.sme.sk (rss)
         
                                     
     
        žena slobodnej mysle, ktorá v srdci nosí túžbu pomôcť najchudobnejším z chudobných - nechceným embryám
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6106
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.cbreurope.sk
                                     
                                                                             
                                            www.abortionfacts.com
                                     
                                                                             
                                            www.familychoices.info
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




