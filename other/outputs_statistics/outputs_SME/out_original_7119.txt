
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alexander JÁRAY
                                        &gt;
                Kvantová matematika
                     
                 Tretí dialóg s kvantovým chemikom. 

        
            
                                    23.5.2010
            o
            18:20
                        (upravené
                26.6.2010
                o
                17:05)
                        |
            Karma článku:
                1.11
            |
            Prečítané 
            624-krát
                    
         
     
         
             

                 
                    No a táto, štátom financovaná, mentálne zaostalá, bodovo matematická háveď, spolu s jej obeťami, relativistickými fyzikmi odvažuje si ma nazvať bláznom, jedine preto, že nemienim rozmýšľať ako oni, ale iba tak ako rozmýšľa materiálna príroda. Malá, ale zásadná pripomienka, overením zmeny hmotnosti telies pri ich pružinovom, či kyvadlovom pohybe, vyvrátili by sa ja Einsteinove bludy o relativite pohybu matérie. Udivuje ma aj tá skutočnosť, že drvivá väčšina diskutérov k mojím vedeckým témam, je viac ako fyzikálne a matematicky, podkutá gramaticky. Usudzujem z toho, že oni majú hlavne výhradu k môjmu pravopisu, nie však k mojím matematicko – fyzikálnym argumentom. A tým končím, tvá Máňa.
                 

                 
  
    aaaaaaaaaaaaaa      Dôležité!        
       Rozumný človek sa prispôsobuje svetu, nerozumný trvá na tom, že sa bude pokúšať prispôsobiť svet sebe. Preto každý pokrok závisí na nerozumných ľuďoch.      
 (George Bernard Shaw)         Tretí dialóg s kvantovým chemikom.   Môj spolu diskutujúci kvantový fyzik, položil mi ďalšiu otázku v nasledovnom znení.      Otázka č. 7.   “ Vo svojom príspevku ste popísali Vašú tzv. materialistickú, alebo  kvantovú matematiku. Podľa nej číslo musí mať svoju fyzicky  odmerateľnú pozíciu na číslnej osi, danú podľa nejakej zvolenej  dĺžkovej jednotky, či "dĺžkového kvanta". Nemyslíte si, že takého materialistiké ohraničenie definície čísel nás  obmedzuje pri matematických a fyzikálnych úvahách ? Zoberme napríklad, že takto "najmenšie" kvantové číslo bude nejakých 10(-n), avšak  nejaké presné merania dajú údaj ešte menší, 10(-n-m), (m,n sú kladné  celé čísla). Potom by sme museli sústavne znižovať hodnotu dĺžkového  kvanta podľa toho, aký najmenší objet by sme dokázali odmerať.   M.I       Moja odpoveď na uvedenú otázku znie nasledovne:   Vami položená otázka je síce na prvý pohľad logická, ale iba v rovine myslenia súčasnej relativistickej vedy. Hluchá a slepá materiálna príroda, takúto otázku by nikdy nepoložila.      Slovo materiálne kvantum, v JÁRAYovej Kvantovej matematike, neprejudikuje konečnú, národnú, a preto iba jednu dĺžkovú hodnotu materiálneho elementu, ktorého konečný súčet tvorí syntax, (stavbu) čiže dĺžku materiálneho priestorového parametra, ale skutočnosť, že každý reálny materiálny parameter, je tvorený z konečného, (nie z nekonečného ani nie z limitného) celočíselného počtu (súčtu) základných materiálnych elementov, atómov konkrétneho chemického prvku, teda z objektov majúcich (pokojovú) hmotnosť.    Nekonečná deliteľnosť ničoho je opísaná podrobne na adrese:   http://jaray.blog.sme.sk/clanok.asp?cl=94839   Pritom tie základné elementy musia na viac spĺňať ešte aj tieto dve podmienky.      1. Musia mať konečnú, (nie neustále sa zmenšujúcu, nie limitnú) hodnotu !      2. Ich začiatky nesmú splývať s ich koncami, čiže v žiadnom prípade nesmú byť (nezmyselnými, metafyzickými) matematickými bodmi !      Následky dodržiavania, či nedodržiavania  uvedených dvoch podmienok, budem prezentovať na nasledujúcich dvoch obrazoch.                          Na obraze č. 1, je priestorovo (plošne) znázornená funkcia y = ½.x2, vytvorená z matematických bodov, teda z úsečiek, ktorých začiatok splýva s ich koncom.    V skutočnosti na tomto obraze je znázornená iba, myšlienková predstava matematikov o priestorovej aplikácii funkcie y = ½.x2.   Platnosť tejto funkcie zaniká v tom momente, keď  ju znázorňujeme v objektívnej realite, čiže na materiálnej ploche, pomocou materiálnych elementov, čiže pomocou atómov kriedy, tužky, či pomocou atómov nejakej farby.    (Pomocou hmoty s pokojovou hmotnosťou.)   Striktná platnosť matematickej funkcie y = ½.x2,  je akceptovateľná iba pre prípad, že táto funkcia nie je implantovaná do reálneho materiálneho priestoru.    Ona platí Iba vtedy, keď sa nachádza v hlavách matematikov.   Bodový (nezmyselný) charakter výstavby matematickej funkcie y = ½.x2, prezrádza až tá skutočnosť, že táto funkcia (ako aj všetky  ostatné funkcie)  v súčasnej (nezmyselnej bodovej) matematike, má iba jedinú hodnotu prvej derivácie, ktorej hodnotu tvorí dĺžka jediného rezu funkčnej plochy trojuholníka, vedeného kolmo na os x v bezrozmernom, či limitnom bode x.               Na obraze č. 2, je priestorovo (plošne) znázornená funkcia y = ½.x2, vytvorená z atómov konkrétneho chemického prvku, teda z úsečiek, ktorých začiatok nesplýva s ich koncom.   Materiálny, kvantový, atómový charakter výstavby matematickej funkcie y = ½.x2, znázornenej na obraze č. 2, prezentuje tá skutočnosť, že predmetná funkcia má dve rozdielne hodnoty prvej derivácie predmetnej funkcie, ktorých hodnotu tvoria dve rôzne dĺžky rezu funkčného trojuholníka vedených kolmo na os x.    Raz na začiatku materiálneho kvanta Dx, x1 a druhýkrát na jeho konci x2.       V JÁRAYovej Kvantovej matematike, funkciu y = ½.x2  tvorí takzvaný kvantový, pyramídový trojuholník.    A takýto kvantový trojuholník, dá sa rezať iba na začiatku a konci syntaktických (stavebných) kvánt, lebo materiálne kvantá, teda atómy chemických prvokov sú nedeliteľné, iba zničiteľné !!!   Delením sa atómy chemických prvkov rozpadávajú na iné materiálne časti, ktoré z pôvodnými vlastnosťami atómu nemajú nič spoločného.    Delením atómu vodíka (Hidrogén) zaniká samotná podstata atómovej štruktúry hmoty.       Nech by boli materiálne dĺžkové kvantá akékoľvek krátke, vždy by pre uvedenú funkčnú závislosť tvorili iba pyramídický a nikdy nie  hladký, matematikmi vysnený bodový trojuholník !!!         Súčasná bodová matematická obec nie je schopná za žiadnych okolností akceptovať inú aplikáciu derivácii funkcie y = ½.x2, ako teoretickú, ako jednohodnotovú, čiže inú ako bodovú!      Predstava dvoch rôznych hodnôt derivácie funkcie y = ½.x2  matematikov desí, ba až uráža do špiku kosti.    Ako ateistov exaktný dôkaz existencie Boha.       Následkom toho, bodovou matematikou osprostení relativistickí fyzici, nie sú schopní za žiadnych okolností akceptovať rôznu, dvojitú hmotnosť telies, vznikajúcu pri spomalení a pri ekvivalentnom zrýchlení telies, bez ktorých by nemohol existovať pohyb pružiny, ani pohyb kyvadla.   A preto nie sú ochotní za žiadnych okolností, túto (podľa mňa úpne triviálne logickú) materiálnu realitu epxerimentálne overiť na pohybe pružiny, či na pohybe kyvadla.   A tak bodová matematika neprispieva k poznaniu reálnych parametrov materiálnej prírody, ale zavádza fyzikov (a celé ľudstvo) do ríše krkolomných relatívnych bludov, v lepšom prípade do štatistickej fyziky.       Z JÁRAYovej Kvantovej matematiky dvojitá hodnota derivácie funkcie y = ½.x2  jednoznačné vyplýva a z JÁRAYovej Kauzálnej fyziky zase jednoznačne vyplýva dvojitá hmotnosť telies vznikajúca pri ich spomalení, ako aj pri ich ekvivalentnom zrýchlení, ktorá tvorí existenčný princíp pohybu pružiny i pohybu kyvadla.       No a táto, štátom financovaná, mentálne zaostalá, bodová matematická háveď, spolu s jej obeťami, relativistickými fyzikmi, opovažuje si ma nazvať bláznom, jedine preto, že ja nemienim za nič na svete rozmýšľať tak ako oni, ale iba tak, ako rozmýšľa materiálna, hluchá a slepá príroda.   Malá, ale zásadná pripomienka.    Overením zmeny hmotnosti telies pri ich pružinovom, či kyvadlovom pohybe, vyvrátili by sa aj Einsteinove bludy o relativite pohybu matérie.          Fyzik Wilhelm Muller hovorí: „ Teóriu relativity vymysleli u písacieho stola  matematici, ktorí stratili zmysel pre prírodu, čo je problém zaváňajúci mágiou .... Závery Teórie relativity sú absurdné, ale relativistom práve táto absurdita pripadá ako znamenie najvyššej oduševnenosti“.    
  Udivuje ma aj tá skutočnosť, že drvivá väčšina diskutérov k mojím vedeckým témam, je viac ako fyzikálne či matematicky, podkutá gramaticky. Usudzujem to z toho, že oni majú hlavne výhrady k môjmu pravopisu, nie k mojím matematicko – fyzikálnym argumentom.          "A tým končím, tvá Máňa".                        
       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Overte si, či ste múdrym človekom s IQ 201 bodov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Ženský genetický kód!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Experimentálny dôkaz neplatnosti „Všeobecnej teórie relativity“ pohybu matérie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Veritas vincit (Pravda víťazí.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Čas sem, čas tam, nám je to všetko jedno.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alexander JÁRAY
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alexander JÁRAY
            
         
        jaray.blog.sme.sk (rss)
         
                                     
     
        Pravde žijem, krivdu bijem, verne národ svoj milujem. To jediná moja vina a okrem tej žiadna iná.
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    255
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1326
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kvantová matematika
                        
                     
                                     
                        
                            O zločinoch vedcov
                        
                     
                                     
                        
                            Kde neplatia zákony fyziky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Overte si, či ste múdrym človekom s IQ 201 bodov.
                     
                                                         
                       Ženský genetický kód!
                     
                                                         
                       Veritas vincit (Pravda víťazí.)
                     
                                                         
                       Čas sem, čas tam, nám je to všetko jedno.
                     
                                                         
                       Kvíkanie divých svíň.
                     
                                                         
                       Ako nemohol vzniknúť priestor a hmota.
                     
                                                         
                       Odpoveď na otázku z Facebooku.
                     
                                                         
                       Moja dnešná elktornická pošta (26. 2. 2014).
                     
                                                         
                       Kde bolo tam bolo, bola raz jedna „Nežná revolúcia“.
                     
                                                         
                       Zákon zachovania hmoty.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




