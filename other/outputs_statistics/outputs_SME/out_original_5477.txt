
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Babinová
                                        &gt;
                Súkromné
                     
                 Už som sa nehodila,no niekto tam hore pri mne stál - čásť druhá. 

        
            
                                    29.4.2010
            o
            10:30
                        (upravené
                29.4.2010
                o
                10:35)
                        |
            Karma článku:
                5.83
            |
            Prečítané 
            1270-krát
                    
         
     
         
             

                 
                    Pozerám sa okolo seba, no okolie nepoznám. Nikdy som tu nebola. Všetko je cudzie a okrem zimy pociťujem i strach. Cítim, ako sa mi nafukuje bruško. Som veľmi hladná. Nejedla som už niekoľko dní, i sprcha by sa zišla.
                 

                 Prechádzam okolo plotu, chcem sa pozrieť do vnútra. Možno tam niečo nájdem, čo by som zjedla a pôjdem ďalej. Výhodou je, že som maličká a môžem sa pretiahnúť cez malé otvory úplne bez problémov. Ha, podarilo sa! Pomaly prestupujem z nohy na nohu, nepoznám to tu a nechcem, aby ma niekto videl. Cítim sa hrozne. Kvôli hladu som prišla na cudzí dvor a snažím si niečo nájsť. Som ako zlodej. Je mi smutno, ale pocit hladu je silnejší než moje svedomie. Obzerám sa a hľadám,zabudla som na svet okolo mňa a to bola chyba. Oproti mne sa vyrútilo akési veľké čudo, ktoré beží smerom ku mne.Ách, veď to je veľký pes.No vôbec nevyzerá,žeby sa chcel hrať.Beriem nôžky na plecia a utekám.Pretiahla som sa medzerou v plote a o kúsok ešte odbehla. Stál za platom a veľmi mi nadával. „Dobre, prepáč.Viem, že pánom si tu Ty, nechcela som." Niečo si ešte zamumlal, no už som ho nevnímala a pobrala som sa ďalej.   Všade okolo mňa boli domy. Veľké, malé, všade bola nejaká záhradka. Pozerala som do okien, v ktorých sa svietilo. Určite sa už svieti i u nás doma. Len ako sa tam dostať? Zase začalo pršať, chcela som sa niekam schovať. Nebolo kam. Skúsila som ešte raz vstúpiť za iný plot, no bola som nepozvaný hosť. I tam už niekto býval a tá veľká slečna, mi dala hlasne najavo, aby som odišla. Pridala som do kroku a šla po dlhej ceste, keď tu zrazu niekto zapískal. Obzrela som sa a videla som toho človeka, ktorý sa na mňa chodil pozerať. Nie, teraz nie - pomyslela som si. Naozaj som dosť vysílená, než aby som utekala. Ten človek si nedal povedať, pridal do kroku a snažil sa ma dohoniť. Začala som utekať. Po viac ako týždni a pol, bez jedla, na vetre a daždi sa ženiem dole ulicou a za sebou mám niekoho, koho nepoznám a neviem čo odo mňa chce. Videla som kríky, skočila som tam a čakala. Po nejakom čase som zaspala a spala až do rána.   Po prebudení som vyčkala, kým sa ulica ukľudnila. Prešlo okolo mňa veľa áut. Potom som vyšla a stála som na ceste bez toho, aby som vedela čo ďalej. Už som to začala chápať. Naši ma nezabudli, nechali ma tak. Prebehujem medzi domami, ktoré nepoznám, nikto ma nehľadá, nikto nepotrebuje. Som sama. Chce sa mi zase spať, hlad už prestávam vnímať. Uľahujem na kraj cesty a pomaly zaspávam, keď odrazu počujem: „ Aha, tu je tá tuláčka!" - pozriem hore a zas. Stojí tam ten podivný človek, už nie je sám. Vedľa neho stoja ďalší dvaja a pozerajú na mňa. Počujem okolo seba hovoriť pánov dvorov. Ozývajú sa z každého dvora. Čo sa deje? Na ulicu prišli ďalší dvaja, majú maskáče, sú to vojáci. Pomaly si sadám a obzerám sa okolo seba. Som sama, oni piati. To nie je dobrá šanca. Začali sa pomaly rozostupovať a posunkami si niečo ukazovali. Chcú sa hrať? Ďakujem, nemám náladu. Otáčam sa a odchádzam. Počula som dupot a tak som sa obzrela. Bežali. Dvaja oproti mne, dvaja z ľavej strany, jeden z pravej strany. Pchá, myslia si, že nemám rozum a neviem utekať? Rozbehla som sa smerom k veľkej ceste, ešte som tam nebola, no za dva týždne čo sa sem pohybujem som videla, že tam chodia autá z tejto oblasti a ešte také veľké, ktoré vydávajú veľkú hluk. Tých sa dosť bojím, no teraz mi to bolo jedno. Bežala som, kľučkovala, tak ako som malá, som i rýchla. Približovala som sa k tej hlavnej ceste, keď na mňa niečo dopadlo. Čo to je? Okolo mňa je tma. Som vyľakaná, ostávam stáť. Cítim, ako sa odlepujem od zeme, niekto ma berie na ruky. Odkryl mi z tváre to,čo na mňa hodil a tak zisťujem, že je to obyčajná deka. Aha, ďalší. Toho som si pred tým nevšimla. Držal ma pevne, no nebolelo to. Pozerala som na neho a čakala. Dobehli ostatní. Jeden vyťahuje bielu stužku a omotáva mi ju okolo pusy. Druhý má na rukách gumené rukavice, berie ma na ruky, deku omotáva tuhšie okolo mňa. Má pocit,žeby som chcela ujsť a tak už len pre zachovanie vlastnej hrdosti sa o to pokúsim i keď viem, že veľkú šancu na útek už nemám. No paráda. Toto je ako lov na leva, akurát nemám hrivu a neviem tak silno kričať. Dobre, teraz by som ani nemohla, tá stužka nie je silno omotaná, ale i tak ju okolo pusy mám a chce sa mi kýchať, šteklí ma na nose. Zabalená v deke, so stužkou okolo pusy odchádzame. V náručí človeka s gumenými rukavicami niekam odchádzam. Tí chlapi si pokojne podávajú ruky, usmievajú sa a pyšne kráčajú vedľa mňa. Ideme smerom k jednému z domov, okolo ktorého som dva týždne chodila. Vošli sme na dvor, otvorili dvere do domu. Nič som nepovedala, len tíško hľadela na neznáme tváre.   Či som sa bála? Áno a veľmi. Cítila som tlkot vlastného srdiečka a čakala, čo sa bude diať.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Nebyť dokonalá sa oplatí.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Kedy končia kamarátstva?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Nastavujem zrkadlo.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Už nehovorím dcére: " Neplač!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Vyznanie.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Babinová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Babinová
            
         
        evababinova.blog.sme.sk (rss)
         
                                     
     
         Som mama, manželka, dcéra a sestra, kamarátka. Jak pribúdajú roky, prichádzajú aj skúsenosti, ktoré by som nemenila. Som vďačná za všetko, čo mi život dal a budem vďačná aj za to, čo ešte prinesie. 

   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    40
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1645
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Pochod myšlienok
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




