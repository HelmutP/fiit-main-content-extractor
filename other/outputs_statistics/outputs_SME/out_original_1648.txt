
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Pšeno
                                        &gt;
                Nezaradené
                     
                 Odkaz na NOVÝ blog 

        
            
                                    2.9.2009
            o
            8:17
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            559-krát
                    
         
     
         
             

                 
                    Tento blog je už starší, písal som ho v časoch, kedy som chodil na strednú školu a potom som už doň písať prestal. Je tomu od tohto dátumu už vyše dvoch rokov. Čítaj ďalej....
                 

                 Preto veľa vecí, čo tu nájdete, už tiež nemusia byť úplne aktuálne: napríklad už moc nechodím na Lumídkovu stránku, ktorá sa i napriek tomu nachádza v mojich obľúbených blogoch. A naopak sú teraz prirodzene iné blogy a stránky, kde rád chodievam :-). Tiež aj moje motto či krátky opis "o mne" by som dnes už asi takto nenapísal - proste sa vyvíjame a meníme.   Napriek tomu, že už by som asi väčšinu článkov a podobne už dnes takto nenapísal a asi by som ich nenapísal ani vôbec, nemám potrebu to nejak všetko rušiť alebo odhovárať ľudí od prečítania si niečoho. Celý blog je časťou mojej minulosti, ktorá je taká istá reálna ako prítomnosť, akurát z hľadiska prítomnosti je prítomnosť prítomnejšia a aktuálnejšia ako minulosť, no tiež prirodzený rád vecí je tiež taký, že minulosť ovplyvňuje prítomnoť, takže i keď sa zaoberáme čistou prítomnoťou, sú v nej či chceme alebo nie stopy minulosti. A to platí bez výnimky i na mojom prítomnom živote, ktorý je mojou minulosťou ovplyvnený (okrem iných vplyvov, ktoré nespomínam :-) ). Preto ak ma chce človek poznať, takého, aký som teraz, tak tento blog nie je úplne zbytočný. Ak ma chce človek poznať, takého, aký som bol pred dvoma rokmi, tak tento blog je viac než užitočný (toto platí asi najmä pre mňa) a ak ma človek nechce spoznať vôbec, tak tento blog nemôže byť snáď menej užitočný, ako blog, ktorý by som písal dnes...   Od doby, kedy som písal tento blog, som písal ďalšie dva blogy. Prvý som začal písať prakticky hneď, ako som skončil s týmto a druhý až nedávno. Dám sem link na ten prvý blog, jediné dva články z druhého možno prekopírujem na ten prvý a možno v dohľadnej dobe začnem písať i úplne nové články. Inak o tom blogu, na ktorý dávam link teraz, platí úplne to samé, čo som písal v tomto článku ohľadom tohto blogu, keďže tie články sú asi rovnako staré.   Neviem presne, prečo sa teraz o túto záležitosť s blogmi takto zaujímam, keďže to všetko môže byť možno vcelku jedno. No asi by bolo naozaj dobré spraviť aký taký poriadok v svojich blogoch, ktoré nosia moje meno a na ktoré možno občas i niekto narazí, či už náhodne alebo nie...   Tak dávam sem spomínaný link: http://moonseagull.blogspot.com/    Prajem všetkým, tento článok čítajúcim, pekný a šťastný deň! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Pšeno 
                                        
                                            Najkrajší a najsmutnejší deň, zároveň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Pšeno 
                                        
                                            Moje posledné...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Pšeno 
                                        
                                            "Do smrti nezastavím..."
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Pšeno 
                                        
                                            Ráno
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Pšeno 
                                        
                                            Motto: Zničme si celý svet aby sme nemuseli viacej žiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Pšeno
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Pšeno
            
         
        pseno.blog.sme.sk (rss)
         
                                     
     
        Niekedy sa cítim ako logik - so všetkým čo to prináša...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    986
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            O svete z "inej" strany
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Kliknite na ĎALŠIE&gt;
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            Nájdete tam odkazy na zopár (legálnych) .mp3 na stiahnutie.
                                     
                                                                             
                                            Ktoré sú dokonca to najlepšie čo poznám...
                                     
                                                                             
                                             
                                     
                                                                             
                                            • Sigur Ros (MUST z islandu):
                                     
                                                                             
                                             
                                     
                                                                             
                                            untitled8.mp3
                                     
                                                                             
                                            untitled4.mp3
                                     
                                                                             
                                            vidrar_vel.mp3
                                     
                                                                             
                                            rokklagid.mp3
                                     
                                                                             
                                            orralagid_sample.mp3
                                     
                                                                             
                                            von.mp3
                                     
                                                                             
                                            fonklagid.mp3
                                     
                                                                             
                                             
                                     
                                                                             
                                            • Dlhe diely (alias Longital):
                                     
                                                                             
                                             
                                     
                                                                             
                                            vlasy.mp3
                                     
                                                                             
                                            ako_dunaj.mp3
                                     
                                                                             
                                             
                                     
                                                                             
                                            • Eels (velmi silne):
                                     
                                                                             
                                             
                                     
                                                                             
                                            somebody.mp3
                                     
                                                                             
                                            stop_pretending.mp3
                                     
                                                                             
                                             
                                     
                                                                             
                                            • Waking Wision Trio ("oceanic" jazz):
                                     
                                                                             
                                             
                                     
                                                                             
                                            through_the_canyon.mp3
                                     
                                                                             
                                            in_the_full_moon_akasha.mp3
                                     
                                                                             
                                            prayer_for_the_ocean.mp3
                                     
                                                                             
                                            moonseagull.mp3
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Neuveritelná
                                     
                                                                             
                                            Ač (GW)
                                     
                                                                             
                                            Lumídek (ST)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            dve stránky kde je VŠETKO:
                                     
                                                                             
                                            Google
                                     
                                                                             
                                            Wikipedia
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




