
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Strapko
                                        &gt;
                Domáca politika
                     
                 Pamätník Trianonu - pamätník čomu? pre koho? načo? 

        
            
                                    3.6.2010
            o
            14:43
                        (upravené
                1.3.2011
                o
                8:39)
                        |
            Karma článku:
                9.58
            |
            Prečítané 
            1241-krát
                    
         
     
         
             

                 
                    Obdobie pred hocijakými  voľbami na Slovensku má jedno unikátne špecifikum. Je ním skutočnosť, že vždy, spolu s približovaním sa k ich realizácii, rastie aj nebezpečenstvo zo strany nášho južného suseda (?!). Maďarská karta bola kedysi doménou SNS, ale v poslednom čase jej ju kradne Smer. SNS nemá na výber a musí konať...
                 

                 SNS sa chytá topiacej slamky. Smer jej prebral jedinú tému, na ktorú vábi voličov. Do volieb zostáva málo času a strata voličských hlasov v prospech Smeru je viac ako reálna.  Smer promptne zareagoval na prijatie zákona o dvojitom občianstve. SNS už mohla len pritakávať. Úvodné a hlavné protimaďarské slovo na protimaďarskej tlačovej besede mal tentokrát nie Slota ale Fico.   Našťastie pre SNS, tohto roku je výročie podpisu Trianonskej zmluvy. Zmluvy, ktorá už de facto len právne ustanovila to, na čom sa víťazné mocnosti dohodli už po skončení prvej svetovej vojny.   Niektorí maďarskí občania a politici verdikt Trianonu dodnes nestrávili. Nevedia sa zmieriť s tým, že ako prehratá mocnosť si nemohli vyberať. Nevedia stráviť skutočnosť, že dejiny vždy tvoria víťazi...a tak teraz, kedy sa Maďarsko zmieta v ekonomických a sociálnych problémoch je táto téma populárna. Veď čo je lepšie ako pripomínať ľuďom minulosť - namiesto riešenia problémov súčasnosti a budúcnosti...   A SNS neváha. 4. júna sa chce po dlhšej odmlke znova architektonicky činiť - postaviť pamätník Trianonu v Komárne.   Prečo to vlastne robí?   Slota oficiálne tvrdí, že je to snahou o ukázanie Maďarom kde sú hranice atď. Každý však vie prečo to SNS robí - nemá už na výber, buď sa prezentuje teraz alebo už inde voličov nezíska, čas k voľbám sa kráti.    K čomu to bude dobré? Ako tým napomôže vývoju Slovenska?   Toto je podľa mňa oveľa dôležitejšia otázka. Mnoho ľudí na Slovensku ani netuší, čo to tá Trianonská zmluva je.  Všade vo svete sa stavajú pamätníky osobnostiam a udalostiam, ktoré majú ľudia stále pred sebou, ktoré pre nich niečo naozaj znamenajú.   SNS na Slovensku stavia pamätník udalosti, ktorú každý bežný občan považuje za úplne samozrejmú (vojna skončila, rozpadla sa monarchia, nakreslili sa hranice).   SNS na Slovensku stavia pamätník udalosti, ktorá je v Maďarsku hlavnou témou nacionalistov, populistov a podobných vymývačov ľudských mozgov.       Netvrdím, že udalosť táto udalosť z roku 1920 nebola dôležitá pre vývoj budúcej ČSR a neskôr SR.   Ale pýtam sa vás pán Slota a spol.: Na to ste prišli až teraz, keď sa zrazu  chystáte stavať pamätníkov po celom južnom Slovensku, a to 10 dní pred voľbami????   Nesmrdí toto práve tým farizejstvom, ktorým tak radi častujete KDH????       Namiesto toho, aby sa Slováci nad výrokmi členov maďarského parlamentu hrdo povzniesli, im na úder nadhadzujú ďalšie a ďalšie loptičky.   Ale to vám pán Slota a spol. vyhovuje. A momentálne to vyhovuje aj Ficovi a jeho s.r.o., ktorý vás v tom všetkom vymývaní podporuje.  Vám skutočne nejde o národnú hrdosť všetkých obyvateľov Slovákov! Pretože keby ju majú, tak ľudia ako vy nesedia vo vláde...   Po výstavbe dvojkrížov je tu zo strany SNS ďalší ponižujúci nezmyseľ a desím sa toho, čo môže prísť, ak by vo vláde boli aj po 12. júni.... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Fico odchádza - podobne ako Mečiar aj Putin
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Komunálne voľby odhalili množstvo chýb - Môžu vás stáť budúci post!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Z vlastnej vôle, či bez - hlavne, že ideš preč!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Should I stay or should I go?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Strapko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Strapko
            
         
        strapko.blog.sme.sk (rss)
         
                                     
     
         Mám rád ľudí, aj keď často nedokážem pochopiť ich konanie. A napriek tomu, že som často moralista a skeptik, nikdy neprestávam hľadať pozitívne aspekty negatívnych javov. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    267
                
                
                    Celková karma
                    
                                                8.08
                    
                
                
                    Priemerná čítanosť
                    1523
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Domáca politika
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Z môjho života
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Škola
                        
                     
                                     
                        
                            Osobnosti, zaujímavosti
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Úradnícka vláda ako plán Ficovho návratu
                     
                                                         
                       Stránka katedry žurnalistiky radila, ako obísť Piano
                     
                                                         
                       Fico de facto pripustil Paškovu zodpovednosť, ale...
                     
                                                         
                       Anarchista odstúpil
                     
                                                         
                       Slovenský gympel, americká "high school"
                     
                                                         
                       Yankees go home! (desať minút s národniarom)
                     
                                                         
                       Medzi väzňami v Nairobi, alebo keď si myslíte, že vás už v živote máločo prekvapí.
                     
                                                         
                       Buď chlap
                     
                                                         
                       'Satanistický' festival? Meriame dvojakým metrom. A je to ešte horšie.
                     
                                                         
                       Slušne platený bočák
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




