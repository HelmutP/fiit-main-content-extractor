
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Slovákov sprievodca po kade&amp;ta
                     
                 Slovákov sprievodca po Rusku a iných ruských krajinách 

        
            
                                    18.6.2010
            o
            10:33
                        (upravené
                18.6.2010
                o
                11:04)
                        |
            Karma článku:
                16.49
            |
            Prečítané 
            3298-krát
                    
         
     
         
             

                 
                    Problém je ten, že na východ od nás bývajú Rusi, lenže keď im to poviete, tak sa naštvú. Nikto nechce, aby o ňom vraveli, že je Rus. Okrem Rusov. Tí to o sebe hovoria úplne normálne a bez hanby, lebo nevedia, že v slovenčine je to skoro taká zlá nadávka ako Maďar, zlepenec alebo Vatikánska zmluva. Lenže zase Rusi si môžu hovoriť, čo chcú a čo povedia, to aj platí, lebo sú strašne obrovskí, strašne pijú a dali svetu päťročnice.
                 

                 Bielorusko   Najmenším Ruskom je Bielorusko a je to zároveň jediná krajina, kde, keď poviete, že sú Rusi, tak vám prikývnu.  Celkovo sú tam ľudia zvyknutí prikyvovať, lenže na druhej strane sa tam majú dobre, tak čo by neprikyvovali. Bielorusko je strašná nuda, lebo tam majú len lesy, lesy a ďalšie lesy plus ženy, ktoré sú všetky blonďavé. Národným jedlom sú zemiaky, čiže sú od nás horší, lebo našim národným jedlom sú vlastne tiež zemiaky, ale mali sme aspoň dosť rozumu na to, aby sme z nich spravili halušky. Čiže sme pravdepodobne na vyššom vývojovom stupni, ale je nám to nanič, lebo oni majú zase lacnejší plyn. Bielorusko nám nemá čo ponúknuť a aj tak to tam ide dole kopcom, lebo zlí študenti, ktorí opustili rodný kraj a šli študovať do Poľska do krajiny pašujú marijuanu. V telke vraveli a čo vravia v telke, to je aj pravda. Aspoň v Bielorusku si to veľa ľudí myslí. A to je dobre, lebo keď celý národ čumí na telku a nemá blbé otázky, vtedy je pokoj. Z čoho vyplýva, že v Bielorusku je vlastne strašne dobre.       Pobaltie   Táto krajina je špecifické tým, že tam majú radšej Nemcov ako Rusov, čo je divné, lebo podľa nás sú oni sami tiež Rusi. Inak im trochu závidíme, že sa to neboja povedať nahlas, lebo my sme tiež napríklad kedysi mali radšej Nemcov ako Rusov, ale nahlas o tom nehovoríme. Blbo to znie a my radšej o veciach, ktoré blbo znejú, mlčíme. Myslíme si, že potom zmiznú, ale zatiaľ sa to nikdy nestalo. Pobaltie je nuda, lebo sú tam tri VÚC-čka a ani jeden kopec.  VÚC-čka sa volajú Litva, Lotyšsko a Estónsko a ich župany sedia v Rige, Vilniuse a Talline, ale priradiť si ich k sebe môžete ako chcete, aj tak je to každému jedno.   V Litve hrá každý basketbal a sú v ňom dobrí, ale nie dosť dobrí na to, aby porazili Američanov a to je dobre. Nie je totižto dobre, keď je niekto príliš dobrý. Litva prehrala futbal s Faerskými ostrovmi, čo je o trochu väčšia hanba ako remíza s Novým Zélandom a to nás teší. V Lotyšsku žijú ľudia, len o nich nikto ešte nepočul. Dve Lotyšky som síce vo Varšave stretol, lenže na mňa hovorili po rusky, tak som z toho mešuge. Aj Lotyši sú mešuge, aby som v tom nebol sám a nevedia hrať hokej. Estónci tiež nevedia hrať hokej, oni vlastne nevedia vôbec nič okrem behania na lyžiach, lenže to nás nezaujíma ak práve nie je olympiáda. Okrem toho sú Estónci severní Maďari a južní Fíni, čiže v podstate ako národ ani neexistujú. Pobaltie ako štát je rozumné dlhodobo a zodpovedne ignorovať, lebo je úplne nepodstatné.       Rusko   Rusko je také obrovské, že keď si to Rusi uvedomia, zakaždým sa ožerú a oni si to uvedomujú stále. Z Ruska pochádzajú kosáky, kladivá, gruziňák a Dedo Mráz, ale ten už asi zomrel. Stalin z Ruska nepochádza, ale aj tak tam vládol, čo len dokazuje, že keď máte v ruke kosák a kladivo, zľakne sa vás úplne každý. Možno skôr toho kladiva ako vás, ale keď ste to skúsili vysvetliť Stalinovi, viac ste už nikdy nič vysvetľovať nemuseli. Rusi strašne radi spievajú, lenže sú potom smutní, tak pijú a potom spievajú ešte viac a sú ešte viac smutní. Ďalej Rusi dokážu písať knihy, ktorým nikto nerozumie, prehrať vojnu s Japonskom a prísť k nám na návštevu, aj keď ich nikto nevolal. Je to prinajmenšom neslušné, ale Rusi sú takí silní, že čo je slušné, určujú oni. Nemáme ich preto radi, ale sme v tom tak trochu nerozhodní, lebo nahlas im to povedať nemôžeme. Tak sa radšej tvárime, že ich radi máme a dúfame, že neprídu zase od radosti na návštevu. Oni sa totižto na tej návšteve furt ožerú a potom nevedia odísť. Asi jediná vec, ktorú máme naozaj radi, je ruská zmrzlina, ale tá z Ruska nepochádza. A ešte máme radi aj ruský plyn, oni to vedia, tak nám ho občas vypnú, aby sme sa príliš netešili. Rusi nie sú fasa, ale nemôžem to tu napísať, takže nieže im poviete, že som to fakt aj napísal.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (77)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




