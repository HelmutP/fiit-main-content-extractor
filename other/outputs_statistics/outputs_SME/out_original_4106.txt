
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radka Mydlarčíková
                                        &gt;
                Nezaradené
                     
                 Princ na bielom koni alebo aj rytieri sú muži 

        
            
                                    7.4.2010
            o
            17:00
                        (upravené
                7.4.2010
                o
                17:06)
                        |
            Karma článku:
                5.56
            |
            Prečítané 
            1454-krát
                    
         
     
         
             

                 
                    Čo vlastne pojem „princ na bielom koni“ znamená? Na Wikipedii je o tomto fenoméne napísaná jedna výstižná veta, ktorú musím citovať: „Charakteristickou črtou PNBK je to, že ešte nikdy neprišiel. Nikto ho ani nikdy nevidel.“ Priznám sa, že som to potešenie nemala ani ja. Ale hádžem to na fakt, že mám len 23 a  tiež nepredpokladám, že oňho nasledujúce desaťročie zakopnem.
                 

                 Momentálne zakopávam o všetko možné len nie o princov. Neverím na podobné mýtické bytosti, ktoré vznikajú v hlavách romantických hrdiniek všedných dní. Týmto samozrejme neodsudzujem romanticky založené ženy,  to by mi ani nenapadlo vzhľadom k tomu, že každá žena má v sebe trochu romantiky.  Ale pojem „PRINC“ je v dnešnej dobe príliš preceňovaný. Zabúda sa totiž na to, že princ netvorí celé kráľovstvo.                     A tu sa dostávame k meritu veci. Všetky ochkáme a achkáme nad princom a pritom zabúdame na jeho verných kamošov  a.k.a rytierov. Rytieri sú tí, ktorí verne stoja po princovom boku a ktorí zaňho bojujú veľké víťazstvá a priznajme si to, vyšliapavajú mu cestičku k úspechu. Rytieri namiesto tesných pančušiek nosia výstroj hodný muža ( neodsudzujem pančušky, len elastické nohavice) a sú ochotní spraviť aj nemožné, aby niečo dosiahli.                   Prečo by sme sa my ženy mali obmedzovať len na vyššiu kastu, keď na svete behá toľko mužov v lesklej výstroji, ktorí by nám zniesli aj modré z neba? Prečo sa naháňame za niekým kto:    a)      neexistuje,    b)      nemá záujem,   c)       je vyberavý?    Prečo nás tak láka niečo, čo nemôžeme mať namiesto toho, čo mať môžeme? Možno sú to pudy, ktoré nás k tomu ženú, ale treba si určiť priority. Môj  najlepší kamarát mi povedal jednu vec, ktorá ma inšpirovala pri napísaní tohto článku. „You have to try many knights  to get a real princ.“  Jednoducho povedané, čím viac rytierov skúsiš, tým väčšia šanca, že sa dopracuješ k tomu pravému. A nemusí to byť práve princ, kto si vás osedlá. Možno to bude ten, ktorý možno nebude sedieť na bielom koni, ale bude sedieť vedľa vás v momente, keď ho budete potrebovať.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (221)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            what goes around comes around
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Odsudzovanie = delete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Štockholm mojimi očami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Grécko mojimi očami (časť druhá)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Grécko mojimi očami (časť prvá)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radka Mydlarčíková
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radka Mydlarčíková
            
         
        mydlarcikova.blog.sme.sk (rss)
         
                                     
     
        Som veľkou neznámou aj pre seba samotnú.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1954
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




