
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Sýkora
                                        &gt;
                cesty/necesty
                     
                 Opäť jeden zázrak z Dánska 

        
            
                                    18.6.2010
            o
            12:44
                        |
            Karma článku:
                9.00
            |
            Prečítané 
            1876-krát
                    
         
     
         
             

                 
                    Znovu ma zaviali severné vetry do DK - dokonalej krajiny na severe Európy, vo svete známej pod menom Dánsko. Už som sa tu pár krát rozplýval nad tým, ako je táto krajina stvorená pre život (viď súvisiace články). Teraz vám prinesiem ďalší dôkaz o tom, ako kvalitne môže štát fungovať a slúžiť svojim občanom. Som presvedčený, že aj my - občania, musíme žiadať od nášho štátu zmenu. Neviem, či som si správne zapamätal nedávno zverejnenú správu, ale práve v DK je oproti SK pri porovnateľnom celkovom počte obyvateľov 7x menej úradníkov. Matej našiel na webe čísla iné (361 017 u nás oproti 150 000 u nich), ale stačí deň dva pobytu v Dánsku a pocítite ľútosť, prečo aj Slovensko nemôže byť krajinou, kde veci fungujú normálne, prirodzene a jednoducho, tak aby náš každodenný život bol lepší.
                 

                 Ja viem, že mnohí z vás okamžite namietnete: ale oni musia platiť vysoké dane. Hoci som v slovenských reáliách zástanca pravice a znižovania daní, vôbec by mi nevadilo, keby som štátu platil 50 či 60% daní, ak by mi poskytoval taký servis, ako som zažil včera v mestečku Randers. Syn tu práve včera záverečnou skúškou končil dvojročné štúdium a tak si išiel vybaviť s tým spojené formality. T.z. zrušiť svoju kartu CPR (niečo ako náš občiansky preukaz) a svoje konto v banke. Keďže obhajobu práce mal až poobede, išiel si vybaviť uvedené veci ešte predtým. Napriek pozitívnym skúsenostiam spred dvoch rokov pri prihlasovaní som predsa len mal isté obavy, ako pochodíme práve okolo 12-tej v obidvoch inštitúciách. Som odchovaný na socialistickom nadradenom prístupe úradníkov u nás, ktorý sa žiaľ ani 20 rokov „po" nezmenil a tiež som si už za tých 46 rokov života zvykol na obrovskú byrokraciu, úradné hodiny pre verejnosť, obedné prestávky a neuveriteľnú neschopnosť a nepružnosť väčšiny úradníkov (česť svetlým výnimkám!!). A nakoniec aj tie naše dane a odvody dohromady dávajú pomerne vysoké sumy, za ktoré nám náš štát poskytuje mizerný servis!   Jedna vec je celkovo ľudský prístup a základné zlé smerovanie štátnej správy, ktorá u nás nie je nastavená tak, aby slúžila a uľahčovala život občanom. Druhá vec je, už roky plánovaná a drahé peniaze odčerpávajúca informatizácia verejnej správy ako takej. Proste zostáva z toho rok čo rok už takmer 20 rokov zmes prázdnych sľubov, fantázií, míňania peňazí a žiaľ asi aj vedomých okrádaní štátu a nás občanov. Ale ozaj si za to môžeme sami, keď skláňame hlavy a zostávame „verejnosťou" s jej úradnými hodinami a nie sme „slobodní sebavedomí občania", ktorí vedia na čo majú nárok a čo majú od štátu požadovať. To je veľké (ne)morálne dedičstvo tých rokov neslobody, kedy nám komunisti sľubovali svetlé zajtrajšky a kedy nastal taký rozvrat spoločnosti, že dodnes môžeme o niečom takom, ako som videl v Dánsku len snívať. Posúďte sami:      Vchádzame do budovy s označením „Borgerservice", niečo ako náš mestský úrad, či obvodný úrad, len zeleň a normálna jednoduchá architektúra je rozdiel na prvý pohľad. Žiadne honosné a nezmyselne členené budovy, ale doslova „služba občanom", ktorá integruje viac zložiek. O tom, že to je všetko ľahko a hladko dostupné aj bezbariérovo je v severskej krajine ozaj zbytočné hovoriť.      Decká sú síce na odchode, ale to je vchod do priestorov, kde si občania môžu vybaviť všetky potrebné doklady. Nepáčil by sa vám taký žlto modrý koník či iný kúsok umenia, ktorý by vás vítal na úrade spolu s kvetmi a čistotou aj u nás?      Na úvod by stačila jedna podstatná vec, takáto krabička s tlačítkami, kde si vyberiete, čo chcete vybaviť a po stlačení správneho gombíka vám mašinka vyplazí jazyk v podobe lístočka, kde máte svoje poradové číslo. A potom si môžete počkať v príjemnej debate za stolom s kamarátkou alebo si vyhľadať všetko potrebné v dotykovo dostupnom informačnom systéme. A detail, ktorý zatiaľ u nás nemusí byť, ale pekne dokresľuje severskú mentalitu a dánsky zmysel pre maličkosti. Vpravo na stene je umiestnený meter, kde si môžete zmerať svoju výšku. Že prečo? Len tak, pre radosť a zábavu. Veď snáď o tom by mal byť náš život, či nie?      Žiadna nervozita, pokojné čakanie. Najmenší majú pripravené nielen ministoličky a ministolčeky, ale aj plnohodnotné počítače. Tie sú rovnako dostupné aj pre dospelých spolu s dvomi laserovými tlačiarňami. Tú fotku síce nemôžem nájsť, ale verte mi, že na fotke ďalej vľavo pokračuje takýto vybavený kút. Nehovorím, že nenájdeme aj u nás prvé lastovičky v podobe PC, ktoré sú funkčné a dokonca sa dá na nich ísť na internet, prípadne si môžete na nich vyhľadať niečo potrebné pre vybavenie, ale... tá pointa je v inom.      Pointa číslo 1: Ten veľký display ukazuje čísla, ktoré držíte na vytlačenom papieriku v rukách zároveň s číslami, ku ktorému priečinku máte prísť. Podotýkam, že žiadne malé okienka, žiadne dvere a ďalšie dvere, ale normálne pulty, kde stojí človek voči človeku a nie chudák žiadateľ kontra pán úradník resp. verejnosť voči štátnej správe! Ach jo!       Pointa 2 na záver bez fotky, lebo reálne som začal ten zázrak fotiť až keď bolo po všetkom. Matej nevedel, ktorý čudlík stlačiť, tak sa išiel opýtať takto rovno oproti dverám. Pani č. 1 nevedela úplne presne čo a ako, ale milo sa usmiala, vytiahla formulár a spýtala sa kolegyne - pani číslo 2. Nakoľko tá obsluhovala staršieho pána, ktorý držal v rukách hrsť bankoviek a potreboval viac poradiť ako obslúžiť moderné mašinky, tak chvíľku nereagovala, ale potom plynulou angličtinou s Matejom dokomunikovala, čo bolo treba a rovno to s ním vybavila. Popri tom pravdepodobne vybavila aj jeden telefonát, nakoľko na uchu mala zavesené handsfree slúchatko a nevypadala, že trpí samomluvou. Nech to celé trvalo 5 minút aj s opísaným čakaním tak je veľa. Možno sedem, fakt neviem, ale dovtedy som len žasol ako to tam frčí a prebieha. A rýchlo vydrankal od Bašky jej foťák, aby som pre seba aj vás mohol priniesť toto svedectvo. O tom, že podobný princíp čísielok a rýchle vybavenie hladko prebehlo aj  v banke, snáď nepochybuje nikto z vás.   Viem, je to bolestná cesta, ale raz tak či tak, ju budeme musieť podstúpiť. Zoštíhliť a zefektívniť verejnú správu ako napríklad Nový Zéland, kde dokázali redukovať ministerstvá s tisíckami zamestnancov na efektívne mašinérie, kde pracuje pár desiatok ľudí, ak nie jednotlivcov bude pre nás nevyhnutné. A nespomínam to kvôli futbalovej remíze, ono to skutočne bolo tak. A ten link z Inessu stojí za prečítanie, aj dva razy, to znie až neuveriteľne!! Proti tomu ponúkam jeden z nášho prostredia, už len ten nadpis zo sme.sk hovorí za všetko: „Audit: Znížiť počet zamestnancov štátnej správy o 20% je nereálne" a to je článok z marca 2007.   Práve teraz politici vedú rozhovory o novej vláde, ktorá dáva ľuďom, ktorí zmýšľajú podobne ako ja a sú hodnotovo orientovaní podobným (ne)smerom ako ja, istú nádej na zmenu k lepšiemu spravovaniu krajiny ako doteraz. Myslím, že je vhodná príležitosť sa zamyslieť, ako a kde začať s tou zmenou. Prvá príležitosť sa naskytne už čoskoro, hneď ako pôjdeme najbližšie na úrad. Bude veľmi zle, ak naň vojdete ako hrdý občan a odídete z neho ako zbitý pes, ktorý toho pobehal veľmi veľa a „hovno" vybavil. Pardón za výraz!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Tri rohy jedenástka aneb Jarek a Róbert zažiarili v Prievidzi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Prečo by som nešiel do povstania ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Dobrovoľná brigáda v dnešnej dobe? Áno, podarila sa!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Nech sa dielo podarí.....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            42 do šťastia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Sýkora
            
         
        ivansykora.blog.sme.sk (rss)
         
                                     
     
        mierne unavený, ale vcelku optimistický štyridsiatnik, čo má rád prírodu, šport, čokoládu, modrú farbu a číslo 4 a riadi sa životným krédom "Rozhodujúci je človek"

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    128
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1573
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o behu a inom športovaní
                        
                     
                                     
                        
                            Berkat - pomoc
                        
                     
                                     
                        
                            fotoreportáž
                        
                     
                                     
                        
                            cesty/necesty
                        
                     
                                     
                        
                            výplody strapatej hlavy
                        
                     
                                     
                        
                            rozhovory
                        
                     
                                     
                        
                            Pozorovania a minipríbehy
                        
                     
                                     
                        
                            starinky
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            nohavica/plíhal to je klasika
                                     
                                                                             
                                            Pink Floyd kapela mladosti je stále skvelá
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nielen o behaní
                                     
                                                                             
                                            nielen o maratóne
                                     
                                                                             
                                            dobrá inšpirácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       7. ročník filmového festivalu Jeden svet v Prievidzi sa blíži
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




