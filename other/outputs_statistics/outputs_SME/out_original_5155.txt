
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                História
                     
                 Zelená je tráva... 

        
            
                                    24.4.2010
            o
            10:20
                        |
            Karma článku:
                5.48
            |
            Prečítané 
            1961-krát
                    
         
     
         
             

                 
                    Nie som žiadny fanatický fanúšik futbalu a priznám sa, ani neviem kto je momentálne na čele najvyššej slovenskej ligovej súťaže. Dobrý futbal si však rád z času na čas pozriem, ale aj tak ma viac zaujíma jeho história a zjednocujúca sila, ktorá mení ľudí bez rozdielov na všetkom možnom, na jednoliatu masu, ktorej sa hovorí fanúšikovia.
                 

                 Preto nadväzujem na môj článok o histórii slovenského futbalu ďalšími riadkami, o tomto nielen na Slovensku, populárnom športe. V minulom článku som opísal kreovanie prvých na území Slovenska pôsobiacich futbalových klubov. Zámerne ich ešte nenazývam ako slovenské, pretože vznikali v čase najsilnejšej maďarizácie. Preto boli aj názvy spolkov a klubov v maďarskom jazyku a aj rokovacou rečou klubov bola maďarčina. S istotou sa dá povedať, že aj funkcionári vtedajších klubov z radov šľachty a meštianskych podnikateľských vrstiev sa hlásili k maďarskej etnicite, ak by sme však nahliadli do dobových archívnych materiálov, napríklad súpisiek hráčov, hlavne športových klubov zo severného Slovenska, zistili by sme, že futbal hrali vo vysokom počte aj hráči s čisto slovenskými menami. Počet slovenských hráčov sa tesne pred prvou svetovou vojnou s určitosťou zvyšoval, práve v súvislosti s prudkým rozvojom futbalu a so vznikom nových športových klubov, ktorými členmi sa stále viac stávali aj drobní remeselníci a živnostníci ( a dostávali sa do funkcií ). Na poslovenčovanie klubov malo vplyv aj to, že aktívnymi športovcami sa čoraz viac stávali aj robotníci a roľníci z čisto slovenského prostredia.   Počas prvej svetovej vojny sa šport a s ním aj futbal odmlčal, ale o to dynamickejšie sa potom rozvíjal po jej skončení a vzniku samostatného Československa. Práve vznik ČSR mal veľký význam aj pre rozvoj športového hnutia, pričom nezmazateľný význam mali hlavne športové spolky z českých krajín. Ako ústredná organizácia pre športové hnutie vznikla už na konci roka 1918 Československá obec športová, ktorá mala koordinovať činnosť zväzov, a ako jeden z prvých vznikol aj Československý zväz futbalový. Situácia na Slovensku bola však omnoho zložitejšia, maďarské vládnuce kruhy sa odmietali vzdať svojej moci, čo sa prejavovalo aj v športovej organizácii. Pôvodne športové kluby s maďarským aktívom sa odmietali podriadiť sa novovznikajúcim československým športovým zväzom. Podobná situácia bola aj s futbalom, keď väčšina športových klubov praktizujúcich futbal na Slovensku bola stále v maďarských rukách. Aj preto prílevom českej inteligencie, úradníkov, četníkov a robotníkov po vzniku ČSR na Slovensko, začali vznikať aj československé športové kluby.   Futbalisti na Slovensku si chceli vytvoriť samostatný Futbalový zväz pre Slovensko, ktorý by združoval futbalové kluby na Slovensku bez nacionálnych rozdielov, pretože po vzniku ČSR existovali športové kluby na národnostnom princípe, teda československé, maďarské, nemecké a židovské. Iniciatíva vychádzala pravdepodobne z maďarských športových kruhov, takýto postup sa však priečil vládnucimi kruhmi presadzovanej idei čechoslovakizmu a aj na priamy zásah Ministerstva s plnou mocou pre správu Slovenska, neboli tomuto zväzu schválené stanovy. Ministerstvo s plnou mocou pre správu Slovenska pod vedením V. Šrobára sa už v októbri 1919 zameralo aj na revíziu športových klubov, s úmyslom odstraňovať tak aj posledné zbytky maďarizácie. Jeho aktivity boli zamerané na spolky, ktoré chceli stále pôsobiť v maďarizačnom duchu a aj napriek existencie nového názvoslovia miest a obcí, stále trvali na používaní maďarských pomenovaní. Maďarské kluby si vytvorili na Slovensku vlastný futbalový zväz, ten sa potom v roku 1922 spolu s Československým futbalovým zväzom, nemeckým a židovským združil do Československej asociácie futbalovej, ktorá bola v roku 1923 prijatá do FIFA.   Súťaže sa odvtedy hrali po zväzových linkách, osobitne v českých krajoch a na Slovensku. O majstra Slovenska hral víťaz slovenskej časti Čs. futbalového zväzu s víťazom súťaže Maďarského futbalového zväzu na Slovensku. Na Slovensku bol dlhodobo najsilnejší už 1. apríla 1919 vytvorený I. Československý športový klub Bratislava (neskôr známy ako ŠK Slovan). Keď konečne došlo už v spomínanom roku 1922 k ukončeniu zväzových sporov, spustila sa na Slovensku aj prvá regulárna súťaž s jasnými pravidlami. Celoslovenská súťaž sa rozbehla v troch župách (Západoslovenskej, Stredoslovenskej, Východoslovenskej) a prvým majstrom Slovenska sa stal práve I. ČsŠK Bratislava. Víťaz slovenskej časti súťaže mal potom právo štartovať na tzv. zväzových majstrovstvách za účasti ďalších piatich majstrov žúp z Čiech a Moravy. Futbalové kluby z územia Slovenska sa v silnej konkurencii česko-moravských a nemeckých klubov v ČSR nedokázali presadiť, podarilo sa to až I.ČsŠK Bratislava v roku 1927, keď vo finále porazil majstra nemeckého zväzu DSV Budweiss (České Budějovice). Majstrovský titul takto organizovanej súťaže vyhral I.ČsŠK ešte raz v roku 1930. Od roku 1935 vznikla celoštátna liga, na ktorej sa zo Slovenska podieľali iba dva kluby, a to tak často spomínaný  I. ČsŠK Bratislava a ŠK Rus Užhorod (bol zapojený do slovenskej časti súťaže). Ich pôsobenie v tejto najvyššej súťaži však bolo bez výraznejších úspechov a možnosť zlepšovania im prekazili pomníchovské udalosti a rozpad ČSR. Aj tak sa však dá skonštatovať, že slovenský futbal bol na vzostupe a rozvíjal sa veľmi dynamicky. Slovenskí futbalisti sa dostávali aj na súpisky československých reprezentačných mužstiev a tak sa podieľali aj na prvom väčšom úspechu československého futbalu. Týmto úspechom bol zisk strieborných medailí, na iba druhých majstrovstvách sveta vo futbale, ktoré sa konali vo fašistickom Taliansku v roku 1934.   Aj v období Slovenskej republiky (1939 -45) bol futbal najpopulárnejším športom. V prvých ročníkoch slovenskej ligy kraľoval ŠK Bratislava (bývalý I.ČsŠK Bratislava), v IV. a V. ročníku ligy získal majstrovský titul nový kluby OAP Bratislava (Oddiel armádnych pretekárov), ktorý tvorili hráči vojenskej prezenčnej službe, ktorí nesmeli hrať za civilné oddiely.      ŠK Bratislava; zdroj: Kamenec, I.:Slovenský štát v obrazoch (2008)   Bratislava tak mala v lige prevahu, aj keď v slovenskej lige mali zastúpenie kluby dnes už všetkých tradičných futbalových  miest ako napríklad Trnava, Žilina, Trenčín, Prešov. Keďže v období druhej svetovej vojny nám nepatrila Petržalka, kde sa nachádzali prvé futbalové ihriská v Bratislave, musel štát, uvedomujúci si zjednocujúcu a propagandistickú silu futbalu podporiť vznik nového futbalového stánku. Tak vznikol projekt štadióna na Tehelnom poli, ktorý bol dokončený už v októbri 1940. Štadión pojal 15 000 divákov, ktorí ho aj pravidelne zapĺňali.   Podľa denníka Slovák z 21.11. 1943 bolo na Slovensku v tomto čase 220 klubov, v ktorých bolo organizovaných 10 898 registrovaných futbalistov. Počas vojny samozrejme stagnovala reprezentačná rovina slovenského futbalu, kvôli vojne a nepriateľstvu štátov sa neorganizoval svetový šampionát a slovenská reprezentácia počas tohto obdobia odohrala len 16 medzištátnych zápasov aj to len s mužstvami Nemecka a jeho satelitov.      Slovenské národné mužstvo (1939); zdroj: Kamenec, I.: Slovenský štát v obrazoch (2008)   Vypuknutie SNP sa pochopiteľne dotklo aj futbalu, posledný zápas súťaže sa odohral 27. augusta 1944 a znovuzrodenie futbalu nastalo až po vojne. Celoštátna liga bola obnovená až v sezóne 1946/47 a začala sa tak nová história slovenského futbalu, okrášlená už aj výraznejšími úspechmi.           Zdroje: Perútka, J.: Dejiny telesnej výchovy a športu na Slovensku (1980)    Kamenec, I.: Slovenský štát v obrazoch (2008)          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4080
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




