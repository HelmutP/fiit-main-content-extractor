

 A povinnosti. Síce príjemné, ale s pokojom na lôžku, ktorý kázal lekár, nemajú nič spoločné. 
 Tak chodím po svete bez hlasu. Dlhá snaha o rozprávanie bolí, tak sa snažím nehovoriť. Ja, ktorá má zvyčajne ku všetkému nejaký komentár, spomienku, pripomienku, opačný názor. Bolesť je ale hnusná, tak nehovorím. Zvládam. A - počúvam. 
 Počúvam priateľov. Aj celkom cudzích ľudí. Ich hlasy, melódie. Počúvam slová, ktoré nechcú byť povedané, slová, ktoré nevedia vyjadriť city, aj také povedané len tak - aby sa nepovedalo. Mne slová ostávajú uväznené v hlave.  A nie je im tam zle. Pozorujem, vnímam. 
 A teším sa z ticha. Lebo hovoriť  je fajn. Ako inak sa dozviem veci? Ale hovoriť len preto, aby nebolo ticho, je trápenie. Hovoriť spolu a rozumieť si je trochu umenie. Oveľa krajšie je vedieť spolu mlčať. 
 Je toho viac, o čom by som chcela rozprávať. Ale som vďačná chorobe. Že mi pomáha byť vo svete vlastného ticha a dotýkať sa hlasných svetov iných. Je to poznanie. A nový rozmer, ktorý ma veľa učí. O druhých, ale veľa aj o mne samej. 
   

