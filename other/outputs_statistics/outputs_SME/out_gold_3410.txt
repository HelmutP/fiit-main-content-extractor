

 Prístup číslo 1 
 Geniálny amaterizmus. Prvé video je z produkcie víťazky nemeckej úrovne súťaže Eurovision Song Contest, čo je jeden z najtradičnejších formátov klasických médií. No napriek tomu v posledných dňoch si získava obľúbenosť medzi divákmi netradičných médií, teda youtube.com. Toto video je dnes 7. v poradí najsledovanejších hudobných videí dnes (s celkovým počtom videní okolo 3,5 mil.). 
 




 
 Toto video je zlé, pretože: 
 
 otrasná kamera, pripadá mi to celé akoby niekomu dali do rúk profesionálnu kameru a jimmy jib, pričom ten človek nevedel čo s tým 
 otrasná choreografia, baba z ulice postavená pred kameru, tá baba sa dá zhrnúť ako "zkaderuka zkadenoha" 
 neostré zábery 
 aranžmá piesne nič moc 
 hrozná angličtina 
 otrasné osvetlenie, v niektorých záberoch sa pomaly speváčka nedá identifikovať 
 nalíčenie akoby vyšla z domu, totálny amaterizmus 
 ten náhrdelník na krku, bože môj 
 nezmyselné tetovanie na vnútornej strane ramena ruky 
 
 Toto video je úžasne úspešné a dobré, pretože: 
 
 otrasná kamera, pripadá mi to celé akoby niekomu dali do rúk profesionálnu kameru a jimmy jib, pričom ten človek nevedel čo s tým 
 otrasná choreografia, baba z ulice postavená pred kameru, tá baba sa dá zhrnúť ako "zkaderuka zkadenoha" 
 neostré zábery 
 aranžmá piesne nič moc 
 hrozná angličtina 
 otrasné osvetlenie, v niektorých záberoch sa pomaly speváčka nedá identifikovať 
 nalíčenie akoby vyšla z domu, totálny amaterizmus 
 ten náhrdelník na krku, bože môj 
 nezmyselné tetovanie na vnútornej strane ramena ruky 
 
 Prečo som si odporoval? Jednoducho tieto všetky faktory pomáhajú k tomu, že tej babe sa dá uveriť, že len pred minútou ste sa s ňou stretli v meste, kde ona bola s partiou (svojich adolescentných kamarátov). A teraz si jednoducho odbehla a zaspievala pesničku. A bolo jej úplne jedno kto ste, čo robíte, ona sa jednoducho zabavila so svojím špecifickým šarmom. To robí túto pesničku atraktívnou. 
 Prístup číslo 2 
 Inovatívny profesionalizmus. Takto sa dá zhrnúť najnovšie video Lady Gaga. Bez ohľadu na nejaké moje sympatie alebo antipatie voči hudbe tohto štýlu, jedno sa musí uznať: Lady Gaga je neuveriteľný marketingový nástroj. Jej je úplne jedno čo je na titulke bulváru, hlavne že tam je ona. Tento prístup už viacero ľudí skúšalo, ale nikto to nedotiahol do takéhoto extrému. 
 




 
 Toto video sa určite nedá nazvať nudou. Pesnička len dopĺňa jednu kontroverznú show, plnú odhalovania sa, nemravných gest. Nie je to klasický hudobný klip, rozumne zvolila prístup použitý už dávnejšie Michaelom Jacksonom pri klipe Thriller. Toto je krátky film. 
 Ako spomína medialne.sk, jej nové video je plné product placementu. Áno, logicky. Doba sa mení a namiesto boja s pirátstvom sa treba zamerať na rozumný predaj reklamného priestoru. Predáva sa pieseň a spolu s ňou aj "nealkoholický nápoj Diet Coke, chlieb Wonder Bread, šalátový dressing Miracle Whip, telekomunikačný operátor Virgin Mobile, fotoaparát Polaroid, on-line zoznamka PlentyofFish.com či limitovaná edícia laptopu HP Envy." (citované medialne.sk) 

