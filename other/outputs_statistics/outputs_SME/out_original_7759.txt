
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                www.theglobalcoolingproject.co
                     
                 Povodne šarapatia častejšie ako politici 

        
            
                                    1.6.2010
            o
            13:55
                        |
            Karma článku:
                6.49
            |
            Prečítané 
            1589-krát
                    
         
     
         
             

                 
                    Na Slovensku šarapatia povodne častejšie ako politici. K téme povodne, suchá a klimatická zmena vydalo  MVO Ľudia a voda viacero publikácií. Posledná z nich "Voda bez hraníc - Vodou ku klimatickej stabilite regiónov" spojená s diskusiou o príčinách vzniku častejších a extrémnejších povodní a následných súch s údajnou klimatickou zmenou, bude prezentovaná vo  štvtok 3. júna o 19:00 v kníhkupectve Artforum  Kozia ulica v Bratislave a v piatok 4. júna o 19:00 v kníhkupectve Artforum Mlynská ulica v Košiciach. V mene kolektívu autorov si Vás dovoľujem pozvať Úvodným slovom k publikcii.
                 

                 Úvodné slovo k publikácii „Voda bez hraníc"       Téma tejto publikácie je veľmi závažná. Je o vode, ktorá je základom rozkvetu alebo zániku celých civilizácií súčasného sveta. Vieme, že ľudstvo sa v minulosti obávalo hrozieb rozličných katastrof, ktoré sa nevyplnili. Riziká, o ktorých táto publikácia prináša poznatky, sa bohužiaľ v histórii ľudstva vyplnili veľakrát. Z histórie poznáme civilizácie Mezopotámie, Perzie, Stredného Východu, Egypta, tiež indiánske civilizácie Severnej i Južnej Ameriky a mnohé ďalšie, ktoré sa úspešne rozvíjali v úrodnej krajine s bohatou vegetáciou a dostatkom vody. Viaceré z nich si privodili úpadok či dokonca zánik degradáciu svojich vodných zdrojov. Keď dnes archeológovia vykopávajú z piesku doklady o ich rozkvete, ani nás nenapadne, že upadli a zahynuli v procese, ktorý je u nás v plnom prúde. Táto publikácia je nie len o zdvihnutom prste ohrození dnešných civilizácií dezertifikáciou, o príčinách extrémnych dažďov, tornád, hurikánov, povodní, súch a sprostredkovane hladu a biedy, ale aj príspevkom s navrhovanými riešeniami, ktoré sa v skromných podmienkach na Slovensku rozvinuli, ako aj rôzne technické opatrenia, ktoré boli rozvinuté v zahraničí .   Voda je veľmi unikátna látka. Slnečné teplo vyparuje vodu z morí, riek, pôdy i rastlinstva do atmosféry. Molekuly vyparenej vody v atmosfére pohlcujú, odrážajú a rozptyľujú slnečné svetlo, teplo i UV žiarenie. Voda je úžasný termoregulátor, ktorý podľa potreby chladí alebo zohrieva. Vyrovnáva teplotné extrémy medzi dňom a nocou, medzi jednotlivými sezónami, medzi jednotlivými oblastiami a tým zároveň tlmí extrémy v počasí. Čím je viac vody v atmosfére, tým je efekt vyrovnávania teplôt silnejší a výkyvy v počasí menšie. Čím je menej vody v atmosfére, tým je skleníkový efekt slabší a výkyvy v počasí extrémnejšie.   Výpar každej molekuly vody spotrebúva teplo, o ktoré ochladzuje zemský povrch. Vodné pary vystúpia vyššie do atmosféry, kde kondenzujú vplyvom chladu do malých kvapiek či ľadových kryštálikov a vracajú sa späť v podobe dažďa. Opakovanie tohto procesu predstavuje účinný mechanizmus na elimináciu nadbytočnej tepelnej energie pri zemskom povrchu a jej prenos do vyšších vrstiev atmosféry. To sa podobá dômyslenému chladiarenskému zariadeniu.   Vodné pary sa od ostatných plynov v atmosfére líšia mobilitou vplyvom zmien tlaku a teploty, premenlivosťou v čase a priestore, ako i zmenou skupenstva. Vodné pary ovplyvňujú podnebie na Zemi. Napriek tomu patrí ich úloha v atmosfére k málo preskúmaným a málo diskutovaným  otázkam. Pre zdravú krajinu je charakteristický uzatvorený (krátky) obeh vody. Vďaka zmierňovaniu rozdielov teplôt medzi dňom a nocou či medzi lokalitami s iným teplotným režimom, voda cirkuluje v malých množstvách na krátke vzdialenosti. Väčšina vody, ktorá sa odparí, sa opäť zráža v danej oblasti. Časté a pravidelné miestne zrážky spätne udržujú vyššiu hladinu spodnej vody. Ak dôjde k rozsiahlemu narušeniu prirodzeného vegetačného krytu, výpar vody klesá.   Slnečná energia dopadá na vysúšené plochy a mení sa v teplo. Dochádza k výrazným výkyvom teploty v priebehu dňa. Zväčšuje sa rozdiely teplôt medzi dňom a nocou i medzi lokalitami s iným teplotným režimom. Zvýšenie rozdielu teploty medzi lokalitami spôsobuje zrýchlenie prúdenia vzduchu. Vodná para je teplým vzduchom unášaná ďaleko. Väčšina vyparenej vody sa z krajiny stráca. Ubúdajú malé a časté zrážky, pribúdajú mohutné a menej časté zrážky od mora. Cyklus sa otvára. Klíma se mení na kontinentálnu, stepnú. Voda cirkuluje prevažne v otvorenom dlhom kolobehu. Zníženie schopnosti krajiny zadržovať vodu prispieva ku vzniku ničivých povodní.   Presušenie krajiny môže spôsobiť človek jej odlesnením, poľnohospodárskou činnosťou či urbanizáciou. Kým sa na ploche s bujnou vegetáciou za horúceho slnečného dňa dominantná časť dodajúcej slnečnej energie premení na výpar a len malá časť na citeľné teplo. Na odvodnenej ploche je to naopak. Odstránenie vody z pôdy je mimoriadne nebezpečný akt. Začiatok konca viacerých dávnych civilizácií spomínaných v úvode sa začínal nadmerným klčovaním lesov - či už na rozšírenie poľnohospodárskej pôdy, stavbu obydlí, výrobu lodí, dreveného uhlia alebo na iné účely.   Ľudia postupne vysúšali krajinu, až kým neboli nútení ju zavlažovať. Pôda sa zasolila a prestala rodiť. Úbytok vody v malom vodnom cykle znamená prehlbovanie nedostatku vody pre ľudí, potraviny i prírodu, rast extremalizácie počasia, častejší výskyt živelných pohrôm, povodní, sucha, požiarov i zmenu klímy. Úbytok vody v malom vodnom cykle zároveň znamená prehlbovanie rozdielov teplôt a tlakov medzi atmosférou nad oceánmi a kontinentmi s náhlymi zmenami počasia na kontinentoch.   Ale najintenzívnejšie človek v súčasnosti vysušuje krajinu urbanizáciou:  asfaltovaním, dláždením, zastrešovaním a zemského povrchu. Prakticky všetka dažďová voda z miest vyspelého sveta je odvádzaná dažďovou kanalizáciou. Kanalizovanie dažďovej vody obmedzuje akumuláciu vôd v krajine, znižuje výpar do atmosféry a zvyšuje odtok dažďových vôd do potokov, riek a oceánu.   Skanalizovanie dažďovej vody z kontinentov do oceánov znamená úbytok vody malých vodných cykloch. Naše mestá sa menia na vyprahlé púšte, ktoré sa od svojho okolia líšia zvýšenou teplotou. Chýba v nich voda, ktorá by teplo zmierňovala. Človek svojou činnosťou teda mení smery obrovského množstva vody a energie. Presušené mestské prostredie zhoršuje kvalitu života pre ľudí, spôsobuje vysoké teploty v letnom období, nižšiu vlhkosť ovzdušia, vyšší výskyt alergénov v ovzduší, čo má negatívne dopady na zdravotný stav občanov žijúcich v mestskom prostredí.   Z povedaného vyplýva, že doterajší koncept manažmentu vodných zdrojov rozpracovaný a aplikovaný v 20. storočí je nevyhovujúci. Vytvára veľmi rizikové situácie pre povodne i nedostatok vodných zdrojov s následnými extrémnymi výskytmi prejavov počasí, ktoré  síce pripisujeme globálnemu otepľovaniu, ale realita je v našej mentálnej nedokonalosti hospodárenia s vodou v prostredí svojej komunity.   Súvislosť medzi mestom, regiónom a kontinentom je zjavná a perspektíva  veľmi smutná, ak sa zásadným spôsobom nezmení hospodárenie s vodou. Smutný je i súčasný stav vedy o globálnom otepľovaní, ktorý venuje minimálnu pozornosť vode. Znie to paradoxne, ale podľa súčasného stavu poznania prispieva Amazonský prales globálnemu otepľovaniu viac, ako Sahara. Máloktorý vedec sa to odváži povedať otvorene, ale medzi ich riadkami často čítame veľmi malý záujem o zavodňovanie a zazeleňovanie. Našu generáciu je ťažko dostať von z vybehaných koľají myslenia. Preto potrebujeme novú generáciu vodohospodárov, ktorí budú kapacitne vedomostne pripravení rozvíjať inovatívne holistické riešenia obnovy vody v zdevastovanej krajine od technológií, cez ekonomické, kultúrne, sociálne a environmentálne aspekty.   Naša generácia v tejto oblasti zlyhala naplno. V 20. storočí sme sa systémovo podieľali na likvidácii vody v krajine. Zbavovali sme sa sladkej vody a premieňali sme ju na slanú vodu v oceánoch. Paradoxne je, že globálna populácia rastie, sladkej vody na kontinentoch ubúda a pribúda slanej vody v oceánoch. Je najvyšší čas mobilizovať novú generáciu mladých ľudí, aby zobrali zodpovednosť za vodu do svojich rúk a vo svojej komunite na konkrétnych príkladoch otvárali oči svojím priateľom, rodine, komunite, že je možné obnovovať sladkú vodu, že je možne naspäť premieňať slanú vodu na sladkú jednoduchým spôsobom. Zadržať všetku dažďovú vodu v krajine.   Dobrá správa je, že nová generácia mladých ľudí  citlivejšie vníma súvislosti, ako naša technokratická generácia. Nová generácia môže byť iniciátorom nového globálneho programu obnovy vodného cyklu na kontinentoch. Obnova vodného cyklu nad kontinentmi premení žlté vysušené regióny sveta na zelené. Kolektív autorov tejto publikácie verí, že novej generácii vodohospodárov sa podarí napraviť to, čo my sme pokazili.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




