

 Princíp prerušovanej súlože je na prvý pohľad veľmi jednoduchý. Pri pohlavnom styku pár nepoužíva žiadnu ochranu, či už bariérovú, hormonálnu alebo chemickú, avšak muž tesne pred vyvrcholením vytiahne penis z pošvy a ejakuluje mimo rodidlá partnerky. 
 Bezstarostná? Metóda je postavená na tom, že muž dokáže včas rozpoznať prichádzajúce vyvrcholenie a spraví opak toho, čo mu velí pud. Teda nedoručí spermie na najvhodnejšie miesto, odkiaľ majú najväčšiu šancu oplodniť vajíčko, ale penis vytiahne a vyvrcholenie nastáva mimo tela partnerky. Táto forma ochrany na ženu pri súloži nekladie žiadne nároky. 
 Na prvý pohľad je táto metóda úplne prirodzená a úplne zadarmo. Avšak až do chvíle, keď zlyhá. Gynekológovia ju považujú za mimoriadne nespoľahlivú pre jej Pearlov index 15-35. Znamená to, že zo sto žien počas jedného roka otehotnie pri používaní tejto metódy 15 až 35. 
 Môžem jej veriť? Prečo však zlyháva? Mnohí muži sa dušujú, že sa naplno ovládajú a nič im „neušlo". Bohužiaľ, to nie je pravda. Už od prvých chvíľ, kedy je muž vzrušený, vylučuje jeho penis tzv. kvapôčky lásky. Tie majú za úlohu penis zvlhčiť a obsahujú tiež určité množstvo spermií. Práve tieto spermie sú zvyčajne zodpovedné aj za nechcene splodené deti. 
 Niektorým párom táto forma ochrany vyhovuje a bez rizika ju používajú po celé roky. Na jednej strane je to preto, že s prípadným tehotenstvom nemajú problém sa vyrovnať, iní ju aplikujú len v dňoch, ktoré sa považujú za neplodné. 
 Čo na to partner? Mnohí muži však túto metódu nepovažujú za šťastnú, pretože s jej častým používaním stúpa psychický tlak, či nezlyhajú, niektorí tvrdia, že im to príjemné chvíle pri sexe dokáže pokaziť. Vtedy je rozhodne lepšie voliť iné formy ochrany. 
 Nie každá žena tejto metóde dôveruje, a neistota môže mimoriadne stúpnuť s každým dňom meškajúcej menštruácie. Z neistoty vás potom môže vyviesť len negatívny tehotenský test, alebo návšteva gynekológa. 
 Pre koho sa hodí? Túto metódu rozhodne nemožno odporučiť mladým ženám, ktoré nemajú stáleho partnera a dieťa si zatiaľ „nemôžu dovoliť". Vhodnejšia pre dlhodobých partnerov či manželské páry, ktoré prípadné zlyhanie tejto metódy nezaskočí. 
   

