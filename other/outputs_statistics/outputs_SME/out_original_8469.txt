
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Bohdana Machajová
                                        &gt;
                Nezaradené
                     
                 Voľby, prach dejín a foto. 

        
            
                                    9.6.2010
            o
            22:55
                        (upravené
                9.6.2010
                o
                23:05)
                        |
            Karma článku:
                3.85
            |
            Prečítané 
            814-krát
                    
         
     
         
             

                 
                    Potrebovala som fotky, taká obyčajná vec. Dnes to nie je jednoduché. Akosi totiž zmizli kabínky z podchodov s foto automatmi. Občianske preukazy a pasy sa dnes pružnejšie vybavujú na polícii, kde je priamo zabezpečené fotenie a pasy sú už biometrické, všetko sa spracováva elektronicky. Tak som sa rozhodla navštíviť  fotoateliér na Mariánskej ulici a dobre som spravila.
                 

                 Možno to neviete, ale tento ateliér má bezmála sto rokov. Ešte v starej  zástavbe tu bol ateliér. Keď sa urobila nová zástavba - urobil sa nový. Asi to bola dobra adresa - v strede  Starého Mesta. Ateliér založil niekto, zdedil ho jeho syn, a ten o ateliér prišiel pri znárodnení... ateliér ostal  a potom nastúpili Komunálne služby mesta Bratislavy. Po  89 roku je ateliér opäť v súkromných rukách... . Kúpil ho spravodlivo pán Sopko,  zamestnanec komunálnych služieb. Možno aj preto priestor ostal ateliérom naďalej. Jeden malinký priestor, ktorý sa  za celé vari tridsaťročie nezmenil. Teda myslím dispozícia. Dve miestnosti - v jednej skromný pultík, unikátna zbierka zložená z asi päťdesiat starých foťákov, otvorená kabínka so sliepňajúcim svetielkom a diskrétnym závesom a druhá miestnosť vlastný fotografický ateliér.    Ja si tento priestor veľmi dobre pamätám. Fotky na prvý občiansky preukaz som si dala robiť na tomto mieste a to bolo žiaľ veľmi dávno. Poznáte to - chcete na fotke vyzerať čo najlepšie a tak idete do kabínky, kontrolný pohľad, hrebeň, s pribúdajúcimi rokmi prípadne rúž či pudrenka... . A potom ak sú pred vami iní klienti tak čakáte .         Hlavný hrdina tohto príbehu bude ale pultík a na ňom sklo. Pod sklom história. More, more fotiek, jedna tvár cez druhú. Ostala som v nemom úžase, ako by sa čas zastavil.   Hneď ako som vošla vstúpil so mnou mladý muž, vážny srandista - tiež túžil po fotkách na víza. Študent a hneď začal: tak pane na nebi, tak ja sem vojdem a hneď tu starší na mňa tento, no dúfam, že bude o chvíľu v prachu dejín (myslel  parlamentného opilca národniara Slotu). Aj ja som sa tam okamžite pozrela a fakt..., fuj. Ale začala som sa ponárať v príbehoch, v informáciách, v čase... . Naľavo sa nachádza známy kontroverzný podnikateľ Kočner, neďaleko bývalý šéf bratislavského podsvetia Čongrády. Hore vpravo sa pekne usmieva Kamila Magálová, tak spred dvadsiatich rokov. A vedľa seba pekne po poriadku generálni prokurátori Hanzel s Trnkom. Pomedzi to sa vystrieda plejáda neviest a ženíchov, neznáme fešandy, fešáci a aj deti. Zhora z kútika pozerá Harabín. Je tam aj náš prvý kozmonaut Bella. A famózny mužný hrdina televíznych pondelkov, či z Tisícročnej včely  Kvietik. Na doplnenie je tam aj  otec Jarošovej  filmovej Tisícročnej včely Jakubisko, a tiež jeho šikovná energická manželka Deana. Prefíkaný Flašík, smerácky Kaliňák tiež. Hneď vedľa kamaráti Mikloš s Kaníkom, kúsok ďalej Dzurinda. Teraz po novom akože aktuálne celebrity na pulze dňa Mokrý sám a sama Mórová. Napravo sa usmieva všeobímajúco Miško Dočolomanský a smutný Vlado Durdík obaja toho času už asi z neba. No a ako inak so sarkastickým výrazom tam nechýba ani Lasica, ale aj šibal Satinský... . Pýtala som sa, Ferko Mikloško sa sľúbil, ale ešte neprišiel i keď býva za rohom.       Tak keď na mňa prišiel rad, ani sa mi nechcelo od stolíka odlepiť, napadlo mi aké je to všetko zvláštne. Sú tu tváre minulosti, sú tu tváre prítomnosti. Tak som si potíšku povedala - no niektorí  by tu mohli naozaj ostať v prachu dejín  bezpečne pod sklom. Tajne dúfajúc verím, že tá naša cesta bude po sobote jasná, že to bude zmena. (slovo smer ani radšej nepoužijem pre istotu) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Kto z Dúhového pochodu chytil svadobnú kyticu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Tlaková níž
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Námestie SNP volá - pomôžte prosím!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Génius loci
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Ondrejský cintorín v ohrození
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Bohdana Machajová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Bohdana Machajová
            
         
        machajova.blog.sme.sk (rss)
         
                                     
     
        Komunálna politička Bratislava - Staré Mesto.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    9
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1204
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




