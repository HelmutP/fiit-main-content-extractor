
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Krajmerová
                                        &gt;
                Súkromné
                     
                 Prečo si sadne opitý človek za volant? 

        
            
                                    7.4.2010
            o
            20:20
                        (upravené
                7.4.2010
                o
                20:30)
                        |
            Karma článku:
                6.86
            |
            Prečítané 
            1489-krát
                    
         
     
         
             

                 
                    Autonehodu prežil, zapríčinil alebo sa jej iba zúčastnil nejeden z nás. Pred 15 rokmi som i ja zažila pri predbiehaní na mrznúcom snehu šmyk a kamión ma odhodil na okraj priekopy, kde som to akosi ubrzdila a bez väčšej ujmy na zdraví i prežila. Odvtedy ctím zásadu zimných pneumatík. Ale čo i len s kvapkou alkoholu som si za volant za 30 rokov nikdy nesadla. Prečo to spravil náš syn?
                 

                 
  
     Šoférovanie bolo jeho najväčšou vášňou.   Rodičia nechali doma kľúčiky od auta a odišli na návštevu.   Pretože bolo šoférovanie jeho vášňou, bol zároveň presvedčený, že zvládne túto činnosť aj po niekoľkých pivách.    Vôbec mu nevadilo, že bol po nočnej službe a príslužbe, a teda po 30 hodinách bez spánku.   Rozhodol sa prekvapiť priateľku v práci, veď to bolo „iba" 25 kilometrov!   Pravdaže, zabudol doma všetky doklady, aj od auta.   Keďže pri šoférovaní esemeskoval, ani si nevšimol, že „drgol" do odstaveného auta a pristal zozadu až na jeho volante - atak pokračoval vceste.   Nakoľko bolo naše auto mimoriadne zničené a neovládateľné, začalo na ceste „tancovať" a skončilo vobjatí verejného osvetlenia.   Náhodný chodec našťastie uskočil tejto neriadenej strele a privolal policajtov.   Policajti mu dali fúkať, namerali viac ako 2 promile alkoholu vkrvi, odobrali mu vodičský preukaz - a náš superman so slzami vočiach dookola opakoval: „Vy ste mi vzali moju životnú vášeň - šoférovanie! Ako budem bez nej žiť?"      Záver          Náš syn mal z pekla šťastie, že nikoho nezabil, lebo bola Kvetná nedeľa a v okolí zavretého Polusu sa motalo minimum ľudí. Šofér bol ozaj výborný - pokiaľ sa nenapil a miesto spánku po nočnej službe, niekoľkých pivách a následnej sprche si vsugeroval, že je už dostatočne svieži. Nebol. A prvý raz si pod vplyvom alkoholu sadol za volant.      Policajtov som nikdy extra nemusela, ale tomu veľkonočnému, ktorému som bola predložiť doklady  môjho zničeného auta, dávam za pravdu.       „Mladá pani, keby váš syn nebol dospelý, tak by si zaslúžil prehnúť cez koleno a zmlátiť remeňom ako žito. Na takýchto nič iné nezaberie. Za 15 rokov, čo slúžim, 80 percent nehôd spôsobili mladí chalani od 15 - 25 rokov. Áno, aj bez vodičáku. Sú to jednoducho machovia s premršteným sebavedomím. A kamoško alkohol im ho ešte pritvrdzuje, tak robia blbosti, ako včera váš synátor."         Súhlasím aj s tým, že za tieto úlety si bude musieť náš syn poriadne uťahovať opasok. Našťastie sa vážne nezranil, ale jeho ego je riadne doráňané. Kiež by sa už konečne začal správať zodpovedne ako dospelý človek! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Voniaš ako...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Týrané Slovenky, nedajte sa !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Spod plesovej krinolíny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Prírodný detox ľudského organizmu zvládne každý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Rok čínskeho draka má optimistický charakter
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Krajmerová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Krajmerová
            
         
        krajmerova.blog.sme.sk (rss)
         
                                     
     
        Autorka v júli 2012 zomrela. Blog nechávam na jej pamiatku. Dcéra Katka
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    235
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2101
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voniaš ako...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




