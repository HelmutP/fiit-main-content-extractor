
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kamenský
                                        &gt;
                filozofické zamyslenia
                     
                 Každý je zodpovedný za stav v štáte 

        
            
                                    2.3.2010
            o
            20:36
                        (upravené
                20.8.2012
                o
                15:14)
                        |
            Karma článku:
                3.01
            |
            Prečítané 
            585-krát
                    
         
     
         
             

                 
                    Radi si volíme ľudí aby riadili naše životy aby snáď ak sa nám niečo  zlého prihodí, bolo na koho váľať vinu. Každý je sám pre seba bezúhonný a  zlí sú len tí druhí. Dobrí sú všetci, lebo každý je sám pre seba dobrý a  podivuhodne aj zlí sú všetci pretože nie je hádam človeka na ktorého  iní neukázali ako na vinníka niečoho zlého. Každý z nás to už zažil. Pre  druhých sme vinníci, pre seba samých dobráci a sú aj výnimky kde nás  očisťuje pravé priateľstvo a láska.
                 

                 Často zodpovedný za svoj život nie som ja, ale tí druhí. Napríklad zlá vláda. Keby nebola zlá, všetko by bolo O.K., ale ona nie je a ja si myslím že ak sa nezmení sám človek a neurobí seba zodpovedného za svoj život, žiadna vláda nebude dobrá. Nebude, lebo my všetci sme zlí. Tam "hore" sedia ľudia ktorí sú jednými z nás a akí sme my, takí sú aj oni. Že to nie je tak? Že to len ja som dobrý a iní sú blbci?   Kedysi som pracoval v reklamnom štúdiu. V určitom období prišlo do módy aby obec mala svoj erb a vlajku, čo boli artefakty o ktoré sa obec sa začala zaujímať až keď sa blížila slávnosť k výročiu založenia. Veľa obciam v okolí sme vypracovali erb z dochovaných pečatí majiteľov obcí, grófov a pod. Podľa želaní sme vyrábal vlajky, orientačné tabule, brožúrky s popisom histórie a veľa iného.   Chodievali k nám starostovia obcí zadávať objednávky, konzultovať a vlastne v prvom rade dojednávať cenu. A práve k tým cenám by som mal niečo. Každý (!) starosta bezostyšne navrhol šéfovi rekl. štúdia či by sa nedala na faktúru vyššia cena a zvyšok by si rozdelili. Išlo o obecné peniaze, takže tak ...  Táto prax bola bežná aj inde, každý predsa vie, alebo má skúsenosť ako sa kradne zo spoločného čo je vlastne cudzie.   Vtedy  mi napadlo že ako to tí ľudia vedia rozprávať o zlodejstve vo vláde a pritom zlodejstvo prekvitá už na komunálnej úrovni! A to ide až dole ...   Zlodej kričí  - chyťte zlodeja. Keď kričí s davom, je pochopený. A hlavne ak kričí na verejné inštitúcie, či napr. obchody.   Blogy sú plné sťažností na obchodné reťazce, Pokazené mäso, neochota, atď ... Človek zákazník je iný človek ako človek zamestnaný niekde v obchoďáku. Áno, každý má právo kričať po spravodlivosti ak ho oklamú a ak mu napr. predajú pokazené mäso. No je zákazník morálne iný ako zamestnanec?   Krádeže v obchodoch, vyjedený tovar, vypité fľaše, dokonca aj vymočenie sa medzi regálmi je známe. No na moje prekvapenie sa medzi ľuďmi toleruje aj iná "zábavka." Zneužíva sa zákon že do troch dní môžte vrátiť tovar bez udania dôvodu. Ľudia potrebujú niečo odrezať, navŕtať a pod., a tak si z marketu urobili požičovňu pracovných nástrojov. Neurobili by to ak by neboli klamári, podvodníci a  zlodeji.   Je veľa príkladov ako národ ktorý žiada morálku po svojich mocných, po svojich blížnych  a je sám nemorálny. No na to aby som to všetko zdokumentoval, nemám dosť času a neviem ani ako je to s kapacitou blogu. Však kto by to aj čítal ...   Ja viem že každý máme svoje ilúzie, svoje "pravdy" o svete. Myslíme si že ak sa to a ono a hento urobí tak a tak, že už bude všetko v pohode. Nebude a nie je. Čo tak každý začať sám od seba? Ak ty a a ty a aj ty začnete pracovať na sebe, to percento čo nepracujú na sebe nemá šancu ...  Lebo len morálne vedomie každého jednotlivca môže pozdvihnúť celý národ!   Slovensko, hore sa!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Oneskorene k 17. Novembru ....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Milujeme tých, ktorých ľutujeme?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Liberalizmus trhu, ale otroctvo výroby?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Naozaj ide o komunizmus?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            O liberalizme a pôvode slova idiot
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kamenský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kamenský
            
         
        jozefkamensky.blog.sme.sk (rss)
         
                                     
     
         Zaváži len to, čo neumiera a pre nás neumiera to, čo umiera s nami. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    285
                
                
                    Celková karma
                    
                                                3.85
                    
                
                
                    Priemerná čítanosť
                    1019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            filozofické zamyslenia
                        
                     
                                     
                        
                            poézia
                        
                     
                                     
                        
                            duchovno
                        
                     
                                     
                        
                            umenie
                        
                     
                                     
                        
                            portréty blogerov
                        
                     
                                     
                        
                            Duch pôsobiaci v čase
                        
                     
                                     
                        
                            alchýmia varenia
                        
                     
                                     
                        
                            angelológia
                        
                     
                                     
                        
                            história
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Otvorený list prezidentovi SR A. Kiskovi.
                     
                                                         
                       Demografia a druhý pilier
                     
                                                         
                       Andy Warhol za 105 miliónov dolárov
                     
                                                         
                       Maňka-Kotleba...0:1
                     
                                                         
                       Mužskosť a ženskosť ako sociálne implantáty?
                     
                                                         
                       Volím život. Aj pre počaté deti
                     
                                                         
                       Lotyšské obrázky
                     
                                                         
                       Štyri generácie
                     
                                                         
                       Slovensko treba spájať
                     
                                                         
                       Uličská dolina
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




