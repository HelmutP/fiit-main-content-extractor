

 Ako dorastenec začal hrať v miestnom klube “Dolumestie” a v roku 1941 prestupuje do mestského klubu Turčiansky sv. Martin. Talentovaného krídelníka si všimol vtedajší tréner bratislavského Slovana Ferdinand Daučík, ku ktorému sa Július v roku 1945 sťahuje. Tu aj získava prezývku Kloky - vraj kvôli podobnosti jeho behu s cvalom klokana. Vysoký, chudý, rýchly, húževnatý a inteligentný hráč dokázal nielen skvele strieľať ale aj hlavičkovať a pripravovať šance pre spoluhráčom. 
 Počas prvej sezóny strieľa v Slovane v 18 zápasoch 23 gólov. Táto štatistika neunikla ani zahraničným pozorovateľom a po jedinej sezóne sa spolu s českým futbalistom Čestmírom Vycpálkom zo Slávie Praha sťahuje za 300 000 lír do Juventusu Turín. Korostelev síce strieľa v 30 zápasoch 15 gólov no Juventus na konci sezóny končí v tabuľke druhý za miestnym rivalom AC.  
 Druhú sezónu v Taliansku je šokujúco predaný do Atalanty Bergamo. V Bergame sa mu nepodarilo nadviazať na predošlú sezónu a počas nasledujúcich dvoch sezón strieľa 36 zápasoch iba 9 gólov. V roku 1949 sa prestupuje do klubu zo tretej najvyššej súťaže Reggina Calcio, ktorej pomohol postupu do Serie B. V roku 1951 sa znova sťahuje, tentokrát do klubu AC Parma, kde pôsobí nasledujúcich 5 rokov a strieľa 49 gólov. Tu dokonca figuruje až do 90. rokoch v miestnych historických tabuľkách ako najúspešnejší klubový strelec. Hráčsku kariéru ukončil ako 33 ročný v roku 1956 v klube Mantova. 
 Už v roku 1958 sa Korostelev vracia do veľkého futbalu ako tréner klubu AC Piacenza, ktorú trénuje dva roky. Na Juventus však nezanevrel a napriek nešťastnému prestupu sa v roku 1960 stáva asistentom trénera Gunnara Glenna na lavičke tohto giganta. Trénerské duo nedosahuje požadovaný  úspech a v priebehu sezóny su vystriedaní. Od vedenia doživotne dostáva nárok na dve vstupenky na každý zápas Juventusu, neskôr mu pravidelne volával prezident klubu Roberto Bettega.  
 V roku 2003 sa Július Korostelev po rokoch v cudzine vrátil domov na Slovensko. V posledných rokoch života vážne ochorel na Burgerovu chorobu, kvôli ktorej mu museli amputovať nohu.  
 Aj po 60 rokoch hovoriaci vzorovou spisovnou slovenčinou zomrel ako 83 ročný roku 18. októbra 2006 v Turíne. 
   
 Zdroje: 
 http://ilpalloneracconta.blogspot.com/2008/07/jlius-korostelev.html 
 http://en.wikipedia.org/wiki/J%C3%BAlius_Korostelev 
 http://denniksport.sk/article/60324/ 
 http://ru.wikipedia.org/ 
   
   

