
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Juriš
                                        &gt;
                Nezaradené
                     
                 Vinobranie v Pezinku (2007) 

        
            
                                    21.9.2007
            o
            21:33
                        |
            Karma článku:
                5.23
            |
            Prečítané 
            2446-krát
                    
         
     
         
             

                 
                      Takže dnes sa nám začalo v Pezinku Vinobranie - oslava vína ako kráľovského nápoja a vzdanie holdu ľudom, ktorí sa podieľajú na jeho výrobe. Ulice v centre mesta zaplavili kolotoče a stánky. (kolotoče idú okolo 50 Sk za jazdu) V stánkoch možno kúpiť občerstvenie, hračky, knihy a okolo hlavného námestia sú remeselnícke stánky, kde je v ponuke keramika, prútené výrobky, kožené výrobky, fúkané sklo, fujary, drevené výrobky, skrátka všetky ľudovoumelecké výrobky. Z občerstvenia je najrozšírenejšia cigánska (priemerne 65,- Sk), kebab (60-80 sk), ale v ponuke sú aj pečené ryby, klobásky a samozrejme lokše. Bohatý výber pijatiky :-) Vystúpenia hudobných a folklórnych skupín možno sledovať na dvoch tribúnach, na námestí a pred Zámkom v parku.
                 

                    Každý rok sa mesto snaží obohatiť program vinobranie o niečo nové a to formou sprievodných podujatí. V tomto roku je to:     - Pezinský permoník - medzinárodná výstava minerálov, fosílií a drahých kameňov,     - Výstava vinohradníckej, záhradnej a komunálnej techniky, spojená s predvádzaním      (na priestranstve na rohu Trnavskej a Šenkvickej cesty (oproti replike sochy slobody)     - Pekársku šou pôjdem pozrieť a ochutnať do stanu, ktorý patrí Cechu pekárov, ktorý je na námestí vo veľkoplošnom stane v rámci pekárskej šou a  ponúka chlieb a pekárske výrobky ako vhodnú potravinu k vínu.     a už tradične - Fyzulnačka - medzinárodná súťaž vo varení fazuľovej polievky. Ta Fyzulnačka je super akcia, bo po navarení polievok v kotlinách môžu ľudia ochutnávať rôzne chute fazuľovej polievky. Tejto fyzulnačky sa osobne zúčastním (sobota od 9.00 do 16.00), pretože som bol už minule na tom a veľmi sa mi to páčilo aj chutilo. Podotýkam, že najlepšie je sa tam motať okolo 12.00 a 13.00 hod, bo neskôr  pri veľkom počte ľudí môžete obísť naprázdno. Viac o fyzulnačke na tejto linke: http://www.pezinok.sk/index.php?yggid=541     Podrobný program akcií vinobrania si môžete pozrieť na webe mesta Pezinok: http://www.pezinok.sk/index.php?yggid=539 a ak by ste sa rozhodli prísť pozrieť do Pezinka, tak za zmienku stojí aj to, že u nás výnimočne na železničnej stanici zastavia vybrané  rýchliky. Viac o dopravnom spojení na tejto linke: http://www.pezinok.sk/index.php?yggid=540     A ešte aby ste si vedeli predstaviť ako to tu vyzerá, tak pripájam pár fotiek z dnešného otváracieho dňa. Zajtra už budú ulice plnšie ako dnes. Takže zajtra sa vidíme...                                                    Par fotiek stačí, zbytok treba vidieť na vlastní bulvy :-)        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Friday's american &amp; oldtimer cars (Danubia, August 2013)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Zápas II. futbalovej ligy žien o prvé miesto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Bolek Polívka o slovensko-maďarských vzťahoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Kvitnúca jabloň s jablkami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Veterný mlyn v Holíči
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Juriš
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Tatiana Lajšová 
                                        
                                            Recenzia: Fantázia 2014
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Juriš
            
         
        marianjuris.blog.sme.sk (rss)
         
                                     
     
        Muškát žltý
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2938
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Urda - filatelia
                                     
                                                                             
                                            Anton Kaiser
                                     
                                                                             
                                            Ivka Vráblanská
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O hlasovacom stroji Smeru, piatej slobode a Ficových akademikoch
                     
                                                         
                       Friday's american &amp; oldtimer cars (Danubia, August 2013)
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




