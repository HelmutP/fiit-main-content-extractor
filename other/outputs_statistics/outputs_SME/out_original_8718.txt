
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Súkromné
                     
                 Skrat po volebnej noci 

        
            
                                    13.6.2010
            o
            10:18
                        (upravené
                13.6.2010
                o
                14:05)
                        |
            Karma článku:
                11.32
            |
            Prečítané 
            2154-krát
                    
         
     
         
             

                 
                    Bolo už dávno po polnoci, keď som dávala manželovi dobrú noc. Na moju otázku, kedy sa chystá do hajan on, odpovedal neurčito. Pozeral volebné štúdiá, samozrejme s rukou na diaľkáči, striedajúc stanicu za stanicou. Nemohlo mu predsa nič ujsť, však to mnohí poznáte! Manžela som teda opustila a nechala som ho v obývačke s našim morčiatkom Moruškom a nedopitou fľaškou červeného vínka.
                 

                 
  
   Keď som sa o druhej prebrala, zistila som, že stále mám k dispozícii celú manželskú posteľ. Vedela som, že vyzvania typu: "Poď už spať, pozri koľko je hodín...." aj tak nepadnú na úrodnú pôdu, tak som to vopred vzdala. Darmo, je volebná noc, korunovaná poriadnou búrkou. Ktovie, žeby symbolika?   Zaspala som asi tvrdo, lebo už ani neviem, kedy sa manžel rozhodol zmeniť miesto svojho pobytu z obývačky na spáleň. Ráno som sa prebrala pár minút po siedmej. Tiež sa na mne prejavilo ponocovanie a tak som si pospala dlhšie, ináč vstávam skôr. O manželovi ani nehovorím, takto "dlho" nespal už dávno.   Keďže neviem len tak polihovať, tak som opatrne, potichúčky, vyliezla z postele a čo najtichšie som za sebou zavrela dvere. Zavreté zostali len pár sekúnd. Prudkým pohybom kľučky ich už otváral manžel. Nakoľko som bola v najmenšej miestnôstke nekomentovala som, že čo sa plaší, veď šiel spať nad ránom, nech si ešte pospí, alebo  niečo v tom zmysle... Zakričala som len ranný pozdrav.   O chvíľu som zmenila najmenšiu miestnosť za miestnoť iba o trochu väčšiu od nej a spustila som v pohode štart rannej hygieny. Vyleziem a môj manžel s vytrešteným pohľadom, oblečený, taška s notbookom pri dverách, obúva botasky. Kašľal na všetko, len rýchlo preč, veď je už "neskoro". Pýtam sa s úškrnom čo blbne, kde ide. S vážnym hlasom s miernou iskierkou podráždenosti mi odpovedal: "No kde asi? Do roboty". Tak to som už vedela koľká bije. Ešte som ho  chvíľu naťahovala, evidentne som  však nenarazila na zmysel pre humor a keď som sa už bála, že exploduje, tak som mu zvestovala, že nech sa kľudne vyzlečie, ukľudní, predýcha, odloží tašku, notbook, lebo že je dnes predsa nedeľa!   Hm, to ste mali vidieť! Nevedel, či má pokračovať v podráždenom tóne, či sa smiať. Asi uhádnete, čo zvíťazilo. Smiech, smiech, smiech.... Vybuchla som už aj ja.  Jednoducho skrat!   Urobila som môjmu vyplašenému chlapcovi raňajky. V kľude ich zjedol, pozrel čo nového na TA3 a začal sa znovu obliekať. Tentokrát už správne. Kraťasy, tričko - záhradkársky úbor. Zamietol po sebe stopy náznaku pondelka a vydal sa s výrazom uškŕňajúcich sa uletených včiel, do záhrady, do svojho pracovného raja. Ja som o chvíľu zaparkovala  vo svojom vareškovom raji a spustila som nedeľné tango, cha.       Aj takéto môže byť povolebné ráno...   Peknú nedeľu všetkým!       PS: nebonznite ma, prosím, že som ho bonzla 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




