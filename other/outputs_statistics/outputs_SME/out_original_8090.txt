
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Šplho
                                        &gt;
                Čo život dal
                     
                 Varovanie matkám zo zahraničia 

        
            
                                    5.6.2010
            o
            9:08
                        (upravené
                5.6.2010
                o
                9:20)
                        |
            Karma článku:
                11.66
            |
            Prečítané 
            1366-krát
                    
         
     
         
             

                 
                    Vydali ste sa na Slovensko, máte dieťa jedno či viac a ste na materskej dovolenke? Žijete v ilúzii že štát za vás platí dôchodkové poistenie, lebo máte povolenie na trvalý pobyt a podali ste si prihlášku a Sociálna poisťovňa vám ju prijala? Prebuďte sa! Sociálna poisťovňa si teraz podala vás...
                 

                 Moja manželka je poľka. Pred odchodom na materskú dovolenku pred tromi rokmi bola na Slovensku riadne zamestnaná päť rokov a má povolenie na trvalý pobyt už desiaty rok. V Sociálnej poisťovni vybavila všetky náležitosti, aj dôchodkové poistenie, ktoré matkám na materskej neplatí zamestnávateľ ale štát. Porodila zdravé dieťa, prešli dva roky a dva mesiace, medzičasom do rodiny pribudlo druhé dieťa.   Jedného pekného jesenného dňa manželke zatelefonovala pani zo Soc. poisťovne s radostnou novinkou, že dôchodkové poistenie, ktoré jej dva roky platil štát spätne zrušili, pretože podľa nových zákonov a objavov v SP na Záhradníckej ulici v Bratislave jej nárok na toto poistenie nevzniká. Zlý vtip alebo nedorozumenie? Presvedčený o platnosti jednej z tých dvoch možností zavolal som spomínanej pani, nech mi veci vyjasní, predsa len - manželka je cudzinka a nemusela všetko správne porozumieť.   Manželka síce je cudzinka, ale všetko správne porozumela. Pani zo SP mi vysvetlila, prečo to tak je a že sa pokojne môžem obrátiť na jej nadriadených, dokonca aj na tú najvyššie postavenú osobu. Vybavila ma aj všetkými potrebnými telefónnymi číslami. Zavolal som teda priamo najvyššie postavenej osobe. Už to nezaváňalo len zlým vtipom, ale aj podivnou prílišnou osobnou iniciatívou zodpovedných osôb, následkom čoho neraz vznikajú podobné rozhodnutia.   Najvyššie postavená osoba ma ubezpečila, že rozhodnutie je správne a všetko čo môžem podniknúť je podať žiadosť na preverenie situácie, musím pritom doložiť potvrdenie o tom, že manželka má na Slovensku trvalý pobyt. Ak manželka chce, môže si dôchodkové poistenie odteraz platiť sama, ale neodporúčala nám spätné vyplatenie za predošlé dva roky, lebo by sme museli zaplatiť aj úroky z omeškania.   Čistý surrealizmus hovorím si, Dalí je oproti skutočnému životu obyčajná handra. Podali sme teda žiadosť na preverenie situácie aj s fotokópiou dokladu o povolení na trvalý pobyt na území SR pre moju manželku. Dočkali sme sa oficiálnej odpovede na papieri, s uvedenými zákonmi aj ich interpretáciou. Podpísanej osobou poverenou vykonávaním funkcie riaditeľky pobočky. Manželke nárok na dôchodkové poistenie nevzniká. Pretože aj keď manželka má povolenie na trvalý pobyt, podľa toho, ako SP interpretuje zákon č. 48/2002 Z. z. toto povolenie nie je trvalým pobytom na území SR. Je to iba povolenie na trvalý pobyt. Ak chcete mať trvalý pobyt, musíte byť štátnym občanom SR, toto huncútstvo vás ale vyjde na takmer 700 eur. Je vám jasné, aká zaujímavá ponuka to je...   Pracovník cudzineckej polície v Bratislave ani zamestnanec poľskej ambasády tento rozdiel v definícii nevidí, podľa nich povolenie na trvalý pobyt je zároveň trvalým pobytom. Ale títo ľudia nesedia v SP na Záhradníckej ulici v Bratislave.   Keďže máme veľa známych, kde manželky sú cudzinky a nekúpili si slovenské občianstvo a majú iba povolenie na trvalý pobyt a vychovali aj štyri deti a niektoré ešte vychovávajú - okamžite sme ich varovali. Choďte do svojej SP a preverte si vašu situáciu. Išli a preverili si. Odpoveď - nezmysel, v iných pobočkách SP aj v iných mestách Slovenska tento problém nemajú a dôchodkové poistenie matkám cudzinkám naďalej vyplácajú. Pre nich povolenie na trvalý pobyt JE trvalým pobytom. Vraj si to dôkladne preverili a ich postoj k veci je takýto.   Je toto problém jednej pobočky? Je to problém tých ostatných pobočiek? Kde možno vec naozaj vyriešiť keď interpretácia zákona závisí od jej interpretátora? Je to tragédia? Skrivodlivosť? Nonsens? Sme naozaj tak bezmocní? Sme obete? Sám neviem.   Jasné ale je - sme bezmocní. Platiť súdne trovy a právnika si môže dovoliť Sociálna poisťovňa ale nie my. Lehota na odvolanie uplynula.  Ste cudzinka, matka jedného či viac detí, v svojej rodnej zemi už trvalý pobyt nemáte a tu máte len povolenie? Ste v zemi nikoho. Bez záchranného pásu. Prajem vám veľa zdaru.   Pomôž kto môžeš...   Pripájam kópiu rozhodnutia SP, dobre si ho preštudujte...    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šplho 
                                        
                                            Média, klamstvá, pravda, ľudia a slová čo stratili svoj význam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šplho 
                                        
                                            Posolstvo nočného života na Vajnorskej ulici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šplho 
                                        
                                            Z druhej strany: Patrícia Koyšová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šplho 
                                        
                                            Zamestnávateľ o ktorom snívam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šplho 
                                        
                                            Mentalita sólového gitaristu 2: Ace Frehley
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Šplho
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Šplho
            
         
        splho.blog.sme.sk (rss)
         
                        VIP
                             
     
        Výtvarník.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1920
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Ľudia
                        
                     
                                     
                        
                            Fejtón
                        
                     
                                     
                        
                            Kultúrka
                        
                     
                                     
                        
                            Čo život dal
                        
                     
                                     
                        
                            Občianska náuka
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            E. Gombrich
                                     
                                                                             
                                            H. Kissinger
                                     
                                                                             
                                            Iľf a Petrov - všetko zaradom
                                     
                                                                             
                                            Encyklopedie J. Suchého
                                     
                                                                             
                                            U. Eco - Skeptici a tešitelé
                                     
                                                                             
                                            P. Johnson - Zrození moderní doby
                                     
                                                                             
                                            H. de Balzac - Stratené ilúzie
                                     
                                                                             
                                            U. Eco - Meze interpretace
                                     
                                                                             
                                            K. Földvári - O karikatúre
                                     
                                                                             
                                            A. Makolkin - Name, hero, icon
                                     
                                                                             
                                            F. M. Dostojevskyj - Zápisky z mŕtveho domu
                                     
                                                                             
                                            N. V. Gogoľ - Bláznove zápisky
                                     
                                                                             
                                            W. Gombrowicz - Ferdydurke
                                     
                                                                             
                                            H. Kissinger - Umenie diplomacie
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            geniálny blog na voľný čas - Freeheart
                                     
                                                                             
                                            blog martina balogová
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




