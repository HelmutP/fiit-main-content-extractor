
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Šipikal
                                        &gt;
                Nezaradené
                     
                 Carnarvon: "Vidíte niečo?". Carter : "Ano. Nádherné veci!" 

        
            
                                    27.4.2010
            o
            18:05
                        (upravené
                28.4.2010
                o
                10:35)
                        |
            Karma článku:
                5.11
            |
            Prečítané 
            1122-krát
                    
         
     
         
             

                 
                      Písal sa 4.november 1922, keď Howard Carter, plný očakávania, vstúpil do ponurej hrobky kráľa Tutanchamona.  
                 

                  Desať rokov tvrdej driny, neúspechov a sklamaní, boli dôvodom, prečo mecenáš a priateľ Howarda Cartera, lord Carnarvon, povedal: dosť!, a odmietol ďalej  sypať peniaze do Egypta. Celé roky veľmi štedro financoval Carterove vykopávky, až stratil nádej. Ale Carter, s jeho neprekonateľnou túžbou dosiahnúť cieľ, presvedčil Carnarvona ešte raz, aby mu umožnil posledný pokus. Táto okolnosť, Carterova vytrvalosť a neskôr aj mladý nosič vody, ktorý upozornil na zvláštny kameň, keď takto náhodou objavil prvý schod , zohrali dôležitú úlohu v tom, že objav sa nakoniec uskutočnil. Z dnešného hľadiska uponáhlaného sveta, a vôbec prejavu sebaovládania, obdivujem neuveriteľnú Carterovu trpezlivosť, akú prejavil, kým vstúpil do hrobky. Po vykopaní a vyčistení schodov, ktoré viedli smerom dole, sa objavil uzatvorený vchod hrobky. Možno by sme očakávali prirodzenú reakciu, Carter sa bude ponáhľať, aby rýchlo prekonal poslednú prekážku a ... Lenže on naopak ,odolal tomuto veľkému pokušeniu, a prikázal schody znovu zasypať. Dôvod? Ako sa hovorí, bol "zo starej dobrej školy". Skutočné priateľstvo a určite aj hlboké ocenenie podpory zo strany Carnarvona, podnietili Howarda Cartera počkať na priateľa a spoločne prežiť ten neopakovateľný okamih.  Ale lord Carnarvon nečakal len tak niekde "za rohom". Telegram s neuveritľným obsahom ho zastihol v jeho sídle Highclere Castle, v Anglicku, a trvalo dva a pol týždňa (!) kým dorazil, spolu s  dcérou lady Evelyn, do Luxoru. Carnarvonove nadšenie muselo byť veľké. V telegrame stálo : "V údolí konečne uskutočnený objav. Nádherná hrobka s neporušenými pečaťami. Všetko znovu prikryté až do vášho príchodu. Blahoželám!"Pozvánku Carter poslal aj archeológovi A.R. Callenderovi. A konečne, 4.novembra 1922, stáli už všetci štyria pred hrobkou. Carter urobil otvor. To, čo nasledovalo, sa dozvedáme zo spomienok, ktoré neskôr napísal : "Pridržal som sviečku pri otvore a nazrel som dovnútra. Lord Carnarvon, lady Evelyn a Callender stáli nepokojne vedľa mňa a dychtivo čakali, až započujú verdikt. Najprv som neuvidel nič, horúci vzduch unikajúci z komory spôsobil, že plameň sviečky sa mihotal, avšak postupne si oči zvykali na svetlo a z hmly predo mnou sa začali pomaly vynárať detaily miestnosti, neznáme zvieratá, sochy a zlato - všetko sa ligotalo zlatom. Na okamih, ktorý sa ostatným musel zdať večnosťou, som onemel prekvapením, a keď sa lord Carnarvon, neschopný vydržať ďalej toto napätie, s úzkosťou v hlase spýtal: "Vidíte niečo?", dokázal som zo seba vydať len: "Áno. Nádherné veci". To bola veta, ktorá vošla do histórie. Na inom mieste píše: " Naše dojmy a nadšenie sa dajú ťažko popísať, pretože lepšie osvetlenie odhalilo pred nami veľkolepú zbierku pokladov." Čo uvideli?    Keď vstúpili do hrobky, na začiatku predsiene sa z tmy  vynorili tri veľké pozlátené postele, znázorňujúce hrocha, kravu a levicu. Pod strednou posteľou boli poukladané zásobníky jedla. Ako sa neskôr zistilo, bolo v nich konzervované  hovädzie mäso a husacina, faraónove zásoby na dlhej ceste. K tomu mal k dispozícii aj  pivo, víno, chlieb, cesnak a med. Ale týmto veciam, v tej chvíli, venovali štyria priatelia pozornosť len okrajovo. Pochopiteľne. Pri posteliach sa nachádzali krásne, vykladané a zdobené skrinky, kvety, alabastrové vázy, poháre rôznych tvarov, biele truhlice, vyrezávané stoličky. Na konci predsiene, pred  vchodom do siene pokladov a pohrebnej komory, stali dve čierne sochy z ebenového dreva, vo veľkosti dospelého muža. Strážcovia v zlatých sandáloch, držiac v rukách pozlátené žezlá a palice. Sochy, okrem toho, že strážili večný pokoj faraóna, predstavovali aj jeho životnú silu, "Ka". V sieni pokladov sa nachádzali zlaté postavy Tutanchamona v rôznych pozíciách, majstrovsky zhotovené šperky, sošky bohov, odevy a ďalšie plné truhlice. V strede siene stála socha šakala umiestnená na prepychovej zlatej skrini, ktorá bola plná skvostných umeleckých predmetov zo zlata a alabastru. Blízko tejto skrine boli  malé urny z vysušenými vnútornými orgánmi Tutanchamona, čo síce nekorešpondovalo s ligotajúcim sa okolím, ale bol to cenný nález pre ďalší vedecký výskum. V pohrebnej komore našiel Carter to, čo hľadal najviac. Všetky pochybnosti vystriedala úľava a ďalšie nadšenie, keď otvorili prvú zo štyroch obrovských pozlátených truhiel, ktoré mali chrániť múmiu faraóna. A múmia na svojom mieste naozaj bola! Tutanchamon odpočíval v najspodnejšej truhle z masívneho zlata o váhe 110 kg. Maľby na stenách tej istej miestnosti znázorňovali  faraóna ako boha. Keď sa dnes spomenie meno Tutanchamon, väčšine prvé čo zíde na um, je preslávená zlatá maska, ktorú poznáme z nespočetných fotografií a možno ju obdivovať v Káhirskom múzeu. Táto maska zakrývala hlavu múmie a zrejme verne znázorňuje tvár mladého faraóna. Starovekí majstri vypracovali tento klenot podľa najprísnejších kritérií egyptského umenia. Keď sa vrátime ešte nachvíľu do predsiene, nespomenul som bojové dvojkolesové vozy. Boli umiestnené oproti pozlateným posteliam, akosi zvláštne poskladané a nedbalo uložené.  Zatiaľ čo niektoré predmety boli len v jednej, alebo inej miestnosti, po celej hrobke bolo obrovské množstvo modelov lodí rôznych veľkostí. Stalo sa. Tutanchamonova hrobka vydala poklad aký dovtedy, ani potom na svete nebol.   Ak boli z počiatku Carter s Carnavornom ohúrení najmä zlatom a truhlou s múmiou - a kto by nebol, neskôr uskutočnili podrobný popis a fotografovanie každého predmetu. Vyžiadalo si to veľa času a úsilia. Do mozaiky uspechov v údolí kráľov treba vložiť aj zmienku o skvelej práci, akú odviedol výnimočný fotograf, Harry Burton. Nasledoval niekoľkoročný výskum, čo prinieslo ďalšie zaujímavé objavy.   Egypt pod pieskom skrýva ešte veľa pokladov. Vyriekne niekto slávnu Carterovu vetu ešte raz?                               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Tajnostkári starí egypťania.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Lišiacky človek, skvelý dejepisec. Josephus Flávius.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Možno vieme, prečo papyrus, ani kameň, nič nehovoria o Mojžišovi.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Šipikal
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Šipikal
            
         
        sipikal.blog.sme.sk (rss)
         
                                     
     
        Niečo o mne? Rád si vypočujem Shirley Bassey, Sarah Brightman, Robbieho Williamsa, či Barbru Streisand. Mám rád fitness v teórii i praxi, zaujímam sa o všetko, čo súvisí so starovekým Egyptom a biblickou archeológiou. Milujem atmosféru 20.-30. rokov 20.storočia so všetkým tým rozbiehajúcim sa pokrokom a nefalšovanou zdvorilosťou. Relaxom je pre mňa príroda, rodina, šport a spoločnosť priateľov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1321
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




