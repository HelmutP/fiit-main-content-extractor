

 I keď Smer získal cca. 35 %, je mu to na nič. Odpálil si svojich koaličných partnerov a tak nemá s kým ísť do vlády. Chváľa Pánu Bohu, pretože ak by pokračovala súčasná vládna koalícia, Slovensko by bolo na tom veľmi zle. Vyrabovaná pokladnica, obrovské dlhy a zlé maďarsko - slovenské vzťahy. To však nebude. Miesto toho Slovensku vzniká nová nádej v novej pravicovej koalícii. Už na začiatku hovorím, že to nebude lízanie medu. Vôbec nie. 
 Občania sa musia pripraviť, že sa bude šetiť, kde sa dá. Aj odborníci vraveli, že nová vláda bude musieť šetriť, aby sa Slovensko neprimerane nezadlžovalo. Nová pravicová vládla stojí pred výzvami. Ako ich zvládne, uvidíme za 4 roky. A voliči ich ohodnotia vo voľbách. 
 Výzvy adresované pravici: 
 - zahraničná politika: 
 zlepšenie vzťahov s Maďarskom, 
 nepožičanie peňazí Grécku, 
 budovanie dobrých vzťahov s Európov a USA 
 - sociálna politika 
 reforma sociálnych dávok /štát by nemal živiť tých, čo nechcú pracovať, rozvoj aktivačných prác/ 
 pomoc deťom z detských domovov, aby sa vedeli uplatniť 
 reforma dôchodkového systému 
 - ekonomika 
 rozvoj podmienok podnikania 
 zníženie daní a odvodov 
 vytváranie nových pracovných miest 
 - školstvo 
 zlepšiť kvalitu škôl 
 nová reforma školstva /školy by nemali chŕliť absolventov pre úrad práce, absolventi by sa mali uplatniť v odbore, ktorý vyštudovali, zrušiť financovanie na žiaka, zaviesť iné kriéria financovania školstva/ 
 čiastočne spoplatniť vysoké školy 
 - zdravotníctvo 
 Pacienti by mali platiť lekárom na ruku a nie poisťovniach. S poisťovňami sú problémy a neplatia výkony tak ako by mali. 
 dobré pracovné podmienky pre začínajúcich lekárov 
 Toto sú hlavné výzvy pravice. Pevne verím, že sa pravicovej vlády podarí niečo urobiť. Bude to mať veľmi ťažké, lebo reformy u ľudí nie sú populárne, ale pre chod krajiny sú veľmi dôležité. Veľmi dôležité sú aj pre budúcnosť krajiny. 

