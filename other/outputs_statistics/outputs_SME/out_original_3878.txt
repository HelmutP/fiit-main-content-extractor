
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Fandel
                                        &gt;
                Nezaradené
                     
                 Dav ľudu vydal Krista na kríž 

        
            
                                    2.4.2010
            o
            23:11
                        |
            Karma článku:
                5.92
            |
            Prečítané 
            861-krát
                    
         
     
         
             

                 
                    Veľká noc je veľkolepým príbehom, ukrývajúcim veľa univerzálnych právd pre všetkých ľudí, bez ohľadu na ich vieru...
                 

                1. Dav je nestály. Ak kríčí raz „hosana“, je možné, že o niekoľko málo dní bude hučať „ukrižuj“ 2. Nikto nie je doma prorokom 3. Aj jeden z našich najbližších priateľov nás môže zradiť, za pár drobných 4. Aj jeden z najbližších priateľov nás môže zaprieť 5. V ťažších životných situáciách zostávame často opustení 6. Mama je osoba, ktorá nás nikdy nezradí, ktorá aj v tých najťažších chvíľach zostáva pri nás 7. Sfanatizovaný dav ľudí vydal Krista na kríž. Verejná mienka nie je synonymom pre mienku múdru a spravodlivú 8. Ľudia napriek tomu dávajú na názor verejnej mienky a rozhodujú sa podľa neho. Pričom následne si alibisticky umyjú ruky 9. Napriek tomu, že sú situácie, keď nás druhí urážajú, keď sa nám posmievajú, vydržme 10. Nesúďme iných. „Otče, odpusť im, pretože nevedia, čo činia.“ 11. Kto je bez viny, nech hodí kameňom 12. Každý z nás si v živote nesie svoj kríž 13. V našom živote sme každú chvíľku bičovaní osudom 14. Napriek ťažobe kríža či bičovaniu osudom, musíme vždy vstať pod krížom a niesť ho ďalej 15. Pomôže, ak máme pri sebe niekoho, kto nám s krížom aspoň na časti našej Krížovej cesty môže a chce pomôcť 16. Každý z nás si však až na Golgotu svoj kríž musí vyniesť sám, inej cesty niet 17. Aj tá najťažšia chvíľa v našom živote však nikdy nie je beznádejná. Pretože nádej existuje. Po každom „ukrižovaní“ prichádza „zmŕtvyvchstanie“ 18. Ak nezabudneme žiť lásku, bude sa nám na tomto svete žiť lepšie Som veriaci človek. A práve viera mi každoročne pomáha intenzívnejšie prežívať tajomstvá Veľkej noci. Pomáha mi aj v tých ťažších časoch hľadať sily niesť svoj kríž, aj vtedy, keď sa mi zdá, že tých síl už veľa nezostáva. Vtedy si uvedomím jediné – že musím vydržať. Že v minulosti tu bol niekto, kto vydržal oveľa viac. A preto nemám prečo to vzdať práve ja. Veď náš život je práve o tej ceste. Len na nej spoznávame samých seba. Len na tej svojej ceste spoznávame zmysel a pravé hodnoty života...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Zbraň na votrelcov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            ...až na hranicu úzkostnej tmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Tolerantný je len ten človek, ktorý je za registrované partnerstvá?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Učebnicový dvojaký meter z Haagu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Prečo?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Fandel
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Fandel
            
         
        fandel.blog.sme.sk (rss)
         
                                     
     
         Obyčajný človek 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    110
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1617
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




