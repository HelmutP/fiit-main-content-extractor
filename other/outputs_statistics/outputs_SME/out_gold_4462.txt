

   aaaaaaaaaaaaaaaaaaaaaa 
   aaaaaaaaaaaaaaaaaaaaaa 
   aaaaaaaaaaaaaaaaaaaaaa 
  
 Rozhovor JÁRAY-Dvurčneskij 
 Touto cestou predkladám širokej laickej i odbornej verejnosti, môj rozhovor s riaditeľom Matematického  Ústavu SAV, pánom : 
  
       
 prof. RNDr. Anatolijom Dvurčenskijom DrSc.  
 Úvodom dovolím si predstaviť účastníka tohto rozhovoru: 
 Prof. RNDr. Anatolij Dvurčenskij DrSc. 
 Je riaditeľom Matematického ústavu SAV, ako aj vedec roka 2005. Rozhodol o tom nasledovný kolektív matematikov zo SAV: 
  
 Jeho najdôležitejšie vedecké výsledky sú nasledovné:  Vyriešenie problému združených rozdelení pozorovateľných v kvantových logikách (spolu s doc. RNDr. Pulmanovou, DrSc.), Zovšeobecnenie a aplikácie Gleasovnovej vety v kvantových logikách Hilbertovho priestoru, tenzorový súčin diferenčných posetov, reprezentácia pseudo MV-algebier a pseudo efektových algebier pomocou unikátnych čiastočne usporiadaných grúp. 
 Slávny výrok Anatolija Dvurčenskijho: 
 „Matematik musí veľa vedieť, aby mohol málo povedať.“  
  
 aaaaaaaaaaaaaaaaaa 
 
  Priebeh rozhovoru je nasledovný: 
  JÁRAY. 
  Pán riaditeľ MÚ SAV, môže vaša osoba reprezentovať úroveň matematických poznatkov slovenských matematikov na najvyššej úrovni ? 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 
 
  
 
 
   JÁRAY. 
 Súhlasíte so mnou, že výsledky z vyučovania matematiky na školách a univerzitách SR sú najhoršie zo všetkých vyučovaných predmetov? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 

  
 
 
  JÁRAY.  
 Mohli by ste stručne uviesť, z uhla vášho pohľadu, aké sú príčiny tejto neblahej reality. 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Jednoznačne ide o zlý spôsob výučby matematiky a to hlavne z toho dôvodu, že sami učitelia nie sú odborne pripravení vyučovať matematiku, ale aj nízke platy učiteľov matematiky.  

  
 
 
  JÁRAY. 
 Tam kde matematiku vyučuje matematický odborník s vysokým platom, tak tam s matematikou nie sú problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Jednoznačne nie. 

  
 
 
  JÁRAY. 
 Stručne povedané, keby ste vy osobne vyučovali matematiku, tak vaši žiaci by nemali s matematikou žiadne problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 

  
 
 
   JÁRAY. 
 Pán profesor, matematika učí, že každé matematické (teda bezexponenciálné) číslo násobené číslom nula (0) dáva výsledok nulu (0). Súhlasíte s týmto tvrdením matematiky?  
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 

  
 
 
  JÁRAY. 
 Aby som vec nekomplikoval, uvediem iba súčin čísel:  
 1.0 = ? 
 Tento súčin je natoľko zrozumiteľný občanom SR, rôznej národnosti, že by sa zdalo, niet o čom hovoriť v súvislosti s tým súčinom.  
 Ale keď sa budeme zaujímať o veľkosť exponentov uvedených čísel tohto súčinu, vec sa stane neriešiteľnou. V prvom rade preto, lebo aj podľa pravidiel matematiky, tento súčin nie je realizovateľný, nakoľko súčin: 
  11. 01, je súčin typu a.b a s tým sa už nič viacej robiť nedá. 

 Pre prípad súčinu typu: 10.00 = 11.11. lebo keď každé číslo, tak aj číslo 0  umocnené na nultú 00, musí byť podľa matematiky číslom 11.  
 
 
 Súhlasíte s tým? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Nič iného mi neostáva! 

  
 
 
 JÁRAY. 
 Načo sa vyučuje táto poučka, keď ona ani v matematike, ani v materiálnej praxi neplatí.  
 Uvádzam dva príklady. 

  
 
 
  
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
  
  
  

 
 
  
  
  
x   =  
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 
 
   
  

  x  =   
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 
 
 Dvurčenskij. . 
 Súhlasím. Matematika na kone a kravy neplatí. 

  
 
  JÁRAY. 
 Načo táto matematická poučka teda platí?  

 
  

 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
  
 Dvurčenskij. . 
 Iba pre čísla bez exponenta. Iba pre čisté, slobodné, od materializmu oslobodené, metafyzické, čiže abstraktné, ničím nepolapiteľné matematické myšlienky. A tie sú pre vás, ako pre materialistu neprístupné.  
 Tie čísla nepoznajú ani kone ani kravy, ale ani atómy chemických prvkov, ale ani sám pán Boh. Tie čísla poznáme iba my, matematici, aj v SAV, my ľudia s mozgovými zrastami. 
  
 Tento text organicky doplňujú nasledovné články: 
 http://jaray.blog.sme.sk/clanok.asp?cl=206648&amp;bk=42411  
 http://jaray.blog.sme.sk/clanok.asp?cl=215377&amp;bk=98564  
   
  

