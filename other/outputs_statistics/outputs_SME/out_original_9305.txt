
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Baričák
                                        &gt;
                Tak plače a smeje sa život
                     
                 Keď sa nad hrobom klame (pohľad zľava) 

        
            
                                    21.6.2010
            o
            9:50
                        (upravené
                22.6.2010
                o
                6:42)
                        |
            Karma článku:
                14.50
            |
            Prečítané 
            3701-krát
                    
         
     
         
             

                 
                    Otec môjho veľmi dobrého kamaráta bol pijan. Veselý alkoholik pre okolie, skrytý agresívny a výbušný cholerik pre rodinu. Keď zomrel, išiel mu na pohreb aj spomínaný syn, ktorý sa posledné roky dušoval, že sa tam neukáže. Plakal. A ja som bol tomu rád, pretože som v tom videl časť odpustenia a pokory. Stál som v kúte a žasol...
                 

                 Farár nešetril chválou nad truhlou skonanej duše. Spomínal ako dotyčný rád čítal knihy, hral ping-pong, často chodil do hory, miloval rodinu, bol veriaci... Nemo som stál v poslednej rade a prevracal očami nad celou tou fraškou. Či ešte aj pri poslednej rozlúčke musíme hrať pred sebou divadlo. Ak si odpustím tretinu geriatrie, ktorá chodí na každý pohreb ako Francúz na partičku petangu do parku a vonkoncom ani netuší aký človek zomrel, tak ostatok zúčastnených dobre vedelo, že skonaný muž v žiadnom prípade nebol vzorná hlava rodiny. Že vymenované kladné veci pestoval možno do prvej polovičky života, ale potom sa venoval hlavne poháriku, vysedávaniu v krčme a grobiánstvu. Že do kostola nechodil a meno božie vyslovil iba keď klial. Nie, mne nikdy neublížil, mal som ho ako malý chlapec rád, pretože stále trepal somariny. No jeho syn, môj dobrý kamarát, mi veľakrát uplakaný rozprával čo sa deje pod pokrievkou, keď sa zavrú jedny sídliskové dvere.   Mal som chuť vystúpiť z radu ako Mulla Nasredin, odpľuť si ako to vedia len jednoduchí dedinskí ľudia, teda tak, že im to nikto nebude mať za zlé. A v tom pľuvanci by bolo všetko: klameš, pán farárko! Nesnaž sa maľovať letné kvety na zhorený šporák, my sme s ním žili, my ho poznáme. Načo táto komédia, polopravdy, vymyslené vety, zamlčané skutočnosti? Zo súcitu? Zo slušnosti? Veď tá duša sa narodila a splnila na svojej púti čo mala. Raz bola učiteľom, inokedy boli iní učiteľom pre ňu. Raz bola týraná, inokedy bola tyranom. Tak to tu totiž chodí, tak už raz prestaňme hrať sami sebe maškarádu, túto pozemskú frašku. Veď aj to, že človek si rád vypil sa dá povedať pekne. Aspoň sa človek nebude cítiť akoby si pomýlil cintorín a pohreb človeka.   Keď raz zomriem, chcem aby sa o mne hovorili aj moje hriechy. Že som v slušných reštauráciách olizoval nožík a s úsmevom na tvári jedol rukami. Že som sa veľakrát opil, fajčil cigarety aj trávu, nadával a správal sa ako nevybúrený buran, pubertiak, idiot, somár... Že som ako mladý zapaľoval smetiaky, čmáral po panelákových stenách, vyrýval vo výťahu názvy mojich obľúbených kapiel. Že som bol drak aj čert, lebo bez toho by som nikdy nemohol zažiť odlesk anjela. Že som sa býval namyslený, nadradený, povýšenecký. Že som ohováral, šíril klebety, súdil, ubližoval, závidel, padal na dno a snažil sa od neho odraziť. Že som bol obyčajný slaboch, ľudská hnida, ale že som nemal rád, keď sa niekto pretvaroval. Že som sa vždy snažil dopátrať pravdy a nadovšetko si cenil priznanie viny.   Preto, pán farár, ak máte nad mojim hrobom klamať, ani naň nechoďte. Ľudia si to všetko pekné ako aj zlé, čo som tu vykonal, aj tak povedia. A ja budem nad nimi lietať a verte mi, že im to nebudem mať za zlé. Aj keby klamali... Oni ma totiž poznali. Vy nie. Vy sa iba snažíte rozprávať, aby to pekne znelo. No falošné vety chcú počuť len falošní ľudia. Tak buď si nalejeme čistého vína, alebo si to posledné varieté na môj účet odpustite.   Ďakujem ti, Bože, že si ma vypočul.       Buďte šťastní, Hirax           Aktivity Hiraxa:   Utorok, 22. 6. 2010, Zlaté Moravce, súkromná čítačka v reedukačnom zariadení pre mladistvých. Vstup jeden "hriech" a jeden úsmev.    Utorok, 22. 6. 2010 o 16:02, Piešťany, Kníhkupectvo MODUL (Winterova), Hiraxova čítačka, vstup jeden úsmev.    Pondelok, 28. 6. 2010, Topoľčany, 17:00 hod Galéria (program pre mladých - spevokol, básne Hirax), 18:00 reštaurácia Kaiserhof (Námestie M. R. Štefánika). Čítačka Hirax, vstup jeden úsmev.   Máj-jún 2010: Výstava Hiraxových fotografií z Thajska v martinskej kaviarničke Kamala. Pozor, nejedná sa o žiadnu "galériu", ale o 12násť záberov, ktorými som sa snažil vystihnúť túto krajinu. Vstup jeden úsmev.   Štvrtok, 4. 11. 2010, Svet, Oficiálne vydanie Hiraxovho štvrtého románu "Nauč ma umierať".     Štvrtok, 4. 11. 2010, 16:37, Bratislava, Svet knihy Bene Libri (v centre mesta, Obchodná, druhé poschodie). Prvá Hiraxova čítačka k jeho štvrtému románu "Nauč ma umierať". Vstup jeden úsmev.   Utorok, 9. 11. 2010, Považská Bystrica (bude upresnené). Čítačka Hirax, vstup jeden úsmev.   Streda 10. 11. 2010 o 11:02 hod., Nitra, Krajská knižnica K. Kmeťka. Čítačka Hirax, vstup jeden úsmev.   Štvrtok 11. 11. 2011(-1) o 11:11, Šaľa, gymnázium. Čítačka Hirax, vstup jeden úsmev.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (168)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Ako urýchliť zámer? Obetou a pokorou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (2.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (1.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Všetci sme mágovia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Vnútorným dialógom k osobnému šťastiu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Baričák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Baričák
            
         
        baricak.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ľudia majú neuveriteľnú schopnosť pripísať všetko, čo bolo napísané autorovi, že to na vlastnej koži aj sám prežil. Prehliadajú schopnosť vnímania, pozorovania sveta a pretransformovania ho do viet s úmyslom pomôcť druhým. Ale na druhej strane sa mi dobre zaspáva s pomyslením, že ostatok sveta na mňa myslí. Cudzí ľudia o mne vedia všetko, teda aj to, čo neviem ani ja sám. Ďakujem im teda za ich všemocnú starostlivosť! Buďte všetci šťastní, prajem Vám to z celého môjho srdca. Hirax &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/anketa_0.php?id=754643" href="http://blueboard.cz/anketa_0.php?id=754643"&amp;amp;gt;Anketa&amp;amp;lt;/a&amp;amp;gt; od &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/" href="http://blueboard.cz/"&amp;amp;gt;BlueBoard.cz&amp;amp;lt;/a&amp;amp;gt; 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    558
                
                
                    Celková karma
                    
                                                7.12
                    
                
                
                    Priemerná čítanosť
                    3665
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šlabikár šťastia 1.
                        
                     
                                     
                        
                            Šlabikár šťastia 2.
                        
                     
                                     
                        
                            Vzťahy (Úvahy)
                        
                     
                                     
                        
                            Tak plače a smeje sa život
                        
                     
                                     
                        
                            Zachráňte malého Paľka!
                        
                     
                                     
                        
                            Po stopách komedianta
                        
                     
                                     
                        
                            Sárka Ráchel Baričáková
                        
                     
                                     
                        
                            Sex (Úvahy)
                        
                     
                                     
                        
                            NEVERŠOVAČKY (Básne)
                        
                     
                                     
                        
                            PRÍBEH MUŽA (Básne a piesne)
                        
                     
                                     
                        
                            SEKUNDU PRED ZBLÁZNENÍM (Román
                        
                     
                                     
                        
                            KÝM NÁS LÁSKA NEROZDELÍ (Román
                        
                     
                                     
                        
                            RAZ AJ V PEKLE VYJDE SLNKO/Rom
                        
                     
                                     
                        
                            ČESKÁ REPUBLIKA (CESTOPIS)
                        
                     
                                     
                        
                            EGYPT (Cestopis)
                        
                     
                                     
                        
                            Ekvádor (Cestopis)
                        
                     
                                     
                        
                            ETIÓPIA (Cestopis)
                        
                     
                                     
                        
                            FRANCÚZSKO (Cestopis)
                        
                     
                                     
                        
                            INDIA (Cestopis)
                        
                     
                                     
                        
                            JORDÁNSKO (CESTOPIS)
                        
                     
                                     
                        
                            KOSTARIKA (Cestopis)
                        
                     
                                     
                        
                            Mexiko (Cestopis)
                        
                     
                                     
                        
                            Nový Zéland (Cestopis)
                        
                     
                                     
                        
                            PANAMA (Cestopis)
                        
                     
                                     
                        
                            POĽSKO (Cestopis)
                        
                     
                                     
                        
                            SLOVENSKO (Cestopis)
                        
                     
                                     
                        
                            THAJSKO (Cestopis)
                        
                     
                                     
                        
                            USA (Cestopis)
                        
                     
                                     
                        
                            VIETNAM (CESTOPIS)
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Niečo o zrkadlení a učiteľoch
                                     
                                                                             
                                            Vlastné šťastie ide cez prítomnosť
                                     
                                                                             
                                            Áno, dá sa naraz milovať dvoch ľudí
                                     
                                                                             
                                            Chcete zmeniť vzťah? Zmeňte najprv seba.
                                     
                                                                             
                                            Chcete byť šťastnými? Prijmite sa.
                                     
                                                                             
                                            Ohovárajú vás? Zraňujú vás klebety? Staňte sa silnými!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Liedloffová Jean - Koncept kontinua
                                     
                                                                             
                                            Alessandro Baricco - Barbari
                                     
                                                                             
                                            Christopher McDougall - Zrození k běhu (Born to run)
                                     
                                                                             
                                            Ingrid Bauerová - Bez plenky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            The Killers
                                     
                                                                             
                                            Brandom Flowers
                                     
                                                                             
                                            Sunrise Avenue
                                     
                                                                             
                                            Nothing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Martin Basila
                                     
                                                                             
                                            Spomíname:
                                     
                                                                             
                                            Moskaľ  Peter
                                     
                                                                             
                                            Miška Oli Procklová Olivieri
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HIRAX Shop
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Skutočná Anna Kareninová bola Puškinovou dcérou
                     
                                                         
                       Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                     
                                                         
                       Surinam - zabudnutá juhoamerická exotika
                     
                                                         
                       Hodvábna cesta - 56 dní z Pekingu do Teheránu 2.časť ( Uzbekistan, Turkménsko, Irán)
                     
                                                         
                       Detskí doktori, buďte detskí
                     
                                                         
                       Nemohli nič viac
                     
                                                         
                       Metalový Blog - Folk metal crusade
                     
                                                         
                       Veľká noc vzišla z pohanských sviatkov
                     
                                                         
                       Stačilo p. premiér, oblečte sa prosím
                     
                                                         
                       Jiřina Prekopová: Podmienečná láska je bezcenná
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




