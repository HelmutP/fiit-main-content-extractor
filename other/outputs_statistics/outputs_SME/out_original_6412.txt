
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Hruška
                                        &gt;
                Trips
                     
                 Atény 

        
            
                                    14.5.2010
            o
            9:00
                        |
            Karma článku:
                6.12
            |
            Prečítané 
            1643-krát
                    
         
     
         
             

                 
                    Pred pár týždňami som sa bol pozrieť v kolíske demokracie na to, čo z nej zostalo. Svoj pobyt som si tu kvôli sopke predĺžil o tri dni, takže som videl aj to čo som nechcel, respektíve neplánoval.
                 

                 Hneď po prílete a ubytovaní sa v hoteli v prímorskej časti Atén, sme okúsili čo je to právo na štrajk. MHD nepremávalo celé poobedie, tak sme sa nehnali za pamiatkami ale zahájili kúpaciu sezónu v mori. Našťastie večer už všetko premávalo tak ako malo a my sme mohli ísť do mesta. Do centra premávala nová električka, ktorej celú trať postavili pred olympiádou 2004. Cesta na námestie Syntagma, pred grécky parlament bola dlhá (32 zastávok) a trvala 50 minút ale aspoň sme nemuseli prestupovať. Vystúpili sme priamo pred parlamentom a začali prvú zbežnú obhliadku mesta, z ktorej vzniklo len zopár nočných fotiek.      Parlament v noci a pre porovnanie aj cez deň         Pohľad na antickú knižnicu      Vysvietená Akropola nad mestom, pod ňou vľavo Veža Vetrov   Ďalšie dni sme podľa turistického sprievodcu prechádzali centrom mesta. Celé centrum sa točí (aj doslova) okolo Akropole. Pešia zóna vedie dookola popod celý kopec na, ktorom stojí Akropola, takže skončíte tam kde ste začali. Ja som začal pri múzeu Akropole, ktoré je postavené s výhľadom na starovekú Akropolu.      Mapa centra Atén s pamiatkami - znázorňuje 2 okruhy. Malý trval pomalou chôdzou pol hodinu, veľký hodinu      Múzeum Akropole - moderna verzus antika      Vydláždená cestička pokračovala popod kopec k Odeónu Heroda Attika, čo je staré antické divadlo, v ktorom sa dodnes konajú predstavenia (vystupoval tu napríklad Pavarotti). Toto je vec, ktorá sa mi z Atén páčila asi najviac, preto som ju fotil z každého uhla.                    Ďalšiu zástavku som si spravil na Fillapapposovom pahorku. Je to kopcovitý parčík, kde sa po menšej túre, naskytá výborná vyhliadka na Akropolu.       Pohľad z Filapopposovho pahorku na Akropolu (tie hradby v spodnej časti, sú zadnou stenou Odeonu Heroda Atttika)      Po dokonalom obzretí Akropole z vonku a z diaľky som si povedal, že je najvyšší čas na pohľad z blízka. To som ale netušil, že najvýznamnejšia grécka pamiatka je otvorená len do 15:00 a posledný vstup je o 14:30, napriek tomu, že každý turistický sprievodca uvádza otváracie hodiny do 19:00. Takže na Akropolu som sa dostal až na druhý deň. Vstupné bolo 12 eur a platilo aj na ďalšie pamiatky v okolí. Výstup hore nebol náročný ale pohľad z blízka na Akropolu, ktorú už niekoľko rokov  rekonštruujú bol trochu sklamaním. Tá monumentálnosť, ktorú vidno z diaľky sa vytratila a zblízka bolo vidno viac žeriav ako celistvosť objektu. Okrem toho nás päť minút pred 15:00 začali vyháňať zamestnanci, že o chvíľu bude CLOSE a im padne. Hnali nás pred sebou k východu ako stádo oviec pričom spýtavé pohľady okolo ustupujúcich ľudí  boli jasné - toto nie je normálny prístup k turistom. Ja by som dodal toto sa nestane ani v bratislavskej ZOO.               Poslednou, ale pre mňa najdôležitejšou pamiatkou, ktorú som si chcel v Aténach pozrieť, bol starý olympijský štadión. Vstupné bolo 3 eurá, dostal som slúchadlá s nahratým sprievodcom a mohol som vstúpiť. Prehliadka mala zvláštnu atmosféru - na ovále si turisti dávali poklusom čestné kolečko, zo slúchadiel znel krik fanúšikov a ja som sa cítil ako v gladiátorskej aréne, nad ktorou sa vznášali olympijské kruhy.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            10 fráz z prezidentskej diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Sčítanie, druhý krát.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Sme anonymné ovce?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Taká malá trafika pre otca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Trip autom – part 4 – Grenoble, Ženeva, Zürich, Vaduz
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Hruška
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Hruška
            
         
        michalhruska.blog.sme.sk (rss)
         
                                     
     
        Pracujem v IT, okrem toho hrám vodné pólo, cestujem, venujem sa rodine a sem tam ma niečo tak zasiahne, že o tom napíšem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2440
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Trips
                        
                     
                                     
                        
                            Benelux
                        
                     
                                     
                        
                            Francúzsko
                        
                     
                                     
                        
                            Názory
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Francúzske piesne
                                     
                                                                             
                                            Schwangau
                                     
                                                                             
                                            Bezpečnosť na Internete
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Cmorej
                                     
                                                                             
                                            Tibor Pospíšil
                                     
                                                                             
                                            História
                                     
                                                                             
                                            Excel
                                     
                                                                             
                                            Zdenko Somorovsky
                                     
                                                                             
                                            Kybi - cestovateľ na bicykli okolo sveta
                                     
                                                                             
                                            Peter Dzurinda
                                     
                                                                             
                                            Roman Herda
                                     
                                                                             
                                            Miroslav Babič
                                     
                                                                             
                                            Tomáš Kubuš
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       10 fráz z prezidentskej diskusie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




