

 Minulý mesiac mi nejako nevyšiel čas na každomesačné video preto dnes pridávam jedno z mojich najobľúbenejších. No, lepšie povedané, k jednej  z mojich najobľúbenejších piesní. 
 Martin Wiese sa na mojom rebríčku obľúbených hudobníkov usídlil na prvom mieste už dávno. Začalo to skupinou Persone v ktorej bol po väčšinu času lídrom. Skupina Persone už nanešťastie nie sú aktívna, aj keď sa nikdy nerozpadla, zjavne nechcú spraviť unáhlené prehlásenie... Lenže keď sú členovia rozlezení po celej Zemi tak sa im ťažko hrá... Martin sa však s takouto nútenou prestávkou nechcel zmieriť a preto si založil nový sólový projekt. Pozval si kamošov zo svojej ďalšej skupiny, Jetpilot, takto spolu vystupujú pod názvom Martin &amp; la talpoj. Avšak na nahrávanie  sólového albumu mu popri druhej skupine, práci, rodine, atď. atď ostalo málo času, komentoval to slovami: "Nahrával som vo voľných chvíľkach, hodinu vtedy, polhodinu vtedy." Keďže chcel kvalitu aj technickú aj umeleckú tak nahrávanie trvalo 2 roky. Tieto problémy pri nahrávaní vyjadril aj názvom albumu, podľa titulnej piesne ho nazval Pli ol nenio (v esperante: Viac ako nič). Pieseň Pli ol nenio nahral už so skupinou Persone, vtedy ale v akustickej verzii tak sa po rokoch dočkala vynovenia. Nápodobne sú na tom piesne Homoj kiel mi a Vivo duras sed vi molas, prvú však nevydali Persone vôbec a druhou sa Martin sólovo zúčastnil na výberovke Vinilkosmo kompil' Volumo 2. 
 Martin zjavne nemá veľkú záľubu vo videoklipoch. Napríklad skupina Persone definovala rámec kvality esperantskej hudby na dlhé obdobie, bola pozývaná na veľké medzinárodné podujatia, slová ich piesní dokonca prešli do slangu ale ani k jednej piesni z ich 5 albumov nemajú video. Martin &amp; la talpoj už konečne má na albume aj videá ale ide len o videorozhovory. Dokonca video, ktoré Vám predstavujem dole nie je spracované extrémne profesionálne, vytvoril ho Martinov známy počas podujatia vo Francúzku (kde Martin koncertoval). Podstatu piesne však vyjadruje dobre a vhodne ju dopĺňa. 
 Kto by mal problém s textom tak originál je tu, dostupný je aj slovenský preklad. A kto si chce vychutnať priamo koncert tak Martin v júli príde na Slovensko a zahrá počas Letnej školy esperanta, čo bude zjavne jeho prvý koncert u nás. 
   
 Martin &amp; la talpoj - Pli ol nenio 
   
 






 

