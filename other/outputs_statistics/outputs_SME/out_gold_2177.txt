

   
 Nemala by to vedieť EU, keď platí za to, aby starostlivosť o „nechcené deti“ bola odinštitualizovaná? Nemal by vedieť celý svet, čo na Slovensku znamená „sanácia rodiny“?  
 Matynko, Gejza, Adamko, Nikola, Nikolka, Gitka, nekonečný zoznam mien detí, ktorým sa ubližuje. 
 Ak to nezaujíma (málo zaujíma) naše senzáciechtivé médiá, možno by to zaujímalo nejaké seriózne zahraničné. 
 Tento príbeh nazvime Johanka. 
   
 Johanka je rómske dievčatko. Mama ju opustila hneď po narodení – z pôrodnice ušla a viac sa o malú nezaujímala. Dostala sa do profesionálnej rodiny, kde je takmer rok. Zo strany príbuzných nebol doteraz o malú prejavený žiadny záujem. Profesionálna mama vedela, že v inom domove má aj malého brata. (Rozdeliť súrodencov evidentne nie je vôbec žiadny problém) Profesionálna mama sa rozhodla oboch adoptovať alebo vziať do pestúnstva. Oslovili príslušný úrad a na veľké prekvapenie zistili, že deti nie sú v zozname. (Prečo? Jožko v domove od narodenia už 4 roky, Johanka rok! Prečo doteraz nebol podaný ani návrh na osvojiteľnosť? Detský domov vraj podklady odoslal.) Dozvedeli sa, že vraj sanujú rodinu! Johankina mama je psychicky chorá a zo šiestich detí, ktoré porodila, nemá doma jediné. Doslova gól je to, že 4 Johankiných súrodencov zverili do starostlivosti babky, ktorá však má svoje dieťa -Johankinho strýka v detskom domove!  Matka nemá vlastné bývanie, o deti sa nezaujímala a ťažko by sa o ne vedela starať. A vlastne ani nechcela, jej druh i ona sa vyjadrili, že Johanku nechcú, lebo nedokážu sa o ňu postarať.  
   
 Rodina profimamy podala na súd návrh na súd o zverenie detí do pestúnstva. Začalo súdne pojednávanie. Matka sa nedostavila, napísala však, že nemá na cestu, preto sudkyňa odročila pojednávanie a dala matku vypočuť v mieste trvalého bydliska. Veľkým šokom pre profesionálnu mamu bolo, keď ju včera informovali v detskom domove, že matka  požiadala o zrušenie ústavnej starostlivosti. 
 Opäť jedna „akože sanácia!“  
   
 Na sťažnosti a listy úradu profesionálna mama nedostáva žiadnu odpoveď.  
   
 Musíme niečo spoločne urobiť, aby tie deti neboli bezdôvodne v detskom domove a aby žili v rodine, i keď náhradnej, ale milujúcej, pomáhajúcej, podporujúcej, aby mali šancu na lepší život. 
   

