
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Lobodáš
                                        &gt;
                Filmy
                     
                 StreetDance 3D 

        
            
                                    7.6.2010
            o
            23:06
                        (upravené
                7.6.2010
                o
                23:14)
                        |
            Karma článku:
                2.68
            |
            Prečítané 
            895-krát
                    
         
     
         
             

                 
                    Koncom mája uviedla spoločnosť Tatrafilm do našich kín ďalší film vo formáte 3D - StreetDance. Aj napriek nelichotivým kritikám som sa rozhodol pozrieť si ho, pretože filmy o tanci, počnúc legendárnym Flashdancom, Hriešnym tancom či Let´ s dance, mám veľmi rád.
                 

                 
  
   Opäť raz som sa presvedčil, že všetky nepriaznivé hodnotenia či komentáre „fundovaných" odborníkov nie vždy korešpondujú s mojim názorom. Priznám sa však, že tieto kritiky spolu s britskou produkciou tohto hudobného snímku ma trochu odrádzali. Svoje rozhodnutie som však neľutoval.   StreetDance 3D je debutom režisérskej dvojice Max Giwa - Dania Pasquini.  Hlavnú predstaviteľku Carly stvárnila mladá rodáčka z Leedsu Nichola Burley, hlavnej mužskej úlohy sa chopil Richard Winsor.  Tejto mladej dvojice sekundovali z pozície vedľajších postáv známejšie mená -  Rachel McDowall (Mamma Mia, Quantum of Solace), George Sampson (známy z reality show Britain´s got talent), Frank Harper (Hooligans), či Charlotte Rampling (Kosatka zabijak).   Príbeh zodpovedá klasickému postupu pri tanečných filmoch. Podceňovaná skupina streetdancových tanečníkov, ktorú zradil jej vodca, sa spojí s baletnými umelcami a spoločne pod názvom Breaking Point bojujú o titul UK Street Dance Champions proti favorizovaným obhajcom titulu.   Dejová línia však nebola to, čo ma v tomto filme zaujalo. Tento film bol predovšetkým o tanci a hudbe. 90% percent času sa tancovalo, breakovalo, či baletilo. A to bolo na tomto filme čarovné. Úžasné choreografie, skvelé tanečné výkony, spojenie dvoch úplne odlišných štýlov. Battle, ktorý predviedli na párty, bol asi najlepším tanečným vystúpením, aké som kedy vo filmovom prevedení videl.   Myslím si, že diváci, ktorí prišli do kina s tým, že idú na tanečný film, museli byť spokojní. Ten, kto hľadal nejaký hlbší zmysel, kvalitnejšiu réžiu, či strhujúci príbeh, sa sklamal, ale na to musel byť pripravený už vopred.   3D prevedenie bolo v poriadku a patrilo k tým kvalitnejším. Herecké výkony neboli excelentné, za to tie tanečné výrazne zatlačili do pozadia krivkajúcu dejovú líniu. Najväčším mínusom filmu bol miestami neznesiteľný dabing hlavnej hrdinky Carly.   Britský StreetDance v mojich očiach prekvapujúco prevalcoval i americkú hitovku Let´s Dance. Ostáva nám len sa tešiť, na septembrovú odpoveď...        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Nominačné okienko č. 1: Brankári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Lev zareval na odchod
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Najlepšie vývozne artikle extraligy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Tímy NHL podľa draftu: Anaheim Ducks
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Vlna, ktorú sme nezachytili
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Lobodáš
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Lobodáš
            
         
        lobodas.blog.sme.sk (rss)
         
                                     
     
        Športový fanatik, ktorého zaujíma všetko, čo sa týka futbalu a hokeja, ale miluje aj kvalitnú hudbu a filmy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2618
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nominačné okienko č. 1: Brankári
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




