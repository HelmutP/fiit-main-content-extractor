
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Cibulková
                                        &gt;
                -
                     
                 Moje dieťa 

        
            
                                    2.6.2010
            o
            9:53
                        (upravené
                3.6.2010
                o
                10:10)
                        |
            Karma článku:
                5.48
            |
            Prečítané 
            1100-krát
                    
         
     
         
             

                 
                    Keď som bola malá, bolo MDD veľkým sviatkom. Maľovali sme na chodníky farebnými kriedami, cez vyučovanie sme šli do kina a na obed sme mali v školskej družine dukátové buchtičky. Bola to hotová polahoda. Zakázané bolo dovolené a my sme sa ako deti na tento deň určite veľmi tešili. Nemyslím si, že to bolo tým, že bola iná doba. Príčina je jednoduchá – boli sme deti.
                 

                   
 Teraz je už zo mňa mama. Na MDD sa pozerám z dvoch strán, ale vždy nostalgicky. Vidím seba ako malú žubrienku s balónom v ruke, ktorý mi zrejme napokon uletel, ale v momente, keď som ho vlastnila, bol pre mňa symbolom šťastia. Súčasne vidím svoju dcéru Dominiku, ako sa hrá v hlúčiku detí na parkovisku pred domom, smeje sa a naháňa tak, až jej ofina nadskakuje. Cez nadskakujúcu ofinu sa jej obraz plynule mení a Domča pobehujúca na chodníku pred domom odrazu behá po kurte a snaží sa vychytať všetky loptičky. Keď sa jej darí zatne malú päsť, keď nie ostane stáť, ako by ju niekto zadržal, zakloní hlavu dozadu a ruky má spustené dolu. Nerezignuje, len si presne uvedomuje moment, kedy spravila chybu.    Chcela som však niečo k MDD... Ak si mám vybaviť spomienky na Dominiku ako dieťa, mala by som sa vrátiť na parkovisko pred dom. Mali sme vtedy také malé autíčko. Červenú .... Pre mňa a Dominiku akurát. Vozila som ju na ňom do škôlky a na zadnom sedadle mala taký malý vankúšik, aby si ešte cestou mohla ľažkať a privrieť oči. Veľmi rada spala. Hlavne ráno. Každý rodič to pozná, ako je ťažké budiť ráno svoje dieťa, keď ono chce spať. Ale Dominika bola poslušné dievčatko. Ak bola ešte ospalá, tak objala svoj vankúšik a schúlila sa na zadné sedadlo. Dominika, nespi, už sme tu. Vystúpi z auta a ťahá za sebou veľkú tašku na tenisové rakety. Moja spomienka opäť úplne plynule prešla spred škôlky na tenisový kurt (asi som v spomienke odbočila o ulicu skôr). To malé dievčatko s veľkou taškou podskočí a beží k tenisovej hale, potom sa otočí a spýta sa, či pre ňu prídem. Otázka skôr zo zvyku. Čo by som neprišla, ak nie ja, tak tatino. A už jej niet. Ani neviem, či som si vtedy uvedomovala, že moja dcéra je len taký malý chrobáčik, zraniteľný a krehký. Ale silný zároveň.   Detstvo a tenis, to bola u nás tá istá pesnička. Nedali sa od seba oddeliť Vlastne ani teraz to inak nevnímam, hoci Domča je už dospelá žena. Stále ju vidím ako to malé dievčatko, ktoré keď začalo chodiť na turnaje, sprevádzal na cestách otec a ktoré mi z ďaleka aj z blízka z úspešných turnajov volalo a po nezdare písalo esmesku: fňuk- fňuk. A ja som doma čakala, verila som jej, že na to má a bála som aj za ňu všetkých možných nástrah.   Je veľa takých rodím, kde rodičia ešte za tmy budia malé dievčatká alebo chlapcov, lebo telocvičňa, či štadión je pre malé deti k dispozícii hlavne zavčasu ráno. Možno tí rodičia niekedy aj váhajú, či pokračovať. Kdekto im povie, že je to len trápenie ich detí. Príde aj argument, že nie každý malý športovec to dotiahne ďaleko. No a v neposlednej miere môže zavážiť aj fakt, že to stojí čas a peniaze a čas a peniaze... Žiaľ, spoľahnúť sa na to, že naše športové kluby nájdu a vychovajú talenty, sa nedá. To potvrdia všetci rodičia futbaloví, hokejoví, plaveckí... Odkazujem všetkým, nevzdávajte to. Ak nebude z malého športovčeka majster sveta, nevadí. Hlavný vklad je v tom, že sa mu dostane do vienka disciplína, samostatnosť, férovosť. Prejde čas a z dieťaťa bude dospelý človek. Dobrý človek.   Dominika je už veľká, nečaká, či ju odveziem na tréning. Svoj život podriaďuje svojim rozhodnutiam. Bez ohľadu na to, že občas robí chyby, čo ťažko prehĺtam a málokedy zabudnem okomentovať, viem, že každý deň urobí niečo, aby naplnila svoj sen (a aj môj sen). Každý deň môžem s kľudom povedať, že som pyšná na svoju dcéru. Dominiku neľúbim len na MDD. Ľúbim ju celý jej život.   Som mama, ktorá má dospelú dcéru. Nespomínam si, či niekedy dostala na MDD balón, ktorý jej potom aj tak uletel. Dúfam len, že to čo som jej dávala, to dnes pevne drží v rukách. Nepusti to, Domča!  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Cibulková 
                                        
                                            ZAPLATÍM ZA TO, ŽE SOM MAMA
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Cibulková 
                                        
                                            Povinná volebná účasť - čo Vy na to?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Cibulková 
                                        
                                            Grécko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Cibulková 
                                        
                                            Súdruhovia prečo?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Cibulková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Cibulková
            
         
        katarinacibulkova.blog.sme.sk (rss)
         
                                     
     
        Pôsobím ako  poslankyňa Národnej rady Slovenskej republiky za SDKÚ-DS, som členkou Ústavnoprávneho výboru SR a Mandátového a imunitného výboru SR.
Od roku 1998 som poslankyňou Mestského zastupiteľstva mesta Piešťany a v novembri tohto roku som bola zvolená za poslankyňu do zastupiteľstva Trnavského samosprávneho kraja. 
Narodila som sa 14.januára 1962. Som vydatá, mám dcéru Dominiku. Vyštudovala som právnickú fakultu Univerzity Komenského v Bratislave.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1201
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




