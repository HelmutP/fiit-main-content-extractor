

 Jazzové The Illusions Trio z východné Slovenska sa predstavilo s veľmi kvalitnou porciou vlastnej tvorby. Líder formácie Andrej Karlík sólovou hrou na gitaru presvedčil, že podpora Martina Valihoru nie je náhodná . 
  
 S výzorom dieťaťa udieral presne na bicie Michal Fedor. 
  
 Basová gitara v rukách Jozefa Cingeľa nezostávala ani v jednej jazzovej ilúzií. 
  
 Pomalé tóny sa striedali s rýchlymi pasážami, ktoré boli okorenené výbornými sólovými partami. Krátke hudobné promo skupiny bolo veľmi príjemný predkrmom pred hlavným chodom. 
 Tradičné jazzové formácie vystriedalo hviezdne zoskupenie pod vedením Marcela Palondera. Tango Ultimo je hudobné teleso zložené z inštrumentálnych osobnosti slovenského hudobného neba. 
  
 Marcel Palonder presvedčil spevom aj prejavom 
  
 Ján Brtka najmladší člen zoskupenia nenechal na pochybách nikoho, akordeónu rozumie 
  
 Gitarová ikona Juraj Burian autorsky prispel skladbou Obsesion 
  
 Eugen Vizvári a klavír ideálne spojenie pre jazz 
  
 Štefan Bugala spolupracuje v rôznymi umelcami v Tango Ultimo ovláda perkusie 
  
 Martin Valihora znovu a znovu kvalitne a presvedčivo 
  
 Temperamentné rytmy tanga, klasické jazzové prvky, kvalitný spev Marcela  Palondera najviac charakterizujú večernú atmosféru v divadle Aréna.  Záverečný „Standing ovation" pre Tango Ultimo bol určite zaslúžený. 
 Photo@Juraj Kačo 

