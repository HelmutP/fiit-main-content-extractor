

 Začal sa rok 2010.  
 Priviezli malý, studený, zakrvavený batoh. Malý chlapec, ktorý  sa vracal z lyžovačky. Sedel v aute. Vzadu, nepripútaný. Šmyk, protismer, auto vletelo bokom do druhého jazdného pruhu. Náraz dodávky na zadné dvere. Dvaja malí bratia lietali ako handrové bábiky po kabíne auta.stranu. Neovládateľná sila mykala chlapcami zo strany na stranu. 
 Jeden tam ostal. Hneď. Natrhnutý mozgový kmeň, rozdrvené pľúca.  
 Druhého kriesili. Pre počasie sa nelietalo, tak ho priviezli záchranári k nám. 
 Malý, zakrvavený, studený ako ľad. Mŕtvy. Kriesili sme, stláčali hrudník a dýchali, pumpovali krv a ohrievali, kanylovali, prekrývali rany, drénovali, sledovali vytekajúcu krv, počúvali bublanie v pľúcach, hľadali ozvy srdca. Kmitali, riešili, hľadali možnosti, trápili sa. Preniknutí hrôzou, so stiahnutým hrdlom. 
 Len na malú chvíľu sa objavil zvuk, záblesk oziev srdca, aby sa stratili ako meteor v tme. Už bol inde. A tam aj ostal. 
 Najprv mame povedali, že ten prvý je mŕtvy. Šalela od bolesti. 
 Potom som čosi o druhom vravela, prehĺtala slová... Sestričky ju držali, aby si v tom hroznom kryštalickom svetle hrôzy neublížila. Aj oni s ňou plakali, hltali slzy, potláčali strašné zvuky. 
   
 Po polnoci muž s dierou v hlave. Strelil sa. Cestou do nemocnice žil, rozprával. Ale, ba!  
 O pár minút už chrčal. Intubovaný, kriesený a napriek všetkým resuscitačným pokusom zomrel. 
   
 Na druhý deň. Mladá žena v aute s deťmi. Ľad zo strechy kamióna vletel cez čelné sklo a vrazil jej do tváre. Devastačné poranenie nosa, úst, očí . Všetko zmiešané, dolámané, potrhané, krvavé, opuchnuté. Ako veľa bude treba, kým sa to vráti na miesto. Ak sa vráti. 
   
 Hádka manželov, vresk, krik. Deti ako diváci akčného filmu pozerajú na rituálny tanec otca. Tanec končí ranou v srdci. Resuscitovaná, prevezená, operovaná. Bezvýsledne.  Synček tomu porozumel, prestal rozprávať.  
   
 A v sobotu večer biely hipogrif oznamuje divákom z televíznej obrazovky, že sa začína ples. Najväčšia spoločenská udalosť. A bude taký krásny, ako ho vytvoria zúčastnení, taký krásny, aké budú spomienky naň. Poďte sa všetci pozerať ! 
 Neviem, aký nerv sa mi zauzlil, aký tón sa vo mne zachvel ? Sklonila som hlavu do dlaní.  
 A to je asi všetko.  

