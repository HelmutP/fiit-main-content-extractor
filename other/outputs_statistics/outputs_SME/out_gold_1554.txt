

   
 Pôjde o plynutie času. Našťastie žiadna hlboká filozofia, ako by sa odo mňa dala čakať- irónia. Len stručná úvaha o tom, ako rýchlo preletia letné mesiace. Október s novembrom tak naponáhlo nemajú. Neviem, či je to tým teplom, že keď sa ním predlžujú koľajnice, nepriamo úmerne sa (pocitovo) skracujú dni. Neviem. Skúmal to niekto? Také peniaze idú na kadejakú nezmyselné projekty a čas, ktorý sa okrem iného rovná peniaze (počuje ma krízový štáb našej ekonomiky?!), nás necháva chladnými? 
 No poďme spomínať. Nie vždy to tak bolo. Keď sa vrátim do základoškolských čias, musím povedať, že sa prázdniny neznesiteľne vliekli. A z väčšiny si pamätám len niekoľkotýždňové pobyty u babky s maľovanými krížovkami a prílohami z Káčera Donalda (zaujímalo by ma, kde mám ten skvelý kompas z júlového čísla 1997...), či strach samej doma, keď Kráľova hoľa mala odstávku (keď mi nehučal televízor všetky bytové ruchy zneli ako vylamovanie zámku na dverách a rozbíjanie skla na okne v spálni). Keď už televízor hučal, zaručene v ňom fičal nejaký letný seriál, matne pamätám len Konečne zvoní, ktoré som si minule nostalgicky pozrela a už mi neprišlo ani vtipné, ani Zack sexi a nedocenenú Lekáreň, z ktorej si pamätám jedinú scénu a to keď sa dvaja mladý pri bozkávaní zasekli zubnými strojčekmi do seba. Po jej vzhliadnutí sa vo mne vyvinula fóbia z bozkávania so strojčekom, ku ktorej sa podľa môjho zubára na jeseň s veľkou pravdepodobnosťou vrátim. Och... Jáj, jasné, ešte Saint Tropez, aby každý dostal aj svoju dávku slnka, mora a intríg, ako sa na správne leto patrí. 
 Takže to boli prázdniny, ktoré sa nenormálne vliekli a nemohla som sa dočkať školy. Niežeby som ich celé presedela pred telkou, ako sa môže zdať , veď si spomínam aj na stanovanie pred bytovkou a chodenie po brata do škôlky, kde na mňa vždy číhal nejaký mafián s nabitou zbraňou. Ach, tá detská fantázia. A potom sa to zvrhlo. Prázdniny boli čoraz kratšie. Posledné školské dni, hranie kariet miesto šmirgľovania lavíc, rozlúčka so spolužiakmi, ráno vypadnúť z domu, pobehovať vonku s kamarátmi, vrátiť sa večer, spečená od slnka so šúpajúcimi sa plecami, blúzniaca od úpalu, zase prvá školský deň, sloh na tému „Čo som prežil v lete“ na slovenčine, výkres na výtvarnej o tom istom. Väčšinou som nevedela o čom, asi preto, že to zbehlo tak rýchlo, že nič poriadne som prežiť nestihla. 
 Posledné letá by mi už bolo blbé neprežiť niečo poriadne. Už sme síce žiadne trápne slohy nevypisovali, ale aj tak. Posledné tri letá som bola mimo. Dokonca viac mimo ako som zvyčajne. V zahraničí. V Nemecku. Nielen preto, že sa mi tam páči a nemecké mestá majú pre mňa zvláštne príjemnú atmosféru, ale aj preto, že nemčina je asi jediná vec, ktorú vo mne škola zanechala a preto, že Nemecko na šťastie mojou pracovnou silou nikdy nepohrdlo. Aj v takýchto podmienkach sa však pocitová dĺžka júla a augusta vyvíja od viacerých faktorov: 
 
 
 od 	zloženia prázdninujúceho osadenstva 
 
 
 V tomto mám našťastie len dobré skúsenosti. Zobrať kamarátky alebo byť zobratá kamarátkami sa mi vyplatilo. V depkách vypočujú, v hlúpostiach sa pridajú. Nie je nuda a dva mesiace zbehnú ako voda. Lebo viem si predstaviť, že napríklad s mojím profesorom dejín divadla by nudné leto mohlo trvať aj celé dlhé veky. 
   
 
 
 od 	kvality a kvantity pracovnej náplne 
 
 
 Chce to action. Nezávidím pracantom v pásovej výrobe alebo zberať krčiacich sa pri drobných jahôdkach. Montovať jedno GPSko za druhým či umývať šalát pre mekáče čas nepoženie dopredu. Zato kuchárska šou pred očami zákazníka z desiatich hodín na nohách spraví chvíľku (no dobre, teraz preháňam, lebo desaťhodinová služba je vážne priveľa a po takej piatej hoďke už nenávidíte každého kto len otvorí ústa na vyslovenie slova „pasta“ a vrčíte na tých, čo nevidia rozdiel medzi tagliatellami a linguinami, ale globálne to fakt ide celkom šup šup). 
   
 
 
 od 	zloženia domáceho osadenstva 
 
 
 A nemyslím tým ocka, mamku a bryndzové pirohy, ktorí na vás doma čakajú. Od tých si treba oddýchnuť, na pirohy nazbierať slinky. Pod pojmom domáce osadenstvo mám na mysli milých, drahých, priateľov, fešákov, wie auch immer. Ak vás doma čaká milujúci chlap, dni z kalendára odkrajujete na nešťastie oboch zúčastnených pomalšie. Ak máte so sebou všetko čo potrebujete (čím som teda chcela upokojujúcejším spôsobom povedať, že ma doma nečaká nič a nikto...), dni plynú rad za radom. Až vás niekedy zarazí, že veď kam sa tak ženú, keď vás teda aj tak nik nečaká. 
   
 Toto leto mi plynie ozaj veľmi rýchlo. Pred chvíľou sme strastiplnou cestou do Berlína dorazili a čochvíľa je koniec júla. Potom slastná výplata, jej míňanie a hor sa domov. Na diár na stole mi sadá prach, v zásade netuším koľkého je (preto sa ospravedlňujem aj všetkým, na ktorých narodeniny či meniny sa mi v posledných dňoch podarilo zabudnúť). Väčšinou neviem ani len to, či už víkend bol alebo sa len chystá, z dňa si pamätám len otrasnú melódiu budíka (či už vyzváňa skoro ráno alebo neskoro poobede) a seba vstávajúcu z postele (zvláštne je, že seba zaspávajúcu vidím v oveľa rozostretejších farbách). Približne sa orientujem len podľa nálepiek s dátumom spotreby na mäsách a morských príšerách , ktoré hádžem do woku. Minule som sa riadne prekvapila, keď na hovädzom svietilo 21.7.2009. To už? 
   
 To už. Najvyšší čas začať si užívať leto. Kým nebude po ňom. 
   
   

