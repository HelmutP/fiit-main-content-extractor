
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Špurek
                                        &gt;
                galéria u Maxa
                     
                 Medzi konármi a kúsok ďalej 

        
            
                                    17.5.2010
            o
            14:00
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            232-krát
                    
         
     
         
             

                 
                    Obchádzal kaluž. Nemohol som ho vidieť. A práve preto sa do nej nepozrel. Nepozeral.   Brloh bol neďaleko. Kdesi pod svahom. Lesu ešte vládne lístie. Kráča. Je sám. Na štyroch nohách. Zima bola dlhá. Je vychudnutý. Les sa zdá temný a prázdny. Ktosi kdesi povedal, že svetlo hviezd je mrazivé. A slnko obchádza ľahkou nohou všetko, čo sa mu dnes nepozdá.
                 

                 Kráča. Nikto ho nevidí. Nevidí, lebo odrazu akoby sa každý stratil. Brloh je dnes málo zimný... Mrazivo je sťaby v ňom, nie von. Ale iba cez obed. Neďaleko sa objavil jarný potok, ktorý potom zas každý rok ustúpi. Pozná cesty hromov. Prídu a sú, ale v zime nie sú.       Nad svahom je les, všade je les. Je ešte skoro. Malo by sa loviť. Možno v ďalekej hore práve beží divoko srna, lebo vietor zaniesol pach vlka. Nepobeží, kríva. Zima bola ťažká a namáhavá.          Ale oči svieta ako svietili včera. Vravia a vidia. Zdá sa temer krotký, nepriblížim sa. Zdá sa akoby už nebol vlkom vo vlčom rozmere. Leží. Nevošiel ani do brholu. Leží. Iba občas pokrúti hlavou, stále vetrí. Možno o mne vie. Ale to je jedno, zrejme sa nepohne. Odrazu otvorí papuľu a cerí zuby. Je na nich krv. Aspoň sa zdá. Áno... nepochybne... Je to krv...       Kam mieria vlky, kam mierili pred rokmi...       Aký je krehký, ten obávaný. Aký je skromný. Akú má zvláštnu srsť. Skoro by som si ho privlastnil. Privlastnil krásu, dobro. Prchavosť nádhery, ktorá je taká bohatá pre moju malú dušu. Aké sú krásne stromy v jeho pozadí. Je ako sólista hôr, ako ten, čo sprevádza karavánu nocou, keď ho nikto nevidí.       Každý tvor má svoj príbytok. Lebo hlava sa skladá, zloží do príbytku. Známe steny, posvätné steny. Aj líšky. Pravdaže. Vošiel, zaliezol. Brloh, sväté steny. Bože, koľko námahy si vložil do vlka.       Chrám lesa, tajomno, nepoznanie. Kaluž ako labyrint jazera. Každý výmoľ, každý pohodený konár. Tíško dnes ležia. Kráča pomedzi ne. Pozná hraničné body, zájde ku nim, postojí, obnoví, vráti sa. Jeho oči horia, mysticky zlostné. Je ukrytý. Je skrytý v horách, a v tých očiach.       Keď sa vracal, zastavil sa opäť pri tej istej kaluži. Alebo inej. Pozeral ponad ňu. Napadlo ma, že tie jeho oči sú ako kaluže. Sivý kožuch ako krása pravosti.              Keď sa vrátim, znovu by som ho tu chcel nájsť. Ale či tu bude... Znovu by som rád kráčal po tých istých miestach pod klenbou stromov. Ukrytý pred jeho očami a on predsa o mne vie.   Vlk šiel svojou stranou. Chcel som opýtať, a to je všetko.... nič. To už nič. Nič sa nedozviem... Nič neokúsim, ani strach, ani nič. Ja neviem. Tak stojím a pozerám. Bol tu.       Oči sa obracajú k tebe. Oči všetkých sa upierajú k tebe. Vieš to. Oči nemusia byť viditeľné. Dôležité je, aby oči mali čo vidieť. Keď umiera čosi v nás, vždy bežím za Stvoriteľom, za Spasiteľom, lebo  v tej jednej osobe by sa dal nájsť život. Cítim vlčie oči na sebe. Nedajú pokoj. Myslel som, že ho pozorujem, ale on ide za mnou. Sleduje ma. On sleduje mňa. Nijaké zamknuté dvere. Ja už ani nepoznám pojem dvere. Okolo tretej v noci zreteľne počujem jeho skučanie. Počul som, ako sa hýbe. Zišiel som z postele a šiel. Nezastavil som.       Preto som šiel za ním. Preto za ním blúdim po lese. Blížim sa k nemu a  ustupujem. Vrčí na mňa a pripája sa navždy k môjmu bytiu. Inokedy uniká do hĺbok, kde ľudská noha nie je nevyhnutná. Že sedí na slnku a nevýrazne výrazne ma preniká. To všetko ma fascinuje. A že sa to stalo, že sa to stáva, že príbeh chodí hore a potom sa spustí zasa dolu, to nebudem hovoriť. To je len tu. Tu. V mojom srdci. Na to nikto nárok nemá. To si vezmem so sebou.                                      + 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Super vec! Poďte to skúsiť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Svetlo Ježiša Krista premôže tmu! Určite!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Moje telo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Dojatie je mŕtve
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Cyril a Metod, Róbert Bezák
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Špurek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Špurek
            
         
        spurek.blog.sme.sk (rss)
         
                                     
     
        Kresťan. Človek fascinovaný Ježišom Kristom. Tvorca a konzument umenia. Doktorand...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    384
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            literatúra
                        
                     
                                     
                        
                            kresťanské umenie
                        
                     
                                     
                        
                            galéria u Maxa
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľudmila Onuferová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            des Jano
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




