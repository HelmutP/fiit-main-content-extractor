
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľudmila Moleková
                                        &gt;
                Hrady, zámky, kostoly
                     
                 Zámok Smolenice 

        
            
                                    10.6.2010
            o
            18:00
                        (upravené
                13.12.2010
                o
                17:19)
                        |
            Karma článku:
                8.03
            |
            Prečítané 
            2997-krát
                    
         
     
         
             

                 
                    Smolenický zámok, postavený na mieste stredovekého hradu, sa vypína na úpätí Malých Karpát, nad obcou Smolenice, približne 30 km od Trnavy. Stavba svojím vzhľadom patrí k predstavám rozprávkových zámkov.
                 

                   Zámok neslúži ako múzeum, v ktorom by sa celoročne robili prehliadky pre turistov. V jeho interiéroch sa nenachádzajú historicky vzácne exponáty. Od roku 1953 patrí Slovenskej akadémii vied a ako jej Kongresové centrum poskytuje svoje priestory najmä na usporiadanie sympózií, kongresov, seminárov a školení. Voľne prístupné sú iba jeho niektoré časti a to počas letných prázdnin.     Zariadenie je hotelového typu, hosťom sú k dispozícii izby a apartmány, jedáleň, bar, vináreň, terasa a salóniky, ktoré sú zariadené nábytkom v klasickom štýle. Po dohode so správcom, vo voľných termínoch, je možné ubytovanie aj pre nečlenov SAV. Pre pracovné stretnutia slúžia vedeckým pracovníkom kongresové miestnosti, veľká zasadačka, okrúhla zasadačka, knižnica.         Kedysi pôvodný hrad slúžil ako strážny hrad na ochranu malokarpatských priesmykov, zámok má aj dnes opevnené nádvorie s baštami. Jeho najznámejšími a poslednými majiteľmi boli Pálffyovci, ktorí hrad získali už v roku 1777. Nemali však dostatok peňazí na jeho obnovu, nebol obývaný, chátral, počas napoleonských vojen a po jeho vyhorení sa premenil na zrúcaninu.     O znovuzrodenie sa postaral gróf Jozef Pálffy, ktorý na prelome 19. a 20. storočia  dal zámok postaviť podľa návrhu architekta Jozefa Huberta. Zámok má romantickú podobu francúzskych zámkov. Bol postavený podľa predlohy zámku  Kreuzenstein pri Viedni, ktorý vlastnila matka Jozefa Pálffyho, grófka Vilczeková. Na stavbe sa podieľali skúsení kamenárski majstri z Talianska, stavitelia z Nemecka, Rakúska, Maďarska, ale aj robotníci zo Smoleníc a okolia.      Na poschodie vedú mramorové schodiská.     Osvetlenie miestností dotvárajú rôzne krištáľové, kovové alebo sklenené lustre a svietniky, steny zdobia olejomaľby v zlatých rámoch a benátske zrkadlá. Podlahu na chodbách v ľavom krídle zámku tvorí benátska dlažba, v pravej časti je travertínová mozaika, v izbách sú parkety a dlážky sú pokryté plyšovými kobercami.        Za povšimnutie stojí krásne zdobená baroková truhlica s vázou.     Zámku dominuje mohutná viacboká veža s vyhliadkovou terasou s kamenným zábradlím, odkiaľ je pekný výhľad na okolie. Kým sme sa dostali hore, napočítali sme 92 betónových, točitých schodov.     Slabšia kondička sa dá zamaskovať záujmom o históriu zámku, urobením si prestávky pri vystavených exponátoch, pretože medzi druhým a tretím poschodím veže je galéria.     Dve veľké pivnice s vínom sa pod východnou časťou hradu nachádzali už za čias panovania grófov. Dnes príjemné posedenie pri vínku ponúka zámocká vinotéka.       Romantický vzhľad zámku a tiež nádherný anglický park v okolí, priťahuje mladé páry, ktoré ho využívajú na svoj „veľký deň“. Zámok je obľúbeným miestom pre svadby, ktorých súčasťou býva fotenie a filmovanie v interiéroch a exteriéroch zámku.          Na pravej strane nádvoria sa nachádza studňa, ktorá bola vykresaná v skale. V roku 1950 bola vyčistená a prehĺbená. Jej hĺbka je 60 metrov a je hlavným zdrojom vody pre zámok.        Počas nášho pobytu na Smolenickom zámku stále pršalo. Ale v slnečných dňoch zámok a jeho okolie vytvárajú skutočne romantickú atmosféru. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudmila Moleková 
                                        
                                            Žižkovská veža
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudmila Moleková 
                                        
                                            Prvé farmárske trhy vo Zvolene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudmila Moleková 
                                        
                                            Zberatelia pivných suvenírov v Martine
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudmila Moleková 
                                        
                                            Zima na Skalke pri Kremnici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudmila Moleková 
                                        
                                            Za lyžovačkou do Rakúska - Stubai
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľudmila Moleková
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľudmila Moleková
            
         
        molekova.blog.sme.sk (rss)
         
                                     
     
        "Váš čas je obmedzený, tak ním neplytvajte tak, že budete žiť život niekoho iného. Nenechajte sa vlákať do pasce dogmy - teda žiť na základe výsledkov myslenia iných ľudí. Nedovoľte, aby hluk cudzích názorov utopil váš vnútorný hlas. A čo je najdôležitejšie, majte odvahu ísť za vlastným srdcom a intuíciou."
                                  Steve Jobs
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    116
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2695
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Hrady, zámky, kostoly
                        
                     
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Austrália
                        
                     
                                     
                        
                            Amerika
                        
                     
                                     
                        
                            Ázia
                        
                     
                                     
                        
                            Európa
                        
                     
                                     
                        
                            Afrika
                        
                     
                                     
                        
                            Greyhound
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




