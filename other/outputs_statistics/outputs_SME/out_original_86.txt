
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján šoltés
                                        &gt;
                Nezaradené
                     
                 Prečo sa niekde jazdí vľavo a niekde vpravo? 

        
            
                                    4.8.2007
            o
            13:05
                        |
            Karma článku:
                12.23
            |
            Prečítané 
            13984-krát
                    
         
     
         
             

                 
                    Historický exkurz v otázke smere jazdenia vo svete, príčiny a dôsledky
                 

                  Ak chceme zistiť, prečo sa v niektorých krajinách jazdí vpravo, v iných zase vľavo, musíme načrieť do histórie - až do dôb, kedy po autách nebolo ani chýru, ani slychu. V pomerne hlbokej histórii  kotví príčina tohto javu, siaha to až do stredoveku. Aj najväčiemu rovnostárskemu idealistovi , pri pohľade na dejiny, musí byť jasné, že nikdy v dejinách neexistovala totálna, plošná rovnostárska spoločnosť (komunisti nech prepáčia); stále niekto niečo mal, druhí nie. Vždy bol niekto „kapitalistom" (vlastníkom majetku, výrobných prostriedkov), iný nie. „Triedne" spoločenské odlíšenie sociálnych vrstiev fungovalo vždy, funguje aj teraz a pravdepodobne aj bude fungovať navždy (ľavicoví teoretici, pardón!). Viditeľné to bolo napríklad aj v doprave. Ľudia majetní, vplyvní, mocní, šľachta... používali ľavú stranu cesty pre pohyb, chudoba sa radila vpravo - a peši! Jednoducho povedané: šľachta vľavo, chudoba vpravo. Prečo? Objasním!     V minulosti príslušníci majetných vrstiev chodili po cestách vľavo - bol to znak mocných, vplyvných spoločenských vrstiev, feudálov. Keďže väčšina populácie je pravákov, logicky sa feudáli radili na ceste vľavo  - v prípade, že dôjde k fyzickému stretu s oproti prichádzajúcim oponentom, môže využiť pravú ruku na tasenie meča (pošva umiestnená na ľavom boku) a je pripravený na boj. Rovnako tak robil prichádzajúci protivník, teda radili sa obaja na ľavú stranu cesty. Taktiež mal feudál so zbraňou v pravej ruke, sediaci na koni na ľavej strane cesty,  kontrolu nad tým, čo sa deje na celej ploche cesty: chudoba bola „pod bičom". Aj samotná „obsluha" koňa bola jednoduchšia pri držaní uzdy ľavou rukou a pravačka slúžila na boj, resp. kontakt s okolím.     Prelomovú historickú premenu "západnej občianskej" spoločnosti priniesla Francúzska revolúcia (1789), okrem iného aj v zmene strany jazdenia. Po páde Bastily kontrolu nad vývojom udalostí získala "chudoba", "ľud", zvíťazila ulica - a teda formálne gesto nastalo aj v zmene strany jazdenia po cestách. Aristokracia spoločensky prehrala, jej status bol zdiskreditovaný a bola nútená prejsť na opačný smer jazdy na ceste (myslím, že to ich trápilo najmenejJ). Oficiálny právny predpis na tento účel bol vydaný v Paríži v roku 1794, viac-menej paralelný právnemu predpisu z Dánska, kde jazdenie vpravo bolo povinné od roku 1793.     Neskôr, Napoleonske výboje priniesli nové právo (aj v otázke smere jazdenia) do Belgicka, Holandska, Luxemburska, Švajčiarska, Nemecka, Poľska, Ruska a mnohých časí Španielska a Talianska. Štáty, ktoré odolali Napoleonovi, ostali pri jazdení vľavo - Británia, Rakúsko-Uhorsko, Portugalsko. Takéto európske delenie medzi jazdením vľavo a vpravo pretrvávalo ďalších 100 rokov, až do čias po prvej svetovej vojne.     Taktiež, koncom 18. storočia pohoniči vo Francúzsku a USA začali prepravovať náklady vo veľkých záprahoch, na ktoré potrebovali niekoľko párov koní. Tieto záprahy nemali určené sedadlo pre pohoničov; tí však sadali na ľavú stranu, aby pravou rukou mohli mocne držať opraty, uzdu (praktický dôvod -  väčšina populácie je pravákov, bola potrebná sila).Z toho je logicky odvoditeľné, že bolo žiaduce, aby sa oproti prichádzajúce záprahy obchádzali zľava: pohoniči mohli pohľadom dole skontrolovať, aby sa kolesá záprahov obišli bez kolízie. Aj preto používali na cestách pravú stranu, čo znamenalo zmenu oproti minulosti, kedy sa jazdilo vľavo (šľachta).     Nastúpený trend medzi národmi počas nasledujúcich rokov sledoval k jazdeniu vpravo, ale Británia urobila všetko preto, aby čelila globálnej unifikácii (do úvahy berme prestíž hrdých Britov, ktorí by nepočúvali nikdy nikoho, Francúzov už vôbec nie;  vzájomná "láska" hrdých Francúzov a hrdých Britov je všeobecne známou vecou). So zvýšením objemu dopravy a masovou výstavbou  ciest  v 18. storočí (expanzia národných ekonomík - cesta k nacionalizmu v 19. storočí) sa do praxe dostali aj právne predpisy, regulujúce premávku v tej-ktorej krajine. Jazdenie vľavo bolo povinne nariadené v Británii v roku 1835. Krajiny britského imperia urobili tak tiež. Tu je vysvetlenie, prečo do dnešného dňa India, Austrália a bývalé krajiny britského imperia v Afrike jazdia vľavo.     Aj keď Japonsko nebolo nikdy časťou britského imperia, jazdí sa tam vľavo. Pôvod treba hľadať v období EDO (1603 - 1867), keď krajinu ovládali samuraji a sociálnou formou zriadenia bol šogunát. Praktické dôvody jazdenia vľavo sú paralelné ako dôvody európskej šľachty spred Francúzskej revolúcie (viď. vyššie). Reformy meidži zasiahli Japonsko. V roku 1872 sa v Japonsku začalo s výstavbou prvej železnice, za výraznej pomoci Britov. Postupne sa v Japonsku vytvorila široká sieť železníc a, samozrejme, začali jazdiť vľavo - podľa britského vzoru, keďže pri zrode motorizovanej premávky v Japonsku stáli Briti. V roku 1924 bolo jazdenie vľavo v Japonsku uzákonené.     V prvých rokoch anglickej kolonizácie severnej Ameriky kolonizátori jazdlili vľavo - zaužívaný zvyk z kontinentu. Po dosiahnutí nezávislosti od Anglicka sa pozvoľna zmenil aj smer jazdenia - vpravo. Bolo to formálne gesto nových Američanov, ktorí sa odtrhli od bremena Británie (Bostonské pitie čaju)  a začali si budovať vlastný štát: poznačilo to aj smer jazdy - vpravo! Prvá zákonná úprava smeru jazdenia vpravo sa udiala v Pensylvánii v roku 1792, podobné právne úpravy prebehli neskôr v New Yorku (1804) a v New Jersey (1813).     Rozpad Rakúsko-Uhorska nespôsobil zmenu v smere jazdenia v nástupníckych krajinách: Československo, Juhoslávia a Maďarsko pokračovali v jazdení vľavo. Kuriózna situácia bola v Rakúsku: polovica krajiny jazdila vľavo, druhá polovica vpravo. Presnou deliacou čiarou bolo územie dobyté Napoleonom v roku 1805.  Keď Nemecko anektovalo Rakúsko v roku 1938, Hitler nariadil povinný smer jazdenia vpravo.     Československo a Maďarsko prešli na jazdenie vpravo medzi poslednými štátmi Európy: v roku 1939, keď boli násilne ovplyvnené  Nemeckom.     Medzičasom jazda vpravo sa ujala masovo, prispela k tomu najmä Amerika. Americké autá jazdili vpravo, automobilová doprava sa stala masovou histériou, prebehla tam plošná motorizácia spoločnosti, nastal export automobilov do zahraničia. Jazdenie vpravo sa stalo spravidla uznávanou spoločenskou normou i právnym zákonom v mnohých krajinách.     V 60. rokoch Veľká Británia brala na vedomie zmeny vo svete v otázke smeru jazdenia, ale konzervatívne sily v krajine robili všetko preto, aby ostali pri jazdení vľavo. A teda Británia zavrhla túto zmenu. Dnes iba štyri európske krajiny jazdia vľavo: Veľká Británia, Írsko, Cyprus a Malta. -------------------------------------------------------------------------------------     Zoznam krajín (v anglickom jazyku), v ktorých sa jazdí vľavo; väčšina vodičov používa vozidlá s volantom vpravo;     1. Anguilla  2. Antigua and Barbuda  3. Australia  4. Bahamas  5. Bangladesh  6. Barbados  7. Bermuda  8. Bhutan  9. Botswana  10. Brunei  11. Cayman Islands  12. Christmas Island (Australia)   13. Cook Islands  14. Cyprus  15. Dominica  16. East Timor  17. Falkland Islands  18. Fiji  19. Grenada  20. Guernsey (Channel Islands)  21. Guyana  22. Hong Kong  23. India  24. Indonesia  25. Ireland  26. Isle of Man  27. Jamaica  28. Japan  29. Jersey (Channel Islands)  30. Kenya  31. Kiribati  32. Cocos (Keeling) Islands (Australia)   33. Lesotho  34. Macau  35. Malawi  36. Malaysia  37. Maldives  38. Malta  39. Mauritius  40. Montserrat   41. Mozambique  42. Namibia  43. Nauru  44. Nepal  45. New Zealand  46. Niue   47. Norfolk Island (Australia)   48. Pakistan  49. Papua New Guinea  50. Pitcairn Islands (Britain)   51. Saint Helena   52. Saint Kitts and Nevis  53. Saint Lucia  54. Saint Vincent and the Grenadines  55. Seychelles  56. Singapore  57. Solomon Islands  58. South Africa  59. Sri Lanka  60. Suriname  61. Swaziland  62. Tanzania  63. Thailand  64. Tokelau (New Zealand)   65. Tonga  66. Trinidad and Tobago  67. Turks and Caicos Islands  68. Tuvalu  69. Uganda  70. United Kingdom (England, Wales, Scotland and Northern Ireland)  71. Virgin Islands (British)  72. Virgin Islands (US)  73. Zambia  74. Zimbabwe     Zoznam krajín (v anglickom jazyku), v ktorých sa jazdí vpravo; väčšina vodičov používa vozidlá s volantom vľavo;     1. Afghanistan  2. Albania  3. Algeria  4. American Samoa  5. Andorra  6. Angola  7. Argentina  8. Armenia  9. Aruba  10. Austria  11. Azerbaijan  12. Bahrain  13. Belarus  14. Belgium  15. Belize  16. Benin  17. Bolivia  18. Bosnia and Herzegovina  19. Brazil  20. British Indian Ocean Territory (Diego García)  21. Bulgaria  22. Burkina Faso  23. Burundi  24. Cambodia  25. Cameroon  26. Canada  27. Cape Verde  28. Central African Republic  29. Chad  30. Chile  31. China, People's Republic of (Mainland China)  32. Colombia  33. Comoros  34. Congo  35. Congo (former Republic of Zaire)  36. Costa Rica  37. Croatia  38. Cuba  39. Czech Republic  40. Denmark  41. Djibouti  42. Dominican Republic  43. Ecuador  44. Egypt  45. El Salvador  46. Equatorial Guinea  47. Eritrea  48. Estonia  49. Ethiopia  50. Faroe Islands (Denmark)   51. Finland  52. France  53. French Guiana  54. French Polynesia  55. Gabon  56. Gambia, The  57. Gaza Strip  58. Georgia  59. Germany  60. Ghana  61. Gibraltar  62. Greece  63. Greenland  64. Guadeloupe (French West Indies)  65. Guam  66. Guatemala  67. Guinea  68. Guinea-Bissau  69. Haiti  70. Honduras  71. Hungary  72. Iceland  73. Iran  74. Iraq  75. Israel  76. Italy  77. Ivory Coast  78. Jordan  79. Kazakhstan  80. Korea, Democratic People's Republic of (North Korea)  81. Korea, Republic of (South Korea)  82. Kuwait  83. Kyrgyzstan  84. Laos  85. Latvia  86. Lebanon  87. Liberia  88. Libya  89. Liechtenstein  90. Lithuania  91. Luxembourg  92. Macedonia  93. Madagascar  94. Mali  95. Marshall Islands  96. Martinique (French West Indies)  97. Mauritania  98. Mayotte (France)   99. Mexico  100. Micronesia, Federated States of  101. Midway Islands (USA)   102. Moldova  103. Monaco  104. Mongolia  105. Morocco  106. Myanmar (formerly Burma)  107. Netherlands  108. Netherlands Antilles (Curaçao, St. Maarten, St. Eustatius, Saba)  109. New Caledonia  110. Nicaragua  111. Niger  112. Nigeria  113. Northern Mariana Islands  114. Norway  115. Oman  116. Palau  117. Panama  118. Paraguay  119. Peru  120. Philippines  121. Poland  122. Portugal  123. Puerto Rico  124. Qatar  125. Réunion  126. Romania  127. Russia  128. Rwanda  129. Saint Barthélemy (French West Indies)   130. Saint Martin (French West Indies)  131. Saint Pierre and Miquelon (France)   132. Samoa  133. San Marino  134. Sao Tome e Principe  135. Saudi Arabia  136. Senegal  137. Serbia and Montenegro  138. Sierra Leone  139. Slovakia  140. Slovenia   141. Somalia  142. Spain  143. Sudan  144. Svalbard (Norway)   145. Sweden  146. Switzerland  147. Syria  148. Taiwan  149. Tajikistan  150. Togo  151. Tunisia  152. Turkey  153. Turkmenistan  154. Ukraine  155. United Arab Emirates  156. United States  157. Uruguay  158. Uzbekistan  159. Vanuatu  160. Venezuela  161. Vietnam  162. Wake Island (USA)   163. Wallis and Futuna Islands (France)  164. West Bank  165. Western Sahara  166. Yemen        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (65)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján šoltés 
                                        
                                            Odťatý Galileov prostredník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján šoltés 
                                        
                                            T. Hobbes – idealista: Človek človeku človekom...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján šoltés 
                                        
                                            Čo si odnesieš z tohto sveta? Nič! Ešte aj Teba odnesú!!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján šoltés 
                                        
                                            Slovenský stolár pracuje v Čechách a nábytok kupuje z Poľska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján šoltés 
                                        
                                            Sloboda: robím, čo chcem, alebo nerobím, čo nemôžem?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján šoltés
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Mráziková 
                                        
                                            Ako tri Slovenky z UKF precestovali Ameriku
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján šoltés
            
         
        jansoltes.blog.sme.sk (rss)
         
                                     
     
        radový Homo sapiens sapiens, príslušník viac sa pýtajúci ako odpovedajúci
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    56
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1466
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mečiarovi pohrobkovia kántria lesy.
                     
                                                         
                       Bezradnosť v našom školstve
                     
                                                         
                       O udavačoch a slušných ľuďoch
                     
                                                         
                       Dopravný podnik v Bratislave sa zbláznil...
                     
                                                         
                       Som cyklofašista ale liečim sa
                     
                                                         
                       Ako som si z Antolskej odniesla traumu z pôrodníc
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Vzorec na život
                     
                                                         
                       Dovolenka
                     
                                                         
                       Od Čopu po Azovské more (UA 2013/1)
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




