

 
Sú piati. Majú vyvalené oči, na rukách štyri prsty, ich pleť je žltá a na animované postavičky trúsia neobvykle ironické hlášky. Simpsonovci radikálne nabúrali predstavy o tom, ako má vyzerať úspešný televízny seriál. Príbehy vydarenej rodinky priťahujú pred obrazovky nové generácie divákov už neuveriteľných pätnásť rokov. Keď ich americká televízna stanica 14. januára 1990 prvýkrát nasadila do vysielania, sotva niekto mohol tušiť, že sa zrodil novodobý popkultúrny fenomén, autora ktorého zaradí časopis Time medzi najvplyvnejšie osobnosti zábavného priemyslu 20. storočia.
 
 
"Nechápem, čo je také príťažlivé na animovanom seriáli, ktorý dávajú v sobotu ráno," poznamená ironicky mama Marge v jednom z prvých dielov Simpsonovcov. Dnes už je to všetkým jasné. Originálne vymyslené postavy trúsia vtipné narážky na svet popkultúry, hýria čiernym humorom a nemilosrdne ironizujú okolitý svet. To sú základy úspešného ťaženia Simpsonovcov.
 
 
Hlavou rodiny je dobrácky tučniak Homer. Pracuje ako bezpečnostný technik v jadrovej elektrárni a najradšej doma posedáva na gauči. Jeho žena Marge po vystriedaní niekoľkých zamestnaní zostala v domácnosti, stará sa o maličkú Maggie a jej dvoch súrodencov Barta a Lisu.
 
 
Uličník Bart obľubuje jazdu na skejte a ukazovanie svojho zadku, Lisa dáva prednosť škole a hre na saxofón. Obaja majú radi sledovanie televízie, presnejšie dobrodružstvá Itchyho a Scratchyho (drsnejšia verzia iného slávneho kresleného seriálu Tom &amp; Jerry).
 
 
Približne takto vyzeral nápad, s ktorým sa animátor Matt Groening pohrával počas desaťminútového čakania na televízneho producenta Jamesa Brooksa. Namiesto dohadovania o prepise starého novinového komiksu sa rovno za dverami vytasil s príbehmi neobvyklej rodiny a svojím nadšením strhol Brooksa, ktorý ich predložil televízii Fox. Postaral sa jej tak o programovú bombu a sebe o niekoľko miliónov.
 
 
Najskôr sa príbehy Simpsonovcov objavovali v krátkych skečoch pred šou Tracey Ulmanovej. Vo Foxe však už čoskoro pochopili ich potenciál a vznikol samostatný seriál, na ktorého nové diely sa začínalo tešiť čoraz viac a viac divákov.
 
 
Zrod masového fenoménu signalizoval už rok 1991, keď riaditelia niekoľkých amerických stredných škôl zakázali žiakom nosiť tričká s Bartom. Niet divu, pedagógovia ťažko motivujú študentov, ktorí majú na hrudi kresleného puberiaka so zježenými vlasmi a nápisom "Som podpriemerný a som na to hrdý".
 
 
V roku 1994 sa Simpsonovci stali prvým televíznym seriálom, ktorý sa v USA vysielal simultánne v španielčine. O tri roky neskôr prekonali prvenstvo Flinstonovcov ako najdlhšie vysielaný animovaný seriál v televíznom prime time. To už na obrazovky nedočkavo zízali aj tínedžeri a nenápadne aj mnohí z ich rodičov.
 
 
Vždy sa s napätím čaká, čo si tvorcovia vymyslia v nových častiach. Obľúbeným terčom sú populárne osobnosti a spoločenské udalosti. V mestečku Springfield, kde Simpsonovci žijú, sa za 15 rokov objavili mnohé celebrity od Eltona Johna cez Richarda Gereho až po Tonyho Blaira. Mnohí z nich sami svoje kreslené verzie nahovorili. O popularite svedčí aj to, že hoci Michael Jackson sa v seriáli neobjaví, aspoň prepožičal svoj hlas postave, ktorá si myslela, že je Michaelom Jacksonom.
 
 
Kým hrdinovia iných seriálov väčšinou postupne starnú, oni sa nemenia. Bart a Lisa chodia stále na základnú školu, Maggie žmolí v ústach cumeľ, Homer platonický vzťah k plneniu pracovných povinností a vlasy Maggie sú bez známky šedín.
 
 
K prototypu ideálnej americkej rodiny majú ďaleko - keď napríklad v hádke začne mať navrch syn, otec ho obvykle trošku priškrtí. Napriek výchovným a iným rezervám sa však všetci členovia rodiny majú radi.
 
 
The Simpsons sa stali renomovanou značkou a trh je zaplavený množstvom predmetov, na ktorých sú vyobrazení. Neďaleko Las Vegas stojí verná replika ich domu. Postavila ju v roku 1997 firma Pepsi ako hlavnú cenu v jednej svojej súťaži. Šesťdesiattriročná výherkyňa sa rozhodla spraviť z neho turistickú atrakciu.
 
 
Matt Groening vlani oslávil päťdesiatku a jeho rodičia a súrodenci, ktorých zaopatril do konca života, mu už iste dávno odpustili, že ich skutočné krstné mená kedysi použil pre Simpsonovcov.
 
 
  
 

