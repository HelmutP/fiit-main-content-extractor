

 Kategorizácia predpisov: 
 
 
 Predpisy ktoré definujú cestnú sieť. (Cestný zákon)
 
 Predpisy ktoré definujú podmienky pre vozidlá určené na cesty(1). (Zákon o podmienkach prevádzky vozidiel)
 
 Predpisy ktoré definujú ako sa s vozidlami(2) jazdi po cestách(1). (Zákon o cestnej premávke)
 
 Ostatne predpisy ktoré priamo alebo nepriamo súvisia s predchádzajúcimi predpismi 
 
 

 
 Zrušené predpisy: 
 
 
 Zákon 315/1996 Z. z. z 20. septembra 1996 o premávke na pozemných komunikáciách - (Zákon o premávke na pozemných komunikáciách), 
 Vyhláška MV SR č. 225/2004 Z. z., ktorou sa vykonávajú niektoré ustanovenia zákona 315/1996 Z. z. o premávke na pozemných komunikáciách 
 Vyhláška Ministerstva zdravotníctva Slovenskej republiky č. 164/1997 Z. z. o zdravotnej spôsobilosti na vedenie motorového vozidla 
 


 Nové predpisy: 
 
 
 Zákon NRSR 8/2009 Z. z. z 3. decembra 2008 o cestnej premávke a o zmene a doplnení niektorých zákonov (Zákon o cestnej premávke) 
 
 Vyhláška MV SR 9/2009 Z. z. z 20. decembra 2008, ktorou sa vykonáva zákon o cestnej premávke a o zmene a doplnení niektorých zákonov 
 


 Zmenené, novelizované prepisy (číslo zákona zostáva): 
 
 
 Zákon č. 135/1961 Zb. o pozemných komunikáciách (Cestný zákon)
 
 Zákon č. 372/1990 Zb. o priestupkoch  
 Zákon č. 564/1991 Zb. o obecnej polícii 
 Zákon č. 145/1995 Z. z. o správnych poplatkoch 
 Zákon č. 725/2004 Z. z. o podmienkach prevádzky vozidiel v premávke na pozemných komunikáciách (Zákon o podmienkach prevádzky vozidiel)
 
 


 Vymenovať čo len zoznam všetkého čo sa mení, znamená skopírovať sem celý 
zákon a celú vykonávaciu vyhlášku, nakoľko nejde o novelizáciu, ale o nové paragrafové a grafické znenie. Preto vyberám len najdôležitejšie a  najzaujímavejšie veci hlavne pre bežných vodičov. V skratke uvediem popis o čo ide, vrátane odkazu na paragrafové znenie. 

   

 
 
 Všeobecné predpisy: 

 Prepisy platia už aj na účelových komunikáciách.(8/2009, §2,1 voči 315/1996, §2, b) 

 Dať prednosť znamená povinnosť konať tak aby ten kto prednosť má nemusel prispôsobovať ani rýchlosť jazdy. (8/2209, §2, b voči 315/1996, §2, d),

podrobnosti 

 K zákazu rušičky radarov pribudol aj zákaz detektora radarov. Za detektor radaru nie je považované autorádio (dopravné spravodajstvo o radaroch) a ani poznámky v papierovej alebo elektronickej forme.(8/2209, §3,3) 


 Vodič je povinný dať prednosť chodcom, ktorí vstúpili na vozovku a prechádza 
cez priechod pre chodcov. Čiže aspoň jednu nohu má chodec dole z chodníka na 
priechode pre chodcov. (8/2009 §4, 1, f voči 315/1996, §4, 1 , f) 
 Vodič je povinný zabezpečiť vozidlo a prepravované veci proti odcudzeniu
prostriedkami montovanými do vozidla jeho výrobcom, ak sa vzdiali od vozidla 
mimo jeho dohľadu. Čiže zatvoriť okná a vozidlo uzamknúť všetkými prostriedkami, 
ktoré sú vo vozidle namontované. (8/2009 §4, 1, h) 
 Vodič nesmie počas vedenia vozidla používať telefónny prístroj okrem 
telefonovania s použitím systému „voľné ruky“ alebo vykonávať inú obdobnú 
činnosť, ktorá nesúvisí s vedením vozidla. Čiže nie je absolútny zákaz robiť inú 
činnosť ako napríklad nastavenie teploty kúrenia čo súvisí s tepelnou pohodou 
pre posádku a nie vedením vozidla. Zakázané je obsluhovať rukami telefónny 
prístroj alebo niečo iné, obdobné, napríklad vysielačku. Čiže treba mať všetko 
zabudované v prístrojovej doske, vo volante a podobne  alebo ovládané 
hlasom, tak aby vodičovi ostali voľné ruky. (8/2009 §4, 2, l) 

 Vodič nesmie používať hanlivé gestá voči ostatným účastníkom cestnej 
premávky, vyhadzovať z vozidla predmety a obťažovať ostatných účastníkov cestnej 
premávky ani iné osoby najmä nadmerným hlukom, prachom, znečisťovaním ovzdušia, 
rozstrekovaním kaluží, blata alebo zbytočným ponechaním motora stojaceho 
vozidla v chode (8/2009 §4,2, m) 

 Povinne reflexne vesty  v obci aj mimo obce ak sa vodič alebo 
člen posádky zdržiava na vozovke mimo vozidla počas núdzového státia. Bezpečnostný odev nesmie byť zameniteľný s ozbrojenými silami. Čiže ak nemáte dostatok reflexných viest pre celu posádku, tak musí zostať vo vozidle alebo isť až za hranicu cesty (8/2009 §5,3) 

 Vodiči vozidiel nad 3,5 tony sú povinný pustiť pred seba kolónu vozidiel, 
ktorú vytvorili pomalšou jazdou do svahu. Ak si to situácia vyžaduje, sú povinný 
odstaviť vozidlo na najbližšom parkovisku, a to až do prejazdu týchto vozidiel. 
Pritom termín "pri jazde do svahu" nie je presne definovaný a vzhľadom na 
hornaté Slovensko je to akékoľvek stúpanie, kde výkon motora vzhľadom na 
hmotnosť nákladu a stav vozidla spôsobuje stav, že za takýmto vozidlo sa vytvorí 
kolóna vozidiel. (8/2009 §5,5) 

 Na ceste s troma jazdnými pruhmi vyznačenými na vozovke v jednom smere jazdy je vodič prechádzajúci z ľavého jazdného pruhu do stredného jazdného pruhu povinný dať prednosť v jazde vodičovi prechádzajúcemu do stredného jazdného pruhu z pravého jazdného pruhu. (8/2009, §10, 7) 

 Vodiči vozidiel, pre ktorých je vyhradený jazdný pruh určený, sú povinní 
vyhradený jazdný pruh použiť prednostne.  (8/2009, §12,2) 

 Vodič smie v obci jazdiť rýchlosťou najviac 50 km.h-1, a ak ide po diaľnici v obci alebo po rýchlostnej ceste v obci, najviac 90 km.h-1. Mimo obce najviac 90 km.h-1 a mimo obce na diaľnici alebo po rýchlostnej ceste 130 km.h-1, ak nie je dopravnou značkou dovolená vyššia rýchlosť. Pritom nesme ísť rýchlejšie než je konštrukčná rýchlosť alebo maximálna dovolená rýchlosť pre dané vozidlo,(8/2009, §16) 

 Vodič motorového vozidla je povinný dodržiavať za vozidlom idúcim pred ním 
takú vzdialenosť, aby sa predchádzajúce vozidlo mohlo pred neho bezpečne zaradiť. 
Doteraz bola tato povinnosť len pre vozidlá nad 3,5 tony  (8/2009,§17, 
2 voči §16, 2) 

 Vodič vchádzajúci do kruhového objazdu označeného príslušnou dopravnou značkou je povinný dodržať smer na kruhovom objazde vyznačený šípkami. Vodič vchádzajúci do kruhového objazdu má prednosť v jazde, ak dopravnou značkou nie je ustanovené inak. Na Slovensku by sa nemal vyskytovať kruhový objazd bez úpravy prednosti jazdy dopravnými značkami (8/2009,§20,5) 

 Znamenie o zmene smeru jazdy vodič nedáva pri vjazde do kruhového objazdu, ale dáva ho vždy pred výjazdom z kruhového  objazdu. Pri jazde po kruhovom objazde vodič dáva znamenie prípade preraďovania sa medzi jazdnými pruhmi (8/2009, §30,5) 

 Vodič nesmie na moste zastaviť, stáť, otáčať sa a cúvať. Most je označený napríklad dopravnou značkou IS 35. (8/2009, §21, g,  8/2009, §25, m) 
 Vo vzdialenosti 50m (prv bolo 30) pred železničným priecestím 
sa môže ísť najviac 30 km.h-1 (8/2009, §27 3 voči 315/1996 §25, 3) 

 Motorové vozidlo musí mať rozsvietené stretávacie svetlá cely deň. Nemotorové len za zníženej viditeľnosti. Za nezníženej viditeľnosti môžu byť na vozidle namiesto stretávacích svetlometov rozsvietené denné prevádzkové svietidlá,
ak je nimi vozidlo vybavené.(8/2009, §32,1) 

 Na diaľnici mimo obce smie vodič motorového vozidla jazdiť rýchlosťou najmenej 80 km.h-1. a v obci najmenej 65 km.h-1 (8/2009, §35,10) 

 Vodič motorového vozidla ktorého najväčšia prípustná celková hmotnosť presahuje
7 500 kg, alebo vodič jazdnej súpravy, ktorej najväčšia prípustná hmotnosť presahuje 7 500 kg, nesmie pri jazde po diaľnici predchádzať iné motorové vozidlo. (8/2009, §35, 3) 

 Ak sa je na vozovke sneh,ľad alebo námraza, vodič motorového vozidla kategórie
M1 a N1 môže použiť len vozidlo, ktoré na všetkých nápravách vybavené zimnými pneumatikami s označením „M+S“, „M.S.“ alebo „M &amp; S“. Vozidlá kategórie M2, M3,
N2 a N3 musia byť vybavené takými pneumatikami aspoň na jednej z hnacích náprav +/- výnimky. Znamená to, že ak máte len letné  pneumatiky tak vozidlo zazimujte alebo používajte len keď na cestách nie je sneh, ľad a podobne (8/2009, §38,1) 

 Počet prepravovaných osôb nesmie byť vyšší, ako je počet miest uvedených v osvedčení o evidencii alebo v technickom osvedčení vozidla a platí to aj pre deti. Doteraz to platilo len pre osoby staršie 12 rokov. (8/2009, §46,1 voči 315/1996, §43,1) 

 Pri zastavení alebo státí na chodníku musí zostať aspoň 1,5m zo šírky chodníka, pokiaľ dopravnou značnou nie je stanovene inak. 
(8/2009, §52, 2) 


 Osoba, ktorá sa pohybuje po chodníku na športovom vybavení, je povinná 
sledovať situáciu a nesmie ohrozovať alebo obmedzovať iných účastníkov 
cestnej premávky. Ak ma vyhradenú cestičku je povinná použiť ju. (8/2009, 
§52, 6) 

 Chodec pred vstupom na vozovku sa  musí presvedčiť, či tak môže 
urobiť bez nebezpečenstva, a len čo vstúpi na vozovku, nesmie sa tam 
bezdôvodne zdržiavať ani zastavovať. To platí na priechode pre chodcov i mimo 
neho. (8/2009, §53, 4) 

 Chodec nesmie vstupovať na vozovku, a to ani pri použití priechodu pre chodcov, ak vzhľadom na rýchlosť prichádzajúcich vozidiel nemôže cez vozovku bezpečne prejsť. Nie je to nič iné ako akceptácia fyzikálneho zákona, ktorý má fyzickú prednosť pred právnym zákonom. (8/2009, §53, 2) 

 Cyklista do 15 rokov prilbu vždy, starší aspoň mimo obce.  (8/2009,  
§54, 9) 

 V obytnej zóne, pešej zóne a školskej zóne vodič smie jazdiť 
rýchlosťou najviac 20 km.h-1 a platí to aj pre cyklistov (8/2009, §59, 3) 

 Oprávnenie na zastavovanie vozidiel na veľa osôb ale nemýliť si to s cestnou 
kontrolou,(8/2009, §63) 

 Dopravná nehoda je len vtedy ak: sa usmrtí alebo zraní osoba, poškodí sa 
cesta alebo všeobecne prospešné zariadenie, uniknú nebezpečné veci, na niektorom zo zúčastnených vozidiel vrátane prepravovaných vecí alebo na inom majetku vznikne
hmotná škoda zrejme prevyšujúca jedenapolnásobok väčšej škody podľa Trestného 
zákona. Inak je to škodová udalosť (8/2009, §63,1) 

  Ak sa účastníci škodovej udalosti nedohodnú na jej zavinení alebo je niektorý 
vodič pod vplyvom alkoholu tak sa škodová udalosť stáva nehodou. (8/2009, §65, 2) 

 V evidencii dopravných nehôd pribudlo množstvo povinných údajov. (8/2009, §67) 

 Policajt môže ale nemusí zadržať vodičský preukaz ak vodič motorového 
vozidla, je ochotný ju zaplatiť pokutu, ale nemôže tak urobiť na mieste.(8/2009, § 71,1) 

 Do skupiny motorových vozidiel B+E patria jazdné súpravy zložené z motorového vozidla skupiny B a prípojného vozidla, ak nejde o jazdnú súpravu podľa skupiny B, kde najväčšia prípustná hmotnosť jazdnej súpravy nepresahuje 3 500 kg a najväčšia prípustná celková hmotnosť prípojného vozidla nepresahuje prevádzkovú hmotnosť ťažného vozidla. (8/2009, §75, 7) 

 Vodičské oprávnenie sa udelí žiadateľovi, ktorý okrem splnenia štandardných 
podmienok musí mať na území Slovenskej republiky pobyt v trvaní najmenej 185 
dní. (8/2009, §77, 1, b) 

 Bežní vodiči sú povinní podrobiť sa pravidelnej lekárskej prehliadke od veku 
65 rokov každé dva roky. (8/2009, §87, 4) 

 Ak možno dôvodne predpokladať, že nastala zmena zdravotnej spôsobilosti alebo 
psychickej spôsobilosti držiteľa vodičského oprávnenia, orgán Policajného zboru 
rozhodne o preskúmaní jeho zdravotnej spôsobilosti alebo psychickej spôsobilosti 
a určí lehotu na jej preskúmanie. (8/2009, §91, 3) 

 Fyzickej osobe sa poskytne informácia o údajoch, ktoré sa o nej uchovávajú v 
evidencii vodičov. (8/2009, §109,2 

 Porušenie povinností sa považuje za porušenie všeobecne záväzných 
právnych predpisov (8/2009, §137,1) 

 Porušenie vymenovaných povinnosti je porušením pravidiel cestnej premávky 
závažným spôsobom (8/2009, §137,2) 

 Ak sa na vozidle zistilo, že sa prekročila celková prípustná hmotnosť alebo 
šírka nad 3,0 m, alebo výška nad 4,5 m, dopravca nesmie pokračovať v jazde bez povolenia na zvláštne užívanie (135/1961, §8, 9) 

 Je nové členenie zvislých dopravných značiek na: výstražné značky, značky 
upravujúce prednosť a dodatkové tabuľky s tvarom križovatky, zákazové 
značky, príkazové značky, informatívne prevádzkové značky, informatívne 
smerové značky, informatívne iné značky, dodatkové tabuľky, (9/2006 §5, 1 ) 

 Zákaz, obmedzenie alebo príkaz vyplývajúci z príslušnej zvislej dopravnej značky sa končí na vzdialenejšej hranici najbližšej križovatky, ak nie je skôr skončený inak, čiže nie na vstupe do križovatky (9/2006 §5, 13 voči 2004/225, §5,12) 

 Križovatka nekončí zvislé dopravné značky označujúce obec, miestnu časť obce, obytnú zónu, pešiu zónu, školskú zónu, zónu s dopravným obmedzením, zónu s plateným alebo regulovaným státím, diaľnicu a rýchlostnú cestu, ktorých platnosť sa končí dopravnou značkou označujúcou ich skončenie. Zvislé dopravné značky č. P 1 až P 15,  platia pre najbližšiu križovatku. Zvislé dopravné značky č. IP 12 až IP 17b, (Parkovanie) končí 5 m pred hranicou najbližšej križovatky, ak nie 
je skôr skončená inak pričom vjazd a výjazd z miesta mimo cesty len prerušuje ich 
platnosť. Doteraz museli byť ukončene dodatkovou tabuľkou. (9/2006 §5, 13) 

 Použité zvislé dopravné značky, dopravné zariadenia a vodorovné dopravné značky v cestnej premávke musia byť včas viditeľné z dostatočnej vzdialenosti a počas celej doby použitia musia poskytovať úplný a jednoznačný výklad. (9/2006 §8, 8) 

 Zvislé dopravné značky možno upevniť na jednom stĺpiku alebo na inej nosnej konštrukcii v maximálnom počte najviac dve dopravné značky s príslušnými dodatkovými tabuľkami. (9/2006 §8, 10) 

 Na jednom stĺpiku alebo na inej nosnej konštrukcii možno umiestniť iba zvislé dopravné značky v rovnakej veľkosti a nemožno kombinovať zvislé dopravné značky rôzneho vyhotovenia, ako je napríklad rôzna úroveň retroreflexnej úpravy alebo retroreflexné a neretroreflexné alebo presvetlené zvislé dopravné značky. (9/2006 §8, 11) 

 Zákazy a príkazy platné v križovatke sa umiestňujú pod značku upravujúcej 
prednosť v jazde. (9/2006 §8, 18) 

 Doplnkový signál so zeleným svetlom v tvare zelenej šípky s plným červeným svetlom "Stoj!" môže byť aj v inom smere ako vpravo. pritom vodič je povinný dať prednosť v jazde vozidlám idúcim vo voľnom smere atd. (9/2006 §9, 3, g) 

 Zelene svetlo môže pred skončením platnosti pravidelne 
zhasínať a rozsvecovať sa. (9/2006 §9, 4 ) 

 Pokyn na zastavenie vozidla možno dávať z vozidla idúceho pred zastavovaným 
vozidlom, ako aj z vozidla idúceho za zastavovaným vozidlom. (9/2006, §13 , 2) 

 
 

 Dopravné značky: 
Výber nových alebo zmenených dopravných značiek. 

  
Obr.1. Výstražné značky. Návestné tabule sú symetrické. 

  
Obr. 2. Zákazové značky 

  
Obr. 3 Príkazové značky 

  
Obr. 4. Informatívne prevádzkové značky 

  
Obr. 5. Zóny. Namiesto zjednodušenia, sa počet zón rozšíril. 

  
Obr. 6 Informatívne smerové značky. Návesť pred križovatkou označuje výjazd a 
nie meno križovatky. Diaľnice sa označujú písmenom D a rýchlostné cesty písmenom 
R 

  
Obr. 7. Začiatok a koniec obce alebo mestskej časti. 

  
Obr. 8. informatívne iné značky. Už by sa nemalo stať, že na diaľnici budú 3 
rovnaké smery: Lamač, Lamač a Lamač. Kde jeden označoval cestu po diaľnici, 
druhý odbočku mimo diaľnice a tretí odpočívadlo s rovnakým názvom. 

  
Obr. 9. Svetelné signály 

  
Obr. 10. Vodorovné značenie. Šípky V9a menia dizajn a označujú smer v križovatke a nie možnosť preradovania. 

  
Obr. 11. Vodiace pasy pre nevidiacich a varovná tabuľa 

  
Obr. 12.Smerovacie dosky menia dizajn. 

 
 
 Pokuty: 
Pokuty sú veľmi vysoké. Napríklad za nie veľké prekročenie rýchlosti, ktoré sa riešilo a môže nadalej riešiť blokovou pokutou, tam kde doteraz bola pokuta od 0 do 2 000 Sk sa mení od 150 euro do 650 Euro (4 500 Sk az 19 500 Sk). 


	
Priestupok
Dolná hranica
Horná hranica
Maximum v hotovosti




Právnickej osobe alebo fyzickej osobe – podnikateľovi, ktorá 
a) prikáže alebo dovolí, aby sa na jazdu použilo vozidlo, ktoré vrátane nákladu nespĺňa podmienky na cestnú premávku ustanovené týmto zákonom alebo osobitným predpisom, bez vedomia vodiča vozidla alebo jeho prevádzkovateľa pri nakladaní tovaru prekročí najväčšiu prípustnú celkovú hmotnosť vozidla, najväčšiu 
prípustnú hmotnosť jazdnej súpravy, najväčšiu prípustnú celkovú hmotnosť 
prípojného vozidla alebo najväčšiu prípustnú hmotnosť pripadajúcu na nápravy vozidla, 
c) prikáže alebo dovolí, aby sa na jazdu použilo vozidlo na takej ceste, na ktorej je pre také vozidlo jazda obmedzená alebo zakázaná, 
d) neodstráni prekážku cestnej premávky, 
e) zverí vedenie vozidla osobe, ktorá nespĺňa ustanovené podmienky na jeho vedenie, nemá pri sebe doklady ustanovené na vedenie vozidla, je pod vplyvom alkoholu alebo inej návykovej látky alebo ktorej schopnosť viesť vozidlo je inak znížená, 
f) nezabezpečí, aby sa na jazdu pribral potrebný počet spôsobilých a náležite poučených osôb, ak je mu vopred známe, že to bude vyžadovať bezpečnosť cestnej premávky, 
g) prikáže alebo dovolí neoprávnené vybavenie vozidla zariadením umožňujúcim používanie typického zvukového znamenia alebo zvláštneho výstražného svetla ustanoveného pre vozidlá s právom prednostnej jazdy, alebo prikáže alebo dovolí, aby sa také typické zvukové znamenie, zvláštne výstražné svetlo alebo zvláštne výstražné svetlo oranžovej farby neoprávnene použilo, 
h) prikáže alebo dovolí, aby sa na jazdu použilo vozidlo, ktoré svojím farebným vyhotovením a označením možno zameniť za vozidlo podľa § 6 ods. 
4,i) v súvislosti s konaním o priestupku proti bezpečnosti a plynulosti cestnej premávky orgánu oprávnenému objasňovať alebo prejednať 
priestupok neoznámi osobné údaje osoby, ktorej zveril vedenie vozidla, v rozsahu meno, priezvisko a adresa, 
j) poruší ustanovené alebo určené podmienky o vedení, uschovávaní alebo o predkladaní výkazov o tlačivách dokladov a tabuliek s evidenčným číslom vydaných orgánom Policajného zboru alebo o manipulácii s nimi, 
k) nesplní povinnosť podľa § 43, 
l) poruší alebo si nesplní povinnosť podľa § 60, 
m) nesplní povinnosť podľa § 90, 
n) nesplní povinnosť podľa § 143 ods. 16.
0 euro (0 Sk)
3 500 euro (105 000 Sk)
nerieši sa




Právnická osoba alebo fyzická osoba – podnikateľ sa dopustí porušenia niektorej z povinností podľa odseku 1 opätovne do jedného roka od nadobudnutia právoplatnosti rozhodnutia o uložení pokuty.
0 euro (0 Sk)
7 000 euro (210 000 Sk)
nerieši sa




Právnickej osobe alebo fyzickej osobe – podnikateľovi, ak poruší podmienky o vedení, uschovávaní alebo o predkladaní výkazov o výrobe tabuliek s evidenčným číslom alebo o manipulácii s nimi, na ktorých výrobu alebo manipuláciu je potrebné povolenie ministerstva 	vnútra, alebo osobe, ktorá takéto tabuľky s evidenčným číslom vydávané podľa tohto zákona vyrába alebo s nimi bez povolenia manipuluje.
0 euro (0 Sk)
17 000 euro (510 000 Sk)
nerieši sa





 Užíva diaľnicu, cestu alebo miestnu komunikáciu iným spôsobom než zvyčajným alebo na iné účely, než na ktoré sú určené. Za priestupok ... možno 
uložiť vodičovi pokutu ..... ktorým sa ustanovuje výška úhrady za 
užívanie vymedzených úsekov diaľnic, ciest pre motorové vozidlá a ciest I. triedy. 

0 euro
50 násobok mesačnej známky
10 násobok mesačnej známky





a, ako vodič vozidla sa odmietne podrobiť vyšetreniu na zistenie požitia alkoholu alebo inej návykovej látky spôsobom ustanoveným osobitným predpisom, hoci by také vyšetrenie nebolo spojené s nebezpečenstvom pre jeho zdravie
300 euro (9 000 Sk), VP 1 rok
1 300 euro (39 000 Sk), VP 5 rokov
nerieši sa





b, ako vodič vozidla, ktorý sa zúčastnil na dopravnej nehode, bezodkladne nezastavil vozidlo, nezdržal sa požitia alkoholu alebo inej návykovej  látky po nehode v čase, keď by to bolo na ujmu zistenia, či pred jazdou  alebo počas jazdy požil alkohol alebo inú návykovú látku, alebo nezotrval na mieste dopravnej nehody až do príchodu policajta alebo sa na toto miesto bezodkladne nevrátil po poskytnutí alebo privolaní pomoci, alebo po ohlásení dopravnej nehody,

300 euro (9 000 Sk), VP 1 rok
1 300 euro (39 000 Sk), VP 5 rokov
nerieši sa




c, vedie motorové vozidlo bez príslušného vodičského oprávnenia alebo počas zadržania vodičského preukazu okrem prípadu, ak sa učí viesť motorové vozidlo v autoškole, podrobuje sa skúške z vedenia motorového vozidla alebo má v čase zadržania vodičského preukazu povolenú jazdu,

300 euro (9 000 Sk), VP 1 rok
1 300 euro (39 000 Sk), VP 5 rokov
nerieši sa



d, vedie motorové vozidlo v stave vylučujúcom spôsobilosť viesť motorové vozidlo, ktorý si privodil požitím alkoholu,

200 euro (6 000 Sk)
1 000 euro (30 000 Sk), VP 5 rokov
650 euro (19 500 Sk)




e) ako vodič počas vedenia vozidla požije alkohol alebo vedie vozidlo v takom čase po jeho požití, keď sa na základe vykonaného vyšetrenia podľa osobitného predpisu,  alkohol ešte nachádza v jeho organizme,

150 euro (4 500 Sk)
800 euro (24 000 Sk), VP 3 roky
650 euro (19 500 Sk)




f) ako vodič počas vedenia vozidla požije inú návykovú látku alebo vedie vozidlo v takom čase po jej požití, keď sa na základe vykonaného vyšetrenia podľa osobitného predpisu, návyková látka ešte nachádza v jeho organizme,
200 euro (6 000 Sk)
1 000 euro (30 000 Sk), VP 5 rokov
650 euro (19 500 Sk)





g) poruší všeobecne záväzný právny predpis o bezpečnosti a plynulosti cestnej premávky, v ktorého dôsledku vznikne dopravná nehoda
150 euro (4 500 Sk)
800 euro (24 000 Sk), VP 3 roky
650 euro (19 500 Sk)




h) ako vodič motorového vozidla prekročí rýchlosť ustanovenú v osobitnom predpise, 
alebo prekročí rýchlosť ustanovenú dopravnou značkou alebo dopravným 
zariadením,

150 euro (4 500 Sk)
800 euro (24 000 Sk), VP 3 roky
650 euro (19 500 Sk)




i) použije vozidlo, ktorého najväčšia prípustná celková hmotnosť vozidla, najväčšia prípustná hmotnosť jazdnej súpravy, najväčšia prípustná celková hmotnosť prípojného vozidla alebo najväčšia prípustná hmotnosť pripadajúca na nápravy vozidla je prekročená,
150 euro (4 500 Sk)
800 euro (24 000 Sk), VP 3 roky
650 euro (19 500 Sk)




j) iným spôsobom ako uvedeným v písmenách a) až i) sa dopustí porušenia pravidiel cestnej premávky závažným spôsobom podľa osobitného predpisu, 
Porušením pravidiel cestnej premávky závažným spôsobom je: 
g) prejazd cez križovatku na signál STOJ! dávaný policajtom alebo inou 
oprávnenou osobou, alebo vyplývajúci z dopravnej značky alebo dopravného 
zariadenia, 
h) prejazd cez železničné priecestie v čase, keď je to zakázané, 
i) nedanie prednosti v jazde, 
j) neuposlúchnutie pokynu, výzvy alebo príkazu policajta v súvislosti s výkonom jeho oprávnení pri dohľade nad bezpečnosťou a plynulosťou 
cestnej premávky, 
k) jazda v protismernej časti cesty, ak si to nevyžaduje situácia v cestnej premávke, 
l) neumožnenie bezpečného a plynulého prejazdu vozidlu s právom 
prednostnej jazdy, 
m) vedenie vozidla bez tabuľky s evidenčným číslom alebo s tabuľkou s 
evidenčným číslom, ktorá nezodpovedá svojím vyhotovením a umiestnením na 
vozidle tomuto zákonu, 
n) vedenie vozidla vyradeného z cestnej premávky alebo vozidla, ktoré 
nebolo schválené na prevádzku v cestnej premávke, alebo vozidla 
vyradeného z evidencie, 
o) predchádzanie iného vozidla v mieste, kde je to zakázané, 
p) vedenie motorového vozidla bez príslušného vodičského oprávnenia 
alebo počas zadržania vodičského preukazu; to neplatí, ak sa osoba učí 
viesť motorové vozidlo v autoškole, podrobuje sa skúške z vedenia motorového vozidla alebo má povolenú jazdu podľa § 70 ods. 3 a §71 ods. 2, 
q) porušenie povinnosti podľa § 3 ods. 3, § 4 ods. 3, § 9 ods. 2 alebo § 
25 ods. 1 písm. g).
60 euro (1 800 Sk)
300 euro (9 000 Sk), VP 2 roky
150 euro (4 500 Sk)




k) iným konaním, ako sa uvádza v písmenách a) až j), poruší všeobecne záväzný právny predpis o bezpečnosti a plynulosti cestnej premávky.
0 euro (0 Sk)
100 euro (3 000 Sk)
60 euro (1 800 Sk)

	


 Záver: 
Zmien je veľa, času málo. Nie všetky zmeny sa sem vmestili. Ak si myslite, 
že som nejakú významnejšiu zmenu podcenil a neupozornil na ňu, rád ju sem 
dopíšem. V každom prípade odporúčam nový zákon aj novú vykonávaciu vyhlášku 
naštudovať. Prajem veľa šťastných kilometrov na cestách po Slovensku. 

 
Text zákona o cestnej premávke 8/2009 Z. z.  

Text vykonávacej vyhlášky k zákonu o cestnej premávke 9/2009 Z. z. 

