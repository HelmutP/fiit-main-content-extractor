
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maximos Dragounis
                                        &gt;
                Nezaradené
                     
                 Macedónia-krajina ktorá rozšírila grécku kultúru až do Indie 

        
            
                                    15.4.2010
            o
            20:24
                        (upravené
                19.4.2010
                o
                20:03)
                        |
            Karma článku:
                2.81
            |
            Prečítané 
            1532-krát
                    
         
     
         
             

                 
                    Makedonia, si a budeš, pýcha Grékov a mi k tebe budeme nazerať iba s hrdosťou! To je súčasťou hymny gréckej Makedonie. Makedonia je severogrécka provincia v ktorej žije 2,5 milióna obyvateľov.  Hlavné a najväčšie mesto je Thessaloniki (slov. Solún). Iné veľké mestá sú Serres, Kavala, Drama, Kilkis, Giannitsa, Veria, Kozani, Katerini, Grevena, Kastoria, Florina, Edessa, Ptolemaida a Polygyros. Územie Makedonie sa rozprestiera v samom severe Grécka, z juhu hraničí s krajom Thesalia, zo západu s krajom Ipiros (starogr. Epiros, lat. Epirus), z východu s krajom Thraki (Trácia) a zo severu so štátmi Albánsko, FYROM a Bulharsko. Nachádza sa tu bájne pohorie Olymp (gr. Olymbos), najvyšší vrch je Mytikas. V tomto článku vás oboznámim s históriou a tradičnou ľudovou kultúrou a folklórom tejto nádhernej krajiny a poviem tiež, prečo si grécky národ nikdy túto krajinu nedá zobrať a že jej história vždy bola spätá s Grékmi, hoci dnes si naši severní susedia robia nároky nie len na našu históriu, ale čo je horšie, aj na naše územie. Nasledujúci článok opisuje dejiny a kultúru severného Grécka-Makedonie, teda krajiny, kde sa rozprestierala aj staroveká Makedonia a nie slovanskú Macedóniu.
                 

                 História-od doby mýtickej po súčastnosť   Pelasgovia a tiež Tráci obývali územie dnešnej Makedónie už od doby eneolitu. V druhom tisícročí pred Kr. sa tu usádzajú grécke kmene, preodovšetkým však Dóri, ktorí sa z tohto územia neskôr presunuli do južného Grécka. Podľa najnovších výskumov však neodišli všetci Dóri, ale mnohi tu ostali a boli to predkovia neskorších starovekých Makedóncov. Táto teória nachádza podporu aj v starovekom makedónskom dialekte starogréčtiny, kde sú mnohé spoločné prvky s dórskym dialektom. Iná teória tvrdí, že antickí Makedonci boli potomkovia tzv. severogréckych kmeňov, akými boli obyvatelia kraja Epiros. Podľa gréckej mytológie, zakladateľ gréckeho národa Hellén (Ellinas) bol príbuzný s Makedónom, zakladateľom makedónskeho kmeňa. Makedón bol synom najvyššieho gréckeho boha Dia. V archaickej gréčtine znamenalo slovo makednos "vysoký, vysokopoložený alebo severný. Prvý štát tu vznikol niekedy v 8 stor. pred Kr. a založila ho dynastia Argeoi (Argeovci). Prvý kráľ bol Perdikas (od gr. slova perdika-jarabica). Tento panovník založil aj mesto Aigai, blízko dnešného mesta Vergina. Spočiatku žili Makedonci iba západne od dnešného mesta Thessaloniki, v kraji Imathia a Pella, no neskôr dobyli nové územia, hlavne (dnešné Kozani, Grevena, Kastoria, Florina a Pieria) a územia na sever od polostrova Chalkidiki. V týchto oblastiach dovtedy žili Makedóncom príbuzné gréckohovoriace kmene (kraje Kozani, Grevena, Kastoria a Florina). V kraji Pieria a na územií dnešného severného Grécka však žili Tráci a keďže boli podrobení a nehovorili po grécky, Makedónci ich vyhnali a územie kolonizovali svojimi ľuďmi. Tráci z tohto územia sa následne usadili v Paiónií, teda na územií dnešného štátu FYROM. Tak, približne v 5 stor. pred Kr. bolo definitívne ukončené osídlovanie Makedónie a makedónska identita.   V klasickom staroveku sa Makedónci líšili od južných obyvateov Grécka. Makedónci mali málo miest, žili predovšetkým na vidieku a venovali sa chovom zvierat. Ich kultúru by sme nemohli porovnať s Aténčanmi, Sparťanmi, či Maloázijskými Grékmi, skôr boli podobní obyvateľom stredného Grécka a Epiru. Klasické grécke vzdelanie postupne prenikalo aj sem, hlavne vtedy, ked Makedónci dobyli polostrov Chalkidiki, kde žil vyspelý grécky kmeň Iónov. Chalkidiki sa tak stali súčasťou Makedónie, Makedónci sa však na polostrove nikdy neusadili, iba na jeho severe, keď odtiaľ vyhnali Trákov. Jeden z najvýznamnejších makedónskych kráľov bol Filippos II. (po gr. priateľ koňa), ktorý sa zaslúžil o skultúrnenie Makedónie, bolo postavené nové hlavné mesto, Pella, podľa vzoru gréckych miest (poleis). Filippos ďalej úplne ovládol územie Paiónie (dnešné FYROM), kde žili divokí Tráci a im príbuzní Paióni a Illýri. V tomto území sa však Makedónci neusadili, iba priniesli barbarskému obyvateľstvu grécku kultúru a tak sa miestni barbari postupne pogréčťovali. Filippos mal aj iný, odvážny cieľ, zjednotiť rozdrobené Grécko do jedného silného štátu, aby ich už nemohla ohrozovať Perzia. Filippos využil slabosť južných Grékov, ktorí po Aténsko-spartskej Peloponézskej vojne boli značne oslabení a zaútočil na juh. Juhogrécke štáty sa spojili, no pri Chéronei v Boiótií boli v roku 338 pred Kr. Filippom zničení. Makedónci ovládli celé pevninské Grécko aj s Aténami.   Po nečakanej Filippovej smrti nastúpil na trón jeho syn Alexandros (Alexander), neskôr známy ako O Megas Alexandros (Alexander Veľký). Alexander bol iba polovičný Makedónec, jeho matka Olympias pochádzala zo severogréckeho Epiru. Alexander chcel zakončiť úspešné ťaženie svojho otca a zaútočil na Perziu, večného gréckeho rivala. Naverboval armádu z celého Grécka. V troch veľkých bitkách Alexander Peržanov aj napriek ich obrovskej presile zničil a keď našiel mŕtve telo perzského kráľa Dariosa III. Gréci definitívne ovládli Perziu. Alexander pokračoval ďalej až do Indie. V roku 323 pred Kr. Alexander zomrel v Babylone.   Po jeho smrti sa jeho ríša rozpadla na veľa kráľovstiev, kde vznikali nové grécke osady, kde sa usadili mnohí Makedónci. Gréci žili aj v Indií a založili tu grécko-Baktrijskú ríšu. Makedónsky rod Antigonovci vládli v Makedónií, ktorá však ovládala aj severné územia Paiónov a južné Grécko. Kráľ Kassandros založil nové hlavné mesto, Thessaloniké (Solún).  V 2 stor. však Makedóniu dobyli Rimania, pretože kráľ Filipps V. sa spojil s Hannibalom proti Rímu. Rimania Makedóncov porazili a to zopakovali aj proti poslednému makedónskemu kráľovi Perseasovi. Od tejto doby je tu vytvorená provincia Macedonia, ktorá však ovládala aj susedný Epiros, Paióniu, grécku Tráciu a Thesáliu. Hlavné mesto a centrum rímskeho guvernéra bolo Thessaloniké. Nastáva postupný rozmach kresťanstva po misiach apoštola Pavla, ktorý mnoho Grékov obrátil na kresťanstvo. V 3-5 stor. Makedóniu vyplienili Góti, Herulovia, Huni a bojujúce cisárske armády. Od roku 395 patrí Makedónia do Východorímskej ríše (grécka Byzantská ríša). Obyvatelia už boli všetci kresťania.   Definitívny rozmach kultúry nastáva v 6 stor., kedy nevydržala byzantská hranica na Dunaji, ktorú prelomili Slovania a všade sa usadzovali. Prenikli až do Makedónie a usadzovali sa tu. Zaplavili celú starú Paióniu-dnešný FYROM (v tej dobe miestni obyvatelia rozoprávali grécky) a sever Grécka po Thessaloniki, ktoré sa im však nikdy nepodarilo dobyť. Slovania sa pomiešali s pogréčtenými Paiónmi a s Makedóncami na severe dnešného Grécka a územie ostalo čisto slovanské. Staré mestá ako Heraklea sa zmenili na ruiny a jednoduché slovanské osady. Byzantská grécka vláda podnikla proti Slovanom v Grécku viacero akcií a armáda generála Staurakiosa v 8 stor. porazila a zotročila mnoho gréckych Slovanov. Aj z južnej Makedónie potom Gréci odviedli veľa Slovanov a usadili ich na hranici s Perziou, kde muži slúžili v armáde. Definitívny obrat nastal až začiatkom 9 stor., kedy byzantskí Gréci úplne zničili gréckych Slovanov pri bitke pri peloponézskom meste Patra. Väčšina Slovanov z Grécka bolo odvedených na perzskú hranicu. Toto opatrenie spravila byzantská vláda aj v južnej časti historickej Makedónie, kde sa Gréci kontinuálne udržali. V jej severnej časti (Paiónia a moderné grécke kraje severná Kastoria, Florina, Pella a Kilkis) sa však Slovania udržali, lebo územie dobyli Bulhari a Gréci tak slovanské obyvateľstvo nemohli presídliť a ani sa o to nesnažili, naopak, Grékov z týchto území presídlili inde. Takto rozdelená Makedónia, medzi Slovanov a Grékov ostala až do 20 stor. Slovania z historického územia Makedónia a Paiónia sa začali označovať ako Bulhari, prípadne Srbi, hoci neboli potomkovia ani jedných z uvedených kmeňov. V byzantskej dobe sa územie historickej Makedónie nazývalo Thessaloniki-správny územný celok théma. Existovala aj théma Makedonia, tá sa však nachádzala ďaleko od tej antickej, až v dnešnom strednom Bulharsku, ktoré Gréci dobyli v 10. stor. Byzancia ovládla svoje stratené územia až po Dunaj, no teraz tu už ostalo Slovanské obyvateľstvo a žiadne pokusy o presídlenie sa nekonali. Z Makedónie pochádzali aj slávni slovenskí vierozvestci Konstantinos a Methodios (Konštantín a Metod). Neboli to však úplní Makedónci, ich otec Leon pochádzal zo Sparty, matka z Makedónie, no nevie sa, či bola Grékyňa, alebo Slovanka. Slávny cisár Basileos Veľký, ktorý porazil Bulharov tiež pochádzal z Makedónie, no opäť to nebol Makedónec, jeho predkovia boli Arméni a Gréci zo Sparty. Územie Makedónie však prosperovalo, Thessaloniki boli druhým najvýznamnejším mestom, hneď po Konstantinopole. Významné mesto bola tiež Veria (staroveká Beroia). Tieto dve mestá sa nachádzali v južnej, gréckej časti Makedónie. V 13. stor. Byzanciu dobyli Európania a vytvorili feudálne kráľovstvo Thessaloniki. Európska moc však padla a nová Byzancia opäť Makedóniu ovládla. V roku 1430 však Thessaloniki ovládli Turci a neskôr si podmanili celú Makedóniu.   Turkom sa následne podarilo islamizovať časť Slovanov a Grékov v Makedónií, ktorí tak stratili svoj jazyk a stali sa z nich Turci. V Thessalonikách žili predovšetkým Židia. V tureckej dobe tu vzniklo aj grécke mesto Kozani, ktoré založil Ioannis Trandas. Iné dôležité mestá gréckej kultúy boli Grevena, Kavala, Veria, Naussa, Siatista, Serres, Nigrita a najsevernejším bodom Grékov bolo mesto Kastoria. Najvýznamnejším mestom však bolo Thessaloniki. Severne od mesta Kastoria už žili Slovania. Grécky živel sa neskôr objavil aj v meste Edessa. Makedónski Gréci boli výborní podnikatelia, mnoho z nich sa usadilo v dnešnom Rumunsku, v rôznych končinách Balkánu, vo Viedni a tiež na území dnešného Slovenska a Maďarska. Prosperovali aj Chalkidiki, kde vzniklo združenie gréckych obcí-Mademochoria, ktoré viedli podnikateľské aktivity. V tomto období sa v častaich Makedónie usadili aj Albánci a grécki Arumuni (Valasi). Významným centrom Arumunov bolo hlavne mesto Monastiri (dnes Bitola-FYROM) a jej široké okolie, obývajú tiež malé územie s pár dedinami za horou Olymp a severne od mesta Naussa. V roku 1831 po gréckej národnej revolúcií vznikol Grécky štát, Makedónia mu však nepatrila, hoci aj tu boli významné protiturecké grécke povstania. Najznámejším makedónskym gréckym revolucionárom bol kapitán Emanuil Pappas z mesta Dovista pri Serres, ktorý viedol povstanie Grékov na Chalkidiki.   Koncom 19 stor. vypukol obrovský boj o Makedóniu. Viedli ich dva štáty, ktorých menšiny tu tvorili najpočetnejšie obyvateľstvo, teda Grécko a Bulharsko. Bulharsko argumentovalo, že slovanské obyvateľstvo sa cíti byť Bulharmi (v skutočnosti tu však bolo mnoho Slovanov bez určitej národnej identity) a keďže Slovania patrili pod novovzniknutú Bulharskú orthodoxnú cirkev, Bulahrsko si robylo nároky. Oslabení Turci sa iba prizerali, ako sa Gréci a Bulhari hádajú, kto im prvý ukoristí časť ich územia. Malá časť slovanského obyvateľstva v krajoch Kastoria, Edessa a Kilkis sa začala označovať za Grékov a nesúhlasila s bulharskou možnou vládou. Naopak, skutoční makedónski Gréci sa obávali vyhrážkam Bulharska a tak tu vznikla veľká revolúcia miestnych Grékov. Toto sa naplno vyostrilo medzi rokmi 1904-1908. Proti Bulharom vzniklo obrovské povstanie makedónskych Grékov, ktorých prišli podporiť aj bojvní Kréťania a Peloponézania. Makedóncov viedol biskup z mesta Kastoria, Jermanos Karavangelis (pôvodom z ostrova Lesbos) a grécky veľvyslanec Ion Dragumis (pôvodom Makedónec), spolu s miestnymi generálmi, ktorými boli Epirót Paulos Melas, Vangelis Strebreniotis, Armen Kupstios, Peloponézan Tellos Agras, Gonos Jotas a Konstantinos "Kottas" Christu. Konstantinos Christu bol slavofónny Grék, teda rozprával slovansky, pochádzal z Floriny. Jeho posledné slová boli Zivia Gritsia, sloboda i smyr! (Nech žije Grécko, sloboda, alebo smrť!). Grécki bojovníci sa nazývali Makedonomachi (teda makedónski bojovníci). Krvavé strety medzi Grékmi a Bulharmi sa odohrali pri Kastórií a Giannitsi (Janici). Obidve strany kruto zaobchádzali aj s civilným obyvateľstvom, Gréci útočili na slovanské obyvateľstvo, Bulhari na grécke. Okrem toho, obidve skupiny útočili na turecké jednotky a oslabovali tak tureckú moc. Grécki povstalci účinne kontrolovali územia obývané Grékmi, naopak Bulhari ovládli slovanské územia dnešného severného Grécka. Situácia sa zmenila až po 1 sv. vojne, kedy sa Gréci pridali na stranu víťazov. Grécka armáda premiéra Eleftheria Venizela vtedy obsadila celú Makedóniu aj s Thessalonikami a bulharské jednotky sa stiahli. Grécko ovládlo celé územie, hranice dostali svoju dnešnú podobu.   Gréci však s Bulharmi spravili kompromis a obidva štáty sa dohodli na výmene obyvateľov. Väčšina Slovanov z Grécka odišla do Bulharska, z Bulharska do Makedónie prišli Gréci, ktorí už od 8 stor. pred Kr. žili na brehoch bulharského Čierneho mora. Grékov, ktorí do Grécka prišli však bolo oveľa menej, ako Slovanov, ktorí Grécko opustili a tak ostala celá severná časť gréckej Makedónie iba riedko osídlená. Situácia sa nečakane vyriešila v roku 1923, kedy Grékov porazili Turci v Malej Ázie a Gréci definitívne stratili všetky svoje územia v Malej Ázií. Následne bola podpísaná Grécko-turecká výmena obyvateľstva, pričom z Turecka do Grécka prišlo 2 000 000 Grékov a  Grécko aj s Makedóniou opustilo 500 000 moslimov. Viac ako 750 000 Grékov z Turecka sa usadilo vo vyľudnenej severnej Makedónií, 250 000 z nich sa usadilo v Thessalonikách. Títo Gréci obnovili mestá a dediny, ktoré ostali po odchode Slovanov prázdne. Makedónia tak bola husto zaľudnená s drvivou gréckou etnickou prevahou. Hlavná časť obyvateľstva z Turecka, ktorá sa v Makedónií usadila boli tzv. Pontskí Gréci (gr. Pondi), potomkovia antických gréckych Iónov (od 8 stor. pred Kr. sídlili na severovýchode Turecka) ktorí rozprávali svojim vlastným gréckym jazykom a preto sa ťažko rozumeli s miestnymi Grékmi. gréčtinu z Grécka sa museli naučiť ako nový jazyk. Veľký počet Slovanov odišiel z Grécka po 2 sv. vojne, nakoľko kolaborovali s porazenými komunistami. Títo Slovania sa usadili v Juhoslávií.   Ľudová kultúra Makedónie   Makedonia xakusti, tu Alexandru i chora, pu edioxes tus varvarus ke eleftheri ise tora (Makedónia slávna, Alexandrova krajina, čo si barbarov vyhnala a slobodná si teraz).   Toto sú úvodné slová hymny gréckej Makedónie, ktoré odkazujú na slávnu minulosť krajiny.       
 Folklórne tradície zo severného Grécka-Makedónie majú korene ešte v antickej Makedónskej dobe. Symbolom gréckej Makedónie je už od staroveku tzv. Vergínske slnko (gr. Ilios tis Verjinas). Tradičná makedónska ľudová hudba sa odlišuje od ostatnej pevninskej gréckej hudby, podobná je hudbe z gréckej Trácie (Thraki) a isté spoločné znaky má aj s bulhaskou hudbou. Tak, ako aj ostatnú grécku hudbu, aj makedónskú zaraďujeme pod orientálne typy hudby. Začiatkom 19 stor. do pevninského Grécka prišiel hudobný nástroj klarinet (gr. klarina), ktorý nahradil tradičné grécke, klarinetu podobné dychové nástroje zurna a flojera, ktoré sa vyvynuli zo starogréckeho nástroja aulos. Jedine Makedónia klarinet neprijala a uchovala si zurnu a flojeru. Tieto nástroje dominujú Makedónií aj dnes. Okrem nich, je tu tradičným ľudovým nástrojom aj nástroj gaida (gajdy). V pozadí hrajú orientálne mandolíny lauto a udi. Tak ako aj ostatní Gréci, aj Makedónci tancujú do kruhu. Tradičné makedónske tance sú Akritikos, Baiduska, Kastorianos (z mesta Kastoria), Levendikos-Pustseno (tanec z mesta Florina, po slovansky Pušteno), Makedoniko chasapiko (Makedónske chasapiko, chasapiko je pôvodne byzantský grécky tanec z Konstantinopolu, ktorý bol prinesený aj do Makedónie), Zaiko kukuraiko, Makedonikos andikristos (tanec z mesta Kozani) a Syrtos Makedonias (makedónska verzia panhelénskeho tanca Syrtos). Tradičný je aj ženský tanec z mesta Alexandria z kraja Imathia, tento tanec má ešte staroveký pôvod. Hudbu, ktorá nám k Makedónií nepasuje, nájdeme na polostrove Chalkidiki, kde sa ľudová hudba viac podobá tej ostrovnej, prevládajú tu husle a brnkacie udi a lauto.    Tradičné kroje Makedónie sú veľmi zaujímavé. Ženy z kraja Imathia (centrum antickej Makedónie) nosia na hlavách čiapky, ktoré vyzerajú ako staroveké grécke korintské prilby. Táto tradícia siaha ešte k Alexandrovi Veľkému, ktorý miestnym ženám povolil nosiť prilby, lebo pomohli jeho vojsku. Centrum antickej Makedónie Imathia má zaujímavosť aj v mužských krojoch. Počas karnevalového obdobia nosia muži tradičné ľudové masky, čo súvisí s povstaním Grékov z Imathie proti Turkom, ktorí chceli brať miestne deti do služieb. Počas karnevalu nosia muži aj bojový šupinový byzantský pancier. Makedónski muži nosia hlavne biele košele (pukamisso), čiapku fez (fesi) a čiernu suknicu (fustanella) alebo čierne nohavice (pandelonia), v ostatnom Grécku sa nosí biela suknica. V kraji Imathia, Kozani a Grevena však nájdeme biele suknice tiež. Ženy nosia pestrofarebné kroje. Do makedónskeho tradičného folklóru patrí od roku 1923 aj folklór Grékov z Malej Ázie (Turecko), ktorí tvoria väčšinové obyvateľstvo severnej gréckej Makedónie. Nájdeme tu teda aj anatólske-grécke piesne, tance a jazyk Pontských Grékov, keďže však chcem písať o tradičnej autochtónnej makedónskej kultúre nebudeme detailne rozoberať Maloázijskych Grékov (ak sa chcete dozvedieť o ich tradičnej kultúre prečítajte si môj článok-Maloázijskí Gréci). Môžeme však spomenúť, že Pontskí a Kappadócki Gréci si v Makedónií hrdo udržiavajú svoj jazyk a kultúru.     
 Pôvod gréckych Makedóncov    Moderní obyvatelia Makedónie sa rozdeľujú na tri skupiny. V južnej gréckej Makedónií, teda v srdci tej antickej Makedónie (kraj Imathia, Pieria, Kozani, Grevena, polovica kraja Kastoria a čiastočne kraj Thessaloniki) žijú potomkovia starovekých Makedóncov. Obyvatelia týchto krajov stále rozprávali po grécky, slovanský vplyv na obyvateľstvo bol iba minimálny a Slovania tu nezanechali stopy svojho jazyka, ani kultúry. Naopak je to v severnej časti gréckej Makedónie (kraje Florina, severná polovica Kastorie, Pella a Kilkis). V tejto časti tvorili väčšinu až do roku 1923 slovansko hovoriace obyvateľstvo. Po spomínanej výmene obyvateľov medzi Grékmi a Bulharmi však väčšina Slovanov odišla, ešte viac Slovanov odišlo do Juhoslávie po roku 1947 a preto sú kraje Kilkis a Pella dnes obývané hlavne Grékmi z Malej Ázie, ktorí sem prišli po Grécko-tureckej výmene obyvateľov v roku 1923 (hlavne Pontskí Gréci, čiatsočne aj Kappadócki Gréci). V kraji Florina a v severnej časti kraja Kastoria však Slovania žijú dodnes, je ich okolo stotisíc a títo mohli v Grécku zostať, lebo sa považovali za Grékov (je to čiastočne pravda, jedná sa hlavne o slavonizovaných Grékov zo 7 stor.). Grécki Slovania sa sami označujú ako Slavofoni Ellines (Slavofónni Gréci). Grécka vláda ich v minulosti vyzývala či nechcú byť uznaní za menšinu, vyzývala vtedy aj linguistické menšiny Arumunov-Valachov (románsky hovoriaci Gréci zo stredného Grécka) a Arvanitov (albánsky hovoriaci Gréci z južného Grécka). Za všetkých vtedy odpovedal Panellinios Sylogos Vlachon (Panhellénske združenie Valachov), keď povedal, že "My nechceme byť menšina v našej vlasti, v jedinej krajine našich predkov, lebo nie sme o nič menší Gréci od tých gréckohovoriacich". Niekoľko dedín severne od mesta Naussa, pri meste Grevena a za horou Olymp obývajú Arumuni-Valasi. Na záver teda môžeme povedať, že obyvateľstvo gréckej Makedónie sa rozdeľuje na dve najväčšie skupiny-gréckohovoriacich Makedóncov na juhu a Maloázijskych Grékov na severe.     
 Makedónia vs FYROM    Slovansko-Grécke spory o Makedóniu v súčastnosti po Bulharoch prebrali obyvatelia státu FYROM (antická Paiónia). Snahy o "makedonizáciu" miestneho slovanského obyvateľstva môžeme nájsť začiatkom 20 stor., kedy miestna inteligencia chcela nezávislosť od Bulharskej orthodoxnej cirkvy a tak mali záujem vytvoriť si vlastný národ. Táto ambícia sa im však nepodarila a väčšina obyvateľstva sa necítila byť Makedóncami. Situácia sa zmenila až po 2 sv. vojne, kedy stará Paiónia pripadla komunistickej Juhoslávií. Vládca Tito vtedy oživil starú snahu o vytvorenie "Makedónskeho národa", keďže dovtedy sa obyvatelia dnešného FYROM cítili byť Bulharmi a Srbmi. Aby moc Srbov nebola v Juhosláví veľká a aby v Juhoslávií neboli Bulhari, vytvoril Makedóniu a Makedónsky národ na územií dnešného FYROM, ktorá sa dovtedy volala Krajina Vardarská Bánovina. Začala sa tam šíriť veľká promakedónska propaganda, v školách sa vyučovali dejiny antickej Makedónie a predostrelo sa, že obyvatelia Vardarskej Bánoviny sú potomkovia Makedóncov. Po rozpade Juhoslávie sa Vardarská Bánovina osamostatnila pod názvom Macedónsko. Grécku však vadilo, že pretvára grécke dejiny v svoj prospech, dokonca sa v školách vyučovalo, že Alexander Veľký a Macedónci boli Slovania a rozprávali Slovansky (hoci Slovania na Balkán prišli až 900 rokov po Alexandrovej smrti!). Štát FYROM vtedy zobral tradičný symbol Grékov z Makedónie, Vergínske slnko a umiestnil si ho na štátnu vlajku. Obyvatelia FYROM dokonca Grékom zobrali aj národné heslo z protitureckého odboja-Sloboda, alebo smrť. Táto propaganda je veľmi nebezpečná, sám premiér Gruevski sa totiž zúčastnil stretnutia radikalistov, ktorí hlásajú pripojenie severného Grécka aj s Thessalonikami do FYROMu Iba neovplyvnená inteligencia FYROMu krúti nad touto propagandou hlavu, historici zo Skopje sa od týchto názorov dištancujú a kritizujú vládu za neoprávnené kroky a fanatickú postkomunistickú propagandu. Grécko viackrát ustúpilo a ponúklo FYROMu vybrať si názov ako Severná Makedónia, Nová Makedónia, Slovanská Makedónia, politici Skopje však všetky návrhy odmietli. Historikovi je však jasné, že obyvatelia tejto krajine nemôžu byť vôbec potomkovia antických Makedóncov, či už slovanizovaných Makedóncov. Ich krajina sa totiž v staroveku nazývala Paiónia a žili tu Paióni, Illýri a Tráci, no Makedónci na územií dnešného FYROMu nikdy nežili. Teda je síce pravda, že moderní obyvatelia sú potomkami starovekého etnika Illýrov, Paiónov a Trákov, pomiešaných so Slovanmi, no nemajú žiadne spojenie (či už kultúrne, alebo biologické) s antickými Makedóncami. Staromakedónske kultúrne tradície a skutočných potomkov Makedóncov nájdeme v severnom Grécku. Tento spor je zároveň vôbec prvým sporom, kedy sa niekto snaží oživiť negrécku Makedónsku identitu. Počas Grécko-bulharských bojov síce mali Bulhari záujem ovládnuť Makedóniu, no uznávali, že Makedónci sú Gréci. V tej dobe išlo o Makedónsko-bulharský spor, teda Grécko-bulharský.   Záver   Dúfam, že tento článok pomohol slovenskému čitateľovi nahliadnuť do bohatej kultúry, ale aj problematiky Makedónie a jej ľudí, ktorí sú hrdí na svoju minulosť. Makedónia ponúka mnoho miest ktoré stoja zato navštíviť. Slnečné stredomorské Chalkidiki, miliónové veľkomesto Thessaloniki, zimné lyžiarske centrá, karneval v Imathií či Kozani, bájnu horu Olymp, jazero Prespes, starogrécke pamiatky v mestách Pella, Potidea, Thessaloniki, Olynthos, Stageira (Aristotelovo rodisko) a Vergina a rímske a byzantské grécke pamiatky hlavne v Thessalonikách či Verií (starogrécka Beroia). Môžete si spraviť aj turistiku do nádhernej zelenej makedónskej prírody.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok Gréka XI-Grécke voľby-bezvládie, pat a čoskoro ďalšie voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Rembetiko, hudobný štýl gréckeho podsvetia a gréckych gangstrov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka X: Prešiel úsporný program, rozpad parlamentu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného GrékaIX-Nová pôžička, rozpad koalície a grécke voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka VIII.-Historická genéza gréckej krízy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maximos Dragounis
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maximos Dragounis
            
         
        dragounis.blog.sme.sk (rss)
         
                                     
     
        Volám sa Maximos, pochádzam z Grécka. 
Facebook:
http://www.facebook.com/pages/Maximos-Dragounis-Gr%C3%A9cko-bez-propagandy-%CE%97-%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1-%CF%87%CF%89%CF%81%CE%AF%CF%82-%CF%80%CF%81%CE%BF%CF%80%CE%B1%CE%B3%CE%AC%CE%BD%CE%B4%CE%B1/103882279704191?sk=wall
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1935
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




