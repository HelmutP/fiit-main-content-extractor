
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Roy
                                        &gt;
                Všetkým
                     
                 Páči sa mi ten Hanzel - 

        
            
                                    1.6.2010
            o
            15:09
                        (upravené
                1.6.2010
                o
                20:39)
                        |
            Karma článku:
                7.14
            |
            Prečítané 
            649-krát
                    
         
     
         
             

                 
                    i keď, tuším, v tohto týždňovom, .Týždni ho Štefan Hríb nie veľmi vyznamenal označením za morálneho invalida. Neviem, či je Bohumil Hanzel morálny invalid, neviem o ňom vlastne nič okrem toho, čo je dostupné na internete, ale morálnych invalidov je nás tu, na Slovensku, podstatne viac.
                 

                 Páči sa mi ten Hanzel. Páči sa mi ako vystúpil, povedal, oznámil a teraz už aj predal, podľa neho, dôkazy o možnej Ficovej korupcii Švédskemu veľvyslancovi v SR.   Som rada, že tak urobil, že sa rozhodol vyjsť s faktami von.   Keď prvýkrát verejne vystúpil s informáciami, že s Ficovou predvolebnou kampaňou to nie je úplne kóšer, keď hovoril o tom, čo sa dialo v zákulisí strany Smer pri jej zakladaní, keď hovoril o čistení účtovníctva, aby bolo čisté, čistunké, aby bolo nad slnko jasnejšie, že Smer má ruky čisté prečisté, a keď som si potom o ňom prečítala z jeho životopisu, že podpísal Chartu a bol donútený k emigrácii, v duchu som si povedala - "Hej, veď kto iný, než človek, čo podpísal Chartu by to ustál. Kto iný by dokázal prehovoriť nahlas a verejne o tom, čo je v tomto štáte, v štátostrane Smer choré, ak nie človek, čo ustál prenasledovanie, výsluchy. Podpisom Charty vedel do čoho ide a napriek tomu podpísal. Kto iný?"   Potom prišiel Štefan Hríb s označením "morálny invalid" a s dohadmi, prečo tak neurobil skôr, prečo práve teraz, prečo vlastne vôbec zakladal Smer, veď to musel tušiť už od začiatku, čo za eseróčku to bude, čo za prapodivné typy sa okolo vidiny budúcej moci a prospechu z jej budúceho (korupčného) použitia, zlietajú.   Nuž, neviem, či to všetko mohol a mal vedieť už vtedy, i keď, povedzme si to otvorene, mnohí sme to tušili, mnohí sme dopredu vedeli, že v prípade Smeru pôjde o veľmi pokrivenú cestu k "hodnotám". Len sme netušili ako. Stačil jeden pohľad na nablýskanú Moniku Beňovú, ktorej označenie "politička" obsahovo sadne asi tak ako mne nositeľka Nobelovej ceny za literatúru.   Možno chcel Bohumil Hanzel vytvoriť aj u nás niečo na spôsob švédskeho modelu - ekonomiky, politiky, politickej kultúry, len si, chudák, nevšimol, že slovo ako také, je u nás nástrojom manipulácie, nie nástrojom transparentnosti, odkrývania skutočnosti a hľadania vyššej kvality našej reality. Slovo má v našom (nielen) politickom živote hodnotu (váhu) asi ako bublina z bublifuku - vypustíš, letí, chvíľu ho vidíš a potom prááásk, nie je, neexistuje, nič neznamená, akoby nikdy nebolo. Nevšimol si, že slovo "sociálny", prípadne "sociálny demokrat" má v našich podmienkach úplne iný obsah a kvalitu, než je tomu vo Švédsku.   Mal s tým prísť skôr? Neviem, možno mal. Ale, ako vieme, hlasoval v parlamente podľa svojho svedomia, alebo aspoň podľa svojho názoru - aj to je na dnešné pomery až-až. Podľa svojho názoru a dokonca porti SMERU?!   Ako je to teda s tým morálnym invalidom?   ___________________________________   Na Slovensku sme zvyknutí, že akonáhle sa objaví človek - osobnosť, ktorý sa preukáže svojím konaním, svojím slovom ako človek čestný, ako človek, ktorý chce poslúžiť Dobrej veci, Pravde, vzápätí ho zomelie náš politicko-spoločenský stroj a je na neho vytiahnuté niečo z minulosti, čo vraj robil úplne v opačnom duchu, alebo je nejakým spôsobom negatívne onálepkovaný - napr. morálny invalid.   Vyzerá to u nás tak, že, zrejme, nemáme tak čestných ľudí, ktorí by sa proti Zlu v jeho každodennej, i politickej, podobe dokázali postaviť bez nejakých bočných úmyslov, ktorí by neboli hrdinami až ex post, kým v minulosti zastávali opačné postoje, názory.   Podobne to bolo aj s Alojzom Hlinom. Len čo mal za sebou prvý úspešnejší míting s veľmi slušnou účasťou opozične zmýšľajúcich občanov, vytiahli na neho "fakty" z minulosti a predostreli nám obraz - akéhosi podnikatelíka, ktorý hrá raz na jednu, raz na druhú stranu. Zasa nejaký pragmatik, za ktorého dobrými úmyslami, aktívnymi skutkami, bude zrejme znovu len prospechárstvo, či iné podozrivé motivácie.   Akoby v tejto krajine nežili ľudia, ktorí sa snažia o viac elementárnej slušnosti, kultúrnosti, ľudskosti (nielen v politike) len tak, bez nároku na (finančnú) odmenu či zakázku, ale preto, že, jednoducho, tak je to správne, tak je to dobré pre jednotlivca i spoločnosť, že práve oná slušnosť a kultúrnosť je vlastne tým "ziskom".   Možno takých ľudí na verejných miestach naozaj nemáme. Aj teraz sa zjavil len "morálny invalid". Možno sme stále na ceste a vo fáze hľadania takýchto autorít a vzorov.    Nuž, aj hľadanie (sa) je dôležitá fáza!    ____________________________________   Keď som asi pred mesiacom bola s rodinou na výlete vo Viedni, všimla som si jeden zaujímavý volebný plagát. Patril žene, zrejme kandidátke politickej strany, a bolo na ňom napísané: "Ohne Mut, keine Werte!" - "Bez odvahy niet hodnôt!".   Pomyslela som si: "Hm, aký posun vo vedomí!"   Bavia sa o Odvahe, o čom sa bavíme my?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            O Zaujatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Situácia je fakt lepšia: Už sa len Klame, Kradne a Kupujú hlasy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Ako sme volili v porodnici alebo O "prave nevolit"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Prochazka &amp; Knazko, alebo Nevolim zbabelcov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Nemozem uz citat spravy zo Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Roy
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Roy
            
         
        zuzanaroy.blog.sme.sk (rss)
         
                                     
     
         V posledných rokoch ma najviac oslovili myšlienky a knihy Anselma Grúna - nemeckého benediktína, a slovenských feministiek z ASPEKTu. Píšu o tom, kde nám to drhne (v živote) a ako z toho von. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    152
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1606
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Foto: Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Verše bez veršov
                        
                     
                                     
                        
                            Letters to Animus
                        
                     
                                     
                        
                            Môj pes Dasty
                        
                     
                                     
                        
                            Budúci prezident - aký si?
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Alternatívne školy - Bratislavský kuriér
                                     
                                                                             
                                            Môj Prvý pokus - Démonizácia Waldorf. pedagogiky v slov.médiách
                                     
                                                                             
                                            Odkaz Sorena Kierkegaarda
                                     
                                                                             
                                            Tomáš Halík: O výchove a vzdelávaní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Joni Mitchell - absolútna nádhera: Both Sides Now
                                     
                                                                             
                                            Anselm Grün: Zauber des Alltäglichen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dr.Christiane Northrup - Womens Wisdom
                                     
                                                                             
                                            Sloboda Zvierat
                                     
                                                                             
                                            Anselm Grün
                                     
                                                                             
                                            Tomáš Halík
                                     
                                                                             
                                            Dominik Bugár - Fotograf, kolega - rodič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Suma bola zaplatená pánovi dekanovi, ktorú pán dekan schoval do zásuvky"
                     
                                                         
                       David Deida - "Intimní splynutí - Cesta za hranice rovnováhy"..
                     
                                                         
                       All inclusive a piesok na zadku. Je toto dovolenka?
                     
                                                         
                       Moja Pohoda
                     
                                                         
                       Lož na kolesách: Pád Lancea Armstronga
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Gentlemani na Wimbledone
                     
                                                         
                       Druhá šanca pre človeka
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Mantra národa.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




