
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Bohdana Machajová
                                        &gt;
                Nezaradené
                     
                 Tlaková níž 

        
            
                                    6.5.2010
            o
            22:05
                        (upravené
                6.5.2010
                o
                22:11)
                        |
            Karma článku:
                3.53
            |
            Prečítané 
            897-krát
                    
         
     
         
             

                 
                    Prežila som také obyčajné jarné popoludnie. Vonku šumel dážď a prichádzala už dlho avizovaná tlaková níž. Som meteosenzitívna a prebúdzal sa vo mne ten zvláštny, smutný pocit. Možno to poznáte, začne to okolo žalúdka a vám sa chce ani neviete prečo plakať....
                 

                 Nechcelo sa mi ísť po práci hneď domov a tak som si vymyslela ešte jeden cieľ. Ako členka sociálnej komisie staromestského zastupiteľstva som sa rozhodla pozrieť akú akciu Staré mesto zorganizovalo pre našich dôchodcov. Miesto - Poľovnícka reštaurácia na Lazaretskej ulici v Bratislave. Akcia - staromestskí dôchodcovia a ich tanečné popoludnie. Vstúpila som do priestoru a ďalej som šla po zvukoch hudby.   Nemohla som uveriť, všade navôkol šum, hudba a vrava. Plno ľudí, naozaj veľa. Asi sto sivých hláv... ženy, muži. Vyvoňané, vyobliekané akoby naše mamy, babky či otcovia. Ako keby sa tu zrazu predo mnou odvíjal nejaký starý film. Počujem živú hudbu, starý hit Waldemara Matušku a jeho nesmrteľných  „Slávikov z Madridu “, ...já jsem jak zahrada stiná, kdo chce ať závidí, ja si dám sklenici vína... žízeň je veliká, život mi utíká, nechte mě píjemně snít, ve stínu pod kříky poslouchat slavíky spívat si s nima a pít.... Alebo hit repertoáru Karola Duchoňa... Elena, ty si život môj, Elena chcem byť iba tvoj, Elena lásku mi vráť, Elena veď mám ťa rád.... Aby toho nebolo málo pokračujem Olympicom, ...zas si tak smutná, kto se má na to koukat, nic jíst Ti nechutná, v hlavě máš asi brouka, tak nezoufej nic to není, za chvíli sa to změní... snad sem to zavinil já... já já jáá.... Ľudia sa smejú, spievajú, tancujú. No normálne ma dostali! Úprimne sa priznám, chcela som len vedieť, či sa akcia pre staromestských dôchodcov podarila. Nechcela som sa usmievať a ani sa nikomu prihovárať. Proste šla tlaková níž a nemala som „usmievací“ deň. Také niečo sa sem tam prihodí každému, veď napokon nie je každý deň nedeľa.    Záver? Môžu len závidieť všetci tvorcovia zábavných televíznych programov, či všelijakých seriálov. Prepáčte mi prosím všetci Pycovia, Adely, Sajfovia, ale nielen oni. Títo ľudia nepotrebovali žiadne umelé imlpulzy, tešili sa jeden z druhého.... Tešili sa z hitov svojej mladosti, tešili sa z toho, že vládzu, a že sa spolu vidia.... Toľko koncentrovanej pozitívnej energie som už dávno necítila. Šedivé hlavy, žiariace oči, prirodzenosť a veselosť.   Tancovali Letkis a robili hada. Čo poviete – milé, nie? Kto vie, čo nás v ich veku stretne, či dokážeme byť takí ako oni a či budeme mať vôbec na to chuť. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Voľby, prach dejín a foto.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Kto z Dúhového pochodu chytil svadobnú kyticu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Námestie SNP volá - pomôžte prosím!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Génius loci
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohdana Machajová 
                                        
                                            Ondrejský cintorín v ohrození
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Bohdana Machajová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Bohdana Machajová
            
         
        machajova.blog.sme.sk (rss)
         
                                     
     
        Komunálna politička Bratislava - Staré Mesto.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    9
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1204
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




