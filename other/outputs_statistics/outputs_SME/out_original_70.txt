
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Filip Glasa
                                        &gt;
                Podnikanie
                     
                 Neplaťte poukážky neoficiálnym obchodným registrom 

        
            
                                    27.7.2007
            o
            8:02
                        |
            Karma článku:
                11.50
            |
            Prečítané 
            9681-krát
                    
         
     
         
             

                 
                    Register obchodu SR, Register obchodu a živnosti, Register obchodných spoločností SR – stačí si vymyslieť názov podobný oficiálnemu obchodnému registru a novým podnikateľom posielajte poukážku na 2000 korún s nič nehovoriacim sucho-úradníckym listom.
                 

                  Treba sledovať novo registrované podnikateľské subjekty, a všetkým rozposlať poukážku aj s listom. List musí obsahovať údaje novej spoločnosti ale inak musí byť nič nehovoriaci – maximálne s nápisom Návrh zverejnenia. Nový podnikateľ musí nadobudnúť dojem, že je to úradný list, dodatočná povinnosť pri registrácii jeho firmy v oficiálnom obchodnom registri. Časť ľudí to takto pochopí a poukážku vám zaplatí. Dobrý podnikateľský nápad. Ak ste práve nezamestnaný možno dostanete aj príspevok od miestneho úradu práce ak tú tému rozviniete až po finančnú prognózu spoločnosti. Do názvu si nedajte veľmi známu skratku s.r.o., ale zvoľte inú exotickejšiu formu: v.o.s., alebo k.s.     Ak si však vašu firmu na naozaj založíte zistíte, že hneď po zápise vám poštou prídu poukážky s návrhom zverejnenia v nejakom pochybnom zozname s menom nebezpečne podobným oficiálnej inštitúcii. Tieto duplicitné registre môžu byť až tri.     Nie každému je potom jasné, že ide o súkromnú spoločnosť a registrácia v jej zozname nie je potrebná - podnikať na Slovensku môžete aj bez nej.     Duplicitné registre:  Register obchodu SR v.o.s. www.or.sk  Register obchodu a živnosti, Firmdata Slovenská republika, s.r.o., www.firmdata.sk  Register obchodných spoločností SR, Infodat, www.regfirm.sk           Link na diskusie o tejto téme:  http://www.porada.sk/t55664-register-obchodu-a-zivnosti.html  http://www.porada.sk/t37362-register-obchodu-sr.html  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Glasa 
                                        
                                            Porovnajte fondy druhom pilieri v grafoch a číslach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Glasa 
                                        
                                            Druhý pilier a dôchodkové fondy v roku 2013
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Glasa 
                                        
                                            Koľko slovenských firiem je v strate a koľko v zisku?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Glasa 
                                        
                                            Zisk aj tržby každej firmy budú na internete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Glasa 
                                        
                                            Nové firmy rastú ako huby po daždi, podnikatelia sa ponáhľajú
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Filip Glasa
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Filip Glasa
            
         
        glasa.blog.sme.sk (rss)
         
                                     
     
         V roku 2006 som založil web o investovaní Ako-investovat.sk. Od roku 2013 prevádzkujem FinStat.sk, ktorý spracováva a vizualizuje finančné dáta o firmách. 
Pre SME sme s kolegami vytvorili projekt Druhypilier.sme.sk. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5315
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názor
                        
                     
                                     
                        
                            Cestujeme
                        
                     
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Internet
                        
                     
                                     
                        
                            Investovanie
                        
                     
                                     
                        
                            Podnikanie
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bývalí SIS-kári sa údajne priživujú na personálnych agentúrach
                     
                                                         
                       Turkmenistan 2014 (foto)
                     
                                                         
                       Cesta československého legionára okolo sveta za 2038 dní
                     
                                                         
                       Porovnajte fondy druhom pilieri v grafoch a číslach
                     
                                                         
                       Ako Ficova vláda pomáha významne predraženej investícii
                     
                                                         
                       100 rokov od narodenia príliš zvedavého Američana
                     
                                                         
                       FED v pasci: QE3 alebo nízke sadzby navždy
                     
                                                         
                       Druhý pilier a dôchodkové fondy v roku 2013
                     
                                                         
                       Koľko slovenských firiem je v strate a koľko v zisku?
                     
                                                         
                       Zisk aj tržby každej firmy budú na internete
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




