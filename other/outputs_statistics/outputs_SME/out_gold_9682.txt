

 Donedávna tu veľké nákupné centrá neexistovali. Potom sa postavili naraz tri a odvtedy sa polka mesta prechádza tam. Minimálna mzda v Bulharsku je 240 leva (120 Euro)... a aj tak sa zdá, že všetci nakupujú. A najlepšie handry aj tak nájdete na otvorenom trhu, kde sa hovorí prevažne turecko - cigánsky. Ale tam tí Sofijčania nakupovať nechodia. 
   
 My sa chodíme prechádzať na Vitošku. Sofia leží na úpätí pohoria a nám sa to zakaždým zdá neuveriteľné. Tridsať minút a akoby sme boli v Tatrách. Rieky s obrovskými kameňmi, vodopády a kláštory... a ticho a niekedy aj bezľudie. Okolo Sofie sú dediny s jazerami, minerálnymi vodami a veľmi veľa stánkami s pečeným mäsom. Tie sú vlastne aj na Vitoši - kvôli tomu, že na niektoré miesta sa dá dostať aj autom. A tak na Vitoške niekedy stretnete aj dievčatá v opätkoch... prišli na kafe a fašírky s ľutenicou. Po týchto skalách ale nelozia: 
  
  
  
 Aj my sme boli na Vitoši, keď sa slávilo narodenie Jána Krstiteľa. Po bulharsky Eňovden - 24 júna - najdlhší deň roka, sviatok rastlín, mágie a liečenia. V tento deň si Ján na seba dáva kožuch, aby odišiel za snehom - a sila slnka sa začne zmenšovať.  Stretli sme tety, ktoré prišli zbierať rastliny na 77 chorôb a robili jánske vence. Hovorili o bulharskom Kristovi. A ani nevadilo, že vôbec ešte nebolo 24. júna, ale bola sobota a oni si nazbierali jánske rastliny a odložili na 77 a pol choroby. 
  
  
  
 Bankya je hádam najobľúbenejšia bulharská minerálna voda a dedinka pri Sofii. Krásne je tam cez týždeň... cez víkend je plná fašírok, že sa cez dym ani nedá vidieť a áut so sofijskými poznávacími značkami. Na vodu si vystojíme rad, lebo Sofijčania si priniesli najmenej päť desať litrových bandasiek každý jeden. 
  
  
 Všetko je tu také bulharské. Samé kaviarničky, obchodíky z papiera, ošarpaná omietka, dva svadobné sprievody a rozbité okná. Proste bulharská pohoda. 
   
  
  
  
  
  
   
  
  
 A papierový obchod, akých je v Bulharsku neúrekom. Tento je v Bankyi, ale jeden taký je aj v úplnom centre Sofie. Máte chuť na rakiju či mastiku? 
  

