
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 V Pravde opäť vyšiel recyklovaný komentár 

        
            
                                    23.7.2007
            o
            15:16
                        |
            Karma článku:
                8.95
            |
            Prečítané 
            3268-krát
                    
         
     
         
             

                 
                    Novinárka Vanda Vavrová skopírovala či upravila časti svojho komentára spred roka
                 

                Denník Pravda dnes ponúka ako svoj hlavný komentár text Vandy Vavrovej Rukojemníci štátu. Takmer doslovne sú niektoré pasáže prebraté z vlastného textu autorky, ktorý čitateľom ponúkla takmer pred rokom. Z dnešného textu:    Občan aj firmy sú už celé roky rukojemníkmi sudcov, teda štátu. Ten je zodpovedný za stav justície, darmo sa vyhovára, že súdna moc je nezávislá. Pre nerozhodnuté spory je ekonomika zaťažená roky stámiliónmi korún, matky roky čakajú na určenie výšky výživného, ľudia na vyriešenie banálneho sporu so susedom.   ...Zmeniť tento obraz sa pokúša už štvrtá vláda. Zatiaľ stále bezúspešne.  ... Minister spravodlivosti sa rozhodol zvýšiť počet mužov v talároch. Je však otázne, či táto finančne náročná iniciatíva bez iných opatrení je riešenie, ktoré zabezpečí, aby sa občan dočkal rozhodnutia v primeranom čase, ako mu to zaručuje ústava.  ...   Sudcovia sú totiž tiež len ľudia. Väčšina z nich si svoje povolanie váži. Iní však využívajú všetky výhody, ktoré im poskytuje štát. Za to jeho občanom odovzdávajú len minimum.   ...  Verejnosť tak stále zostáva rukojemníkom toho, že v talároch sú ľudia, ktorí tam nepatria. Nič na tom nezmení ani to, či vláda napumpuje do súdnictva ďalšie stámilióny.    Taláre za milióny, Vanda Vavrová, Pravda, 7.8.2006    Roky trvajúce súdne spory, milióny vyplácané sťažovateľom, ekonomika zaťažená stovkami miliónov pre nerozhodnuté spory. Aj to je obraz slovenskej justície. Na tomto stave nezmenili nič ani reformy v súdnictve, o ktoré sa pokúša už štvrtá vláda.   Minister spravodlivosti Štefan Harabin vypočul svojich bývalých kolegov a rozhodol sa zvýšiť počet mužov v talároch. Je však otázne, či je táto iniciatíva jediné riešenie, ktoré zabezpečí, aby sa občan dočkal rozhodnutia v primeranom čase, ako mu to zaručuje ústava.   ...Sudcovia sú totiž tiež len ľudia. Väčšina z nich si svoje povolanie váži a uvedomujú si, že matka žiadajúca výživné ho potrebuje teraz a nie o päť rokov. Iní však, nie je ich málo, využívajú všetky výhody, ktoré im poskytuje štát. Za to jeho občanom odovzdávajú len minimum.   ...  Verejnosť tak stále zostáva rukojemníkom toho, že v talároch sú ľudia, ktorí tam nepatria. Nič na tom nezmení ani to, či vláda napumpuje do súdnictva ďalšie stovky miliónov.    Parafrázovanie vlastných myšlienok ako doplnok k novým, nosným myšlienkam by nikomu nevadilo. Ale copy-paste a miniedit staršej veci, ktorá je obsahovo takmer totožná s komentárom spred roka je nevkusné. Noviny z definície majú ponúkať niečo nové a originálne a pri citovaní starších textov ich majú aj príkladne „historicky“ označiť.    PS Pravda mala podobný problém už v minulosti, šlo o komentáre Martina Kováčika.      Problémy s novelou tlačového zákona: Tomáš Czwitkovics z TRENDU napísal výborný článok o Maďaričovej novele tlačového zákona, a jej pochybných miestach – nejasnosti okolo práva na odpoveď či vynechaní čisto internetovej žurnalistiky (napr. aktualne.sk). Tiež je zvláštne, prečo sa nerobí komplexný zákon o vzťahu s médiami, ale len tlačový. Právo na ochranu zdroja pred súdom by tak mala len tlač, ale nie elektronické médiá.    Oprava: Ako ma upozornila Zuzana Hošalová, posledné YouTube video z môjho piatkového článku  nebolo skutočným záznamom z talkshow, ale cielenou paródiou. Ospravedlňujem sa, že som to prehliadol.    Maori, nie Mauri: Tom Nicholson si vo víkendovom SME pomýlil dve celkom rozdielne etnické skupiny. Píše opakovane o novozélandských „Mauroch.“ Ako ma upozornil Juraj Lörinc, v skutočnosti ide o Maorov, pôvodných obyvateľov Nového Zélandu. Mauri sú africkí či arabskí moslimovia žijúci na západe Stredomoria. Koniec koncov, SME ich doteraz správne rozlišovalo (pozri starší článok SME so zmienkou o Mauroch, a ešte starší  o Maoroch).   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




