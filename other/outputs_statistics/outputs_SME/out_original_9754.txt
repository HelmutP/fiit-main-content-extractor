
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Murar
                                        &gt;
                Nezaradené
                     
                 Typografia podľa Googlu 

        
            
                                    28.6.2010
            o
            8:44
                        (upravené
                28.6.2010
                o
                9:09)
                        |
            Karma článku:
                7.33
            |
            Prečítané 
            2295-krát
                    
         
     
         
             

                 
                    Jedným s problémov, s ktorým web bojuje od svojho vzniku sú limity  webovej typografie. Na rozdiel od tej klasickej sa totiž musí spoliehať  iba na obmedzený počet typov písma. Časom vzniklo niekoľko spôsobov, ako  tento nedostatok riešiť. Najnovší je od Google.
                 

                 Služba má názov Google Font Directory a jej  cieľom je rozšíriť možnosti webovej typografie pri zachovaní maximálnej  možnej miery prístupnosti a použiteľnosti webu. Časť riešení webovej  typografickej mizérie totiž fungovala na princípe nahrádzania textu  obrázkom, čo nie je v súlade s požiadavkami na sémantiku a použiteľnosť  webu. Taktiež nevyžaduje žiadne doplnkové pluginy typu Flash.   Riešenia na sto spôsobov   Podstatou problému tkvie v tom, že internetový prehliadač dokáže  zobraziť iba font, ktorý je na danom počítači nainštalovaný. Ak ho  nenájde, zobrazí text v predvolenom, čo je vo Windows Times New Roman.  Webdizajnéri si na to už zvykli, je to feature, nie bug.  Jednako sa z času na čas sa objaví niekto, kto ponúkne nejaké  riešenie.  V posledných rokoch sa veľkej obľube teší napríklad metóda sIFR. Funguje na  báze nahradzovania textu flashom. Z ďalších spomeniem Cufón,  Facelift Image Replacement (FLIR),  Typeface, Scalable Inline Image Replacement (SIIR) či  Dynamic Text Replacement (DTR).  Niektoré z nich fungujú na princípe automaticky generovaných obrázkov,  iné berú na pomoc Javascript alebo Flash.   Za všetkým hľadaj Microsoft 
   Google vlastne oprášil koncept, s ktorým svojho času prišiel  Microsoft. Jeho metóda Web Embedding Fonts Tool (WEFT)  pracuje na princípe načítania súboru s fontom spolu s webstránkou,  podobne ako sa načítavajú javacsripty či kaskádové štýly. Užívateľ si  stihane jednoduchú utilitu a pomocou nej skonvertuje font vo formáte TTF  (True Type Font) do formátu EOT (Embedded Open Type). Výsledný súbor  nahrá na server a do dokumentu vložil príslušný link. Z nejakého dôvodu  sa však tento systém príliš nepresadil, užívatelia dali prednosť iným  riešeniam.  Na rozdiel od Microsoftu to Google zariadil tak, aby  užívateľ nemusel sťahovať žiadne utility a generovať žiadne doplnkové  súbory. Na svoj server umiestnil 18 fontov, kde sú kedykoľvek  k dispozícii. Väčšinou ide o rôzne antikvy a grotesky, poteší však aj  niekoľko skriptov – mňa najmä Lobster. Všetky sú licencované niektorou z  otvorených licencií (najčastejšie SIL Open Font License), čiže ich  použitie je bezplatné.      Banálne jednoduché   Nemusíte vedieť PHP, rozumieť Flashu, ani ovládať Implementácia  týchto fontov je zarážajúco jednoduchá.  Stačí vložiť do svojho webu  odkaz na príslušný font. Nájdete ho uvedený pri každom z nich, spolu  s tabuľkou znakov a zoznamom dostupných rezov (bold, italic...).   &lt;link href=' http://fonts.googleapis.com/css?family=Cantarell'  rel='stylesheet' type='text/css'&gt;   Štýlovanie vybraných prvkov je úplne normálne – žiadne špecifické   kombinácie selektorov – vyplníte atribút font-family a je to. Funguje to   vo všetkých prehliadačoch, dokonca aj v Internet Exploreri 6.   h1 { font-family: 'Cantarell', arial, serif; }   Pre a proti 
   Rozširovanie webovej typografie so sebou  prináša aj určité riziká.  sIFR je závislý na Flashi a Javascripte, väčšina ostatných metód zase  robí z textu obrázky, čo znemožňuje vytvorenie sémanticky korektného  dokumentu a znižuje prístupnosť webu. Google tieto nedostatky  odstraňuje, no aj jemu jesto čo vytknúť. V prvom rade obmedzenú ponuku  fontov. Či ich niekedy bude viac ako osemnásť možno len hádať. Druhou  nevýhodou je nižšia kvalita zobrazeného fontu. Zvyknutí na perfektne  hladké kontúry, dokonca aj vo Photoshope, musíme rátať s tým, že oblúky  budú o čosi zubatejšie, než by sme čakali.  Navyše pri otvorení webu trvá cca sekundu, než sa priložený font  načíta. Napriek všetkému túto  iniciatívu vítam. Aj keď Google Font Directory zrejme nespôsobí žiadnu  typografickú revolúciu, s nulovými nárokmi na implementáciu i štylovanie je jeho prínos nesporný. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Murar 
                                        
                                            SOZA nechápe, že YouTube je hlavne o voľnom zdieľaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Murar 
                                        
                                            Bude Tatrabanka na Facebooku komunikovať, alebo sa iba tváriť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Murar 
                                        
                                            Pútnik do Compostely – o čom príručky nepíšu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Murar 
                                        
                                            Príliš vysoká hra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Murar 
                                        
                                            HootSuite 5: V znamení HTML5
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Murar
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Murar
            
         
        murar.blog.sme.sk (rss)
         
                                     
     
        Učiteľ, lektor, skaut do špiku kosti
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2831
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Alexander Plencner
                                     
                                                                             
                                            Privát pod Tatrami
                                     
                                                                             
                                            Slovenský skauting
                                     
                                                                             
                                            Muro.sk
                                     
                                                                             
                                            Slovenská ekumenická lesná škola
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo opúšťam Slovensko
                     
                                                         
                       Na azerbajdžanských cestách - 2
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




