

   
   
 Kapitola druhá: 
 Ako túto, (zatiaľ iba kacírsku)  Kauzálnu realitu dokazuje Galileiho symetrické kyvadlo a ako JÁRAYové excentrické kyvadlo! 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
  
  
 A. Pohyb Galileiho kyvadla v sústave, ktorá stojí nehybne v priestore.  
 (V gravitačnej sústave a = 981m/cec2, ktorá má nulovú absolútnu rýchlosť vo vesmíre.) 
  
   
 1. Udelíme kyvadlu hybnosť:  H = 1kg.1m/sec. 
 2. Následkom tejto hybnosti, kyvadlo sa vychýli so zvislej (kolmej) polohy do pravej strany kyvadla o určitý, konštantný uhol, v ktorom sa jeho kinetická energia premení na potenciálnu energiu gravitačného poľa sústavy. V skutočnosti, kyvadlo sa počas jeho vychyľovania sa z kolmej polohy, do konštantného uhla výkyvu, spomaľuje z rýchlosti 1m/sec, na 0m/sec !!! 
 3. Potenciálna energia gravitačného poľa, udelí nehybnému, vychýlenému kyvadlu opačne orientovanú kinetickú energiu, ktorá ho zrýchli a tým pádom ho vráti späť do zvislej polohy, kde nadobudne  opačne orientovanú rýchlosť 1m/sec. Táto rýchlosť umožní kyvadlu vychýliť sa do ľavej strany kyvadla o uvedený konštantný uhol, v ktorom jeho hybnosť znova zanikne a premení sa na potenciálnu energiu gravitačného poľa. A tak sa začína perpetualny (večný)  pohyb kyvadla v nehybnej gravitačnej sústave. 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Charakteristické rysy pohybu Galileiho kyvadla v gravitačnej sústave, ktorá stojí nehybne vo vesmírnom priestore, sú nasledovné: 
 a) Najväčšia rýchlosť predmetného kyvadla môže byť iba 1m/sec! Táto jeho rýchlosť je aj jeho jedinou vesmírnou rýchlosťou. 
 b) Pohyb kyvadla je vždy symetrický ku kolmej osi (y)  kyvadla. 
 c) Keby toto kyvadlo konalo eliptický pohyb, šlo by o neprirodzenú, symetrickú, matematikou neopísanú elipsu so stredom v priesečníku hlavnej a vedľajšej osi elipsy. Tento eliptický pohyb kyvadla bol by symetricky  eliptickým aj voči kozmickému časopriestoru. 
 d) Keby toto kyvadlo konalo kružnicový pohyb, šlo by o rovnomerný pohyb telesa po kružnici v tom istom strede, ako u eventuálneho symetrického eliptického pohybu kyvadla. Tento kružnicový pohyb kyvadla bol by kružnicový aj voči kozmickému časopriestoru. 
  e) Uvedené pohyby kyvadla boli by rovnaké vo všetkých smeroch predmetnej nehybnej gravitačnej sústavy. 
 f) Podľa Galilea i podľa Einsteina, tu opísaný pohyb rovinného i kónického Galileiho kyvadla prebiehal by rovnako v nehybnej, ako  aj v pohybujúcej sa zotrvačnej sústave !!! Toto tvrdenie tvotí pilier relativity pohybu. 
 g) V tom spočíva vzácna jednota Galileových a Einsteinových úvah o pohybe hmoty, presnejšie o relativite pohybu hmoty. 
 h) Tu je potrebné sucho konštatovať, že aj JÁRAYove kyvadlo by sa v nehybnej sústave pohybovalo presne tak ako Galileiho kyvadlo. Ale len v nehybnej sústave! 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
   
 B. Pohyb JÁRAYovho excentrického  kyvadla v sústave, ktorá sa pohybuje zotrvačnou rýchlosťou v = 100/msec, vo vesmíre. (V gravitačnej sústave a = 981m/cec2, ktorej zotrvačná, vesmírna rýchlosť má hodnotu v = 100/msec.) 
  
  
  
 Ide síce o Foucaltovo kyvadlo, ale z efektmi JÁRAYoho kyvadla. 
  
  
  
  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 1. Udelíme kyvadlu hybnosť H = 1kg.1m/sec, vo smere vesmírneho zotrvačného pohybu predmetnej gravitačnej sústavy. Výsledná, vesmírna rýchlosť tohto kyvadla bude mať hodnotu 101/msec, (ale iba v smere pohybu sústavy). 
 2. Následkom zdanlivej hybnosti H = 1kg.1m/sec, kyvadlo sa vychýli so zvislej (kolmej) polohy do pravej strany kyvadla o určitý, uhol, v ktorom sa jeho zdanlivá kinetická energia premení na potenciálnu energiu gravitačného poľa pohybujúcej sa zotrvačnej sústavy. V skutočnosti, kyvadlo počas jeho vychyľovania sa z kolmej polohy doprava, do konkrétneho uhla výkyvu, spomaľuje sa z vesmírnej rýchlosti 101m/sec, na vesmírnu rýchlosť 100m/sec !!! So spomaľovacou hmotnosťou. 
 3. Potenciálna energia gravitačného poľa, udelí zdanlivo nehybnému kyvadlu opačne orientovanú kinetickú energiu, ktorá ho ale nezrýchľuje, ako u Galileiho kyvadla, ale naďalej spomaľuje z vesmírnej rýchlosti 100m/sec, na vesmírnu rýchlosť 99m/sec a tým ho vráti do zvislej polohy, kde kyvadlo nadobudne vesmírnu rýchlosť 99m/sec !!!.  
 Táto 99m/sec rýchlosť kyvadla je ale menšia než je vesmírna rýchlosť zotrvačnej sústavy, takže v dobe, keď sa kyvadlo zdanlivo vychyľuje z kolmej polohy do ľavej strany, čiže keď by sa malo zdanlivo spomaľovať, ono sa v skutočnosti nespomaľuje, ale zrýchľuje z vesmírnej rýchlosti 99m/sec, najprv na rýchlosť 100m/sec, po vychýlení sa do ľavej strany a  potom sa zdanlivým návratom späť do zvislej polohy kyvadla ďalej zrýchľuje na vesmírnu rýchlosť 101m/sec. So zrýchľujúcou hmotnosťou hmotnosťou. 
 Pohyb kyvadla v pohybujúcej  zotrvačnej sústave, sa počas jeho výkyvu v smere vesmírneho pohybu sústavy, čiže z kolmej polohy kyvadla doprava a späť do kolmej polohy, vždy spomaľuje následkom čoho sa zvyšuje jeho hmotnosť, takže sa vychyľuje o väčší uhol ako Galileiho kyvadlo a počas výkyvu v opačnom smere vesmírneho pohybu sústavy, čiže z kolmej polohy doľava a späť do kolmej polohy vždy zrýchľuje, následkom čoho sa zmenšuje jeho hmotnosť, takže sa vychyľuje o menší uhol ako Galileiho kyvadlo. Následkom tejto reality JÁRAYove kyvadlo koná vo smere absolútneho pohybu sústavy, excentrický pohyb !!! 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
  
 Charakteristické rysy pohybu JÁRAYovho excentrického  kyvadla v sústave, ktorá sa pohybuje vesmírom zotrvačnou rýchlosťou  v = 100/msec, v priestore. (V gravitačnej sústave a = 981m/cec2, ktorá má v = 100/msec absolútnu rýchlosť je zotrvačného pohybu vo vesmíre) sú nasledovné: 
 a) Najväčšia rýchlosť predmetného kyvadla v uvedenej zotrvačnej sústave môže byť iba 101m/sec!  
 b) Pohyb kyvadla nie je symetrický ku kolmej osi (y)  kyvadla. Je excentrický! 
 c) Keby JÁRAYové kyvadlo konalo eliptický pohyb v zotrvačnej sústave, šlo by o prirodzenú, matematikou opísanú, excentrickú elipsu so stredom v  jednom ohnisku elipsy. Tento eliptický pohyb kyvadla nebol by eliptickým aj voči kozmickému časopriestoru. Išlo by o cikloidný vesmírny pohyb kyvadla. 
 d) Keby JÁRAYové kyvadlo,  konalo začiatočný kružnicový pohyb v zotrvačnej sústave, keby sa aplikovalo ako  kónické kyvadlo, v pohybujúcej sa sústave, nešlo by o rovnomerný pohyb telesa po kružnici, ale o prechod kružnicového pohybu na pohyb excentricky eliptický, ktorej smer hlavnej osi, bol by zhodný so smerom vesmírneho pohybu zotrvačnej sústavy.  
 Tento pôvodne kružnicový pohyb kyvadla, nebol by bol kružnicovým ani voči kozmickému časopriestoru. Išlo by o cikloidný vesmírny pohyb kyvadla. 
 e) Uvedený pohyb rovinného  JÁRAYovho kyvadla, nebol  by rovnaký vo všetkých smeroch predmetnej zotrvačnej gravitačnej sústavy. Vo smere kolmom na smer absolútneho pohybu zotrvačnej sústavy vesmírom, uvedené efekty JÁRAYovho rovinného kyvadla by sa nevykazovali.  
 Vo smere kolmom na smer zotrvačnej sústavy JÁRAYové kyvadlo by sa pretransformovalo na Gelileiho kyvadlo. 
 A práve tu opísanými efektmi rovinného i kónického JÁRAYovho excentrického kyvadla dá sa jednoznačne identifikovať smer pohybu zotrvačnej sústavy vesmírom, napriek tomu, že Einsteinove teórie relativity pohybu, identifikáciu smeru zotrvačného pohybu vesmírom experimentálne nepripúšťajú, priam ho zakazujú. 
  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
  
 Záverečné konštatovanie: Gelileiho rovinné i kónické kyvadlo ignoruje zákon Kauzality, pričom akceptuje anti kauzálny Newtonov zákon Akcie a reakcie, následkom čoho, akceptori pohybu tohto kyvadla konštatujú, že hmotnosť zrýchľujúceho sa, i hmotnosť spomaľujúceho sa kyvadla je vždy rovnako veľká a že  aj sprievodné tlaky (vo väzbe kyvadla) pri jeho spomaľovaní, ako aj pri jeho zrýchľovaní sa sú pre rovnaké uhly, vždy rovnako veľké. Niesú !!!  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 JÁRAYho rovinné i kónické kyvadlo akceptuje zákon Kauzality, pričom ignoruje anti kauzálny Newtonov zákon Akcie a reakcie, následkom čoho konštatuje, že hmotnosť zrýchľujúceho sa a hmotnosť spomaľujúceho sa kyvadla nie sú nikdy rovnako veľké a že  aj sprievodné tlaky (vo väzbe kyvadla) pri jeho spomaľovaní, ako aj pri jeho zrýchľovaní sa sú pre rovnaké  uhly vždy rôzne veľké.  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Excentricita pohybu rovinného JÁRAYovho kvadla pohybujúceho sa vo smere zotrvačného pohybu sústavy, ako aj deformácia pôvodne kružnicového pohybu kónického Jarayovho kyvadla z kružnicovej dráhy na dráhu excentricky eliptickú, ktorej hlavná os akceptuje vesmírny smer zotrvačného pohybu sústavy, experimentálne dokazujú rozdiel medzi sústavou ktorá stojí nehybne v priestore a sústavou, ktorá sa vesmírom pohybuje nenulovou zotrvačnou rýchlosťou. 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Excentricita pohybu rovinného JÁRAYovho kyvadla pohybujúceho sa vo smere zotrvačného pohybu sústavy, ako aj deformácia pôvodne kružnicového pohybu kónického JÁRAYovho kyvadla z kružnicovej dráhy na dráhu excentricky eliptickú, ktorej hlavná os akceptuje vesmírny smer zotrvačného pohybu sústavy, experimentálne vyvracia herézy (bludy) o relativite pohybu matérie. 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Excentricita pohybu rovinného JÁRAYovho kyvadla pohybujúceho sa vo smere zotrvačného pohybu sústavy, ako aj deformácia pôvodne kružnicového pohybu kónického JÁRAYovho kyvadla z kružnicovej dráhy na dráhu excentricky eliptickú, ktorej hlavná os akceptuje vesmírny smer zotrvačného pohybu sústavy, exaktne spochybňujú pochabé  výroky Albereta Einsteina, ktoré neprávom povyšujú objaviteľa tejto excentricity (Alexandera JÁRAYa ) na Boha. 
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Experimentálne preverenie (potvrdenie) tu opísaných efektov  JÁRAYho rovinného i kónického kyvadla, ktorých základom je konštatovanie, že spomaľujúca sa hmotnosť telies je vždy a za každých okolnosti a na viac aj v celom materiálnom časopriestore väčšia od hmotnosti zrýchľujúcich sa telies, ale aj neplatnosť zákona zachovania hmoty počas dynamických silových interakcii, dá sa dosiahnuť tým, že do ramena (do väzby rovinného, či kónického kyvadla) sa zapojí citlivá (piezoelektrická) aparatúra, ktorá vie identifikovať aj pomerne malú zmenu tlaku (ťahu) v ramene kyvadla pri rovnakých uhloch výkyvov kyvadla doprava a späť, ako aj doľava a späť, alebo malú zmenu tlaku (ťahu) v ramene kyvadla pri jeho usmernenom, excentrickom, eliptickom pohybe v podobe JÁRAYového kónického kyvadla v pohybujúcej sa v zotrvačnej sústave, ktorou  je aj naša planéta Zem. 
 Touto experimentálnou previerkou pohybu kyvadla na pohybujúcom sa povrchu Zeme, skončila by sa éra anti kauzálnej relativity pohybu hmoty a zrodila by nová éra, éra kauzálnej fyziky, fyziky absolútneho pohybu hmoty. 
 Od Ústavu Experimentálnej Fyziky SAV v Košiciach, očakávam pomoc pri experimentálnom  potvrdení (overení) tu opísanej, dosiaľ nepoznanej, ale objektívne existujúcej reality materiálnej prírody, tu opísaných efektov JÁRAYovho excentrického kyvadla a to na slávu Božiu, i na slávu národa Slovenského, ako aj na slávu všetkých občanov SR, ale aj v prospech celého ľudstva.  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay-------  
 Tu kliknúť na prvý diel: 
 http://jaray.blog.sme.sk/clanok.asp?cl=206420&amp;bk=63950 
  
   
 Amen. 
   

