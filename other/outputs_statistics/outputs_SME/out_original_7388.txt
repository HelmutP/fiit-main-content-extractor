
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Bórik
                                        &gt;
                veda
                     
                 Rýchly reaktor Monju je po 14 rokoch opäť v prevádzke 

        
            
                                    27.5.2010
            o
            15:15
                        (upravené
                27.5.2010
                o
                16:21)
                        |
            Karma článku:
                8.23
            |
            Prečítané 
            2843-krát
                    
         
     
         
             

                 
                    Japonský reaktor s výkonom 280MW bol v roku 1995 odstavený kvôli nebezpečnej havárii. Uniklo chladiace médium - tekutý sodík. Monju bol v riadnej prevádzke len jeden rok. Chladenie je v týchto reaktoroch najväčším problémom. Naopak výhodou je efektívnejšie narábanie s objemom paliva a menej odpadu. Oplatí sa riskovať? V článku nájdete aj podrobný opis havárie reaktora.
                 

                 
Jadrová elektráreň MonjuIAEA
   V prírode sa nachádza urán v rôznych rudách, najčastejšie však v  Uraninite (Smolinec). Podiel izotopov je 99,276% ,U238 0,718%, U235 a  0,004% U234. Využitie v klasických reaktoroch má len U235, ktorého obsah  v zmesi izotopov sa zvyšuje obohacovaním až na 2-4 %. Pri vyhoretom  palive sa jeho koncentrácia opäť znižuje a vzniká aj plutónium 239. Urán 238, ktorý sa taktiež nachádza v palivových kazetách ostáva  v približne rovnakej koncentrácii a tvorí tak podstatnú časť jadrového odpadu.   Pre  budúcnosť jadrovej energetiky a efektívneho využitia vyhoretého paliva sú  preto možnosťou práve rýchle množivé reaktory. Tieto dokážu spracovávať  aj urán 238, ktorý sa premieňa na plutónium 239. Problémom reaktorov s plutóniom je ich vysoká toxicita.      Alternatívou pre rýchle ale aj klasické reaktory využívané primárne na výrobu elektrickej energie je prepracované palivo tzv. MOX. Skladá sa predovšetkým zo zmesi oxidov plutónia a uránu. Väčšina súčasných klasických reaktorov dokáže po rekonštrukcii aktívnej zóny použiť palivo MOX, aj tie Slovenské. Palivo MOX bude v budúcnosti určite významnou strategickou surovinou. V Japonsku (a iných krajinách) sa využíva bežne.   Rýchly reaktor sa však nedá uchladiť vodou. Na chladenie primárneho okruhu sa preto využíva najmä sodík, ktorý je nad 98°C v tekutom stave a má vynikajúcu tepelnú vodivosť. Teplota na výstupe z aktívnej zóny sa pohybuje okolo 550°C a pri potenciálnom úniku nemôže zovrieť ako voda, pretože bod varu má až pri 883°C. Tak pracuje aj reaktor v Japonskom Monju.   Stavba Monju začala v roku 1985 a prvá reťazová reakcia bola spustená v apríli 1994. Na elektrickú sieť bola pripojená 29. augusta 1995. Využívanym palivom je MOX a elektráreň má tri chladiace okruhy z toho prvé dva sodíkové a tretí vodný.      Reaktor bežal len na 40% výkonu a už 08. decembra 1995 ho zasiahla vážna nehoda. Táto bola označená prvým stupňom medzinárodnej stupnice INES, ktorá má až sedem stupňov. Dôvodom na tak nízke označenie bolo predovšetkým to, že k úniku chladiaceho média došlo na sekundárnom okruhu a neunikli rádioaktívne látky.      O 19:47 sa ozval v blokovej dozorni alarm, ktorý upozorňoval na vysokú teplotu v oblasti tepelného senzoru v sekundárnom chladiacom okruhu. O minútu neskôr už ďalší senzor upozorňoval na reálny únik tekutého sodíka do vonkajšieho prostredia. 19:50 posiela operátor na kontrolu jeden tým, ktorý sa však k miestu úniku sodíka nemôže dostať kvôli nebezpečnému bielemu dymu. 19:52 je únik potvrdený. Operátor však čaká až do 21:20, kedy manuálne odstavuje reaktor. Primárny okruh sa postupne ochladzuje a sekundárny okruh začínajú od 22:40 odčerpávať, toto je ukončené o 0:15. Operátor svojim oneskoreným postupom umožnil uvoľnenie veľkej masy tekutého sodíka. Je však možné, že ho chcel ešte využiť na dochladenie respektíve udržanie cirukulácie systému. Pri znížení teploty totiž sodík ochladne a stuhne. Pre zabezpečenie prietoku sa preto do okruhu môžu vstretkovať iné látky, ktoré so sodíkom nereagujú.   Uniknutý sodík reaguje so vzduchom v miestnosti a následne vzniká požiar, ktorý postupuje cez ventilačný systém a poškodzuje miestnosti na vyššom poschodí. Niektoré kovové nosníky sa ohli, teplota  tam bola od 700 do 750 °C. Tekutý sodík naštastie neprepálil betón a nedostal sa do nižších častí, kde by mohol spôsobiť aj výbuch. Podľa odhadov uniklo až 640 ton chladiaceho média.      Príčinou úniku bolo uvoľnenie spojov na tepelnom tesnení sekundárneho okruhu, čo bolo zapríčinené vibráciami vzniknutými pri toku chladiva. Časti vnútorného testenia prešli vnútrom sekundárneho okruhu ešte 160 metrov ďalej, kde sa zakliesnili v armatúre. Tieto boli uvoľnené až štyri mesiace po odstávke. Pri kontrole potrubia sa zistilo, že spoj, ktorý spôsobil únik bol poškodený už skôr (takmer šesť mesiacov predtým), havária sa tak dala predpokladať.   V roku 2000 rozhodla Japonská agentúra pre atómovú energiu o opätovnom spustení Monju a to v roku 2010. Vo februári dostala elektráreň oficiálne "ok" a šiesteho mája bol reaktor reštartovaný. Testovacia prevádzka s výkonom 40% bude prebiehať až do roku 2013, kedy bude opätovne pripojený k elektrickej sieti.   Počas štrnástich rokov, kedy bol Monju odstavený, však došlo k zostarnutiu techniky, armatúr a elektroniky a preto je otázne či bude vôbec schopný reálnej prevádzky a najmä či nespôsobí ďalšiu nehodu napríklad na primárnom okruhu.   Únik bol zaznamenaný aj pracovníkmi elektrárne. Toto video bolo pôvodne tajné, no po uverejnení oficiálnych správ bolo po roku získané médiami a následne uverejnené.          Pozn. zábery sú až od prvej minúty na videu.   CNIC: Restarting Monju - Like Playing Russian Roulette   Scienceworld: V Japonsku znovu uvedli do provozu rychlý reaktor 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Kandidujem pretože politici ma denne dvíhajú zo stoličky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Živnostník so zápalkami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Režim žije aj po 45 rokoch, vďaka nasledovníkom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Mesto Martin hľadalo riaditeľa kultúry, bol som na výberovom konaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Ficova olympiáda spôsobí deštrukciu domácej ekonomiky.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Bórik
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Bórik
            
         
        borik.blog.sme.sk (rss)
         
                                     
     
         Zaoberám sa kreatívnym marketingom, PR, procesným manažmentom a japonskou filozofiou Kaizen. Zakladateľ webu s dobrými správami z Turca. 


 
Šéfredaktor 
Správca metra 
 



 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    150
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4393
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            technika
                        
                     
                                     
                        
                            marketing
                        
                     
                                     
                        
                            komentáre
                        
                     
                                     
                        
                            sci-fi ?
                        
                     
                                     
                        
                            svet
                        
                     
                                     
                        
                            právo
                        
                     
                                     
                        
                            veda
                        
                     
                                     
                        
                            všeobecne
                        
                     
                                     
                        
                            súkromné
                        
                     
                                     
                        
                            zakázané
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ústavné právo Slovenskej republiky
                                     
                                                                             
                                            Projektový manažment
                                     
                                                                             
                                            Wikinomie
                                     
                                                                             
                                            Psychológia pre manažérov a podnikateľov
                                     
                                                                             
                                            Psychologie lidské odolnosti
                                     
                                                                             
                                            Psychologie osobnosti
                                     
                                                                             
                                            Forenzná kriminalistika
                                     
                                                                             
                                            Personálny manažment
                                     
                                                                             
                                            Kaizen (Masaaki Imai)
                                     
                                                                             
                                            Anton Heretik - Forenzná psychológia
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Clint Mansell
                                     
                                                                             
                                            Rob Dougan
                                     
                                                                             
                                            Ennio Morricone
                                     
                                                                             
                                            Jimi Hendrix
                                     
                                                                             
                                            Leningrad
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Wired
                                     
                                                                             
                                            Stratégie online
                                     
                                                                             
                                            eTREND
                                     
                                                                             
                                            Popular Science
                                     
                                                                             
                                            ScienceBlogs
                                     
                                                                             
                                            Nicola Tesla Society
                                     
                                                                             
                                            Archív internetu
                                     
                                                                             
                                            Scienceworld
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako mravec zvíťazil nad Gorilou
                     
                                                         
                       Ako si pripraviť projekt na "eurofondy" vlastnými silami (1.časť)
                     
                                                         
                       Otvorenie letnej turistickej sezóny.
                     
                                                         
                       Vstup do Petržalky má byť pompézny. Pozrime sa, aká bude realita…
                     
                                                         
                       Prečo šváby nemajú kolesá
                     
                                                         
                       Strana TIP - bohatá a neznáma.
                     
                                                         
                       Do Moskvy vlakom? Prečo nie!
                     
                                                         
                       Stredovek 21. storočia vs. Róbert Bezák
                     
                                                         
                       Blúdivý nerv žirafy
                     
                                                         
                       Slovenská pošta podporuje kvalitné nemecké výrobky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




