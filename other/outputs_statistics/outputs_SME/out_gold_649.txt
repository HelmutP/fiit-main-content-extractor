

Ceny ropy prekonávajú rekordy takmer neustále a jej súčasná cena je viac ako 4-násobne vyššia ako v roku 2003. Stále viac prognóz predpovedá v blízkej budúcnosti ceny ropy niekde medzi 140 až 200 $ za barel ropy a realita ropného zlomu sa konečne začína odrážať aj v prognózach finančných trhov (pozri tu a tu).  Vysoká cena ropy na jednej strane vytvára podmienky pre rýchlejšie zavedenie alternatívnych technológií, na strane druhej v súčasnosti nepoznáme takú technológiu, ktorá by dokázala nahradiť ropu v takom množstve, ako ju dnes spotrebovávame. Asi 98 % dopravy závisí na jej prísune. Denne vzlietne asi 80 000 komerčných lietadiel a počet áut sa o pár rokov priblíži k jednej miliarde. Zatiaľ čo výroba elektromobilov sa už pomaly rozbieha, nahradenie leteckej, ale aj lodnej dopravy je v nedohľadne. Kto si stále myslí, že rastúce ceny ropy nie sú problém, mal by vedieť, že bez nej sa nezaobíde ani moderné poľnohospodárstvo a odhaduje sa, že bez umelých hnojív vyrábaných z ropy by na Zemi bolo najmenej o 2 miliardy ľudí menej. Už dnes sa rýchly nárast cien umelých hnojív podpísal na dramaticky rastúcich cenách potravín (spolu s pestovaním plodín pre biopalivá, rastúcim dopytom po mäse v Indii a Číne, ale aj suchom v Austrálii). 
 
Cena ropy nepriamo ovplyvňuje aj cenu alternatívnych zdrojov energie, pretože ich inštalácia je viac alebo menej spojená s dopravou. Nedávno bol napríklad zastavený projekt výstavby veterných turbín v hodnote 300 mil. $ v Škótsku (Nate Hugens). 
 
 
Vrátim sa však k pôvodnej myšlienke článku. Tak, ako ropný zlom môže byť motívom pre rozvoj a využívanie obnoviteľných zdrojov energií, ktoré znižujú emisie CO2, tak môže podporovať aj rozvoj takých technológií, ktoré problém klimatickej zmeny zhoršujú (napr. spaľovanie uhlia, technológie coal-to-liquid, gas-to-liquid, alebo výroba ropy z nekonvenčných zásob – ropné bridlice a ropné piesky – všetky tieto technológie majú nízku energetickú návratnosť a sú nepriaznivejšie pre životné prostredie ako súčasné fosílne palivá). 
 
 
Spaľovanie uhlia (čierneho alebo hnedého) je najväčším zdrojom emisií CO2 na jednotku vyrobenej energie (EIA, Wikipédia). Pre porovnanie, 1 kWh z uhlia = 890 g CO2 a 1 kWh zo zemného plynu = 600 g CO2. K „renesancii“ uhlia však nedochádza len v Japonsku, ale napríklad aj v Kórei, Austrálii alebo Číne. Uhlie je skrátka takmer všade a je ho veľa....pozri celý článok v The New York Times a nasledujúce obrázky... 
 
 

 
 
 
 
 
 
 
 

 

 
Ťažba uhlia v North Antelope Rochelle Mine – je to najväčšia a najproduktívnejšia baňa s severnej Amerike – denná produkcia uhlia naplní 5900 (!) vlakových vagónov. 
 
 
 
 
 

 
 
   
 
 
A takto sa uhlie dopravuje na západe USA.... 
 
 
  
 
 
Takto to vyzerá v Austrálii... 
 
 
 
 
 
A takto v číne... 
 
 
Obrázky boli prebraté z prezentácie Davida Rutledgeho Hubbert's peak, the Coal Question and Climate Change (odporúčam pozrieť celú prezentáciu) z California Institute of Technology. 
 


