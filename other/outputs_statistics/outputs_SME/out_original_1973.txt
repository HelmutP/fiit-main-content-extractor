
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jožo Ivanič
                                        &gt;
                Súkromné
                     
                 Príbeh o tom ako Slovenská sporiteľňa pohŕda občanmi a zákonmi 

        
            
                                    1.4.2010
            o
            8:00
                        (upravené
                1.4.2010
                o
                23:18)
                        |
            Karma článku:
                10.83
            |
            Prečítané 
            3098-krát
                    
         
     
         
             

                 
                    Stalo sa našim už vari hádam aj folklórom porušovať stavebné zákony, pravidlá a všeobecné etické normy, keď sa jedná o to postaviť čosi-kdesi niečo. Dokonca som počul, že za to môžu neefektívne štátne úrady a že je oveľa jednoduchšie postaviť niečo načierno a dodatočne to legalizovať, ako vopred absolvovať tortúru vedúcu k stavebnému povoleniu. U nás totiž údajne platí, že "keď už to raz stojí", tak to úradníci "nejak schvália" (príbehom samým o sebe by mohli byť billboardy načierno vybudované na cudzích, väčšinou mestských pozemkoch po celej Bratislave). Toto je príbeh o Slovenskej sporiteľni a jej bez stavebných (a všakovakých iných - napr. hygiena) povolení vybudovanej svetelnej reklame.
                 

                 
jivanic + http://comps.fotosearch.com/comp/IMZ/IMZ005/man-insomnia_~pbu0111.jpg 
   Čosi-kdesi pár rokov dozadu sa na rozhraní bratislavského Nového Mesta a Ružinova vybudoval veľký obytný komplex (nazvaný honosne - KOLOSEO). Čoskoro nato hneď KOLOSEU cez ulicu postavili novú centrálu Slovenskej sporiteľne.  Na tom by v zásade nebolo nič zlé, komu by už len mohla vadiť banka?   Pár mesiacov bolo spolužitie v princípe idylické, až kým sa marketing banky nerozhodol využiť priestory novej budovy na veľkoplošnú reklamu (no nech) a najmä na strechu nenainštalovali obrovské svietiace panely s logom banky.   Budova banky je, bohužiaľ, o dosť nižšia ako KOLOSEO. Takže ich strechu majú horné (9 a viac) poschodia KOLOSEA ako na dlani. Spolu s jej reklamou, ktorá svieti doďaleka. Aby ste si to vedeli predstaviť, budem to ilustrovať na príklade, ktorý všetci poznáme - spln Mesiaca. Viete si predstaviť Mesiac v splne, kedy je naozaj jasný a veľký? Zhruba tak svieti reklama SLSP mne do izby. Modrým svetlom. Pri troche snahy sa dá pri tom čítať kniha. No a náš byt je na druhej strane komplexu, vzdušnou čiarou asi 80 metrov od reklamy. Byty, ktoré sú najbližšie sú od reklamy zhruba 20-30 metrov. Skrátka také malé domáce solárko.   Prirodzene, že sa to nám - obyvateľom nepáčilo a začali sme zisťovat, čo sa s tým dá robiť. No a okrem iných vecí sme zistili, že Slovenská sporiteľňa na túto reklamu VÓBEC NEMÁ POVOLENIE! Miestny úrad ju dokonca údajne vyzval, aby to ihneď vypli, ale z toho si tam evidentne hlavu nerobia.   SLSP, samozrejme, požiadala o dodatočné schválenie, ktoré momentálne prebieha a proti ktorému, samozrejme brojíme. Oslovili sme všetky kompetentné inštitúcie, ale chyba lávky - proces vyhodnocovania žiadosti predsalen chvíľku trvá.   A SLSP si zatiaľ veselo svieti ďalej. Áno, hrozí im pokuta 13000 EUR - zhruba toľko zaplatia za jeden reklamný spot v primetime niektorej zo slovenských TV staníc. A ja sa pýtam - príde to iba mne choré? Ako je možné, že v tomto údajne právnom štáte neexistuje žiaden spôsob ako prinútiť porušovateľa predpisov neporušovať ich minimálne do právoplatného rozhodnutia? A ak teraz niektorý z úradov rozhodne, že to má SLSP vypnúť, oni sa odvolajú a aj tak to nevypnú, tak čo? Dostanú ďalšiu 13000 EUR pokutu?   Niečo zhnité je v štáte dánskom.       reportáž TA3 - http://www.ta3.com/sk/reportaze/148455_sporitelna-rusi-spanok     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jožo Ivanič 
                                        
                                            Ing. Marián Vereš: Volebné sľuby ad absurdum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jožo Ivanič 
                                        
                                            Záhada s cigánskou reklamou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jožo Ivanič 
                                        
                                            Tak sa facebooku zase raz niečo podarilo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jožo Ivanič 
                                        
                                            Prečo kašlem na eurovoľby 2009
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jožo Ivanič 
                                        
                                            Kto do pekla je Lucius Malfoy?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jožo Ivanič
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jožo Ivanič
            
         
        ivanic.blog.sme.sk (rss)
         
                                     
     
        chlapík, ktorého možno prehliadnete na ulici, ale to je už váš problém :-)
v diskusiach sa vyskytujem pod nickom meriu
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    26
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1669
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Príbehy (bez pointy) mojej žen
                        
                     
                                     
                        
                            Rozprávky pre Kuba
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vojenská rozviedka – Greatest hits, 1. časť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




