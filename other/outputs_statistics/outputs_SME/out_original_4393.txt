
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Turanský
                                        &gt;
                Brazília
                     
                 Z blata do kaluže 

        
            
                                    12.4.2010
            o
            2:40
                        (upravené
                12.4.2010
                o
                2:51)
                        |
            Karma článku:
                6.41
            |
            Prečítané 
            1353-krát
                    
         
     
         
             

                 
                    Raz som vo svojom článku „Podľa skutočnej udalosti...“ mierne nadnesene opísal život vo svojich hosťovských rodinách a ku koncu článku som sa vyjadril, že by som len veľmi nerád menil a odchádzal do tretej. Moje prianie bohužiaľ vypočuté nebolo.
                 

                     Môj prechod do novej rodiny sprevádzalo hneď niekoľko šokov. Ten najväčší bol asi, že som sa dozvedel o tom dozvedel ani nie 24 hodín dopredu. Jednoducho si ma jedného pekného večera posadili a oznámili mi, že idem meniť rodinu. Okamžite ma napadla otázka, prečo mi to hovoria až teraz, tak som sa opýtal, či to vedeli aj skôr. Dostal som odpoveď, ktorá ma nesmierne prekvapila. Vraj mi to nepovedali skôr práve preto, aby som nemal čas vymýšľať a vybavovať „nezmenu rodiny“. Milé.   V mojej súčasnej rodine mám štyroch súrodencov. Troch zdivočených bratov vo vekovom rozmedzí 10-15 a jednu pubertálnu sestru, ktorá očividne dáva najavo, že nie je spokojná so mnou v ich domácnosti. Dôvod je k tomu veľmi jednoduchý. Je tu so mnou ďašia výmenná študentka, ktorá tu bola predo mnou a v milosti je tu práve ona. Som tu ako rušivý element.   Štyri deti, dvaja dospelí, dvaja výmenní študenti. Dom je malý. Spím na zemi, na matraci, v izbe so súrodencom. Izba je malá. Doslova si cez noc dýchame na krk.  Keď som tu bol prvú noc tesne pred spánkom som si položil otázku : „Boha, kde mám posteľ?“, teraz sa pýtam už len : „Boha, kde mám matrac?“. Mohlo by to byť však aj horšie, napríklad mohol by som sa pýtať : „Boha, kde je miesto, aby som si tam položil matrac?“. Mimochodom, včera mi niekam zmizol vankúš...   Som v Brazílii na výmennom pobyte na desať mesiacov, to znamená, že veci nemám práve málo. Štyri deti, dvaja dospelí, dvaja výmenní študenti. Miesta je málo. Nemám skriňu, poličky, miesto na osobné veci. Iba jeden provizórny regál, ktorý vyzerá ako požičaný z oddelenia nápojov v Tescu. Všetky ostatné osobné veci ako fotky, knihy, časopisy a iné drobnosti, ktoré mi to zpríjemňovali, sú zamknuté v kufri. Bohužiaľ, nie je miesto.   Veľa ľudí, malý dom = veľký neporiadok. Nikto ho neopratuje. Prečo by aj ? Aj tak sa znovu zašpiní. Preto ma v kúpeľni pri sprchovaní napína, keď vidím pavučiny a očúranú záchodovú dosku.   Do školy chodím v uniforme. Tá sa väčšinou po týždne používanie zašpiní. Treba ju oprať. Priznám sa, nikdy som to nerobil. Už som sa naučil. Nemal som na výber. Veľa ľudí, veľa oblečenia, pre jedného človeka nezvládnuteľné. Každý si perie sám.   Rodina je silne veriaca. Musím sa pred jedlom modliť, pochytať sa s ľudmi okolo stola za ruky a povedať niekomu niečo pekné. Nikdy neviem, čo by to malo byť. Cudzí ľudia, ktorí porušili pravidlá výmenného programu Rotary klubu (v jednej domácnosti nemôžu byť dvaja výmenní študenti a mám nárok na vlastnú posteľ). Pekné je snáď len to, že o mesiac a pár dní budem doma, ale o to sa oni nijako nepričinili.   Čo robiť ?...Nebudem s tým robiť nič. Zostáva mi dokonca zopár dní. Nebudem už robiť cirkus a žiadať o výmenu rodiny. Aj tak by trvalo dva týždne, kým by našli nejakú, čo by ma prijala.   Pokiaľ mám počas mojej výmeny akýkoľvek problém, mám určeného človeka z miestneho Rotary klubu (counselora), ktorý mi má pomôcť ho vyriešiť. Takže mal by som mu zavolať a povedať mu svoje problémy ohľadom rodiny a požiadať o náhradnú. Raz počas pobytu o to môžem požiadať. Prečo to ale neurobím ? Pretože bývam v rodine svojho counselora.   Tak idem si ja teda pohľadať matrac a vankúš a ľahnúť si, snívať si o domove...Sťažoval som sa na rodinu predtým, nepáčilo sa mi ich správanie, ale mal som pohodlie. Tu nemám splnené ani základné potreby. Doplatil som na staré známe „Keď sa cítiš zle, pamätaj, že vždy môže byť aj horšie.“ Prešiel som z blata do kaluže...Nie, z blata do žumpy...a topím sa...Mrzelo by ma, keby mi to malo pokaziť celkový dojem z môjho pobytu v tejto krajine, aj keď to teraz viem len veľmi ťažko posúdiť. Snáď sa tak nestane...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Nie všetko "značkové" je naozaj "značkové" alebo o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Posledný večer
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Amazónia - Tajomná a očarujúca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Komunisti ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Spomínam...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Turanský
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Turanský
            
         
        tomasturansky.blog.sme.sk (rss)
         
                                     
     
        Som študent, bývalý výmenný, ktorý si absolvoval rok v Brazílii a teraz si zvyká znovu na realitu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1528
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Brazília
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Andrzej Sapkowski - Boží Bojovníci
                                     
                                                                             
                                            Roald Dahl - Neuveriteľné Príbehy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Led Zeppelin
                                     
                                                                             
                                            Sex Pistols
                                     
                                                                             
                                            Ramones
                                     
                                                                             
                                            Rádio Expres
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




