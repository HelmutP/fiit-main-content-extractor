

 Nie každý  človek túto nedeľu venuje meditácii či modlitbe. Niekto pracuje v záhrade, iní sa vybrali užiť si prírodu pri športovej aktivite, no sú aj takí, čo musia byť v práci.  Všade sa však dá zamerať svoje myšlienky na dobro a lásku a nedá sa nevšimnúť si obrovskú silu, ktorá k nám prúdi z prebúdzajúcej sa zeme. 
 U nás palmové ratolesti v kostoloch nahrádzajú bahniatka, ktoré sa posviacajú. Bolo zvykom po omši roznášať ich po dedine do každého domu. Robili to slobodné dievčatá, niekde aj deti a dostávali za to drobné mince. Z takýchto prútikov sa nosil kúsok aj na hroby zosnulých ku krížom. Okrem toho si z nich v každej domácnosti zastrčili za hradu, alebo za zrkadlo, či za obrázok s Ježišom, čo malo dom ochraňovať pred bleskom. 
 V niektorých končinách Slovenska donedávna nosili dievky na Kvetnú nedeľu leto, pričom spievali jarné piesne. Bol to vŕbový, alebo brezový konár ozdobený stuhami a maľovanými vyfúknutými vajíčkami. Na mnohých miestach bolo zvykom uviť veniec z jarnej zelene, bola to kolektívna obradná činnosť, ktorá sa nám v podobe najrozmanitejšich vencov zachovala, veď gazdinky venujú veľkonočnej výzdobe svojich domovov veľkú pozornosť. 

