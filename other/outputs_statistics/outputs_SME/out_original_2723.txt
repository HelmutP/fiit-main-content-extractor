
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Kalmár
                                        &gt;
                Komunálne veci
                     
                 Prečo dopravný podnik tají informácie o novej linke  do Rajky? 

        
            
                                    16.3.2010
            o
            14:35
                        |
            Karma článku:
                9.91
            |
            Prečítané 
            3384-krát
                    
         
     
         
             

                 
                    Už takmer rok sa pripravuje spustenie novej medzinárodnej autobusovej linky z Bratislavy do   maďarskej Rajky. Hlavné mesto na jej zriadenie uspelo v žiadosti o dotácíu z fondov EÚ. Keď sa však dopravného podniku opýtate na informácie k pripravovanej autobusovej linke pravdepodobne neuspejete.
                 

                 
Ilustračné fotoFoto: autor
      Podľa pôvodných informácii od predstaviteľov hlavného mesta autobusy na tejto linke mali začať premávať niekedy v septembri 2009. Do dnes sa tak však ešte nestalo. Prečo? A kedy sa to zmení? Čo bude stáť lístok na tejto linke? Tak práve tieto informácie som sa chcel dozvedieť od Dopravného podniku Bratislava a.s. Od toho dopravného podniku, ktorý prevádzkuje v Bratislave mestskú hromadnú dopravu.   Môj postup bol štandardný a to podľa zákona o slobodnom prístupe k informáciám. Dopravný podnik si ho ale vykladá pravdepodobne po svojom. Tvári sa, že nemusí sprístupniť skoro nič. Zákon o slobodnom prístupe k informáciám obmedzuje povinnosti firiem zriadených štátom, vyššími územnými celkami, alebo obcami na povinnosť sprístupniť iba informácie o hospodárení s verejnými prostriedkami, nakladaní s majetkom štátu majetkom vyššieho územného celku alebo majetkom obce, o životnom prostredí, o úlohách alebo odborných službách týkajúcich sa životného prostredia a o obsahu, plnení a činnostiach vykonávaných na základe uzatvorenej zmluvy.   Nesprístupníme!    Dopravný podnik však vôbec netrápilo , že zriadenie autobusovej linky Bratislava - Rajka má byť z 85% financované zo zdrojov Európskeho fondu regionálneho rozvoja a z 10% z prostriedkov štátneho rozpočtu SR. Prevádzka dopravného podniku je každoročne dotovaná z rozpočtu Hlavného mesta SR Bratislava. Preto podľa zákona máme jednoznačne právo vedieť minimálne informácie o tarife na tejto pripravovanej linke. Tarifa udáva cenu prepravných služieb, teda cenu lístkov, ktoré budú príjmom dopravného podniku. Tarifa je preto jednou zo základných veličín vyplývajúcich na budúcu ziskovosť, alebo stratovosť prevádzky tejto linky.   Taktiež minimálne obyvateľov mestských častí Jarovce, Rusovce a Čunovo, cez ktoré má táto linka premávať, oprávnene zaujíma či služby tejto linky budú môcť využívať aj oni. Keďže sa v uplynulom čase, medzi zverejnenými informáciami o tejto pripravovanej linke objavili aj také, ktoré hovorili o hrozbe nemožnosti vnútroštátnej prepravy na tejto linke.  Dopravný podnik Bratislavy si to však nemyslí a informácie tají. Prečo asi?   Prečo dopravný podnik tají informácie?   Nuž asi preto, že zavedenie tejto linky je administratívne zatiaľ stále nedotiahnutué. Situácia v momente písania tohto člnku bola taká, že nikto nevedel, kedy  bude spustená premávka na tejto linke. Magistrát hlavného mesta SR Bratislava zatiaľ nemal s Ministerstvom výstavby a regionálneho rozvoja SR podpísanú zmluvu o poskytnutí dotácie na zriadenie tejto linky. Verejné obstarávanie autobusov, ktoré majú premávať na tejto linke nebolo uzavreté a ani dopravný podnik zatiaľ nedisponoval potrebnou licenciou na prevádzku medzinárodnej autobusovej linky do Rajky.   Zaujímavá tarifa   Mnohé však naznačujú dokumenty, ktoré Dopravný podnik Bratislava a.s. priložil k žiadosti o vydanie licencie na prevádzku medzinárodnej autobusovej linky Bratislava - Rajka. Okrem iného hovoria o tom, že vnútroštátna doprava bude na tejto linke síce povolená, ale otázne je že či bude pre niekoho aj zaujímavá. Dopravný podnik plánuje na tejto linke zaviesť tarifu, kde základný lístok bude neprestupný a na jednu jazdu bude stáť 1,50 EUR. Nebude však záležať, že či sa touto linkou zveziete z centra Bratislavy až do Rajky, alebo len z centra mesta napríklad do Rusoviec. V jednom ako aj v druhom prípade zaplatíme rovnako 1,50 EUR.   Pre koho bude táto linka určená?   V mediach sľubovaná nadväznosť spojov tejto pripravovanej autobusovej linky na vlaky premávajúc z a do Rajky tiež nevyzerá byť vo väčšine prípadov dodržaná. Maďarské železnice v decembri 2009 totiž menili grafikon. Pôvodne v rámci úsporných opatrení takmer zrušili úplne osobnú železničnú dopravu z a do Rajky. Tento zámer sa však podarilo zvrátiť a do resp. z Rajky premávajú denne len dva páry vlakov ráno a dva páry popoludní. Bratislavský dopravný podnik si asi zatiaľ túto zmenu nevšimol. Preto ranné vlakové spoje pripravovaný autobus z a ani do Bratislavy vôbec nebude nadväzovať. O nadväznosti na autobusy premávajúce z Rajky ďalej smerom na Mosonmagyarovár ani nemá zmysel baviť.   Z v súčasnosti dostupných informácii sa význam celého projektu scvrkol len na sprístupnenie pracovných trhov nachádzajúcich sa v Bratislave pre Slovákov, ktorí sa presťahovali do blízkej maďarskej Rajky. Podporu turistického ruchu pre nenadväzovanie spojov z a do Rajky nemožno očakávať. A vďaka pripravovanej tarife táto linka nebude prakticky zaujímavá ani pre obyvateľov Jaroviec, Rusoviec a Čunova. Ako je vidieť aj z tohto projektu cezhraničnej spolupráce hranice v hlavách ľudí sú stále. Projekt tejto autobusovej linky mohol naznačiť, že sa to mení. Zatiaľ to však tak nevyzerá.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Je nízky záujem o regionálne voľby opodstatnený?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Malý prieskum ponuky piva na bratislavskej hrádzi, časť prvá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Aké pivo pijete?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Ako sa dá hovoriť o výsledkoch volieb, keď o nich nemôžete hovoriť?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Kalmár
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Kalmár
            
         
        kalmar.blog.sme.sk (rss)
         
                                     
     
        Mne nie je jedno kto a ako v Bratislavskej VÚC minie ročne 122 miliónov z našich daní! A Vám??? Poďme to spolu zmeniť 9.11.2013. Po viac ako 10 ročnej skúsenosti ako poslanec v Miestnom zastupiteľstve MČ Bratislava Rusovce som sa rozhodol kandidovať za poslanca Bratislavskej VÚC. VÚC treba viac priblížiť k ľuďom a riešiť v nej naše reálne každodenné problémy. Práve ako poslanec miestneho zastupiteľstva mám s týmto skúsenosti. Petržalka, Jarovce, Rusovce, Čunova - Dovoľujem si Vás požiadať o Vašu podporu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1651
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Komunálne veci
                        
                     
                                     
                        
                            O pive
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       návraty pod poľanu
                     
                                                         
                       Je nízky záujem o regionálne voľby opodstatnený?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




