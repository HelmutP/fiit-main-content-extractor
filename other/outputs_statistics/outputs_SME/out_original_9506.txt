
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            daniela matulová
                                        &gt;
                Nezaradené
                     
                 Učiť sa? Asi hej.. 

        
            
                                    24.6.2010
            o
            9:50
                        |
            Karma článku:
                5.30
            |
            Prečítané 
            1042-krát
                    
         
     
         
             

                 
                    Niektorí tvrdia, že učiť sa počas strednej školy je zbytočné.Je to však recept na úspech?
                 

                 
  
    Čerstvá absolventka osemročného gymnázia. Áno, som jeden z tých chudákov, ktorí majú o rok menej školy, no za to žiadne prijímačky na strednú.        Keď sme boli malí, tak väčšinu z nás rodičia nútili, aby sme si sadli nad učivo a učili sa. Môžem len ďakovať mamine, že ma k tomu dokopala a ja som prešla gympel úspešne. Tie hodiny driny, keď som bola menšia do mňa zakorenili isté semienka, ktoré ma hecovali do lepších výkonov. Lebo mám na viac, povedala som si vždy keď sa niečo nepodarilo podľa predstáv a ostatní sa mi čudovali, prečo sa som naštvatá za 2ku. Vraj to je dobrá známka.        Chodila som na gymnázium so zameraním na cudzie jazyky s rozšíreným obsahov všetkého ostatného. Niet divu, že keď mal niekto slabé miesto napríklad v Náuke o spoločnosti, tak mu svietila na vysvedčení 3ka alebo niečo iné.  Posledné dva roky sme si už len hovorili, prejdem Vazku, zvládnem všetko. A mám pocit, že je to tak. Takmer celú strednú školu som na vyučovaní dávala pozor, a prinieslo mi to ovocie tým, že pred písomkou som sa to nemusela bifliť, či prebrať si to celé odznova, ale som si to len pozrela a takých 40% si pamätám doteraz.        Najviac sa mi páčili výkriky do tmy mojich spolužiakov, načo sa takéto kraviny musíme učiť? Veď to nikdy nebudeme potrebovať. Niečo na tom pravdy je, to musím uznať. Na strednej som nenávidela, keď som sa musela učiť filozofov a ich nepravdepodobné teórie o tom, ako vznikol svet, ktoré zväčša vznikali po takých piatich pohárikoch nejakej nekvalitnej pijatiky. Ale prekúsala som sa cez to a mám ako sa hovorí všeobecný rozhľad. Kvôli tomu, sa učíme všetko, čo sa dá. Aby sme neskončili ako neandrtálci, čo si nevedia dať dve a dve dokopy z najrôznejších  oblastí.        Učenie na strednej dáva základ, ako sa učiť. Rôzne spôsoby učenia sa sa overili pred maturitou. Vlastne vtedy sa overilo ako človeku záleží na ďalšom postupe. Počas roka všetci hovorili, á čo sa budeme učiť to sa stihne cez akademický... Ako čas plynul a materiály sa hromadili, postupne takmer všetci zisťovali, že cez ten akademický sa to naučiť nedá a nestíha. Z nepoctivých študentov sa stávali poctiví a napokon sa každý do toho pozrel.        Samotná maturita je vec šťastia. Myslím, že na nej viac menej nejde o to, či ste sa učili, ale skôr o to, akú otázku si vytiahnete. Niektorí majú šťastie, iní nie, iných nemá rada skúšajúca.  Je to i vec, toho či človek pracuje na hodinách počas školského roka. Veď zoberte si, keď niekto pracuje a na maturite odpovie slabšie učiteľka vie, že tam niekde v hlave to je a dá vám tie pomocné otázky, za účelom ich odtiaľ vydolovať. No ak človek neprejaví nikdy aktivitu, na písomky sa vykašle, tak napriek tomu, že sa učil mu bude jeho snaženie márne.        Po strednej sa ide na výšku a tam budú iné kvantá vedomostí, ktoré sa bude treba naučiť. Je pravda, že výšku si človek vyberá sám, ale ak niekto sa neučil celú strednú, pravdepodobne nemá systém učenia sa, a na výške mu to bude robiť väčšie problémy.         V čase podávania prihlášok na vysoké školy, som neraz počula, keby som sa viac učil mohli ma prijať bez prijímačok. Odmena za vynaloženú prácu na strednej škole? Možno. No prijímačky sú i na to, aby vytriedili uchádzačov na tri skupiny. Tí ktorí na to nemajú, tí ktorí na to majú a tí ktorí na to síce majú, ale nie je pre nich miesto na štúdium na danej fakulte vysokej školy.        Preto sa pýtam učiť sa? Viem si dať rýchlu odpoveď.        Áno. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        daniela matulová 
                                        
                                            Nakŕmi veda ľudstvo???
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        daniela matulová 
                                        
                                            Seriály..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        daniela matulová 
                                        
                                            Curling na Slovensku?? Áno!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        daniela matulová 
                                        
                                            tvárokniha- dobrá či zlá vec?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        daniela matulová 
                                        
                                            Tragédia, ktorá otriasla moderným svetom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: daniela matulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                daniela matulová
            
         
        matulova.blog.sme.sk (rss)
         
                                     
     
        priveľa myšlienok na ľudskú hlavu...:)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1120
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




