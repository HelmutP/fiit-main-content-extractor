
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondro Urban
                                        &gt;
                Web
                     
                 Lesk a sláva na obrazovke, chudoba a bieda na webe 

        
            
                                    26.3.2010
            o
            9:18
                        (upravené
                26.3.2010
                o
                16:43)
                        |
            Karma článku:
                6.17
            |
            Prečítané 
            1826-krát
                    
         
     
         
             

                 
                    Kamarátka na Facebooku postla link na web nedávno predstavenej show Let's Dance 4, vystrelo ma. To čo sa deje na obrazovke, môžme vo všeobecnosti označiť ako profesionálny produkt zo špičkovej domácej dielne. Ale to čo sa deje na ich webe je prinajmenšom výsmech celého webu. Žeby mali cieľovku 90+?
                 

                 
Horúčka nedeľňajšej noci a webuZdroj: http://letsdance.markiza.sk/
   Web je na adrese http://letsdance.markiza.sk/ - adresa je dobrá, jednoduchá a hlavne intuitívna (nesnažili sa o nejake brutálne skratky typu ld a podobne). Tak, a tu sa (skoro) všetko dobré končí.   To, čo sme považovali za top stránku v 90tych rokoch, sa nám TV Markíza pokúša priniesť dnes. Ťažké technologické a grafické retro. Takéto výplody som zažíval akurát pri hľadaní študijných materiálov. Pri vyučujúcich to však človek pochopí, veď nemajú rozpočet. Ale najväčšia televízia na Slovensku? Už mi chýba iba rozanimovaná zemeguľa      Vrch stránky   Pochopiteľný vrchný čierny pás, predsa markiza.sk je portál (nie som veľmi nadšencom pásikov s odkazmi na ostatné portálové stránky, asi najlepšie má u nás tento problém vyriešený etrend.sk). Mierne mi to vadí, ale v porovnaní s ostatnými zločinmi voči oku, je toto bezproblémové.      Obrázky so súťažiacimi sú jednoducho zlo. Od rozkockovaného kruhu (fakt netuším ako sa im to podarilo) cez prednastavený fx glow z Photoshopu. Preloder obrázku pri mouseover je tiež veľmi postrádaný (keď už niekto nerobí cez JS, tak aspoň cez CSS a positioning pozadia). Obrázok „po prejdení myšou" nad neho má nejaký exkluzívny radiálny gradient, ktorý som teda vážne nepochopil, akokoľvek som sa snažil.   Čo je relatívne dobré, je logo Let's Dance vo vrchu. Nápad s televízorom je možno zaujímavý, no graficky nedotiahnutý. Okrem toho, že ten televízor je zle nakreslený, aj ten dátum pod ním je absolútne nezmyselný.   Zle, zle a ešte raz zle   Ach jaj, to menu. Ak by mi grafik došiel s takýmito gifkami, tak sa s ním ani nebavím. To nie je menu, ale gýč àla 90s. Nebudem sa venovať detailom, ale aj ten glow svedčí o zlobe, ktorá je za tým schovaná.      A obsah nie je na tom o nič lepšie. Glow efekt celého content boxu, bublinkové pozadie boxíkov, buttonový efekt na pozadí boxov s textom, nízky kontrast textu s pozadím, nedoriešené odsadenia, znásilnené pomery strán obrázkov, zabité stránkovanie komentárov, nezarovnané inputy v komentároch, šedý text na béžovom pozadí, za dve sekundy zbúchaný gombík „Postúpiť" v komentároch pomocou jedného primitívneho gradientu (mimochodom, čo znamená Postúpiť?). Radšej už nebudem pokračovať, lebo si deriem klávesnicu. Ešte ale stojí za povšimnutie totálne zabitý priestor pod reklamným boxom napravo.   To, že stránky Markízy nie sú v poriadku vo všeobecnosti vieme, ale táto stránka je výsmech webu. Bodaj by si v TV Markíza aspoň trošku brali príklad od ich konkurencie TV JOJ. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Problém taxíkov v zlom počasí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Výtlk, výtlčisko a také obyčajné výtlčíky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Onedlho tu už Fico nebude!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Hľadanie konečného riešenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Moja starostka - Táňa Rosová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondro Urban
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondro Urban
            
         
        ourban.blog.sme.sk (rss)
         
                                     
     
        http://twitter.com/ourban
http://facebook.com/ourban
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2255
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Web
                        
                     
                                     
                        
                            Sociálne médiá
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Donovaly: slovenský Aspen, kde Vás smetiari vrátia späť do reality
                     
                                                         
                       Fico vyrába ďalších 65 tisíc ľudí bez práce
                     
                                                         
                       Výpalníci z Úradu vlády sú horší ako smrť.
                     
                                                         
                       Na rómske komunity štátny rozpočet opäť raz zabudol
                     
                                                         
                       Prečo Slovensko nepatrí medzi šťastné krajiny?
                     
                                                         
                       Nie som ako ostatní učitelia...
                     
                                                         
                       Slovensko má depresiu. Fico ju chce liečiť marihuanou.
                     
                                                         
                       Voda bude drahšia a to nie je všetko...(časť prvá)
                     
                                                         
                       V akej krajine to žijeme?!
                     
                                                         
                       Minister šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




