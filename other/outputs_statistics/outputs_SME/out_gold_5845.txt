

    
   
   
 Poznám ľudí, ktorých obyčajné slová dokážu rozladiť do krajnosti, iných zasa vyburcujú k bravúrnym výkonom a ďalších môžu doslova zabiť. Zrejme ste sa už stretli s frázou „tak toto mi pohladilo dušu“, možno ju aj používate. Slová totiž občas pôsobia ako dotyk človeka, ktorý ich píše či vysloví – ako pohladenie, kopnutie, facka či postrčenie do života. Neraz sú jediným kľúčom, ktorý odomkne naše vnútro a posunie nás ďalej. 
 Pamätám si na prvú reakciu kompetentného človeka, ktorý hodnotil moju románovú prvotinu. Bol to Dr. h. c. prof. Zdenko Kasáč (1924) – literárny historik, ktorý za svoj život spoznal nejednu poprednú literárnu osobnosť, hodnotil tisíce textov a verím, že svojim láskavým prístupom posunul vpred množstvo ľudí. Jeho prioritou bolo nájsť v texte v prvom rade nie chyby, ale náznak talentu, teda akúkoľvek výnimočnosť. Toto sa snažil vyzdvihnúť aj následne pri osobnom hodnotení. Chyby ostali druhoradé – tie predsa kedykoľvek opravíte, poprípade zdokonalíte svoje trhliny. Myslím, že ak by sa v tom čase prof. Kasáč zameral na prísnu deštruktívnu kritiku, vzhľadom na môj mladý vek, by som v sebe možno nedokázal nájsť toľké oduševnenie pre literatúru a zároveň chuť do písania. Ak by ma zhodil za nedostatky, za slabé stránky, za banálne vety a všetko to, s čím sa napokon dá v budúcnosti popracovať, ostal by som zarazený a váhal by som nad sebou. Cením si slovo tohto pána, s ktorým sa v súčasnosti stretávam v príjemnom priateľskom vzťahu.  
 Je mojím želaním, aby sme pri voľbe slov zachádzali predovšetkým do dobrých stránok človeka, vkladali do nich čo najviac nádeje a snažili sa nimi posúvať. Občas pár obyčajných slov dokáže rozohriať vnútro ľudí a vybičovať ho k vytrysknutiu toho dobrého, čo v ňom celý čas drieme – snáď i toho výnimočného, čo je v ňom uzamknuté.  
 Cením si všetkých, ktorí sa slovom, či už originálne či v banálnom šate, pokúšajú ohriať svet ľudskosťou. Ďakujem Vám! 
   

