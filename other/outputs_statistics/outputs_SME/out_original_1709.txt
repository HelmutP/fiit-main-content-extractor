
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Fülöp
                                        &gt;
                svet okolo mňa
                     
                 Skutočný príbeh 

        
            
                                    21.9.2009
            o
            23:03
                        |
            Karma článku:
                6.01
            |
            Prečítané 
            1638-krát
                    
         
     
         
             

                 
                    V sobotu sme sa s ockom vybrali navštíviť babku z Biskupíc. Išla s nami aj moja krstná mama, ktorá je u nás na návšteve z Ameriky a doniesla mi odtiaľ tú najsuprovejšiu bábiku American girl. Krstná niekde v meste vystúpila a my sme išli najprv na zmrzlinu.
                 

                 Ocko ľúbi najviac tú z tej cukrárne, kde je vždy veľa ľudí a musíme čakať v rade. Aj mne chutí. Zaparkovali sme, všade na okolo boli vysoké paneláky, a postavili sa do radu. Zmrzlina bola výborná, slnko svietilo a bolo teplo. Stáli sme pri zaparkovanom aute a popri lízaní zmrzliny som ockovi rozprávala, že moja kamarátka zo škôlky má presne takú barbienu ako ja a že sme sa dohodli, že keď pôjdeme znova do škôlky, tak sa s nimi budeme spolu hrať.  Okolo nás vtedy prechádzal jeden ujo a niečo povedal, čomu som celkom nerozumela, lebo aj keď chodím do slovenskej škôlky a mama s otcom sa so mnou občas rozprávajú po slovensky, ešte všetkým slovám nerozumiem. Vyrozumela som iba niečo o tom, že som malá a že ako rozprávam. Ten ujo sa pozeral škaredo ale neviem, prečo. Ocko mu asi rozumel viac a možno aj vedel, prečo sa na mňa ten neznámy ujo takto škaredo pozerá, lebo mu niečo tiež povedal a tiež sa škaredo pozeral. Potom sa už pamätám iba na to, že som sa veľmi bála, lebo ocko bol veľmi nahnevaný a ten ujo mával rukami, zase niečo hovoril a nakoniec odišiel. Veľmi som plakala, mala som strach. Neviem, prečo sa na mňa ten ujo hneval, naozaj som nič zlé neurobila. Ocko sa potom ešte dlho kvôli nemu mračil a mňa hladkal aby som neplakala a povedal mi, že to bol zlý ujo. Asi preto, lebo straší malé deti... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Prečo sa mi oplatí žiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Rakovina moja každodenná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            (Skoro) Perfektný večer v Aréne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Ľudia na onkológii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Vojna pokračuje
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Fülöp
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Fülöp
            
         
        evafulop.blog.sme.sk (rss)
         
                                     
     
        Som matka, manželka, dcéra, priateľka, kamarátka, známa aj neznáma a som aj onkologický pacient
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3253
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Mám sa stále lepšie
                        
                     
                                     
                        
                            svet okolo mňa
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Silvia Sabova
                                     
                                                                             
                                            Miriamin blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Internetový časopis Priateľka
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




