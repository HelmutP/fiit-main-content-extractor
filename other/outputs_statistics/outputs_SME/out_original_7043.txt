
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dusan Jurena
                                        &gt;
                Nezaradené
                     
                 “Najnovšia“ sovietská vírusová hitofka na internete 

        
            
                                    22.5.2010
            o
            18:00
                        (upravené
                22.5.2010
                o
                15:58)
                        |
            Karma článku:
                4.63
            |
            Prečítané 
            1181-krát
                    
         
     
         
             

                 
                    Je to už pár mesiacov, čo sa na youtube objavilo nenápadné videjko so starou nahrávkou sovietskeho speváka z roku 1966. Interprét, Eduard Chiľ, vo videoklipe spieva pieseň s názvom „Som šťastný, že sa konečne vraciam domov" (v originále „Я очень рад, ведь я, наконец, возвращаюсь домой..."). Spomínaná pieseň, ktorej speváka pomenovala mladá generácia ako „Trololo", alebo tiež „Trololo man", sa stala obrovským hitom v Amerike a následne sa táto horúčka preniesla späť do Ruska.
                 

                 Na tom by ešte nebolo nič extra zaujímavé, keby Chiľ za celých 2 a pol minúty naozaj aj spieval nejaký text. Celý vtip je v tom, že daný čas vypĺňajú „slová" ako „Ááááá, la la la la, ohohóóó, lo lo lo lo ...". Pritom človek sa počas piesne vôbec nenudí a mimický prejav speváka musia, bezpochyby do morku kostí, závidieť Rowan Atkinson spolu s Jimom Carreym.   Samozrejme pôvodne mala pieseň aj zmysluplný text - o kovbojovi, ktorý sa rúti na svojom mustangovi domov cez celú prériu ku svojej milej Mary, ale schvaľovacej komisii sa zdalo nie príliš „sovietske", aby vznikol hit o čomsi americkom. A tak autori piesne vyhodili celý text a nahradili ho ohókaním a lalákaním. Dielko v hŕbe iných piesní upadlo rýchlo do zabudnutia, veď aj sám Eduard Chiľ naspieval kopec iných pesničiek, a na svetlo ho znovu vytiahli až neznámi recesisti, ktorí tento starý videoklip zavesili na youtube. Veľmi rýchlo sa stal jedným z najpozeranejších a najrôznejšie verzie tohoto videa si pozrelo dokopy asi 20 miliónov ľudí. Jeho fansite na facebooku má takmer 32 tisíc členov.   Pieseň samotnú nie je vôbec ľahké zaspievať, keďže spevák musí mať hlasový rozsah 3 oktávy. Eduard Chiľ - penzista, má momentálne kopec pozvánok od svojich nových amerických a kanadských fanúšikov, ktorí vytvorili peňažnú zbierku a pozývajú ho na turné po severnej amerike. Sám Chiľ sa v interview vyjadril,že ho nový zvýšený záujem veľmi teší a skutočne zvažuje, že ponuku od američanov príjme.   Nakoniec dodám, že dielko, ktoré zaspieval Chiľ sa veľmi ponáša na niekoľko starších piesní z raných 60tych rokov v prevedení Muslima Magomajeva, Valerija Obodzinského, či dokonca na staré americké scat-songy z medzivojnového obdobia. Autori piesne pre Chiľa sa určite pri tvorbe „nechali inšpirovať". :)   A samozrejme pár linkov :   http://trololololo.ru/   http://trolololololololololol.com/   http://lurkmore.ru/Trololo   http://knowyourmeme.com/memes/trololo-edward-hill-russian-rickroll   Dokonca ruský telekanál TNT vytvoril reklamný šot v "trololo" hudobnom prevedení  (záver šotu znie : Pocíť naše Trololo) :   http://rutube.ru/tracks/3180286.html?v=6a80d18f2b0893eae2b53efede95e2d4 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dusan Jurena 
                                        
                                            Spišský Hrhov a nerovnaký meter
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dusan Jurena 
                                        
                                            architekti - sme naozaj až takí diletanti?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dusan Jurena 
                                        
                                            Dajte mu aspoň ľahký guľomet!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dusan Jurena 
                                        
                                            Slovenský Raj - Rakúske Alpy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dusan Jurena 
                                        
                                            Bič na Procházkovú
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dusan Jurena
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dusan Jurena
            
         
        dusanjurena.blog.sme.sk (rss)
         
                                     
     
        rozvíjajúci sa architekt vo veľkom meste
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1391
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            George Dumezil - Mytus a Epos II
                                     
                                                                             
                                            Bulent Atalay - Matematika a Mona Liza
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            ELP
                                     
                                                                             
                                            Frank Zappa
                                     
                                                                             
                                            Lyapis Trubetskoy
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Polícia nechráni občanov ale podvodníkov.
                     
                                                         
                       V tejto krajine majú policajti kolty prekliate nízko
                     
                                                         
                       Kryje primátor korupciu a rodinkárstvo?
                     
                                                         
                       Zničia developeri Železnú studienku?
                     
                                                         
                       Prečo nehovoria pravdu primátor Raši a minister Žiga o uráne?
                     
                                                         
                       Ftáčnikov vzorec
                     
                                                         
                       Pozrite sa, čo si dovoľujú ku klientom na úrade práce
                     
                                                         
                       Monikine paralelné cesty
                     
                                                         
                       Angličtinu učia maturanti a ministerstvo školstva vedú tiež experti
                     
                                                         
                       Ako je prakticky nemožné zorganizovať prednášku Toma Nicholsona
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




