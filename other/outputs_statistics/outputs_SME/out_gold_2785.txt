

 Tiež sa vám niekedy zdá, že slúchadlá nie sú vhodným pomocníkom tam, kde je potrebné podeliť sa s hudbou v teréne s ostatnými? Vraj ste ešte nikde nevideli použiteľné cestovné reproduktory? My sme narazili na zaujímavé riešenie. Hodí sa nielen k MP3 prehrávaču, ale i k notebooku - reproduktorové "véčko" Arkon. 

 FOTO - MILAN GIGEL

 Už na prvý pohľad je zrejmé, že výrobcovi nechýbal pri návrhu dôvtip a cit pre mechanickú odolnosť. Dvojica reproduktorových systémov PA-209 a PA-211 sa v zásade líši v dvoch aspektoch. Prvým je rozmer. Systém PA-209 je o čosi menší ako jeho všestrannejší kolega PA-211. Druhým je fakt, že kým menší reproduktorový set je pasívny, PA-211 je vybavený zabudovaným zosilňovačom napájaným akumulátormi. Podľa svojich nárokov tak máte na výber medzi dvojicou riešení. Obe vás prekvapia svojim zvukovým výstupom. 

 FOTO - MILAN GIGEL

 "Véčková" konštrukcia so stredovým kĺbovým uchytením zaisťuje dokonalú ochranu pri preprave. Aby sa výrobca vyhol komplikovaným pružinovým systémom pri uzatváraní ktoré by sa mohli časom opotrebovať, pre aretáciu v uzavretej polohe využil dvojicu zabudovaných magnetov. Tie zaistia pevné uzavretie i pri mechanickej námahe. Konektor ovinutého káblika chráni špeciálne navrhnutá preliačina vo vnútri systému. 

 FOTO - MILAN GIGEL

 Keďže ide o pomocníka, pri ktorom sa predpokladá že ho budete nosiť so sebou všade kam sa pohnete so svojou batožinou - či už príručnou alebo v pravom slova zmysle - nechýba ani syntetické jemné puzdro pre prepravu. I keď je vonkajší obal "véčka" dostatočne robustný a pevný, predsa len ho dokážete ochrániť od prípadných škrabancov. 

 FOTO - MILAN GIGEL

 Vonkajší polykarbonátový obal je je čiastočne reliéfovaný pre jednodnoduchší úchop a manipuláciu. Dominantné logo výrobcu je dnes už štandardom pri všetkom. Za pozornosť však stojí skutočnosť, že povrch je lesklý a úplne hladký, aby ste ho dokázali jednoducho očistiť od povrchového znečistenia. V teréne človek nikdy nevie, či sa vráti na reproduktoroch s blatom, popolom z ohniska či čímkoľvek podobným. 

 FOTO - MILAN GIGEL

 V prípade variantu so zabudovaným zosilňovačom nájdete na rubovej strane kabinet pre vloženie napájacích monočlánkov. Ide o posuvný systém ktorý nemá žiadne mechanické slabiny, ktoré by mohli časom viesť k nemožnosti uzavretia či fixácie krytu v zavretej polohe. Trojica výstupkov zaistí jednoduché otvorenie i pri mastných či vlhkých rukách. 

 FOTO - MILAN GIGEL

 Napájanie zaisťuje trojica akumulátorov či monočlánkov formátu AA, ktorá je ku kontaktnému pružinovému poľu dotlačená molitanom. Ani pri páde na zem sa nestane, aby články "vyskočili" zo svojej prevádzkovej polohy. Štandardizovaný formát zaisťuje jednoduchú dostupnosť náhrady v prípade núdze. 

 FOTO - MILAN GIGEL

 Pre napájanie je však pripravený i alternatívny vstup - či už pre ľubovoľný externý batériový kit, cestovný solárny modul alebo čokoľvek iné. Vstupné napätie by malo byť od 4 do 6 voltov. 

 FOTO - MILAN GIGEL

 Káblik je v dopravnej polohe zasunutý do voľnej drážky a konektor vložený do dutiny fixuje jeho polohu tak, aby sa nepoškodil. Nepotrebujete ho nijako navíjať, iba ho uložíte na svoje miesto. Bezkyslíkatá lanková meď zaisťuje vysokú ohybnosť a trvácnosť. 

 FOTO - MILAN GIGEL

 Po uvoľnení má káblik dĺžku približne 20 cm, čo je pre MP3 prehrávače, MiniDisc playery a iné mobilné audiozariadenia viac ako dosť. V prípade notebooku by sa však asi hodila v niektorých prípadoch káblová predlžovačka. Pýtate sa prečo pripájať čosi podobné k notebooku? Zvukový výstup dosahuje kvalitou a intenzitou minimálne 10:1. Pozeranie filmu vo vlaku s dobrým zvukom za to stojí... 

 FOTO - MILAN GIGEL

 V prípade pasívneho "véčka" sú reproduktorové jednotky priamo pripojené k 3,5mm jack konektoru. Žiadne ovládacie prvky tam nenájdete. Na aktívnych je na vrchnej strane bočnice červená indikačná LEDka, posuvný vypínač a potenciometer pre riadenie hlasitosti. 

 FOTO - MILAN GIGEL

 Reproduktory sú na oboch krídlach chránené pevnou kovovou mriežkou ktorá je tesne zasunutá do plastového polykarbonátového rámu. Ak ju budete chcieť odstrániť kvôli rozoberaniu, budete mať s jej vyťahovaním trochu ťažkosti. Pod mriežkou sa nachádza tkaninová ochrana, ktorá zamezí vnikaniu prachu a nečistôt do vnútra, či na membránu. 

 FOTO - MILAN GIGEL

 Pre nazvučenie sa používa plochá konštrukcia na báze slúchadla s priemerom 6 centimetrov a hrúbkou necelých 5 milimetrov. Membrána je plastová a na spodnej strane sa nachádza kovová výstuž s otvormi pre tok zvuku do vnútra plastovej ozvučnice. Pri 8 ohmovej impedancii je výkon každého reproduktora 1 watt. Keďže ide o pevný nerezonujúci plast s dostatočným vnútorným objemom, zvuk je pomerne dynamický a dosahuje zaujímavé akustické vlastnosti ktoré umožňujú použitie nielen v miestnosti, ale i na otvorenom priestranstve bez toho, aby ste mali dojem že počúvate "plážové rádio". Zvýraznené hĺbky majú za následok čiastočné potlačenie čistých výšok, čo však pri posluchu vonku vadiť nebude. Zvuk je pomerne čistý, bez skreslenia bez príznakov rezonovania, chýbajúcich stredov a hĺbok a hlavne bez "huhňania". Zvukový výstup je v danej triede skutočným prekvapením. 

 FOTO - MILAN GIGEL

 Zosilňovač je postavený na báze frekventovaného čipu TEA2025, takže s prípadnými úpravami, vylepšeniami či opravami by v prípade potreby nemali byť žiadne problémy. Vo vnútri je dostatok voľného priestoru pre sebarealizáciu. 

 FOTO - MILAN GIGEL

 V skratke možno povedať, že ide o zaujímavé riešenie do terénu i na pracovný stôl, kde nie je dostatok voľného miesta alebo kde sa vyžaduje podmazové ozvučenie pre viacerých ľudí. Po zvukovej stránke poskytujú reproduktorvé súpravy zaujímavé výsledky s drobným defektom v absencii výšok. Kompaktné vyhotovenie, jednoduché napájanie a praktický dizajn však hovoria v prospech tohto riešenia. 
 Dostuposť: http://www.arkon.com/ipodaccessories.html Orientačná cena: 25-30 dolárov 

