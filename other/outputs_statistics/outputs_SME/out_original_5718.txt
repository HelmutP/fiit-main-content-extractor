
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Bórik
                                        &gt;
                svet
                     
                 Údajne mám požičať Grécku 150 eur, v žiadnom prípade. 

        
            
                                    3.5.2010
            o
            9:43
                        (upravené
                3.5.2010
                o
                10:24)
                        |
            Karma článku:
                14.94
            |
            Prečítané 
            3564-krát
                    
         
     
         
             

                 
                    Bedár požičiava posledný groš. Gréci veľa rokov rozhadzovali a žili svoj bohémsky život. Už si nebudú môcť dovoliť štrnáste platy a niekoľkomesačné odstupné. Konkrétna otázka: prečo by sme im mali požičať? Aká je vymožiteľnosť dlhu?
                 

                 Fakty:    - Grécko potrebuje 110 miliárd eur, z toho 30 dodá MMF   - Slovensko má požičať 816 miliónov eur (ekvivalentne 150 eur dá každý Slovák)   - Na poskytnutie pôžičky si bude musieť Slovensko požičať   - O pôžičke rozhodne až budúca vláda   Názory:   - renomovaní ekonómovia odporúčajú bankrot   - nedá sa presne určiť, či Grécko unesie splátky   - situácia sťahuje aj kurz eura, trhy strácajú dôveru   - polovica Grékov nesúhlasí s úspornými opatreniami, organizujú protesty a demonštrácie   Zhrnutie:   Nesúhlasím s tým, aby som mal požičiavať mojich 150 eur niekomu, kto dlhé roky vedie bohémsky život, má vysokú skorú rentu a mnohonásobne vyššie odstupné. Považujem to za stratené peniaze. Slovensko má svoje vlastné vnútorné problémy - stredná vrstva upadá, každý deň sa vynárajú nové podvody, klesá úroveň vzdelania, je slabá podpora vedy, verejná mienka sa manipuluje a na politike sa už dá len zasmiať. Je najmenej vhodný čas na nejaké požičiavanie, kým nemáme upratané na vlastnom dvore.   Gréci protestujú proti zníženiu platov a iným úsporným vládnym opatreniam. Boli naučení žiť na vysokej nohe. Ťažko sa im bude odvykať a nemali by sme im tento život predlžovať. Keď si to porovnám so životnou úrovňou v našej spoločnosti, rád by som sa niekedy dozvedel od Gréckeho občana, či vie adekvátne vyjadriť svoju vďaku za pripravovanú pôžičku. Keď už teraz chodí po uliciach a vykrikuje aby sa neznižovali platy, na ktoré im idem požičať ja čo mám 3x menší plat. Ako keď bedár požičiava posledný groš.   Poskytnutie tejto pôžičky bude závažným rozhodnutím, o ktorom by sa malo rozhodnúť v referende, ak budú politici počúvať hlasy svojich občanov, odpoveď budú poznať aj bez neho.   Každý Slovák požičia Grécku asi 150 eur (SME)         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (84)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Kandidujem pretože politici ma denne dvíhajú zo stoličky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Živnostník so zápalkami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Režim žije aj po 45 rokoch, vďaka nasledovníkom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Mesto Martin hľadalo riaditeľa kultúry, bol som na výberovom konaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Bórik 
                                        
                                            Ficova olympiáda spôsobí deštrukciu domácej ekonomiky.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Bórik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Bórik
            
         
        borik.blog.sme.sk (rss)
         
                                     
     
         Zaoberám sa kreatívnym marketingom, PR, procesným manažmentom a japonskou filozofiou Kaizen. Zakladateľ webu s dobrými správami z Turca. 


 
Šéfredaktor 
Správca metra 
 



 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    150
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4393
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            technika
                        
                     
                                     
                        
                            marketing
                        
                     
                                     
                        
                            komentáre
                        
                     
                                     
                        
                            sci-fi ?
                        
                     
                                     
                        
                            svet
                        
                     
                                     
                        
                            právo
                        
                     
                                     
                        
                            veda
                        
                     
                                     
                        
                            všeobecne
                        
                     
                                     
                        
                            súkromné
                        
                     
                                     
                        
                            zakázané
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ústavné právo Slovenskej republiky
                                     
                                                                             
                                            Projektový manažment
                                     
                                                                             
                                            Wikinomie
                                     
                                                                             
                                            Psychológia pre manažérov a podnikateľov
                                     
                                                                             
                                            Psychologie lidské odolnosti
                                     
                                                                             
                                            Psychologie osobnosti
                                     
                                                                             
                                            Forenzná kriminalistika
                                     
                                                                             
                                            Personálny manažment
                                     
                                                                             
                                            Kaizen (Masaaki Imai)
                                     
                                                                             
                                            Anton Heretik - Forenzná psychológia
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Clint Mansell
                                     
                                                                             
                                            Rob Dougan
                                     
                                                                             
                                            Ennio Morricone
                                     
                                                                             
                                            Jimi Hendrix
                                     
                                                                             
                                            Leningrad
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Wired
                                     
                                                                             
                                            Stratégie online
                                     
                                                                             
                                            eTREND
                                     
                                                                             
                                            Popular Science
                                     
                                                                             
                                            ScienceBlogs
                                     
                                                                             
                                            Nicola Tesla Society
                                     
                                                                             
                                            Archív internetu
                                     
                                                                             
                                            Scienceworld
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako mravec zvíťazil nad Gorilou
                     
                                                         
                       Ako si pripraviť projekt na "eurofondy" vlastnými silami (1.časť)
                     
                                                         
                       Otvorenie letnej turistickej sezóny.
                     
                                                         
                       Vstup do Petržalky má byť pompézny. Pozrime sa, aká bude realita…
                     
                                                         
                       Prečo šváby nemajú kolesá
                     
                                                         
                       Strana TIP - bohatá a neznáma.
                     
                                                         
                       Do Moskvy vlakom? Prečo nie!
                     
                                                         
                       Stredovek 21. storočia vs. Róbert Bezák
                     
                                                         
                       Blúdivý nerv žirafy
                     
                                                         
                       Slovenská pošta podporuje kvalitné nemecké výrobky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




