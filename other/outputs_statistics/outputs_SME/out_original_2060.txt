
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Jablonska
                                        &gt;
                Moje najmilsie / Moji najmilsi
                     
                 Inicialy BB 

        
            
                                    22.3.2010
            o
            22:08
                        (upravené
                22.3.2010
                o
                17:52)
                        |
            Karma článku:
                7.48
            |
            Prečítané 
            2657-krát
                    
         
     
         
             

                 
                    Od februara je vo francuzskych kinach film "Gainsbourg (Vie heroique)" o zivote jednej z velkych osobnosti francuzskej kultury. Jedna pasaz filmu je venovana jeho kratkemu, ale velmi intenzivnemu vztahu s dalsou ikonou - Brigitte Bardot - ktoru predstavuje Laetitia Casta. V jednom z rozhovorov Casta uviedla, ze len co sa dozvedela, ze ulohu dostala, telefonovala znamej herecke, ktora jej s radostou svoj vztah s Gainsbourgom priblizila. O tom, ze z nej Gainsbourg stratil hlavu niet pochyb, ved kvoli nej a pre nu bol schopny za jedinu noc zlozit tri piesne. Stacilo zasepkat: "Milacik, chod ku klaviru a napis mi tu najkrajsiu piesen o laske" a "Je t'aime, moi non plus" bola na svete.
                 

                 
  
   28. septembra 1934 sa v peknom parizskom byte neozval plac zelaneho chlapca. Naopak, rodicom Bardotovym sa narodilo dievcatko, ktore im neskor pripravilo nejedno prekvapenie. Pilou a Toty Bardotovci tvorili sucast parizskej burzoazie; on priemyselnik, ktory vo volnych chvilach pisal poeziu, ona sa velmi zaujimala o tanec a modu. Svoje deti Brigitte a Marie-Jeanne vychovavali velmi prisne, co mladej Brigitte, tuziacej po slobode, velmi nevyhovovalo. Zacala sa venovat tancu a od svojich strnastich rokov robila modelku pre viacere parizske modne domy, dokonca sa objavila aj na jednej z tituliek casopisu Elle.       Jej tvar neunikla pozornosti filmara Marca Allegreta, ktoreho asistentom bol isty Roger Vadim. Ten sa do mladuckej Brigitte okamzite zamiloval. Vztahu mladej dcery s o dost starsim Vadimom rodicia branili a tak sa na protest Brigitte pokusila v sestnastich rokoch o samovrazdu. O dva roky neskor sa par zosobasil.  Vo filme sa prvykrat objavila v roku 1952 ("Le Trou normand"), no bez vyrazneho uspechu. To ju vsak neodradilo a v hrani pokracovala aj v nasledujucich rokoch, pricom stretavala coraz viac filmovych hviezd. Az prisiel rok 1956 a Vadim ju postavil do filmu "A boh... stvoril zenu". Mytus zvany "BB" bol na svete: Brigitte zacala modu kratkych siat, drdolov a baleriniek a vsetky Francuzky chceli byt ako ona. Vo filmoch, ktore prichadzali, hrala novy typ francuzskej zeny - zeny oslobodenej, zvadzajucej, citlivej a slobodnej.      Vzdy hrala len ulohy, ktore sa jej charakterovo podobali. Sama priznavala, ze ine by zahrat nevedela. V roku 1959 sa po druhykrat vydala (za herca Jacquesa Charriera) a v roku 1961 sa stala matkou. Fakt, ze otehotnela a samotne materstvo ju doslova rozhadzali a po narodeni syna Nicolasa sa podruhykrat pokusila o samovrazdu.  Po cele roky mala vsetko, po com tuzia mnohe a mnohi - slavu, peniaze, uspech,... Ale nikdy ju to naozaj neuspokojovalo, naopak, tvrdila, ze ten humbuk okolo nej ju znepokojuje. V roku 1973 sa rozhodla s filmovanim skoncit a zacat uplne novy zivot venovany ochrane zvierat, najma zachrane tulenich mladat.  Po skonceni kariery zacala pisat svoje pamate, aby ukazala zo seba viac pre tych, ktori v nej videli len pekny obrazok. Bola to aj ona, ktora stala za "oslobodenim" francuzskej zeny, napriek tomu sa sama nikdy necitila feministkou. Velka obdivovatelka Charlesa de Gaula, odsudzujuca upadok mravov a laxizmus sucasnej francuzskej vychovy. Kontroverzna svojimi vyrokmi na adresu francuzskych moslimov a homosexualov. Odmietajuca byt zapisana vo francuzskom socialnom systeme. Neschopna upnut sa na vlastne dieta, zato rozdavajuca lasku zvieratam. Plna protikladov, v 75 rokoch sa, kedysi jedna z najkrajsich zien na svete, dnes vobec nesnazi zakryvat svoj vek.    Nemozem sa pochvalit tym, ze som videla filmy, v ktorych hrala a ani nepatrim medzi jej velkych obdivovatelov. V kazdom pripade si myslim, ze Brigitte Bardot je zaujimava osobka hodna zmienky. Zena, ktora mala v mladosti cely svet pri nohach, nikdy vsak nenasla stastie, zije vo svojich 75 rokoch cudackym a utiahnutym zivotom.           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Jablonska 
                                        
                                            Ako Stella komunikovat s klientom nevedela
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Jablonska 
                                        
                                            List konkretnemu do nekonkretna (po piatich rokoch)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Jablonska 
                                        
                                            Štafeta za život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Jablonska 
                                        
                                            Aznavour nazivo (druhykrat)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Jablonska 
                                        
                                            Zle spravy nepockali
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Jablonska
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Jablonska
            
         
        jablonska.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pozeram filmy Woodyho Allena. Pocuvam Gainsbourga. Citam Marqueza.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    100
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2499
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kuba
                        
                     
                                     
                        
                            Peru
                        
                     
                                     
                        
                            Takmersvokrovci
                        
                     
                                     
                        
                            U nas v Luxemburgu
                        
                     
                                     
                        
                            Ulety
                        
                     
                                     
                        
                            Moje najmilsie / Moji najmilsi
                        
                     
                                     
                        
                            Male cesty
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Kamila od veternych mlynov
                                     
                                                                             
                                            Kaya Africka
                                     
                                                                             
                                            Katarinka od Presova
                                     
                                                                             
                                            Medved a Macko
                                     
                                                                             
                                            Matka Zuzana
                                     
                                                                             
                                            Moj naj
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Sietofka
                                     
                                                                             
                                            Flash Lucia
                                     
                                                                             
                                            Flash Igor
                                     
                                                                             
                                            Malovanky
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Skúste budúci rok Anglesey
                     
                                                         
                       Loďkou po Mekongu z Kambodže do Vietnamu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




