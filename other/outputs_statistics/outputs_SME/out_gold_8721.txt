

 V prvom rade by som chcel v tomto článku poďakovať všetkým voličom SDKÚ-DS, SaS, KDH a Mostu-Híd, za to, že poslali Fica, Kaliňáka, Tomanovú, Maďariča, Jahňátka, Slotu, Rafaja atď. atď. atď. do opozície a Vladimíra Mečiara, Harabina a spol. na smetisko dejín. Fico bude štekať, pripravovať opäť referendum o predčasných voľbách, bude však štekať ako obyčajný poslanec NR SR sediaci v opozičných ľaviciach. 
 V druhom rade by som chcel poďakovať všetkým voličom (presne 469), ktorí ma krúžkovali na kandidátnej listine SDKÚ-DS, čo mi v hlasoch pomohlo k peknému skoku zo 141. miesta na 87. miesto, v Bratislave dokonca na pekné 39. miesto. Ďakujem Vám veľmi pekne za podporu, veľmi si ju vážim a zaväzuje ma  smerom do budúcnosti. 
 Daný výsledok má teší aj z toho dôvodu, že som získal o skoro 200 krúžkov viac ako starosta Rače, momentálne dosluhujúci poslanec NR SR za Smer-SD, Ján Zvonár (nehovoriac už o pomere v prepočte na celkový počet hlasov), v samotnej Bratislave som ho porazil o 64 hlasov. Ján Zvonár ako poslanec NR SR skončil a verím, že sa nám v komunálnych voľbách v novembri podarí mu v Rači zbaliť veci do krabice a poslať kade ľahšie. 
 Priatelia, už dávno sa mi nezobúdzalo tak dobre ako dnes, verím, že aj Vám. Teším sa, ako spoločne posunieme Slovensko opäť dopredu. Vulgarizmus, papalášske maniere, hľadanie triedneho nepriateľa, rozkrádanie a zadlžovanie dostali jasnú červenú kartu! 
 PS: V rámci maximálnej transparentnosti zverejňujem celkové výdavky na moju kampaň - 26,28 € (792 SKK). 

