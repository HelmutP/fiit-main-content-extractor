

 Referendum v roku 1978 bolo kritickým zlomom pre budúcnosť jadrovej energetiky v Rakúsku. 50,47% bolo proti spusteniu a 49,53% za. O budúcnosti energetického vývoja tak rozhodlo približne 1% voličov. Priamo počas navážania paliva sa celé spustenie odložilo a reaktorový sál sa už nikdy nedočká svojej premiery. 
 Elektráreň momentálne slúži ako výukové centrum, väčšina súčastí je zakonzervovaná a niektoré časti boli predané do Nemecka do podobných typov elektrární. Priamo počas prehliadky bolo zrejmé, že celá budova a všetky zariadenia sú vo vynikajúcom stave odhliadnuc od zriedkavej korózie na niektorých ventiloch. Hneď vedľa Zwentendorfu bola postavená tepelná elektráreň Dürnrohr. V jadrovej elektrárni nebola nameraná žiadna radiácia, za to Dürnrohr, ktorý spaľuje uhlie, jej vypustí viac do ovzdušia aj v súčasnosti. V blízkosti areálu je taktiež vybudovaná zatiaľ malá slenečná elektráreň, no plánuje sa s jej rozšírením. 
 Reaktor typu BWR 
 Je tepelný varný reaktor, ktorý na chladenie i moderovanie využíva obyčajnú vodu. V hornej časti reaktorovej nádoby vzniká para, ktorá poháňa turbínu a po ochladení sa vracia späť na chladenie aktívnej zóny. Para sa ochladzuje až za turbínou, čo je nevýhoda oproti reaktorom so sekundárnym okruhom - pri okruhu BWR je turbína vystavená priamo rádioaktívnej pare. Najviac reaktorov tohto typu nájdeme stále funkčných v USA, Nemecku a Japonsku. Voda na chladenie primárneho okruhu sa mala získavať priamo z Dunaja, odkiaľ bola za pomoci veľkých púmp poháňaná cez sekundárny okruh a chladiacu vežu. Po ochladení sa táto voda mala vrátiť späť do Dunaja. 
     
     
     
 Zwentendorf ako miesto pre cvičenia 
 Pri návšteve elektrárne je hneď zrejmé, že nie je v prevádzke - žiadny personál, prázdne chodby, ticho, bez radiačných kontrol a prehliadok. Vstúpite kde chcete a kedy chcete. Nepočuť vzduchotechniku. Pod reaktorom je príjemne teplo - je tam elektrický ohrievač, ktorý ako-tak zabezpečuje vlhkosť vzduchu kvôli konzervácii súčiastok. Že bola stavba dokončená pred tridsiatimi rokmi je hneď poznať - starodávne telefóny, vyblednuté farby na stenách, chátrajúce elektrorozvody. Bolo by možné po rozsiahlych úpravách elektráreň znovu uviesť do prevádzky? Nevedno. Priamo v reaktorovom sále či v strede štítu biologickej ochrany sú vyrezané veľké dvere pre návštevníkov. Tu sú verejné aj tie miesta, kde v bežných elektrárňach nie je prístup, respektíve len na veľmi krátku dobu počas odstávok - no nepoznám prípady že by to niekto riskoval. 
     
     
     
   
 



 
 Miesto spoločenských podujatí 
 Turbína s genetárom je v rozsiahlej hale, ktorú v budúcnosti plánujú prenajímať na rôzne spoločenské akcie. Priamo pred elektrárňou sa často konajú rôzne hudobné alebo enviromentálne podujatia, na ktoré prichádza množsto ľudí. Po Černobyľskej havárii v roku 1986 Rakúšania ani nerozmýšľajú nad tým, že by vôbec niekedy spustili túto či inú elektráreň do prevádzky. Stretávajú sa preto pri Zwentendorfe aby verejne prezentovali svoje postoje a názory na jadrovú energetiku. 
 Vedeli ste vôbec, že Rakúsko malo (má) jadrovú elektráreň? 
 Do Zwentendorfu by som sa rád ešte vrátil. Je to vynikajúce miesto pre štúdium techniky ako takej a vôbec jadrovej elektrárne zblízka. Technika je v úžasne zachovalom stave, akoby všetko čakalo na spustenie zavážania paliva. Je to jedno z najväčších unikátnych múzeí svetového významu. A nebude sa nikdy rozoberať. 

