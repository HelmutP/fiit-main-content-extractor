
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boba Baluchová
                                        &gt;
                Globálne témy
                     
                 Ako rozpoznať rodovo (ne)citlivú tvorbu? 

        
            
                                    24.3.2010
            o
            20:50
                        (upravené
                29.4.2014
                o
                9:15)
                        |
            Karma článku:
                5.99
            |
            Prečítané 
            966-krát
                    
         
     
         
             

                 
                    Ak na neznámych občas vypustím slovné spojenia, ako: rodové stereotypy v učebniciach, rodová rovnosť v zamestnaní, tréning rodovo-citlivé písania na školách, diskriminácia žien v médiách, či sexizmus v reklame – zľaknú sa. Aj vy ste sa? Hlavu hore, oči v strehu: feminizmus nie je synonymom pre fanatické názorové hnutie, feministka nie je nadávka a rodová rovnosť nie je výmysel Európskej únie.
                 

                 Toto sa nesnaží byť komplexným pohľadom na rodovú problematiku, len vypichnutie pár tém, ktoré ľudia potrebujú pretriasať (a do krvi sa pohádať) v bare pri víne, keď dôjde reč na porovnanie zafixovaných vlastností žien a mužov. Ak ho dočítate zodpovedne dokonca, tak sa pravdepodobne nachádzam v raji – takom tom tematickom: rodovo-citlivom, rovnovážnom, nediskriminujúcom. Lebo keď sa z desiatich oslovených aspoň jeden/jedna nad témou zamyslí či dokonca pochopí podstatu rodovej problematiky, potrebu rodovej rovnosti (nielen na Slovensku) a snahu ženských združení, budem spokojná. Aspoň chvíľu.           Racionálni muži a emocionálne ženy?       Pojmy, ako: feminizmus, rodovosť, sexualita, či rodové stereotypy sa vyskytujú najmä v komunikácii o rozdielnostiach medzi mužmi a ženami. Netreba ich hľadať v encyklopédiách, ale v reálnom každodennom živote. Navštevovať gender-studies na univerzite nie je módna záležitosť, ale potreba. Od členov/iek spoločnosti sa akoby očakávalo, že sa budú správať v súlade s relatívne stabilnými vzorcami správania a vzájomnej interakcie. Problémom pretrvávania rodových stereotypov je práve to, že mužskosť a ženskosť sú definované ako protiklady, obsahujúce v sebe automatický predpoklad, že (všetci) muži sú (výhradne) mužskí a (všetky) ženy (výhradne) ženské. Akékoľvek vybočenie z predpísaného správania sa považuje za nenormálne a je trestané: výsmechom, odsudzovaním, vylúčením z komunity.   Rôzne kultúry delia činnosti na „vhodné pre mužov“ a „vhodné pre ženy“. Nie náhodou sa všade stretávame s: racionálnymi „mužskými“ charakteristikami, spojenými s kompetenciou (nezávislosť, ovládanie, kontrola, rozhodovanie); a emocionálnymi „ženskými“ charakteristikami, vyjadrujúcimi zameranie na medziľudské vzťahy (pokoj, prispôsobivosť, starostlivosť). Ak teda viem narábať s príklepovou vŕtačkou, budem pravdepodobne nenormálna alebo mužatka, že? Ak muž pečie vianočné pečivo, je nedocenený podpapučník alebo gej, hej? L           Mamy varia, tatovia čítajú noviny (a nikdy inak?)       „Tradičné“ obrazy o správaní aj konaní žien a mužov sú deťom tlačené do hláv už prostredníctvom učebníc, neskôr prostredníctvom médií. Už v predškolskom a školskom veku sme sa dozvedali o obraze nás samých: „dievčatá sa obliekajú do ružovej farby a hrajú sa s bábikami, pričom chlapci sa obliekajú do farby modrej a hrajú sa s autíčkami“. Takže ak sa moja sestra hrá na pieskovisku s autíčkom, treba ju dať vyšetriť, a ak chce susedov syn chodiť na balet, treba volať psychológa/ičku?   A čo s ďalším obrazom o našich rodičoch? V šlabikári vidíme čierne na bielom: „mama varí za sporákom a otec číta v kresle noviny“. Dôsledkami stereotypného uvažovania a konania na rôznych úrovniach je faktická nerovnosť žien a mužov. Vedľa problematiky (ne)rovných príležitostí žien a mužov v rodine, zamestnaní či v politike je to napríklad aj problém násilia, páchaného na ženách, u ktorého je ako jedna zo základných príčin identifikovaná: všeobecne prijímaná predstava o role a autorite mužov – ideálov mužskej moci a dominancie, tvrdosti a cti. Treba spomenúť, že kampaň „Piata žena“ (o každej piatej Slovenke – týranej mužom) mala byť vlastne nazvaná: „Štvrtá žena“.   Sledujme reklamné televízne spoty či reklamné fotografie. Vzhľad mužov a ich pozíciu voči ženám. Muži sú výrazne silnejší, vyšší než ženy, stoja nad ženami, sedia vzpriamene, majetnícky partnerku držia, sú bez úsmevu (drsoňský pohľad!) a takmer vždy oblečení až po krk. Vyobrazované ženy vyjadrujú svojím postojom neistotu, krehkosť, zasnenie (prižmúrené oči a pootvorené ústa!), citlivosť, nakláňajú hlavu, nastavujú na obdiv svoje telo, málokedy zahalené. Muž-manažér, odborník, riaditeľ firmy v obleku múdro vysvetľuje, vážne poučuje, zatiaľčo žena-sekretárka, asistentka, ošetrovateľka, matka naslúcha, prikyvuje a vykonáva pokyny. Rodová (genderová) stereotypizácia v reklame je neprehliadnuteľná.           Muži divákmi, ženy tiež divákmi (nie diváčkami?)       Medzi európskymi jazykmi sú jazyky, ktoré prihliadajú na pohlavia v spoločnosti. Čo sa stavby koncoviek podľa pohlavia týka, tak angličtina je úplne bez reflexie na pohlavie, ale zasa slovenčina rozlišuje pomenovanie pohlaví. Tak prečo to nevyužívať? Zdržuje nás to? Divný argument. Mne to problém nerobí, komu áno? Buď napíšem o vás, že ste čitateľská obec, alebo čitatelia a čitateľky, alebo najčastejšia možnosť: čitatelia/ky – žiadne komplikované premýšlanie či zdržovanie (aj keď lomka nevyzerá typograficky dvakrát sexi – tentoraz nejde o formu). Rodová citlivosť v jazyku je predovšetkým o obsahu! Podľa odborníčok na rodovo-citlivý jazyk používanie generického maskulína a považovanie mužského rodu za normu zabraňuje zrovnoprávneniu žien (nielen v jazyku).    V slovenskom jazyku je veľmi časté a bežné nadmerné používanie generického maskulína v oslovovaniach, titulovaniach, jazyku formulárov, úradnom a právnom jazyku, taktiež na vizitkách, v novinových titulkoch, ale aj v hovorovej reči. Napríklad po ukončení vysokoškolského štúdia sa žena stáva „magistrom“, v zamestnaní sa na zmluve podpisuje žena do kolonky „podpis zamestnanca“, a na dvere si žena dáva vývesný štít „advokát“. Mnohokrát sú ženy úplne vynechávané a javy sú adresované len pre mužského adresáta. Nikoho neprekvapia oslovenia: „vážení diváci, milí čitatelia“, a to aj v čisto dámskom magazíne, či „naši verní zákazníci“ v reklamom texte.           Spočítané ženské autorky (na koľkých prstoch?)       Keď počujem v rádiu ženu, ako hrdo hovorí o sebe, že je „súkromný podnikateľ“ – neverím vlastným ušiam. Asi aj doma je otcom, nie matkou. Alebo rovno dedkom, nie babkou. Zamrzelo ma tiež, keď pri preberaní dôležitej novinárskej ceny hovorila o sebe Zlatica Puškárová ako o novinárovi. Je ťažké pridať k slovu tie dve hlásky a povedať: novinárka? Ešteže sa tímu výborných basketbalistiek už nehovorí mužstvo, lebo to by som asi nerozdýchala. Ak používame pri oslovovaní dievčat aj chlapcov iba oslovenia v mužskom rode, potvrdzujeme tým rodové stereotypy, to je bez debaty.   Ak hovoríme napríklad o starostlivosti o deti, mali by sme zdôrazniť, že muži môžu vykonávať túto činnosť rovnako ako ženy – teda žiadne materské dovolenky, ale rodičovské dovolenky! Otázka je, koľko mužov zo svojho okolia na rodičovskej dovolenke po narodení dieťaťa poznáte. A nasleduje ďalší test pozornosti vášho okolia! Výhodou je, ak tušíte o existencii záujmového združenia žien na Slovensku Aspekt, ktoré sa nehemží militantnými feministkami (ako sa niektorí ľudia predpojato obávajú), ale rozhľadenými autorkami a rodovými expertkami. Koľko ženských režisérok poznáte? Koľko diel od spisovateliek máte vo svojej knižnici? Zdvihnú sa vám všetky prsty aspoň na jednej ruke? Kiežby.    Čo sa týka vzhľadu: na ženy sú kladené neporovnateľne vyššie nároky než na mužov, a médiá tento stav odrážajú sústavným hodnotením – ako spĺňame ideály krásy a femininity. Tým opäť napomáhajú reprodukcii mýtov o objektívnej kráse. Hoci to nie je pravda, médiami spoluvytváraný mýtus krásy obmedzuje slobodu a možnosti žien, podkopáva ich sebavedomie. Určite ste si všimli, že ženská krása je v našej kultúre konštruovaná ako mladá krása, a teda profesná kariéra žien v médiách (na obrazovke či na stránkach časopisov) je výrazne kratšia. Kdežto u mužov to neplatí, viď prešedivelí, postarší moderátori televíznych novín.           Sexizmus v reklame (budeme ho prehliadať?)       Tvorcovia/kyne reklamy sa často odvolávajú na to, že verejnosti predkladajú len to, čo sa od nich očakáva a za čo si zadávatelia inzercie zaplatia. To je ako s tou sliepkou a vajcom – čo bolo skôr, čo z čoho vzniklo a kde je problém. Ako veľmi teda reklama (a médiá všeobecne) vo svojich výstupoch realitu len odráža a ako aktívne ju (spolu)utvára – toť je rečnícka otázka. Reklamu totiž nielenže prestupujú rodové stereotypy, ale stereotypné roly v reklame často spätne upevňujú rodovo-stereotypné postoje spoločnosti. A ako silno-zviazaná je spojitosť medzi reklamou a postojmi k sexuálnemu násiliu? Stačí prebehnúť pár zahraničných výskumov a úsmev nám zamrzne. Rodovo stereotypné postoje upevňujú a reprodukujú mýty, ktoré panujú o násilí na ženách, dokonca ho častejšie iniciujú.    A čo taký dosah reklamy a publikované texty v ženských a dievčenských časopisoch na sebavnímanie žien – najmä na ich vzhľad, telesnú váhu? Žiadne chudnutie až poruchy príjmu potravy? Nie náhodou majú tieto magazíny desať-krát viac inzercie a článkov, propagujúcich prostriedky na chudnutie, starobu či nadváhu redukujúce kozmetické prípravky, alebo rovno plastickú chirurgiu, než mužské časopisy. Stačí si ich prelistovať – stovky ich ležia v regáloch nákupných centier a čakajú každý mesiac na svoje zákazníčky.   Sexizmus je diskriminácia na základe pohlavia, pričom v kontexte našej spoločnosti sú znevýhodnenou skupinou predovšetkým ženy. Základom pre sexistické jednanie či vyjadrovanie sú rodové stereotypy – zjednodušujúce predstavy o ženách a mužoch. Koľko-krát v živote ste sa stretli s tvrdeniami, že „ženy nevedia šoférovať autá“ a „muži predsa neplačú“?           Ženu nemožno vyfúknuť (dá sa to pochopiť bez požitia alkoholu?)       Práve reklama je jednou z oblastí, ktorá využíva sexistické vyjadrovanie a zobrazovanie. Cielene pracuje s tradičnými rolami a rodovo-stereotypnými vlastnosťami, využíva prvoplánové a dvojzmyselné slogany, používa telá ako objekty na upútanie pozornosti bez ohľadu na chýbajúcu súvislosť s propagovaným výrobkom. Za posledné obdobie ma asi najviac šokoval plagát zo susedného Česka s nahou ženou v neprirodzenej póze, ponúkajúci spracovanie kovov (hliníkových a pozinkovaných dielov) – s akože-vtipným sloganom: „přeřízneme a ohneme vše dle vašeho přání“. No, uff, ďakujem pekne.   Za sexizmus v reklame možno považovať, ak modelka v reklame má len dekoratívnu rolu, nemá žiadny iný vzťah k výrobku a je ukazovaná len z dôvodu svojej telesnej príťažlivosti a sex-appealu. Koľko párov očí upúta billboard s nahou ženou a koľko obrázok šedého pletiva či strešnej krytiny?   Azda najbúrlivejšiu diskusiu (v kruhoch rodových expertov/iek, členov/iek Rady pre reklamu aj u verejnosti) na tému rodovo-necitlivej a diskriminujúcej reklamy spustil televízny spot na tmavý alkohol v roku 2007. Určite sa na ten spot i vlnu nepokoja pamätáte! Dvojica sedí na deke na letnej pláži: žena partnerovi vyčíta, ten otvorí ventil na jej krku a vypustí z nej vzduch ako z nafukovacej panny. Bol to exemplárny príklad sexizmu v reklame: ženu môže muž kedykoľvek umlčať? Na prvé pozretie vám to príde možno vtipné, ale potom nastane smutné precitnutie.           Skutočná krása je krásna (zvykneme si na to?)       Máte prelúskaný Mýtus krásy v knižnej podobe? Nájdete tam zmienku aj o tom, že starnutie je „škaredé“, pretože rokmi sa ženy stávajú mocnejšími. Pritom mladosť žien sa považuje za „krásne“, pretože znamená sexuálnu neznalosť a neskúsenosť. Lenže krásu treba vidieť vo všetkých a odprezentovať ju verejne na prirodzených/obyčajných ľuďoch!   Pamätáte si kozmetickú značku (s labuťou na mydlách), ktorá odštartovala pred pár rokmi úspešnú „Kampaň za skutočnú krásu“? Práve táto firma sa vymanila zo stereotypného nanucovania súčasných ideálov krásy. Odvážila sa ukázať svetu reálnu krásu naozajstných žien, ktoré majú plnšie či nesúmerné tvary, pehy alebo vrásky na tvári. Odštartoval sa tak nový trend prezentácie spokojných žien (takých, aké sú) a pomaly začal nahlodávať kult mediálneho klamstva o žene/ženách. Dosť bolo nereálnych a vyretušovaných fotografií neskutočných krások. Rozpútala sa verejná diskusia o formách skutočnej krásy žien.    Médiá zohrávajú v uplatňovaní rodovo-citlivého prístupu zásadnú úlohu. Aj vy to môžete v budúcnosti skúsiť. Ako píšuci ľudia či ako spätnú väzbu dávajúci čitatelia/ky. Pre začiatok napríklad používaním rodovo-citlivého jazyka a zamýšlaním sa nad výberom fotiek k jednotlivým článkom či do konkrétnych reklám. Môžete sa prejaviť aj tak, že sexizmus vyjadrujúce a diskrimináciu žien prezentujúce periodikum svojou kúpou nepodporíte. Ale Inspire si kúpte aj o tri mesiace, inak ma šéfredaktorka hodí na „black-list“.    Nadhodila som tu veľmi rýchlo a povrchne veľa tém. O každej sa dá napísať slušná diplomka či dizertačka. Všade samé otázniky. Vo mne stále, vo vás možno tiež. Ono to ale na začiatok stačí. To je ako s trollovaním a podkurovaním vo flejmoch/slovných vojnách na sociálnych sieťach a v diskusiách pod článkami v on-line vydaniach slovenských denníkov – stačí „vhodne“ zareagovať a prúd sa spustí sám. Ono sa ten rodovo-citlivý proces vo vás naštartuje, ak je otvorená myseľ a záujem. U mňa sa tiež naštartoval až počas štúdia na druhej výške. Ale nikdy nie je neskoro. Na nič! A to som tým chcela vlastne povedať. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Neseďte na korytnačkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Dobrý projekt jedna krádež nezastaví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Oslepnúť na celý život pre nevedomosť dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Dojčiť – radšej všade, ako nikdy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Pytliakmi proti svojej vôli
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boba Baluchová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boba Baluchová
            
         
        bobabaluchova.blog.sme.sk (rss)
         
                        VIP
                             
     
        nekončiaca študentka a pedagogička, novinárka aj filmárka, pracujúca v roku 2013 ako terénna rozvojová pracovníčka v Keni - snažiaca sa hovoriť o témach, čo sa dejú, čo nás zaujímajú i trápia... http://twitter.com/bobinkha
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    36
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestopis
                        
                     
                                     
                        
                            Kinematografia
                        
                     
                                     
                        
                            Reportáže
                        
                     
                                     
                        
                            Lekcie rozvoja
                        
                     
                                     
                        
                            Globálne témy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            O médiách
                                     
                                                                             
                                            Týždeň v nových médiách
                                     
                                                                             
                                            SPW
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




