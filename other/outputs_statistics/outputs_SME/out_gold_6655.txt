

 Si scenárista... 
 svoj príbeh píšeš sám 
 réžiu vkladáš do rúk Života 
 pamäť velí spomienkam 
   
 Píš scenáre 
 aké najlepšie vieš 
 režíruj svoje priania 
 život je prechádzka 
 bez možnosti gumovania 
   
 Choď za každou dúhou 
 naplň svoj tajný sen 
 raduj sa z toho  
  čo máš, nežiaľ, 
 že niečo postrádaš 
   
 Život je prax 
 nie teória 
 nenaučíš sa mu z kníh... (Vajanský) 
   
 Hýčkaj lásku 
 dôveru neroztop 
 soľ život humorom 
 cukruj romantiku 
   
 Ideál je v Tebe 
 prekážky tiež 
 celý vesmír sa spojí 
 keď niečo veľmi chceš   
 ...a pomôže ti (Coelho)  
   
   
 *** 
   
 E. Roosevelt: Budúcnosť patrí tým, ktorí veria kráse svojich snov. 
 L. da Vinci: Voda v rieke, ktorej sa dotýkaš, je posledná odtekajúca a prvá, čo priteká. Tak je to aj s prítomnosťou. 
   
   
   
   
   
   
   
   
   
   
   

