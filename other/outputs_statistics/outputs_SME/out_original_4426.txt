
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Libor Lukáč
                                        &gt;
                Nezaradené
                     
                 Autority nemajú vždy pravdu! 

        
            
                                    12.4.2010
            o
            17:01
                        |
            Karma článku:
                2.20
            |
            Prečítané 
            390-krát
                    
         
     
         
             

                 
                       Domnienka kresťanských cirkví, že svet bol spasený krížom a vzkriesením Krista je nesprávna! Táto téza je tragickým omylom, ba čo viac, je jednou z hlavných príčin neustále sa prehlbujúceho duchovného, morálneho a mravného úpadku civilizácie na planéte Zem.
                 

                        Človek, ktorý tvrdí, že náš svet bol spasený Kristovou smrťou na kríži a jeho utrpením musí byť pravdepodobne slepý, keď pod vonkajším, ligotavým pláštikom súčasnej spoločnosti nevidí a nevníma ten desivý úpadok všetkých vyšších hodnôt a všeobecný rozklad elementárnej ľudskosti. Vari má takto naozaj vyzerať svet, ktorý bol spasený?           Nie, tisíckrát nie! Realita, ktorú každodenne prežívame, je sama o sebe až príliš očividným svedectvom toho, že spása a spasenie musí predsa len tkvieť v niečom úplne inom, čo by sa zásadne omnoho pozitívnejším spôsobom odzrkadlilo v každodennom živote jednotlivcov i celej spoločnosti.           Dôrazne preto treba povedať: Súčasné ponímanie spásy a spasenia je skazonosný a nebezpečný blud! Je to smrteľný uspávací prostriedok, prostredníctvom ktorého bola šikovne využitá duchovná pohodlnosť ľudí za účelom získavania a rozširovania priaznivcov kresťanských cirkví. Pozornosť veriacich však bola týmto spôsobom odvedená od toho hlavného a najpodstatnejšieho! Od poznania, v čom je spása naozaj skrytá!          Spása nespočíva v Kristovej smrti a zmŕtvychvstaní! Spočíva jedine v Kristovom Slove, v ktorom nám bolo jasne ukázané, ako máme myslieť, cítiť, hovoriť a konať. Kto príjme toto Slovo a bude podľa neho naozaj žiť, ten zaručene dôjde k spáse. Iná cesta k nej nevedie a nikdy ani neviedla! Keby bol práve na toto cirkvami dávaný hlavný a jediný dôraz, dnes by pravdepodobne všetko na našej planéte vyzeralo úplne inak. Určite rozhodne lepšie! Žiaľ, dôraz sa však kládol na niečo úplne iné a týmto spôsobom bola výhybka, vedúca k spaseniu, nastavená nesprávnym smerom.          Skúsme nad tým už raz vážne pouvažovať! Naša spása spočíva jedine v živote podľa Slova Pravdy, v živote podľa učenia Ježiša Krista! K spáse teda môže dospieť iba ten, kto sa snaží žiť tak, ako nám ukázal Spasiteľ. Nikto iný!          Ľudia vo všeobecnosti i celý svet však nežijú podľa Spasiteľových slov a preto sú na míle vzdialení od spásy. Ba práve naopak, kráčajú k záhube! K svojej osobnej i k záhube celej civilizácie! Tvrdiť v takejto situácii, že svet už bol spasený Kristovým krížom a zmŕtvychvstaním je doslova absurdné.          Veď ľudstvo vpodstate ešte doteraz vôbec nepochopilo a nezrealizovalo najdôležitejší princíp Kristovej náuky, spočívajúci v zásade, že voči iným sa máme chovať tak, ako chceme, aby sa iní chovali voči nám. Svet sa v skutočnosti vôbec neriadi Synom Božím ukázaným princípom pomáhajúcej lásky, vzájomnej podpory, úcty a ľudskej spolupatričnosti. A žiaľ, nemalú vinu na tomto stave majú všetky kresťanské cirkvi, ktoré svojim veriacim správnym spôsobom neukazovali, ako skutočne dospieť k spáse. Tie kresťanské cirkvi, ktoré ešte doteraz mylne prehlasujú, že spása spočíva v smrti a vzkriesení Ježiša Krista. Je to lož, pretože skutočná spása spočíva jedine v živote podľa Kristovho učenia a v ničom inom!           Podobné bludy mohli a doteraz môžu cirkvi hlásať iba preto, lebo väčšina veriacich nemala odvahu, čas a ani chuť hlbšie sa zamyslieť nad tým, čo im je ich cirkevnou vrchnosťou predkladané. Slepo a bez vlastného premýšľania, či zvažovania iba všetko apaticky prijímali. To je ale nesprávne!           Ľudia by už konečne mali začať samostatne uvažovať, hľadať a nakoniec i nájsť spásu tam, kde naozaj je: V živote podľa učenia Ježiša Krista! Ak to pochopia a zrealizujú, veľmi pozitívne sa to prejaví nie len na kvalite ich vlastného, osobného života, ale aj na zlepšení kvality medziľudských vzťahov v ich najbližšom okolí a nakoniec i v celej ľudskej spoločnosti. A až bude týmto spôsobom žiť väčšina ľudí na našej planéte, iba potom možno hovoriť o reálnej nádeji na spásu tohto sveta.       L.L., sympatizant Slovenského občianskeho združenia pre posilňovanie mravov a ľudskosti    http://www.pre-ludskost.sk/         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Televízia – hriech náš každodenný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Iný pohľad na katastrofu v Japonsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            O radosti a bolesti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Zahynieme na devalváciu slova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Katastrofa v Japonsku – hlas Matky prírody!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Libor Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Libor Lukáč
            
         
        liborlukac.blog.sme.sk (rss)
         
                                     
     
        Snažím sa byť človekom s vlastným pohľadom na realitu. Emailový kontakt: libor.lukac@atlas.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    504
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




