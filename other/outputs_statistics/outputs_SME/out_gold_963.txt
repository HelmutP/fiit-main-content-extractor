

 
Naša rumunská lektorka bola prekvapená, keď videla spolužiakov, ako si zaliali kávu, čo to robia a dôrazne protestovala proti nazývaniu takej kávy tureckou. Súhlasím, zalievaná káva ma neoslnila a dlho som kávu vôbec nepila. Naozajstná turecká káva - v Grécku samozrejme nazývaná grécka káva - je pochúťkou pre labužníkov. Je silnejšia, hustejšia a delikátnejšia. 
Turecká káva sa dá pripraviť doma aj bez klasickej džezvy, v odchodoch s domácimi potrebami dostať aj moderné džezvy z nerezu s plastovými rúčkami, alebo postačí aj obyčajný malý hrnček. Klasická džezva (nazývaná aj ibrik) má však výhodu v špeciálnom tvare, ktorý zadrží zomletú kávu v nádobe a do šáločky prenikne minimum usadeniny. 
 
 
 
moderné a tradičné džezvy  
 
 
Príprava tureckej kávy 
Turecká káva by sa dala prirovnať k zákusku, pije sa z drobných šáločiek - nie z čajových dvojdecových šálok. Klasické džezvy sú vyrábané vo viacerých veľkostiach podľa počtu porcií, ktoré sa chystáme podávať. Počet porcií poznáme podľa čísla vyrazeného na dne typickej džezvy. 
Primerané množstvo kávy (prax robí majstra) treba na jemno zomlieť - ideálne tesne pred prípravou kávy. Do džezvy naberieme vodu, pridáme pomletú kávu a prípadne aj cukor. Turecká káva chutí lepšie sladšia, aj ľuďom, ktorí sú zvyknutí piť zalievanú kávu nesladenú. 
 
 
 
 
 
Kávu nenecháme vrieť, ale keď sa spení, stiahneme ju z horáka, počkáme, kým pena klesne - penu možno aj odobrať a rozdeliť do šáločiek. Spenenie opakujeme trikrát, chvíľu počkáme, kým sa mleté zrnká usadia a kávu z džezvy pomaly nalievame do šáločiek. 
 

	
		
			 

			
			 
			  
			 
			
		
	

 
Nevyhnutným doplnkom každej kávy je pohár vody. 
Do vody pred varením spolu s kávou a cukrom môžme pridať drvený kardamóm alebo mletú škoricu. 
 
 
 
kardamómové zrno  
 
 
 
 
podávanie tureckej kávy  
 
 
Arabská káva 
Príprava a podávanie arabskej kávy je obrad. Slušnosťou je ponúkať hosťovi nepárny počet šálok - samozrejme, pravou rukou. Arabská káva by sa nemala sladiť cukrom a dolievať mliekom. Podáva sa tradične s datlami, ktoré obsahujú veľa cukru. 
Podľa tradičného obradu sa používajú nepražené kávové zrná, ktoré sa tesne pred prípravou qahwy opražia na kovovej panvici s dlhou rúčkou nad ohňom, vychladnuté sa nasypú do mažiara a podrvia. Arabská káva sa nevarí v džezve, ale v nádobe pripomínajúcej čajník, ktorá sa volá dallah. 
 
 
 
dallah, rôzne typy, v popredí staršia, ktorá má ešte džezvovitý tvar  
 
  
 
 
 
 
 
 
na hrubo podrvená káva v mažiari   
 
 
Dallah sa naplní vodou a tesne pred zovretím do vody nasypeme opraženú podrvenú kávu a podrvený kardamóm. Oheň, plyn alebo elektrický horák stiahneme na minimum a varíme. Približne desať minút. Ako pri tureckej káve, necháme zrná usadiť sa a dallu chytíme do ľavej ruky a polmesiacovitým hrdlom nalievame kávu do šáločiek. Podávame hosťom pravou rukou a sprava doľava. Kávu by mala podávať najmladšia osoba. Nalieva sa asi do polovice šáločky. 
 
 
 

	
		
			 

			 

		
	


 
Saudi túto kávu pijú až prekardamómovanú, pomer kávy a kardamómu upravíme podľa chuti. Na začiatok je asi najlepšie začať s minimom kardamómu, má výraznú chuť. Okrem kardamómu možno použiť škoricu alebo šafran.  
 
 
 
usadenina na dne šálky - minimum, na rozdiel od zalievanej "tureckej" kávy  
 

