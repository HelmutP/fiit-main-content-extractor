

 Vyrástol som. Už si nemyslím, že medvede a vlci žerú ľudí a že politici sú slušní ľudia. Zistil som, že môj pradedo – chmurnik – bol zázračný človek, ktorý vysoko prekonával presnosťou predpovedí celý Slovenský hydrometeorologický ústav. Štyri mačety a mapy Austrálie a Kanady odpočívajú na dne mojej skrine. To všetko je logické. Čo som nepochopil hneď, je fakt, že ľudia okolo mňa stále považujú lesy za nebezpečné miesto a radšej sa pohybujú v pochybných lokáloch veľkých miest a za najlepší les považujú vyrúbaný hrebeň s ďalekým výhľadom. 
 Bolo mi to dosť nepochopiteľné, pretože aj ten veľký medveď naháňa radšej uja poľovníka s puškou ako nejakého flautistu, ktorý si náhodou nevšimol, že v lese je medvedia razia. Nikde v žiadnom našom, aj najhlbšom lese, som sa nestretol s prípadom, že by vlci selektovali turistov podľa národnosti a nejakú cudzojazyčnú žienku napadli a na tričko jej zubami vykusali nápis „Bež za Dunai“.  
 Potom som pochopil. Všetci ľudia, alebo takmer všetci, vrátane mňa v rannom štádiu vývoja, chodia po lese a okolí od značky k značke, od chaty k chate, zaoberajú sa svojimi dievčatami alebo chlapcami, nesú so sebou problémy z miest a večer to všetko zapíjajú kdesi v teple chát. Milióny bystrušiek, sov, užoviek, rastlín, mravcov a komárov ostáva na vymeranej trase pochodu nepovšimnutých a kruh života, ktorý predstavuje každá divočina, ostáva panenský. 
 Keď som si odškrtol štvrtú tisícku nocí odspaných v lese pod širákom, všimol som si, že som trošku iný. Prestal som robiť čiarky za každú noc v divočine. Romantiku ďalekých výhľadov a dlhé túry vystriedali pobyty na jednom mieste, hodinové pozorovanie bystrušiek či pokojné počúvanie dlhých spevov neznámych vtákov.    
 
  
 Teraz reagujem na množstvo výziev kamarátov, ktorí ma neustále prosia, aby som zverejnil moju metodiku pobytu v slovenskej divočine, aby som odtajnil to čo viem a čo nemôžu nájsť na www.hiking.sk a podobných webových stránkach. 
 Tak dobre. 
 Pre budúci pobyt v divočine je najdôležitejšia dôkladná psychická príprava od najútlejšieho detstva. Ja som začal od svojho prvého roku nestálym kontaktom s najväčšími mysliteľmi ľudstva.  
 
 
 
  
 
 Veľmi dôležité je pravidlené otužovanie. Moji rodičia mi to zaisťovali aktívnym stykom s Dedom Mrázom, ktorý k nám prichádzal z mrazivého Ruska.  
 
  
 Keď sme psychicky a fyzicky dostatočne otužilí, môžeme vyraziť do slovenskej divočiny. Nedoporučujem klásť oheň, zbytočne tým plašíte zvieratá, získavate arómu údenenej slaniny a strácate čas, ktorý by ste ináč mohli venovať pozorovaniu okolia. 
 Výnimkou môžu byť deti, ktoré v mraze spadli do potoka alebo si doma zabudli rukavice. Potom rozloženie ohňa má svoj význam. V tomto prípade oheň zachránil zdravie mojich dvoch dcér na hrebeni Nízkych Tatier.  
  
 Až deti vyrastú a oheň sa stane pre nich úplne zbytočný, na zimné pobyty s rodinou začne postačovať obyčajné iglú.  
  
 Ak sú už deti preč, začne byť aj stavanie iglú zbytočné. Rovnako ako používanie akýchkoľvek stanov či spanie po chatách. Ak spíte na chate, izolujete sa od okolia, nevidíte, nepočujete, nie ste v divočine. Podobne je to aj s pobytom v uzavretom stane, ktorý okrem toho stojí obyčajne nemalé peniaze. 
 Preto na spanie v lesoch používam desiatky rokov obyčajnú plachtu. Máte prehľad po okolí, vidíte a počujete, a hlavne taká plachta je veľmi ľahká. 
  
 Ak nesneží, nepotrebujete ani plachtu a kontakt s divočinou sa stáva úplne dokonalým. 
 
  
 Problémom môže byť pes. Pri teplotách pod mínus dvadsať stupňov je dobre svojho miláčika do niečoho zabaliť. Ja pre svojho Skúkama nosím v takýchto prípadoch aj karimatku. 
  
 Čo sa týka oblečenia, skvelá je obyčajná robotnícka vatová súprava za niekoľko sto korún z remeselníckych potrieb. Je dobré nezabudnúť na čiapku a hlavne rukavice. Ak by ste nemali rukavice, tak v zime, pri dostatočne nízkych teplotách, vám bude k rukám primŕzať pivová fľaša.  
  
 Na pohyb v zimnej divočine sú jednoznačne najvhodnejšie snežnice. Umožňujú vám udržať sa na povrchu aj v náročnom teréne. Ak ich nemáte, v lese, v dostatočne veľkom snehu neurobíte ani krok. 
 
 
  
 Populárne bežky sú dobré do upravenej stopy na Štrbskom plese, ale do skutočného slovenského lesa nie sú veľmi vhodné. Do cesty vám zákerné stromy hádžu rôzne konáre a vo veľkom snehu je každé vstávanie s ťažkým batohom veľmi náročné. 
 
  
 Divoké zvieratá neohrozujú v lese nikoho. Raz za čas môžete pri svojich potulkách stretnúť diviaka uloveného vlkmi. 
 
  
 Okrem diviakov lovia vlci aj jeleňov. Turistu uloveného vlkmi som za štyridsať rokov v lese ešte nenašiel. 
  
 Až uvidíte v lese niekde v snehu alebo v blate takúto stopu, zbystrite pozornosť. Je to medveď. V zime väčšinou polihávajú kdesi v brlohoch, ale mohlo ich niečo vyrušiť a potom sú značne nevrlí. 
 
  
 V prípade výskytu medvedej stopy odporúčam túru smerovať presne opačným smerom ako smeruje medveď. Nie každý je taký kamarát s týmito šelmami tak ako kolega Erik, ktorého pozná v Tichej a Kôprovej doline každý medveď. 
  
 Všetko ostatné je nepodstatné. Variče, spacáky, karimatky, topánky, ponožky či jedlo, si každý zvolí sám podľa seba. Dôležité je v lese prebývať, neuzatvárať sa pred svetom, počúvať vtákov, sledovať stopy. Prespať pod stromami stovky tajuplných nocí v lete, zime, na jar či na jeseň. Potom sa pre vás "obyčajný" les v Malých Karpatoch alebo v Strážovských vrchoch stane skutočnou divočinou. 
 
  
 Prajem vám k tomuto poznaniu krásnu cestu.  
 
  

