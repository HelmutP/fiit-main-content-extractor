
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Zemaník
                                        &gt;
                Nezaradené
                     
                 3 vynálezy čo by sa ľudstvu zišli 

        
            
                                    29.6.2010
            o
            16:38
                        (upravené
                29.6.2010
                o
                16:53)
                        |
            Karma článku:
                4.30
            |
            Prečítané 
            751-krát
                    
         
     
         
             

                 
                    V dnešnej dobe technickej už existuje takmer všetko, prístroje čo nám uľahčujú či spríjemňujú život. Stále sú však veci, čo ešte nie sú používané a ktoré, myslím si, by ľudstvo ocenilo. Nasledujúcich pár riadkov sa budem venovať čisto fantazijným a utopickým myšlienkam.
                 

                     Najhorúcejším kandidátom na titul naj-život-uľahčujúceho prístroja je bezpochyby teleport. Každý vie, o čo ide, vyskytol sa v Star Treku či v rôznych počítačových hrách. Aké by to len bolo super, keby ste sa nemuseli pariť v hnusnom, rozheganom vagóne vlaku slovenských železníc, ale len stisnúť tlačidlo a byť v cieli svojej cesty, nie? Ušetrilo by to kopec času a nervov. Alebo nekonečné hodiny presedené pri medzikontinentálnych letoch. Za nejaký rozumný poplatok, v závislosti od diaľky, by sa dokonca dalo predísť výpadkom v príjmoch aeroliniek, železníc či autobusových spoločností. Tieto by totiž dostávali istú sumu peňazí, čo by prúdili od zákazníkov do fondu teleportického cestovania (FTC) pred každým použitím teleportu. Mohlo by to vyzerať zhruba tak, že človek príde k stanici teleportu, zaplatí sumu adekvátnu na presun napr. do Košíc z Bratislavy, táto by putovala do FTC a človek by sa za pár okamihov ocitol na druhom konci republiky. Z FTC by sa potom mesačne odvádzali peniaze v určitom pomere pre dopravné podniky, ktoré by paralelne fungovali, pretože by boli lacnejšie ako teleport, ale časovo náročnejšie. Nikto by nestratil, naopak, človek by získal.   Ďalším vynálezom by mohol byť tzv. rozdvojovač. Jednoducho by ste vošli do akejsi kabíny a namiesto jedného Joža, čo vstúpil dnu, by vyšli Jožovia dvaja. Identickí, s rovnakými vlastnosťami, vzhľadom, rovnakým, no vlastným vedomím aj svedomím. Fungovali by každý autonómne, každý by sa mohol pohybovať na inom mieste v rovnakom čase, a keď toho budú mať dosť, pôjdu späť do kabíny a všetky zážitky sa zlejú naspäť do pôvodného Joža. Toto by sa obzvlášť hodilo pri situáciách, keď nemôžete byť v rovnaký čas na dvoch miestach. Takto to možné bude. Zíde sa to napríklad pri letných festivaloch, práci a súbežne bežiacom futbalovom zápase a podobne. O nič by jedinec neprišiel, všetky povinnosti by mal splnené, dokonca sa dá hovoriť aj o zvýšenej efektivite práce, pretože dve činnosti by mohli byť vykonané navonok tým istým človekom. Iste, treba ešte doladiť pár legislatívnych a technických záležitostí, ale istotne by to bolo užitočné, nemyslíte?   Posledným prístrojom, ktorému sa budem v tomto texte venovať, je zariadenie, čo by umožnilo užívateľovi spomaliť či zrýchliť sebou vnímaný čas. V praxi by to vyzeralo tak, že napr. v robote by ste si čas zrýchlili, no pri tom by ste si stihli splniť povinnosti, ale hotový by ste boli za zdanlivo kratší čas. Vaše vedomie by vnímalo čas inak, niečo ako pocit keď sa ráno zobudíte a máte dojem, ako by ste si líhali len pred hodinou. Odrobili by ste si svojich, povedzme 8 hodín, no zdalo by sa vám, že ste pracovali len hodinku. Podobne by fungovalo aj spomalenie času. Pri srdcu milej činnosti, ja neviem, párty, sex, hocičo by ste tok reálneho času pociťovali dlhšie. Takže ak párty bola reálne 4 hodiny, vy by ste ju vnímali ako by trvala trebárs 7 hodín. Išlo by o akési natiahnutie času pre väčší zážitok z činnosti.   Tak toto, milí čitatelia, je prvá várka vynálezov potrebných pre ľudstvo, čo ešte neexistujú, ak áno, nevie o nich nik, okrem vlád štátov. Ďalšia bude nasledovať, ak mi niečo nové napadne na um. Apelujem týmto na vynálezcov, aby mojim nápadom venovali aspoň pár sekúnd. Tento text je len čisto výplodom mojej dlhej chvíle v robote, preto ho nemusíte brať vážne. ;)         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Zemaník 
                                        
                                            Remember October
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Zemaník 
                                        
                                            Záclony
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Zemaník 
                                        
                                            Y vs i
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Zemaník
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Zemaník
            
         
        michalzemanik.blog.sme.sk (rss)
         
                                     
     
        Obyčajný človek...mám rád šport, zaujímam sa o politiku, históriu a snažím sa byť dobrým občanom tohto štátu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    570
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pán prezident, hanbite sa!
                     
                                                         
                       V akej krajine to žijem?
                     
                                                         
                       Všetko, čo ste chceli vedieť o rodičoch (ale báli ste sa spýtať)
                     
                                                         
                       Fico: Dlh zdvíhame kvôli Slovensku
                     
                                                         
                       Prichytení pri čine
                     
                                                         
                       Tak to chceli deti
                     
                                                         
                       Eli a Tuti
                     
                                                         
                       Keď Fico a Harabin povedia: Vitajte u nás doma
                     
                                                         
                       List Karolovi: Narkomani, zlodeji, homosexuáli a Kuffa
                     
                                                         
                       Kde skončia daňové, odvodové a iné reformy
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




