
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Čínske dievča-Chen Lidka Liang 梁晨
                                        &gt;
                Súkromné
                     
                 O priezvisku a mene Číňanov 

        
            
                                    18.7.2007
            o
            17:41
                        |
            Karma článku:
                11.99
            |
            Prečítané 
            35019-krát
                    
         
     
         
             

                 
                    Keď som začala študovať slovenčinu, hneď v prvý deň v škole nám náš slovenský lektor daroval slovenské meno. Odvtedy som mala niečo SLOVENSKÉ, čo spája slovenčinou s touto čínskou JA. Neskôr som vchádzala do nádherného sveta slovenského jazyka, kde ma všetko tak nesmierne priťahuje. Za mnou je tisícročná stará čínska kultúra, predo mnou je úžasna európska civilizácia. V takom priestore som našla veľa krásneho a zaujímavého. MENO to je jedna dobrá téma, v ktorej by sme mohli trošičku zacítiť rozdiel medzi našimi kultúrami.
                 

                 
Olivový stromVanGao z Holandska
    Číňania nemajú MENINY. No, nemusíme sa ani čudovať, veď nezabudnime, z čoho sa skladá čínština – znak. Zvyčajne keď jeden človek aktívne zvládne 3000 znakov, stačí mu pekne čítať noviny a časopisy. Priezviska Číňanov obyčajne majú len jeden znak, ale aj niekoľko rodín má také priezvisko, ktoré má dva znaky, sú to výnimka. A mená Číňanov, tie sú úplne rôzne a pestré! Rodičia vždy dajú svojmu dieťatku meno plné nádeje či krásneho vinšu. V čínštine sú tiež také zlepšené slová, aj škaredé slová. Každý znak má niekedy veľa významov, a v slovníku je najmenej 11,000 slov, tak vieme si predstaviť, koľko volieb majú rodičia.       Podľa čínskeho zvyku, deti nesmú mať v mene znak taký istý ako v mene rodičov ani žiadnych príbuzných. Tu môžeme porovnať dve odlišné kultúry – Na Slovensku keď som sa dozvedela, že ocko a synček sa môžu volať rovnako, hneď som sa tak usmievala, povedala som si : Ako je to milé, vždy ocko vidí svoj tieň v synčekovi, to je naozaj veľmi krásne! Nechce sa mi povedať, ktorý zvyk je lepší, to skutočne nemá zmysel, veď každá kultúra si zaslúži náš srdečný rešpekt. V Číne deti nesmú sa volať ako ostatný člen z rodiny, lebo pred 2000 rokmi, Konfucius už povedal, že deti musia rešpektovať rodičov, mená starších členov z rodiny sú sväté, nedá sa tak isto volať. Môj názor na to je taký : rešpekt musia mať deti k svojim rodičom, ale to vlastne nezáleží na tom, či deti sa volajú alebo nevolajú takým istým menom rodičov, záleží iba na ich srdiečkoch, či nechajú pre rodičov miesto v svojom srdiečku. V budúcnosti keď sa stanem mamičkou, dáme dcérenke meno Lidunlienka, po mojom mene Lidka, synčekovi dáme meno jeho ocinka, ale musia sa naše detičky správať s rešpektom k ostatným ľuďom, keby nie, tak určite dostanú jemnučko na zadoček – veď máme výhovorku: čínske dieťatko dostáva take jemné „pohľadkanie“ keď nie je dobručké.      Na slovenskom kalendári, vidím, že každý deň má skupina ľudí meniny, tak sa mi zdá, že asi Slováci majú najmenej 365 mien, to je ale málo pre 5 milión obyvateľov. Neviem, či ľudia už si zvykli na to, že často stretávajú cudzinca, s ktorým sa delia o jedno meno. Ale v Číne, to by bola neuveriteľná náhoda, keď nájdeme v mori ľudí takého, ktorý sa tiež volá ako my.           Čínske ženy nezmenia svoje priezvisko, keď sa vydajú. Ale čo je zvláštne, v starej Číne sme predsa mali taký zvyk ako teraz na Slovensku : keď dievčatá sa vydajú, hneď dostanú priezvisko svojho manžela, ale ešte nechajú svoje pôvodné priezvisko. Ako napríklad určité dievča sa volá Jin Liya, keď našlo chlapca, ktorý má priezvisko Liang, tak sa bude volať oficiálne Liang Jin Liya. Taký zvyk sa dnes len ponechal na Taiwane aj v Hongkongu. Teraz čínske ženy majú priezvisko len po otcovi.       Na záver, venujem len slová jednej čínskej pesničky, všetci Číňania ju dobre poznajú. Táto pesnička nám šepká, nech kamkoľvek pôjdeme po svete, nezabudnime na svoju vlasť, nech rozšírime len lásku, nech prinesieme len najčerstvejší vetrík.      „ Nespýtaj sa ma, odkiaľ pochádzam, Moja vlasť je veľmi ďaleko,        Prečo sa túlam, túlam sa do ďalekej krajiny...         Pre lietajúce vtáčiky v oblohe,        Pre tečújuce potôčiky v horách,        Pre nádhernú lúku,  Túlam sa do ďalekej krajiny...         Ešte, ešte,         Pre ten olivový strom v sníčku...“          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (146)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čínske dievča-Chen Lidka Liang 梁晨 
                                        
                                            Po pozeraní týchto 9 obrazov sa zlepšuje nálada
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čínske dievča-Chen Lidka Liang 梁晨 
                                        
                                            Poďme sa učiť čínštinu s Lidkou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čínske dievča-Chen Lidka Liang 梁晨 
                                        
                                            Druhá čínska rozprávka --- Miska plná pokladov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čínske dievča-Chen Lidka Liang 梁晨 
                                        
                                            Môj prvý pokús prekladu čínskej rozprávky pre slovenské deti --- Múdry princ Čong Cao
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čínske dievča-Chen Lidka Liang 梁晨 
                                        
                                            Etnokultúrne špecifiká v slovenskej a čínskej rozprávke--V.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Čínske dievča-Chen Lidka Liang 梁晨
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Čínske dievča-Chen Lidka Liang 梁晨
            
         
        liang.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som Číňanka, vyrástla som v Pekingu. Ked´ som mala 19 rokov,osud mi dal príležitosť študovať slovenčinu. Mojim najväčším snom je písať o Slovensku knihu pre Číňanov, aby viac Číňanov vedelo, aká je táto krásna krajina v strednej Európe. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    77
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10844
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie po krásnej zemi
                        
                     
                                     
                        
                            druhá strana nášho sveta
                        
                     
                                     
                        
                            Myšlienky
                        
                     
                                     
                        
                            Ohňostroj lásky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            život na Slovensku
                        
                     
                                     
                        
                            život okolo mňa
                        
                     
                                     
                        
                            Žiar nádhernej civilizácie
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Moj blog o Cine--Viktorko---nadherny blog
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Renáta Kališeková
                                     
                                                                             
                                            Bronislav Bátor
                                     
                                                                             
                                            Dávid Králik
                                     
                                                                             
                                            Júlia Hubeňáková - Bianka
                                     
                                                                             
                                            Gerhard Sebastian Hámor
                                     
                                                                             
                                            Martin Malobický
                                     
                                                                             
                                            Marián Minárik
                                     
                                                                             
                                            Miroslav Tropko
                                     
                                                                             
                                            Marián Križovenský
                                     
                                                                             
                                            Andrea Sekanová
                                     
                                                                             
                                            Boris Burger
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Po pozeraní týchto 9 obrazov sa zlepšuje nálada
                     
                                                         
                       Poďme sa učiť čínštinu s Lidkou
                     
                                                         
                       Druhá čínska rozprávka --- Miska plná pokladov
                     
                                                         
                       Je dobré hovoriť pred druhými o svojich cieľoch?
                     
                                                         
                       Môj prvý pokús prekladu čínskej rozprávky pre slovenské deti --- Múdry princ Čong Cao
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




