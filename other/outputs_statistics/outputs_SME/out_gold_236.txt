

 
Suma peňazí, ktorú za prácu zamestnanca zaplatí zamestnávateľ sa označuje
mzdový náklad. Skúsme si vypočítať, koľko peňazí z tejto sumy môže
pracovník skutočne vymeniť za tovary a služby, ktoré potrebuje
k svojmu životu.
 
 
Povedzme, že zamestnávateľ za prácu zamestnanca zaplatí 14 873,- Sk za
mesiac. Z tejto sumy zamestnanec nikdy neuvidí:
 
 
	 výpalné nazvané „Nemocenské poistenie" - 154,- zaplatí zamestnávateľ a 154,- zamestnanec 
	 výpalné nazvané „Starobné poistenie" - 1540,- zaplatí zamestnávateľ a 440,- zamestnanec 
	 výpalné nazvané „Invalidné poistenie" - 330,- zaplatí zamestnávateľ a 330,- zamestnanec 
	 výpalné nazvané „Poistenie v nezamestnanosti" - 28,- zaplatí zamestnávateľ 
	 výpalné nazvané „Rezervný fond" - 523,- zaplatí zamestnávateľ 
	 výpalné nazvané „Úrazové poistenie" - 88,- zaplatí zamestnávateľ 
	 výpalné nazvané „Zdravotné poistenie" - 1100,- zaplatí zamestnávateľ a 440,- zamestnanec 
	 výpalné nazvané "Daň z príjmu" - bude pre tohto zamestnanca len 296,-, aby to vyzeralo, že máme nízke dane  
 
 
Z mzdového nákladu 14.873,- Sk, ktorý musí pracovník zamestnávateľovi
odpracovať, dostane na svoj účet 9.230,- Sk, čo je 62%. Tomuto sa veľmi nesprávne hovorí "čistá mzda",
pretože ani tieto peniaze nie je možné „čisto" vymeniť za tovary a služby.
Každý nákup je podmienený zaplatením výpalného, nazvaného „DPH". Za 9.230,- Sk
si môžeme kúpiť tovary a služby v skutočnej hodnote  7.756,- Sk, čo je skutočná čistá mzda, t.j.
skutočná hodnota tovarov a služieb, ktoré si môže zamestnanec kúpiť za
svoju prácu, za ktorú mu zamestnávateľ zaplatil 14.873,- Sk. K zamestnancovi
sa teda dostane len cca 52% sumy, ktorú
za jeho prácu zamestnávateľ zaplatí. 
 
 
Slovenskí (a nielen slovenskí) zamestnanci sú skutočne vykorisťovaní.
Akokoľvek som pravicovo orientovaný, vyberať výpalné (tzv. „poistné") od prvej
koruny príjmu zamestnanca, bez ponechania aspoň minimálej čiastky na základné
potreby bez „výpalného", to sa naozaj nedá nazvať inak, ako hnusné
vykorisťovanie. Vykorisťovanie pracujúceho občana „jeho" štátom. So solidaritou, ktorej presadzovanie by som od biskupov očakával úplne samozrejme, to nemá spoločné vôbec nič. Starostlivý
štát sa však takto nespráva ku každému občanovi. Najšikovnejší
a najbohatší občania neplatia výpalné temer žiadne . Nie z vôle zamestnávateľov, ale
z vôle štátu, ktorý tento vykorisťovateľský systém prevádzkuje.
 
 
A to ešte nehovorím o „neviditeľnom" štátnom vykorisťovateľskom
nástroji, finančnom systéme, pomocou ktorého sú prerozdeľované aj tie
prostriedky,  ktoré sa k chudákovi zamestnancovi cez všetky platby výpalného
nakoniec dostanú. 
 
 
Súdruhovia biskupi. Ako pre veriaceho človeka je pre mňa správanie cirkevných
hodnostárov najväčším sklamaním, ktoré som zažil po roku 1989. Neverím, že
neviete počítať. Ale keď držať ruku vo vačku pracujúcich vedno so štátom je tak
povznášajúce ...
 
 
Nie je nutné, ani zmysluplné " ... urobiť všetko, aby sme týmto ľuďom dali, čo potrebujú na dôstojný život. ... ", pretože niekomu dať bude znamenať iného (zväčša chudobnejšieho) obrať. Úplne postačí NEBRAŤ. Prerozdeľovanie od chudobných k bohatým zmeniť na prerozdeľovanie solidárne. 
 
 
 
 

