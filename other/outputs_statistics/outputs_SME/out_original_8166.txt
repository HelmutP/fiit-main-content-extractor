
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Frasch
                                        &gt;
                Verejné
                     
                 Povodne zmenili predvolebnú kampaň 

        
            
                                    6.6.2010
            o
            13:22
                        (upravené
                6.6.2010
                o
                13:29)
                        |
            Karma článku:
                6.85
            |
            Prečítané 
            945-krát
                    
         
     
         
             

                 
                    Povodne zmenili predvolebnú kampaň, zrušili sa mítingy, otvorila sa tému protipovodňových opatrení a efektivity poskytovania pomoci. Médiá sa menej venujú kampani a viac vode v domoch ľudí. Už neriešia Maďarov, Ficom predané Slovensko za 284mil SKK, politicky podfarbené obchody SNS, či financovanie SDKÚ. Napriek nešťastiu, ktoré Slovensko postihlo, parlamentné voľby o týždeň budú a ich výsledky ovplyvnia náš život najbližšie 4 roky. Volič sa musí o týždeň rozhodnúť!
                 

                 
Rieka Ipeľ 
  Povodne zmenili predvolebnú kampaň aj mne. Napríklad jeden míting, na ktorý som sa chystal sa zrušil a na druhý som sa nevedel dostať kvôli uzavretému mostu. Aká to symbolika pre stranu Most-Híd, bez mosta, ktorý spája brehy sa žije ťažko na oboch brehoch! Na povodne strany zareagovali rôzne. Niektoré rušili predvolebné mítingy, šli pomáhať, iné rozbehli finančné zbierky na pomoc postihnutým, aj Fico sa konečne prestal zaoberať Maďarmi a šiel sa pošpacírovať po zničenom Slovensku. No a samozrejme sľubovať odškodnenia, vraj 25 miliónov eur zo štátneho rozpočtu. Svoju rezervu už rozflákal na ihriská a rôzne iné propagandistické akcie. Teraz keď by ľudia jeho peniaze (pardon naše peniaze v jeho vládnej peňaženke) skutočne potrebovali, rezerva vlády zíva prázdnotou. Uvidíme, či aspoň tých 25 miliónov sa dostane k postihnutým, alebo ako zvyčajne sa mnohé vyparia vo vreckách kamarátov. Je na novinároch a treťom sektore, aby ustrážili tok peňazí, aby spočítali peniaze, ktoré skutočne prišli od štátu do obcí a postihnutým ľuďom. To čo sa deje na Slovensku je bezpochyby veľká tragédia. Iste sa  v hlavách mnohých ľudí preháňa pár otázok typu, prečo práve ich obec, či mesto vytopilo. Či vláda urobila všetko, čo mala, aby zabránila záplavám? Čo bude ďalej, kto im pomôže? Medzi odpoveďami sa objaví asi aj taká, že vláda neurobila všetko, čo mala, aby zabránila tak rozsiahlym škodám. No toto konštatovanie neplatí len o Ficovej vláde, ale aj o Dzurindových vládach, či Mečiarových. Je u nás zvykom, že pokým sa niečo nestane, dovtedy sa chyby, zaváhania, zlyhania, nekvalitná práca prehliadajú, tolerujú, akceptujú ako niečo normálne. Teraz sa však stalo! Napríklad obyvatelia obce, kde pretrhlo nedávno vybudovanú hrádzu, ktorá mala danú obec ochrániť pred storočnou vodou, asi budú chcieť teraz vedieť, kto je zodpovedný za zaliatie ich domov. Opäť novinári by sa mali začať vypytovať kompetentných, či to muselo dopadnúť tak, ako to dopadlo. Napriek všetkému, teraz z pohľadu budúcnosti Slovenska nie je dôležité, že ktorá vláda najviac zanedbala protipovodňovú ochranu. Nie je dôležité ani to, že ktorá politická strana počas kampane najviac pomôže postihnutým povodňami. Dokonca nie je dôležité ani to, či vláda zareaguje správne a pomoc bude efektívna, lebo za týždeň, kedy budú voľby, sa nebudú dať objektívne vyhodnotiť kroky vlády. Z pohľadu budúcnosti je najdôležitejšie to, aby sa do vlády dostali strany, ktoré ponúknu a hlavne potom zrealizujú najefektívnejšie a rozsiahlejšie protipovodňové opatrenia. To, že klíma sa mení už hádam pripustí aj najväčší skeptik. Nie je vylúčené, že takéto, alebo ešte horšie povodne sa zopakujú.  Preto dôležité sa pýtať ako chcú politici, ktorí sa uchádzajú o hlasy voličov, pripraviť túto krajinu na ďalšie vlny ničivých povodní. Týždeň pred voľbami asi ťažko nejaká strana naformuluje zmysluplný komplexný program zameraný na protipovodňové opatrenia. No volič má stále možnosť si vybrať. Vybrať si tých politikov, ktorí občas načúvajú aj radám odborníkov, ktorí závažné veci neriešia z večera do rána, ktorí konzultujú riešenia aj s občanmi, ktorí sa neboja verejnej kontroly, ktorí sa budú snažiť peniaze využiť na pomoc ľuďom, nie svojim kamarátom! Myslím si, že zo skúseností vnímavý občan vie, že to nie sú politici strán súčasnej vládnej koalície. Ako náhle sa počas súčasnej vlády ozval nejaký odborník na nejakú tému a nesúhlasil s návrhmi vlády, hneď ho označili za lobistu opozície a pod. Kvalitu práce súčasnej vlády jasne dokazujú problémy s financovaním severnej diaľnice kvôli trasovaniu diaľnice ľahostajnej k životnému prostrediu. Príroda nám to teraz asi vracia aj s úrokmi

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Frasch 
                                        
                                            Ftáčnik, akoby člen SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Frasch 
                                        
                                            Nešťastná doprava bratislavská
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Frasch 
                                        
                                            TA3 robí Ficovi reklamu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Frasch 
                                        
                                            Potemkinov most v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Frasch 
                                        
                                            Vážený pán prezident,
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Frasch
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Frasch
            
         
        frasch.blog.sme.sk (rss)
         
                                     
     
         Pracujem v oblasti počítačových sieťových technológií. Zaujímam sa aj o ekonomiku, dopravu a vedu. Som členom strany OKS od jej založenia. *************************************************** Za gramatické chyby a nepresné formulácie sa vopred ospravedlňujem. Je to spôsobené nedostatkom času, poriadne prelúskať text. Blogy si píšem sám, nemám žiadny štáb, či asistenta, ktorý by ich kontroloval, či nebodaj dokonca písal. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    67
                
                
                    Celková karma
                    
                                                7.78
                    
                
                
                    Priemerná čítanosť
                    1903
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vecné zhrnutie stavu našej politiky a udalostí v nej
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dennik RSS
                                     
                                                                             
                                            OKS
                                     
                                                                             
                                            Trend
                                     
                                                                             
                                            Hospodarske noviny
                                     
                                                                             
                                            .tyzden
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




