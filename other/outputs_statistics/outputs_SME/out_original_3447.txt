
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Makatúra
                                        &gt;
                Nezaradené
                     
                 Prečo nás ničia letným časom? 

        
            
                                    27.3.2010
            o
            19:49
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            2129-krát
                    
         
     
         
             

                 
                    Už je to tu zas. Posúvame hodiny. Na sedem mesiacov si násilím obrátime biorytmus a budeme sa tešiť ako vraj ušetríme. Z mnohých jedincov sa ale na dlhé týždne stanú ranní zombíci. Má vôbec prechod na letný čas  význam?
                 

                 Na území Slovenska  bol letný čas vyskúšaný niekoľko ráz. Od roku 1916 do roku 1918 a  1940 až 1949. Potom sa tento experiment v roku 1979 zaviedol  znovu.  Zavedenie letného času malo šetriť energiu lepším využitím denného svetla. Ekonómovia a energetici upozorňujú, že očakávaná úspora nie je taká výrazná, ako sa od tohto opatrenia očakávalo. Čoraz častejšie používaná klimatizácia energetické úspory vraj skoro úplne eliminuje. Preukázané negatívne vplyvy časových zmien dlhoročne sledujú aj iné obory. Zdravotníci upozorňujú na zhoršenie zdravotného stavu množstva ľudí, policajti na zvýšenú nehodovosť, poľnohospodári na narušený biorytmus hospodárskych zvierat a preťaženie pracovnej sily ...   Európska komisia však vo svojej smernici stanovuje päťročné intervaly na plánovanie zmeny času. Teraz sme v „päťročnici" 2007-2011 a potom sa rozhodne, ako ďalej.   Myslím, že málo preskúmaným dopadom prechodu na letný čas je aj jeho vplyv  študentov. Som priam presvedčený, že hodinový posun dramaticky ovplyvňuje návštevnosť ranných prednášok na vysokých školách. Pozornosť tých, čo sa dostavia je značne znížená a nepochybujem, že obe strany- tá v kreslách auly aj prednášajúci trpia. Keďže stav adaptácie trvá aj dva mesiace, časť vyučovacieho procesu je u citlivejších študentov zabitá minimálne do skúškového obdobia.   Úprimne dúfam, že sa na budúci rok nájde V Európskej únii nejaký politik- hrdina, ktorý sa zastane všetkých „normálne časovo nastavených" a skoncuje s  letným časom.  Tí, ktorým prechody v časových pásmach nevadia, by mohli súcítiť s množstvom unavených, nervóznych a biorytmicky pomýlených spoluobčanov a podporiť ich v skončení tohto bezcitného pokusu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (83)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            So sestrami sa hrá hra na hlúpych
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Kolaps parlamentnej demokracie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Slovensko neuživí toľko politikov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Krajina kde práca znamená prežívanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Surová doba alebo ako speňažiť objekt domova sociálnych služieb
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Makatúra
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Makatúra
            
         
        makatura.blog.sme.sk (rss)
         
                                     
     
        Elév-idealista

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    98
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1538
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Krajina kde práca znamená prežívanie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




