



 

 
 Prezident FNM Jozef Kojda (vľavo) sa s niektorými privatizérmi Mečiarovej éry dohodol na doplatení kúpnej ceny. Na snímke z mája 2000 je so Slavomírom Hatinom, spolumajiteľom spoločnosti Slovintegra, ktorá v roku 1995 sprivatizovala Slovnaft. Štát sa s privatizérmi dohodol a oni vrátili 10 percent akcií rafinérie.	FOTO - TASR 
 




 Pred desiatimi rokmi odštartovala tretia Mečiarova vláda privatizáciu nebývalých rozmerov. Kreslo šéfa Fondu národného majetku obsadilo Združenie robotníkov Slovenska, väčšinu vo výkonnom výbore FNM si zabezpečilo Hnutie za  demokratické Slovensko. Štát počas štyroch rokov previedol stovky podnikov na tzv. domácu kapitálotvornú vrstvu. Majetok v účtovnej hodnote 110 miliárd korún predal fond oficiálne za 30 miliárd, na jeho účet však v skutočnosti prišlo necelých 20 miliárd korún. Päť rokov po vydaní Čiernej knihy privatizácie sme sa o náprave  nevýhodného výpredaja štátneho majetku zhovárali s JOZEFOM KOJDOM, súčasným prezidentom FNM. 
 Nežalujete niekdajších predstaviteľov FNM za privatizácie, ktoré fond po výmene vlády v roku 1998 označil ako nevýhodné? 
 
 Nie, fond súdnou cestou vymáha len pohľadávky, ktoré eviduje voči nadobúdateľom majetku. Ja neviem, či ľudia z niekdajšieho vedenia fondu figurujú ako vlastníci firiem, ale my sa zásadne súdime len o peniaze. Samozrejme, dávame aj trestné oznámenia, ale zasa len na manažmenty privatizérov, ak predávali alebo prevádzali majetok privatizovaných firiem bez nášho súhlasu. Takýchto trestných oznámení sme podali niekoľko desiatok. 
  

 Koľko súdnych sporov s privatizérmi vediete? 
 
 Od roku 1994 sme boli vo viac ako 800 súdnych sporoch. Ak hovoríme o živých súdnych sporoch, musíme ich rozdeliť na dve skupiny. Prvú tvoria spory, v ktorých nás žalujú. Privatizér si napríklad nárokuje nejaké odškodnenie, alebo tvrdí, že naše neskoršie odstúpenie od zmluvy bolo neprávoplatné. Tých je 256. Druhú skupinu tvoria súdne spory s nadobúdateľmi štátneho majetku, ktorí neplnili zmluvy. Takých sporov je 220. Okrem súdnych sporov sme aktívni aj v konkurzoch, to je oblasť, kde má dnes fond najviac práce. Dnes ich je v približne 280. 
  

 Viete vyčísliť hodnotu majetku, ktorý ste už získali späť? 
 
 Pri súdnych sporoch je individuálne, či ide o vrátenie majetku, alebo náhradu škody vyjadrenú finančne. Pri uplatňovaní konkurzov sa nedá získať naspäť žiadny majetok. Povinnosťou konkurzného správcu je majetok predať a zaplatiť dlhy firmy. S odstupovaním od zmlúv, ak privatizér za majetok neplatil, máme zlé skúsenosti. My sme síce získali majetok späť, ale aj so všetkými záväzkami. Stávalo sa tak, že odstúpením od zmluvy sme dostali naspäť majetok v účtovnej hodnote 30 miliónov korún, ale 'sedelo' na ňom 60 exekútorov. Tie podniky boli doslova vytunelované až do základov. Preto ak dnes niekto za majetok neplatí, na 99 percent ho čaká konkurz. 
  

 V roku 1999 vydal fond tzv. Čiernu knihu privatizácie. Ako sa snažíte vyriešiť privatizačné kauzy, ktoré sú v nej spomínané? 
 
 O zhruba päťdesiatich prípadoch obsiahnutých v tomto dokumente bola svojho času informovaná aj vláda. Išlo o predaje štátneho majetku, kde bol porušený zákon alebo boli predané veľmi lacno. Z najproblematickejších zmlúv je dnes väčšina v konkurze, alebo sme sa s privatizérmi dohodli. Napríklad v prípade Slovnaftu, kde vláda schválila dohodu, na ktorej základe nadobúdateľ vrátil časť akcií, čím zvýšil svoju kúpnu cenu, a tým bola pre fond vec uzavretá. Fond následne tieto akcie predal za 1,3 miliardy. Dnes je z najznámejších káuz živý už len jeden súdny spor, a to Kúpele Sliač a Kováčová. 
  

 A čo Štúdio Koliba, ktoré svojho času sprivatizovala firma Mečiarových detí MMM? 
 
 Štúdio Koliba je ten prípad, kde sa jednoznačne ukázalo, že vedenie súdneho sporu vedie samotnú firmu pomaly, ale isto do ekonomických problémov. Pred zopár rokmi vstúpilo Štúdio Koliba do konkurzu, FNM si uplatnil pohľadávku vyše 160 miliónov korún. To bola nezaplatená časť kúpnej ceny plus investície, ktoré mali byť, ale neboli vykonané. V prvom kole nám bola síce táto pohľadávka popretá, ale pred vyše dvoma týždňami súd uznal, že fond si ju nárokoval oprávnene, a mali by sme sa teda stať veriteľom v pokračujúcom konkurze. 
  

 V prípade jednej privatizácie už z čias vášho pôsobenia vo FNM sa však súdite. Ide o Solivary Prešov. 
 
 V prípade Solivarov sa vedie niekoľko veľmi zložitých súdnych sporov. Nevylučujeme, že ich bude viac. Solivary sme vlani predali Nováckym chemickým závodom (patriacim finančnej skupine 1. garantovaná). Súdne spory dnes vedieme sčasti s 1. garantovanou a sčasti s NChZ. 
  

 Aké sú základné rozdiely v privatizácii dnes a v rokoch 1994 až 1998? 
 
 Po prvé: zmenil sa zákon, teda o veľkých predajoch musí vždy rozhodovať vláda. Po druhé: robíme medzinárodné tendre, oproti minulosti sa teda začalo inak pozerať na zahraničný kapitál, ktorý nijako neznevýhodňujeme. A po tretie: prestali sme predávať na splátky a vždy uprednostňujeme záujemcov, ktorí ponúkajú jednorazové vyrovnanie. 
  

 V portfóliu fondu je ešte aj dnes dosť podielov v rôznych firmách. Čo s nimi plánujete robiť? 
 
 Bežné podniky z rôznych rezortov sú de facto sprivatizované. Ak dnes máme niečo na predaj, tak sú to väčšinou firmy, ktoré boli kedysi klasifikované ako prirodzené monopoly. Momentálne pripravujeme privatizáciu teplární - nad jednou visí otáznik, tak hovoríme o piatich. Počítame tiež s dopredajom podielov v tých firmách, kde už privatizácie bola. Vlastníme aj menšinové podiely v niekoľkých firmách, ktoré budeme predávať klasickým spôsobom zverejnením inzerátu a následným predajom záujemcovi s najvyššou ponúknutou cenou. 
  

 Čo bráni tomu, aby fond zverejnil inzeráty, všetko predal a vstúpil do likvidácie? 
 
 Takýto scenár je nereálny, keďže sme povinní postupovať podľa zákona, ktorý hovorí, že o každom predaji musí rozhodnúť vláda. Osobne si však myslím, že ak budú splnené predpoklady doprivatizácie, tak obdobie, keď je oprávnená existencia Fondu národného majetku, sa naozaj začína krátiť. Bude aktuálna aj príprava legislatívy umožňujúcej zánik fondu. Prvá predstava je o tom, že k 31. decembru 2006 by táto inštitúcia mohla zaniknúť. 
  

