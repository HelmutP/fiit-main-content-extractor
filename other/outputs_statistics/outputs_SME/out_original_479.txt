
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rastislav Soviš
                                        &gt;
                Stavebný fotodenník
                     
                 Stavebný fotodenník - inžinierske prípojky 

        
            
                                    26.2.2008
            o
            11:55
                        |
            Karma článku:
                10.67
            |
            Prečítané 
            7361-krát
                    
         
     
         
             

                 
                    Rôzne časové obdobia 2007. Inžinierske prípojky boli od začiatku realizácie niečo, na čo som sa vôbec netešil a tajne som dúfal, že to niekto za mňa vyrieši. Pre mňa jedna  veľmi vzdialená španielska dedina. V skratke len uvediem, po úspešnej realizácii, veľmi laický komentár opodiaľ stojaceho fotografa.
                 

                Celý proces realizácie každej jednotlivej inžinierskej prípijky sa dá rozdeliť do troch etáp. Tak napríklad elektrická prípojka.     1. Elektrikár, ktorý robil vnútorný rozvod, do vyhĺbenej jamy položil z domu vedúci prípojkový kábel a doviedol ho k hranici pozemku. Vyrobil aj revíznu správu. Zaplatil som ja.     2. V podniku "elektrárne" po predložení relevantnej dokumentácie v mojom mene oslovili ich autorizovaný avšak súkromný komerčný subjekt, u ktorého som si následne objednal realizáciu prípojky od hranice pozemku po verejný rozvod elektriny (celých 0,5 m). Aj tento subjekt mi vyrobil revíznu správu. Zaplatil som ja.     3. Podnik "elektrárne" po predložení ďalšej relevantnej dokumentácie prišiel nainštalovať elektromer. Neplatil som nič. A bolo svetlo! :-)     Taký istý trojfázový postup s malými kozmetickými obmenami som absolvoval ďalej pri plynovej prípojke aj pri vodovodnej prípojke. "Našťastie" do tejto končiny dediny Ivanky pri Dunaji ešte nadorazila kanalizačná prípojka. Takže v tomto prípade sa všetko odohrávalo súkromne na súkromnom pozemku a končilo v žumpe.. :-)     Fotodokumentácia v tomto prípade je len symbolická. Ak si všimnete, tie AVIE, ktoré patria tým "skromným komerčným subjektom", myslím že vo všetkých prípadoch pamätajú celkom isto aj nesúkromné, nekomerčné - štátne poskytovanie služieb svojmu obyvateľstvu. Ale to už sú preč časy, keď strategické podniky patrili štátu. To už je ale o inom..                                    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rastislav Soviš 
                                        
                                            Camino de Santiago - deň 6.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rastislav Soviš 
                                        
                                            Camino de Santiago - deň 5.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rastislav Soviš 
                                        
                                            Camino de Santiago - deň 4.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rastislav Soviš 
                                        
                                            Camino de Santiago - deň 3.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rastislav Soviš 
                                        
                                            Camino de Santiago - deň 2.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rastislav Soviš
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rastislav Soviš
            
         
        sovis.blog.sme.sk (rss)
         
                                     
     
        

"Not before 1977" 




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    88
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3364
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Review
                        
                     
                                     
                        
                            Videoblog
                        
                     
                                     
                        
                            Stavebný fotodenník
                        
                     
                                     
                        
                            Camino de Santiago I.
                        
                     
                                     
                        
                            Fotka dňa
                        
                     
                                     
                        
                            Ďejavovica
                        
                     
                                     
                        
                            Klub, im memoriam
                        
                     
                                     
                        
                            Kto máš uši, slyš
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Bez komentáru
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Dotazník
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




