

 Maruška sa hneď na to rozplakala a plakala dosť dlho na to, že už dávno nie je dieťa. V skutočnosti plakala kvôli niečomu úplne inému, ale niekedy (vlastne vždy) je dobré, že si človek môže nájsť náhradný problém a vyhovoriť sa naň. Aspoň nemusí pomenovať to, čo sa pomenovať aj tak bojí. Nahlas. Aj potichu. Plač býva síce fatálny pre ľudskú sebaúctu, hneď vás katapultuje späť do detstva, ale aspoň vás nikto nezačne hneď rozoberať na súčiastky, aby dokázal, že ste rozoberateľný, rovnako ako bábika či rádio, čo aj tak vlastne ste. 
 Plač vytvára bariéru, povedal mi raz v noci A., keď sa mi snažil vyzliecť čierny rolák, to sme práve pitvali Kafku a Ingmara Bergmana a ja som sa bránila (jeho rukám, čo ma chceli vyzliecť a) závislosti na svete, akoby sa to dalo. Haha. 
 Je asi vopred dané, že niektorých ľudí sklameme, keby som tomu uverila už pred pár rokmi, možno by som hneď ušla niekam do Malmö, ale asi by to nepomohlo, lebo niektorým skúsenostiam a niektorým ľuďom sa nedá vyhnúť. Pravdepodobne. 
 Večery už nie sú také chladné, ani Mikeš už nie je taký nezrozumiteľný, len plač už nie je to, čo býval, ale asi musí byť iný (ten plač) a možno sme len my vyrástli z výhovoriek. Asi. 
 Večer sme chceli sledovať šou Tiny Nordström, ale bežal hokej. Ako vo Švédsku vždy. 
 Keď vyrastiem, tiež chcem mať svoju televíznu šou a malé prasiatko a tiež psa, mohol by to byť jazvečík, ale hlavne musím mať kocúra, čo sa bude volať Garfield a bude oranžový, povedala včera večer Mikešova dcéra nekompromisne. Je pozoruhodné, ako deti o ničom nepochybujú. 
 A tak sme utekali dolu na pláž okúpať sa, vlasy sme si previazali hodvábnymi šatkami a v okuliaroch ako Jackie O. sme opatrne skúšali more a drkotali zubami, pretože Golfský prúd už tiež nie je to, čo býval. Asi to bude fenomén doby. Povedal by možno A., keby tu bol. 
 Maruška potom opäť trochu plakala, keď to spustíte, už sa to tak ľahko nedá zastaviť. Nejaké deti pľuli do vody, asi tiež len preto, aby vedeli, že môžu. 
 Iní sú vždy na vine. Nie my. Myslím si. Možno by stačilo prejsť zrkadlom ako v Imagináriu Dr. Parnassa a človek by zistil, že v skutočnosti nič nechce. NIČ. Tvrdil to Mikeš, keď o niečo neskôr balil s Maruškou zázvorové keksy do priesvitného hnedého papiera, šlo im to, a to ani neboli v Helsingborgu ako ja. Možno je to skutočne tak alebo sme sa len stali obeťami ilúzie o svojom vlastnom svedomí. 
 Radšej mi z očí nesnímaj tú tmu. (Rúfus) 
 V mojom živote je strašne veľa pokašľaných včerajškov, možno by sa malo začať včerajškami trochu viac šetriť. Ale jedného dňa nám aj tak dôjdu a už nebudú žiadne ďalšie. Ako keď v tom malom zadymenom lundskom pajzli veľký pehavý Viking zvracal do záchoda, držiac sa pritom (svojho a) porcelánovej misy a krásna svetlovlasá Greta Garbo dramaticky zahlásila do ticha, že mäsové guľky došli. Ách! 
 Just so! Just so! 
 Pokašľané včerajšky sa v mojom živote akosi kopia a dnešok je vždy len jeden, aj zajtra bude dnešok len jeden (ešte dobre, že o zajtrajškoch nevieme nič). Absolútny nepomer včerajškov a dneškov v čase ma vedie k smutnej úvahe, že nespravodlivosť je od prapočiatku zakotvená v našich životoch, lebo tieto sa dejú v čase a je pre nás oveľa prirodzenejšia ako spravodlivosť, ktorú celý život hľadáme. Toto zvláštne zistenie ma trochu upokojuje. Možno. 
 Zasejeme semínka a budeme tu pěstovat kytičky, povedala Maruška oveľa oveľa neskôr vysokému Švédovi, ktorý vyzeral ako Max von Sydow, vychrstla mu pivo do tváre a odišla. Nie, nebolo to málo. Akokoľvek to bolo všetko. (Rúfus) Neviem, na čo sa jej ten Švéd spýtal. Sú také veci, na ktoré sa netreba pýtať. Pre istotu. 
   
   
  
   
 foto: fotky.sme.sk 

