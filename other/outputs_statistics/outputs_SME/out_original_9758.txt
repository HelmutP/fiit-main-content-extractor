
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Baričák
                                        &gt;
                EGYPT (Cestopis)
                     
                 Bežná all inclusive dovolenka 

        
            
                                    28.6.2010
            o
            9:46
                        (upravené
                28.6.2010
                o
                9:53)
                        |
            Karma článku:
                12.91
            |
            Prečítané 
            6518-krát
                    
         
     
         
             

                 
                    Tak som tu, v krajine arabskej, na púšti sinajskej, ktorá sa v roku 1988 definitívne stala opäť súčasťou Egypta. A ten sa s vidinou groša rozhodol na území duchov dostavať komplex hotelov, ktorý začali budovať ešte Izraelci. Oblasť Taba, klitoris Akabského zálivu, priesečník Jordánska, Izraelu, Saudskej Arábie a Egypta. Sedím tu v päťkrátpäťcípom hoteli, kde vám za žmurknutie vytrú červenokošelí arabskí lokaji aj riť. Samozrejme nezabudnú potom natrčiť dlaň, nech do nej niečo cinkne. A najlepšie ak ten cingot zapríčiní papier. Sú síce spomalení (tak ako aj ich muchy), ale natíska sa mi aj iná odpoveď: možno sme my, Európania, zrýchlení.
                 

                 Spotil som sa, keď mi priateľka predostrela sen o svojej dovolenke. Dokonca ešte viac, ako keď som na začiatku nášho pobytu šarmantným pohybom maličkého chrbtového batoha rozbil v bratislavskom Duty free shope parfum. Našťastie to bola vzorka, ale i tak ma myšlienka „No, pekne to začína" neminula. Posledné roky som sa totiž trmácal svetom v špinavých šortkách a s batohom na pleci. Ako správny pokrytecký sráč som si spomenul na mnou vyslovený sľub, že na all inclusive zájazd už nikdy nepritakám. Niečo pozitívne vo mne si odmietalo pustiť tri roky starý film so zábermi, ako sa vraciam prejedený, spálený, prepitý, preležaný a prenudený z egyptského Sharm El Sheikhu. Tam to zachránil nočný výlet na Mojžišovu horu, kláštor svätej Kataríny, Káhira, pyramídy a prestrelka s vodnými pištoľami pri zadnom bazéne, lebo už som bol pripravený začať si trhať chlpy z nosa, len aby sa v tom stojacom sparnom egyptskom výpeku niečo STALO.   A teraz sa tu zasa motám ako pseudozbohatlík v Babylone rečí hotelového areálu a dávam všetkými mojimi sekundárnymi dorozumievacími prostriedkami okoliu najavo (tak ako aj ono mne), že toto som už zažil, videl, všade som bol, nič na neprekvapí a som veľmi unudení. Nie som predsa sedlák, ktorý vyhral športku, vyhodil slamu z galoší a dovolil si konečne „megazájazd" mimo smer Jadran. Z Chorvátska sa už predsa nehíka. Síce ani z Egyptu, ale i tak je to všetko za „veľa prachov" a to sa už počíta. A tak prvý deň počúvam nárek Moravákov, že tie syry, salámy a džúsy nie sú ono. Áno, ťažko sa to porovnáva s Karibikom. Na druhej strane by možno stačilo, keby komando únoscov celé osadenstvo tohto snového komplexu na dva dni ubytovalo do priemernej egyptskej dedinky. Prípadne etiópskej. Alebo somálskej? Možno by tie podivné kozie syry, čudne sa červenajúce salámy a extra sladké džúsy po celodenných modlitbách za návrat domov chutili ako omšová oblátka vrahovi so zlatým krížikom na hrudi.   Staršia Slovenka mi na našom uvítacom sedení s delegátkou pripomenie moju maminu, pretože všetkým hneď vysvetlila čo, kde a prečo, bez toho, aby sa ju niekto pýtal na názor. Asi bývalá pani učiteľka. Slovák skutočne spozná Slováka vo svete už z kilometra... V saune sa ticho smejem poľskému páru, kde chalanisko odhaduje cestu do chystaného Jeruzalemu na tri hodiny, ale dievčina mu hádže do viet stopku so slovami, že „to neisto, na granici nás možu pridusiť". To teda môžu, slečna poľskaja, hlavne ak na otázku, či sa  o vašej rodine nachádzajú teroristi odpoviete zo srandy áno. Tieto „prefíkané" otázky predbehli už len Američania, ktorí sa vás s nehranou rafinovanosťou spýtajú, či nemáte v kabelke granát alebo či sa vo vašej rodine nevyskytoval terorista. „Pravdaže áno, niekoľko. Starká bola zažratá atentátnička. A granát mám v krabičke od Nivei. Prečo na mňa mierite samopalom? Som len turista..."   Večer ticho sledujem ako dvaja Angličania balia dve Slovenky na sólovej babskej jazde, pričom ony ich na oplátku učia, že „ja tebja ľubľú" je po rusky, no po našom je to „milujem ťa". Prípadne „milujem vás", ale vyučovanie vykania už nie je povinné. Hladkajúce ruky zúžili počet osôb na dvakrát dva. Neskôr sa tvárim, že nevidím nadrbaného Čecha, ktorý pchá čašníkovi hneď prvý večer za „grátis" teplé pivo smiešnu bankovku a spod ofiny, ktorou mi pripomína jedného z dvojice Blbý a blbší, zvodne žmurká na moju priateľku. Až potom sa  vykĺbeným tanečným krokom zamilovane vracia k svojej žene, ktorá vo mne spálenou pokožkou evokuje zabudnuté klobásky na rošte piatkovej opekačky.   Lepšie sa mi vedú rozhovory s arabskou obsluhou. Tamira som premenoval na Timura. Vysvetlil som mu, že je v mojich očiach podobný hrdina ako ruská postavička, ktorou nás v detstve motivovali doniesť na zber papiera čo najviac starých novín. Pracuje šesť týždňov vkuse, aby sa potom mohol vrátiť necelých tisíc kilometrov za svojou rodinou. Na týždeň. Jeho mesačný plat je „tučných" dvesto eur. „Timur, poprosím čokoládovú zmrzlinu, dva kopčeky. Áno, zaliať colou... to vieš, päť hviezdičiek." Potom kráčam okolo troch Arabov šéfujúcich tomuto hotelu. Saláma sem, saláma tam. Žeby riešili problém tých salám? Veru ani u nás v Tescu nenazvú liptovskú salámu šunkou.   Antikonzumné zviera vo mne sa búri. Počas raňajších behov úsmevmi zrkadlím zdvihnuté palce hotelových vartášov strážiacich imaginárne územia susediacich turistických oáz „všetko v cene". Pravidelne vymetám posilňovňu, kde fučím pod železom a skrývam smiech nad elektrickou zásuvkou namontovanou na zrkadle, ktorá interiérovou nezvyklosťou prekonala aj kovanie dverí priskrutkované bez zmyslu a logiky na stene našej hotelovej kúpeľni. Radšej naberám dych a ponáram sa v jakuze ku dnu, aby som si v ľahu mohol vychutnať silu trysky. Egypťania ju totiž namontovali tak, že miesto toho, aby vám v sede voda masírovala spodnú časť chrbtice, ženie sa vám voda akurát tak do análu. Tu sa výborní arabskí počtári sekli. Minimálne o sedem centi.   Aj moslimské ženy pridávajú svojou kúpacou atrakciou do ohňa, keď do bazéna vchádzajú zásadne oblečené až po hlavu. Nechávajú voľnú len časť okolo očí. Asi pre šnorchel. Chcel by som sa ale na chvíľku dostať do ich zmýšľania a vyzistiť, čo si ony myslia o mojej priateľke, ktorá okolo bazéna tancuje výhradne s jedným trojuholníčkom a tromi šnúrkami oviazanými okolo bedier. Asi by o nás Európanoch padali súdy v štýle: totálny morálny úpadok, Sodoma-Gomora, neveriaci psi. Musia nás mať určite za hlupákov, keď sledujú ako sa hala-bala prejedáme, ale hlavne na tom slnečnom výpeku do seba lejeme jeden ľadový nápoj za druhým. „My pijeme teplý čaj. Vás v detstve neučili, že v horúčave sa nemá piť chladné? Učili? Vraj je to ťažké doviesť do praxe? Tak potom sa nečudujte, že vás bolí hrdlo, brucho a máte hnačky," idú mi v hlave arabské odpovede.      Kúpajúca sa arabská rodinka. Do pozornosti dávam maminu napravo. Nie, nešnorchluje, ona sa zahaľuje.   Potím sa v tureckej saune kde je skutočné peklo a slaným pohľadom popoháňam padajúci piesok v presýpacích hodinách. Plávam viac ako za celý rok na Slovensku a zároveň kloktám slanú morskú vodu z bazéna ignorujúc myšlienku, že tak súčasne pijem vzorku moču celého sveta. Na to, koľko turisti vyhrievajúci sa pri mori či hotelových bazénoch vypijú behom hodiny nápojov a skoro vôbec nenavštevujú toalety, ale naopak - občas sa idú ochladiť do vody - je totiž len jedno vysvetlenie. Odmietam vynikajúce arabské zákusky a nahrádzam ich čerstvým ovocím. Pečivo vypadáva z hry, zelenina má na krku zlatý vavrín. Zo dňa na deň jem menej, alkohol som obmedzil na liečivé minimum. Chudnem, viac súložím, smejem sa, veľa čítam, píšem, fotím, tvorím! Vychutnávam si celodenné výlety do izraelského Jeruzalemu a jordánskej Petry a s každým dňom som čoraz viac spokojnejší. Duša spieva. Stačí naozaj iba naše rozhodnutie a prejedací maškarný zájazd sa môže zmeniť na pozitívnu jazdu.   Ale kde-tu sa mi vynoria spomienky ako som po páde socíku išiel do Talianska a prvýkrát videl západné upratané sociálne zariadenia na prvom autoodpočívadle. Pripadal som si ako opica, ktorá zliezla zo stromu. Spomínam na dovolenku v Rusku, kde som ako malý fagan ťahal obrovské drevené šachové figúrky a za uznalého šumenia sledujúcich starších Rusov „z toho chlapca niečo bude" robil svoje prvé tajné ťahy. Nechápal som, prečo sa mama prejedá toho hnusne chutiaceho kaviáru a odmietal som jesť otrasnú polievku z obilia či rýb. Teraz tu sedím ako dospelák a vďaka tomu, že vlastním byt, aj ako skutočný korunový milionár, pri arabskom bazéne a vysvetľujem priateľke, že raz sme boli ako malí s bracháčom a maminou na dovolenke v Bulharsku a tam sme spali na priváte u nejakých cudzích ľudí. Na sídlisku, v paneláku. V nejakej rodine ľudí, ktorým socialistický štát dovolil privyrobiť si a tak vypratali obývačku z dvojizbového bytu a celé leto ju prenajímali teplomilným dušiam zo spriatelených socialistických krajín.   - A ako ste chodili na pláž? - pýta sa ma priateľka.   - Normálne, mestskou emhádečkou. Ten bus šiel asi tridsať minút, ale do necelej hodinky sme boli na pláži.   - A čo ste jedli? - nechápe.   - Ja neviem. Mama vždy niečo splašila a uvarila, - odpovedám.   - Ja ti neverím...   - Nemusíš.... Dáme si niečo? - zahováram, ťažko jej totiž budem rozprávať koľko tajomstiev skrývala babkina povala.   - Áno, dvakrát piňakoládu. Láska, a zbehni aj po palacinky, dostala som chuť, - dodáva priateľka.   - Už idem...           Buďte šťastní, Hirax       Aktivity Hiraxa:   Pondelok,  28. 6. 2010, Topoľčany, 17:03 hod  Galéria (program pre  mladých - spevokol, básne Hirax), 18:07  reštaurácia Kaiserhof (Námestie  M. R. Štefánika). Čítačka Hirax, vstup  jeden úsmev.   Máj-jún 2010: Výstava Hiraxových fotografií z Thajska v martinskej   kaviarničke Kamala. Pozor, nejedná sa o žiadnu "galériu", ale o 12násť   záberov, ktorými som sa snažil vystihnúť túto krajinu. Vstup jeden   úsmev.   Štvrtok, 4. 11. 2010, Svet, Oficiálne vydanie   Hiraxovho štvrtého románu "Nauč ma umierať".   Štvrtok, 4. 11.   2010, 16:37, Bratislava, Svet knihy Bene Libri (v   centre mesta, Obchodná, druhé poschodie). Prvá Hiraxova čítačka k jeho   štvrtému románu "Nauč ma umierať". Vstup jeden úsmev.   Utorok, 9. 11. 2010, Považská Bystrica, knižnica,   havná budova, čitáreň o 16:04. Čítačka Hirax, vstup jeden úsmev.      Streda 10. 11. 2010 o 11:02 hod., Nitra, Krajská   knižnica K. Kmeťka. Čítačka Hirax, vstup jeden úsmev.   Štvrtok 11. 11. 2011(-1) o 11:11, Šaľa, gymnázium.   Čítačka Hirax, vstup jeden úsmev     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Ako urýchliť zámer? Obetou a pokorou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (2.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (1.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Všetci sme mágovia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Vnútorným dialógom k osobnému šťastiu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Baričák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Baričák
            
         
        baricak.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ľudia majú neuveriteľnú schopnosť pripísať všetko, čo bolo napísané autorovi, že to na vlastnej koži aj sám prežil. Prehliadajú schopnosť vnímania, pozorovania sveta a pretransformovania ho do viet s úmyslom pomôcť druhým. Ale na druhej strane sa mi dobre zaspáva s pomyslením, že ostatok sveta na mňa myslí. Cudzí ľudia o mne vedia všetko, teda aj to, čo neviem ani ja sám. Ďakujem im teda za ich všemocnú starostlivosť! Buďte všetci šťastní, prajem Vám to z celého môjho srdca. Hirax &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/anketa_0.php?id=754643" href="http://blueboard.cz/anketa_0.php?id=754643"&amp;amp;gt;Anketa&amp;amp;lt;/a&amp;amp;gt; od &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/" href="http://blueboard.cz/"&amp;amp;gt;BlueBoard.cz&amp;amp;lt;/a&amp;amp;gt; 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    558
                
                
                    Celková karma
                    
                                                7.12
                    
                
                
                    Priemerná čítanosť
                    3665
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šlabikár šťastia 1.
                        
                     
                                     
                        
                            Šlabikár šťastia 2.
                        
                     
                                     
                        
                            Vzťahy (Úvahy)
                        
                     
                                     
                        
                            Tak plače a smeje sa život
                        
                     
                                     
                        
                            Zachráňte malého Paľka!
                        
                     
                                     
                        
                            Po stopách komedianta
                        
                     
                                     
                        
                            Sárka Ráchel Baričáková
                        
                     
                                     
                        
                            Sex (Úvahy)
                        
                     
                                     
                        
                            NEVERŠOVAČKY (Básne)
                        
                     
                                     
                        
                            PRÍBEH MUŽA (Básne a piesne)
                        
                     
                                     
                        
                            SEKUNDU PRED ZBLÁZNENÍM (Román
                        
                     
                                     
                        
                            KÝM NÁS LÁSKA NEROZDELÍ (Román
                        
                     
                                     
                        
                            RAZ AJ V PEKLE VYJDE SLNKO/Rom
                        
                     
                                     
                        
                            ČESKÁ REPUBLIKA (CESTOPIS)
                        
                     
                                     
                        
                            EGYPT (Cestopis)
                        
                     
                                     
                        
                            Ekvádor (Cestopis)
                        
                     
                                     
                        
                            ETIÓPIA (Cestopis)
                        
                     
                                     
                        
                            FRANCÚZSKO (Cestopis)
                        
                     
                                     
                        
                            INDIA (Cestopis)
                        
                     
                                     
                        
                            JORDÁNSKO (CESTOPIS)
                        
                     
                                     
                        
                            KOSTARIKA (Cestopis)
                        
                     
                                     
                        
                            Mexiko (Cestopis)
                        
                     
                                     
                        
                            Nový Zéland (Cestopis)
                        
                     
                                     
                        
                            PANAMA (Cestopis)
                        
                     
                                     
                        
                            POĽSKO (Cestopis)
                        
                     
                                     
                        
                            SLOVENSKO (Cestopis)
                        
                     
                                     
                        
                            THAJSKO (Cestopis)
                        
                     
                                     
                        
                            USA (Cestopis)
                        
                     
                                     
                        
                            VIETNAM (CESTOPIS)
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Niečo o zrkadlení a učiteľoch
                                     
                                                                             
                                            Vlastné šťastie ide cez prítomnosť
                                     
                                                                             
                                            Áno, dá sa naraz milovať dvoch ľudí
                                     
                                                                             
                                            Chcete zmeniť vzťah? Zmeňte najprv seba.
                                     
                                                                             
                                            Chcete byť šťastnými? Prijmite sa.
                                     
                                                                             
                                            Ohovárajú vás? Zraňujú vás klebety? Staňte sa silnými!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Liedloffová Jean - Koncept kontinua
                                     
                                                                             
                                            Alessandro Baricco - Barbari
                                     
                                                                             
                                            Christopher McDougall - Zrození k běhu (Born to run)
                                     
                                                                             
                                            Ingrid Bauerová - Bez plenky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            The Killers
                                     
                                                                             
                                            Brandom Flowers
                                     
                                                                             
                                            Sunrise Avenue
                                     
                                                                             
                                            Nothing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Martin Basila
                                     
                                                                             
                                            Spomíname:
                                     
                                                                             
                                            Moskaľ  Peter
                                     
                                                                             
                                            Miška Oli Procklová Olivieri
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HIRAX Shop
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Skutočná Anna Kareninová bola Puškinovou dcérou
                     
                                                         
                       Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                     
                                                         
                       Surinam - zabudnutá juhoamerická exotika
                     
                                                         
                       Hodvábna cesta - 56 dní z Pekingu do Teheránu 2.časť ( Uzbekistan, Turkménsko, Irán)
                     
                                                         
                       Detskí doktori, buďte detskí
                     
                                                         
                       Nemohli nič viac
                     
                                                         
                       Metalový Blog - Folk metal crusade
                     
                                                         
                       Veľká noc vzišla z pohanských sviatkov
                     
                                                         
                       Stačilo p. premiér, oblečte sa prosím
                     
                                                         
                       Jiřina Prekopová: Podmienečná láska je bezcenná
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




