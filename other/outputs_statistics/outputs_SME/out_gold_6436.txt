

 Aby naše ženy nemuseli plakať. 
 Tento billboard ma naozaj zaujal, aj keď presne nerozumiem, čo tým chcela strana povedať. Ženy budú plakať, keď pôjdu ich muži a synovia do vojny s Maďarmi, alebo čo?? Naozaj si neviem vysvetliť hlavnú myšlienku, ktorú chcel dať tomuto dielu autor, ale môžem vysvetliť niečo iné. 
 Viete prečo veľa žien doma plače? 
 Náš dom je postavený rovno oproti dedinskej krčme, no počas šiestich rokov, čo tam bývame, som v nej nebola ani raz. Keby som mala stráviť v spoločnosti tých skrachovaných existencií čo len pol minúty, asi by som sa prepadla od hanby. (Názvom skrachované existencie tých ľudí nechcem urážať, iba opisujem ich reálny psychický, fyzický, emocionálny a ekonomický stav.) Muži, ktorí od siedmej rána sedia v krčme alebo pred ňou, už vtedy naliatí v alkohole, o deviatej doobeda spia na stole, na obed sa poberú na chvíľu domov (za svojimi plačúcimi ženami, ak im nejaké ostali), no poobede sa opäť vracajú za starý známy stôl a pohárik, opäť sa opijú (nejdem písať ako čo) a neskoro večer ich niekto musí odviesť domov, aby náhodou nezaspali niekde nad kanálom. 
 Myslím, že ženy takých mužov sa doma naplačú dosť. Ešte šťastie, že pán Slota ukazuje verejnosti len svoj obraz triezveho a striedmeho života. Aby sa podľa neho chlapi naučili a ženy nemuseli plakať. 
 Aby bol národ múdry. 
 Hm... na toto sa dá reagovať veľmi jednoducho - keby sa mal národ a vlastne hlavne deti vychovávať logikou SNS, myslím, že by sme tu onedlho mali samých malých rasistov. Okrem toho, neviem, či som ja nejaká nedovzdelaná, ak nepoužívam Slotov slovník. Úroveň jeho vyjadrovania je naozaj vysoká, tak neviem... možno keď budem stará a prepitá ako on... 
 Aby naše hranice zostali našimi hranicami. 
 Toto je tá ich večná paranoja. Nebezpeční útoční Maďari (toto nie je môj názor, len sa snažím vžiť do logiky SNS) chcú obsadiť Slovensko. Stav najvyššej pohotovosti! :):) To vážne? Choďte sa do nejakej pohraničnej maďarskej obce opýtať tých Maďarov, čo ich trápi. Naše územie to nebude, majú dosť vlastných starostí. Samozrejme sa nájde aj tam niekoľko extrémistov (veď aj my máme SNS), ale buďme trochu realisti. Naše hranice naozaj zostanú našimi hranicami. 
 Aby naše deti mali istotu. 
 Táto veta môže tiež vyjadrovať strach pred Maďarmi (viď. predchádzajúci odsek), alebo sa nám SNS snaží povedať niečo o inej istote? Snáď ekonomickej? Sociálnej? Morálnej? Nech napíšem čokoľvek, ani jedna istota sa mi nespája s SNS. Politická - to určite, veď by sa najradšej vrhli na Maďarov. Ekonomická - asi nie, pokiaľ budeme rodinkárčiť, dávať peniaze kamarátom, korumpovať a pod. Sociálna - pri spojení s SNS mi pri tomto slove nesvieti nič, oni sa proste starajú len o Maďarov. A morálna - opäť sa môžem odvolať na predchádzajúce odseky (viď. Vyjadrovanie Jána Slotu a jeho alkoholizmus). Tak akú istotu to teda mali na mysli? 
 Aby sme nekŕmili tých, čo nechcú pracovať. 
 To najlepšie som si nechala na koniec a bude veľmi jednoduché to vysvetliť. SNS si vzala na mušku Rómov. Viem, je veľa takých, čo nepracujú, ale v prípade tejto strany stačilo pohľadať tvár na billboard vo vlastných radoch.... hm... taký Janko Slota. Keď je niekto poslanec, berie za túto prácu plat. A ak už nič zmysluplné nenavrhuje a nezapája sa do činnosti parlamentu aktívne (neráta sa osočovanie ostatných poslancov), človek by očakával, že ten plat teda dostane za to, že sa zúčastňuje politického diania aspoň pasívne - sedením na zadku v parlamente. Ale pán Slota nie, pán veľkomožný má na všetko ľudí, dokonca aj takých, čo sa zaňho podpíšu, keď treba. 
 A tak sme sa dostali k tomu, že kŕmime aj skorumpovaných opilcov, ktorým sa nechce pracovať. 
   
 Tak prosím, skúsme Slotu nekŕmiť ďalšie štyri roky. 

