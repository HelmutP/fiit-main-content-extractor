

   
 Keď sa každé ráno môžme zobudiť zdraví, 
 sme šťatný a máme celý deň úsmev na tvári. 
 To, že prekonáme nejaký stres a depresiu, 
 je dôkaz toho, že nemáme, zo života Fóbiu... 
 Umyjeme sa pred zrkadlom, 
 je to naša očista pred zajtrajškom. 
 Vážme si tie dni, kedy môžme stávať a isť vpred, 
 ako tie, ktoré nas privedú do svete prázdnych viet. 
 Skúsme premyšlať o budúcnosti z nadhľadom, 
 nevieš, kedy príde niekto z nepríjemným oznamom. 
 Kašli na to, čo bolo včera a ži pre dnes, 
 zbytočne sa stresuješ nad tým, čo je už preč. 
 Skúsme si vychutnať pocit o ktorom hovoríme "TERAZ", 
 nikdy nevieš, kedy budeš ťahať za ten nesprávny povraz. 
 Ludia v dnešom svete nie su vôbec zlí, 
 skus začať od seba a vytvor si nejaké sny. 
 Snívaj o niečom, čo nikdy nemôžeš mať, 
 možno to raz príde, ako tvoj životný zvrat. 
 Možno to bude len sen včerajšej noci, 
 no aj tie sa rataju do života, hoci... 
 ich nie je tak veľa, 
 aby som si povedal, že mi "chutila včerajšia večera". 
 Postav sa na miesto svojho vytúženého cieľa, 
 predstav si pod tým túžbu, silnú ako viera. 
 Krič a volaj koľko ti hrdlo ráči, 
 ser na to, že v tvojom okolí žiju sráči. 
 Sú to iba závistlivé svine, 
 ktoré keď nemajú, tužia po vidine. 
 Keď pojdeš večer unavený spať, 
 vieš, že tento deň nevráti žiadna časť. 
 Myslím tým tie trápne televízne seriály, 
 ktoré si môžeš pozrieť doma, na internete či v žiali... 
   

