
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katka Benková
                                        &gt;
                rada pečiem
                     
                 Muffiny s whisky a iné. 

        
            
                                    26.4.2009
            o
            14:41
                        |
            Karma článku:
                11.25
            |
            Prečítané 
            5660-krát
                    
         
     
         
             

                 
                    Tak som si uvedomila, že som vlastne ešte nikdy nezverejnila žiaden recept a keďže ma cez víkend pochytila obsesia (rozumej posadnutosť) muffinmi, tak Vám ponúkam zopár receptov.
                 

                 Muffiny s whisky: (suroviny na cca 12 kúskov)      Budeme potrebovať:      250 g múky (zmes polohrubej a hladkej) 100 g kryštálového cukru 125 g masla 1 vanilkový cukor 1 KL sódy bicarbóny 1 čokoládu na varenie 4 PL whisky 4 KL instantnej kávy 1 dcl vody (na uvarenie kávy) 2 vajcia kúsok kvalitnej čokolády   Postup:   Držíme sa zásady, sypké suroviny miešame zvlášť a tekuté zvlášť. Múku preosejeme a zmiešame so sódou.      Pridáme cukor a vanilkový cukor a štipku soli. Vo vodnom kúpeli rozpustíme maslo a čokoládu na varenie. Uvaríme silnú kávu, do ktorej zamiešame 4 lyžice whisky. Do čokolády zamiešame kávu s whisky a keď je dostatočne chladná tak aj vajcia. Do sypkej zmesi múky vylejeme tekutú a premiešame, snažíme sa miešať jemne a v žiadnom prípade nepoužívame mixér (platí to pre všetky muffinky), na koniec jemne zapracujeme kúsky kvalitnej čokolády. Cesto plníme do košíčkov, alebo do formy na muffiny, ktorú si ale vopred vymastíme tukom. (Ja som použila kombináciu a do formy som dala košíčky.) Cesto plníme asi do dvoch tretín, pretože zväčší svoj objem. Pečieme vo vyhriatej rúre rozohriatej približne na 180 - 200 stupňov Celzia asi 15 minút. Počas pečenia sa snažíme odolať a trúbu neotvárať priveľmi, aby nám nespľasli.       Muffiny s ovocím: (suroviny na cca 12 kúskov)      Základné cesto:   250 g múky ( zmiešanú polohrubú a hladkú) 100 g kryštálového cukru 125 g masla 1 vanilkový cukor 1 KL sódy bicarbóny 2 vajcia trocha soli   Náplň: Biely jogurt (3 - 4 lyžice) Mrazené ovocie (lesné, alebo čučoriedky, maliny, ostružiny..)      Postup:   Múku preosejeme, zmiešame so sódou, vanilkovým cukrom, štipkou soli.   V ďalšej miske vymiešame maslo s cukrom, pridáme vajcia a jogurt.    Obe zmesi jemne premiešame a pridáme mrazené, alebo zavárané drobné ovocie.      Plníme do košíčkov a pečieme cca 15 minút.                        Muffiny s brusnicami: (suroviny na cca 12 ks)      Základné cesto:   250 g múky ( zmiešanú polohrubú a hladkú) 100 g kryštálového cukru 125 g masla 1 vanilkový cukor 1 KL sódy bicarbóny 2 vajcia trocha soli   Náplň:   Kyslá smotana (3 - 4 lyžice) Sušené brusnice ( postačuje hrsť)      Postup:   Cesto pripravíme ako v predchádzajúcom recepte, len namiesto jogurtu použijeme kyslú smotanu a do hotového cesta zamiešame hrsť sušených brusníc (sú dostať v každom hypermarkete a hrsť bude stáť iba pár centov).      Sú nádherne červené a v žltučkom muffine vyzerajú prekrásne.      Ak Vám ich pár zvýši tak ich pokojne použite na ozdobenie. Môžete vyskúšať aj citrónovú polevu a pár kúskov pozapichovať na povrch.       Muffiny s kúskami čokolády: (suroviny na cca 12 ks)      Základné cesto:   250 g múky ( zmiešanú polohrubú a hladkú) 100 g kryštálového cukru 125 g masla 1 vanilkový cukor 1 KL sódy bicarbóny 2 vajcia trocha soli   Náplň: Kyslá smotana (3 - 4 lyžice) Kúsky čokolády (mne sa osvedčila Milka s nugátom plnená orieškami - také 3 v jednom)      Postup:   Cesto pripravíme ako v predchádzajúcom recepte. Do hotového cesta pridáme kúsky čokolády. Po upečení môžeme poliať čokoládovou polevou či inak ozdobiť.       Rady na záver:   Ja keď pečiem podľa receptu, automaticky dám všade menej cukru. V týchto receptoch je množstvo cukru už minimalizované, ale môžete ho dať menej v prípade, že muffiny plníte čokoládou.   V prípade, ak na ovocné muffiny použijete podobnú mrazenú zmes lesného ovocia ako ja, tak Vás upozorňujem, že aj 200 g je veľa a tak Vám jedno balenie bude stačiť na dve dávky.   Muffiny si môžete upiecť vopred a dať zmraziť. K nedeľnej rannej káve, akoby ste našli.      Pečenie muffinov je jednoduché, nezaberie Vám veľa času a výsledok stojí za to.   Ak sa rozhodnete vymeniť maslo za margarín, whisky za rum, 70 % čokoládu za nejakú z menším obsahom kakaa uisťujem Vás, že sa to na kvalite odrazí, ale dobré budú aj tak, akurát, že oveľa lacnejšie. Nájdite si pre vás najvýhodnejší pomer kvality a ceny. Čím kvalitnejšie suroviny použijete, tým ale lepšie muffinky budete jesť a prepočet na jeden kus bude stále menší, ako keď si ho kúpite v obchode.      Prajem Vám dobrú chuť! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Benková 
                                        
                                            Barbra Streisand – žena plná protikladov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Benková 
                                        
                                            Dobrá reštaurácia v Bratislave? Benihana.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Benková 
                                        
                                            Krása.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Benková 
                                        
                                            Rýchlo do postieľok, veď už mesiac svieti. Rozlúčte sa s mackom ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Benková 
                                        
                                            Pravda je v nás.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katka Benková
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katka Benková
            
         
        katkabenkova.blog.sme.sk (rss)
         
                                     
     
        spiritual coach, poradca v ťažkých životných situáciach
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2971
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ako to vidím ja
                        
                     
                                     
                        
                            rada pečiem
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            cestovanie
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Chalíl Džibrán - Prorok
                                     
                                                                             
                                            Paulo Coelho
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            simply red
                                     
                                                                             
                                            Mango Molas - Ultima Rumba
                                     
                                                                             
                                            Zdenka Predná
                                     
                                                                             
                                            Lionel Richie
                                     
                                                                             
                                            Sade
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivka - fotografie
                                     
                                                                             
                                            austrálčan
                                     
                                                                             
                                            recepty
                                     
                                                                             
                                            život nie je jednoduchý
                                     
                                                                             
                                            cestovanie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Barbra Streisand – žena plná protikladov.
                     
                                                         
                       Zblúdilá ovečka, ktorú pastier nestráca
                     
                                                         
                       Dobrá reštaurácia v Bratislave? Benihana.
                     
                                                         
                       Psie a ľudské duše
                     
                                                         
                       Na život máme 4 hodiny
                     
                                                         
                       Pravda je v nás.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




