

 Tak by som sa snažila vryť si všetko hlboko do pamäte. Zvečniť si tú chvíľu a stovky jej podobné. Zapamätať si každú vrásku na tvojej tvári, tvar každej mozole na tvojich rukách. Zadívať sa hlboko do tvojich oči, a navždy si zapamätať ich odtieň a lesk. 
  Čas nemilosrdne trhá listy z knihy mojich spomienok a ja mám obavy, že príde deň, keď už v nej neostane ani jedna strana, len nadpis. Ktorý, ostane posledným svedkom kapitoly, ktorá je pre mňa najcennejšie. Keby sa tak dala zapečatiť táto kniha spomienok, aby čas neporušil jej obsah, aby v nej nevybledla ani vôňa pitralonu, ani nezriedili sa počty spomienok na spoločné chvíle. 
  Viem, že sa to nedá. Panta rhei. Čas plynie a veci sa menia. Jedna vec sa však nemení, dnes mi chýbaš práve tak veľmi, ako pred dvadsiatimi rokmi. 
  Papa, všetko najlepšie k tvojim narodeninám. 

