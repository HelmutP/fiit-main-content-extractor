
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Michalčík
                                        &gt;
                Zážitníček
                     
                 Slovensko má Filca a Weissa 

        
            
                                    24.6.2010
            o
            19:04
                        (upravené
                24.6.2010
                o
                19:10)
                        |
            Karma článku:
                7.50
            |
            Prečítané 
            2020-krát
                    
         
     
         
             

                 
                    Poctivosťou ďalej zájdeš, to je jedno z hesiel, ktoré vyznávajú títo výnimoční tréneri. Aspoň tak sa mi to javí ako radovému fanúšikovi. Každý je síce úplne iný (Weiss prchký, emocionálny, Filc s väčšou vnútornou disciplínou, viac diplomatický), ale obidvaja chcú od svojich zverencov poctivý výkon.
                 

                     Myslím si, že aj preto môžeme pri tých krásnych chvíľach, aká sa nám naskytla aj dnes, prežiť krásnu, povedal by som až čistú radosť. Z vypočítavosti takú radosť nie je možné vydolovať. Všetci sme dnes opäť videli, ako tam naši nechali srdce, že dali do toho všetko a ešte niečo naviac. Že naše víťazstvo (ako si ho ľahko pripisujeme) je zaslúžené. Nemožno ich pochváliť za taktizovanie, veď práve zo zbytočného naháňania sa dopredu dostali prvý a možno aj druhý gól. Ale treba ich pochváliť za športového bojovného ducha, vďaka ktorému strelili aj tretí gól, ktorý sa ukázal byť víťazný. A to mi je omnoho sympatickejšie a nielen to. Je to tak správne.   Máme Filca a Weissa, ktorý ukázali státisícom mladým, že sa oplatí byť poctivým a oplatí sa popasovať zo súperom a nevzdávať sa. A to je cesta, ktorá vedie k úspechu nielen v športe. Slovensko do toho.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Michalčík 
                                        
                                            V šťastí i nešťastí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Michalčík 
                                        
                                            Pýtame sa čo je láska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Michalčík 
                                        
                                            Ľudskosť sa nedá prikázať, ale ukázať áno
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Michalčík 
                                        
                                            Rozšírené znenie vyhlásenia podporovateľa PRIDE
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Michalčík 
                                        
                                            Rodina – bavorák medzi inými typmi spolužitia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Michalčík
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Michalčík
            
         
        marekmichalcik.blog.sme.sk (rss)
         
                                     
     
        Pútnik, ktorý sa snaží nestratiť.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    26
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2053
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názorníček
                        
                     
                                     
                        
                            Zážitníček
                        
                     
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Národný pochod za život
                                     
                                                                             
                                            Aliancia za rodinu
                                     
                                                                             
                                            Národný týždeň manželstva
                                     
                                                                             
                                            Fórum života
                                     
                                                                             
                                            Zachráňme životy
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       V šťastí i nešťastí
                     
                                                         
                       Ocitli sa Clara a Kristy v sexistickej reklame?
                     
                                                         
                       Pýtame sa čo je láska
                     
                                                         
                       Ľudskosť sa nedá prikázať, ale ukázať áno
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




