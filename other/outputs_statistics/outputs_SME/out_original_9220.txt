
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Trpčevski
                                        &gt;
                Nezaradené
                     
                 O potratoch alebo o potratení zdravého rozumu 

        
            
                                    19.6.2010
            o
            20:55
                        (upravené
                19.6.2010
                o
                21:04)
                        |
            Karma článku:
                15.95
            |
            Prečítané 
            2604-krát
                    
         
     
         
             

                 
                    Priznávam sa, som krajne netolerantný. Krajne netolerantný ku všetkým extrémistom. Napriek tomu sa snažím pochopiť ako asi rozmýšľajú alebo skôr nerozmýšľajú niektorí ľudia. Myslíte, že aj takýto extrémisti uvažujú nad názormi ostatných? Viete, som človek dobrej vôle a mám obavu. Obavu, že sa pri hre s ohňom môžu popáliť.
                 

                 Asi najzúrivejšia kampaň bola venovaná téme potratov, resp. ich zákazu. Ako asi rozmýšľa taký človek ako Tutková? Potraty sú zlé. Súhlasím. Čo ďalej? Potraty sú zlé. Chápem. Ďalej? Potraty sú zlé. To je všetko k danej téme? Čo takto príčiny, nejaké súvislosti? Sú za ochranu života, za ochranu rodiny (preto protestujú napríklad proti registrovanému partnerstvu, asi si myslia, že ak zakážu partnerstvá, zmiznú homosexuáli). Aj ochrana života, aj ochrana rodiny, aj láska k vlasti sú krásne veci, ale stačí niečo zakázať alebo prikázať? Budeš vlastenec, budeš chrániť rodinu, budeš heterosexuál! A problémy sú vyriešené.   Možno sa takýto ľudia nezamýšľajú nad tým, aké sú príčiny a aké by boli následky takého kroku. Začnem od následkov. Menej potratov, niekoľko životov by zachránili. Viac amatérskych potratov, pár žien zase príde o život (to ale nie je ich téma). Potratový turizmus (Čo sa deje za hranicami nás nezaujíma). Asi sa nezamysleli títo ľudia nad tým, že na potraty chodia ľudia v ťažkej situácii, nie od radosti. Samozrejme, na to majú liek, abstinenciu do svadby, keďže antikoncepcia je taktiež neprípustná pre extrémistov. Na druhej strane, asi pozabudli na to, že niekto môže mať aj iný názor, nebyť katolíkom (irelevantné, sú nekatolíci vôbec ľudia?). Ich ani nezaujíma, že nejaká žena mohla byť znásilnená - jej problém, nás to nezaujíma, v danom momente je pre nás len oplodnený objekt, nespadá do našej témy, naša téma je ochrana nenarodeného dieťaťa. Jej život nie je podstatný, im ide o „ich tému". Kedy začneme teda riešiť problémy od počiatku alebo ak chcete od počatia? Prečo pani (či slečna?) Tutková nebehá po Slovensku a nerobí osvetu a nerozhadzuje na stredných školách kondómy, verím, že vie, čo to je (a na čo to slúži)? Možno by tak reálne nejakému potratu zabránila. Prečo sa nevenuje téme nechcených detí v detských domovoch, či téme detí v odpadkových košoch po narodení? Lebo to nie je ich problém, to nie je ich téma. Extrémistov zaujíma len ich téma, pre ktorú nevidia žiaden iný problém. V skutočnosti ich ten ľudský život až tak nezaujíma, hlavne nie po narodení, to si už človiečku rob, čo chceš, nás nezaujímaš, nie si naša téma.   Takému človeku je jedno, či pôjde KDH do vlády so zlodejom, peniaze nie sú podstatné. Potraty zo sociálnych dôvodov, keď sa matka bojí, že nebude mať ako dieťa uživiť? Tam už ukradnuté milióny môžu chýbať, ale čo tam po tom? Hlavná je tá ich téma! Takýto ľudia nemajú žiadne iné princípy, nemajú nič iné, preto s nimi súcitím. Nemáme im čo závidieť, lebo si často škodia. Taká výhrada vo svedomí. Ak by som chcel byť taký netolerantný ako vyššie uvedené, tak by som povedal, nech si to schvália, ich to bude bolieť napokon najviac. Už vidím ten pracovný pohovor: U nás sa predáva aj v nedeľu, nebude Vám to prekážať? Chcel by som uplatniť... Ďakujeme, ozveme sa Vám. Dá sa aj klamať, ale zákonník práce pozná trojmesačnú výpovednú lehotu a ja by som teda nechcel mať zamestnanca, ktorý mi od začiatku klamal. Voči výhrade nemám výhrady, ale lož mi prekáža, musíme sa rozlúčiť. Mne to nevadí, nech si to schvália, ale potom budú nariekať. Áno, chcú tú výhradu práve preto, aby takéto problémy nemali. Jediný problém je ten, že nie všetko sa dá prikázať, nariadiť a nie všetko dopadne tak, ako autor chcel...   Preto by som bol rád, ak budú chcieť čokoľvek takýto extrémisti prijať, aby sa najprv poradili s niekým, kto nepotratil rozum, aby si náhodou neublížili. Viete, kto sa hrá s ohňom... môže okrem svojej stodoly zapáliť aj tú našu.  Tak buďte prosím tolerantní, tolerantnejší, ako ja a vysvetlite im to. Ďakujem.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (280)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Trpčevski 
                                        
                                            Pán Matovič, tu už sranda končí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Trpčevski 
                                        
                                            Japonský zápisník: Hlas podobný Ficovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Trpčevski 
                                        
                                            Ako rýchlo sa KDH učí z vlastných chýb?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Trpčevski 
                                        
                                            Rozpočet 2014 - chvalabohu, že žiaľbohu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Trpčevski 
                                        
                                            Maslo na Ficovej hlave a U.S. Steel
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Trpčevski
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Trpčevski
            
         
        trpcevski.blog.sme.sk (rss)
         
                                     
     
        Analytik.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1598
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pán Matovič, tu už sranda končí
                     
                                                         
                       Jáj, ale som sa vytočil! Čo Paška robí z parlamentu, je hanba.
                     
                                                         
                       Japonský zápisník: Hlas podobný Ficovi
                     
                                                         
                       Ako rýchlo sa KDH učí z vlastných chýb?
                     
                                                         
                       Ako sa chceli smeráci z krízy preinvestovať
                     
                                                         
                       cvičíme svojho psa. odoberte nám ho.
                     
                                                         
                       Na vlastné oči: Parlamentná fraška s nezamestnanosťou
                     
                                                         
                       Sulíkova karma
                     
                                                         
                       Smutné oči pána Richtera
                     
                                                         
                       Šestoro dobrej reštaurácie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




