
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Odler
                                        &gt;
                ...a iná kultúra
                     
                 Táranie o stredoeurópskych dejinách 

        
            
                                    8.4.2010
            o
            10:33
                        (upravené
                8.4.2010
                o
                9:10)
                        |
            Karma článku:
                9.42
            |
            Prečítané 
            2559-krát
                    
         
     
         
             

                 
                    V pondelok uverejnil L. Palata na stránke Lidových novin stĺpček Lužice, aneb proč jsme malý stát. Obsahuje sympatické názory, týkajúce sa lužických Srbov, ale aj typické inteleguánske táraninky o dejinách, ktoré majú objasniť, prečo sa stredná Európa nenachádza ešte vo väčšej mizérii.
                 

                 
  
   Preberme si tých zopár viet, ktoré chcú načrtávať dejinnú situáciu.       Nám se nějakým zázrakem povedlo v devatenáctém století znovu vzpamatovat, ale je prostě fakt, že první čeští buditelé psali své práce německy, aby jim vůbec někdo, kdo uměl číst a psát, rozuměl.       Po nemecky písali hlavne vedecké diela, tak, aby boli dostupné odborníkom. Ak chceli osloviť české publikum, museli samozrejme písať po česky. L. Palata ďalej akosi zabudol, že dve výrazné postavy českého národného obrodenia, Ján Kollár a Pavol Jozef Šafárik, sa narodili na Slovensku a v ňom aj istú časť života strávili. Teda nielen Česi (a Moravania), alebo ako raz žartom napísal historik Dušan Třeštík Čechožidonemci. Kollár sa ešte v jednom z posledných vydaných diel ponosuje, že sa nikdy nedokázal naučiť výslovnosť písmena ř. Kollár i Šafárik pozitívne hodnotili rozmanitosť slovanských jazykov, čo bolo medzi českými obrodencami výnimočné, ale z ich hľadiska pochopiteľné (Macura 1995, 31-2). František Palacký absolvoval veľkú časť svojich stredoškolských štúdií v Trenčíne a v Prešporku a istý čas býval v rodine Samuela Štúra, Ľudovítovho otca. K hlbšiemu štúdiu češtiny ho okrem iného priviedla trápna udalosť, keď nevedel svojmu slovenskému hostiteľovi F. Bakošovi vysvetliť význam niektorých českých novotvarov. Príhodu uvádza český historik J. Kořalka, Palackého životopisec, ktorého azda ani p. Palata nemôže podozrievať zo slovenskej tendenčnosti.     Súhrnne by sa dalo povedať, že p. Palata akosi rád zabudol spomenúť, ak o tom vôbec vie, že istú zásluhu na zachovaní a udržiavaní českej jazykovej kultúry mali aj slovenskí evanjelici v Uhorsku. A nielen v 19. storočí, ale aj predtým.  A kdo ví, kdyby jinak osvícený panovník Jozef II. panoval stejně dlouho, jako jeho matka Marie Terezie, tak by možná žádné národní obrození nebylo. Protože Jozef II. si přál, aby se v jeho zemi mluvilo jazykem panovnického dvora, tedy německy. Jozef II. však nepanoval dlouho, takže z té jeho osvícenecké germanizace nic nebylo.   To je len české hľadisko. Z pohľadu nemaďarských národností v Uhorsku nastalo v období osvietenského absolutizmu, za Márie Terézie i Jozefa II., oživenie národných kultúr a literatúr, veď práve vtedy pôsobil napr. Anton Bernolák. A je otázne, či zavedenie nemčiny ako úradnej reči by ovplyvnilo jazyk obyvateľov krajiny. V Uhorsku bola úradnou rečou temer deväťsto rokov latinčina et Hungari nobiscum non hodie Latine scribent. Dějiny zemí koruny české II uvádzajú o Prahe, že nemecké publikácie z Frankfurtu, Norimbergu, Lipska a Kolína sa údajne predávali v 18. stor. v rovnakom počte ako produkcia všetkých pražských nakladateľstiev, českých i nemeckých, spolu. Predaj kníh zrejme nebol úradne oktrojovaný… A za ponemčovanie rozhodne nemôže iba Jozef II. Jezuitský rád bol zrušený v roku 1773, teda za vlády Márie Terézie - práve jezuiti vyučovali na gymnáziách češtinu, piaristické gymnáziá boli neskôr orientované na nemčinu.       Naštěstí se "žalář národů" ukázal v příštím století natolik fajnovým kriminálem, že jsme v něm zvládli nejen národní obrození, ale i přípravku na Československo. V něm jsme původně jako součást "československého národa" zachránili zbytky Slováků, kteří na tom byli podobně jako Lužičtí Srbové, ba ještě hůř, protože zatímco Lužice byla korunní zemí, Slovensko nikdy žádný celek ani v rámci Uherska netvořilo.  Azda p. Palata niekedy počul o problematike tzv. Nitrianskeho kniežatstva. Existovalo v rámci Uhorska prinajmenšom v 11. storočí. Interpretačné obsahy jeho významu sa samozrejme líšia, maďarskí historici ho skôr neberú vážne, pre slovenských je možnou stopou územnej integrity krajiny nad Dunajom po páde Veľkej Moravy. Ďalšou by mohla byť ostrihomská diecéza, ktorá mala síce sídlo za Dunajom, ale od 11. stor. sa v prevažnej väčšine rozkladala na predpokladanom území bývalého Nitrianskeho biskupstva. Ba aj niektorí českí historici sa nazdávali, že prostredníctvom územia kniežatstva mohlo byť odovzdané dedičstvo hradskej organizácie medzi Veľkou Moravou a Uhorským kráľovstvom (čo sa ale dnes zdá byť menej pravdepodobné). Jeden z včasnostredovekých kronikárov radí dokonca Uhorsko medzi krajiny, kde sa hovorí najmä slovanským jazykom. Podľa koho? Podľa Maďarov určite nie. Vo vrcholnom stredoveku sa napr. v jednej listine z Liptova spomína cesta versus Hungariam – do Uhorska. Na Liptove teda vedeli, že Uhorsko v „užšom zmysle slova“ je smerom na juh.     Isteže, Slovensko nikdy netvorilo korunnú krajinu. Ale existujú aj historické atlasy, ktoré kreslia hranicu Svätej ríše rímskej nemeckého národa a Uhorska na Morave a v českých krajinách vyznačujú krásne nemecké mestá Prag a Brünn.   Slovensko nikdy žádný celek ani v rámci Uherska netvořilo. Tedy pokud nepočítáme název "Horní Uhry", bezhraniční označení hornatého severu Uherska, který je dnes, Bůh ví proč, považován na Slovensku za iredentistickou nadávku.   Spomenieme si na názov Felvidék, ktorý má oveľa hanlivejší obsah. Horné Uhorsko rozhodne také vášne nevyvoláva (v Memorande bol použitý termín „hornouhorské slovenské okolie“). Kronikár Ulrich z Richentalu píše v Kostnickej kronike o „slovenských krajoch“ medzi Uhorskom a Poľskom. Takže jestvoval iba jeden názov? A našli by sme aj ďalšie.  Dali by sa zhromažďovať aj iné fakty na vyvrátenie povrchných tvrdení p. Palatu. Načo však? Jedna z najkrajších vecí na dejinách je to, že sa nedajú zhrnúť do niekoľkých bystro sa tváriacich viet.  Odporúčaná literatúra (nielen pre p. Palatu)      Bělina, Pavel a kol. 1999: Dějiny zemí Koruny české. II. Od nástupu osvícenství po naši dobu. Praha.   Kořalka, Jiří, 1998: František Palacký (1798-1876): životopis. Praha.   Macura, Vladimír, 1995: Znamení zrodu: české národní obrození jako kulturní typ. Jinočany.   Papsonová, Mária - Šmahel František - Dvořáková Daniela, 2009: Ulrich Richental: Kostnická kronika: historické rozprávanie o meste, ktoré sa stalo stredom Európy, a čo to znamenalo pre Slovákov a Čechov. Budmerice.   Steinhübel, Ján, 2004: Nitrianske kniežatstvo: počiatky stredovekého Slovenska: rozprávanie o dejinách nášho územia a okolitých krajín od sťahovania národov do začiatku 12.storočia. Bratislava.       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Odler 
                                        
                                            Egypt v slovenskej kultúre 19. storočia (1800 – 1918)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Odler 
                                        
                                            Novinky z Avarského kaganátu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Odler 
                                        
                                            Ministerstvo školstva likviduje úspešný projekt
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Odler 
                                        
                                            "Kováč má prsty ako krokodílie pazúry"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Odler 
                                        
                                            Pohreb Václava Havla a obchody
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Odler
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Odler
            
         
        odler.blog.sme.sk (rss)
         
                        VIP
                             
     
        Dvadsaťsedemročný absolvent pravekej archeológie, egyptológie a klasickej archeológie na Univerzite Karlovej. Vedecký pracovník a doktorand Českého egyptologického ústavu. Vo voľnom čase človek, väčšinou čítajúci...
Mojej vedeckej činnosti je venovaný profil na serveri academia.edu, nájdete tu odkazy na publikácie, prednášky a účasť na konferenciách a knižné recenzie.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    87
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2202
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Básničky
                        
                     
                                     
                        
                            Literatúra
                        
                     
                                     
                        
                            ...a iná kultúra
                        
                     
                                     
                        
                            Zo zápisníka
                        
                     
                                     
                        
                            Zo správneho uhla
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Rozhovor
                                     
                                                                             
                                            Dotyky Egypta so slovenskou kultúrou
                                     
                                                                             
                                            Merenptah - úspešný muž v tieni slávneho otca
                                     
                                                                             
                                            Ono no Komači: Do slov má láska odívá se
                                     
                                                                             
                                            Ján Rozner: Sedem dní do pohrebu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Veronika Dulíková et al.: Pražské egyptologické studie VIII
                                     
                                                                             
                                            Times, Signs and Pyramids
                                     
                                                                             
                                            Miroslav Bárta - Martin Kovář et al.: Kolaps a regenerace
                                     
                                                                             
                                            Andreas Hauptmann: The Archaeometallurgy of Copper
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Umm Kulthum
                                     
                                                                             
                                            Klasická mačička
                                     
                                                                             
                                            Český rozhlas D-Dur
                                     
                                                                             
                                            Leoš Janáček
                                     
                                                                             
                                            Dream Theater
                                     
                                                                             
                                            Piano Society
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog Zory Hesovej
                                     
                                                                             
                                            Blog Šádího Shanaáha
                                     
                                                                             
                                            Ďobov blog
                                     
                                                                             
                                            Blog Tomáša Uleja
                                     
                                                                             
                                            "Pražáci"
                                     
                                                                             
                                            Katka Hybenová
                                     
                                                                             
                                            Matúš Maciak
                                     
                                                                             
                                            Jakub Rojček
                                     
                                                                             
                                            Matúš Soták
                                     
                                                                             
                                            Matej Pivoluska
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Nadácia Aigyptos
                                     
                                                                             
                                            Zlatý fond SME
                                     
                                                                             
                                            Egyptology Resources
                                     
                                                                             
                                            Blízky Východ po slovensky
                                     
                                                                             
                                            Český egyptologický ústav
                                     
                                                                             
                                            PES
                                     
                                                                             
                                            Antiquity Journal
                                     
                                                                             
                                            Archeologický výskum v Uzbekistane
                                     
                                                                             
                                            Český rozhlas Leonardo
                                     
                                                                             
                                            Pražské antikvariáty
                                     
                                                                             
                                            ETANA
                                     
                                                                             
                                            Yale Courses
                                     
                                                                             
                                            Grécky text Biblie
                                     
                                                                             
                                            Digital Egypt
                                     
                                                                             
                                            Stránky V. Brůnu
                                     
                                                                             
                                            AMAR
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Egypt v slovenskej kultúre 19. storočia (1800 – 1918)
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




