




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 
?UDIA A UDALOSTI 
Prv? p?sac? stroj 
 
 Vydan? 22. 7. 2004 o 0:00 Autor: ZUZANA V.  O?EN??OV?
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (1)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 Ve?k? ?as? osudu plan?ty sa dnes riadi na z?klade  mili?rd e-mailov?ch spr?v, vyklep?van?ch mili?nmi prstov do kl?vesn?c po??ta?ov na celom svete. Intelektu?li oslavuj? "n?vrat slovesnosti", ?kolopovinn? deti vyrastaj? s po??ta?mi a smskami a v schopnosti ?sporne slohova? v?razne prevy?uj? svojich rodi?ov. ?astej?ie ne? ly?ice ?i inej ?udskej  bytosti sa dnes ?udia dot?kaj? n?stroja, v?aka ktor?mu v?etky tie e-maily a spr?vy te?? - kl?vesnice. Celoplanet?rny rachot prstov ?to?iacich na tla?idl? s p?smenami v?ak nemaj? na svedom? kon?trukt?ri osobn?ch po??ta?ov, ale osamel? ?ud?k z Detroitu 19. storo?ia, vyn?lezca p?sacieho stroja Willam Burt.  Svoj v?mysel si dal patentova? 23. j?la 1829, pred 175 rokmi. 
 Williama Burta pritom v te?rii o vy?e sto rokov predbehol ist? anglick? d?entlmen, in?inier Henry Mill. Na lond?nskom patente jeho zlo?it?ho zariadenia z roku 1714 svieti embl?m kr?ovnej Anny a nasleduj?ca veta: "Umel? pr?stroj alebo met?da vtl??ania a prepisovania p?smen jednotlivo a  progres?vne jedno za druh?m ako pri p?san?, pri?om p?smo je mo?n? zv?razni? na papieri tak, ?e je na nepoznanie od tla?e." 
 Ke?e sa v?ak Angli?anovi Millovi nikdy nepodarilo svoj v?mysel pod?a n?kresov aj vyrobi?, titul "prv? p?sac? stroj" nav?dy ostal na ?t?te Ameri?ana Burta. 
 
 
 

 Stavite? mlynov William Burt si dal patentova?  p?sac? stroj s kruhov?m s?dza?om p?smen v roku 1829. Sedem rokov po ?spe?nom pridelen? patentu prototyp jeho pr?stroja, ktor? nazval typografom, vyhorel aj s cel?m patentov?m ?radom. Civiliz?cia neutrpela nijak? ve?k? ?kodu - prv? americk? p?sac? stroj bol neohraban?, nepraktick? a p?salo sa na ?om ove?a pomal?ie ako rukou. 
 Burt sa plame?mi nedal zmias? a kr?tko po po?iari vyvinul model ??slo dva. Tentoraz d?myselnej??, i ke? vo ve?kosti skrine. Nad?en? vyn?lezca v?ak ostal so svoj?m gigantick?m p?sac?m strojom s?m. 
 Komer?n? ?spech jeho men??ch modelov si vychutnali a? jeho kon?trukt?rski nasledovn?ci. 
 Burt sa ?oskoro na  p?sac? stroj vyka??al a vr?til sa k svojej ob??benej ?innosti - vym???aniu. U? v roku 1837 si dal ?spe?ne patentova? sol?rny kompas. 
 Na s?riov? v?robu ?akal Burtov typograf ?al??ch 30 rokov. ?tyridsiate a p?desiate roky 19. storo?ia boli svedkom fam?znych pokusov kon?trukt?rov o vylep?enia Burtovho vyn?lezu, z  ktor?ch niektor? sa sk?r podobali na organ v katedr?le ne? na neskor?ie prenosn? p?sacie kufr?ky. 
 Prv? pou?ite?n? p?sac? stroj vznikol a? okolo roku 1867 v ?t?te Wisconsin v k?lni troch priate?ov - dom?cich majstrov Christophera Lathama Sholesa, Carlosa Gliddena a Samuela Soula. Typ kl?vesnice, ktor? vymysleli po ve?eroch za domom, svet dodnes  pou??va ako s??as? najmodernej??ch po??ta?ov. 
 Trojica skon?truovala za p? rokov tridsa? pokusn?ch ma??n. Okam?ite sa stretla s nev??ou na ?radoch - ?radn?ci slab? v gramatike si uvedomili, ?e svoju neschopnos? u? neskryj? za v?hovorku ne?itate?n?ho rukopisu. 
 Ako to u geni?lnych majstrov b?va,  Sholesovi, Gliddenovi a Soulovi sa ich p?ro?n? drinu nikdy nepodarilo spe?a?i?. Ke? po ?al??ch ?iestich rokoch z?fal?ch obchodn?ch pokusov vst?pil do ich k?lne elegantn? James Densmore a pr?jemn?m hlasom im za bagate? pon?kol odk?penie autorsk?ch pr?v, nev?hali. Densmore e?te v tom istom roku 1873 podp?sal zmluvu s  tov?r?ou na zbrane a ?ijacie stroje Remington a synovia. 
 Star?mu Remingtonovi nenapadlo ni? lep?ie, ne? zada? v?robu p?sacieho stroja dizajn?rovi stroja ?ijacieho. Tak sa prv? p?sacie stroje objavili na trhu v roku 1874 vybaven? ?liapac?m ped?lom na pos?vanie papiera, s jemn?mi kvietkami vyryt?mi na lakovanom povrchu a dok?zali p?sa? len ve?k?mi  p?smenami. Prv? odv?livci neboj?cne ?liapli do ped?lu a nad Severnou Amerikou a vz?p?t? aj Eur?pou sa pomaly za?al rozlieha? pr?zna?n? ?ukot, ob?as preru?en? elegantn?m cinknut?m riadkova?a. Z?bavn? rozmanitos? ?udsk?ho rukopisu sa nav?dy utiahla do sf?r s?kromnej kore?pondencie. 

 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (1)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 21:30  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v utorok st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
KOMENT?R PETRA SCHUTZA  
Len korup?n? ?kand?ly preferencie Smeru nepotopia
 
 Ak chce opoz?cia vo vo?b?ch zvrhn?? Smer, bez roz??renia reperto?ru si neporad?. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 23:50  
Taliban zabil v ?kole v Pakistane 132 det?
 
 Pakistan a? teraz poriadne zatla?il na islamistov. Odplatou je ?tok na deti vojakov. 
 
 
 
 
 




 
 

 
ZAHRANI?IE  
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi
 
 Na demon?tr?cii proti Orb?novej vl?de bolo v utorok u? len nieko?ko tis?c ?ud?. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






