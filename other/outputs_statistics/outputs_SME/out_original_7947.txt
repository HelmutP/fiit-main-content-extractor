
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Kuspan
                                        &gt;
                Nezaradené
                     
                 Čo robiť keď prší 

        
            
                                    3.6.2010
            o
            14:57
                        |
            Karma článku:
                4.00
            |
            Prečítané 
            1160-krát
                    
         
     
         
             

                 
                    Keďže slnko bolo naposledy viditeľné asi na tri minúty niekedy mesiac dozadu a keďže podľa odborných predpovedí rôznych iľkológov to ani v najbližších dňoch nevyzerá na úpaly, dovolil som si navrhnúť niekoľko možností využitia voľného času.
                 

                 1.       Ako najjednoduchšia alternatíva sa nám núka sedenie doma a nadávanie na Fica. Aj keď je možné, že z tejto spočiatku kratochvíle sa môže stať dlhodobý koníček, v žiadnom prípade však neradím v tomto smere prehánať a na zachovanie zdravej mysle odporúčam aspoň sem-tam zapnúť televíziu a pozrieť si videá z povodní alebo z nejakých vojnových konfliktov.   2.       Melancholickejšie duše si môžu kúpiť fľašu vína a nechať sa unášať čarokrásnym zvukom klopkania dažďových kvapiek na plastové okná. Aj keď aj tento spôsob využitia voľnej chvíle môže po čase viesť k menšej či dokonca aj väčšej duševnej otupenosti, o alkoholizme ani nehovoriac, vždy sa hlavne v bytových domoch nájde ochotný sused, ktorý prípadnú dlhú až nudnú chvíľku spojenú s počúvaním oných kvapiek preruší – hlavne v čase blížiacom sa nočnému kľudu – príklepovou vŕtačkou. Teda bez obáv počúvajte a sledujte ako leje. Prípadne môžete k tomu z okna ešte pľuť, nech sa zem napije.    3.       Pre tých, ktorí sa neboja výziev prírody, mám pripravenú špeciálnu adrenalínovú zábavu spojenú s nezvyčajných rybolovom.  Ak ste odvážni a bez predsudkov, vyberte sa k nejakému vyliatemu potoku a pokúste sa na udici chytiť niektoré z motorových vozidiel, ktoré potok zachytil pri svojej radostnej  ceste do Čierneho mora. Ak ste viacerí, môžete aj súťažiť a úlovky si bodovať, napr. za osobné auto jeden bod, za Aviu dva body, za hasičské auto tri body, pričom za samotného hasiča dostanete prémiový bod a možnosť nahodiť udicu z výhodnejšieho miesta.     4.       K nepriazni počasia sa môžete postaviť aj chrbtom a urobiť si piknik tváriac sa pritom, že vôbec neprší. Veď v porovnaní s pravidelnými záplavami napr. v Bangladéši  o nič nejde a tak šup-šup do záhrady, zabaliť klobásku do nepremokavého igelitu, vylosovať nešťastníka, ktorý bude nad ohníkom stáť so slnečníkom a opekať a veseliť sa až do rána bieleho, teda vlastne sivého.    5.       Ako poslednú možnosť ponúkam sledovanie seriálových hitov ako sú Panelák, Ordinácia v ružovej záhrade, Poisťovňa šťastia a podobných kultov, ktoré vás iste odbremenia od bežných starostí občana tým, že sa v nich riešia samé prkotiny a tak nemusíte myslieť na to, že zajtra treba zasa vstať a tým menej šťastnejším sa ešte k tomu v tomto sk.....om lejaku j....ť do sk.....ej roboty.   Ak ste si náhodou nič nevybrali, tak potom si hoďte slučku, lebo pršať bude aj ďalšie dni a tak či tak by ste boli depresívni a depresie aj tak vedú k slučke, čím celý tento nezvratný proces urýchlite.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Kuspan 
                                        
                                            V rannej električke III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Kuspan 
                                        
                                            Grand Opening (S)Hit
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Kuspan 
                                        
                                            Hurá, idú Vianoce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Kuspan 
                                        
                                            V rannej električke II
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Kuspan 
                                        
                                            Albánske diaľnice alebo čo my k...a robíme zle
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Kuspan
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Kuspan
            
         
        kuspan.blog.sme.sk (rss)
         
                                     
     
        Homo homini lupus.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1875
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       V rannej električke III.
                     
                                                         
                       Grand Opening (S)Hit
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




