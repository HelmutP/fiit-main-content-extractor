

 
  
   
   
 Vážený pán Figeľ! 
 
   
 Som praktizujúci kresťan katolík rovnako ako Vy. Napriek tomu som KDH nevolil. V tom sme rozdielni. Volil som stranu, kde volebnou líderkou bola Prof. Radičová. A to nielen preto, že ako žena je sympatická a ako univerzitný  pedagóg múdra. Zvažoval som aj stranu charizmatického Bélu,  Sulíkovu marketingovú stranu nie. Takže toto je môj voličský profil.  
 Teraz k veci. Po ostatných voľbách sa Slovensko dostalo do zvláštnej situácie. Jedni hromžia a posielajú pravicu do pekiel a KDH do stredoveku, iní sa celkom spontánne tešia a ďalší zasa držia balóniky. Iba na Vašej tvári som na povolebnej obrazovke videl kyslý úsmev. Myslím, že ste tým ako skúsený bruselský komisár a politik dali vedieť čo si myslíte viac ako ste chceli. 
 Dovolím si citovať z môjho blogu, ktorý som napísal v povolebnú nedeľu: „KDH bola vždy strana zvláštna. Nie len tým, že vyštvala svojho etablovaného maskota (v dobrom) Fera Mikloška, ale najmä tým, že často vierou ospravedlňovala svoju vierolomnosť. Či už to boli rusofilné ambície Jána Čarnogurského, alebo rozbitie vlády v podstate kvôli výhrade svedomia. Vyhradili si, tiež možnosť spolupráce so SMER-om, teda aspoň pred štyrmi rokmi.“ 
   
 Áno, je to práve „vatikánska zmluva“, ktorá pravicovo orientovanú verejnosť v súčasnosti straší. Nie tak pre jej obsah, veď ten sa dá predsa modifikovať, ale pre to, že ste ju vytiahli práve teraz. To veru nebol dobrý ťah. To bola priam zaslepenosť, nie hodná bruselského vyjednávača. Sám ako kresťan viete, že „Duch oživuje a litera zabíja.“ Vaše tvrdošijné pretláčanie tejto agendy pripomína praktiky tých, ktorí sa pohoršovali nad klasom vymrveným v sobotu... 
   
 Nie, touto taktikou nevyhráte nič. Stratégia protihráča je totiž zákerná a pošle Vašu stranu do minulosti. Stratégiu protihráča totiž vidím takto: 
   
 - Fico Vás presvedčí, že poskytne polovicu rezortov, premiéra a vatikánsku zmluvu  - Prerušíte rokovania s pravicou a začnete rokovať s Ficom  - Fico vyhlási, že s KDH sa nedohodol - Pravica nechce KDH  - Vláda sa nevytvorí a budú predčasné voľby  - Znemožnené KDH neprejde do parlamentu  - Fico získa 55 percent ako vianočný darček  
   
 A „vatikánska zmluva“ i jej duch ostanú nadlho v zásuvke... 
   
 Takže tak, pán Figeľ. Toto som Vám chcel povedať. A dúfam že ste sa neurazili, keď som písal o vierolomnosti KDH. Pevne verím, že som sa mýlil... 
   
 S úctou 
   
 Tibor Javor    
     
   
   
 Žeby som sa mýlil? (doplnené 17.6.2010 o 15:00)                               http://www.sme.sk/c/5427401/figel-ubezpecuje-o-pravicovej-stvorkoalicii.html  
  
 
 Ilustračné foto: (C) Tibor Javor 

