

    
 Výroky, že mám talent , že som neuveriteľne rýchla, boli krásne a počas športových prenosov, keď 16ročné dievčatá predvádzali nevídané výkony vo svojich kategóriách, som až donedávna premýšľala, aké by to bolo, keby som namiesto tanečnej a výtvarnej chodila na basketbal, tenis, atletiku. Kde by som bola... 
   
 Už asi viem, že by som nebola nikde, a že šport nikdy nebol pre mňa. Výrok lekára, ktorí pokazil, čo mohol (ešte však predtým, ako pokazil, čo mohol) „Ako si chceš zničiť koleno inak ako pri športe“, bol svojho času úžasný a priniesol mi pocit, že naozaj môžem robiť, čo len chcem. Už mu však neverím, napriek tomu, že mi šport koleno zničil..Teda ja športom... Výrok kamaráta :Aj Országh mal osem operácií, až po nich skončil so športom, ty máš ešte štyri k dobru“, je nezabudnuteľný a zakaždým sa mi pri spomenutí si na slová ďalších „ešte budeš hopkať, uvidíš“ „ešte budeme spolu športovať“, tiež  tisnú slzy do očí. Veta „Ročne mi vojde do ordinácie asi päť ľudí s hypermobilitou kĺbov“, by bola príjemná, keby som bola celosvetovo známa hadia žena, nie po úraze, s ktorým je šport, hoci len rekreačný, pre príliš ohybných ľudí (laicky moja diagnóza), neskĺbiteľný. 
 Neviete, ako to bolí, neviete, aké to je v ordináciách, keď vám povedia, že im je to ľúto, málokto z vás vie, aké to je, dúfať, že piata operácia bude posledná. Málokto z vás ma videl plakať. Tak nevravte, že aj bicyklovanie je fajn, že aj v plavkách vyzerám dobre, a že basketbalové kraťasy vyzerajú cool aj pri prechádzaní sa po bulvári. Básnická otázka , ktorú si často kladiem: „Budem môcť pobehnúť aspoň na zastávku autobusu?“ je oprávnená? Len to nie...   Vetu všetko zlé je na niečo dobré sa neopovážte pri mne spomenúť. Zlo skrz to všetko totižto ostane zlom a vždy to bude bolieť. 
   

