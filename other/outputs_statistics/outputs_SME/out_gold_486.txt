

 
Nechcem vám radiť, či zatepľovať alebo nie, na túto tému tu bolo x nesprávnych aj správnych článkov. Ale ak bývate v paneláku, ktorý sa bude zatepľovať, vyplatí sa vám prečítať moje postrehy.
 
 
  
 
 
Aj takto môže vyzerať nová fasáda domu, keď sa na to poverená firma "vyserie". V tomto prípade ide o nízku budovu, kde sa to určite dalo spraviť omnoho lepšie. Iný prípad sú ale sedem a viac poschodové panelové domy, kde nerovnosti v rámci celej steny presahujú až do desiatok centimetrov. Vyrovnať takúto stenu je možné teoreticky, nie však prakticky.
 
 
Príklad:
 
 
Ak máte stenu, kde odskok od najmenej a najviac vypuklého miesta predstavuje 10 centimetrov, potom to pri zateplení izoláciou  znamená, že na najmenej vypuklé miesta budete musieť použiť 9 centimetrovú vrstvy lepiacej malty alebo ich budete musieť podlepiť (centimeter lepiacej malty, 5 cm hrubá polystyrénová doska, ďalšia 3 cm vrstva lepiacej malty a až potom vlastná izolácia). To však znamená jednak oveľa väčšiu spotrebu materiálu, jednak musíte zaplatiť viac izolatérskej firme, keďže bude izoláciu lepiť vlastne dvakrát dajmetomu na 40 % steny. Nehovoriac o tom že vyrovnať bočný štít paneláka bez okien  o plochách okolo 300 metrov štvorcových aj viac je v praxi na 99 % nemožné.
 
 
  
 
 
 
 
 
Ak však obyvatelia panelového domu zainvestujú niekoľko miliónov do obnovy fasády, je ich právom očakávať, že to bude aj dobre vyzerať. Takže aby sa vlk nažral a koza ostala celá...
 
 
Aby fasáda domu nemala výrazné nerovnosti ale zároveň aby sa stavba nepredražila, volí väčšina firiem a investorov nasledujúce riešenie:
 
 
Stena sa nevyrovná úplne, ale "stlmia" sa výraznejšie skoky medzi jednotlivými panelmi - to znamená že ostrý skok medzi panelmi sa roztiahne na dlhší oblúk. Maximálna veľkosť takého oblúka je väčšinou určená v zmluve medzi investorom a zhotoviteľom - napríklad maximálna nerovnosť 10 mm na 2 bežné metre.  Tým sa dosiahne to, že stena po nalepení izolácie nie je síce úplne rovná, ale namiesto desiatich výrazných "skokov" je na stene päť menších oblúkov. Podlepia sa len výrazne nerovné časti steny (napr. namiesto 40 iba 5 %) a použije sa oveľa menej lepiacej malty ako pri úplnom vyrovnaní steny.
 
 
  
 
 
Aj takto "miernejšie oblúková" stena však môže vyzerať škaredo - najmä v prípade ak je blízko rušná komunikácia. Autá víria prach a ten sa usádza na stene. Kým v prípade úpne rovnej steny by sa usadzoval rovnomerne po celej ploche, na zvlnenej stene sa usadí viac na hornej časti oblúkov, ktoré nie sú v úplnej vertikálnej polohe. Takto po čase vznikajú škvrny na fasáde - jednotlivé časti sú špinavšie ako iné. Čím krivšia fasáda a čím viac prachu tým väčšie "fleky".
 
 
Riešenie je úplne jednoduché - stačí stenu tzv. rozbiť. Rozbiť na viacero farebných úsekov, čiže použiť viac farieb. Namiesto tohto:
 
 
 
 
 
treba použiť viac farieb:
 
 
 
 
 
 
 
 
Vznikne očný klam - to znamená že pri letmom pohľade na stenu oči vnímajú prechody medzi farbami a až pri sústredenom skúmaní steny si všimnete že jednotlivé farby nemajú úplne rovnaký odtieň po celej ploche. Tak aj po niekoľkých rokoch vyzerá panelák pekne.  Na podobnom princípe fungujú aj steny s oknami - krivá stena s oknami vyzerá aj pri použití jednej farby oveľa krajšie ako rovnako krivá stena bez okien - oči si všímajú okná a nie nerovnosti na stene.
 
 
Použitie viacerých farieb tiež predražuje stavbu, ale iba v minimálnej miere niekoľkonásobne nižšej ako v prípade úplného vyrovnania steny. Samozrejme podmienkou je kvalitné nanesenie omietky - jednotlivý farebný úsek musí byť nanesený v jednom kuse a v časovej tiesni (pretože omietka po nanesení je spracovateľná len po určitú dobu). Treba rešpektovať tepelné limity (väčšinou od 5 do 25 stupňov celzia) a poveternostné podmienky ( vietor prirýchlo vysuší omietku).
 
 
Mnohé firmy pri jednofarebnej stene volia postup: Zoberiem zákazku, dám trošku vyššiu cenu, investor strhne 10 % za nepodarený výsledný efekt a všetci sú v suchu. Vy sa však na výsledok budete pozerať každý deň a tak stojí za to zaplatiť to percento naviac. Tento článok nie je moja teoretická "vízia", sú to skúsenosti z praxe - 2 a pol roka som zatepľoval v Prahe a pri nedávnej návšteve hlavného mesta ČR som si pozrel výsledky práce s odstupom času. 
 
 
Viac farieb nezaručí dobrý výsledok, ak zhotoviteľ nedodrží správne postupy napríklad pri lepení sklotextilnej mriežky, ale na správnosť postupov dohliadne vami určený stavebný dozor. Ale o rôznych "cestičkách" ako si firmy vedia zarobiť extra peniaze na zatepľovaní a čoho sa vyvarovať napr. pri zateplení rodinného domu napíšem v ďalšom blogu. 
 

