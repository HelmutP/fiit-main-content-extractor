
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Obmedzenie jazdy kamiónov počas víkendov - komentár návrhu zákona 

        
            
                                    14.1.2008
            o
            0:05
                        |
            Karma článku:
                10.24
            |
            Prečítané 
            20781-krát
                    
         
     
         
             

                 
                    Návrh zákona o premávke na pozemných komunikáciách mení zákaz jazdy nákladných vozidiel počas víkendov. V niečom zákon zákaz sprísňuje a v niečom zmierňuje.
                 

                 
  
   Avizovaným cieľom zákazu je znížiť počet vozidiel (najmä veľkých a pomalých) v období najvyššej hustoty premávky. Dopravné špičky nie sú len časom najvyššej hustoty dopravy, ale aj časom najčastejšieho výskytu dopravných nehôd, najmä hromadných.   Text návrhu zákona:   § 39 Obmedzenie jazdy niektorých druhov vozidiel      (1) Na diaľnici, na rýchlostnej ceste a na ceste I. triedy je zakázaná jazda motorovým vozidlám s najväčšou prípustnou celkovou hmotnosťou prevyšujúcou 7 500 kg a motorovým vozidlám s najväčšou prípustnou celkovou hmotnosťou prevyšujúcou 3 500 kg s prípojným vozidlom,   a) v piatok alebo v posledný pracovný deň pred dňom pracovného pokoja ) v čase od 15.00 hod. do 20.00 hod.,  b) v prvý deň pracovného pokoja a v sobotu, ak nasleduje po pracovnom dni, v čase od 07.00 hod. do 11.00 hod. a v období od 1.júla do 31. augusta v čase od 07.00 hod. do 19.00 hod.,  c) v posledný deň pracovného pokoja v čase od 15.00 hod. do 20.00 hod.    (2) Ak ide o jeden deň pracovného pokoja, ktorý nasleduje po pracovnom dni, zákaz jazdy podľa odseku 1 platí v čase od 07.00 hod. do 11.00 hod. a v čase od 15.00 hod. do 20.00 hod.    (3) Zákaz jazdy podľa odseku 1 a 2 neplatí pre  a) autobusy, obytné automobily, vozidlá ozbrojených síl, ozbrojených bezpečnostných zborov, Hasičského a záchranného zboru, ostatných hasičských jednotiek20) a Slovenskej informačnej služby,  b) vozidlá použité na prepravu zdravotníckeho materiálu, liečiv a biologického materiálu do zdravotníckeho zariadenia alebo na zabezpečenie prevádzky zdravotníckych prístrojov v zdravotníckych zariadeniach,   c) vozidlá použité v kombinovanej doprave, na nakládku a vykládku lodí alebo železničných vagónov,  d) vozidlá použité na zabezpečovanie športových a kultúrnych podujatí,   e) vozidlá použité na prepravu humanitárnej pomoci,  f) vozidlá použité na odstraňovanie havárií a ich následkov, ako aj pri živelných pohromách,   g) vozidlá použité pri výkone činnosti spojenej s údržbou, opravami a výstavbou ciest,  h) vozidlá použité na prepravu potravín alebo na prepravu živých zvierat.    (4) Neplatnosť zákazu podľa odseku 3 sa vzťahuje aj na prepravu, ktorá s použitím vozidiel uvedených v písmenách b) až h) súvisí. Vodič je povinný na výzvu policajta hodnoverným spôsobom preukázať použitie vozidla na účely uvedené v odseku 3.    (5) Používanie zvláštnych motorových vozidiel na cestách je s výnimkou ciest III. triedy, miestnych komunikácií a účelových komunikácií zakázané; to neplatí   a) pre zvláštne motorové vozidlá ozbrojených síl, ozbrojených bezpečnostných zborov a Slovenskej informačnej služby a zvláštne motorové vozidlá použité na odstraňovanie havárií a ich následkov, pri živelných pohromách, pri výkone činnosti spojenej s údržbou, opravami a výstavbou ciest,  b) ak je používanie zvláštnych motorových vozidiel dovolené na základe povolenia na zvláštne užívanie ciest.1)     (6) Na ceste I., II. a III. triedy je zakázaná jazda záprahovým vozidlám a ručným vozíkom s celkovou šírkou väčšou ako 600 mm  a) vždy v čase od 23.00 hod. do 04.00 hod., od 06.00 hod. do 09.30 hod. a od 14.30 hod. do 18.30 hod.  b) v čase zákazu jazdy niektorých druhov vozidiel podľa odseku 1 a 2, ak tento čas nie je rovnaký s časmi podľa písmena a).      Keď som o tejto problematike písal dávnejšie, po tlačovej konferencii MV, kde bol zámer prezentovaný tak sa ozvalo viacero vodičov kamiónov, že je to zlý zámer. Teraz je zákon v medzirezortnom pripomienkovaní a časy sú mierne pozmenené. Z dôvodu prehľadu som spravil tabuľku kde porovnávam terajší návrh s terajším platným stavom.     Orientačné porovnanie zákazu jazdy niektorých  vozidiel počas dopravných špičiek na D,R,1,2,3tr.     vozidlo / deň 
 posledný pracovný deň prvý voľný deň prvý voľný deň počas prázdnin posledný voľný deň ak voľný deň je prvým aj posledným    Návrh zákona   vozidlá nad 3,5 tony bez obmedzenia bez obmedzenia bez obmedzenia bez obmedzenia bez obmedzenia   vozidlá nad 3,5 tony s prívesom, pre DR1tr. 15:00-20:00 07:00-11:00 07:00-19:00 15:00-20:00 07:00-09:00 15:00-20:00   vozidlá nad 7,5 tony, pre DR1tr. 15:00-20:00 07:00-11:00 07:00-19:00 15:00-20:00 07:00-09:00 15:00-20:00   zvláštne vozidlá pre DR12tr. len na individuálne povolenie len na individuálne povolenie len na individuálne povolenie len na individuálne povolenie len na individuálne povolenie   záprahové vozidlá 123tr. 23.00-04.00 06.00-09.30 14.30-18.30 23.00-04.00 06.00-09.30 14.30-18.30 23.00-04.00 06.00-09.30 14.30-18.30 23.00-04.00 06.00-09.30 14.30-18.30 23.00-04.00 06.00-09.30 14.30-18.30    Terajší platný stav   vozidlá nad 3,5 tony bez obmedzenia bez obmedzenia bez obmedzenia bez obmedzenia bez obmedzenia   vozidlá nad 3,5 tony s prívesom bez obmedzenia 00:00-22:00 07:00-20:00 00:00-22:00 00:00-22:00   vozidlá nad 7,5 tony bez obmedzenia 00:00-22:00 07:00-20:00 00:00-22:00 00:00-22:00   zvláštne vozidlá 15:00-21:00 bez obmedzenia bez obmedzenia 15:00-21:00 15:00-21:00   záprahové vozidlá 15:00-21:00 bez obmedzenia bez obmedzenia 15:00-21:00 15:00-21:00    Rozdiel medzi návrhom a doterajším stavom   vozidlá nad 3,5 tony bez zmeny bez zmeny bez zmeny bez zmeny bez zmeny   vozidlá nad 3,5 tony s prívesom zavedenie obmedzenia zníženie obmedzenia zníženie obmedzenia zníženie obmedzenia zníženie obmedzenia   vozidlá nad 7,5 tony zavedenie obmedzenia zníženie obmedzenia zníženie obmedzenia zníženie obmedzenia zníženie obmedzenia   zvláštne vozidlá zvýšenie obmedzenia zvýšenie obmedzenia zvýšenie obmedzenia zvýšenie obmedzenia zvýšenie obmedzenia   záprahové vozidlá. zmena obmedzenia zavedenie obmedzenia zavedenie obmedzenia zmena obmedzenia zmena obmedzenia    Odhadujem, že vodiči osobných aut sa potešia ale reakciu vodičov nákladných aut si netrúfam odhadnúť i keď v niektorých prípadoch došlo k zmierneniu prísnosti zákazu.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




