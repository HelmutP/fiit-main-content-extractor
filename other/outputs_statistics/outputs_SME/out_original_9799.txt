
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Madárová
                                        &gt;
                Z cestovateľského denníčka
                     
                 Na francúzskej strane Rýna 

        
            
                                    29.6.2010
            o
            10:10
                        (upravené
                5.12.2010
                o
                16:04)
                        |
            Karma článku:
                7.00
            |
            Prečítané 
            1445-krát
                    
         
     
         
             

                 
                    Bolo to vlani na jar, keď sme so sesternicou sedeli pri jednom chladenom v letnej záhrade a ona sa práve dozvedela, že dostala štipendium na ročný pobyt v Štrasburgu. Tak som sa hneď hlásila ako návšteva. A po vyše roku sa mi to konečne podarilo.
                 

                    Cez pracovný deň som odkázaná sama na seba. A tak korzujem uličkami Petite France - Malého Francúzska, s typickými alsaskými hrazdenými domčekmi.         A potom pokračujem len tak, za nosom. Ulicami, námestiami, parkami...                  Vo výklade numizmatickej predajne natrafím na slovenské euromince. Kompletná sada za desať eur. Alebo v obale za tridsať. Normálne mám chuť prehrabať peňaženku a ísť sa spýtať, či by prípadne nejaké slovenské mince nekúpili. Nachádzam francúzske, talianske, nemecké... A len jedno jediné slovenské euro... Tak nič, asi na našej mene nezarobím. A mimochodom, súbor vatikánskych mincí majú za rovných sto.         Pomaly sa blížim ku katedrále Notre-Dame. Stredoveká, gotická, s jednou vežou čnejúcou sa k oblohe. Druhú akosi nestihli dostavať. Nie je to jediná katedrála s takýmto osudom. Veľkolepé stavebné plány občas narazia na finančný strop. To sa nezmenilo dodnes.            Točitým schodiskom v nedostavanej veži stúpam na vyhliadkovú plošinu. Pozorujem ruch na námestiach podo mnou a v diaľke vidieť budovu Europarlamentu.         K nemu sa ako uvedomelý euroobčan vyberám nasledujúci deň. Je tu ticho a prázdno. Poslanci sem chodia rokovať iba na pár dní do mesiaca. Inak tu nájdete len zopár turistických skupín.         Zaujala ma britská vlajka visiaca z okna, tak trochu narúšajúca chladno dokonalý imidž budovy. Po celom Štrasburgu z okien vlajú zástavy rozličných krajín, veď sa predsa hrá futbal a v kozmopolitnom meste fandí každý tým svojim, ale keďže Británia ako celok futbal na medzinárodnej úrovni nehráva, pravdepodobne pôjde o aktivitu nejakého hrdého europoslanca.      V sobotu mierime na výlet do alsaského mestečka Colmar. Pri vjazde do mesta nás víta socha slobody. Jej autor je totižto miestny rodák.      Na fasáde Maison des Tetes - Domu hláv sa škerí množstvo tvárí.            Prichádzame ku katedrále, ktorá vraj z cirkevného hľadiska katedrálou nie je. Ale aj tak je pekná. A na streche sídlia bociany.            Pokračujeme uličkami k Petite Venice - Malým Benátkam. Všade sú hrazdené domčeky pastelových farieb a pokiaľ máte záujem, môžete sa turistickou loďkou povoziť po kanáli. Ale nie je to gondola a ani „O sole mio" vám pravdepodobne nikto nezaspieva.               Smerujeme ďalej na juh, do mesta Mulhouse. Po pikniku v tráve si ideme pozrieť múzeum elektriny a vecí s ňou súvisiacich. Prezentácia statickej elektriny je zaujímavá, akurát že po francúzsky. Pokusy sa robia na deťoch, človek si v duchu opakuje zásady prvej pomoci, dobrovoľníkom vstávajú vlasy dupkom a ja si spomínam na scénu z filmu Pelíšky a na hračku na zoceľovanie sovietskych pionierov.      S niektorými exponátmi sa dá hrať a tak si môžete v praxi vyskúšať, ako jazdí vláčik, keď sa zmení polarita batérie či napätie, aký spotrebič by ste dokázali spustiť bicyklovaním a čo sa nachádza za elektrickou zásuvkou.   Zaujímavá je výstava starých spotrebičov - historická fritéza, jogurtovač, hriankovač, vysávač, čiernobiely televízor bez plochej obrazovky, nenaparovacia žehlička, telefón bez integrovaného fotoaparátu, prehrávač veľkých čiernych diskov zvaných dlhohrajúca platňa...   Alebo kalkulačka. Nie som si celkom istá, či zvládala aj násobilku, alebo len sčítanie a odčítanie.      A samozrejme nechýbajú počítače. Fakt by som ich chcela vidieť pracovať.            Po návšteve múzea ešte na chvíľku skočíme do centra mesta.         Posledný deň výletu trávime v jednom z miestnych parkov. Je tu jazierko, dá sa tu člnkovať alebo len tak vyvaliť do trávy. S obľubou praktizovaná činnosť v nedeľné popoludnie pri teplote blížiacej sa k tridsiatke.      V parku majú svoj domov bociany. Poletujú ľuďom ponad hlavy, viac ako spev vtákov počuť klapot zobákov, a občas niektorý zapózuje na trávniku. Celkom by ma zaujímalo, či má počet bocianov v tejto oblasti nejaký vplyv na pôrodnosť. Alebo sú to len také reči?              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Madárová 
                                        
                                            Jeseň na Znojemsku
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Madárová 
                                        
                                            Plavba po jazere Como
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Madárová 
                                        
                                            Janov – Kolumbovo mesto
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Madárová 
                                        
                                            Cinque Terre nielen po vlastných
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Madárová 
                                        
                                            Dva dni v Českom Švajčiarsku
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Madárová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Madárová
            
         
        madarova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Narodená v auguste, milujúca leto, knihy a cestovanie.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    90
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1671
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z cestovateľského denníčka
                        
                     
                                     
                        
                            Rodinná anamnéza
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




