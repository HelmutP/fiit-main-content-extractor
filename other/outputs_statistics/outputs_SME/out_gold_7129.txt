

 Stromčekom sa darilo. Cez leto som ich nechával na daždi na balkóne.  Až kým nám nezačali maľovať zvonka dom. Postavili lešenie okolo nášho balkóna a po dvoch dňoch nebolo po nich ani chýru. Odvtedy som sa tomu prestal venovať. Žena sa o izbové kvety stará vzorne. Keď ma pred rokmi požiadala, či by som jej neostrihal benjamína, skúsil som si spomenúť na moje bonsai - ové pokusy. Benjamín strihanie prežil a teraz po rokoch ho strihávam každý rok. Z odstrihnutých vetvičiek, ktoré sme dali na pár týždňov do pohára s vodou už rastú nové rastliny. Pred posledným strihaním som si pohľadal na internete stránky, ako sa to má správne robiť. Dočítal som sa tam, že treba nechať vždy na vetvičke aspoň dve očká, aby vetvička nevyschla. Rany, z ktorých vyteká mlieko treba ošetriť kúskom dreveného uhlia. Na internetovej stránke som sa prvýkrát dozvedel, že benjamíny znesú aj úplné zbavenie sa listov. Treba to ale urobiť vtedy, keď je dostatok svetla. To znamená od apríla do júna. Listy som opatrne poodtŕhal tak, aby som čo najmenej poškodil kôru vetvičiek. Maličké rašiace lístočky, ktoré sú pod každým listom som neodtrhol. 
  
 Benjamín po mojom zásahu vyzeral ako keby ho obhrýzli kozy. 
  
 Napriek tomu sa po štyroch týždňoch objavili na ňom prvé lístočky, ktoré by mali byť podľa návodu o niečo menšie ako tie predošlé a stromček by sa mal oveľa viac podobať na bonsai.  Viem, že ozajstní tvorcovia bonsai - ov sa pousmejú nad mojim pokusom, ale mne ten stromček svojim výzorom pripomína skutočný strom. 

