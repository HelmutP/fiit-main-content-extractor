
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Fulajtárová
                                        &gt;
                ... čo sa ma bytostne dotýka
                     
                 Blog.sme.sk 

        
            
                                    2.4.2010
            o
            11:08
                        (upravené
                6.4.2010
                o
                14:02)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            627-krát
                    
         
     
         
             

                 
                    Som v šoku. Vraj, táto stránka končí. Prečítala som si tých pár príspevkov o tom, že prečo je tomu asi tak a kto je za tým, ale, priznám sa, vôbec tomu nerozumiem. Prosím, ak si nájde niekto čas, aby mi to v diskusii vysvetlil.
                 

                 
Nevypovedané nahlas-foto-spomienkanie ja, ale ďakujem!
           Blogerkou som sa stala nie preto, že som sa chcela zviditeľniť, ale najmä preto, že tento blog mi dával možnosť vyjadriť si svoje myšlienky, city a názory a hneď, ako sa v mojej hlave zrodil príspevok, o pár hodín bol na blogu.Takmer žiadna recenzia, nečaká sa na tlač a tiež aj, že tzv. archív príspevkov a jednotlivých blogerov je v okamžiku k dispozícii. To myslím oproti iným médiam narábajúcich s písaným slovom.  Tých blogov rôznych existuje na webe mnoho. Do niektorých prispievam aj ja, ale... Blog sme.sk má oproti iným predsa len jedno veľké plus, resp. možno pre iných mínus. A síce. Musí sa písať pod skutočným menom autora, žiadna anonymita. Vyhovuje to? Ako kedy. Určite je ľahko písať tak, že človek sa schová za akýsi nik, i keď u mňa vraj aj to prezrádza. Som dosť priehľadná, možno originálna a tí, ktorí poznajú spôsob môj spôsob tvorby slova, tvrdia, že ja sa pod žiaden nik neschovám.   Takže, vlastná identita. Som za! Aspoň v mojom prípade. Takto som sa odvážila vysloviť nekonečný rad myšlienok, ktorý zverejňuje mnohé intímnosti z môjho života a tí, ktorí ma dobre poznajú, tvrdia, že neraz ide až o duševný spriptíz.   Je zaujímavé, že nikto z radov mojich najbližších, t.j. moje deti, vnúčatá, sa proti tomu nepostavil. Pravdaže, ide o to, že moje príspevky čítajú iba vtedy, keď im ich akosi „nanútim", t.j. prepošlem. Hlavne tie, kde sa prezentuje naša Vikinka. Ďalší príbuzní možno ani netušia, že mám akýsi blog a prispievam do neho. Sú viac orientovaní na rôzne pokecy, facebooky, atď., stránky, na ktoré zase ja nechodím, takže sa nemôžeme stretnúť.   Preto s kľudom Angličana a povahou Slováka môžem písať čokoľvek, bez toho, že by to malo k mojej osobe od blízkych príbuzných nejaké odozvy.   To už nemôžem povedať od mojich „kolegov" blogerov, ale preto ten blog.sme.sk existuje a prihlásila som sa do tejto veľkej rodiny práve preto, aby som mohla byť účastníkom diskusie z oboch strán. Ako zvládam jazyk, tvorbu slova, štylistiku, to nechám na iných. Každý sme čímsi výnimočným, máme vlastný štýl, mal ho Janko Kráľ i Pavol Dobšinský a obaja boli vynikajúci. V tom to byť nemusí. Ide o to, čo je obsahom jednotlivých príspevkov.   Nielen u mňa, ale takmer u všetkých, prevláda akési smerovanie k čomusi, čo je človeku najbližšie. Možno profesionálne, možno ako iba koníček. V mojom prípade je to pol na pol ŠŤASTIE SI TY a všetko, čoho sa dotýka. Prirodzene sú to KNIHY a ja, lebo to je ďalšia časť môjho života. A potom samozrejme môj manžel, moja rodina, NAŠA VIKINKA a život okolo mňa,  ... čo sa ma bytostne dotýka. Nateraz sú to pripravované zmeny v rámci blog.sme.sk.   Pýtam sa, čo sa vlastne deje? Prosím, berte to ako fakt, nejdem sa ospravedlňovať za nevedomosť, ale naozaj končíme? Kto to rozhodol a prečo? V nejakom príspevku som sa dnes dočítala, že ktosi za kliknutia dostával provízie. Je to skutočnosť, že VIP blogeri majú za každé kliknutie na karmu platené? Nie je to náhodou myslené ironicky? Vekovo patrím k tej skupine blogerov, ktorých možno viac zaujíma obsah jednotlivých príspevkov, ako práca administrátora, takže sorry! Neovládam všetko to dianie, čo sa deje „v zákulisí" a možno aj preto, že nečítam to, čím by mal prejsť každý bloger, t.j. tie rôzne zásady, podmienky, atď...   U mňa je to vždy z opačnej strany. Keď pochybím, upozornili ma a nedostatky som odstránila až na výzvu. Že to bola z mojej strany chyba, nejak mi to „žily netrhalo".   Nikdy som sa však nespreverenila etickým, morálnym i „prikázaným" zásadam vo vlastných príspevkoch. Vždy som rešpektovala názor iných, ktorí sa k nim vyjadrovali, reagovala som naň a myslím si, že nakoniec vždy došlo k akémusi tichému súhlasu oboch strán. Nikdy som sa nesnažila jeho názor vyvrátiť, iba som ho doplnila o podstatné, v príspevku nevypovedané. Dvakrát som rozhodla o tom, že K tomuto článku nepovoľujem diskusiu. Prečo? V jednom prípade preto, lebo som nechcela byť terčom zla iba za to, že mám iný názor ako tí druhí. A že vieme byť v diskusiach dosť ostrí, sarkastickí až zlí, to som sa presvedčila hlavne pri mojom príspevku Farárova milenka. Nič to. Stalo sa. V tom druhom prípade, som chcela chrániť totožnosť osoby, ktorá mi je aj dnes moc blízka.   Blog.sme.sk bol pre mňa úžasnou možnosťou realizovať sa vo vyjadrovaní mojich názorov na svet, ľudí okolo mňa i k tomu, čo sa priebežne rodí vo mne. Myslela som si, že to pretrvá ďalej, že... blog.sme.sk existoval dávno predtým, ako som sa zapojila do tohto diania a to, že končíme, akosi nedokážem stroviť. Bol pre mňa pojítkom medzi ostatným svetom, ľudí v ňom, ktorí si na moje príspevky zvykli a hodnotia ich ako úsmevné, zaujímavé a ľudsky bohaté. Dokonca počas môjho pobytu v nemocnici ďaleko od môjho domova ma nielen po mene, ale aj podľa fotografie a môjho slovníka spoznal istý pán doktor, ktorý bol členom lekárskeho tímu, ktorý ma liečil. A že šlo o dosť náročný a zložitý operačný či liečebný výkon, bola som tam trochu dlhšie ako zvyčajne. Spolupacienti v mojom okolí, nechápali, ako obyčajná žena kdesi zo stredného Slovenska upútala pozornosť o mnoho rokov mladšieho lekára tak, že sa k nej správal úplne inak, ako k ostatným. V jeho správaní ku mne bol istý obdiv, úcta a hlavne sme mali ľudsky tak blízko, že niekoľko krát sme si na lekárskej izbe čítali navzájom svoje príspevky, diskusie k nim a aj príspevky iných. Do podrobna sa dozvedel, prečo si myslím, že každý z nás je pre niekoho šťastím a ja som sa od neho dozvedela, že medicína a pacienti to nie sú len biele plášte, ale aj paragrafy, zlosť či dokonca osočovanie a závisť. Aj o tomto je pre mňa blog.sme.sk.   Nielen pán doktor z istej nemenovanej kliniky, ale aj niekoľko mediálne zaujímavých ľudí ma okamžite spoznalo hneď pri predstavovaní, ľudia z rôznych inštitúcií, na polícii, u nás na ÚP či daňovom úrade. Hovorím vždy o ľuďoch, i keď ide o jednotlivcoch, ktorí majú svoje meno. V každom prípade som ich vnímala ako zástupcu danej inštitúcie, ich menu som nevenovala pozornosť. Až dovtedy, kým ma neoslovili menom s tým, že: vitajte u nás, moja kolegyňa z blogu, som rád, že Vás osobne poznávam. Ono to bolo vždy neskôr obojstranné. Len dodnes nedokážem pochopiť, že ako ma títo ľudia súkromne v sebe rozpitvali a neskôr sme sa bavili aj o tých najitímnejších nuansách môjho života z uverejnených príspevkov.   Takže doslova: som duševnou striptérkou! Mne to však neprekáža. Ak iným nevadí moja duševná nahota, ak sú ochotní ju zdieľať, necítim to ani ako hanbu, ale ani ako exibicionizmus. Som to skrátka ja, vo vlastnej nahote, taká, aká v skutočnosti som.   Takže, ako to bude s nami, blogermi, so stránkou blog.sme.sk ďalej? Naozaj končíme? Bude sa ešte dať vrátiť k daným príspevkom a ako? Nielen k tým mojím, ale hlavne k iným? Alebo tie, ktoré majú pre mňa nejak osobitný význam, mám si špeciálne uložiť? Je možné, že by mi ktosi urobil archiváciu celej stránky blog.sme.sk tak, aby som sa k nej mohla vrátiť vždy, keď to budem potrebovať, aj po rokoch? Alebo to technicky nie je vôbec možné? Prosím, pomôžte mi zvládnuť tento technický problém, lebo toto neovládam. Akúkoľvek pomoc zo strany Vás, moji kolegovia, blogeri, uvítam aj prostredníctvom pošty. Ak budete mať záujem a chuť, už teraz  ď a k u j e m.   A čo bude ďalej, keď sa stránka blog.sme.sk stratí a prestane byť aktuálna? Bude možno nanovo sa registrovať, písať a čo bude dôležité pre zriaďovateľa, administrátora a pre samotných blogerov? Prosím, vyjadrite sa. Sme predsa len na jednej lodi. Nemám pravdu?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            ... a osoba invalidná :  - časť II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Osoba zdravotne ťažko postihnutá a osoba invalidná :  - časť I.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Práca: právo alebo povinnosť? - časť II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            P r á c a :  právo alebo povinnosť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Pracovať aj napriek ochoreniu  (psychické)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Fulajtárová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Fulajtárová
            
         
        fulajtarova.blog.sme.sk (rss)
         
                                     
     
        Vraj som filantrop, sociálne orientovaná a nadovšetko si cením možnosť slobodne sa rozhodnúť ...

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    99
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1129
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ... čo sa ma bytostne dotýka
                        
                     
                                     
                        
                            Kniha a ja
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            2- (dva mínus)
                        
                     
                                     
                        
                            Rozhodnuté
                        
                     
                                     
                        
                            ŠŤASTIE SI TY
                        
                     
                                     
                        
                            Pacientska advokácia/dôverníct
                        
                     
                                     
                        
                            Obyčajné, ale s pomlčkou
                        
                     
                                     
                        
                            môžem? Môžem, som naša Vikinka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Koruny alebo eurá?
                                     
                                                                             
                                            Žalujem reklamu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dominik Dán:  V š e t k o
                                     
                                                                             
                                            Štefan Klein: Vzorec šťastia
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Slovenské fórum
                                     
                                                                             
                                            HAV Internetové noviny
                                     
                                                                             
                                            Psia duša
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ester
                     
                                                         
                       Aj o tomto môže byť Valentín
                     
                                                         
                       Quo vadis, Cirkev?
                     
                                                         
                       Chlad láske nesvedčí
                     
                                                         
                       Jemný dotyk so smrťou
                     
                                                         
                       Ľúbime ťa, prepáč ...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




