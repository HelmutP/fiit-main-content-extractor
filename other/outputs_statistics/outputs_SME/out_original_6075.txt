
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Lobodáš
                                        &gt;
                Hokej
                     
                 MS začínajú... 

        
            
                                    7.5.2010
            o
            17:40
                        (upravené
                7.5.2010
                o
                19:50)
                        |
            Karma článku:
                2.83
            |
            Prečítané 
            789-krát
                    
         
     
         
             

                 
                    Do začiatku hokejových majstrovstiev sveta v Nemecku, ktoré otvoria zápasom domáceho výberu s USA na 76-tisícovom štadióne futbalového klubu FC Schalke 04 v Gelsenkirchene, zostáva už len pár hodín a nádeje slovenských fanúšikov sa upierajú k mužstvu okolo Glena Hanlona.
                 

                 
Richard Pánik, mladá nádej našej reprezentácie 
   Nový reprezentačný tréner mal situáciu od začiatku sťaženú. Jeho nástup na uvoľnený trénersky post po úspešnom Jánovi Filcovi bol sprevádzaný rôznymi protichodnými reakciami. Jedni brali zahraničného trénera ako potupu a hanbu trénerskému remeslu u nás doma, iní zase prijali jeho príchod s nádejou, že zmení upadajúci systém nášho hokeja. Možno by bolo naozaj dobre nájsť odborníka z domácich zdrojov (i keď naše najlepšie mužstvá trénujú českí tréneri), ale na druhej strane, náš hokej sa musí vydať inou cestou a angažovanie Glena Hanlona bolo prvým krokom.   O tom, či bude úspešný, napovedia práve nadchádzajúce majstrovstvá sveta. Hanlon mal extrémne málo času na spoznanie nielen našich hokejistov, ale i slovenskej mentality. Snahu mu nemožno uprieť, ale aj tak bol výber hráčov v počiatočnom štádiu viac-menej v rukách jeho asistentov či generálneho manažéra reprezentácie Petra Bondru. Výber to však nebol jednoduchý, pretože množstvo hráčov, ktorí sa z rôznych dôvodov ospravedlnili, by stačilo na poskladanie ďalšieho mužstva. Tu sa objavil problém, ktorý momentálne vládne v našom hokeji. Iste je treba rešpektovať rozhodnutie samotných hráčov. Chcú stráviť čas s rodinou, doliečiť si zranenia. V niektorých prípadoch oprávnene. Richard Zedník po manželských problémoch, často preberaných v bulvári, určite túži vidieť svoju dcéru, s ktorou sa naposledy stretol počas Olympijských hier vo Vancouvri; Jozef Stümpel je vo veku, kedy si jeho telo po náročnej sezóne potrebuje oddýchnuť a naši najúspešnejší hokejisti ešte stále hrajú play-off v zámorskej NHL. Tým sa však otvorila šanca pre druhých, pre hráčov, ktorým pri našich esách zostalo miesto vždy v tretej či štvrtej formácii. V Európe pôsobí množstvo slovenských hokejistov a väčšina z nich patrí k oporám či dokonca hviezdam svojich klubov. To isté mohli teraz predvádzať aj v drese s dvojkrížom na prsiach. Nevedno, či sa práve toho zľakli, či nedôverujú novému koučovi, alebo sa báli, že by si urobili prípadným zlým umiestnením zlé meno. Dôvodov môže byť viac, ale vyhlásenia typu „necítim sa psychicky pripravený mužstvu pomôcť", „potrebujem doliečiť staré zranenie", či „nemám zmluvu na novú sezónu a bojím sa zranenia" sme počuli tento rok akosi často. Každý má právo na únavu po náročnej sezóne, ale je rozdiel medzi hokejistom z NHL, ktorý odohral 90 zápasov v maximálnom tempe a hokejistom pôsobiacim v Európe s 50-60 zápasmi. Generálny manažér reprezentácie Peter Bondra nikoho verejne nepranieroval, ale vyhlásenie, že potrebuje len hokejistov, ktorí naozaj chcú reprezentovať svoju vlasť bolo dostatočné. Obyčajný človek, fanúšik, novinár nevie, čo je ozajstná pravda a prečo hokejista odmieta reprezentovať, ale ak môže hrať do konca sezóny za peniaze vo svojom klube, za reprezentáciu by mal s hrdosťou nastúpiť. Lebo dres národného mužstva je pocta. Svoje by o tom mohol rozprávať napríklad Róbert Petrovický, ktorý v roku 1996 doslova utiekol z Kanady, aby mohol reprezentovať, či Pavol Demitra, ktorý hral na majstrovstvách sveta v roku 2003 so zlomeným prstom a špeciálnou dlahou na ruke aj napriek protestom svojho klubu a bol to práve on, kto svojim gólom spečatil zisk bronzovej medaily. A na záver jeden príklad za všetky. Róbert Švehla v posledných sekundách zápasu o bronz na spomínaných MS, kedy by už ani inkasovaný gól nič nezmenil na našom konečnom bronzovom umiestnení, v poslednom zápase svojej kariéry sa hádže do strely českého hráča a inkasuje svoju poslednú hokejovú ranu, symbolický v drese svojej rodnej krajiny. Príkladov by sa dalo nájsť určite viac. Ale dnes, kedy by sa hokejisti, ktorí sa doslova tlačili do reprezentácie, keď vedeli, že príde Hossa, Demitra, Chára či Pálffy, mali obetovať pre reprezentačný dres, dnes, kedy jedna „zlatá" generácia končí a potrebujeme kvalitných hráčov do mužstva ako soľ, dnes zdupkali. Naša reprezentácia by na týchto majstrovstvách bola určite silnejšia s celou päťkou so Spartaka Moskva, s Marcelom Hossom, Jurajom Kolníkom či ďalšími... Teraz má však toto mužstvo jedno veľké plus. Hrdosť, že práve týchto dvadsaťtri hokejistov dostalo šancu zahrať si na majstrovstvách sveta.   Určite to nebudú mať ľahké. Súperi v skupine sú enormne silní. Rusi, nabudení neúspechom z Vancouveru, na čele s hviezdami ako Ovečkin, Kovaľčuk, Sjomin či nám dobre známy Sušinskij, budú pri správnej tímovej chémii nezastaviteľným súperom. Bielorusi, podobne ako Kazachstan ťažia z toho, že polovica ich mužstva hráva spolu i na klubovej úrovni. Pre bieloruskú reprezentáciu bolo dosiaľ najväčším úspechom šieste miesto, ktoré dosiahli práve pod trénerom Hanlonom, a túto pozíciu si budú chcieť zopakovať a treba povedať, že na to majú aj dostatočnú kvalitu. Kazachstan môže byť veľmi nevyspytateľným súperom. V mužstve je ešte pár hráčov, ktorí si pamätajú, pre nás smutne známy zápas z OH v Nagane. Jeremejev, či Koreškov sú doplnení ambicióznymi mladíkmi, čo predstavuje veľmi nebezpečný mix. Našim prvoradým cieľom by mal byť postup zo skupiny. Pri súčasnom stave hokeja na Slovensku bude čokoľvek navyše považované za úspech, štvrťfinále dokonca fantastický. Partia okolo kapitána Lintnera má predpoklady prekvapiť. Tréner Hanlon vniesol do hry nášho mužstva kanadské prvky a bude sa snažiť využiť to, čo je tomuto mladému mužstvu dané - rýchlosť, dynamika, bojovnosť. Slovenský fanúšik musí pochopiť, že tzv. „škaredý" gól má rovnakú cenu ako kombinácia do prázdnej bránky v podaní Hossu s Demitrom, či Pálffyho so Stümpelom. Pre náš mladý tím je určite výhodné, že nemôže vypadnúť. Odbúra to určité obavy, ktoré vzhľadom k našim posledným umiestneniam panovali. Preto, veľa šťastia chlapci. Nechajte na ľade srdce a budeme spokojní.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Nominačné okienko č. 1: Brankári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Lev zareval na odchod
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Najlepšie vývozne artikle extraligy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Tímy NHL podľa draftu: Anaheim Ducks
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Vlna, ktorú sme nezachytili
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Lobodáš
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Lobodáš
            
         
        lobodas.blog.sme.sk (rss)
         
                                     
     
        Športový fanatik, ktorého zaujíma všetko, čo sa týka futbalu a hokeja, ale miluje aj kvalitnú hudbu a filmy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2618
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nominačné okienko č. 1: Brankári
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




