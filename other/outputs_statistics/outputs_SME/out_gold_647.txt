

 
Chcem sa poďakovať za podporu môjmu debutovému románu. Všetkým,
čo sa začítali, dočítali či nedočítali. Napísali do odkazov na tomto blogu, na
stránku Martinus, alebo na stránku
Hirax Records... Ozvali sa mi mailom, esemeskou, zastavili ma na ulici, na
koncerte... Hanili, chválili, prežívali, odložili, ďakovali, nadávali, plakali,
zatratili... všetkým. Aj vďaka vám sa za 5 mesiacov predalo dve tisíc kusov
a tretia tisíc kusová dotlač uzrela svetlo sveta a poputovala minulý
týždeň do celoslovenskej distribúcie. Stále sa nevyhlasujem za spisovateľa a ani
za básnika. Baví ma písať, napĺňa to moju dušu a tak mi preveľkí kritici
literárneho umenia odpustite, ale „keď ja musím“.  
 
 
            Prestal som
odpovedať na reakcie ľudí. Prečo? Ľudia majú neuveriteľnú schopnosť vyčítať
z viet stovky zbytočností, ale základné myšlienky im uniknú.
A takisto obdivujem neuveriteľnú vlastnosť pripísať všetko, čo bolo
napísané autorovi, že to na vlastnej koži aj sám prežil. Prehliadajú schopnosť
vnímania a pozorovania sveta a pretransformovania to do viet
s úmyslom pomôcť druhým. Ale na druhej strane sa mi dobre zaspáva
s pomyslením, že ostatok sveta na mňa myslí. Cudzí ľudia o mne vedia
všetko, teda aj to, čo neviem ani ja sám. A tak som sa napríklad dozvedel,
že Hiraxa nechala baba, bol narkoman, dokonca díler, sedel za dva gramy heroínu
dva roky vo väzení, žije stále s rodičmi, ktorí neuveriteľne šetria...
vedia proste všetko. Ďakujem im za ich všemocnú starostlivosť! 
 
 
            Tu dole
ešte uverejňujem konečný zoznam všetkých kapitol. Chýba zverejniť posledných
sedem „kúskov“. Dúfam, že nebude trvať dlho, kým to niekto zoskenuje
a zavesí na nejaký ruský server.
 
 
            Ja opäť
niekam letím i keď sám ešte neviem kam. Sľubujem ale, že donesiem odtiaľ fotečky
a možno spískam nejaký ten report. Opatrujte sa tu krásne
a nezabudnite, že vynútený pokoj v duši nie je to pravé ticho. Buďte
ŠŤASTNÍ (v tom slovíčku je totiž všetko). 
 
 
 
 
 
Hirax  
 
 
 
 
 
 
 
 
Raz aj v pekle vyjde slnko
 
 
 
 
 
01. Žijem?
 
 
02. Oprite sa o strom
 
 
03. Dve borovičky poprosím
 
 
04. Život, ty si zo mňa robíš
dobrý deň?
 
 
05. Obšťan, spamätaj sa!
 
 
06. Boh je len jeden a tak sa prac z môjho domu!
 
 
07. Prečo ja mám tebe ukazovať moju riť?
 
 
08. Prosím vás, vystreľte ma späť na moju planétu
 
 
09. Máme doma kyselinu?
 
 
10. Jednoduchšie sa hovorí, ako v skutočnosti
ťažko žije 
 
 
11. Oklamali ma!
 
 
12. Hrianku našu každodennú, daj nám i dnes...
 
 
13. Prosím, tu Jimmi Hendrix
 
 
14. Nevideli ste ešte človeka žijúceho v nirváne?
 
 
15. Ten kto nosí svoju mravnosť
len ako sviatočný šat, mal by chodiť holý
 
 
16. Na Sibír!
 
 
17. A ty sa dole vyhoľ! 
 
 
18. Keď duši cez telo dôjde, prečo
jej je zle
 
 
19. Yin-Yang
 
 
20. Ja ťa zabijem skôr!
 
 
21. Musíte byť tyranom aj týraným
 
 
22. Hrášok môj sladučký?
 
 
23. Klever je jebnutý!
 
 
24. Od mäsiarky k Bohu 
 
 
25. Karel drogy nebere
 
 
26. Mustafa Mohamed
 
 
27. Hoďte ho na strúhanku!
 
 
28. Doktor Krotký
 
 
29. Červenšie oči som v živote nevidel
 
 
30. Detektív Oriešok
 
 
31. Slopa spal s Maďarkou
 
 
32. Inšpektor Kolovrátok
 
 
33. Nie si ty náhodou pankáč?!
 
 
34. Sadnite si na peň
 
 
35. Bude v kotle dosť miesta
pre všetkých?
 
 
36. Pustite ma, chcem odísť
dôstojne s hovnami
 
 
- - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - 
 
 
37. Konečne teplo pekla
 
 
 
 

