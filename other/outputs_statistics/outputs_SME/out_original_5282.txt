
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Dostál
                                        &gt;
                Politika
                     
                 PPP vlastenci 

        
            
                                    26.4.2010
            o
            13:23
                        (upravené
                26.4.2010
                o
                13:41)
                        |
            Karma článku:
                17.32
            |
            Prečítané 
            5040-krát
                    
         
     
         
             

                 
                    Myslím národne, cítim sociálne. (Ivan Gašparovič) SMER-SD chce aj po voľbách pokračovať v budovaní sociálneho štátu a ochrane národnoštátnych záujmov. (Robert Fico) Národná hrdosť a robotnícka česť končia v Slovenskej poisťovni. (Vladimír Mečiar) PPP vlastenci.
                 

                 
Dva protesty proti vláde PPP vlastencov 
   Možno to od pravičiara bude znieť zvláštne, ale vcelku sa teším na dobu, keď Slovensku bude vládnuť ľavicová vláda, na ktorej mi najviac bude prekážať to, že je ľavicová. Bude to signál, že pomery v tejto krajine začínajú byť aspoň ako tak normálne. Na vláde PPP vlastencov ma viac vyrušujú iné veci. Neschopnosť zodpovedne spravovať krajinu a hodovanie na účet budúcich generácií. Zlodejiny a korupcia. Niekedy vulgárny a agresívny, inokedy zasa kabaretný nacionalizmus.   Súčasná ficovská verzia vlády PPP vlastencov sa v mnoho podobá tej pôvodnej mečiarovskej verzii z 90. rokov. Silný autoritatívny vodca na čele dominantného populistického politického subjektu vládnuci za asistencie Jána Slotu a tretieho koaličného partnera, ktorého je ťažké brať úplne vážne. Okrem neabstinujúceho asistujúceho Slotu obe verzie vlády PPP vlastencov spája aj osoba Ivana Gašparoviča ako jej najvyššej figúrky v ústavnej hierarchii. Za Mečiara ako predsedu parlamentu (keďže prezident Michal Kováč sa Mečiarovi vzoprel), za Fica už priamo ako prezidenta (keďže Ivan Gašparovič sa Mečiarovi vzoprel neskôr a nechal sa vo vhodnej chvíli adoptovať Ficom, vďaka čomu sa prezidentom stal).   Chlieb a hry majú v oboch verziách vlády PPP vlastencov tiež veľmi podobný charakter. Chlieb má podobu ľavicovej a sociálnej rétoriky, ktorá zastiera, že pre davy sú tu len pohodené omrvinky, ale zadným vchodom sa z pekárne neustále odvážajú plné vrecia múky do skladov kamarátov momentálnych pekárov. Hry v oboch verziách zabezpečuje boj proti opozícii, boj proti novinárom, boj proti mimovládnym organizáciám, ale predovšetkým boj proti Maďarom. Sme predsa vlastenci.   Existujú, pravda, aj rozdiely. Jána Ľuptáka v pozícii toho tretieho do partie, ktorého ťažko brať vážne, vystriedal bývalý vodca Mečiar. Národná hrdosť a robotnícka česť dnes už nekončia pred bránami Slovenskej poisťovne, ale pri eurofondoch, verejnom obstarávaní, emisiách a aktuálne napríklad pri pekelne predražených PPP projektoch. A nacionalistické hrátky dnes viac ako poskakovanie okolo vatier zvrchovanosti stelesňuje chlasteneckým zákonom vnucované hranie slovenskej hymny.   Pre ľudí, ktorí majú PPP vlastencov už plné zuby, mám tri pozvánky. Dve na akcie počas práve sa začínajúceho týždňa.   Dnes (pondelok 26. apríla) sa o 16:00 pred Ministerstvom dopravy, pôšt a telekomunikácií SR (Námestie Slobody 6, zo strany Štefanovičovej ulice) uskutoční Protest Proti Predraženiu (PPP). Jeho organizátor Michal Lehuta naň pozýva takto: "Podľa TREND Analyses je 1. balík PPP projektov predražený o zhruba 1,2 miliardy eur. Balík ešte nie je finančne uzatvorený a tieto peniaze sa dajú zachrániť (a to aj bez zrušenia či výraznejšieho oddialenia PPP)."   Zajtra (utorok 27. apríla) sa o 16:00 pred Národnou radou SR uskutoční už druhý protest proti "vlasteneckému" zákonu, tento raz nazvaný "Vlastenecký zákon raz a navždy do smetí". Organizuje Iniciatíva za transparentú demokraciu. Keď po schválení tohto zákona vyšli študenti do ulíc a protestovali prvýkrát, Fico aj Gašparovič sa zľakli a prezident zákon vetoval. Zajtra by mal o ňom hlasovať parlament znovu.   A tretia pozvánka je aj pre tých, ktorí majú do Bratislavy ďalej a nebudú sa môcť osobne zúčastniť dnešných a zajtrajších protestov proti vláde PPP vlastencov. Hlavný protest proti vláde PPP vlastencov bude celoslovenský a koná sa 12. júna vo volebných miestnostiach, nezabudnite. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Konečne sa dozvieme mená všetkých členov Smeru
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Niekoľko viet o Táni Kratochvílovej
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Smeráčina á la Brixi: Len si tak niečo vybaviť
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Fico nám berie peniaze a dáva ich svojim kamarátom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Dostál
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Dostál
            
         
        dostal.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som predsedom OKS. V novembrových voľbách kandidujem v bratislavskom Starom Meste do miestneho i mestského zastupiteľstva. Ako jeden z členov
Staromestskej päťky. Viac na www.sm5.sk. 

  

 Ondrej Dostál on Facebook 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    202
                
                
                    Celková karma
                    
                                                15.07
                    
                
                
                    Priemerná čítanosť
                    5758
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Smrteľne vážne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                             
                                            Ivan Kuhn
                                     
                                                                             
                                            Juraj Petrovič
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Michal Novota
                                     
                                                                             
                                            Ondrej Schutz
                                     
                                                                             
                                            Tomáš Krištofóry
                                     
                                                                             
                                            Peter Spáč
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            INLAND
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Konzervatívny inštitút M.R.Štefánika
                                     
                                                                             
                                            Peter Gonda
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 robí Ficovi reklamu
                     
                                                         
                       V TA3 Fica hrýzkajú slabúčko
                     
                                                         
                       O tom, čo platí "štát"
                     
                                                         
                       Konečne sa dozvieme mená všetkých členov Smeru
                     
                                                         
                       My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                     
                                                         
                       Smeráčina á la Brixi: Len si tak niečo vybaviť
                     
                                                         
                       Potemkinov most v Bratislave
                     
                                                         
                       Fico nám berie peniaze a dáva ich svojim kamarátom
                     
                                                         
                       Výsmech občanom v priamom prenose alebo .... Mišíková v akcii ...
                     
                                                         
                       Ficov podrazík na voličoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




