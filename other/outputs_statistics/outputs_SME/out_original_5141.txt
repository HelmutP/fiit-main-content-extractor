
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Helena Fábryová
                                        &gt;
                Slnečnicové denníky
                     
                 Nerušte naše kruhy 

        
            
                                    23.4.2010
            o
            21:48
                        |
            Karma článku:
                6.66
            |
            Prečítané 
            1674-krát
                    
         
     
         
             

                 
                    Zostali sme niekde na pokraji. Je celkom jedno akého, skrátka na pokraji (ja napríklad nervového zrútenia, keď mi náš cvičiaci podával písomku z rýb za 84,5% so slovami: „ Mohlo to byť aj lepšie."). V istom zmysle je to fajn, lebo občas najhoršie situácie prinášajú najlepšie výsledky. Napríklad snahu.
                 

                     Boli sme s Bibou v Tescu, čo už nie je Tesco, ale „my" na Kamennom. Za pokladňou stála Mgr. Meno Sinepamatam. Faktom bolo, že koeficient snaživosti v tom momente stratil na hodnote. Utešovali sme sa už ani neviem čím, ale podstatné je, že sa nám to podarilo. Aspoň na chvíľu.   Začala som prekladať v inštitúcii, ktorú netreba menovať niečo o amalgáme a nariadeniach. Zaskakujem za žienku na materskej, čo skončila environmentalistiku a doteraz robí v inštitúcii, ktorú netreba menovať. A tak sme sa len s Jankou pýtali, že načo je to všetko dobré a potom sme si aj odpovedali. Pretože teraz máme dvadsať a máme mať akože vzletné predstavy o živote, a že vynájdeme niečo, čím pomôžeme ľudstvu. Alebo niečo podobné. Tak som sa bola v pondelok miesto prednášky zo zoológie pozrieť na jednom ústave a tam si ma Dr.X vypočul a povedal: „Aaaaale, to máte ešte čas."   Neviem, či som jediná, ale všetci ľudia s príveskami Dr. Doc Prof. Csc. a iné ma občas šokujú a mätú. Lebo iný Csc.X mi povedal, že sa mám už niekam zašiť do labáku a už sa snažiť prichádzať na niečo prevratné. Tak sa zatiaľ prevratne drvím na tú (pre toto skúškové fakt nezáživnú) zoológiu, pre teraz.   A vlastne viem, že nie som jediná. Lebo aj Maťa a Veronika sú na tom tak isto. A keď sa pýtali istej Dr., že či nemá problém utínať potkanom hlavy ako na bežiacom páse, istá Dr. Povedala, že ani nie, že ona začínala na mačkách.   Na inej návšteve ďalšieho ústavu (istá súťaž, z ktorej mám na internete fakt otrasnú fotku) ma schytila naša profesorka zo strednej, že mám ísť na onkologický seminár na našej (mojej strednej) škole spraviť prednášku. A tak som sa tam postavila pred tú bandu z cirkevnej školy a tvárila sa, že tomu rozumiem. A tak som si spomenula, ako sme na tom onkologickom seminári sedávali povinne my a museli počúvať. Teraz sa tomu teším.   A potom mi písala iná Dr. z ústavu, že mi praje veľa šťastia do skúškového, a že sa jej to páčilo. (Že ma to dojalo sa príliš verejne snažím nepriznať, ale skoro som sa rozplakala, lebo tú Dr. si vážim)   Teraz nás dojíma málo vecí. A keď, tak sú také celkom nedojímavé (O pol boda nespravené zápočty. Známi zomierajúci na rakovinu, na ktorých nechceme veľmi myslieť a aj tak myslíme. Telefóny, ktoré majú oznámiť, že je to lepšie, ktoré nezvonia. Pseudošamanoliečitelia magoriaci ľuďom mozgy a trieskajúci somariny, čo záhadne zmiznú, keď je to s tými, ktorým akožepomáhali fakt zlé. Neexistujúce čísla, na ktoré ste mali zavolať.). Možno je to len akési štádium zakuklenia, keď máme dozrieť. Možno do srdcí v kostených obaloch.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Fábryová 
                                        
                                            Základy moderného hejtu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Fábryová 
                                        
                                            Konce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Fábryová 
                                        
                                            Toto mesto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Fábryová 
                                        
                                            O tej prašivej vede
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Fábryová 
                                        
                                            Okná dokorán
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Helena Fábryová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Helena Fábryová
            
         
        fabryova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Už a ešte študentkou. Človek. Snáď.
°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
"Sušienka polomáčaná v skepse, ktorá to nedáva najavo."/Lydka/
°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
"Všetci sú ufóni, ale len ty budeš ovečkus."/BD/
°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
"Môj" vesmír je
fraktálový, donekonečna sa v ňom opakujú tie isté
vzory. /Micro/
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    243
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1445
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            NaturAll
                        
                     
                                     
                        
                            Slnečnicové denníky
                        
                     
                                     
                        
                            Medové letá
                        
                     
                                     
                        
                            Ecce, homo !
                        
                     
                                     
                        
                            Literárne
                        
                     
                                     
                        
                            Pár strelených úvah
                        
                     
                                     
                        
                            My homework
                        
                     
                                     
                        
                            Výčiny mojej rodiny
                        
                     
                                     
                        
                            Súkromnôstky poézne
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Tento ľud
                                     
                                                                             
                                            Robert Frost: The Road Not Taken
                                     
                                                                             
                                            Znamenie blížencov
                                     
                                                                             
                                            Horčica na chodníku
                                     
                                                                             
                                            Ako som sa zaľúbil do matematiky
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            To čo mám na nočnom stolíku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Nobelovci
                                     
                                                                             
                                            °°°°random°°°°
                                     
                                                                             
                                            Alenka Sabuchová
                                     
                                                                             
                                            Saša Grodecká
                                     
                                                                             
                                            Lucka Kramaričová
                                     
                                                                             
                                            moja žienka Lydulienka
                                     
                                                                             
                                            Anna Strachan
                                     
                                                                             
                                            Veronika Vlčková
                                     
                                                                             
                                            Feeling by Mitosinkovie
                                     
                                                                             
                                            Alison
                                     
                                                                             
                                            Aubrey
                                     
                                                                             
                                            Allan Stevo
                                     
                                                                             
                                            °°°°p&amp;#229; svenska°°°°
                                     
                                                                             
                                            The hymn for the cigarettes
                                     
                                                                             
                                            Anna &amp;#229;den
                                     
                                                                             
                                            °°°°ChemABioAMedAVšetko°°°°
                                     
                                                                             
                                            Lenka Abelovská
                                     
                                                                             
                                            Veľmi múdry človek-Jaroslav Flegr
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            °°°°common life°°°°
                                     
                                                                             
                                            Alma mater secunda
                                     
                                                                             
                                            Alma mater
                                     
                                                                             
                                            Alma mater mini
                                     
                                                                             
                                            Alma mater micro
                                     
                                                                             
                                            A o všetkom vyššie uvedenom na sieťovke
                                     
                                                                             
                                            °°°°literárne°°°°
                                     
                                                                             
                                            Múza
                                     
                                                                             
                                            Literární Doupě
                                     
                                                                             
                                            V iných vodách
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




