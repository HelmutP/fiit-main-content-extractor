

 Viem, že nič neviem- Sokrates 
 Ja som dlho nevychádzala z domu. 
 Vedľa práve počúvam zvuky neutíchajúcej telefonickej hádky, ani pridanie hudby nepomáha. 
 Stresy, ktoré nás sprevádzajú spolu s doškriabanými tvárami, z ničoho vzniknutými modrinami a chcejúcimi vnútornými pocitmi. 
 Alebo nepocitovosť, keď sa máš tešiť, radovať a cítiš obrovské nič. 
 Zbytočné frázy, ktoré sa aj napriek všetkému stále opakujú pri stále tých istých situáciách. 
 Falošné predstavy, slová a správanie, ktoré napriek falošosti budí ideál života. 
 Aj taká pesnička, ktorá stratila svoju krásu, aj keď predtým tu pre nás stále bola. 
 Bezvýznamné čakania, prechádzania sa a problémy, ktoré sa nedajú vyriešiť. 
 Vzťahy, ktoré trvajú aj keď nemajú pokračovanie. 
 Nezmyslenosť obrovských prianí a nesplnené plány, ktoré nám bránia v normálnom pokračovaní. 
   
 Tento článok nie je depresívne ladený, len mám chuť ukončiť niečo a začať niečo nové. Dať to na ,,papier,, a tým všetko potvrdiť. 

