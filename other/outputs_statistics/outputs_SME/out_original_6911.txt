
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Libor Lukáč
                                        &gt;
                Nezaradené
                     
                 V čom je skrytá nemožnosť zmeny k lepšiemu? 

        
            
                                    20.5.2010
            o
            18:28
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            205-krát
                    
         
     
         
             

                 
                       Tento krátky text má byť apelom na nevyhnutnosť dodržiavania určitej výšky morálneho štandardu v každej oblasti života spoločnosti a teda aj v politike. Práve toto kritérium je totiž rozhodujúcim a určujúcim prvkom, ktorého uprednostňovaním pred všetkým ostatným je možné vybudovať a zaistiť harmonický chod spoločnosti.
                 

                        Nie som politik a nemám ambíciu navrhnúť konkrétne kroky k uskutočneniu pozitívnych politických a ekonomických zmien. Som však hlboko presvedčený o tom, že nijaké, ani tie najlepšie vyzerajúce riešenia neprinesú nikdy želaný, dlhodobý a trvalo udržateľný prospech ak ich tvorcovia a uskutočňovatelia nebudú spĺňať určité, už spomínané, vysoké morálne štandardy. To je základ, bez ktorého sa nepohneme ďalej!          Ak tento základ nebudeme chcieť akceptovať, každý spoločenský systém a každá snaha o jeho reformu, budovaná na iných základoch, bude mať určité dlhšie, alebo kratšie trvanie, avšak nakoniec, tak ako všetky doterajšie systémy a snahy v dejinách ľudstva, sa vždy zrúti!           Padol Egypt, padlo staroveké Grécko, padol Rím, padla Hitlerova tisícročná ríša, padol komunizmus, ktorý tu mal byť na večné časy a zákonite musí padnúť aj súčasný systém, pretože tak, ako vo všetkých predchádzajúcich prípadoch, aj v súčasnosti už celková morálna prehnitosť vo všetkých oblastiach života spoločnosti dosahuje svojho vrcholu. Dosahuje vrcholného bodu zlomu, za ktorým, tak ako tomu bolo vždy doposiaľ, nasleduje už iba strmhlavý pád nadol.           Už pred 5000 rokmi nám boli ukázané princípy, na ktorých má byť budovaná ľudská spoločnosť, ak má byť úspešná, harmonická a jej vývoj trvalo udržateľný. Tieto princípy sú zhrnuté v Desatore! Ľudstvo dneška ich pozná, hovorí o nich, ale je na míle vzdialené od toho, aby podľa nich aj žilo! Či už každý človek sám, vo svojom osobnom živote, alebo spoločnosť ako celok, ktorá by dodržiavanie týchto princípov zahrnula do svojho zákonodárneho systému.           Avšak každá spoločnosť a každý systém, ktorý sa neriadi princípmi Desatora sa musí skôr či neskôr zákonite zrútiť! Čím neskôr, tým tvrdšie a zásadnejšie! Pohľad do vzdialenej i nedávnej histórie je toho dôkazom!          Na tejto planéte a v tomto stvorení totiž nemá stáleho trvania nič, čo sa neopiera o základné princípy Desatora, čiže o Vôľu Božiu! Ak teda obyčajní ľudia, tvoriaci základ každej spoločnosti, ako i určitá elita, ktorá vzíde z ich radov, čiže súčasní politici, ak títo všetci nebudú sami osobne vnútorne stáť na zmieňovaných princípoch, žiť nimi a aktívne ich uplatňovať pri všetkom, čo robia, k nijakej významnej a zásadnej zmene ľudskej spoločnosti k lepšiemu nebude môcť nikdy dôjsť. Bez toho najpodstatnejšieho základu budeme vždy kráčať od jednej krízy ku druhej a to až ku celkovému zrúteniu a pádu tejto civilizácie.           Inej cesty, ako cesty v súlade s Božou Vôľou totiž v tomto stvorení niet! Ani pre jednotlivca, ani pre ľudskú spoločnosť!            L.L., sympatizant Slovenského občianskeho združenia pre posilňovanie mravov a ľudskosti    http://www.pre-ludskost.sk/            

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Televízia – hriech náš každodenný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Iný pohľad na katastrofu v Japonsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            O radosti a bolesti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Zahynieme na devalváciu slova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Katastrofa v Japonsku – hlas Matky prírody!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Libor Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Libor Lukáč
            
         
        liborlukac.blog.sme.sk (rss)
         
                                     
     
        Snažím sa byť človekom s vlastným pohľadom na realitu. Emailový kontakt: libor.lukac@atlas.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    504
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




