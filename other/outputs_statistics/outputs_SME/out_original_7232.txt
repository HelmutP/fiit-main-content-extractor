
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Turanský
                                        &gt;
                Brazília
                     
                 Posledný večer 

        
            
                                    25.5.2010
            o
            1:38
                        |
            Karma článku:
                8.31
            |
            Prečítané 
            1640-krát
                    
         
     
         
             

                 
                    Už sa zotmelo a ja sedím za malým písacím stolom a dávam si dohromady, čo mi prinieslo posledných desať mesiacov môjho života. Asi by som to pomenoval jednoduchým ale výstižným slovom zmena.
                 

                   
 Zmenu v mnohých zmysloch slova. Asi hlavne citovú. Odišiel som zo Slovenska hlavne preto aby som videl a zažil niečo nové, niečo, čo som dovtedy nemal šancu spoznať ani zažiť. Stalo sa tak. Postupne s každou jednou zmenou sa formoval môj charakter. Sám neviem, či k lepšiemu alebo horšiemu ale zmenil sa. Sám to ani tak nevnímam, hlavne to vnímam zo strany svojich kamarátov, známych, rodiny. Často sa pozastavím nad vlastnou reakciou a poviem si, že takto by som predsa nikdy nereagoval. Potom sa zamyslím a zistím, že tá prvotná reakcia je moja pravá a znovu vidím v plnej kráse zmenu.   Určite som si za tých desať mesiacov začal viac vážiť tradičné hodnoty ako rodina a priatelia, pretože až keď som bol bez nich, uvedomil si, ako veľmi ich potrebujem. Či už to boli moji kamaráti, ktorí sa mi neúnavne počas tých desiatich mesiacov ozývali (tí, o ktorých je reč sa spoznajú) alebo rodina, od ktorej som mal podporu vtedy, keď som to naozaj potreboval. Za ten čas, ktorú som tu strávil som videl ako si ľudia dokážu pomáhať, ako sa dokážu nenávidieť, ako sa sobášia alebo rozvádzajú. Zvláštna bola jedna vec. Každá jedna udalosť, ktorá ma nejakým spôsobom ovplyvnila nebola ničím výnimočným. Boli to obyčajné krásy alebo hnusy všedného dňa, ktoré sa mi zaryli do pamäti.   Včera mi moja hosťovská rodina, s ktorou som od začiatku nevychádzal najlepšie , pripravila oslavu o 50 pozvaných, ktorí mi prišli posledný krát povedať zbohom. Bolo mi ľúto, keď som ich videl ako mi kývajú cez mokré okno auta a až v tej chvíli som si uvedomil, že ich mám rád a že ich stále nepoznám tak dobre, ako by som chcel. Niektorých z nich som pochopil, niektorých menej, ale každý z nich navždy zostane v mojich spomienkach. Aj pre tých pár posledných slov, ktoré som od každého dostal. Každé jedno z nich bolo pre mňa iskrou v tom upršanom večeri.   Vo chvíli, keď sa už schyľovalo k slze sa niečo vo mne preplo. Smútok prešiel a mňa zaliala vlna šťastia. Z dvoch dôvodov. Zajtra sa vraciam späť domov. K svojim najbližším. A takisto zajtra odchádzam zo svojho domova, ktorý sa tu pre mňa za ten čas vytvoril. Od svojich najbližších. Od tých, ktorí ma každí deň iritovali a liezli mi na nervy a ja som si ich nakoniec obľúbil. Každého jedného. Ale ako Zdeněk Svěrák povedal: „Aby mohlo být nějaké vítání, musí být napřed loučení.“   A tak ako včera večer pršalo vonku, pršalo aj vo mne. A tak ako zasvietilo slnko vonku, zasvietilo aj vo mne. Vznikla z toho pekná dúha, ktorá zanechala mnoho spomienok.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Nie všetko "značkové" je naozaj "značkové" alebo o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Amazónia - Tajomná a očarujúca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Komunisti ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Z blata do kaluže
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Spomínam...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Turanský
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Turanský
            
         
        tomasturansky.blog.sme.sk (rss)
         
                                     
     
        Som študent, bývalý výmenný, ktorý si absolvoval rok v Brazílii a teraz si zvyká znovu na realitu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1528
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Brazília
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Andrzej Sapkowski - Boží Bojovníci
                                     
                                                                             
                                            Roald Dahl - Neuveriteľné Príbehy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Led Zeppelin
                                     
                                                                             
                                            Sex Pistols
                                     
                                                                             
                                            Ramones
                                     
                                                                             
                                            Rádio Expres
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




