

 Postupne som sa dostala k celému albumu s kompletnou zostavou piesní z menovaného muzikálu. Priniesla si ho kamarátka z Francúzska a ja som po ňom hneď skočila, keď som tam spoznala milovanú skladbu. Zapáčil sa mi celý, dokonca aj môjmu otcovi, ktorý vie po francúzsky tak akurát pár slov. 
   
 Roméo et Juliette, de la haine a l´amour (R &amp; J, od nenávisti k láske)  
 Kto by nepoznal známy a nespočetne krát spracovaný príbeh lásky Rómea a Júlie? Áno, presne tak, všetky úspešné muzikály majú niečo spoločné - dobre známy príbeh v jedinečnom prevedení.  
 Vo Francúzsku zaznamenal muzikál mimoriadny úspech. Vznikol v roku 2001 na motívy nesmrteľného románu Wiliama Shakespeara v Justičnom paláci v Paríži. Autorstvo patrí Gérardovi Presgurvicovi (text, hudba). Muzikál sa samozrejme v mnohých variáciach a obmenách čiastočne líši od originálnej shakespearovskej verzie. Vyšiel taktiež na CD a DV a predalo sa z nich 6,5 milióna a získal ocenenie od NRJ Music Award. Album vyšiel v jednoduchej verzii (17skladieb), ale aj ako dvojalbum, obsahujúci 38 skladieb. Osobne odporúčam piesne: 
  Vérone   
  Aimer 
  Avoir une fille   
 Sans elle 
 Un jour  
  On dit dans la rue 
 La mort de Juliette  
 Comment lui dire  
 Les rois du monde a vlastne ťažko vyberať...Mám rada všetky.  
 Vo francúzskej verzii nechýbajú skvelé spevácke výkony pre nás neznámych tvárí, akými sú napríklad: Damien Sargue (Rómeo), Cécilia Cara (Júlia), Grégori Baquet (Benvolio), Philippe d´Avilla (Mercutio), Tom Ross (Tybalt), Eléonore Beaulieu (Lady Monteková), či Isabelle Ferron (Lady Kapuletová) a mnohí iní.  
 Od prvého predstavenia, ktoré sa konalo v Paríži 19. januára 2001, bol muzikál prepracovaný do holandčiny, španielčiny, ruštiny, angličtiny, nemčiny i maďarčiny a nové adaptácie sa plánujú aj pre Talianko, Poľsko, Portugalsko, Austráliu, Singapur či Japonosko. Odohralo sa množstvo predstavení po celom svete, ktoré svedkom bolo viac ako 5 miliónov divákov v Kande, Lodnýne, Amsterdame, Budapešti, Viedni, Soule, Moskve a v niekoľkých ďalších mestách. Všade zožalo pozoruhodný úspech. Niektoré z verzií iných krajín sa líšia v spracovaní niektorých motívov (záver hry - smrť Róme a Júlie) a predovšetkým v kostýmových kreáciach.Vo francúzskej verzii sú napríklad farebne odlíšení Kapuletovci, Montekovci a zvyšok účinkujúceho osadenstva.  
 Pre úspech sa muzikál v tomto roku vrátil do Justičného paláca v Paríži, kde sa odohralo 43 predstavení. 
 http://www.romeoetjuliette.eu/ (oficiálna webová stránka) 
 Najúspešnejší singel z rovnomenného muzikálového albumu - "Les rois du monde" (Králi sveta)  
  






 
   
 Notre Dame de Paris 
 Legendárny román známeho Victora Huga Chrám Matky Božej v Paríži sa stal inšpiráciou pre mnohé filmové spracovania a nezabudlo sa naň ani v muzikálovom prevedení. Vo Francúzsku to vlastne bola práve táto hra, ktorá spustila  módu muzikálov." Za jeho zrodom stojí Francúzom dobre známy textár Luc Plamondon a skladateľ Richard Cocciante. Účinkovanie jednotlivých spevákov v muzikáli Notre Dame de Paris, a tým aj ich výrazné zviditeľnenie, dopomohlo mnohým k neskoršej sólovej speváckej kariére . 
 V dobre známom príbehu o milostnom trojuholníku (či snáď aj štvoruholníku) a nielen o ňom, postavu Quasimoda brilantne zvládol dobre známy spevák Garou, Esmeraldu stvárnila nemenej obúbená krásavica Hélène Segara , Phoeba Patrick Fiori, Frolla Daniel Lavoie, postavu Fleur de Lys (Pheobovu nastávajúcu)  Julie Zenatti a neskôr i Natasha St Pier a v mnohých alternáciách sa vystriedalo veľa ďalších umelcov. 
 Po prezentácii muzikálu vo frankofónnych krajinách (Francúzsko, Belgicko, Švajčiarko, Kanada), nasledovala svetové turné v roku 2005 v Pekingu, Šanghaji, Soule a Taiwane. Popritom vznikla i ruská, španielska a talianska adaptácia Notre dame de Paris. 
 Tvorcovia majú v úmysle obnoviť predstavenie koncom roku 2010 pri príležitosti 12. narodenín jeho vzniku, s účasťou pôvodnej zostavy hviezdnych účinkujúcich. 
 Samozrejme vyšlo aj CD s hudbou z muzikálu. Spomedzi piesní mali veľký úspech už ako samotné single skladby Vivre, Le Temps des cathédrales a predovšetkým "Belle" (Krásna), interpretovaná triom Garou, Lavoie, Fiori, ktorá získala v roku 1999 titul Pieseň roka.  
 






 
   
 Les dix commandements (Desatoro prikázaní) 
 Opäť nemenej známy príbeh, tentokrát s biblickým námetom, mal premiéru v roku 2000. Vykresľuje život Mojžiša od jeho narodenia, útek z Egypta po zabití faraonovho slubožníka, pretože bránil hebrejského otroka, Božie zjavenie a poslanie vyslobodiť Izraelitov z egyptského otroctva, Boží trest v podobe desiatich rán za faraónovu neoblomnosť, až po prepustenie ľudu, opätovné prenasledovanie Egypťanmi a zázrak pri prechode cez more, až po príchod do zasľúbenej krajiny a dar Boží v podobe Desatora, ktoré má vyvolený ľud zachovávať. 
 Predstavenie napísané Pascalom Obispom a Eliem Chouraquim samozrejme dostáva modernejšiu tvár a autori vytvorili postavy s priliehavými menami ako Nefertati, Ramses, Yokébed, Aaron a v neposlednom rade Mojžiš. V hlavnom obsadení sa objavil Daniel Lévi, Pedro Alves, Gini Line i speváčka izraelsko-francúzska speváčka, známa aj v našich končinách, Yael Naim. 
 http://les10commandements.ifrance.com/ (oficiálna webová stránka) 
 Album s muzikálovými melódiami s rovnomenným názvom takisto nezostáva nepovšimnutý. Pieseň "L´envie d´aimer" (Túžba milovať) v podaní Daniela Léviho sa stala Piesňou roka 2001 (1.800 000 predaných singlov) vo Francúzsku. 
 






 
   
 Le Roi Soleil (Kráľ Slnko) 
 Príbeh o panovníkovi Ľudovítovi XIV., prezývanom Kráľ Slnko, rozpráva o jeho živote a láskach. Prvé predstavenie sa konalo v septembri 2005 v Palais des Sports v Paríži, posledné v júli 2007 po dvoch sezónach turné vo Francúzsku, Belgicku a Švajčiarsku. Výkony spevákov v muzikáli boli dvakrát ocenené v kategórii Frankofónna skupina roka 2006 a 2007 na NRJ Music Awards. Svojim hlasmi sa zaskveli vynikajúci predstavitelia postáv, menovite Emmanuel Moire ako Ľudovít XIV., Christophe Maé, Ľudovítov brat, Anne - Laure Girbal ako Marie Mancini, Cathliane Andria - Francoise d´Aubigné, Merwam Rim ako vojvoda Beaufort, Lysa Ansaldi i Victoria Petrosillo v postavách Madam Montespan a Isabelle. Mnohí zo spevákov aktuálne fungujú sólovo, Christophe Maé sa aktuálne nachádza na 2. mieste v predajnosti albumov vo Francúzsku (On trace la route). Vrelo odporúčam.   
 Samotný príbeh sa len málo dotýka skutočnej reality, nedáva divákovi kompletnú predstavu o význame panovníka v histórii Francúzska. Avšak motívy vzťahov, lásky a intríg vžy priťahovali diváka, o čom svedčí aj úspech tohto i predchádzajúcich menovaných muzikálov. Príprave show sa venovalo viac ako 150 odborníkov, na vytvorení kulís a hlavne kostýmov si dali extrémne záležať. Po mediálnej prezentácii sa Le Roi Soleil hral 380 krát a videlo ho 1,6 milióna divákov. Ani úspech CD s hudbou z muzikálu nie je zanedbateľný, predalo sa takmer milión kópií. Zo singlov dosiahol najväčší úspech duet Emmanuela Moira s Annou - Laurou Girbal "Je fais de toi mon essentiel" (Si pre mňa to najdoležitejšie).  
 Jednoducho nádhera... 
 






 
   
 Cléopatre, la dernière reine d´Égypte (Kleopatra, posledná egyptská kráľovná) 
 Za zrodom posledného vybraného muzikálu, rovnako aj predchádzajúceho menovaného, stojí Kamel Ouali. Premiéra je taktiež spätá s Palais des Sport v Paríži a s dátumom 29. január 2009, pokračuje s turné po Francúzsku, Belgičku a Švajčiarsku. Záver show, možno dočasný, sa datuje len koncom januára 2010.  
 V predstavení ide o príbeh poslednej kráľovnej Egypta, ktorá utiekla zo svojej krajiny do Ríma až do jej smrti. Nechýba zase láska a vzťahy, intrigy a zápletky. 
 http://www.cleopatre.com/ (oficiálna webová stránka) 
 Album Cléopatre,  la dernière reine d´Égypte vychádza v kompletnej zostave piesní v roku 2009, no už v roku 2008 si niektoré single získavajú pozornosť poslucháčov. Tým prvým bol "Femme d´aujourd´hui," naspievaný skvelou Sofiou Essaïdi, predstaviteľkou samotnej Kleopatry. V roku 2010 získava speváčka od NRJ Music Award 2010 cenu Najlepšia ženská frankofónna umelkyňa roka. V minulosti sa zúčastnila populárnej francúzskej súťaže Star Academy (2003).  
 Nechýbalo ani ocenenie speváckych výkonov titulom Najlepšia skupina/najlepšie duo roka 2009, voilà ukážka z najlepšieho...  
 






 
 Konečne by som rada niečo podobné vzhliadla aj u nás...aj keď s tou francúzštinou to bude komplikovanejšie :-) Lebo bez nej, v inom jazyku, to už pre mňa vlastne nie je ono... 

