
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Tropko
                                        &gt;
                Moskovia
                     
                 Dača podmoskovská 

        
            
                                    26.9.2005
            o
            8:08
                        |
            Karma článku:
                15.16
            |
            Prečítané 
            3974-krát
                    
         
     
         
             

                 
                    V dedinke neďaleko Moskvy sme mali suseda – starý a veľmi nepríjemný človek. Môj dedko mal strašne rád niečo majstrovať, lenže ruky mu rástli nie z toho miesta, odkiaľ by mali, tak im že vyrobené predmety, boli nepodarkami. Ešte by som ho mal trochu opísať, aby ste si ho vedeli predstaviť a o čom budem tu písať. Bol to žilnatý, dosť vysoký, s obrovským nosom a tupím pohľadom. Mal okolo 70 – 75 rokov a mal prezývku Rybár. A ako ju získal?
                 

                  Často dostával záchvat, ktorý sa prejavoval, tak, že začal niečo majstrovať v čase, keď sa všetci normálny ľudia len prevracali na druhý bok. Vtedy začal niečo píliť, strúhať a pribíjať. Polovica dediny nespí, sen je pretrhnutý, všetci chodia už zrána zlý, zachmúrený.     Okolo obeda som sa cez plot opýtal susedky, čo že to dedko od rána píli. Odpovedala že to bude búdka pre vtákov. Ostal som trochu zarazený, pretože podľa toho, koľko dreva sa na túto stavbu narezalo, musí to byť vtáčí mrakodrap. No všetky moje predpoklady, odhady boli .....     To čo som poznal z detstva o stavbách týchto domčekov pre vtáčiky a to čo som večer uvidel, bolo tak rozdielne, že i po nejakej dobe nie som to schopný presnejšie opísať. Neuveriteľne veľká kocka, celá  akoby zhúžvaná, neurčitej zeleno – žlto - hnedej farby, s pliesňou bielej farby. Najlepšie by to vyjadrilo „ veľmi - veľmi škaredé“.     V princípe bolo to presne také, ako všetko čo vychádzalo spod „zlatých“ rúk tohto ľudového umelca. V tejto vtáčej búdke, bolo možné  ubytovať škorcov (pre koho sa to vlastne postavilo)  celej Moskovskej oblasti.     Diera, pardon, vchod, otvor, bol natoľko veľký, že škorec by dovnútra mohol vletieť na plnom lete, s rozprestretými krídlami.     Okrem toho, pod týmto otvorom, bol pribitý vertikálny rebrík (zaujímavé, že  dedko nedomyslel k nemu prirobiť zábradlie, veď tak by sa škorcom pohodlnejšie domov vchádzalo, potom ako by sa nejakých bobúľ prežrali).     Všetko to vyzeralo skôr na to, že dedko chcel aby jeho kocúr prešiel, s cieľom ekonómie na vtáčie jedlo. No iba teraz to začínalo byť zaujímavé.  Dedko tento činžiak pristavil k topoľu, hneď vedľa svojej toalety, dotiahol svoj krivý rebrík, vlastnej výroby a začal liezť do hora zavesiť škorečník ku kmeňu stroma. Keď už bol vysoko okolo 5-7 metrov, a rozhodol sa že tento architektonický skvost, v štýle post modernizmu, bude dostatočne vysoko, dedko začal  škorečník pripevňovať s  drôtom ku kmeňu.     Ešte šťastie, že túto budovu neuvidel žiadny škorec, ktorých čakal výmer do tejto novostavby, inak by dostal infarkt.     A stalo sa to, čo sa dalo čakalo. V jeden okamih sa dedo nejako odklonil, pohupol  sa na nepevnom rebríku a upustil svoj výtvor. Podľa toho akým zrýchlením letela búda k zemi a rozmlátila sa o bok železnej bočky stojacej na zemi, musela byť neuveriteľne ťažká.     Ďalší sled udalostí vošiel do dvoch sekúnd, no ja to predsa opíšem podrobnejšie. Keď vtáčia búda padala, rebrík s dedom sa odklonil od kmeňa jablone, rebrík sa s ním pohral, len jeho ruky siahli do prázdna, keď chcel siahnuť na vzdiaľujúce sa konáre.     A padal s rebríkom, chrbtom priamo na strechu záchodu s krikom: „Gooop!“ - Rusi nemajú H.     Do toho ako sa rozlomila azbestocementová krytina, ktorou bola pokrytá strecha, som ešte zahliadol široko otvorené dedove oči. Výraz jeho tváre bol taký akoby nečakane zo strechy svojej toalety uvidel Ajfelovku, a ako by bol týmto javom úprimne a po detsky prekvapený.     Po tom ako sa strecha prepadla a dedko sa prepadol chrbtom s ňou, hlavou dopredu letel prekrásnym čistým flopom, stihol ešte nohami prepliesť. Ďalej bolo počuť len tresk lámajúcej sa dosky (tá kde je tá diera) a ihneď potom hluché pľasnutie.     Babka Aňa, dedova žena, ktorá videla všetko na svoje oči od momentu, kedy dedo začal let nad vlastným hajzlom a uvidel Ajfelovku, zakričala:     “Zabil sa! Sergejevič sa zabil!!!“     Z vnútra odpadovej jamy bolo počuť:     „ Ap...ap...fuj...zachráňte...ap...pfu...tfuj...pomóc...ap ...topím sa...ap...tfuj...ap...“     Keďže „kadibudka“ bola spustená s vlastného výrobného pása Sergejeviča a preto bola nie veľmi pevná. Inak by si Sergejevič iste rozbil hlavu.     A keďže dedko bol maximalista, žumpa bola hlboká cez 4 metre. K tomu okolo Moskvy je spodná voda nie hlboko, jama bola plná, a dedko sa skoro utopil.     Na krik sa zbehli všetci susedia – zachraňovať starca. V prelomenú sedaciu dosku sa vsunul rebrík, po ktorom náš dedko-škorečník aj vyliezol, po uši v hovne.     Našťastie, dedo mal len zopár modrín. Čo bolo horšie, bolo že dedko vyliezol z jamy na boso a v jame zostal zväzok kľúčov, s kľúčami od domu a od všetkých tých nepekných a krivých skladíkov, budiek, ktoré boli porozhádzané, po celom jeho pozemku. Čižmy a kľúče ešte zopár dni lovil s udicou so závažiami, potom so sieťou na konštrukcii – iste svojej výroby - tam kde ich stratil.     Po tomto incidente dostal náš sused, za svoje pracovné úspechy, čestnú prezývku – Rybár.     A rozmlátený škorečník Sergejevič rozobral, dosky opäť rozpílil, klince vyrovnal a zložil všetko do drevárne, do novej konštruktérskej horúčky.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako bolo zostrelené Malajzijské lietadlo MH17
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako Sibiriaci „profesionálne“ rozohnali demonštrantov „amatérov“.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Internát podľa Sťopu Kinga alebo čo v Mlynskej doline nezažijete.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako sme vysadili Bielorusa na Marse.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Nechcem byť Bratislavčan! No a čo je na tom.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Tropko
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Tropko
            
         
        tropko.blog.sme.sk (rss)
         
                                     
     
         Všetko je v blogoch. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    157
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6146
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sibiriáda
                        
                     
                                     
                        
                            Moje univerzity
                        
                     
                                     
                        
                            Moskovia
                        
                     
                                     
                        
                            Via Russia
                        
                     
                                     
                        
                            Milicejské bájky
                        
                     
                                     
                        
                            Svokrine moskovské bájky
                        
                     
                                     
                        
                            Pohľad z iného brehu
                        
                     
                                     
                        
                            Christiánia
                        
                     
                                     
                        
                            Slovakia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            O Čečencoch, vodke a ako to bolo
                                     
                                                                             
                                            Ruské knihy na Slovensku
                                     
                                                                             
                                            Procházková zo SME - ľudská hyena?
                                     
                                                                             
                                            Toto nás čaka?
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            My iz buduščevo
                                     
                                                                             
                                            Ja zdelan v CCCP
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vysocky mal kamaráta Slováka
                                     
                                                                             
                                            Jediné východisko pre Spiš
                                     
                                                                             
                                            Svedectvo
                                     
                                                                             
                                            O slobodnej demokratickej žurnalistike
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Úžasní ľudia
                                     
                                                                             
                                            Hlasuj!
                                     
                                                                             
                                            Ruské Preklady
                                     
                                                                             
                                            krestan.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pápež ignoruje pápeža, katolicizmus v troskách!
                     
                                                         
                       Spáľme kostoly?
                     
                                                         
                       Skoro som prišiel o dcéru
                     
                                                         
                       Nenechajme si ukradnúť deti
                     
                                                         
                       Informácie (nielen o Rusku) z druhej strany?
                     
                                                         
                       Ako bolo zostrelené Malajzijské lietadlo MH17
                     
                                                         
                       Štyri a pol zemiaka
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Chcete dať svoje deti k skautom? Prečítajte si najprv toto.
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




