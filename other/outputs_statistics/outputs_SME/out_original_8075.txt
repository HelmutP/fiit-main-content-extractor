
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrej Fuksa
                                        &gt;
                Nezaradené
                     
                 Intro do MS 2010 v Afrike 

        
            
                                    4.6.2010
            o
            22:27
                        (upravené
                4.6.2010
                o
                22:50)
                        |
            Karma článku:
                5.57
            |
            Prečítané 
            848-krát
                    
         
     
         
             

                 
                    „Juhoafrická republika" vyšlo z úst prezidenta FIFA Seppa Blatera na pridelení 19. majstrovstiev sveta vo futbale v máji 2004 v Zürichu. O možnosť usporiadať tento futbalový sviatok sa uchádzali ešte ďalšie africké krajiny lebo bolo určené, že majstrovstvá usporiada historicky prvý krát krajina z čierneho kontinentu. Druhé skončilo Maroko a tretí držiteľ posledných troch titulov Afrického pohára: Egypt.
                 

                 
Oficiálny poster majstrovstiev sveta 
       Kvalifikácie   Stručne vám teda predstavím kto sa dostal, kto nie a odkiaľ na MS.   Začnem európskou kvalifikáciou. Tá bola aj z nášho pohľadu najzaújmavejšia, pretože tu hrali aj naši reprezentanti. Práve tu sa začala naša africká rozprávka. Rušili ju iba Slovinci, ktorí nás dva krát porazili. Ostatné tímy si nevedeli dať rady s našou obranou vedenou Škrteľom a privádzali ich do zúfalstva naši útočníci; Vittek, Šesták, Hološko. Všetko sa to začalo domácim víťazstvom nad Severnými Írmi. Schladili nás však Slovinci, ale opäť sme sa otriasli a zvíťazili sme nad San Marínom, i keď nám ako jediným v skupine dokázali streliť gól. Bežala 85. min. domáceho zápasu na Tehelnom poli a prehrávali sme s Poľskom 1:0. Zázraky sa však stále dejú a potvrdil to Šesták, ktorý kurióznymi gólmi v posledných piatich minútach zvrátil výsledok na 2:1 v prospech Slovenska. V Prahe na Letnej sme opäť preukázali že patríme na MS. Šesták strelil v prvom polčase gól, ale ešte do prestávky stihli Česi vyrovnať. Druhý polčas sme úplne ovládli a už sa len čakalo, kedy vsietime gól. Ten prišiel a mohli sme si zakričať notoricky známu vetu „ Jendrišek nedal, Jendrišek góóól". V tomto zápase sa naplno prebudil náš spiaci fanúšikovský obor. Povinné víťazstvo sme zaknihovali doma so San Marínom. Ale čakala nás ďalšia veľká skúška; domáci zápas s Čechmi. Dvakrát sme viedli, ale vždy stihol súper vyrovnať. Navyše sme prišli o vylúčeného Hamšíka. Po tomto zápase sme bezprostredne cestovali do Belfstu. Domáci sa nás však zľakli a nedokázali nič vymyslieť. Naši to potrestali a zvíťazili 2:0. Naplno sa preukázali ako platní hráči aj mladé pušky; Miňo Stoch  a Vladko Weiss syn reprezentačného trénera. Boli sme už blízko, ale posledný krok vždy je najťažší. Potvrdilo sa to, prehrali sme doma so Slovinskom a výrazne sme si tak skomplikovali našu cestu do Afriky. Nič nám neostávalo iba v snehovej chumelici zdolať v Choržove domáce Poľsko. Už v tretej minúte nám k tomu dopomohol vlastencom Poliak Gancarczik. Naši už potom iba bránili. Vynikajúco sa prezentoval náš brankár Mucha a sme mu vďační za jeho ukážkové zákroky. Víťazstvo sme si v nervóznom zápase udržali a Afriku nám už nik nevezme.   Z postupu sa ešte radovalo z Európy priamo Španielsko, Nemecko, Taliansko, Holandsko, Anglicko, Dánsko, Srbsko, Švajčiarsko a teda aj Slovensko. Z baráže na MS pocestuje Francúzsko, Portugalsko, Grécko a Slovinsko. Miestenka sa už neušla takým krajinám ako Švédsko, Chorvátsko, Rusko, Írsko či súperi z našej skupiny Česko a Poľsko.   V zóne Concacaf, čo je Severná a Stredná Amerika si postup vybojovali USA, Mexiko a prekvapujúco aj Honduras na úkor Kostariky.   Kvalifikáciu v Južnej Amerike zvládla najlepšie Brazília. Postúpili ešte Chile, Paraguaj, Argentína a Uruguaj.   Z africkej kvalifikácie sa pozrú na MS Pobrežie Slonoviny, Alžírsko, Kamerun, Ghana, Nigéria a samozrejme usporiadateľská krajina; JAR. Miestenka sa neušla takým tímom ako Togo,Tunisko, Egypt, či Senegal.   Vyžrebovanie   Postupne si prejdeme každú, skupinu tímy, ktoré v nej hrajú.   A-Skupina   Južná Afrika, Mexiko, Uruguaj a Francúzsko.   Skupinu môžeme nazvať pokojne aj skupinu smrti. Nie je podľa papierových predpokladov jasné kto postúpi a kto nie. Myslím si však, že úradujúci vicemajstri z Francúzska sa dobre pripraví a vyladí formu. Snáď sa nezopakuje situácia z MS 2002 keď sa reprezentanti galského kohúta nedostali zo skupiny a nedali ani gól! Zapracovať by mali mladíci: Ribery, Gourcuff, Benzema či Malouda.   O druhú pozíciu zabojujú podľa mňa Uruguaj a domáci usporiadateľ, teda JAR. V prospech Uruguaju hovorí neskutočná strelecká forma dvoch útočných kanónov; Suaréza a Forlána . Veď dovedna nastrieľali spolu 53 gólov v tejto sezóne. Domácich Afričanov budú vrelo povzbudzovať fanúšikovia. Samozrejme, že aj oni majú kvalitný tím veď na minuloročnom Pohári Konfederácii sa dostali až do semifinále.   Môj tip na posledné miesto pripadlo Mexiku. Už to nie je to Mexiko so skúsenými hráčmi doplnených o mladé talenty Napriek tomu stále môžu prekvapiť.   B-Skupina   Argentína, Nigéria, Južná Kórea a Grécko.   Asi nik nebude pochybovať, že Argentína postúpi z prvej priečky. Veď najlepší hráč planéty Lionel Messi valcuje každého protivníka v lige a je teda pravdepodobné, že to isté bude predvádzať aj na MS. Okrem neho tu budú mať Argentínčania k dispozícii aj ďalších superstrelcov; Milito, Agüero či Higuain. O druhé postupujúce miesto sa bude rozhodovať medzi Nigériou, Južnou Kóreou a Gréckom. Všetky tri tímy majú rovnaké šance postúpiť a tak môžu rozhodovať i malé chyby.   C-skupina   Anglicko, USA, Alžírsko a Slovinsko.   Aj tu máme hlavného favorita. Je ním Anglicko. Už dlho majú vyrovnaný tím a na posledných troch MS nechýbali v osemfinále. Snažiť sa nadviazať na túto šnúru bude aj terajší tím okolo trénera Capella. Zaobísť sa však budú musieť bez Davida Beckhama, ktorý je zranený. Nahradiť ho budú musieť Gerard alebo Lampard.   O druhé miesto budú tuho bojovať USA a Slovinsko. USA v minuloročnom Pohári Konfederácii sa dostali až do finále, kde ich zdolala Brazília. Silu Slovincov sme okúsili už aj v našej kvalifikačnej skupine, keď nás dva krát porazili.   Na posledné miesto by som zaradil Alžírsko. Na posledných troch šampionátoch síce chýbali, ale v kvalifikácii tento rok postúpili cez Egypt, čo je trojnásobných majster Afriky.   D-Skupina   Nemecko, Austrália, Srbsko a Ghana.   Favoritom je určite a nepochybne Nemecko. Na domácich MS 2006 brali bronz a na ME 2008 striebro. Na niečo podobné by chceli určite nadviazať veď majú na to manschaft hlavne útočníkov. K štandardom Kloseho a Podolskeho sa pripojili nové hviezdy Kiessling a Gomez. Navyše v bráne budú mať stabilitu vo forme Adler a Neuer. Druhý menovaný nevynechal ani minútu v tohtoročnej Bundeslige.   Ďalšie tri tímy (Austrália, Srbsko a Ghana) majú myslím, že rovnakú šancu postúpiť.   Je to veľmi podobná skupina ako B.   E-Skupina   Holandsko, Dánsko, Japonsko, Kamerun.   Prvé miesto si pravdepodobne vybojujú Holanďania. V kvalifikácii dostali iba dva góly a ich tvorcovia hry: Robben a Snejder sú vo vrcholovej forme.   Druhí budú podľa mojich predpokladov Dáni. Veď keď postúpili cez Portugalcov a Švédov tak to už niečo znamená. Prekvapiť však môžu aj Japonci či Kamerunčania. Skôr však africké levy, ktorí sa budú spoliehať na kanoniera Eto´oa.   F-Skupina   Taliansko, Paraguaj, Nový Zéland a SLOVENSKO.   Keďže tu hrajú aj naši tak si skupinu predstavíme iným kľúčom. 15.6. nás čaká zápas s Novým Zélandom. Všetci fanúšikovia si predstavujú, že to môže byť ideálny rozbehový zápas. Ale pozor! Legionári sú iba deviati hráči a práve to, že hráči, ktorí hrajú spolu v domácich kluboch sú veľmi zohratý. Výsledok tipujem 2:1 pre našich a NZ skončí podľa mňa v skupine štvrtý.   20.6 hráme možno zápas turnaja s Paraguajom. V prípravnom zápase s Chile sme zistili ako hrajú tímy z Južnej Ameriky a môžeme sa na tento zápas poctivo pripraviť. Zápas tipujem na remízu 1:1. a práve s Paraguajom sa pobijeme o postup.   Nakoniec skupinu zakončíme zápasom s favoritom turnaja: Talianskom. Víťaz MS 2006 chce určite obhájiť titul, i keď už to nie je to Taliansko, čo bolo kedysi. Poraziť by nás však mali a podľa mňa 1:3.   G-skupina   Brazília, KĽDR, Pobrežie Slonoviny a Portugalsko.   Bude to veľký súboj o prvé miesto medzi Brazíliou, Portugalskom a možno aj Pobrežie Slonoviny. Kanáriky by však mali ustrážiť prvé miesto. Hviezdy ako Kaká, Ronaldinho, ale plnú sily majú v obrane v podobe Julia Cesara v bráne.   Portugalci budú postupovať podľa mňa z druhej pozície. Oporou tímu bude určite najlepší strelec Realu Madrid Christiano Ronaldo.   Ale aj Pobrežie Slonoviny môže zamiešať karty. V tíme majú popredné hviezdy svojich klubov: Drogba, bratia Touréouci a Kalou.   Veľkou neznámou pre futbalový svet je KĽDR a teda môže aj prekvapiť.   H-skupina   Španielsko, Švajčiarsko, Honduras Chile.   Majster Európy 2008 Španielsko asi zvíťazí skupinu veď vo svojich radoch má hráčov ako Torres, Puyol, Fabregas, Xavi. Navyše v kvalifikácii nestratili ani bod! Snáď už konečne odbúra svoj syndróm MS, keď len dva krát v histórii hralo semifinále.   O druhé miesto sa pobijú Švajčiari, Hondurasania a Chile.   Dúfajme, že organizátori všetko dobre zvládnu a že si budeme môcť vychutnať dobrý a ničím nerušený futbalový zážitok.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Fuksa 
                                        
                                            Pocity z Euro 2012- čo potešilo, prekvapilo, pobavilo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Fuksa 
                                        
                                            Nikto ho nepredbehne!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Fuksa 
                                        
                                            Kanonier na pohľadanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Fuksa 
                                        
                                            Biatlonová kráľovná Anastasia Kuzminová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Fuksa 
                                        
                                            Ondrej Nepela- zázrak na korčuliach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrej Fuksa
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrej Fuksa
            
         
        fuksa.blog.sme.sk (rss)
         
                                     
     
        Som študent bratislavského gymnázia, kde píšem aj do školského časopisu. Aj v budúcnosti by som sa chcel venovať novinárstvu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    985
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Foto
                        
                     
                                     
                        
                            Športové osobnosti
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




