

 Aeróbnym tréningom (tzv. „kardio“) môžme zjednodušene nazvať akúkoľvek aktivitu v miernej zóne intenzity po dlhšiu dobu, pričom telo si vytvára energiu za prístupu kyslíka. Nepodstatná definícia. Áno, to je to vaše 45 minútové cupkanie na bežiacom páse či „mávanie rukami a nohami“ na eliptickom trenažéri už tretí rok, tri krát týždenne. Áno, hovorím o čitateľskom krúžku počas hodinového bicyklovania a pod. A stále bez výsledku. Podstatná vec. 
 Hneď na začiatku chcem citovať z článku napísaného jedným z najlepších trénerov sveta. Charles Poliquin vytváral plány pre olympijských medailistov v 17 rôznych športoch. Mal klientov z rôznych oblastí, počnúc NHL, NBA, NFL cez korporáciu Walt Disney až po ľudí z tajných služieb. Je odporcom aeróbnych aktivít. Vraví, že vytúženú postavu človek dokáže získať aj bez nich. Tu je 6 dôvodov prečo ich nerobiť (stále máš možnosť kliknúť na iný článok): 
   
 
 
Už po ôsmych týždňoch rovnakej aeróbnej      aktivity sa ďalší takýto tréning stáva kontraproduktívny. To býva dosť      veľkým „otváračom očí“ pre ľudí, ktorí si uvedomia, ako dlho možno iba      mrhali časom v ich snahe za lepším telom. 
 
Aeróbny tréning zhoršuje silu      (výbušnosť) lokálne a systematicky – inými slovami, môže Vás      spomaliť. Ak si atlét alebo „víkendový bojovník“, ktorý sa rád zúčastňuje      rôznych športových udalostí, pri rôznych tímových športoch, ktoré      vyžadujú rýchlosť alebo schopnosť skákať, toto je tá posledná vec, ktorú      chcete získať z Vášho „kardia“.
 
 
Aeróbny tréning zvyšuje oxidačný stres,      ktorý môže zrýchliť starnutie. Podľa endokrinológa Diana Schwarzbeina      (autora „Schwarzbein Principle II“), oxidácia je proces, ktorý formuje      „voľné radikály“ v tele. Telo je schopné neutralizovať voľné radikály      látkami známymi ako antioxidanty. Ak je ich však príliš veľa, telo nie je      schopné ich eliminovať všetky. To vedie k zmenám v metabolizme,      čo môže zrýchliť starnutie.   
 
 
Aeróbny tréning zvyšuje „nadobličkový“      stres, ktorý podporuje priberanie, alebo má ďalšie zdraviu škodlivé      následky. Podľa Dr. Jamesa Wilsona (autora „Adrenal Fatigue – The      21st. Century Stress Syndrome), ak niekto vykonáva nepretržité aeróbne      aktivity po dlhšiu dobu,      nadobličkové žľazy sú stresované takým spôsobom, ktorý môže narušiť      rovnováhu vo vylučovaní hormónov, čo môže viesť k únave nadobličiek.      Nadobličková únava je sprevádzaná symptómami ako: únava, úzkosť, časté      chrípky, artritída, depresie, straty pamäte, ťažkosti s koncentráciou,      nespavosť a to najdôležitejšie (a o to vlastne v tomto      článku ide), neschopnosť schudnúť aj napriek značnému úsiliu.
 
 
Aeróbny tréning zvyšuje telesný tuk u ľudí,      ktorí sú v neustálom strese tým, že jednoducho vytvára ďalší stres. Ak vykonávate príliš veľa nepretržitej aeróbnej činnosti, ktorá je pre      telo stresujúcim faktorom, iba Vás to vzďaľuje od Vášho vytúženého cieľa      stratiť telesný tuk.
 
 
Aeróbny tréning zhoršuje pomer medzi      hormónmi testosterón a kortizol, čo zabraňuje schopnosti tela naberať      tuk spaľujúce svaly. Tuk v tele je neaktívny, nespaľuje energiu.      Čistá, pridaná svalová hmota však zvyšuje výdaj tela, čím pomáha opäť      k spáleniu väčšieho množstva kalórií.
 
 
   
 Celý problém s aktivitami aeróbneho charakteru je fakt, že telo je skvelý nastroj na adaptáciu. Naše telo je dokonale pripravené a naprogramované pomôcť si v časoch krízy (škoda, že nie finančnej). Ak neješ, telo spomalí metabolizmus, aby čo najdlhšie vydržalo obdobie hladu. Ak ste smädní, telo má snahu udržať čím viac vody, alebo ju získať z iných zdrojov. 
 To platí aj pre aeróbne aktivity. Čím viac ich robíte, efektivita v ich vykonávaní sa zvyšuje. Možno sa pýtate: a čo je zlé na zefektívnení týchto činností. Efektivita je dobrá...v určitom rozsahu. Pretrvávajúce aeróbne aktivity však často krát napáchajú viac zla ako dobra. 
 Napríklad, ako začiatočník ste schopní zabehnúť jeden kilometer. Po chvíli sa telo adaptuje. Ten jeden kilometer dosiahnete oveľa ľahšie, a tak aby ste získali rovnaký benefit z daného tréningu potrebujete zabehnúť napr. dva. A tento kruh pokračuje ďalej a ďalej a telo to robí stále efektívnejšie a efektívnejšie. Aký je pôžitok v 40 minútovom behu ktorý spáli skoro rovnaké množstvo kalórií aké predtým za 30 minút? A to nehovorím o ľuďoch, ktorí nemajú chuť, energiu klásť vyššie nároky na svoje telíčko. Ale ak aj zväčšujete vzdialenosť, zrýchľujete tempo behu, telo sa dostane do stavu kedy už nebude v aeróbnej zóne a prejde do módu anaeróbneho, teda tvorenie energie bez prístupu kyslíka. Ale vtedy to už nebude aeróbny tréning. Otázka. Ak viete, že raz budete musieť prestať robiť aeróbne aktivity, aby ste dosiahli výsledky, o ktoré vám samotnou aeróbnou aktivitou ide, prečo nezačať s anaeróbnymi aktivitami hneď? 
 Dokonca nie je až tak ojedinelé aby inštruktor (napríklad aerobiku) mal nadváhu. Ide o to, že samotný inštruktor predvádza hodiny aerobiku stále dookola, takže jeho telo si už dávno zvyklo na objem a intenzitu samotného tréningu a postupom času jednotlivé hodiny nevytvárajú dostatočný stimul pre metabolizmus aby pálil tuk. 
 Náš metabolizmus rozhoduje o tom, koľko kalórií spálime každý deň. Metabolizmus je kontrolovaný a riadený našou štítnou žľazou. Avšak faktorom zostáva aj svalová hmota, ktorou naše telíčko disponuje. Aby som to vysvetlil, každý kilogram svalovej hmoty, ktorý získame, vyžaduje a spotrebuje viac kalórií na to aby sa udržal. Tuk k „životu“ nevyžaduje kalórie. Je neaktívny (ako sa už vyššie spomínalo). Čiže, aby sme získali atletický vzhľad, o ktorom snívame, rozhodujúcim nie je množstvo kalórií ktoré naše telo spáli pri tréningu ale množstvo kalórií, ktoré spálime počas celého dňa (samozrejme ktoré musí byť vyššie ako množstvo prijatých kalórií prostredníctvom stravy ale o tom nie je tento článok). Zvýšenie metabolizmu je kľúčovým faktorom pre dlhodobú stratu telesného tuku a fyzickú zmenu. Áno, hovorí sa o tom všade dookola. Každý o tom vie. No málo ľudí vie nájsť ten správny recept ako to aj urobiť. 
 Pre tých, čo nepochopili, píšem ďalej. Kľúčom je pochopiť jednu vec. Nie je možné vykonávať rovnaké aktivity stále dookola a očakávať iné výsledky. Neustále je potrebné meniť premenné akými sú doba trvania, frekvencia a hlavne intenzita samotnej aktivity.    
 Ten, kto miluje maratóny, určite nedočítal dokonca. Ani mu to neberiem. Ale vy, ktorí hľadáte spôsob, ako čo najefektívnejšie získať vytúženú postavu, či kondičku, dá sa to aj inak. Netreba pri tom hodinu sledovať vo fitku televízor, alebo čítať časopis na bicykli. Ale o tom...až v ďalšom článku. 
   
 Zdroj: 
 http://www.charlespoliquin.com/ArticlesMultimedia/Articles/Article.aspx?ID=25 
 MERCOLA Joseph, Dr., Take Control of Your Health 

