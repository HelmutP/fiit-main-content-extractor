

 Tento citát je z článku, kde budúca premiérka "ospravedlňuje" nedodržanie niektorých bodov predvolebných programov. 
 30 najzaujímavejších sľubov novej koalície, z ktorých prvých 13 je ako pohladenie duše občana. Potom sú tam ešte body 24, 25 a 28, ktoré mňa osobne zaujali. 
 Myslím si pani Radičová, že pokiaľ sa Vám podarí presadiť týchto 30 bodov do praxe, tak Vám to voliči "odpustia". 
 Ja Vám (budúcej koalícii) budem držať palce a myslím si, že naplnením týchto bodov (sľubov) dokážete aj 36 %-ám, že si túto vládu zasúži celé Slovensko. 
 Ale aby to nebolo také ľahké, tak pripomínam, že ľahšie bolo tieto sľuby napísať ako ich bude naplniť, ale budeme držať palce. 
 PS: Čo mi tam veľmi chýba, je prešetrenie a došetrenie všetkých káuz a kauzičiek za posledné roky, ktorých zoznam nájdeme aj na webe. Ak to nevyčistíme, tak ten smrad z toho bude cítiť stále, aj keď to nanovo vymaľujeme. (Klasik povedal: Smrad neprečúraš) 
   
 Foto: SME – TOMÁŠ BENEDIKOVIČ 

