
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Ráchela
                                        &gt;
                Fejtóny
                     
                 Keď je národ v puberte 

        
            
                                    27.5.2010
            o
            5:44
                        (upravené
                27.5.2010
                o
                5:52)
                        |
            Karma článku:
                9.27
            |
            Prečítané 
            929-krát
                    
         
     
         
             

                 
                    Keď po osemdesiatom deviatom začalo “národné vzopätie”, “starkoveňje” s krvou podliatymi očami v parlamente i na uliciach argumentovali, že Slováci sú najstarším národom v Európe a preto potrebujú vlastný štát. Nuž, je ťažko dokázateľné, aký je národ starý, lebo historici sú ako právnici - každý má svoju pravdu. Čosi o veku národa sa však dá zistiť z toho, ako sa správa.
                 

                 Slováci sa správajú, akoby s nimi ešte len metala národná puberta. Politické strany akoby ešte len včera odložili plienky, štát sa potáca raz extrémne doľava, potom extrémne doprava, a teraz začali vyčíňať extrémisti. To, že vyčíňajú na futbalových štadiónoch, je úplne normálne. Futbal ako šport dosť benevolentný k faulom a prerastený korupciou je priam uspôsobený na to, aby si naň chodili vybíjať pudy tie najprimitívnejšie živly a aby sa tam mladí policajti učili zvládať davovú psychózu. Veď vezmime si len takých anglických “rowdies”. (Pritom treba uznať, že Angličania sa inak správajú ako starý národ.)  Starý národ - mladý štát, potrebuje trochu zhovievavosti, - bránia “starkoveňje” našu džamahíriu. Susedné národy sa len začudovane pozerajú na Slovákov, čo to stvárajú. Ony už majú národnú pubertu za sebou. Narobili aj ony poriadne skoprciny, keď to s nimi metalo.  Maďari prešli národnou pubertou pred sto rokmi. Vtedy sa oni nevpratali do kože. Potom dostali v Trianone druhý világoš a zvyšok storočia sa správali urazene ako malé decko, keď dostane na zadok. Ale už zdospeleli a sú prefíkaní ako staré líšky. Česi sa na nás vykašľali a my sme tu, Zalitavsku, zostali trčať s našimi Maďarmi. Viktor Orbán ide národ priškripnúť reformami, preto ho musí niečím zamestnať, tak prišiel s veľkou témou národ. Súčasne potrebuje oslabiť Slovensko, preto podporil Slotu, Mečiara a Fica. V Karpatskej kotline rozohrali Maďari a Slováci veľkú šachovú partiu. Šach je hra pre ľudí s vyšším IQ. Diváci musia na šachovom zápase čušať. Extrémisti sa šachu, na rozdiel od futbalu, zďaleka vyhýbajú. Na slovenskej strane šachovnice však ťahajú figúrami ľudia, ktorí nemajú o šachu ani poňatie. Ján Slota hrá na vlastného kráľa a nevedno, či je taký hlúpy alebo je agentom najtemnejších maďarských živlov. Ľudí, ktorí nevedia hrať šach, načim od šachovnice odohnať. Nech idú na futbal. Šach nie je futbal, šach je hra pre aristokratov ducha. A my púšťame k šachovnici k rozohranej partii s Maďarmi hocikoho. Nečudo, že ťaháme za kratší koniec. Človeka by z toho trafil šľak.  Štefan Ráchela, príslušník národa v puberte. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Malé porovnanie dvoch veľkých krajín
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Ukrajina - nešťastná krajina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Nad výsledkami župných volieb v jednom kraji a v jednom okrese
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Za tú našu slovenčinu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Ráchela
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Ráchela
            
         
        rachela.blog.sme.sk (rss)
         
                                     
     
        Skeptik s kritickým myslením.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    60
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1197
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                     
                                                         
                       Július píše Oľge
                     
                                                         
                       Daňové licencie alebo škola podnikania hrou
                     
                                                         
                       Ficov veleodvážny sľub
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                                                         
                       Vďaka, herr Kotleba, za búranie mýtov o tejto krajine
                     
                                                         
                       Hnedý kôň volieb a úbožiak Fico
                     
                                                         
                       Kto za to môže?
                     
                                                         
                       Extrémizmus, moc a politická paradigma spoločnosti
                     
                                                         
                       Volíme menšie zlo, len aby sme v hanbe neostali. Už zasa
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




