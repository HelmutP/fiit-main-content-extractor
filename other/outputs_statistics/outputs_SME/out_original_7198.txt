
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Drotován
                                        &gt;
                Bratislava - Rača
                     
                 Chystá sa poslanec za Smer-SD rozkradnúť obecný majetok? 

        
            
                                    24.5.2010
            o
            19:13
                        (upravené
                25.5.2010
                o
                20:12)
                        |
            Karma článku:
                17.05
            |
            Prečítané 
            3256-krát
                    
         
     
         
             

                 
                    Písať o prešľapoch Jána Zvonára, poslanca NR SR za Smer-SD, poslanca BSK a starostu Rače je, ako sa zdá, príbehom s nekonečným počtom častí. Funkcionárovi za Smer-SD nepomohli ani dva prehraté súdne spory, keď protizákonne zakázal zhromaždenie občanov a protizákonne vypratal materské centrum. Ján Zvonár sa pravdepodobne rozhodol zapísať do histórie Rače ako starosta, ktorý bude po skončení mandátu stíhaný za zneužívanie právomoci verejného činiteľa. Chystané kšefty na zajtrajšom zastupiteľstve tomu nasvedčujú.
                 

                 
  
   Ján Zvonár sa rozhodol deň pred začiatkom zastupiteľstva informovať v oznámení, že bodom programu bude aj predaj Račianskej kúrie na Nám. A. Hlinku (áno presne na tom námestí, kde starosta organizoval stranícku akciu SNS za obecné peniaze) aj s pozemkami. Samozrejme poslanci boli pre istotu oboznámení s týmto bodom programu jeden deň pred schvaľovaním, aby náhodou nevznikla nejaká petícia resp. aby sa to všetko predalo v tichosti. Samozrejme bez verejnej súťaže, bez prerokovania v komisiách a miestnej rade. Nehovoriac o tom, že sa jedná o kultúrnu pamiatku (resp. pamätihodnosť, o ktorú sa má starať obec) a v nedávnej dobe bola kompletne zrekonštruovaná za peniaze daňových poplatníkov.   Ďalším bodom, ktorý narýchlo prepašoval do programu zastupiteľstva, je nenápadný bod 13. - predĺženie zmluvy o nájme nebytových priestorov č. 16/2008. Pod týmto nenápadným bodom sa skýva predĺženie (samozrejme bez verejnej súťaže a za výhodnú cenu) zmluvy v Račianskom zdravotnom stredisku pre jeho vlastnú firmu Eurorehab, kde je tiež zhodou okolností konateľka jeho žena, zhodou okolností tiež poslankyňa miestneho zastupiteľstva Bratislava-Rača, ktorá bude zhodou okolností hlasovať za daný bod spolu s ďalšími Smerákmi. Aby Zvonár so ženou bol zabezpečený, prenájom bude samozrejme predĺžený na 10 rokov. Samozrejme že nedávno bolo zdravotné stredisko opravené, vymenené okná a pod. za peniaze daňových poplatníkov. Cena má byť naozaj akciová - cca 20tisíc € na rok za cca 500 metrov štvorcových.   Takže otázka čitateľom - dovolia si Ján Zvonár, poslanec NR SR za Smer-SD a členovia miestneho zastupiteľstva za strany Smer-SD, HZDS a SNS rozkradnúť obecný majetok mestskej časti Bratislava-Rača zajtra na zastupiteľstve (začiatok o 16,00h. v kultúrnom stredisku na Žarnovickej č. 7)?   Aktualizácia po zastupiteľstve:   Zastupiteľstvo mestskej časti Bratislava-Rača nakoniec stihalo bod predaj Račianskej kúrie z rokovania, bod 13. však bol schválený a firma starostu Jána Zvonára získala prenájom na 10 rokov bez verejnej súťaže. Hlasovanie - za 21, proti 6, zdržali sa 2. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Odpočet práce poslanca v Rači 2010-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Politika bez kšeftov a tajných dohôd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Otvorený list primátorovi vo veci Železnej studienky a jej záchrany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Bude primátor Ftáčnik chlap alebo handrová bábika?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Ohlásenie vstupu do prvej línie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Drotován
            
         
        drotovan.blog.sme.sk (rss)
         
                                     
     
        Blogger na SME 2005-2014, nominovaný na Novinársku cenu 2012. Aktuálne som presunul blogovanie na http://projektn.sk/autor/drotovan/  
Viac o mne na: www.drotovan.webs.com 
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    224
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4323
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Poznámka pod čiarou
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Bratislava - Rača
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Mesto Bratislava
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Môj blog na Račan.sk
                                     
                                                                             
                                            Môj blog- Hospodárske noviny
                                     
                                                                             
                                            Klasický ekonomický liberalizmus ako spojenec konzervativizmu
                                     
                                                                             
                                            Salón
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            KIosk
                                     
                                                                             
                                            Knihožrútov blog
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                             
                                            Ivo Nesrovnal
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyklodoprava
                                     
                                                                             
                                            Bratislava - Rača
                                     
                                                                             
                                            Project Syndicate
                                     
                                                                             
                                            Bjorn Lomborg
                                     
                                                                             
                                            Leblog
                                     
                                                                             
                                            Cyklokoalícia
                                     
                                                                             
                                            Anarchy.com
                                     
                                                                             
                                            EUPortal.cz
                                     
                                                                             
                                            Political humor
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




