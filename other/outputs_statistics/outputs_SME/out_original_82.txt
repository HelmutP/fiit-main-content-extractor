
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomír Kopáček
                                        &gt;
                Motorizmus
                     
                 Dvesto po slovenskej diaľnici bez pokuty? 

        
            
                                    31.7.2007
            o
            13:15
                        |
            Karma článku:
                16.14
            |
            Prečítané 
            6723-krát
                    
         
     
         
             

                 
                    Ponúkam návod ako si bezproblémov zajazdiť na slovenských diaľniciach kľudne aj dvestokilometrovou rýchlosťou, dokonca tak, aby policajti dbali o bezpečnosť tvoju aj ostatných účastníkov cestnej premávky. Potrebuješ si na to len odchytiť dostatočne vypatlaného policajta so služobným autom.
                 

                  Zas jedna pekná story zo života. Idem si po bratislavskom obchvate, nudím sa v povolenej osemdesiatke. V spätnom zrkadle zbadám nový policajný Passat - nie ten smutne známy, civilný ale pekne "obrandovaný". Inak povedné biely so zeleným pásom, nápis polícia, jahodový pretlak na streche, xenóny, všetko jak má byť.     Okrem zapnutých svetiel, nemal zapnuté žiadne "typické zvukové znamenie doplnené zvláštnym výstražným červeným svetlom". Čiže to bolo v tom momente obyčajné auto, ktoré len riadil policajt. Vodič - policajt, nebol v tom momente zbavený povinnosti dodržiavať ustanovenia druhej, štvtej šiestej ani siedmej hlavy druhej časti Zákona č. 315/1996 Zb.  Na obchvate je maximálna povolená rýchlosť 80kmh, mal ju teda dodržať.  Nedodržal.     Passat ma predbehol, za ním ďalší úplne rovnaký kus. Všetky mali žilinské EČV, tak sa zdalo, že si žilinskí policajti prišli do Bratislavy pre nové hračky. A obchvat je asi dobré miesto, kde skúšať koľko sa z nového auta dá vymlátiť, dávali im pekne na knedlík...     V momente som si spomenul na ustanovenie § 37* Zákona č. 315/1996 Zb. a jediné čo ma napadlo - keď môžu oni, môžem aj ja. "Na drzovku" som sa k policajtom pridal. Votrelca odhalili okamžite a kolóna začala ešte zrýchľovať, dobehol nás v poradí tretí rovnaký Passat a zaradil sa za mňa. V priebehu pár sekúnd sme si to už valili skoro dvestokilometrovou rýchlosťou !!! Podotýkam, na osemdesiatke...  Všetko sa dá dokázať, ak existujú záznamy z diaľničných kamier, je možné zistiť kedy sa trojica žilinských Passatov nachádzala v Bratislave, nie je problém si celú situáciu overiť.      
  *§ 37 Vozidlá so zvláštnymi výstražnými znameniami  (1) Vodič vozidla, ktorý pri plnení špeciálnych úloh používa typické zvukové znamenie doplnené zvláštnym výstražným modrým, prípadne aj červeným svetlom (ďalej len "vozidlo s právom prednostnej jazdy"), nie je povinný dodržiavať ustanovenia druhej, štvrtej, šiestej a siedmej hlavy druhej časti tohto zákona. Vodič tým nie je zbavený povinnosti dbať na potrebnú opatrnosť. bla bla bla...  dovi a dopo...      Policajtov som nezaujímal, prečo ? Čítaj vyššie, asi by nebolo vhodné ma dodrbať, za to, čo sami porušovali.  Na dopravných policajtoch je fascinujúce, že oni naozaj vyhlášky a predpisy poznajú a to dosť detailne. Pozorujem, že keď si je policajt vedomý, že sám porušuje predpisy a dám mu to nejak najavo, napríklad do očí bijúcou "drzovkou", tak nič nerieši.     Čo som tým chcel povedať ? Dopravní policajti (samozrejme neplatí to obecne), sústavne, denno-denne porušujú predpisy, kde to je len možné (rýchlosť, otáčanie v križovatke, predbiehanie cez plnú čiaru, zákaz zastavenia - na čokoľvek si len spomenieme). Zneužívajú na to služobné autá a strach "civilistov" niečo im na to povedať. Zvláštnou kapitolou sú policajné autá bez označenia - meranie rýchlosti, toto sú najarogantnejší bastardi pod slnkom.  Policajta, policajt nezastaví, nemajú sa čoho báť. A inšpekčná služba ? Ale no tak... Bez dôkazov napádať príslušníkov ? To nie je pekné, oni predsa sedia v aute dvaja, a oni sú zákon, majú teda nespochybniteľnú pravdu.  To by nebolo tvrdenie proti tvrdeniu, to by bolo tvrdenie proti pravde.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (44)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Mestská polícia - my sme zákon! Ty drž hubu a plať! Časť druhá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Cola moja milovaná, kde v prdeli si?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Mestská polícia - my sme zákon! Ty drž hubu a plať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            V pazúroch mafie a výpalníkov release 2008.1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Thor Steinar = hon na čarodejnice
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomír Kopáček
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomír Kopáček
            
         
        kopacek.blog.sme.sk (rss)
         
                                     
     
        Je to blázen, hoďte na něj síť!
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5497
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezmysly
                        
                     
                                     
                        
                            Lajf stajl
                        
                     
                                     
                        
                            Blacklist
                        
                     
                                     
                        
                            Motorizmus
                        
                     
                                     
                        
                            Staré články
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nepriateľ ma jednoducho miluje :-)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Indy &amp; Wich
                                     
                                                                             
                                            Pure.FM Radio Trance
                                     
                                                                             
                                            podcast D&amp;B Arena
                                     
                                                                             
                                            iPod
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ďalší šťastlivý milionár rekonštruoval svoje sídlo :-)
                                     
                                                                             
                                            Cica Mica
                                     
                                                                             
                                            Dagmar Kopáčková (moja pokrvná príbuzná a jazykový korektor)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Hálova 7 - informačný web našej samosprávy
                                     
                                                                             
                                            Beo.sk – spravodajstvo, spoločnosť, história
                                     
                                                                             
                                            Metapedia
                                     
                                                                             
                                            Thor Steinar
                                     
                                                                             
                                            POLAR Personal Trainer
                                     
                                                                             
                                            Muscle &amp; Fitness
                                     
                                                                             
                                            BIOMag
                                     
                                                                             
                                            Campagnolo
                                     
                                                                             
                                            Root
                                     
                                                                             
                                            Google Maps
                                     
                                                                             
                                            Amateri.cz :-)
                                     
                                                                             
                                            Apple
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




