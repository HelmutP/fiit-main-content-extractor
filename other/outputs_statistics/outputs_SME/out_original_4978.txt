
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Najužitočnejšie médiá volebného obdobia: SME, Pravda, TREND 

        
            
                                    21.4.2010
            o
            18:35
                        (upravené
                22.4.2010
                o
                16:22)
                        |
            Karma článku:
                14.40
            |
            Prečítané 
            8328-krát
                    
         
     
         
             

                 
                    Ďaleko najlepšiu robotu v obrane verejného záujmu robí denník SME. Z týždenníkov TREND a z televízií Markíza.
                 

                 
 flickr-NicksNotToShabby
   Zhruba v polovici volebného obdobia v roku 2008 som sa pokúsil spočítať, nakoľko užitočné sú naše médiá v kontrole moci. Doplnil som teda kauzy za ďalší rok a pol a tu sú výsledky. Je to môj subjektívny zoznam, v ktorom som sa sústredil na exkluzívny  prínos nových tém a informácií zo strany médií. Cieľom nie je presný vedecký zoznam, ale orientačný počet pozície sily a kvality médií z pohľadu verejného záujmu. Okrem pár tém opozície a viacerých zo strany mimovládnych organizácií to boli práve novinári, ktorí najviac chránili verejnosť pred zneužívaním moci, zbytočným míňaním verejných peňazí a iným neetickým správaním politikov či iných verejne činných osôb.     Téma    Médiá, ktoré k nej významne informačne prispeli      Poľovačka št.tajomníka Záhumenského Plus 1 deň   Výpovede o korupcii vo futbale Nový Čas, Pravda, SME   Prípad Cervanová týždeň   Hedviga Malinová STV, .týždeň, SME   Rodinkárstvo pri nomináciách FNM Pravda   Arcibiskup Sokol, slovenský štát a ŠtB TA3, SME   Úplatky pri obchode so zbraňami Jahnátka TREND   Pochybné obstarávanie v zdravotníctve TREND   Dotácia pre Privilégium od Tomanovej Plus 7 dní, SME, Pravda, Nový Čas   Zákazky pre darcu Smeru Ikores Pravda, SME, Plus 1 deň, Plus 7 dní   Pozemkový fond  
 SME   Rozdelenie vplyvu v STV podľa Urbániho Sme.sk   Harabinove zneužitie služobného auta  
 Plus 7 dní   Upratovacie tendre u Kašického Pravda   Slotova kriminálna minulosť Markíza, Plus 7 dní   Tomanovej zneužitie služobného auta SME   Prevod pozemkov v Čiernej Vode SME   Tender na mýto TREND, SME   Stravné lístky pre Doxx blízky SNS  
 SME, Pravda   Skládka v Pezinku SME   Obchodné pozadie spravodajstva  v TA3 TREND   Majetok Jána Slotu Plus 7 dní   Falošný podpis Rafaja za Slotu SME   Dotácie Izáka TA3, .týždeň, Pravda   Harabin a Sadiki SME   Počiatek na jachte, nezvyčajné obchody s Sk Plus 1 deň, SME   Rozšírenie vyvlastňovania pre diaľnice skryté v zákone o cestnej   premávke Pravda   Rozdeľovanie emisných povoleniek firmám TREND, SME   Pochybní darcovia HZDS HN, SME, Markíza   Sociálne podniky Tomanovej Plus 1 deň, SME, TREND, STV 
   Lacný predaj lužných lesov pri Jarovciach Pravda, SME   Janušekov nástenkový tender Plus 1 deň, HN, Pravda, SME   TIPOS a prehraté 2 miliardy TREND, SME, Pravda, HN   Minulosť šéfa NBÚ F.Blanárika Markíza, SME   Zala na jazykovom kurze, nie v parlamente TA3   Istrokapitál a J&amp;T aktivity v Karibiku SME   Utajovanie zmluvy o mýte SME   Mučenie rómskych detských páchateľov políciou SME   Transpetrol a Ilčišin SME   Predaj emisných kvôt pod cenu cez Interblue Group TREND, SME, HN, Plus 1 deň   Pochybné tendre na ministerstve školstva Pravda, TREND, HN   Tunelovanie Štátnych Lesov SME, týždeň, TREND   Policajné zlyhanie - františkáni TREND, HN, Markíza   Tender na sanitky TREND   Majetok Vladimíra Mečiara SME, Plus 7 dní   Slotov let do Chorvátska Markíza   Falošné diplomy Trenčianskej univerzity Pravda, Markíza   Pozemkový fond II. SME   Prevod pozemkov na Bajanovu halu Pravda   Infotender na životnom prostredí (BROS) TREND   Únik informácií zo SIS Nový Čas   Prepojenie Interblue na Slovensko SME, HN   Kontrakt Columbexu na Min.pôdohospodárstva SME     Update 21.4. Oproti pôvodnému zoznamu boli po upozornení pridané ďalšie dve témy, rozdeľovanie emisií firmám a zásah polície proti františkánom.   Poradie médií (počty tém):      SME/sme.sk                             32    Pravda                                     14   TREND                                      13   Hospodárske noviny                   7   Plus 7 dní, Plus 1 deň, Markíza    6     .týždeň                                       5   Nový Čas, TA3                            3   STV                                            2     Z reportérov dominovali Marek Vagovič, Tom Nicholson a Monika Tódová zo SME, Gabriel Beer z TRENDU, a Zuzana Petková z Pravdy. Ale samozrejme na témach pracuje viacero ľudí vrátane editorov v redakciách, respektíve práca sa zadeľuje podľa témy, takže osobitný rebríček novinárov som nechcel robiť. Mimochodom, autori oboch tém STV (Korda - Malinová, Kubániová - Sociálne podniky) mali po svojich reportážach s vedením problémy a v televízii už nepracujú.   Takže novinári, vďaka!       Za SAV či za stranu?: Strana Most-Híd v ekonomickej oblasti zastupuje aj ich ekonomický expert Viliam Páleník. Tak prečo ho Daniel Horňák z TA3 neváha citovať ako nezávislého ekonóma SAV v spore o diaľničné projekty PPP? Páleník robí v tomto čase obe veci, a tak pri komentovaní politických tém nemôže vystupovať len ako „ekonóm SAV.“   Milióny, miliardy: STV si predvčerom zmýlila výšku plánovanej investície o tri nuly, upozornil ma čitateľ:   Na Záhorí možno vyrastie gigantický dopravný terminál za viac ako 37 miliárd eur.   Ak by to bola pravda, šlo by o ďaleko najväčší investičný projekt u nás, zhruba štyrikrát väčší ako postavenie všetkých diaľnic cez PPP projekty. V skutočnosti má ísť o milióny eur, nie miliardy.   Oprava 22.4. Ospravedlňujem sa, STV tú informáciu mala napriek zdaniu správne, mýlila sa TASR, ktorej správu prevzali mnohé ďalšie portály, vrátane sme.sk, hnonline.sk či pluska.sk. Chybu mi potvrdil šéfredaktor TASR Marián Kolár, podľa neho informáciu v ich servise už opravili. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (72)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




