

 Sedel som v starej spráchnivenej sedačke dožívajúceho autobusu a môj dlhostupňujúci sa pocit radosti právom vyčaril na mojej tvári vraj akýsi podivný „teplý úsmev." To, že teplý naozaj bol, bolo bezpochybné, nakoľko oknám dali závesy už dávno zbohom a my sme sa piekli vo veľkorozmernej mikrovlnke s názvom Karosa. Čerstvý tyrkysový náter zapáchajúci skrytou hrdzou a lúpajúcou sa farbou nezachránil úmorný pocit, ktorý nadobudol každý cestujúci ihneď pri nástupe. Bolo ťažké si život spríjemniť chladnou predstavou a čo i len v náznaku vnímať zarosené sklené tabule a prsty čmárajúce rozrušené obsahy myslí žiakov, ktorých mala čoskoro na zasnežených parkoch štípať zima. 
 I nás štípalo, no bol to žiaľ nepríjemný otravný hmyz, ktorý sa so záľubou lepil na spotené telá účastníkov zájazdov. Všetci sme vyzerali ako zdochliny, či páchnuce spoločenstvá ľudí s kartónovou kultúrou. O tom, že záchrana zvykne bývať nedochvíľna niet reči, no dnes sa jej podarilo prísť skôr ako začalo byť neskoro. Cestujúci pri výstupe zvádzali boje a tvárili sa, akoby autobus zastavil s dohľadom na more a tabuľou: „Kto prvý vyjde von, vyhrá zájazd pod morom." 
 Ľudia pred divadlom sa hromaždili a nervózne cupkali na jednom mieste. Na oknách vytvárali sparné fľaky, snažili sa nahliadnuť dnu a zistiť, prečo ich zatvorené dvere nechávajú ešte stále trpieť pod silnými slnečnými lúčmi ako poriadnych neplatičov. Žeby takticky ťah? Žeby pracovníci ešte zapĺňali stoly predraženými minerálkami a precvičovali si vetu: „Dodržujte pitný režim."? 
 Priestory divadla boli útulné, príjemné a navodzovali v človeku pocit, že život na chvíľu zastal. Zo stien a bežného pohľadu naokolo zmizli tváre vyretušovaných politikov sľubujúcich nemožné a nahradili ich čierno-biele postavy hercov, režisérov i dramaturgov. Vstupnú halu chladil vyhladený sivý kameň vlastniaci vysoký lesk, závideniahodný  i lakom najdrahšieho auta, zo stropu vyvieral nádherný luster zdobený kúskami ozajstného smaragdu, ktorý nazeleno sfarboval tlmené svetlo dopadajúce na húževnatý červený koberec, v ktorom boli až po členky zaborené nohy ponáhľajúcich sa hlučných návštevníkov. 
 „Dobrý deň. Z akej ste školy?" nečakane vyhŕklo zo stojacej ženy pred sálou v rovnako červenom kostýme. 
 „Dobrý. Gymnázium Prievidza." 
 „Nech sa páči do štvrtého radu vás poprosím." 
 „Ďakujem." 
 Svetlo zmizlo a objavilo sa na javisku, čo signalizovalo začiatok predstavenia. Okolosediacim stmavli tváre, keď mobily zhasli a odstrihli ich spojitosť s bežným svetom. Herci nestihli vysloviť prvú vetu, keď mňa niekto ťukal po pleci. 
 „Prosím?" otočil som sa a za mnou stála pani v červenom kostýme. 
 „Vy tu nemáte sedieť." 
 „Ako to, že nie? Pred chvíľou ste ma sem poslali." 
 „Ja? Ja nie. To zrejme moja kolegyňa. Ja sa vám ospravedlňujem, ale..." 
 „Tak jej odkážte, že vďaka nej mi kazíte kultúrny zážitok." 
 „Čo prosím?" 
 „Že mám záujem vedieť, čo sa deje na javisku." 
 Dej bol už rozohraný a ja som sa snažil doň úspešne vstúpiť a pochopiť súvis viet, ktoré sa mi dostávali bez počutia úvodných slov. Nadchýnal som sa pri pohľade na farebne zvládnuté kostýmy osvetľované ešte len bodovými svetlami, pretínajúcimi zaprášený vzduch celej sály, pripomínajúci chvíle v kine. 
 „Prosím vás chlapče, tu sedieť nemôžete." jej hlas naberal na naliehavosti a hlasitosti. Predsediaci otáčali hlavy s viditeľným záujmom zistiť, čo sa deje a nenadávať tak bez súvislosti. 
 „Akoby nie? Mám kúpený lístok." 
 „To vám verím, ale nesedíte na mieste, kde sedieť máte." 
 „Som tam, kde ma vaše veličenstvo posadilo, milá pani." 
 „Áno ste, ale byť nemôžete. Tam sedí tento pán." 
 „Nechajte ma tak. Nech si sadne na moje miesto. Kto neskoro chodí, sám sebe škodí. Dovi." 
 To už príbeh na javisku stál, keď som si uvedomil môj kričiaci hlas. Lúč patriaci ešte pred chvíľou hercom, spravil účinkujúceho zo mňa. Mohutný chlapík, ktorému vraj patrila sedačka, na ktorej som sa práve nachádzal, pristúpil ku mne, chytil ma za rameno, potiahol košeľu a behom chvíle som mával nohami vo vzduchu ako kaskadéri pri dlhých pádoch z vysokých budov. Hnev sa premenil na strach, srdce tĺklo ako bubon zahĺbeného šamana pred rituálom a kvapky potu na čele mi pripomenuli vriace chvíle v autobuse. Lenže teraz nehorel vzduch, ale krv v žilách celého publika. Rozšíril sa šuchot, buchot, výkriky a urážky, ktoré zahasili až hlasy vraj pohotových hercov. Čakal som rýchlejšiu improvizáciu. Nechali, pokiaľ sa všetci nasrdili, mňa spoznali ako tvrdohlavého drzáňa a uverili slovám „nenávisť na prvý pohľad." 
 „Vážení návštevníci. Ospravedlňujeme sa za drobné nedorozumenie, ale bol to náš zámer a súčasť scenáru. Chceli sme vás mierne pripraviť na nasledujúcu scénu, ktorou začína naše predstavenie. Tá potrebuje pozorného diváka a práve pri hádke pracujú človeku všetky zmysli naplno. Prajeme vám príjemné a nikým nerušené predstavenie." 
 Stál som sa obeťou výstrelku. Stál som sa na chvíľu populárnym. Škoda, že mojich 5 minút slávy bolo premrhaných práve teraz a takto. 
  
 Foto: Ľubica Martincová 

