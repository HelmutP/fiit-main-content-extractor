
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrej Hajdusek
                                        &gt;
                Himalayas tour 2007
                     
                 V zovretí cudzieho sveta (Údolia národa Kalašov) 

        
            
                                    26.3.2008
            o
            21:24
                        |
            Karma článku:
                8.59
            |
            Prečítané 
            2359-krát
                    
         
     
         
             

                 
                    Stačí prejsť jeden z dvoch mostov niekoľko desiatok kilometrov na juh od mesta Čitrál v severozápadnom Pakistane a ocitnete sa v odlišnom svete farebných ženských šiat, odlišných zvykov a viery. Títo svetlí ľudia, ktorí majú pramálo spoločného s okolitým svetom Islamu, tiahnúceho sa od Turecka až po východ Indie, obýva tri neveľké údolia blízko Afganských hraníc.
                 

                 
  
    Muzikáá k čítaniu (Candie Payne)      Obyvatelia údolí Rumbur, Bumboret a Birir sú viditeľne nemoslimským národom. Ich odlišnosť priťahovala odjakživa misionárov, antropológov a zvedavých turistov. Ženy sa obliekajú do nápaditých farebných šiat, zdobené ozdobami a čipkami, zatiaľ čo muži nosia bežné pakistanské šaty - shalwar kameez, ktoré pozostávajú z voľných nohavíc a dlhej košele. Obkolesení drsnými horami (ktoré sú mimochodom domovom aj snežného leoparda) sa v malých údoliach snažia odolávať moslimských tlakom z vonka.       Ešte v meste Čitrál pri povinnom evidovaní na policajnej stanici, som si vypýtal miesto pobytu našich dvoch známych – Španiela Josepha a Angličanky Melanie. Pri večery nám navrhli, aby sme sa s k nim na nasledujúce 3-4 dni pripojili. Ten večer sme poblúdili uličkami nočného Čitrálu, vychutnali si ovocné koktejly a nasledujúce ráno sme prekročili rieku Čitrál spoznať malý národ, ktorý sa hlási k predkom armády Alexandra Veľkého.       Zatiaľ čo náš džíp pomaly napredoval horskou cestou skrz úzke kaňony s ľadovcovou riečkou pod nami, sme míňali na ceste afgánske ženy v burkách a ich mužský sprievod. Pripomínali, že afganské hranice sú, čo by človek kameňom dohodil a počas problémových období Afganci hojne využívajú horské priesmyky na prekročenie do susedného Pakistanu. Väčšina najhlavnejších priesmykov je po 11.sept. v okolí síce kontrolovaná jednotkami ako Chitral Scouts a inými, ale vždy sa nájdu miesta kde sa dá nepozorovane prejsť na druhú stranu.       Na prvé Kalašské ženy s nezvyčajnými vzormi a ozdobami sme narazili pri práci na políčkach. V údoliach sa pestuje obilie, proso, kukurica a šošovica, ale jedna plodina tu má iné zameranie. Je to hrozno, ktoré premieňajú na hroznové víno a to ďalej pália na tvrdý alkohol. Tieto údolia sú jediným miestom v Pakistane, kde sa legálne podomácky vyrába a pije alkohol. Nečudo, že množstvo pandžábis mieri na tieto miesta hlavne kvôli nemu.       Bola to príjemná zmena po dlhých týždňoch sa znova pohybovať medzi nezahalenými ženami. Už hneď prvý deň pobytu v najväčšom z troch Kalašských údolí, sme sa pridali k našej španielsko - anglickej dvojici si prezrieť obydlia tohto malého národa. Kalašské drevené domy sú postavené terasovito. Strechy jednotlivých domov slúžia ako otvorené terasy pre tie, ktoré sú nad nimi. Dostali sme pozvanie do domu starostu. Hlava rodiny bola práve odcestovaná, ale jeho usmievavá žena nám ponúkla miestne víno a dobré pálenuo. Od odchodu na svoje cesty sme tak všetci štyria ochutnávali ohnivú vodu po dlhých týždňoch, čo malo svoj zvyčajný efekt. V očiach domácej aj z jej správania bolo hneď vidno, že sa nenachádzame v okolitom moslimskom svete. Bola z nej cítiť hrdosť a pri komunikácií s ostatnými pandžábis teda Pakistancami, ktorí si sem prišli kúpiť vypálený alkohol, bolo badať pohŕdanie. Dole v dedine keď sme míňali mladé kalašské ženy, sa takmer všetky otáčali Pakistancom chrbtom akonáhle videli ich nasmerované fotoaparáty.       Nasledujúci večer po celodennej túre sme sa zúčastnili nacvičovania na nadchádzajúci miestny sviatok. Mladé dievčatá sa vzájomné držia rukami a v kruhu spievajú. Ich spev má vskutku svoju krásnu mytologickú silu. Len si ľahnúť na trávu poblíž, vyvaliť sa pod nebeskú klenbu... Realita však bola taká, že návštevníci – Pakistanci ich prekrikovali. Mamička slonej váhy stále strkala svoju malú dcéru niekde medzi dievčatá a fotila. Mladí, pakistanskí chlapci pobehovali medzi nimi a narážali zakaždým do nich. Ich rodičia sa nad tým len pousmievali. Všetci štyria sme pozorovali na túto scénu jemne vyjavení. Osobne som sa čudoval, prečo si Kalašovia nechajú takto skákať po hlave, avšak najsvätejších sviatkov sa Pandžábis nesmú zúčastňovať. Zahraniční turisti majú povolenie sa prizerať, len ak sa vopred dohodnú s domácimi.       Pôvod tohto malého národa sa jednoznačne nepodarilo preukázať. Sami sa považujú za potomkov jedného z generálov Alexandra Veľkého, ktorý sa v týchto miestach usadil s časťou armády. Genetické štúdie skôr potvrdili názory, že Kalašovia patria do Indo-Európskej skupiny – západo Eurázijského pôvodu (analýza Y chromozónu naznačuje 20-40% spojitosť ku gréckemu pôvodu).       Viera a zvyky sa úplne odlišujú od okolitého sveta. Hlavný boh alebo tvorca sa nazýva Dezau (Indo-Európsky boh oblohy – Dyaos, neskorší Grécky boh Zeus a latinsky Jupiter). Okrem neho uctievajú množstvo božstiev, polobohov a duchov. Náboženské festivaly zohrávajú v ich živote dôležitú úlohu. Niektorí turisti kvôli hlavným festivalom preletia pol planéty, len aby sa mohli zúčastniť týchto osláv.       Medzi moslimami sú skôr známi ako Kafirs, alebo neveriaci. Donedávna platilo pravidlo, že ak nejaký Kalaš konvertuje na Islam, je okamžite vylúčený zo spoločenstva. Ale realita je v posledných rokoch taká, že už takmer polovica Kalašov sú moslimovia a naďalej prebývajú v údoliach. Pri jedle som sa niekoľko krát zarozprával v hoteli s mladým chalaniskom, ktorý je poverený digitalizovať kultúru, zvyky, legendy a rozprávky svojho spoločenstva. Na otvorenú otázku, či má jeho národ šancu prežiť obkolesení týmito silnými tlakmi, sa na chvíľku zamyslel a len ticho pokrútil hlavou.      Národ Kalašov dnes prežíva v malých údoliach v častiach, ktoré patria v islamskom svete medzi najkonzervatívnejšie. Časť Pakistanu známa ako Severozápadné pohraničie (North West Frontier Province) je z veľkej časti mimo kontroly Mušaráfa. Blízka hranica Afganistanu, neustále migrujúci uprchlíci, ktorí sa usadzujú v blízkych údoliach neveštia pre budúcnosť tohto malého spoločenstva nič priaznivé. Pri stopovaní ďalej na juh sme mali možnosť sa zoznámiť s Angličankou, ktorá dlhé obdobie svojho života zasvätila práve tomuto národu. O tejto pani a mnohých iných zaujímavostiach a tajomstvách, ktoré blízke okolie skrýva popíšem v ďalšom blogu...           Pár fotiek z cesty (ak seká z prava v Mozille treba prepnúť na internet explorer)               Miestni polísmeni na prvom checkpointe                                           So Španielom Josephom a Angličankou Melanie                           Mamka sa mračí na Pakistanca po mojom ľavom boku a malé na ďalšieho na pravo                            Výdobytky techniky a úsmevy domácich                           Rozhádzané rakvy na miestnom cintoríne                                                                 Slovač a Kalašač - Nájdite rozdiely                                                                                                             takmer všetci žijú v skromných podmienkach                           Tradičnú čiapku severných oblastí Pakistanu nosí aj tento malý národ. Chalanisko v žltom mi tu svoju podaroval.                           Keď pani na obrázku nechápala z blond vlasov                                                  S naším vodičom pri tradičných domoch v údolí Bumboret                            Cez sedlo do susedného údolia                                                                                              Osamelá hora - sedemtisícovka Tirich Mir                            a za tými horami už je Afganistan                                                 krajina snežného leoparda                                                  mladí pastieri v tiesňavách na ceste do susedného údolia                           aké kamene, taká koza                                                 medzi políčkami                     v susednom údolí                                        Pandžábovia po stopovaní                       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Hajdusek 
                                        
                                            Letná stopovačka do strednej Ázie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Hajdusek 
                                        
                                            Leto 2008 – za horsk&amp;#253;m v&amp;#225;nkom &amp;#193;zie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Hajdusek 
                                        
                                            A cesta spať -  smer Európa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Hajdusek 
                                        
                                            Príbehy z North - West Frontier Province
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Hajdusek 
                                        
                                            Od Malých Karpat i od Tatier
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrej Hajdusek
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrej Hajdusek
            
         
        hajdusek.blog.sme.sk (rss)
         
                                     
     
        poďme všetci do lesa...
Feedjit Live Website Statistics
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    55
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2567
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Aljaška 2005
                        
                     
                                     
                        
                            Sev. Afrika a Stredný východ '
                        
                     
                                     
                        
                            Írsko 2006 - 2007
                        
                     
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Himalayas tour 2007
                        
                     
                                     
                        
                            Ázia leto 2008
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje foto albumos
                                     
                                                                             
                                            Môj profil aj ostatne moje oblubene web linky najdete tu
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




