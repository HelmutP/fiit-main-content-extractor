

 Debaty po filme The Age of Stupid sa zúčastnil  prof. László Miklós, minister životného prostredia v rokoch 2002-6, Jaroslav Jaduš, štátny tajomník MŽP SR, Juraj Zamkovský z organizácie Priatelia Zeme-CEPA, a ja. 
   
 Všetkým diskutujúcim sa film páčil, a všetci ho (aspoň podľa slov) považovali za pomerne hodnoverný opis toho, čo ľudstvo čaká, ak nezmení svoju doterajšiu trajektóriu (film končí jadrovou vojnou v roku 2055). Všetci sme sa zhodli aj na tom, že nie je možné, aby naďalej rástli globálne emisie skleníkových plynov, najmä CO2. Samozrejme, jedna vec je identifikácia problému, ale celkom iná vec je identifikácia riešenia. To sa, nie prekvapujúco, prejavilo aj v diskusii. 
   
 Samozrejme si z diskusie nepamätám všetko, pokúsim sa zreprodukovať len udalosti, ktoré ma prekvapili najviac. Začnem od konca. 
   
 Na záver diskusie nás jedna z diváčok filmu poprosila, aby sme sa vyjadrili ku svojej vlastnej uhlíkovej (ekologickej) stope, a vyjadrili sa, čo robíme pre jej zníženie.  
   
 Prof. László Miklós: „...moja ekostopa je 3,5... už sa ale presne nepamätám, čo je to za číslo. ...treba mať na pamäti, že ľudia v Afrike nemajú o stave životného prostredia žiadne alebo len minimálne vedomosti o životnom prostredí.... a keď do Afriky príde biely muž, správajú sa rovnako, ako my... neexistuje legislatíva, ktorá by donútila ľudí, aby sa správali ekologicky... environmentálna výchova má iba malý, alebo žiadny dopad na správanie ľudí...  
   
 (na záver sa ukázalo, že prof. Miklós výpočtom uhlíkovej, či ekologickej stopy neverí a neskôr si spomenul, že číslo 3,5 znamenalo počet planét, ktoré by sme potrebovali, ak by žil každý ako on. Nebolo jasné, čo robí preto, aby minimalizoval svoju ekostopu, pravdepodobne preto, že jej výpočtom neverí) 
   
 Jaroslav Jaduš: ...nezáleží, akú mám uhlíkovú stopu, podstatné je, že robím maximum preto, aby som ju znížil... dôležité je si uvedomiť, že ak svoje uhlíkové stopy neznížia Američania, alebo Číňania, je jedno, čo urobia Európania... musí ísť o globálnu dohodu... nevidím dôvod, prečo by mala byť EÚ lídrom boja proti klimatickým zmenám...   
   
 (pán Jaduš zjavne chápe globálne súvislosti, čo už ale chápe menej je historická zodpovednosť za emisie skleníkových plynov, zodpovednosť jednotlivcov, teda emisie na obyvateľa, a tiež zrejme nechápe ani to, že ťažko možno od Číňanov čakať znižovanie emisií, ak ich uhlíková stopa je niekoľkonásobne nižšia, ako ekostopa Európanov. Tiež predpokladám, že jeho vlastná stopa mnohonásobne vyššia, ako stopa priemerného Slováka (tá je 7,7 tony CO2/rok). Nakoniec u pána Jaduša vysvitlo, že nás zachránia „technológie budúcnosti“ ako napr. jadrová fúzia, či elektromobily.). 
   
 Juraj Zamkovský: ...nemám auto a ani vodičský preukaz, dom vykurujem iba drevom.. 
   
 (uviedol aj konkrétnu hodnotu svojej ekostopy, ako aj svoju dlhoročnú profesionálnu činnosť v spoločnosti CEPA, ktorá sa okrem iného venuje osvete a projektom ohľadom obnoviteľných zdrojov energie na regionálnej úrovni, a mnoho ďalších vecí.) 
   
 Ja: ...uhlíková stopa tvorí asi 60 % ekologickej stopy, a moja ekostopa je 2,5 hektára, čo znamená, že pri mojom životnom štýle potrebujeme 1,3 planéty.... auto nemám, a ani neplánujem, možno v budúcnosti elektromobil, ale k tomu pravdepodobne nedôjde. Do roboty jazdím na bicykli a využívam prevažne hromadnú dopravu (autobusy, vlaky). V BA bývam v zateplenom paneláku s rodičmi a v Brne v podnájme so spolubývajúcimi. Neplánujem si brať hypotéku... mäso jem častejšie ako sa žiada, ale keď si dám vegetariánsku stravu, obvykle mám rýchlo hlad... pár krát som letel lietadlom, zväčša na vedecké konferencie... 
   
 V diskusii pán Jaduš presadzoval výstavbu diaľničných obchvatov, ako spôsob znižovania emisií z dopravy (naozaj!!). Pretože autá jazdia po diaľnici účinnejšie ako po meste. To je nepochybne pravda. 
   
 Tu som parafrázoval výrok Alberta Einsteina, podľa ktorého „nemôžeme riešiť problémy tým istým prístupom, ktorý tieto problémy spôsobil“. Tým som chcel poukázať na to, že zvýšenie účinnosti spotreby zdrojov vedie v konečnom dôsledku ku zvyšovaniu ich celkovej spotreby (viď „Jevonsov paradox“). Poukázal som tiež, že zvýšenie účinnosti parného stroja Jamesom Wattom odštartovalo exponenciálne využitie uhlia a tým aj priemyselnú revolúciu. To isté platí, ak niekto argumentuje, že „šetrenie“ vedie ku znižovaniu emisií. Tento argument ignoruje to, že ľudia primárne „šetria“ preto, aby mohli peniaze minúť niekde inde. Jediný spôsob definitívneho zníženia a zastavenia emisií je zastavenie výstavby uhoľných elektrární (ani technológia CCS nezníži emisie uhoľných elektrární na nulu), zastavenie ťažby ropy a zemného plynu, a zastavenie odlesňovania. K tomu všetkému môže prispieť uhlíková daň, ale ak sa priamo zakáže ťažba uhlia, alebo ropy, nie je potrebná. 
   
 Je tiež „zábavné“ tvrdiť, že o 20-30 rokov budú nové nízko-uhlíkové technológie (znovu pán Jaduš), keď z filmu vyplynulo, že o stave planéty rozhodnú udalosti uskutočnené do roku 2015. Aktuálnym riešením nie sú ani elektromobily, keďže sú drahšie, menšie, pomalšie, a zatiaľ bez podpornej infraštruktúry. Ich podiel na automobilovej doprave tvorí menej ako 1 %. Navyše, leteckú, ani lodnú dopravu nie je zatiaľ možné elektrifikovať. Ak ale po cestách v budúcnosti autá budú jazdiť, budú to iba elektromobily. 
   
 Celkovo som bol s diskusia dosť sklamaný. A preto: 
 „The Age of Stupid Goes On“! 

