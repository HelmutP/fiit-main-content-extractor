

 1.       Simon Sinek a jeho úžasná prednáška bola inšpiráciou pre prítomných na TEDxPuget Sound a od minulého týždňa aj pre celý svet vďaka zverejneniu na TED.com hlavnej stránke. Simon nabáda ľudí na to, aby poznali svoje PREČO, prečo robia to čo robia. Od toho sa až potom odvodí AKO a ČO bude produktom toho vášho PREČO.  Aj ja som venovala Simonovi včerajší  blog príspevok a pridávajú sa ďalší ľudia: aj Garr Reynolds kvituje Simov prejav. Garr  Reynolds, je autorom knihy Presentation Zen (vyšla už aj v češtine) a je top svetovým profesionálnom na dizajn prezentácií. (ak je súčasťou vašej práce prezentovanie a nechcete, aby váš powepoint uspal publikum, odporúčam si pozrieť Garrove práce na slideshare.net) 
 No, ale späť k Simonovi, nech sa páči 18 minút o dôležitosti prečo treba začať s PREČO. 
   
 










 
   
 2.       Na tému Grécko sa hovorilo v poslednej dobe viac ako dosť, tento 8 minútový prejav ma veľmi zaujal. Pozrite si to a posúďte sami. (via Robert Paterson) 
 






 
   
 3.       Na odľahčenie ma pobavil tento študentský rap, ktorý sa páčil aj Chrisovi Broganovi. Chris, je bloggerská celebrita v USA, jeho štedrosť a ochota pomáhať nemá hraníc. Odporúčam sledovať Chrisov blog, jeho tipy a rady pre marketing 21.storočia nenájdete v žiadnych Kotlerových učebniciach. 
   
 






 
   
 4.       Téma školstva a spôsob vyučovania, ma trápi zaujíma už dlhšiu dobu , Dan Meyer je proaktívny učiteľ matematiky a blogger v USA. Jeho prezentácia pre TEDxNYED  je  „must watch" nielen pre učiteľov matematiky. V  dnešnej dobe nie sú žiadne limity na to, ako možno využiť technológie na školách (okrem ochoty tvoriť, inovovať a dávať niečo navyše, samozrejme). Halóooo učitelia, foťte, používajte videá, obrázky, preformulujte problém, zapojte študentov do dialógu. Možno prvé video od Simona Sineka o otázke PREČO robíte, to čo robíte vám pomôže nájsť nové spôsoby ako inšpirovať vašich študentov, ak nie tak možno by ste mali zmeniť povolanie. 
   
 










 
   
 5.       Posledný spot na tento týždeň je z Japonska a dáva pohľad na to ako môžu vyzerať naše ďalšie interakcie vďaka technológiám. (via B.L. Ochman's blog.) 
   
 





 
 N Building from Alexander Reeder on Vimeo. 
   
 Krásny víkend plný inšpirácie vám želám. 
 i. 
   

