
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nikolas Winkler
                                        &gt;
                TOP
                     
                 NHL TOP 10 - Najväčšie sklamania 

        
            
                                    16.3.2010
            o
            10:16
                        (upravené
                16.3.2010
                o
                10:51)
                        |
            Karma článku:
                9.60
            |
            Prečítané 
            3419-krát
                    
         
     
         
             

                 
                    Vravelo sa o nich ako o nástupcoch najlepších hráčov. Dnes sa o nich už nevraví. Niektorí akoby sa vytratili z povrchu zemského. Toto sú najväčšie sklamania histórie.
                 

                 10. Kyle Chipchura /Kanada/ Draftovaný v roku v 2004 Montrealom Canadiens z 18.miesta    Predpovede skautov boli priaznivé. Ide o talentovaného defenzívneho útočníka. V prvom  kole draftu po ňom siahol Montreal a vo svojej prvej sezóne začal ako z partesu. Nazbieral jedenásť bodov a ďalej už nič. Postupne prestal hrávať v prvom tíme a sezónu dokončil na farme. Dva ďalšie roky nedal gól a tak putoval do Anaheimu. Kyle je ešte mladý a má šancu ešte využiť svoj potenciál, aj preto  je na 10.mieste rebríčka.       9. Wade Belak /Kanada, nar 1976/ Draftovaný v roku 1994 Quebecom Nordiques z 12.miesta    Belak nebol považovaný za hráča, ktorý by v kariére mal prepisovať štatistiky. Čakalo sa však viac. Oveľa viac. Po vstupe do NHL dal jeden gól za 6 sezón. Posledných 5 rokov čo hrá nedal žiadny. Celkovo za 531 zápasov skóroval len 8-krát. Dnes je už považovaný za bitkára. Na hráča z dvanásteho miesta draftu - krutý osud.       8. Andre ´Red light´ Racicot /Kanada, nar 1969/ Draftovaný v roku 1989 Montrealom Canadiens z 83.miesta    Bol to slabý brankár. Pri bilancii 17 výhier-5 prehier-1 remíza /sezóna  92/93/ znie až neuveriteľne, že jeho percentuálna úspešnosť bola len 88 %  Montreal v tom čase ťažil najmä zo svojho útoku, a dával našťastie veľa gólov. Racicotov priemer obdržaných gólov na zápas činil 3.50. Svoju kariéru v NHL ukončil ako 24-ročný a potom sa už len potuloval po nižších súťažiach. V čase svojej najväčšej ´slávy´ mu prischla výstižná prezývka - ´Červené svetlo´       7. Jason Doig /Kanada, nar 1977/ Draftovaný v roku 1995 Winnipegom Jets z 34.miesta    Bol ozdobou juniorských súťaží. Odborníci mu predpovedali že ak bude na sebe pracovať môže sa stať moderným obrancom, ktorý vie rovnako dobre brániť ako útočiť. Nič z toho sa nenaplnilo. V NHL hrával sporadicky. Na ľade pôsobil komicky a kluby si ho ´nenápadne´ podsúvali. Vystriedal teda veľa tímov či už v najvyššej alebo v nižších ligách. A hoci vo Washingtone Capitals hrával často, herne nepresvedčil a po lock-oute sa už v NHL neukázal.     6.Hardy Astrom /Švédsko Draftovaný nebol.    Hovorí sa o ňom ako o najhoršom brankárovi aký kedy nastúpil na zámorskyćh klziskách NHL. Niet divu, že ho prezývali ´Švédske sito´. Jeho bilancia v kariére činila chudobných 17 výhier 44 prehier a 12 remíz. K tomu pridajme takmer 4 inkasované góly na zápas. V 30-tich rokoch ho v NHL už nikto nechcel.         5. Patrik Stefan /Česko, nar.1980/ Draftovaný v roku 1999 Atlantou Trashers z 1.miesta        V tomto prípade odborníci chválou nešetrili. ´Najtalentovanejší útočník českého hokeja v novodobej histórii´. Aj taký prívlastok dostal tento rozohrávač. Mala to byť hviezda ligy formátu Jagra. Ale nebola. Patrik sklamal na celej čiare. Bodovo dosiahol len povesť priemerného hráča, a nikto nevedel prečo sa mu to stalo.Kariéru ukončil vo veku 27 rokov.       4. Dave Chyzowski /Kanada, nar 1971/ Draftovaný roku 1989 New Yorkom Islanders z 2.miesta    Mal povesť strelca so zabijáckym inštinktom. ´Rekordy traste sa´ zaznelo pri vstupnom drafte roku 89. Po vstupe do ligy dal za 2 sezóny 13 gólov. V nasledujúcich štyroch ročníkoch skóroval už len dvakrát. Hral čoraz slabšie a slabšie a slabšie a slabšie...až jedného dňa sa už slabšie hrať nedalo a v NHL skončil.       3. Jason Bonsignore /USA, nar. 1976/ Draftovaný roku 1994 Edmontonom Oilers zo 4.miesta    Teraz zbystrite pozornosť. To bolo chválospevov a rečí o tom aká hviezda smeruje do ligy. ´Nový Mark Messier´ a podobné výplody fantázie. V tom čase to samozrejme nikto nemohol vedieť. Za 4 sezóny odohral 79 zápasov. Skóroval len trikrát. Po tomto deprimujúcom zážitku ohlásil že si chce od hokeja odpočinúť. Vynechal teda dve sezóny a v roku 2002 sa zúčastnil tréningového kempu Atlanty Trashers, ale to už nemal žiadnu šancu. V NHL si už nikdy nezahral.       2. Alexander Svitov /Rusko, nar 1982/ Draftovaný v roku 2001 Tampou Bay Lightning z 3.miesta    Draft roku 2001 bol braný veľmi vážne pretože z prvej priečky išiel do ligy Ilya Kovalchuk, z druhej Jason Spezza a z tretej práve Svitov. Išlo o veľmi veľké talenty. Svitov však nebodoval, miesto toho veselo zbieral trestné minúty a z konštruktvneho hráča sa stal deštruktívny buldozér. Herný prejav prakticky žiadny, rovnako tak žiadny zmysel pre fair-play. V roku 2006 sa teda porúčal naspäť domov do Ruska, pričom v NHL zaznel po jeho odchode hlasitý smiech.       1. Alek Stojanov /Kanada, nar 1973/ Draftovaný v roku 1991 Vancouverom Canucks zo 7.miesta    Najlepší vtip na záver. V roku 91 bola hokejová Kanada na nohách pretože do NHL vstupoval z prvého miesta Eric Lindros - absolútna extratrieda a vo veku 18 rokov hotový hráč. Stojanov bol postavou rovnako veľký ako Lindros. Vedelo sa o ňom ale nebol tak na očiach. To bolo brané ako mierna výhoda. Po Alekovi okamžite siahli zástupcovia Canucks a začalo sa potľapkávanie po pleciach, nekonečné podávanie rúk a kajanie sa akýže to výborný krok spravili. Začali tak písať históriu najväčšieho pádu jedného z najtalentovanejších hráčov kanadského hokeja. Talent bol jediné čo tento hráč mal.Pri prvom zápase ho slávnostne predstavili publiku, ktoré mu samozrejme pekne zatlieskalo. Vo svojej prvej sezóne odohral 4 zápasy v ktorých nebodoval a následne bol poslaný na farmu. Tam vôbec neoslnil, ale vzhľadom na to že mal v tom čase 21 rokov sa to dalo prepáčiť. V nasledujúcom ročníku už bol zaradený do prvého tímu Canucks a čakalo sa že to ´rozbalí´. Odohral porciu 58 zápasov. Vedenie bolo znechutené pretože Alek nedal ani gól, v kolónke mal len jeden bod za asistenciu. Bolo nutné konať. Okamžitá výmena do Pittsburghu! U Penguins strávil svoje posledné dve sezóny. Ako 24 ročný v NHL definitívne skončil s bilanciou 107 zápasov - 2 góly - 5 asistencií. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Jeden na jedného: Forsberg vs Lindros
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: USA
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: Česká republika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: Fínsko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Marián Hossa hviezdou NHL? Ani náhodou.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nikolas Winkler
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nikolas Winkler
            
         
        nikolaswinkler.blog.sme.sk (rss)
         
                                     
     
        Žijem v USA. Mojou najväčšou záľubou je hokej. Hlavne severoamerický.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    55
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2121
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            TOP
                        
                     
                                     
                        
                            Recenzie a analýzy
                        
                     
                                     
                        
                            Hráči
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




