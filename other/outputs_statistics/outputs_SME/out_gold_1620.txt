

  
 Základný kameň kostola v najmladšej obci okresu Bytča bol posvätený v roku 1990 
  
 Krížová cesta začína práve pri kostole a stúpa popri chodníku na priľahlý kopec 
  
  
 Výjavy zastavení sú vsadené do nahrubo opracovaných skál 
  
  
 Na niektorých miestach tvoria schody drevené trámy 
  
  
  
 Chodník stúpa, zastavenia pribúdajú. A výhľady, ktoré sa naskytajú poza stromy, stoja za to! 
  
 Hlboké nad Váhom pod Súľovskými skalami 
  
  
  
 Na vrchole kopca stojí popri poslednom, štrnástom zastavení, drevený kríž 
  
 Krížová cesta vznikla nedávno. Kaplnku Božieho hrobu posvätil 12. júla 2009 pri príležitosti osemstého výročia prvej písomnej zmienky o obci generálny vikár Žilinskej diecézy Mons. Ladislav Stromček, za účasti veriacich a predstaviteľov štátnej a miestnej samosprávy. 
  
 Kaplnka bola vybudovaná miestnymi veriacimi pod vedením dekana vdp. Pavla Trnku a starostu obce Dušana Pinčíka. 
  
  
 Výhľady z vrchola Kalvárie sú možné na všetky štyri svetové strany... 
  
   

