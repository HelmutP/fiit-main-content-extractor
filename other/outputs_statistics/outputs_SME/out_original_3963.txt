
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Adriana Markovičová
                                        &gt;
                Čo mi napadlo
                     
                 O sviatkoch jari, iskrách a žltej malinovke... 

        
            
                                    5.4.2010
            o
            18:32
                        |
            Karma článku:
                13.61
            |
            Prečítané 
            3013-krát
                    
         
     
         
             

                 
                    Väčšina oficiálnych jarných sviatkov mi k srdcu nejako neprirástla. Neviem, čo robiť s Veľkou nocou, medzinárodné dni hocičoho a hocikoho ma desia svojím presne stanoveným termínom a oslavovať sviatok práce povaľovaním sa v posteli je od nás celkom dobrá irónia. Neviem, či ma to ospravedlní pred očami verejnosti, ale zdrojom tohto postoja je okrem iného aj protichodná výchova vštepovaná od útleho detstva.  Začalo to povinnou školskou dochádzkou...
                 

                 
  
   Môj iskričkový vedúci sa volal Šaňo, mal asi pätnásť a v prvej triede ZŠ som sa za neho plánovala vydať napriek tomu, že podobné úmysly mala i moja spolužiačka z lavice  iskra Danka a okuliarnatá iskra Luca z druhej lavice. Danka sa však v cez prázdniny odsťahovala do Čiech a Lucu uniesol otec kdesi do Izraela (aspoň tak sa to o nej šepkalo v súdružskoučiteľskom zbore), takže mi obe chtiac-nechtiac uvoľnili cestu k láske. Je hádam jasné, že som bola na pioniersky sľub pripravená najlepšie z celej triedy.  Skladala som ho sotva päť hodín po prvom svätom prijímaní.   Viete si predstaviť ten zmätok v duši deväťročného poslušného dieťaťa?   V škole som patrila do kategórie „samé jednotky", visela som na slovách súdružky učiteľky ako včely na medovom pláte a nedokázala som si predstaviť, žeby šlabikár nemal pravdu, keď tvrdil, že „zhora sa na teba, iskra moja, díva súdruh Vladimír Iľjič". Tak som si súdruha Iľjiča predstavovala aj na tom kríži, ktorý bol namaľovaný v kostole. V tretej triede ma totiž moja mama prihlásila do nedeľnej školy. Pokúšala sa zo mňa vychovať kresťana, lenže na jednej strane tu bol zväzák Šaňo, čo ma so šarmom poďobaného adolescenta učil „domovina moja krásna", a na strane druhej kňaz, ktorý navštevoval našu rozvedenú susedu a občas sa opil tak, že jej vykopával dvere. Ten istý človek potom v nedeľu rozdával svojim malým ovečkám pečiatky do zošitka (obávam sa, že som tam chodila hlavne kvôli nim). Musela som sa učiť naspamäť slová, ktorým som nerozumela, zatiaľ čo iné deti behali po dvore s loptou a hrali sa „kráľu, kráľu, daj vojačka". Malá čierna knižka mi tvrdila, že boh je len jeden, a to otec, syn a duch svätý, a nech som rátala ako rátala, stále mi to vychádzalo, že sú traja. Modlitby som sa učila rovnako poctivo ako ruské básničky - foneticky a nezrozumiteľne. "Vľisú radíla sjólačka, v ľisú aná raslá" a „ako vnebi takinazemi dajnám chlieb"... toto mi znelo v hlave ako ktorákoľvek odrhovačka z rádia, ktorej sa celý deň neviete zbaviť.  Termín pionierskeho sľubu bol určený celkom škodoradostne, teda v rovnakú májovú nedeľu ako prvé prijímanie. Červenokravatoví páni však neboli veľmi dôslední, lebo oba „sviatky" sa dali stihnúť.   Májová nedeľa 1979, dopoludnie.  Stojím v krásnych bielych princeznovských šatách pred oltárom a tajne dúfam, že sa mi tá oblátka neprilepí na jazyk, lebo ak sa tak stane, bude to s najvyššou pravdepodobnosťou nejaký veľmi ťažký hriech (v hriechoch som mala a doteraz mám veľký chaos, poznám ich najmä vďaka krváku s názvom Sedem, ktorý s náboženstvom nemá vôbec nič spoločné, akurát sa tam sériový vrah a režisér celkom vyšantil).    Popoludnie toho istého dňa, niečo po tretej.  Mám na sebe čerstvo vyžehlenú modrú košeľu a minisukňu z hrubej nepoddajnej látky, o chvíľu mi môj idol Šaňo na krk nešikovne uviaže červenú umelohmotnú šatku (vydržala len jedno použitie, kto mal vedieť, že sa nemá žehliť na vysokej teplote).  Večer oba sviatočné okamihy oslávime v rodinnom kruhu, nadlávime sa tortou a dospelí sa opijú na počesť Iljiča i Ježiša.  Netuším, že od tohto okamihu už Šaňa nikdy neuvidím a do kostola ma už tiež nikto nedostane.   Veľká noc bývala pre mňa iná trauma. Vždy vo Veľký piatok sa nás naša mama zo zotrvačnosti pokúšala týrať bezmäsitou stravou, no my sme si z toho robili srandu, najmä keď sa v školskej jedálni podávalo kura. Chodilo sa normálne do školy, akurát dni mali smiešne farebné názvy. Tie sa mi páčili. K zelenému štvrtku a bielej sobote som si vymyslela žltý utorok, aby ani tento deň neprišiel skrátka. Na čo som sa tešila, boli nedeľné raňajky. Šunka, klobása, vajíčka, cvikla a zvláštna žltá hmota so sladkou vianočkou vo mne vyvolávali naozaj božské pocity, bol to dôkaz, že takúto dobrotu môžu zariadiť len anjeli.  A potom prišiel čierny pondelok. Nepripadalo mi vôbec smiešne, že nám so sestrou otec vylieval hrnček teplej vody do postele. Pani Jar by sa asi veľmi čudovala rituálu „poď sa postaviť nad vaňu, nech nenamočíme koberec", na základe ktorého mala oficiálne prísť do našich končín. Potom sa tato ako jediný mužský člen domácnosti na veľké naliehanie mamy pobral na povinnú návštevu príbuzných, zatiaľ čo ja som sa dívala na iného rodinného priateľa, ktorý sa u nás sťal pod obraz boží skôr, ako stihol ofrckať ženy voňavkou so živými konvalinkami vo flakóniku (podozrievala som tie konvalinky, že sú umelé). A tak sa naše veľkonočné sviatky zakaždým vznášali v opare alkoholických a aromatických výparov a najviac som sa tešila na poludnie, keď sa oficiálne skončila oblievačka a ja som si mohla zmyť vlasy. Najhoršie bolo, keď prišli aj spolužiaci zo školy. Jeden z nich, budúci vynálezca, si vyrobil zvláštny prístroj, ktorým striekal vodu z nádoby presne na cieľ. Keď sme sa so sestrou zamkli v izbe, strčil hadičku do kľúčovej dierky a pokúšal sa nás cez ňu trafiť. Sviatok jari...   Prvý máj, niekedy okolo roku 1982.  Stojím v nových sandálkach na chodníku a rátam, koľko je tam prilepených žuvačiek. Moji pojašení spolužiaci sa pokúšajú šermovať červenými zástavkami a ja sa snažím, aby mi nimi nevypichli oči. Za dve hodiny sa rad pohne asi o dva metre. Súdružka učiteľka toho má plné zuby, najradšej by nás všetkých priviazala, aby sme si neublížili. V pouličnom rozhlase počuť nadšeného moderátora rečniť o tom, že súdruhovia od vysokej pece pozdravujú prvý máj a že si akási brigáda socialistickej práce práve dala záväzok prekročiť plán o 120 percent. Naša súdružka učiteľka si zase dáva tabletku na upokojenie. Keď sa sprievod z našej ulice pohne, nevieme sa dočkať, kedy sa dostaneme k tribúne a rozhlas ohlási "našich" pionierov. Nejaký prebudený zväzák však máva veľkou zástavou s Marxom tak vehementne, že na tribúnu tak či tak nevidíme. Na križovatke máme rozchod a ja si utekám do stánku kúpiť za 80 halierov žltú malinovku v igelite, aby som si ňou neskôr mohla nové tlačiace sandálky pooblievať (skúste strčiť jednou rukou slamku do sáčku plného tekutiny bez toho, aby nevystrekla, pričom druhou rukou si tú sladkú hrôzu pridržiavate tak, aby vám na zem nepadlo mávatko s červeno-modro-bielymi papierovými pásikmi).   O pár rokov neskôr na gymnáziu sme tie pochody trénovali vopred cez telocvik. Súdruh telocvikár stál na švédskej debnobedni a zhora kričal: „Dievčatá! Ľavá noha nevie, čo robí pravá, a aj medzi nimi máte zmätok!" Chichotali sme sa, všetko nám vtedy bolo smiešne a hlavne nám odpadla fyzika, a keď išlo do tuhého, aj ruština - učitelia si nemohli dovoliť vypustiť na ulicu stádo gymnazistov, ktorí si stále plietli poradie deklamovaných hesiel a kričali „nech žije prvý máj" namiesto „nech žije KáeSČé".  Sviatok práce...   Začiatok marca, hociktorý rok.  Klinček s červenou stužkou ledabolo strčený pani učiteľke do ruky. Veľkolepé oslavy organizované ROH, metále odovzdávané dojičkám v Televíznych novinách na prvom programe. Po nich nasleduje medajlón Valentiny Tereškovovej, prvej kozmonautky a trinásty diel Ženy za pultom, ktorá s láskou naberá parížsky šalát na tanierik vraviac: "Hliníkovou lžičku si vemte támhle." Muži tackajúci sa pod rúškom tmy z práce,  zatiaľ čo ich manželky už dávno vyzdvihli deti zo škôlok a navarili večeru. Čo vám to pripomína? Sviatok žien...   Nie, netvrdím, že sviatky sú zbytočnosť. Majú svoj význam v poriadku sveta, tradície vyvolávajú spolupatričnosť medzi ľuďmi, ktorí si želajú všetko dobré alebo spoločne spievajú nejakú pieseň. Lenže každý sviatok je takým, akým ho urobia ľudia okolo nás. V mojom živote sa niektoré z nich akosi pokazili...   Niekto zvoní. Prišiel kuriér s kvetmi.  Deti mi dali dlhý bozk na líce.  Mám asi desať telefonátov, žiaden nie je z daňového úradu, ani z banky.  Deň bláznov, môj sviatok...   Teraz naozaj prišla jar.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (96)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Markovičová 
                                        
                                            Knižný konšpiračný koktail alebo čože to matky na severe miešajú deťom do mlieka?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Markovičová 
                                        
                                            Vtedy a dnes: Star Trek (1966 - 1969)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Markovičová 
                                        
                                            Vtedy a dnes: Sissi (1955 - 1957)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Markovičová 
                                        
                                            Jozef Bednárik. Niet čo dodať.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Markovičová 
                                        
                                            Čítanie tejto knihy môže poškodiť vaše duševné zdravie!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Adriana Markovičová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Adriana Markovičová
            
         
        markovicova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Nie ten je bohatý, kto má najviac, 
ale ten, kto potrebuje k životu najmenej. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    206
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    9384
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rozchodológia
                        
                     
                                     
                        
                            Zápisky zblúdeného motovidla
                        
                     
                                     
                        
                            Vtedy a dnes
                        
                     
                                     
                        
                            Čo mi napadlo
                        
                     
                                     
                        
                            Čo ma štve
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Spôsob ako prežiť
                        
                     
                                     
                        
                            Moje malé potešenia
                        
                     
                                     
                        
                            Neverte ničomu
                        
                     
                                     
                        
                            Zverejnené v tlačenom Sme
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Trochu narcizmu
                                     
                                                                             
                                            Prehľad článkov Vtedy a dnes
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Najlepšie slovenské rádio
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tu ma teraz nájdete
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Martinus Cena fantázie
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




