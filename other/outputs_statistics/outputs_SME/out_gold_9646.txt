

   
 Ja som zašiel do záhrady, vzal som si aj platenú tašku. Vyleziem na strom a trhám jablká. Pozriem sa do druhej záhrady a vidím dievča, čo sa objíma s orechom. Brunetka, jej vlasy sú kučeravé a trochu rozhádzané. Má na sebe krémovú sukňu a mätovú blúzku. Lusknem prstom, spadne jej tá sukňa a ja sa zaujato pozerám. Samozrejme, že sa tvárim ako nejaké neviniatko, martýr. Dievča sa chytro obzrie, orech jej prezradí, kto to bol. 
   
 Jabloň, na ktorej som sa začne hrozne rehniť. Chechce sa, až sa jej konáre otriasajú. Nemusím vám pripomínať ako som dopadol. Zletel som zo stromu a odrel som si koleno i lakte. Jablká pristáli pri mne a niektoré ostali na mojej hlave. Pomaličky vstávam, ale čosi mi v tom bráni. 
   
 Všímam si len žihľavovú sukňu a jej nohy. Ani si nevšímam, že akási zlatá čepeľ je na mojej hrudi. 
   
 „Nevieš o tom, že čarovanie je teraz nebezpečné?" Hovorí. Ten meč je zvláštny, jakživ som taký nevidel. Čepeľ je zlatá, hrot čepele má červenú farbu. Šípim, že je tam jed. Rukoväť má na sebe čosi napísané, pripomína mi to staré, dračie písmo, ktoré poznám len z legiend. 
   
 „Nevyzeráš ako inkvizítor." Chechcem sa. Vtedy si pozriem jej oči, hnedé ich má. Raz som také oči videl, keď som sa pozrel do vody. (Pamätám sa ako som sa raz rozprával so škriatkom pod Bukovým lesom. Hovoril mi, že na svete existujú iba dva páry očí, ktoré sa musia nájsť.) 
   
 „Si podarený ako včera. Bolo celkom príjemné, keď sme kŕmili kačky a voňali púpavu. Ale teraz, teraz mám svoj meč na tvojej hrudi." Povedala Druidka a ja som sa pozrel na ňu ako včera. 
   
 V celej záhrade je ticho, Druidka sa nežne usmieva a svoj meč s dračím jazykom už pustila. Vlastne sedíme pri sebe, kolena sa nám dotýkajú a ja obdivujem jej meč. (Teda to sa aspoň tak tvárim.) Cítiť tu typické sobotné popoludnie v záhrade. 
   
 Listy počujú čo si s Druidkou šepoceme, okolo dedkovho dvora počuť kondášov a iných pastierov. Niektorí idú so sviňami, iní s dobytkom, či ovcami a kozami. Jahody mi voňajú, jabloň sa ešte stále smeje a z čerešní sa hráme na slová. Zrazu počujem ako v stajni zaerdží kôň. Orechové stromy mi určujú smer do stajne a tak sa tam s Druidkou dostavím. 
   
 Kôň sa poteší jablkám, Druidka mu pohladí šiju i chrbát. „Jasné, opäť sa flákaš za ženskými." Odvetí Kôň, zaerdží a napije sa vody. 
   
 „Neprebehneme sa na ňom?" Opýtam sa druidky, vďačne súhlasí. 
   
 Sadne si prvá a ja za ňu. Samozrejme, preto, aby som sa jej dotkol a mohol jej voňať žihľavové vlasy. Neprotestuje, možno jej to aj lichotí. Jej voňavé vlasy s ňou robia divy, so mnou tiež. 
   
 Kôň sa začal rozbiehať, miluje, keď môže lietať. Ja sa pozerám na stromy, na farebné čerešne a zatúlané fialkové lúky. Čas od času jej dám pusu, aby mi nepovedala, že som ľudožrút a trielime ďalej. Zastavíme na lúke a necháme Koňa, nech sa napasie. 
   
 Sedíme na lúke, kde je vyschnutá tráva, nejakí šarvanci mlátia slamu a kdesi v diali cítiť seno. 
   
 „Keď chceš, poviem ti legendu o drakovi, čo nevyšiel z jaskyne už 1000 rokov." Začne druidka. Pošteklím ju a ona sa ku mne pritúli. 
   
 „Hlboko v lese je jaskyňa. Nenájdeš ju, lebo nie je otvorená, zasypalo ju lístie. V jaskyni býva drak, ktorý zanevrel na ľudí. Draci sú v okolitých dedinách uznávané ako múdre stvorenia, tu to nie je výnimkou. Príbeh o tom drakovi je o jeho múdrosti a o neľudskosti a krutosti hlúpych ľudí.  
   
 Draci žili v lesoch a pre lesné tvory boli neuveriteľným darcom ohňa. Vychádzali spolu priateľsky ako dobrí susedia. Čas od času sa našla v lese nejaká príšera, s ktorou si draci merali sily. Lesní Ľudia drakom robili rôzne zábaly, elixíry na ich rany. Niektorí čarodejníci sa priatelili s drakmi, vlastne aj môj otec. Bol druid a neraz s týmto drakom, o ktorom je reč viedli mnoho debát o lese, stromoch, ľuďoch a tak.  
   
 Drak ho mal strašne rád, aj keď mu to nikdy nepovedal, ostatne môj otec bol taký istý. Potom si ľudia urobili nejaký stupeň hodnôt, čo uctievali. Spravili si kráľa a všakovakých rytierov, ktorí nemali nikdy staroveké právo meča.  
   
 V tom čase môj otec a Drak si vymysleli tajný jazyk, ktorým sa dorozumievali. Hovorí sa mu Dračí jazyk. Málokto ho ovláda a ľudia ho už vonkoncom nevedia.  
   
 Jedného dňa sa kráľova manželka dostala do lesa a začala páliť včelie úle. Aby si nezabudol, včely majú magickú moc. Aj v ich úľoch je mnoho mágie, ale proti ohni sú bezmocné. Preto nikdy od drakov nepotrebovali ich pomoc.  
   
 Tá kráľova manželka smrdela ako bzdocha, bola krásna ako kikimora a mala srdce ako červy. Ona pálila včelie úle a vrieskala po celom lese. Bola opitá, až cez 3 hory ju bolo počuť. Počula to skupina lesných tvorov. Druidi sa k nej pridružili, čarodejníci vo svojich plášťoch, škriatkovia so svojimi jablkami a trpaslíci so svojimi zlatými sekerami. Vtedy sa tá hétera, švandra a bridivá bzdocha spamätala. Vedela, že by si rozhnevala celý les a preto ušla k svojmu manželovi a povedala mu to.  
   
 Dvaja láskyplní, zamilovaní manželia zabudli na jednu podstatnú vec. Vedel o tom aj Drak, ktorý začal konať." Dopovedala, lebo som zaspal. Už je sobotný večer, ale o ňom nabudúce. 
   
 Zbierať jablká, počúvať starú legendu, jesť jahody, cítiť seno a túliť sa k dievcunke. To je pre mňa sobota poobede. 
   
 Zdroje obrázkov: &lt;http://upload.wikimedia.org/wikipedia/commons/a/a1/Rhoen_Bergwiesen_mg-k.jpg&gt; [cit. 2010-06-26] 
 &lt;http://upload.wikimedia.org/wikipedia/commons/0/03/Flood-meadow_near_Hohenau.jpg&gt; [cit. 2010-06-26] 
   

