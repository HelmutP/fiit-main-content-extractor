
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                              iHRisko - HR blog
                                        &gt;
                odborným okom
                     
                 Arogancia uchádzača vo výberovom procese 

        
            
                                    2.1.2008
            o
            10:11
                        |
            Karma článku:
                11.14
            |
            Prečítané 
            8358-krát
                    
         
     
         
             

                 
                    Výberový proces je častokrát zdĺhavý sled udalostí pozostávajúci z niekoľkých krokov (výberových kôl). Uchádzač o zamestnanie sa však neraz diskvalifikuje už v jeho „ranej“ fáze bez toho, aby tušil pravý dôvod svojho zlyhania. Jedným z častých dôvodov býva jeho nadmierna arogancia.
                 

                  Zdravá arogancia = dynamické sebavedomie  Pri obsadzovaní niektorých pozícií môže zdravá arogancia uchádzačovi pomôcť. Skôr by sme ju však v tomto prípade mohli nahradiť pojmom dynamické sebavedomie. (Ide najmä o pozície, kde je arogantné vystupovanie často základom úspechu, napr. obchod. I tu však treba dbať na to, aby arogantné a sebavedomé správanie neprekročilo akceptovateľnú hranicu a nezamedzilo tak napr. uzavretiu obchodu či rozviazaniu obchodného vzťahu.)   V prípade, ak kandidát disponuje bohatým spektrom skúseností, môžeme tvrdiť, že jeho sebavedomé vystupovanie hraničiace niekedy s aroganciou je na mieste. Častým úkazom výberového procesu je však prípad, kedy kandidátove sebavedomie a arogantné vystupovanie neodrážajú skutočný stav jeho nadobudnutého know – how.    Ako zo seba urobím arogantného hlupáka  „Všade som bol, všetko som videl!“. Presne týmto heslom sa riadi arogantný kandidát, tzv. vševed. Neustále vám skáče do reči, nenechá vás dokončiť vyslovené myšlienky, má neuveriteľnú chuť stále vás dopĺňať. Skúsený personalista sa nenechá vyviesť z miery a podobného kandidáta „usmerní“. Niekedy je dokonca namieste poskytnúť mu spätnú väzbu okamžite, priamočiaro, bezprostredne na pohovore. Kandidát si tak možno sám uvedomí, čo by mal na svojom správaní zmeniť. A ak to nezmení dlhodobo, minimálne nebude tieto prejavy správania realizovať na vašom pohovore (respektíve v menšom množstve). Niektoré personálne agentúry v dnešnej dobe dokonca dávajú kandidátovi jasne najavo, „v čom bol problém“ a položia mu podmienku. Pokiaľ sa bude rovnako správať i naďalej, na (spolu)prácu môže zabudnúť.             Som najlepší, vezmite ma!  Personalistom nie je cudzí typ kandidáta, ktorý tvrdí o svojich schopnostiach (často najmä tých jazykových), že sú „... na veľmi dobrej úrovni, skvelé, vynikajúce“ a pod. V praxi sa potom personalista stretne s prípadom, kedy vedie pohovor s človekom, ktorý ani zďaleka na danú pozíciu jazykovo (či inak) nedozrel. Spomínam si na prípad, kedy slečna, vyštudovaná germanistka (kde predsa len predpoklad požadovanej jazykovej schopnosti bol) vo svojej záverečnej reči tvrdila, že ako „zamestnanecký benefit“ by radšej využila jazykový kurz z angličtiny, nakoľko jej „... nemčina je perfektná, tú ona zlepšovať nepotrebuje.“ Dôvodom pre jej odmietnutie bola ale jej nedostatočná znalosť práve nemeckého jazyka. (Tento problém tiež úzko súvisí s aktuálnym stavom vysokého školstva v našej krajine, čo nie je predmetom článku.) Aby sa zamedzilo plytvaním času (našim i kandidátovým), mnohé spoločnosti v dnešnej dobe zaviedli systém tzv. telefonickej preselekcie zameranej práve na selekciu uchádzačov na základe jazykových schopností.       Čo s arogantným kandidátom?  Skúsený personalista sa zväčša s arogantným kandidátom „pohráva“. Jednou z možností, ako priviesť osobný rozhovor ku koncu (možno pre kandidáta aj úspešného) je jednoznačne mu naznačiť, kto je „na ihrisku pánom“, poskytnúť mu spätnú väzbu a veriť, že kandidát si ju vezme k srdcu. Rozumný kandidát na sebe zapracuje, dostane druhú šancu a napokon možno uspeje. (V praxi som sa stretol  tiež s prípadom, kedy sa uchádzač na prvom kole javil ako „ideálny“, na druhom však nečakane a nevysvetliteľne zlyhal. Úplne zmenil svoje správanie na arogantné a výsledkom bolo jeho odmietnutie. Komunikoval som priamu a pravdivú spätnú väzbu, pozval ho ešte na jedno kolo pohovoru a napokon bol prijatý. Dodnes ho stretávam na chodbách našej spoločnosti.)    Ak je však uchádzač pre spoločnosť už od začiatku „stratený“ a personalista vie, že v ďalšom procese pokračovať nebude, môže sa na pohovore chovať tak, aby mal kandidát pocit svojej dominancie a „majstrovstva“. V každom prípade by bolo pre takéhoto kandidáta osožné počastovať ho spätnou väzbou aspoň na záver výberového procesu, pri telefonickej komunikácii výsledku. (Komunikácia spätnej väzy je však veľmi citlivá záležitosť, najmä keď sa jedná o kandidáta, ktorý si jej obsah  nezoberie k srdcu a spoločnosti narobí veľa razy negatívnu, neopodstatnenú reklamu. Personalista si musí preto dobre rozmyslieť, čo bude komunikovať.)      Záverom  Vo výberovom procese spoznávame množstvo osôb, množstvo osobností. Povahové črty a prejavy správania jednotlivých uchádzačov sú rozdielne, nie vždy natrafíme na „ideálneho kandidáta“. Súčasťou každodenného života personalistu je aj kandidátova arogancia. V málo prípadoch opodstatnená, väčšinou však nadnesená a prehnaná. Je iba na samotnom človeku, ako sa po komunikácii pravdivej spätnej väzby s daným nedostatkom svojej povahy vyrovná a vysporiada. Niekedy v budúcnosti by mu arogancia totiž mohla stáť v ceste za svojím vysnívaným zamestnaním.           Matej Šemšej     autor je personalistom v IT spoločnosti   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (60)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          iHRisko - HR blog 
                                        
                                            HR vs. IT
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          iHRisko - HR blog 
                                        
                                            Ako ne(s)klamať na interview
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          iHRisko - HR blog 
                                        
                                            Prelom v HR - Personálna inzercia 2.0
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          iHRisko - HR blog 
                                        
                                            Ako sa (ne)obliecť na pohovor
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          iHRisko - HR blog 
                                        
                                            Osobnostné testy nezaručujú kvalitný výber zamestnancov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera:   iHRisko - HR blog
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                  iHRisko - HR blog
            
         
        ihrisko.blog.sme.sk (rss)
         
                                     
     
        miesto stretnutí ľudí, ktorí sa chcú deliť o skúsenosti s prácou s ľuďmi, miesto pre personalistov, HR manažérov, trénerov a VÁS!
Autor blogu 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5397
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            na voľnú tému
                        
                     
                                     
                        
                            odborným okom
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




