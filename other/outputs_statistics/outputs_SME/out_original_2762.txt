
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                              katarinka
                                        &gt;
                Čo nám zíde na um, keď sa povi
                     
                 Čo pre mňa znamená Katarínka? 

        
            
                                    17.3.2010
            o
            0:06
                        (upravené
                30.3.2010
                o
                11:02)
                        |
            Karma článku:
                4.87
            |
            Prečítané 
            1565-krát
                    
         
     
         
             

                 
                    Neviem či sa dá slovami vyjadriť čo pre mňa Katarínka znamená, ale tak sa o to aspoň pokúsim. Keď väčšina ľudí počuje slovo Katarínka, predstaví si nejaké dievča, ktoré sa volá Katarína. No u mňa je to iné. Ja si nepredstavím žiadnu osobu, ale v mysli sa mi zjaví obraz časom zruinovaných, no ešte vždy majestátnych a krásnych múrov zrúcanín Kláštora a Kostola sv. Kataríny Alexandrijskej. V mysli vidím nádherne zelené lúky, rozľahlé lesy, jednoduchý drevený kríž, staré vojenské stany, ohnisko a malú kuchynku s pieckami na drevo, čistý prameň a tváre mnohých ľudí, ktorých s vďačnosťou nazývam priateľmi.
                 

                 Projekt Katarínka som spoznala asi pred dvoma rokmi, keď som sa prihlásila na letný tábor, ktorý sa koná každý rok na Katarínke počas celého leta. Úprimne, veľmi som sa tam bála ísť. Nevedela som si predstaviť, ako strávim dva týždne mimo civilizácie. Najviac som sa však bála ako si budem rozumieť s úplne cudzími ľuďmi. No spolužiačka, ktorá Katarínku veľmi dobre pozná ma presvedčila so slovami, že Katarínka bola jedna z najlepších vecí, ktoré jej život priniesol. Sama som bola prekvapená, ako rýchlo som si to miesto a tých  "cudzích"  ľudí zamilovala. Celé miesto dýcha históriou a dlhoročnými modlitbami. Je to miest, kde nájdem napriek veľkému zmätku v sebe, vždy pokoj a porozumenie. Nemusím sa na nič hrať, bez obáv môžem byť sama sebou so všetkými svojimi pozitívami aj negatívami. Neviem, čím to je, ale nikde sa mi nedarí objavovať samú seba tak, ako na Katke. Vždy sa dozviem o sebe niečo nové. A nikde inde nie sú také krásne hviezdy ako tam :). Keby som to mala zhrnúť do jednej vety, Katarínka mi dala nádherné priateľstvá, pomohla mi nájsť záľubu vo varení, pomáha mi nájsť samú seba a vždy tam nájdem pokoj, ktorý mi tak veľmi chýba doma. Som tam jednoducho šťastná. Aj keď je pravda, že to, čo vyhovuje mne, nemusí iným, myslím si, že KTO NEZAŽIJE, NEPOCHOPÍ! Katarínka to nie je len tak, to je už navždy...       Terézia Letenajová 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                          katarinka 
                                        
                                            Čo sa stane, keď piliere nemusia držať strechu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          katarinka 
                                        
                                            Prestaňme nadávať na politiku, vyberme sa na Katarínku (foto).
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          katarinka 
                                        
                                            Vybielená Katarínka (foto)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          katarinka 
                                        
                                            Poézia v podaní tohtoročných novicov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          katarinka 
                                        
                                            Kto sa skrýva za kolesom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera:   katarinka
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                  katarinka
            
         
        katarinka.blog.sme.sk (rss)
         
                                     
     
        Projekt Katarínka - venujeme sa záchrane ruín barokového Kláštora sv. Kataríny Alexandijskej pri Dechticiach v Malých Karpatoch a všetkému čo súvisí s poznaním histórie a s dnešným rozvojom tohto výnimočného miesta.  
Viac na www.katarinka.sk 
Fotografie z tohto miesta nájdete na:
www.katarinka.sk/sk/foto
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1165
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Čo nám zíde na um, keď sa povi
                        
                     
                                     
                        
                            Nejaká tá báseň...a či úvaha?
                        
                     
                                     
                        
                            Naše workshopy
                        
                     
                                     
                        
                            Čosi na úvod
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prechádzka Podhradím
                     
                                                         
                       Kňažkova politická samovražda
                     
                                                         
                       Jedno veľké ďakujem
                     
                                                         
                       Vybielená Katarínka (foto)
                     
                                                         
                       Poézia v podaní tohtoročných novicov
                     
                                                         
                       Špinaví cigáni!
                     
                                                         
                       Kto sa skrýva za kolesom?
                     
                                                         
                       Potulky jesenným pokojom
                     
                                                         
                       O tradícii cestovania autobusmi 31 a 39
                     
                                                         
                       Katarínka otvára svoje nedochované brány
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




