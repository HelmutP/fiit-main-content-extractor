
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tibor Pospíšil
                                        &gt;
                Financie
                     
                 Do Grécka po modrej 

        
            
                                    24.6.2010
            o
            8:13
                        (upravené
                23.6.2010
                o
                20:54)
                        |
            Karma článku:
                5.96
            |
            Prečítané 
            1639-krát
                    
         
     
         
             

                 
                    Lepšie sa cítim v pozícii pesimistu, pretože ten má väčšiu šancu byť príjemne prekvapený. Populizmus, s akým išli do volebného súboja „pravicové" strany dával tušiť, že aj modrá  bude po voľbách viesť do Grécka. Preto som sa rozhodol volieb nezúčastniť. Kdesi v kútiku duše som si želal, aby som toto svoje rozhodnutie oľutoval. Aby som bol príjemne prekvapený. Ale všetko nasvedčuje tomu, že mi to nebude dopriate.
                 

                 Predvolebné silné populistické a nezodpovedné reči by sa ešte dali ospravedlniť ešte populistickejším súperom. Ale keď aj najdynamickejšia strana priamo vo svojom programe napíše, že je svojimi 120-timi nápadmi bude naďalej zaťažovať budúce generácie ... to už dávalo tušiť, že to „pravica" myslí skutočne takto nezodpovedne.   Povolebná realita grécky scénár potvrdila. Dane sa zvyšovať nebudú, sociálne dávky či dôchodky sa obmedzovať nebudú. U veľkých projektov sa síce predpokladá preverenie ich skutočnej ceny, ale ak sa dosiahne „transparentná" cena, tak sa budú realizovať, aj keď ich budú splácať ešte naši pravnuci. No a privatizácia ... zabudnite. Štát je zrejme zlý vlastník len vtedy, ak je červený. Pre modrý, „pravicový" štát to už zrejme neplatí. Ani s rušením teplých miestečok to nebude nejaké horúce.   Vzhľadom na vývoj ekonomickej situácie nielen na Slovensku o štyri roky zvíťazí väčší populista. A modrí to nebudú.  A preto sa zrejme dočkáme obrovských nákladov nevyužitej príležitosti. Modrá vláda, namiesto síce veľmi tvrdého, ale zodpovedného 4-ročného vládnutia sa bude pokúšať vládnuť tak, aby nestratila podporu. Absolútne naivné, pokrytecké a mocibažné.   Áno, budú slušnejší, budú menej kradnúť. Aj o niečo málo zodpovednejší možno budú. Ale to je málo. Zúfalo málo. Čísla nepustia. Tento rok budú príjmy štátneho rozpočtu vo výške cca 11 mld. euro a výdavky cca 16 mld euro. To je deficit 45%. Budúci rok to nebude oveľa lepšie. Takže za približne 30 mesiacov modrá vláda vyrobí deficit na úrovni 100% ročných príjmov. Viete si predstaviť, že by ste podobne hodpodárili so svojim rodinným rozpočtom? Že za 2,5 roka miniete toľko, čo zarobíte za 3,5 roka? Čo myslíte, koľko by ste takto mohli fungovať? Tak prečo si práve takúto správu želáte u štátneho rozpočtu?   Pre budúcnosť to vidím dosť čierne: dva súperiace bloky sociálnych demokratov. Jeden vulgárno-zlodejský, druhý slušnácko-intelektuálny. Ale vzor majú americký.   Na odvrátenie štátneho bankrotu nestačí slušná vláda. Pri akutálnom dlhu Slovenskej republiky vo výške temer 3-násobku jej ročných príjmov sú potrebné razantné a veľmi nepopulárne opatrenia. Ale tých sa aj vzhľadom na želania modrej voličskej základne nedočkáme.       P.S.: na znižovanie dlhu nestačí ani vyrovnaný rozpočet. V rozpočte totiž nie je ani euro určené na splátky dlhu. V rozpočte sa počíta len s platením úrokov. A rozpočty s prebytkom 10% by splatili existujúci dlh za 30 rokov.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (46)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Nekonečné čakanie na ropný zlom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Drahí „liberáli“, od detských izieb našich detí si držte výrazný odstup
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Ako cestovať lacno a pritom pohodlne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Aktuálne udalosti očami anarchistu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Keď sa somrák a príživník stane celebritou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tibor Pospíšil
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tibor Pospíšil
            
         
        pospisil.blog.sme.sk (rss)
         
                        VIP
                             
     
        Léta už tajím svoji příslušnost,
k zemi co vyměnila hrubost za slušnost,
bo se mi nelíbí morální předlohy,
čili papaláši hrající si na Bohy. 
...
Upřímně řečeno trochu teskno mi je,
že nám stačí DEMO-Demokracie.
ˇ(c) Tomáš Klus






        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    245
                
                
                    Celková karma
                    
                                                8.64
                    
                
                
                    Priemerná čítanosť
                    4152
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Menej štátu
                        
                     
                                     
                        
                            Túlavé zápisky
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Financie
                        
                     
                                     
                        
                            Finančná poradňa
                        
                     
                                     
                        
                            Zábavná logika
                        
                     
                                     
                        
                            Nebuď ovca
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Hovorme viac o rasizme Rómov
                                     
                                                                             
                                            Pokora libertariánů a domýšlivost sociálních inženýrů
                                     
                                                                             
                                            Ti dole nevolí o nic hloupěji než ti nahoře
                                     
                                                                             
                                            S eurovalom na večné časy
                                     
                                                                             
                                            Uhni mi z cesty, politik…
                                     
                                                                             
                                            Duševné vlastníctvo nie je vlastníctvo
                                     
                                                                             
                                            Válka právníků s ekonomy
                                     
                                                                             
                                            Nie som ovca
                                     
                                                                             
                                            Ako funguje mediálna demokracia
                                     
                                                                             
                                            Finančná kríza: dôsledok viditeľnej ruky štátu
                                     
                                                                             
                                            Kde udělali soudruzi z USA chybu?
                                     
                                                                             
                                            Verejné školstvo bolo vždy zdrojom útlaku
                                     
                                                                             
                                            Čo všetko je možné publikovať a predsa by publikované byť nemalo
                                     
                                                                             
                                            O slovenskom orlovi :-))
                                     
                                                                             
                                            Aj môj osobný spor s moderným liberalizmom
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Menej štátu, viac slobody
                                     
                                                                             
                                            Zaujímavý finančník
                                     
                                                                             
                                            Skutočne VIP blog
                                     
                                                                             
                                            Človek a Pán Fotograf
                                     
                                                                             
                                            Ukecaný :-)), ale obsažný
                                     
                                                                             
                                            Skutočný ochranca pracujúcich
                                     
                                                                             
                                            Maroš - rovný chlap
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tu hľadám dobré letenky
                                     
                                                                             
                                            Moja nová fotogaléria
                                     
                                                                             
                                            Ako sa vyvíja eurokríza
                                     
                                                                             
                                            Keď nuly ovládnu svet
                                     
                                                                             
                                            Cestovateľský portál
                                     
                                                                             
                                            Magazín Ako investovať
                                     
                                                                             
                                            Kritické myslenie - zaručený recept na vtáčiu chrípku
                                     
                                                                             
                                            Moja fotogaléria
                                     
                                                                             
                                            Aj keď ja som conservative
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Lož menom GENDER
                     
                                                         
                       Tri esemesky z minulosti hľadajú svoju budúcnosť
                     
                                                         
                       Úradnícka vláda ako plán Ficovho návratu
                     
                                                         
                       Jedno víťazstvo nestačí
                     
                                                         
                       Niekoľko mecenášov bude po voľbách v Bratislave veľmi smutných
                     
                                                         
                       V lesoparku sa rúbe a stavia
                     
                                                         
                       Sme vyznávači filozofie „tychinizmu“
                     
                                                         
                       Mal som ja v tej Bratislave zostať
                     
                                                         
                       Schützen Schutz!
                     
                                                         
                       Nie je sused ako sused
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




