
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Podhorský
                                        &gt;
                Nezaradené
                     
                 Obohacujúci piatkový večer s TV JOJ a Mojsejovcami 

        
            
                                    24.1.2010
            o
            16:08
                        (upravené
                24.1.2010
                o
                19:15)
                        |
            Karma článku:
                15.58
            |
            Prečítané 
            5135-krát
                    
         
     
         
             

                 
                    Obyčajný piatkový večer 22.1.2010. Ako väčšina obyčajných pracujúcich ľudí som prišiel okolo štvrtej poobede domov. Najedol som sa a po vyčerpávajúcom pracovnom týždni som si užíval domácu pohodu a oddych. Ako sa spieva v jednej slovenskej piesni - "v piatok podvečer si zresetujem hlavu, možno že v nej nájdem aj myšlienku zdravú..." Lenže aj tá posledná zdravá myšlienka sa z hlavy vytratila, keď som zhliadol program na TV JOJ od 20:05 do 22:02 - Mojsejovci. V čase uverejnenia tohto článku nebolo v archíve TV JOJ nebol k dispozícii záznam toho mediálneho kompostu, ale diskusia na stránke TV hovorí za všetko.
                 

                 Unavený teda zapínam TV s jasným plánom. Pozrieť si nejaký nenáročný film pri ktorom mi neutečie pointa keď na pár minút zadriemem a potom sa poberiem do postele. Namiesto toho pozerám na nejaké Mimoriadne noviny plus. Hneď mi zišla na um tragédia na Haiti, že sa dozviem niečo nové, ale mýlil som sa. Na obrazovke sa objavuje podivné duo. Obstarožná odfarbená blondína neúspešne ukrývajúca svoj vek hrubou vrstvou make-upu a nadmerné kilogramy drahým oblečením. Vedľa nej samozvaný spevák a hudobník, alkoholom vysušený neoficiálny víťaz jeho vlastnej ankety Bulo roka.   Ihneď premýšľam že prepnem na inú televíziu. Ale je to ťažký výber pretože DVB-T vysielanie servíruje samé "skvosty" mediálneho neba. Až som sa nevedel rozhodnúť medzi JOJ, JOJ plus, alebo troma estévéčkami... Takže čo od radosti zvoliť? Nakoniec som sa rozhodol že dopozerám tie dve napodobeniny celebrít z viacerých dôvodov: 1. Bol som zvedavý či počas svojich 120 minút slávy dokážu povedať aspoň pár zmysluplných viet 2. Zaujímalo ma aký veľký humus je TV JOJ ochotná a schopná vysielať v záujme ziskuchtivosti 3. Nezaujal ma žiadny "skvost" mediálneho neba prvého digitálneho multiplexu   Pani Mojsejová, snažiac sa svojím vzhľadom evokovať dojem dámy, predviedla rečnícky výkon približne na úrovni hojdacieho koníka. *dojičky kráv (ospravedlňujem sa dojičkám kráv). Svoj prejav "obohacovala" početnými vulgarizmami a typickým východniarskym dialektom produkovala slovné patvary, pri ktorých sa zakladateľ spisovnej slovenčiny musel v hrobe obracať. Priamo v živom vysielaní počas interview nerušene vybavila pár telefonátov a pomedzi to si spokojne zapaľovala cigarety. Počas tohto vystúpenia nám prezentovala niekoľko dôležitých informácií ktoré boli naozaj hodné piatkového večera. Osobne ma najviac zaujalo, ako dramaticky zachránila Braňovi život tým že mu udrela (ona použila vulgarizmus) po chrbte aby mu z krku vyletel kus mäsa, ktorý následne zožral niektorý z ich psov. Ďalšia hodnotná informácia bola o tom, akú bude mať hrobku a že v žiadnom prípade pri nej nesmie ležať Braňo, lebo by ešte aj tam dolu pri nej chľastal. Počas svojho zmäteného nesúvislého prejavu ešte stihla obviniť krčmára, médiá aj celý svet z toho že Braňo začal znova piť a nezabudla nakydať aj na Braňa. V čerstvej pamäti majú mnohí Norin verejný výrok: "Je to koniec - definitívny!" Asi už naň zabudla keďže Braňo znova sedí v jej obývačke v gýčovom kresle.   Pán Mojsej na úvod svojho prejavu s bulvárnou kamerou za chrbtom zavítal do krčmy kde sa spil a kládol krčmárovi bezprízorné otázky na  ktoré ho nenechal reagovať pretože mu neustále skákal do reči, zvyšoval hlas, verbálne ho atakoval a vyhrážal sa právnikmi. Demonštratívne si objednal kofolu a po svojom verbálnom odventilovaní sa zahral sa na samaritána. Ako veriaci človek mu vo veľkom štýle odpustil. Neskôr v Norinom dome, akože v úlohe hosťa, nesúvisle vysvetľoval prečo znova pil. Dozvedeli sme sa o jeho alkoholických cykloch a o tom že aj medzi tým pije ale potajmä. Dozvedeli sme sa hodnotnú informáciu že byt v Tatrách si zariadili výhrami z tombol. Pripomína mi to vyjadrenie pána Kanisa, ktorý nevedel vysvetliť pôvod svojho majetku, tak uviedol že vyhral jackpot. Schizofrenicky pôsobilo Braňove vyjadrenie že si síce nepamätá kto ho do tej krčmy "doniesol", zato si presne pamätá ako krčmár nabádal mládež aby ho fotili a natáčali kamerou. Zlý a nemorálny je krčmár, hostia krčmy, médiá len nie on sám ktorý sa v krčme spil na padrť. Sám seba ešte charakterizoval "Ja som geniálny muž" a snažil sa postaviť do pozície génia ktorý dokáže urobiť pár domácich prác a je jediný a najlepší po boku pani Mojsejovej.   Úžasné intermezzo v podobe spoločného výstupu medzi rodičmi krčmára a Mojsejovcami bolo na úrovni detí predškolského veku. Vykrikovanie ponad plot pripomínajúce výmenu názorov zvierat susediacich klietkach v ZOO, divákom ešte viac odhalila intelektuálnu úroveň aktérov.   Celú atmosféru "Mojsejovského špeciálu" absolútne jedinečným spôsobom dotvárali zdegenrovaným dojmom pôsobiaci psi, ktorí sa v priamom prenose párili na Braňovaj nohe... Reakcie pani Mojsejovej na kopulujúcich psov kombinovali príznaky narcizmu a downovho syndrómu. Pán Mojsej len "inteligentne" konšataoval: "To je príroda..." Hmm, v tých psích oblečkoch vyzerali naozaj prírodne...   Výkon a kvalita moderátora boli prislúchajúce kvalite celej relácie. Nebol schopný udržať v interview logickú kontinuitu ani poriadok. Pôsobil dojmom piateho kolesa na voze, neviedol debatu žiadnym smerom a nebol schopný dodržať čas relácie. Nechal rozhovor plynúť tak, že sa skĺzol do nesúvislého bezobsažného tárania. Jediné na čo sa moderátor zmohol boli infantilné otázky typu: "Ako si sa cítil na tej psychiatrii? Chcel si robiť kuchára? Čo ti povedali, Braňo, veštice? alebo Si teraz nastavený tak, že teraz už vydržíš bez alkoholu?" Záver svojho excelentného moderátorského výkonu spečatil tým, že hladkal psa do ktorého bol z opačného konca zapichnutý druh pes, so slovami "toto sa robí v priamom prenose?".    Už som pochopil prečo to TV JOJ urobila. TV JOJ vo svojej snahe maximalizovať zisk opäť uprednostnila bulvár pred kvalitou. Potvrdzuje to fakt, že predmetnú reláciu hojne pretkávala reklamami ktoré sa intelektuálnou úrovňou od Mojesjovského špeciálu" nikdy veľmi nelíšili. Zároveň TV JOJ využíva nedostatok iných alternatívnych televízií u divákov, ktorí nie sú ochotní platiť káblovým televíziám a uskromnia sa s tým čo dostanú za koncesionársky poplatok. Podozrivú príchuť dostáva v tejto súvislosti aj nezaradenie možnej alternatívy v podobe TV Markíza do digitálneho multiplexu. Samozrejme využili aj prostú ľudskú vlastnosť - zvedavosť, ktorá v tomto prípade zrejme zvíťazila nad racionálnym uvažovaním. V chorobnej túžbe po zisku odvysielajú prakticky čokoľvek.   Premýšľam, čo diváci očakávajú od sledovania Mojsejovského špeciálu. Hlavní aktéri sú presvedčení že sú pre divákov zaujímaví a že značka "Mojsejovci" prináša peniaze. Ako je smutne známe, alkoholikov, neúspešných hudobníkov, rozpadnutých manželstiev i obstarožných dám túžiacich po kráse je na Slovensku viac ako dosť. Preto som presvedčený, že len z nedostatku iných možností je divák tlačený do kúta a pozrie si aj Mojsejovský špeciál. V nemom úžase sleduje verejné pranie špinavého prádla jednej čudnej neúplnej rodiny. So zvedavosti sleduje dve nepodstatné a ničím zaujímavé osoby, aké veľké nezmysly sú schopné vyprodukovať a ako hlboko chcú ešte v očiach verejnosti klesnúť. Premýšľam tiež o morálke ľudí ktorí vedú TV JOJ. Kedy chcú (ak vôbec chcú) dosiahnuť úroveň napr. CNN alebo BBC? Namiesto kvalitného a objektívneho spravodajstva, namiesto náučných alebo vzdelávacích dokumentov, namiesto kvalitných filmov, inteligentnej zábavy a relaxu nám servírujú primitivizmus v podobe Mojsejovcov. Dokonca sa neštítia vysielať tento mediálny odpad v najsledovanejšom čase - v piatok večer. Prefajčená stará dáma bez základov slušného správania a vyschnutý alkoholik majú byť príkladom pre mladých ľudí? Majú byť piatkovým televíznym spoločníkom Slovákov? Už ma neprekvapuje, že mravná úroveň mládeže je katastrofálna. Už sa nečudujem že v piatkový večer sú krčmy plné.   Kladiem si otázky na ktoré nepoznám odpoveď.  V akej krajine to žijeme, že sa niečo takéto mohlo vôbec objaviť na televízne obrazovke, navyše v piatok večer? Nie je v tejto krajine nik, kto by dohliadal aspoň na minimálnu úroveň a slušnosť TV vysielania a na dodržiavanie vysielacích časov? Kto je zodpovedný za vznik a odvysielanie tohto mediálneho hnoja, Mojsejovci, TV JOJ, alebo niekto celkom iný? Dokedy nás bude televízia kŕmiť odpadkami a odpútavať pozornosť od dôležitých vecí?   Televízne vysielanie je z veľkej časti zrkadlom národa. Čo ľudia chcú konzumovať, to im televízia servíruje. Chcem však veriť v múdrosť a zdravý rozum Slovenského národa, chcem veriť že Slováci nechcú a nebudú v TV sledovať mediálne zvratky. Chcem veriť, že už nikto viac nebude sledovať choré relácie a všetci tak dáme televízii najavo že nie sme primitívny dobytok ktorý zožerie všetko čo mu podhodia.   Ďakujem vám TV JOJ za plnohodnotný a poučný piatkový večer. Som obohatený a konečne som sa dozvedel informácie na ktoré som toľké roky čakal. Týmto ste sa úspešne presunuli na samé dno mediálnej žumpy.       *na podnet v diskusii som dodatočne článok upravil, aby som sa nedotkol dojičiek kráv. Hlboko si vážim ich zamestnanie i prácu, pretože si myslím že je na rovnakej úrovni ako každé iné zamestnanie a zároveň sa ospravedlňujem za nevhodne zvolené prirovnanie. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (80)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Podhorský 
                                        
                                            Šetrenie made in SPP
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Podhorský 
                                        
                                            Veľká sonda do malej cigánovej duše.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Podhorský 
                                        
                                            Oplatí sa nám vôbec voliť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Podhorský 
                                        
                                            Áno, diskriminácia na Slovensku existuje!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Podhorský 
                                        
                                            Rozprávka o jednej priepustke - ako fungujú zdravotné poisťovne?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Podhorský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Podhorský
            
         
        podhorsky.blog.sme.sk (rss)
         
                                     
     
        Snažím sa chápať súvislosti a podať ich aj iným. Nebránim sa inteligentnej diskusii. Rád sa poučím a ak sa mýlim, viem otvorene priznať chybu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1726
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




