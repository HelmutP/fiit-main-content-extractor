
 Jaj, hneď mi je lepšie, keď som sa posťažovala, ale nie o tom som chcela. Mávate nočné mory? Ja skoro denne. Tou mojou najhroznejšou je rútiaci sa výťah bez ovládania, alebo plazy pod posteľou. Keď som bola dieťa a mala som posteľ na nohách, nedopadalo to vtedy hrčou na hlave, ako teraz, keď mám takú s úložným priestorom a ja sa pokúšam nájsť ich pod posteľou a odplašiť. Aké detinské!


     Tento sen považujem za extra, už kvôli tomu, že sa mi sníval dva krát a končil tým, že som sa zobudila so smiechom. Netuším, kde presne mal začiatok a kde koniec, ale pamätám si z neho toto.


     „Žiší, vy čo tu robíte?“ Osopila som sa na Roberta Fica, ktorý sa naozaj  šikovne zvŕtal v mojej kuchyni. 
     „ No, čo by som tu robil, varím predsa krupicovú kašu.“ Slušne mi odpovedal. Bol v sivom obleku a dokonca mal aj kravatu, v rukách mal varešku a ukazoval ňou na mňa. 
     „ Prečo u mňa, preboha?“ Spýtala som sa zdesene.
     „ Prečo, prečo, no predsa deťom  na raňajky. Potom si zaspievame hymnu“ 
V tom momente mi to prišlo hrozne smiešne a chichúňala som sa tak, až som sa zobudila. Tak naozaj neviem, či bola so škoricou, alebo grankom, a či do nej pridal aj maslo ako to majú moje dievčatá najradšej. 


    Mám ešte jeden skvelý reprodukovaný kolegynkou. Jej synovi sa snívalo, že jeho otec má v aute špeciálne GPS. Dvoch papagájov, z ktorých mu jeden cestou zdochol. Od policajtov dostal za to pokutu, pretože bez jedného operenca to GPS už považovali za nefunkčné. Či sa jej syn, zobudil so smiechom ako ja to neviem, ale rozhodne to pobavilo celú ich rodinu a aj nás v práci.


Tak naozaj neviem, nezvyknem sa večer prejedať. Hľadám vykladača snov.  Platím krupicovou kašou.

 
