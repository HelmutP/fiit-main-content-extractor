
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peťo Giertli
                                        &gt;
                Historická faktografia
                     
                 Najväčší vojvodca 20. storočia Maršál Sovietskeho zväzu G. K. Žukov 

        
            
                                    4.1.2008
            o
            19:30
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            8828-krát
                    
         
     
         
             

                 
                    Som syn bývalého vojaka z povolania. Vcelku ma láka military tématika a zvlášť najgrandióznejší historický konflikt, aký naša Zem zažila – 2. svetovú vojnu (ww2). Kamoši o mne vravia, že som fanatik do ww2. Asi to mám v génoch, ako môj otec. Momentálne už nie je v aktívnej službe, lebo po rozpade ČSFR sa podľa jeho názoru dostali do vedenia republiky „noví experti“, a on sa nemienil podieľať na diletantskom riadení. Počas socializmu dosiahol hodnosť podplukovníka a jeho funkcia bola veliteľ pozemných vojsk MV pre Moravu a Slovensko. Na Slovensku bol teda najvyšší šéf. Tak, toľko na úvod stačí...
                 

                  Vždy ma fascinovali úspešní velitelia, ktorí navyše vyhrávali. Je jedno, z ktorej armády pochádzajú. A tí, ktorí neprehrali nikdy žiadnu bitku – tak to je elita v elite. Medzi nich napríklad patrili:                      generál Suvorov (1730 - 1800), ktorý začínal v hodnosti vojaka a dopracoval to až na najvyšší post v Ruskej armáde za Kataríny II. V tom čase si všimol jedného schopného dôstojníka vo Francúzskej armáde – Napoleóna. Chcel mu dať príučku, takú, ktorú by si mladý dôstojník zapamätal na celý život, avšak osud mu nedoprial vychutnať si to víťazstvo. Vždy bojovali na iných bojiskách a v roku 1800 Suvorov umrel. Jeho žiak Kutuzov potom vyhnal Napoleóna z Moskvy. Mimochodom kniha o Suvorovovi patrí medzi najlepšie (nevedel som sa od nej odtrhnúť), aké som kedy čítal. Najväčší úspech jeho života je ten, že dobyl tureckú nedobytnú pevnosť Izmail na Dunaji (1790).            MSS Georgij Konstantinovič Žukov (1896 - 1974), mozog Ruskej armády (RA), centrálny plánovač, najväčší stratég 20. storočia. Predstavte si všetkých velikánov vo všetkých armádach a vo všetkých érach. Tak títo mu siahajú akurát tak po členky. Skúsme si vymenovať niekoľkých veliteľov:       -         Eisenhower, Marshall, Patton (USA),   -         Montgomery, D. McArthur (GB),    -         Guderian, Paulus, Rommel, Zeitzler, von Bock, von Rundstedt, Hoepner, Dietrich, Hausser, von Kluge, Woehler, Nehring, von Kleist, Manstein, Model (GER),     -         Bagramjan, Buďonnyj, Čujkov, Golikov, Konev, Malinovskij, Mereckov, Novikov, Rokossovskij, Rybalko, Sokolovskij, Šapošnikov, Timošenskom Jeremenko, dáme sem i Tuchačevského, Vasilevského, Vatutina, a nesmieme zabudnúť na Vlasova, Pavlova a Vorošilova (SU)     (a samozrejme kopec iných, ktorí sú už aspoň 1000 rokov 6 feet under).       Zo všetkých týchto velikánov bol MSS Žukov najlepší. Poviete si, že je to len moje tvrdenie, tak skúsme ponúknuť fakty a nič než fakty (začneme literatúrou – nenadarmo sa jej hovorí faktografická – a archívmi). Dobre, poďme na vec. Často sa hovorí, že tvorcami „Blitzkriegu“ (čiže bleskovej vojny) boli Nemci, a dôkazom toho je porazenie Poľskej (september 1939) a Francúzskej armády (máj 1940) do 1 mesiaca od invázie. Bola to novinka na bojisku, pretože predchádzajúca vojna (ww1) sa niesla v znamení zákopov. Avšak nie je to pravda, pretože ako prvý predviedol Blitzkrieg Žukov, keď rozbil 6. japonskú (elitnú) armádu pri Chalchyn-gole v Mongolsku (august 1939). Bola to už 2. rusko-japonská vojna v 20. storočí (1. rusko-japonská vojna sa odohrávala v rokoch 1904 – 1905, kde Nippon na hlavu porazil Cárske Rusko na súši i na mori – táto vojna je charakterizovaná ako 1. moderná vojna, pretože armády tu už hájili fronty v dĺžke niekoľko stoviek kilometrov – ale o tom niekedy nabudúce). Aj kvôli tejto porážke Japonci nikdy nezaútočili na Sovietsky Zväz počas ww2, lebo sa poučili, že RA už nie je tá, aká bola v roku 1904 – 1905, ale je omnoho kvalitnejšia. Práve preto si za smer svojej expanzie zvolili juhovýchodnú Áziu, kde nepriateľ nebol tak takticky a technicky vyspelý.       V deň, keď vypukla operácia Barbarossa (asi vám to nič nehovorí – prepadnutie Sovietskeho zväzu nacistickým Nemeckom 22. 6. 1941 – pre mňa je to notoricky známe ;-) zastával Žukov post náčelníka generálneho štábu Ruskej armády (NGŠRA), to znamená, že neomylný, krutý a vždy podozrievavý Stalin mu veril a obdaril ho rozsiahlymi právomocami. NGŠRA je mozog armády, on plánuje operácie, pripravuje ofenzívy a defenzívne akcie, synchronizuje fronty (ruská obdoba skupiny armád u Nemcov – Nemci ich mali na východe 5), celkovo riadi líniu strategickej obrany, vydáva rozkazy, odobruje presuny armád a koordinuje činnosť vojenských okruhov (počas mieru – v prípade vojny sa tie zmenia na spomínané fronty, v Európe ich bolo 12, ďalšie boli v Ázii), má na starosti zásobovanie armád... Jeho nadriadeným bol iba Vrchný veliteľ Ruskej armády (VVRA) – čiže Stalin. Počas ww2 dosiahol Žukov funkciu zástupcu VVRA. On zachránil Moskvu, Leningrad (obe 1941), Stalingrad (1942/43) pred krutou porážkou.      Ďalej on vypracoval a realizoval ofenzívne akcie v Stalingradskom kotli, kde bola zničená 6. nemecká armáda (veliteľ Paulus, celkovo táto armáda mala 300 000 mužov, ktorá bola súčasťou Skupiny armád „B“), operáciu Citadela (Kurský oblúk – tu sa odohrala najväčšia tanková bitka) a jemu pripadla česť dobytia Berlína, keď Stalin zvažoval medzi 1. bieloruským frontom (Žukov) a 1. ukrajinským frontom (Konev). Po vojne bol Žukov veliteľ okupačných vojsk v Berlíne, neskôr v roku 1957 sa stal dokonca ministrom obrany ZSSR.        Celý západný svet ho obdivoval, po vojne pozvali Žukova do USA a nie Stalina, ako generála, ktorý vyhral vojnu na východe. Keď sa blížili Rusi do srdca Tretej ríše, mali Nemci panický strach z ich príchodu, celé Nemecko obehli chýry o ruskej krutosti. Kvôli tomu sa chceli jednotky Wehrmachtu na konci ww2 vzdať Američanom a nie Rusom. Všeobecne sa pokladá východný front za bojisko č. 1, západný front a boje v Afrike je druhostupňové bojisko. Neboli tam také grandiózne operácie, s takou ľudskou masou a mechanizovanými divíziami, zbormi a celými armádami ako v Rusku (a nebola tam ani taká zima...). Keď chceli dôstojníka potrestať, poslali ho na východný front. Mimochodom, keď Nemci realizovali operáciu Barbarossa (1941/42), v Rusku bola najkrutejšia zima za posledných 100 rokov.       Počas svojho života Žukov neprehral žiadnu bitku, kde bol Žukov, tam bolo víťazstvo, stačil mu jediný pohľad na mapu, aby správne zhodnotil situáciu, za svoje zásluhy dostal kopec vyznamenaní:      -         3 rády Červeného práporu (Stalin mal 3)     -         6 Leninových rádov (Stalin mal 3)     -         2 Suvorovove rády I. stupňa (Stalin mal 1) – je to mimoriadne vzácny pán (MVP) hore 1730 - 1800     -         2 rády Víťazstva (Stalin mal 2)     -         titul Maršál Sovietskeho zväzu (MSS)     -         4-násobný Hrdina Sovietskeho zväzu (Stalin „iba“ Hrdina Sovietskeho zväzu)     -         4 Zlaté hviezdy (Stalin mal 1)       Žukov vedel reálne odhadnúť situáciu na bojisku, bol veľmi schopný vojvodca, mal skvelé manévrovacie schopnosti, choval úctu k vojakom...     Po ww2 sa dokonca ktosi ozval, že škoda že Žukov umrel (4. 2. 1997), ten by v armáde urobil poriadok. Dokonca ho chceli svätorečiť (3. 8. 1996). Jeden sv. Georgij však v pravoslávnej cirkvi už existuje a navyše vyznamenanie sv. Georgija je pravdepodobne najväčšie ruské vyznamenanie v súčasnosti.     Toto je fenomén Žukov. Takýmto ho spravila sovietska propaganda. A teraz sa pozrime, aká je pravda. Fakt som si tohto veľkého človeka vážil, prečítal som niekoľko kníh o Žukovovi a zistil som aká je pravda (pomohli mi v tom David Glantz a Viktor Suvorov). Poďme na to chronologicky:       „Vysoko hodnotení a vyzdvihovaní boli len tzv. velitelia „s pevnou vôľou“. Tento výraz bol používaný pre veliteľských hrubiánov, o ktorých bolo všeobecne známe, že nech velili kdekoľvek, všade k nim podriadení cítili priamo smrteľnú hrôzu a nenávisť. Klasickými príkladmi sú Žukov, Jeremenko alebo Konev.“                                                                             V. Batšev.       Chalchyn-gol 1939 – prisvojil si zásluhy celého svojho štábu, rozpracovanie operácií, avšak autorom ofenzívy je náčelník štábu (mozog), v tomto prípade to bol generálmajor Bogdanov. V tejto bitke Žukov rozprášil armádu Nipponu. Na pomoc mu Stalin poslal najlepších bojových letcov ZSSR – Hrdinov Sovietskeho Zväzu. Inak brilantne vedená operácia, po tejto operácii dostal Žukov titul Hrdina Sovietskeho Zväzu a hodnosť armádny generál. Počas 100 dní v Mongolsku vyniesol 600 rozsudkov smrti pre vojakov a dôstojníkov, ktorí pod ním slúžili...       Barbarossa 1941 – v tom čase bol Žukov NGŠRA. Pri sovietskych západných hraniciach bolo dislokovaných 134 divízií RA (na jeho rozkaz). Proti nim stálo 136 nemeckých divízií. A práve toto bola podľa Viktora Suvorova (to bol zase iný Suvorov, kedysi bol členom GRU – ruská vojenská rozviedka, v roku 1978 emigroval do GB) Žukovova najväčšia prehra. Iba hlupák opancieruje svoje hranice celou svojou armádou. Riziko je v tom, že sa skracuje reakčný čas armády a protivník má výhodu momentu prekvapenia, a môže zničiť napríklad letectvo už na letiskách, ktoré boli vzdialené asi 15-30 km od hraníc. To sa aj stalo. Výsledok nie je taký, že by bol Žukov hlupák. Trik je inde. Ak ja totiž sústredím 134 divízií na 1 hranici, znamená to, že chcem zaútočiť a nie sa brániť (Stalin chcel „oslobodiť“ celú Európu, tak ako to predtým spravil s pobaltskými krajinami a Besarábiou. Problém bol v tom, že Hitler ho v útoku predbehol – tým stratil moment prekvapenia – a  preto mohol „oslobodiť“ už iba pol Európy. Stalinov sen o Socialistickej revolúcii v Európe teda zničil nič netušiaci Hitler 22. júna 1941.)     Ak by som sa chcel brániť, vojsko by som sústredil do obrannej línie za zabezpečovacie pásmo (navediem nepriateľa do minových polí, pričom vyhodím 2 posledné mosty do vzduchu a najmem sniperov a poviem im, aby killovali iba dôstojníkov – ako to spravili Fíni vo vojne s Rusmi, kde sa Rusi niekoľko mesiacov trápili, hoci mali kvantitatívnu i kvalitatívnu výhodu). Vtedy protivník dosť ťažko môže využiť moment prekvapenia (kopec času, armády, techniky, munície stratí v zabezpečovacom pásme), a ja mám navyše viac času premyslieť si situáciu a následne vydať rozkazy, ako sa armády dajú do pohybu.     Všeobecne sa tvrdí, že Rusi boli zaskočení a, že nevedeli o pripravovanom útoku. Tvrdí sa i to, že Nemci mali väčšiu palebnú silu. To však nie je pravda. Všetky zásoby (munícia, pohonné hmoty, mazivá, oleje, zdravotnícky materiál, poľné nemocnice) boli sústredené v okolí mesta Brest, niekoľko kilometrov od hraníc. Navyše tu bola i Brestská pevnosť, obtekali ju riečne toky, čo predstavuje výhodu pre obrancu. Obranné valy a fortifikačné línie boli opustené, niektoré z nich dokonca boli demontované a presúvané na novú obrannú líniu. Keď sa pripravujem na obrannú vojnu, potrebujem pevnosti a fortifikácie, valy a ostnatý drát. Keď však chcem útočiť, tieto veci sú mi skôr príťažou (vtedy potrebujem výsadkárov – RA sformovala 3 výsadkové zbory!!! (zbor má od 20000 do 50000 vojakov) ako prvá armáda na svete, a ďalej potrebujem rýchle tanky, napr. BT, aké v RA zhodou okolnosti boli J). Vojakov a rôzny materiál budem potrebovať na fronte a nie v tyle.     Za prvých 5 mesiacov vojny padlo do zajatia 4 milióny vojakov a veliteľov, 20500 tankov, 17900 bojových lietadiel a 20000 diel, bola teda zničená takmer celá predvojnová pravidelná RA. Celá prehra je o to horšia, že nepriateľ disponoval 7-krát nižším počtom lietadiel, mal 6,5-krát menej tankov horšej kvality (skúste porovnať nemecký panzer 3 a ruské KV – nie je o čom, pričom KV a T-34 boli do RA zaradené už v roku 1939, na začiatku vojny mala teda RA najlepšie tanky na svete), a že počet útočiacich vojakov bol asi 3 milióny. Vojnové akcie plánoval NGŠRA, v tom čase Žukov.       Leningrad 1941 – v tom čase sa zmenil hlavný nápor nemeckých vojsk. Centrom útoku už nebola Moskva, ale Kyjev, takže Guderian (tvorca tankových armád v Nemecku, neskôr i hlavný inšpektor tankových síl) musel otočiť svoje „pancierové kliny“ na juh, kde sa podarilo obkľúčiť armádu, ktorá tvrdošijne bránila Kyjev.      Do nemeckého zajatia tu padlo 665 000 vojakov! Tým pádom sa oslabovali i nemecké armády, ktoré útočili na Leningrad. Keďže toto mesto bolo asi 200 rokov hlavným mestom Cárskeho Ruska, panovníci ho počas celej tejto doby opevňovali. Hitler si bol toho vedomý, a preto nedobyl mesto útokom, ale ho 900 dní obliehal, ale nikdy ho nedobyl.      Podľa môjho názoru bol Stalin bystrý, opatrný a veľmi prefíkaný človek. Dôkazom toho, že sám nepokladal obranu mesta za stratenú (hoci Žukov tvrdí, že bol do Leningradu poslaný ako spasiteľ, keď mesto už takmer padlo) je to, že nezničil svoje archívy v Leningrade, hoci napr. v Minsku to spravil, keď už bolo jasné, že mesto je neudržateľné. Nemci sa dostali na vzdialené prístupy k mestu 10. júla a Žukov sa objavil v meste až 13. septembra, keď podľa slov Stalina už bola situácia okolo mesta stabilizovaná. Mesto teda zázračne vydržalo i bez Žukova. Pred svojím príchodom do Leningradu organizoval Žukov útočné operácie v priestore Jelni, avšak bezúspešne. Po tomto neúspechu ho Stalin vysiela do Leningradu, aby dozeral na stabilizovanú situáciu v meste. Čiže sem bol poslaný náš geniálny stratég, aby zachránil mesto na druhotriednom bojisku (v tomto období sa obnovil útok na Moskvu, čo bol hlavný strategický smer). Tomu sa nehovorí služobný postup...       Moskva 1941 – Žukov sa chvastá, že on zachránil Moskvu, v tom čase už nebol NGŠRA, ale túto funkciu zastával Šapošnikov (počas bitky o Stalingrad to bol Vasilevskij – pravdepodobne najlepší mozog RA, žiak Šapošnikova), takže mal už iné pôsobisko, je síce pravda, že Žukov konzultoval plány s NGŠRA, ale rozhodne nemôžeme tvrdiť, že on jediný zachránil Moskvu (a čo potom státisíce civilistov, čo budovali protitankové priekopy pred Moskvou???) Bez nich by sotva čosi zachránil. Najbližšie sa Nemci dostali na vzdialenosť 7 km od centra mesta prieskumnou hliadkou (už tam bola zástavka metra). Post NGŠRA stratil Žukov preto, lebo Stalinovi radil vydať mesto Kyjev bez boja (čiže stiahnuť armádu za ľavý breh Dnepra) a preto, lebo bol 2-krát zničený Západný front (Minsk a Smolensk) pod jeho vedením. Kvôli tomu ho Stalin zbavil funkcie NGŠRA. Nová funkcia obsahovala už iba koordináciu frontov, v oblasti pôsobenia nemeckej Skupiny armád „Stred“. Keď chcel náčelník štábu Západného frontu (jeho veliteľ bol Žukov) presunúť polohu štábu 400 km východne od Moskvy, Stalin sa ho pýtal, či majú lopaty, dôstojník odpovedal, že hej. Na to mu Stalin povedal, že keď presunú štáb východne od Moskvy, môžu si rovno vykopať hroby. Neskôr sa o to isté pokúsil i samotný Žukov (chcel presunúť štáb svojho frontu na západný okraj Moskvy) a na to mu Stalin povedal, že keď to urobí, on zaujme jeho pôvodné miesto...       Stalingrad 1942 – tvrdí sa, že to bola hlavná operácia roku 1942, nie je to však pravda. V priestore Stalingradu (operácia Urán) bolo sústredených vyše milión vojakov, časť z nich tvorili trestné prápory (určite to neboli elitné jednotky, ale je pravda, že najhorší nepriateľ je ten, ktorý nemá čo stratiť – a oni teda nemali čo stratiť). Túto operáciu s názvom Urán vypracovával NGŠRA – Vasilevskij, pravdepodobne mu Žukov pomáhal, ale nemôžeme povedať, že je to iba Žukovova zásluha, že Stalingrad nepadol (hoci Nemci už dobyli polku mesta). Navyše tu nie je žiadna formálna spojitosť s operáciou Urán a Žukovom, keďže ten viedol operáciu Mars (Západný front). Naopak, pri Stalingrade bola zničená 6. armáda Feldmarschalla Paulusa. Nikdy predtým sa nestalo, žeby bol zajatý nemecký Feldmarschall, a preto ho Hitler povýšil do tejto hodnosti, aby sa Paulus zastrelil za to, že nedobyl mesto, on sa však nechal kľudne zajať. Keď ho Rusi zajali, neverili mu, že má takú hodnosť, navyše ani netušili, čo robiť v takejto situácii – aký protokol použiť. V tomto období velil Žukov Západnému frontu (asi najprestížnejší front, lebo bránil Moskvu, v čase prepadnutia Sovietskeho Zväzu tomuto frontu velil Pavlov, ktorý bol neskôr nespravodlivo zastrelený) a rozhodne sa nevyskytoval v priestoroch bojov v stalingradskom kotli, kde bolo zajatých 93 000 Nemcov. Celá 6. armáda mala vyše 300 000 mužov a asi len 5 000 z nich sa po vojne vrátilo do Nemecka.     Hlavná operácia (operácia Mars) bola na úseku nemeckej Skupiny armád „Stred“ (ktorú teda riadil Žukov), kde Rusi sústredili vyše 1 milióna mužov (čiže rovnako ako pri Stalingrade) a mali tu aj elitné tankové zbory (nazývané gardové), ktorým velil Rybalko. Navyše mali i podstatne viac obrnenej techniky, delostrelectva a reaktívnych mínometov (známe „kaťuše“ – Mars 3100 tankov, Urán 2600 tankov). A aby to nebolo málo, stalingradská operácia začala skôr, čiže bola útokom, ktorý má odlákať pozornosť od hlavného úderu (kamuflážny) – útoku na Ržev (západne od Moskvy). Avšak podporný útok sa vydaril a hlavný nie, tak preto propaganda operáciu Mars (rževsko-syčevská operácia) ututlala (Žukov už 2. krát nedobyl Syčevku v roku 1942) a začala glorifikovať podpornú operáciu Urán (Stalingrad) a prezentovala ju ako hlavný úder. Navyše sovietske vrchné velenie bolo prekvapené, koľko divízií uviazlo v stalingradskom kotli (rátali, že uväznia 3-4-krát menej početné sily). Komparatívne straty RA sú nasledovné:     Mars – Urán     335 000 ľudí za 1 mesiac – 485 000 ľudí za 2,5 mesiaca       Ak niekto nerozumie týmto číslam, tak na ilustráciu uvediem, že operácia Mars je identického rozsahu ako operácia Overlord (D-day; vylodenie spojeneckých výsadkov v Normandii 6.6.1944). Predstavte si, že by ututlali operáciu Overlord...       Kusk 1943 – kódové označenie je operácia Citadela, zúčastnilo sa jej 3500 tankov RA a 2500 tankov nemeckej armády. Propaganda tvrdí, že to bola najväčšia tanková bitka v histórii (pri Prochorovke; RA 850 tankov, Nemci 600 tankov), avšak nie je to pravda. Najväčšia tanková bitka sa odohrala v úvodnej fáze operácie Barbarossa 23. – 27. 6. 1941 v priestore Dubna, kde 1. nemecká tanková skupina (799 tankov) porazila 6 sovietskych mechanizovaných zborov (vyše 3000 tankov), ktorým velil Žukov. Ďalšia prehra teda, ale tentokrát dokonale utajená. Archívy sa otvorili až  po rozpade ZSSR. Veď len hlupák by do sveta kričal, aký je neschopný. A to Rusi samozrejme nemôžu pripustiť, veď akoby vyzerali pred svetom. Žukov teda priviedol k záhube 4-násobnú presilu. Tomu teda hovorím výkon. A samozrejme, víťaz píše históriu, čo sa stalo i v tomto prípade. Kursk sa v tom čase stal najlepšie opevneným miestom na svete. Na 1 km frontu pripadalo vyše 100 hlavní. Iba na ilustráciu uvediem, aká nálada panovala v nemeckom hlavnom velení v tom čase (H – Hitler, G – Guderian):     G: Koľko ľudí podľa vás vôbec vie, kde leží Kursk? Svetu je úplne ukradnuté, či Kursk udržíme, alebo nie... Prečo vôbec chceme tento rok na východe útočiť?     H: Máte úplnú pravdu. Kedykoľvek na ten útok myslím, obracia sa mi žalúdok.     G: V tom prípade je to bežná reakcia na tento problém. Nechajte to tak!       Zase raz si Žukov prisvojil zásluhy, za celú operáciu, hoci sa na nej podieľalo mnoho ľudí, okrem iného i NGŠRA Vasilevskij. (Ak to neviete, nemecký útok sa skončil fiaskom, lebo nedosiahli svoje ciele a navyše si rozmrvili svoje najlepšie tankové divízie a zvyšok poslali do Talianska, lebo sa tam vylodili Spojenci. Bola to posledná nemecká ofenzíva na východnom fronte – iniciatívu definitívne prebrala RA).       Berlín 1945 – na mesto Berlín jeho rozkazom zaútočili 2 tankové gardové armády (gardová znamená elitná) pričom po útoku prestali existovať. Nie som žiadny generál alebo veľký stratég, ale viem, že útočiť tankom na mesto a v meste je riadna kravina. Podstata tanku spočíva v jeho akčnom rádiu, mobilite, rýchlosti a pancierovaní. Dokonale využiť operačný prielom frontu môžeme vtedy, keď ohniská odporu tankovými klinmi obídeme, uzavrieme ich v kotly a ich zničenie necháme na delostrelectvo a pechotu (zíde sa i letectvo). Avšak použiť tanky v meste?       Epilóg – Pri Barbarosse začínal Žukov na poste NGŠRA, po konflikte so Stalinom a opakovanou porážkou Západného frontu mu bolo zverené už iba koordinovať fronty a vojnu zakončil ako veliteľ 1. bieloruského frontu, teda iba jedného frontu (hoci k mestu tento front doviedol Rokossovský, Žukov teda zlízal smotanu. Problém bol v tom, že Rokossovský mal poľskú národnosť a ako by to vyzeralo pred svetom, že Berlín nedobyl Rus?). Zdá sa vám to ako služobný postup, mne nie, skôr naopak (treba brať však do úvahy i jeden podstatný detail – územie na západ od Moskvy sa stenčuje). V nižších funkciách by sme to mohli prirovnať nasledovne: vojnu začne ako veliteľ armády, neskôr mu zveria už iba velenie zboru no a vojnu zakončí ako veliteľ divízie. Podľa mňa takýto veliteľ nebol príliš schopný, robil kopec chýb. Tu sa nám ponúka otázka: „Tak prečo ho teda Stalin nedal zastreliť? Veď on si s tým svedomie nezaťažoval. Vyvraždil kopec vlastných vynikajúcich dôstojníkov, počas vojny sa „jeho obeťou“ stal i napríklad Pavlov za to, že ustúpil pri nemeckom nápore (keď dám veliteľovi rozkaz, aby pripravil útok na nepriateľa, ale nepriateľ ma v útoku predbehne, potom stratím kopec strategických surovín a kopec armády + moment prekvapenia – budem už musieť do nepriateľa búšiť. Hlupák som ja, ale aby to nedošlo ostatným, dám zastreliť veliteľa frontu, za to, že ho nepriateľ prevalcoval. Pavlov teda plnil Žukovove písomné rozkazy a za to bol zastrelený spolu s celým svojím štábom. Pavlov je mimochodom spolutvorcom tankov KV a T-34, ktorých sa Nemci strašne báli, lebo oni nemali žiadny tank, ktorým by bolo možné tieto ruské tanky zničiť. Aká irónia osudu...).      Tak prečo Stalin nezastrelil Žukova?“ Lebo s Vasilevským tvorili dobrý tím. Vasilevskij bol mozog a Žukov bol – Česi majú taký pekný výraz – řezník. V žiadnom prípade nechoval k vojakom úctu, každému, kto mal nižšiu alebo rovnakú hodnosť ako on, tikal. Vykal jedine Stalinovi. Bol neuveriteľne ješitný, karierista najvyššieho kalibru, neuveriteľne tvrdohlavý, neľútostný. Životy vojakov preňho nič neznamenali. Hore sa dostal iba po kostiach ostatných. Celý rok 1942 posielal státisíce vojakov do boja na Syčevku (už zase, kde to ksakru je?) bez úspechu.      Jeho bývalý nadriadený (myslím, že to bol práve Rokossovský v Kyjevskom zvláštnom vojenskom okruhu - KOVO) sa o ňom vyjadril, že štábne funkcie z duše nenávidí. A štáb, to je predovšetkým mozog. A veliteľ bez mozgu nemôže byť nebezpečný, teda vlastne môže byť, ale len pre svoju armádu... (Žukov iba raz odporoval Stalinovi a za to stratil funkciu. Spomínaný Rokossovský bol zavolaný pred Stalina s tým, že má navrhnúť plány pre nadchádzajúcu operáciu, urobil to, no Stalin mu povedal, že nech ide za dvere a nech si to rozmyslí. Keď sa vrátil znova trval na svojom. Následne mu Stalin povedal, že nech ide za dvere a nech zmení názor, lebo keď nie, dá ho zastreliť. Rokossovský dlho za dverami rozmýšľal a keď sa vrátil Stalinovi povedal, že ho kľudne môže dať zastreliť, ale to čo navrhuje Žukov so Stalinom je skrátka kravina. Stalinovi sa to páčilo a preto operáciu spravil ako navrhoval Rokossovský).       Aj napriek katastrofe operácie Mars Stalin vedel, že Žukov je jeho najlepší bojovník, preto ho nikdy nedal zastreliť. Potreboval takého bušiča, akým Žukov bol. Avšak v inej armáde, ktorá neoplýva toľkými životmi, by taký úspešný nebol. (Toľko v skratke o ww2 a Žukovovi –  bol to zabednený tupec, ktorému sa za celý život nepodarilo uvedomiť si hranice vlastnej determinácie...)          Fontes:     Paul Carell – Spálená země   Robin Cross – Citadela, Bitva u Kurska   Viktor Suvorov – Stín vítězství (v knihe sú emócie a to by pri faktografii nemali byť)   Ch. Chant, W. Fowler, J. Shawová, R. Humble – Hitlerovi generálové   G. K. Žukov – Vzpomínky a úvahy (príliš politicky orientované ako pokus o znovudosiahnutie nepredstaviteľnej moci, ktorú stratil, lebo bol v nemilosti pre svoju aroganciu, btw v knihe je lož, lebo každé vydanie má iný obsah...)   David M. Glantz – Největší Žukovova porážka (skvelá kniha, skvelé poznámky)   Viktor Suvorov – Beru svá slova zpět (celkom zjavne pán Suvorov neznáša súdruha Žukova J)     PS: len málo vecí je krajších, ako čítať o mechanizovanej armáde v pohybe J

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Giertli 
                                        
                                            Barbarossa, 22. jún 1941, 3:15.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Giertli 
                                        
                                            Palebná sila – Tank
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peťo Giertli
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peťo Giertli
            
         
        giertli.blog.sme.sk (rss)
         
                                     
     
        Som Peťo, je len málo vecí, ktorým rozumiem (niekedy ma to štve). Preto absorbujem zaujímavé info. Medzi ne patrí ww2 téma, a preto ju usilovne študujem, kupujem kvantá kníh, hoci mi nikto za to neplatí. Práve naopak :-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8150
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Historická faktografia
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




