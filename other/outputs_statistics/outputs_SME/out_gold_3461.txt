

  
 Ak by zhasla celá planéta na jednu  hodinu, ušetrilo by sa  cca 5 mil. GWh a znížila by sa  produkcia CO2 o cca 2 mil. ton. Zvláštnosťou  je, že iniciátori kampane neupozorňujú na oveľa závažnejší problém  klimatickej zmeny a tou je vysušovanie krajiny. Práve mestá majú  najdramatickejší vplyv na vysušovanie, lebo zastrešovaním, asfaltovaním  a betónovaním zemského povrchu je „zapečatených" toľko plôch na všetkých  kontinentoch, že z nich sa odkanalizuje ročne cca 100 m3 dažďovej vody v prepočte na jedného obyvateľa. 
 Pripomínam,  že odkanalizovanie dažďovej vody z urbannych zón znamená premenu tej  slnečnej energie na citeľné teplo, ktorá sa v minulosti spotrebovala na  výpare zadržanej dažďovej vody (obr. 1).  Keďže na výpare  jedného kubíka vody sa spotrebuje cca 700 KWh, dosahuje ročná produkcia  citeľného tepla  do atmosféry vplyvom „pečatenia" zemského  povrchu  cca 70.000 KWh na jedného obyvateľa. Táto  produkcia citeľného tepla do atmosféry nad mestami, vytvára tzv.  teplotné dáždniky (obr. 2), ktoré vytláčajú mraky do chladnejších  horských oblastí, kde sa vylejú a spôsobujú tzv. bleskové povodne (obr.  3). Experti na klimatickú zmenu tomu hovoria, že je to vplyvom  zvyšovania koncentrácie skleníkových plynov CO2 , čo nie je  pravda. 
  
  
 Obr. 1. 
  
 Obr. 2 
  
 Obr.3. 
   Prial by som si, aby kampaň ochranárskych iniciatív pre ochranu klímy hľadala pravdu a nie tvorila bubliny nezmyslených kampaní, ktoré aj tak k ničomu nevedú. Sú vhodné iba ak politické marketingové ťahy pre neschopných politikov, aby verejnosti dokazovali, že chránia klímu.  

