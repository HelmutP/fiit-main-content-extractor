

  
 10. NAJDLHŠÍ ZÁPAS 
 24.marec 1936, čas 20.34. Začína zápas. Prvé semifinále medzi Montrealom Maroons a Detroitom Red Wings. Oba mužstvá chcú vyhrať, ale niečo sa deje, nepadajú totiž góly. Zápas končí 0-0. Je na rade predĺženie. Po ňom druhé, tretie, štvrté, piate, šieste. V ňom Modere Bruneteau z Detroitu predsa len strelí gól a zápas končí. Zápas trval 176 minút a 30 sekúnd čistého času. Na hodinách je 2.25 ráno, a všetci sú totálne unavení. Najviac však brankár Norm Smith z Detroitu /predtým chytal za Montreal, no vyhodili ho pretože sa im zdal neperspektívny/. Zneškodnil 90 striel! Detroit napokol vyhral celú sériu 3-0 na zápasy. 
  
 9. REKORDNÁ NÁVŠTEVA 
 1.január 2008. Sabres vs. Penguins. Zápas sa hrá na štadióne Ralpha Wilsona, ktorý pôvodne slúži na americký futbal. Hrá sa na špeciálnej ľadovej ploche pod holým nebom. Neustále sneží, plochu niekoľko krát upravujú rolby. Oba mužstvá hrajú v retro dresoch. Je to veľká udalosť. Lístky na zápas sa oficiálne vypredali za 26 minút. V hľadisku sedí 71 217 divákov, čo je rekord histórie NHL. Zápas končí aj po predĺžení 1-1. Crosby má jednu asistenciu a v nájazdoch symbolicky rozhoduje o víťazstve svojho mužstva. Dva roky predtým v Michigane videlo zápas univerzitných tímov 74 554, čo je najvyššia hokejová návšteva v histórii. 
  
 8. SÉRIA PENGUINS-CAPITALS 
 To čo sa stalo v sezóne 2008/09 sa nestalo v NHL nikdy predtým. V semifinále východnej konferencie sa stretol Washington s Pittsburghom. Nikdy predtým /v play-off/ proti sebe nehrali dve najväčšie hviezdy ligy /Gretzky nikdy nenastúpil proti Lemieuxovi v nadstavbovej časti, rovnako tak Howe proti Richardovi/. Crosby vs. Ovechkin. Bola to strhujúca séria, hral sa výborný hokej. V jednom zápase série dali obe hviezdy dokonca hattrick. Predbiehali sa v hokejovom umení. Séria sa samozrejme hrala na 7 zápasov. Z postupu sa napokon tešili "Pens". O niekoľko dní na to dobili Stanleyho pohár. 
  
 7. NAJPOPULÁRNEJŠIA FOTKA 
 10-teho mája 1970, vznikla jedna z najkrajších fotografií. V play-off sérii Bostonu proti St.Louis. Bobby Orr bol pred bránkou a ked sa dostal k puku, obranca súpera mu prudko podrazil nohy. Orr vyletel do vzduchu a vtedy akoby sa zastavil čas. Celé telo mal nad ľadom a vystrelil... Puk skončil v sieti a Orr ešte stihol zodvihnúť ruky nad hlavu. Až potom dopadol na ľad. Fotografia zachytáva Orra v čase keď má ruky nad hlavou a puk je už v sieti. Veľký moment obrancu, ktorého mnohí dodnes považujú za najlepšieho na svojom poste. 
  
 6. TROFEJ NEDVÍHA KAPITÁN 
 Ray Bourque odohral v drese Bruins 21 sezón, ale nevyhral Stanleyho pohár. V roku 2000 oslávil 40 rokov a rozmýšľal o konci kariéry. Prišla však zaujímavá ponuka. Ozvali sa zástupcovia Avalanche a ponúkli mu zmluvu. Dohrať zbytok sezóny v ich farbách, plus opcia na ďalšiu sezónu. Colorado malo v tom čase veľmi perspektívne mužstvo s reálnou šancou vyhrať NHL. Ray sa rozhodol prijať, túto ponuku s tým, že ročník 2000/2001 bude definitívne jeho posledným. Avalanche postúpili až do finále, kde sa stretli s NJ Devils. Po šiestich zápasoch bol stav série 3-3 a rozhodujúci zápas malo hrať Colorado doma pred zrakmi svojich fanúšikov. Ray vedel že má poslednú šancu. Jeho mužstvo vyhralo 3-1 a on to dokázal! Prezident NHL odovzdával trofej tradične kapitánovi víťazného celku. Sakic ju okamžite nesie Rayovi a ten dvíha pohár nad hlavu ako prvý. 
   



 
 5. HOROR NA ĽADE 
 Najkrvavejší moment, aký sa v dejinách odohral. Clint Malarchuk bol priemerným brankárom, ktorý sa stal hrdinom a nepotreboval na to dych vyrážajúce štatistiky. 22.marec 1989 a zápas Buffalo - St.Louis. Prihrávka smeruje pred bránu, kde sa zráža útočník Steve Tuttle a obranca Uwe Krupp. Tuttle padá, pričom nekontrolovateľne dvíha pravú nohu privysoko. Čepel jeho korčule sa blíži smerom k hlave Malarchuka, ten stihne zakloniť hlavu, čím odkrýva nechránený krk. Čepel ho doslova podreže a pretína tepnu. Okamžite sa mu valí krv a nastává hrôzostrašná scéna. Malarchuk dáva dole masku a drží sa na krku. Pribieha lekár a pritláča prsty na ranu. Nastáva okamžitý prevoz do nemocnice. V priebehu nehody v publiku omdlelo 10 ľudí, dvaja hokejisti vracali priamo na ľade. O tri mesiace sa vracia a zažíva "standing-ovation". Clint neskôr povedal: "Prvé čo mi napadlo? Zápas určite sleduje moja mama, nemožem dovoliť aby ma videla zomrieť v priamom prenose". Hrdina, ktorý vyhral najťažší zápas kariéry. 
   
 4. NOC DARRYLA SITTLERA 
 7.február 1976 bol večer najlepšieho individuálneho výkonu všetkých čias. Toronto-Boston 11:4. Bilancia kapitána Toronta? 6 gólov a 4 asistencie. "Vždy keď som mal puk na hokejke, cítil som, že sa niečo udeje". Ono sa to vlastne dialo aj o pár dní neskôr, keď strelil v play off Philadelphi 5 gólov, ďalej s Montrealom pri výťazstve 5:1 dal znova všetky góly. To všetko sú ukážky zázračného hráča Leafs. No februárovy večer vyčnieva asi najviac. 
   
 3. MARIO LEMIEUX SA VRACIA 
 V roku 2000 prišli Vianoce o 18 dní skôr. Teda aspoň v Pittsburghu. Médiá ohlásili správu že sa na klziská vracia Mario Lemieux. Tento počin mnohí prirovnávajú k návratu Michaela Jordana na palubovky NBA. Okamžite sa začala panika. Fanúšikovia sa chceli presvedčiť či nejde o mediálne ťahy novinárov. Telefónne linky klubu Penguins boli beznádejne preťažené. Každý chcel vedieť pravdu. Potom predstúpil pred verejnosť samotný Mario a povedal: ´Áno, vraciam sa. Dostal som chuť na hokej.´ Začalo sa oslavovať, záujem o zápasy v ktorých hrali ´tučniaci´ prudko stúpol. Veľa hráčov bolo však aj smutných, pretože v zbytku sezóny sa už ich kluby s Mariovým Pittsburgom nestretli. Každý chcel hrať proti nemu hrať. Návrat žijúcej legendy, bol okorenený ziskom zlata na OH 2002 a víťazstvom na svetovom pohári 2004. V oboch prípadoch bol kapitánom Kanady. Išlo o veľmi podarený návrat. 
   
 2. BOD ČÍSLO 1850 
 Moment, ktorý nesmie chýbať. 15.10 1989 Kings-Oilers. Čaká sa už iba na jednu vec. Gretzky /už v drese kráľov, kam prišiel práve z Edmontonu/ dáva gól a prekonáva rekord Gordieho Howa, dosahuje 1850 bodov v kariére. Stáva sa najproduktívnejším hráčom histórie NHL a to už vo veku 28 rokov /!/. Na zápase je prítomný aj samotný Howe. Po zápase sa gratuluje novému kráľovi. Presne o rok dosahuje aj bod č.2000 a tým sa vzdaľuje ostatným niekdam do inej galaxie. 
   
 1. ROZLÚČKA 
 18.4 1999 je najsmutnejším a zároveň najemotívnejším dňom histórie NHL. "The Great One" sa lúči s kariérou v poslednom zápase proti Pittsburghu. Pripisuje si symbolicky jeden bod za asistenciu, hoci Rangers prehrávaju po predĺžení 1:2, to vôbec nie je podstatné. NHL prichádza o Gretzkyho. Aká bude? Liga stratí veľa fanúšikov, a jej popularita určite klesne. To je to čo bolí Garyho Bettmana /prezident NHL/ najviac. V Madison square garden nie je jedno oko suché. Diváci vkuse plačú a zároveň tlieskajú, plačú a tlieskajú, a zase dokola... Rozlúčka snáď nemá konca, na ľad postupne prichádzajú známe osobnosi /Lemieux, Messier, Sather, jeho žena, deti, otec/ rozlúčiť sa. Diváci mávajú transparentmi najčastejšie s nápismi: Thank you 99, Please one more season...Gretzky uzatvára tento pochmúrny večierok slovami: "Nečakajte, nevrátim sa".  
   
 Foto zdroj: nhl.com 
   

