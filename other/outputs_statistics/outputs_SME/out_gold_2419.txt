

 Často sa stáva, že nerozumieme, čo nám v predpovedi počasia chcú povedať, resp. aké počasie máme v skutočnosti očakávať. Preto sa v článku zameriam na niektoré sporné aspekty meteorologických predpovedí, ktoré vznikajú aj v dôsledku neprofesionality ľudí, ktorí ich prezentujú. Prepáčte, rosničky (: 
   
 Vietor 
 Rýchlosť vetra  
 Prvým bodom, ktorý je zdrojom nezrovnalostí, je vietor. Pre začiatok je potrebné uviesť, v čom je problém. Myslím, že by sa už konečne mala unifikovať jednotka rýchlosti, ktorou je intenzita vetra vyjadrovaná. Často sa stáva, že každé médium udáva rýchlosť vetra pomocou inej jednotky. Niektoré v metroch za sekundu (m/s), iné v kilometroch za hodinu (km/h). "Bežným" ľuďom je určite bližšia práve druhá hodnota, ktorá je používaná aj v iných odvetviach ako meteorológia (napr. motorizmus). Mne osobne sa viac páči udávanie hodnoty v m/s, pretože má pre mňa väčšiu výpovednú hodnotu. Dokážem si tak reálnejšie predstaviť akú vzdialenosť "absolvuje" vietor za jednu sekundu. Najčastejšie pojmy v súvislosti s rýchlosťou vetra sú: bezvetrie, slabý, mierny, čerstvý, silný, prudký, búrlivý, víchrica, orkán. Ide o tzv. Beaufortovu stupnicu. 
 Smer vetra 
 Až keď sme sa na geografii učili, že smer vetra udáva smer, odkiaľ fúka vietor, začal som nad týmto problémom hlbšie rozmýšľať. Takže keď budete v predpovedi počuť, že nám zafúka severozápadný vietor, vedzte, že zafúka od severozápadu, nie na severozápad. Je to teda presne naopak ako pri morských prúdoch, kde južný prúd smeruje zo severu na juh. Ak je reč o premenlivom vetre, musíme rátať s tým, že nám bude viať do chrbta, raz do tváre, inokedy zo strán. 
 Tornáda 
 Tornádo je veterný vír, ktorý je z oblakov zreteľne spojený so zemou. Pokiaľ sa minulý rok hovorilo o tornáde v Poprade, bola to len novinárska kačica, pretože išlo len o trombu. Tromba je, ľudovo povedané akési "nedokonalé" tornádo, ktoré nedosadá na zemský povrch. Často sa ľuďom takisto pletie tornádo s vodným vírom, ak uvidia tornádo nad morom. Ono je síce pravda, že v takom prípade tornádo "nasáva" vodu, no vodný vír predsa nemôže fungovať vo vzduchu, preto sa aj tak nazýva... 
 Záverom tejto podkapitolky uvediem prepočet z m/s na km/h. 1 m/s = 3,6 km/h (hodina má 3600 sekúnd a kilometer zas 1000 metrov). 
 Hurikány a iné  
 Hurikány, cyklóny, tajfúny. To všetko sú termíny označujúce rovnaký meteorologický jav, avšak so špecifickou lokalizáciou. Hurikány sú silné tropické cyklóny (pozor! nie cyklón, ale cyklóna - tlaková níž) vznikajúce v oblasti Atlantického oceánu a východného Pacifiku. O tajfúnoch hovoríme v západnom Pacifiku. No a napokon cyklóny sa vyskytujú v oblasti Indického oceánu. Pre označenie intenzity hurikánov sa používa Saffirova-Simpsonova stupnica. 
 Oblačnosť 
 Celkový pomer množstva oblakov k oblohe, ktorú zakrývajú, nazývame oblačnosťou. Pri definovaní množstva oblačnosti sa vychádza z rozdelenia oblohy na 8 rovnakých celkov, ktorých pokrytie označujú tieto pojmy: 
 jasno: 0-1/8 (nula až jednu osminu oblohy pokrýva oblačnosť) 
 takmer jasno: 2/8 
 malá oblačnosť: 3/8 
 polojasno/polooblačno: 4/8 
 oblačno: 5-6/8 
 takmer zamračené: 7/8 
 zamračené: 8/8 (nie je viditeľný ani kúsok modej oblohy) 
 Okrem uvedeného je potrebné rozlišovať nízku, strednú a vysokú oblačnosť, inverziu a hmly. 
   
 Teplota 
 V našich zemepisných šírkach vyjadrujeme teplotu v stupňoch Celzia (˚C). Pre vyjadrenie teploty v Severnej Amerike sa používa Fahrenheit. 0 stupňov Celzia=32 stupňov Fahrenheita. Teplotu meriame zásadne v tieni. Nemôže teda prekvapiť, keď nám za mrazivého, ale slnečného dňa teplomer vystavený slnečnému žiareniu ukáže +20 ˚C. Pocitovú teplotu ovplyvňuje už spomenutá intenzita slnečného žiarenia, vlhkosť a vietor. Mnohí si iste pamätáte jún minulého roka, kedy vlhkosť vzduchu bola často nad 90 % a teplota "len" okolo 25 stupňov. No na pocit bolo neznesiteľne ako pri letných horúčavách. 
 Pokiaľ ide o denné maximá a minimá teplôt, minimum býva merané spravidla tesne pred východom Slnka, keď je zemský povrch ešte vychladený (platí pri slabom vetre). Vrchol dňa býva väčšinou okolo 14-tej hodiny, keď je intenzita slnečného žiarenia na maxime. V rámci územia Slovenska môžeme pri teplotných maximách pozorovať odchýlku až 4-6 stupňov. 
 Na záver podkapitoly jedna dôležitá poznámka: teplota vzduchu je meraná vo výške 2 metrov nad zemským povrchom. S narastajúcou nadmorskou výškou je teplotný gradient nasledovný: každých +100 metrov = cca -0,6 stupňa. 
   
 Zrážky 
 Zrážky sa vyskytujú prevažne v podobe dažďa a snehu či mrholenia. Zriedkavejšie vzniká ich kombinácia alebo mrznúci dážď, krupobitie, snehové zrná, hviezdice a pod. Prognóza počasia zvykne hovoriť o slabých, miernych či silných zrážkach. Pokiaľ ide o priestorové rozloženie zrážok, používané sú najčastejšie pojmy ako ojedinele, miestami či na mnohých miestach. 
 Ojedinele: pravdepodobnosť výskytu zrážok na 10-30 % územia resp. na jednom mieste počas celého dňa, pre ktorý je predpoveď platná, je pravdepodobnosť výskytu zrážok 10-30 % 
 Miestami: pravdepodobnosť 35-65 % 
 Na mnohých miestach/väčšina územia: 70-90 % 
 Okrem toho musíme brať do úvahy aj časové rozlišovanie. To udáva, po akú časť dňa musíme rátať so zrážkami. Predpovedá sa občasný dážď, časom sneženie, prechodne s prehánkami... Uvedené adverbiá predstavujú zrážky netrvalého charakteru. Pokiaľ ide o trvalé zrážky v rámci celého dňa, zväčša sa nepoužíva žiadna špeciálna príslovka, ale jednoducho dážď, sneženie. 
   
 Dúfam, že sa mi týmto laicky poňatým článkom podarilo aspoň sčasti uviesť najčastejšie používané meteorologické formulácie na pravú mieru. Nebolo v mojom úmysle ani silách podať vyčerpávajúcu a profesionálnu správu o "výrobe" prognóz počasia. Pokiaľ ale máte nejaké otázky alebo nezrovnalosti, do diskusie s nimi! 
   

