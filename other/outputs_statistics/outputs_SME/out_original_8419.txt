
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radovan Gergeľ
                                        &gt;
                Logikou k športu...
                     
                 Ako chceš vyzerať, tak aj trénuj... (druhý diel) 

        
            
                                    9.6.2010
            o
            11:51
                        (upravené
                9.6.2010
                o
                12:17)
                        |
            Karma článku:
                12.48
            |
            Prečítané 
            9001-krát
                    
         
     
         
             

                 
                    Pýtam sa: „Kto má menej tuku, šprintér, alebo maratónec?“ Odpoveď zväčša: „maratónec“. Skutočnosť je však iná. Správna odpoveď: šprintér. Ale nemôžeme sa čudovať nad množstvom nesprávnych dedukcií, keďže širokej verejnosti je stále dookola vysvetľované, že na spálenie tuku musia vykonávať dlhé hodiny aeróbnych aktivít. Šprintéri nerobia žiadnu. A predsa majú menej tuku. Ako je to možné?
                 

                 
  
       Jedným z hlavných dôvodov je intenzívny charakter ich tréningu. Opäť nechcem diskutovať v tomto článku o strave. Samozrejme, že ak môj príjem prostredníctvom stravy bude vyšší ako výdaj, neschudnem ani gram. Ani kúzelník mi nepomôže ak sa budem prežierať, a zároveň usilovať o stratu tuku. Avšak, ak upravím príjem stravy a zvýšim výdaj energie, cesta k lepšej postave zrazu otvorená. Áno, je to tak jednoduché.   Ako zvýšiť výdaj? Jednou z najlepších foriem je anaeróbny tréning. Čo znamená napríklad intervalový prístup k športovým aktivitám. Intervalový tréning môžme charakterizovať ako sériu rýchlych vĺn danej aktivity (beh, bicykel, plávanie, cviky s vlastným telom...) vo vysokej intenzite oddelených krátkou pauzou alebo vlnami s nižšou intenzitou. A máme tu opäť to vyššie spomínané slovíčko „intenzita“ ovládajúce svet šprintu.   Čím vyššia intenzita v tréningu, tým viac spálených kalórií. A čo je podstatnejšie, ako odmenu získate „zvýšenú po - tréningovú spotrebu kyslíka“ (EPOC - excess post-exercise oxygen consumption), čo v preklade znamená aj spaľovanie kalórií dlho po samotnom tréningu. A to s aeróbnymi aktivitami v „balíku“ nemáte. Bohužiaľ.   Niektorí „odborníci“ však stále prehlasujú, že EPOC neexistuje. Nemusíme byť vedci aby sme vedeli logicky rozlišovať. Opäť stačí zdravý sedliacky rozum. Porovnajme človeka, ktorý práve dobehol interval (dajme tomu 4 krát 30 sekúnd šprint s 1 min 30 sek pauzami) a človeka, ktorý práve dobehol 45 minút miernou intenzitou - aeróbne . Ten prvý, kľačiac na kolenách lapá po dychu ešte ďalších pár minút, celý červený, fyziologické procesy naštartované, metabolizmus zvýšený, nedokáže rozprávať. Ten druhý si vypije svoj nápoj a rozpráva s vami ako keby sa nič nestalo (pokiaľ samozrejme nebežal prvý krát v živote).    Preto väčšina odborníkov sveta zaoberajúcich sa stratou telesného tuku radí namiesto monotónnych aeróbnych aktivít, vykonávať tie anaeróbne, a teda intervalové.   Výhody intervalového tréningu:     
Šetrí čas. Zväčša trvá do 15 - 20      min, TABATA iba 4 minúty, ale o tej neskôr.   
Šetrí zdravie končatín, keďže nohy sú logicky      v menšom, kratšom kontakte so zemou. Väčšina úrazov vo svete      športu býva z opotrebovania z dlho trvajúcich stále sa      opakujúcich pohybov. 
   
Šetrí vaše horko-ťažko nadobudnuté      svaly. Pri dlhých aeróbnych aktivitách, ruka v ruke      s nedostatočnou výživou, dochádza často krát k úbytku svalového      tkaniva. Čím menej svalov, tým nižší výdaj tela pri bežných aktivitách ako      aj pri športe. Čím nižší výdaj môjho tela, tým menej môžem papať aby som      si udržal postavu akú mám. Čím menej môžem papať, tým som viac nahnevaný      a tým viac papám. Opäť začarovaný kruh.   
Buduje aj aeróbnu kondíciu. Áno,      vydržíte viac na vašom obľúbenom aerobiku.   
Je zábavnejší. No comment. Treba      vyskúšať.
   
Spaľuje viac kalórií keďže ako      bonus dostávate vyššie spomínaný EPOC, čiže zvýšený metabolizmus ešte      niekoľko hodín po tréningu.     „Nevýhoda“ intervalového tréningu:   · Je ťažký. Čo si budeme klamať. Sme leniví. Ľudia radšej prečítajú časopis na bicykli či bežiacom páse v miernom tempe a v domnení ako veľa robia pre svoje telo. Rok čo rok. Bez výsledkov. Spomeňte si na toho „kolegu“, čo už dva roky každé ráno sedí v rohu na bicykli, číta najnovšie číslo „mínus sedem dní“, potí sa, mení tričká a stále nič.   Pre tých čo potrebujú vedecké fakty, uvádzam napríklad štúdiu pod názvom „Gibala study“. V tomto výskume odborníci porovnávali krátke intervaly (4 až 6 krát 30 sekundové šprinty na stacionárnom bicykli so 4 minútovými prestávkami) oproti tradičnému vytrvalostnému tréningu v miernej zóne po dobu od 90 do 120 minút. Obe skupiny trénovali 3 krát do týždňa.  Výsledok. Rovnaké zvýšenie výkonnosti a spôsobu akým sval využíval kyslík. V preklade. 6 až 9 minút tvrdého intervalového tréningu (ak nepočítam prestávky) do týždňa je porovnateľne s takmer piatimi hodinami bežnej aeróbnej aktivity! Čistá efektivita. Šetrenie času.   Je však dôležité zdôrazniť (to by mohla byť asi ďalšia „nevýhoda“), že k tvrdým intervalom sa jednoducho treba dopracovať. Nikomu neodporúčam začať naplno šprintovať ako napríklad vo vyššie spomínanej štúdií. Zvlášť ak máte 120 kg a vašou jedinou dennou aktivitou je prepínanie programov večer pred telkou. Všetko má svoj čas.   Čo dodať na koniec, ak už neveríš žiadnym štúdiám, ak neveríš mne (prečo by si aj mal), ak neveríš skúsenostiam ostatných ľudí, skús uveriť aspoň svojej logike. Skús sa pozrieť na tých najlepších maratóncov. Napríklad skupinku víťazných bežcov tesne pred cieľom. A povedz či tam vidíš niekoho s postavou akú by si chcel mať ty. Stavím sa, že nie. Potom prepni na šprint a napíš mi do diskusie koľko vysnívaných postáv si tam videl. Ako chceš vyzerať, tak trénuj. Opäť stačí len popremýšľať...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (46)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Gergeľ 
                                        
                                            Moderné sacharidy - najlacnejšie, najčastejšie a preto najhoršie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Gergeľ 
                                        
                                            Acai berry - detoxikuje a očisťuje - aj peňaženky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Gergeľ 
                                        
                                            5 x 5 = menej tuku, viac svalov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Gergeľ 
                                        
                                            Všetko o mojej ceste...a ešte aj čosi naviac
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Gergeľ 
                                        
                                            Veľká cholesterolová záhada...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radovan Gergeľ
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radovan Gergeľ
            
         
        radovangergel.blog.sme.sk (rss)
         
                                     
     
        "Jednou z prvých povinností doktorov je vzdelávať masy nebrať lieky.."(Sir William OSLER, častokrát nazývaný aj Otec Modernej Medicíny). Nie som žiaden doktor, či špičkový atlét, nerobím výskumy pre Slovenskú Akadémiu Vied, nemám plnú stenu diplomov...no mám zdravý rozum, snahu o tie najkvalitnejšie informácie a oči otvorené.

Viac na: radyactive.sk.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    12844
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Logikou k zdraviu...
                        
                     
                                     
                        
                            Logikou k športu...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




