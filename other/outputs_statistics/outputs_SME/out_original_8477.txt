
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tatiana Cocherová
                                        &gt;
                Ostatné
                     
                 Slovenčina, svetový jazyk 

        
            
                                    10.6.2010
            o
            2:48
                        |
            Karma článku:
                11.41
            |
            Prečítané 
            1588-krát
                    
         
     
         
             

                 
                    V jeden bežný deň v Petržalke sa stretnú v kaviarni na rohu dvaja starí známi. Poznajú sa už aspoň tri dekády. Chodili spolu na vysokú školu, voľakedy zvykli spolu chodiť na dovolenky a ich deti sa tam spoločne hrávali. Po revolúcii v osemdesiatomdeviatom sa ale čas zrýchlil a bolo ho menej. Aj preto sa dnes veľmi tešili na spoločnú kávu.
                 

                    Veľká miestnosť bola plná. Zmiešané rozhovory sladkožrútov sa odrážali z dlažby i kachličiek na stenách, ale im to vôbec neprekážalo. Hoci si chceli toho veľa porozprávať, nikam sa neponáhľali. Tu zrazu pristúpi k ich útlemu okrúhlemu stolíku bielej farby (také tie, čo už tradične stoja v cukrárni na jednej nohe), tak tu stojí jeden evidentne zahraničný študent Ekonomickej univerzity v Bratislave. Rád by si prisadol, lebo inde miesto nevidí. A možno tušil, že si s pánmi dobre porozumie. Starí priatelia ho privítajú s upozornením, že ich päťdesiatročné debaty a stokrát opakované vtipy (aj váš otec má ten dar?) nemusia byť príliš zaujímavé. Biele zuby prezradia, že to príšelcovi vadiť nebude. Ako bolo tak bolo, náhoda je niekedy najlepší plán. Všetci traja páni sa dali do družnej debaty, ktorá trvala dlhšie ako ktorýkoľvek z nich pôvodne plánoval. Nakoniec bol najzodpovednejší náš čierny študent a so slovami, že štúdium volá, ohlási pomalý odchod. Keď vstáva od stola, ešte raz sa poďakuje za poskytnutú stoličku a príjemnú spoločnosť. A dodá: „Už dávno som sa tak dobre so Slovákmi neporozprával.“ Obaja páni boli slušní a preto sa úctivo poďakujú, počkajú až ich nový spoločný známy celkom nezájde. Potom z nich vyletí hlboký úprimný smiech, ktorý vyruší všetkých ostatných hostí.   Naši bodrí spolužiaci (Maďar a Ukrajinec) sa spoznali pred tridsiatimi rokmi počas zahraničného štúdia. Tam si našli i svoje slovenské manželky. Dnes už zo zvyku medzi sebou používajú reč svojich žien.  Slovenčinu.   I keď treba priznať, že Maďar sa chodí „koňovať“, jeho deti sú občas „ohoreté“ a tiež trávi víkendy na „hate“. Ukrajinec nepoužíva „sa, si“ pri zvratných slovesách a pravidelne mäkčí tam, kde netreba a z času na čas chytí v dome „myšu“. Ale to nevadí. Alebo áno?      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (41)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Cocherová 
                                        
                                            Ku kaderníkovi na masáž
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Cocherová 
                                        
                                            Prechádzkou ku kaplnke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Cocherová 
                                        
                                            Dovolenka v Bretónsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Cocherová 
                                        
                                            O princenznách, meditácii a všežravcoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Cocherová 
                                        
                                            E-mail z Číny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tatiana Cocherová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tatiana Cocherová
            
         
        cocherova.blog.sme.sk (rss)
         
                                     
     
        Som dobrodruh i zodpovedná mama... čo s tým? :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1646
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z Číny
                        
                     
                                     
                        
                            Ze Záhorá
                        
                     
                                     
                        
                            Z Bretónska
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Ostatné
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




