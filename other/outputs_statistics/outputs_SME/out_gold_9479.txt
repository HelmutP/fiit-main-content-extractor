

   
 
  No čo už s milnosťou naších osudov, 
 dobovo nezávislých predurčených ov... 
 A čo tá naša úmyselná milnosť, smiech 
 a radosť z určenia a možnosť, rojko... 
 Červené líčka žiaria krásou, 
 usmievajúc sa, jarnou a chápavou. 
 Len kde chlapec jej blúdi, 
 vari hentam je v núdzi? 
 Slniečko sa škerí a smeje, 
 veď ten neduh, to prejde. 
 Čierňava zalieva oblohu, 
 mláku a hravú potvoru. 
 Roky vlečúce sa ,vlny netušiace 
 bolesť veliká, všelijaká. 
 A vari okúsil on prehnité, 
 baladicky zabité, 
 mlčanie vraviace. 
 Na jablku nepoznať keď, 
 je z vnútra prehnité. 
 

