

 Slanina je pre mňa všetko, nikdy sa mi nezunuje. A viem, že najskôr bola slanina a potom prasa. Najlepšia je slanina domáca, ktorá mala zo svojho chlieva výhľad do dvora. Čo stučnela na troch teplých chodoch denne. 
 Slanina je pre mňa poézia, je to prvé a posledné. Ivan doniesol z Oravy takú, bol tam na návšteve u dcéry. Slanina ležala na stole, na drevenom lopáriku sa pomaly tvorila mastná škvrna vysokej kvality. Bola prerastená mäsovým pásom jemne, nevtieravo. Pásik mäsa nerušil celkovú, originálnu konštrukciu toho skvostu. Vzhľad, aj decentná dymová vôňa sľubovala neopakovateľný gastronomický zážitok. 
 Ivan ju doniesol zabalenú v papieri. Nikdy sa nebalí do umelej hmoty, to je veľký priestupok. Rozbalil. Podržal pred očami, na vizualizáciu a poťažkal. Nedotkol. Len inhaloval. 
 Mala cez pol kila a bola nenačatá. Ostrým nožom, so špicatou čepeľou, odkrojil malý kus zvrchu a šikmo. Nastokol na koniec noža a ochutnal. Bola jeho, mal na to právo. Potom prišiel ten okamih, začali sme jesť. Nostalgické spomienky na ten deň budú vibrovať mysľou a žalúdkom niekoľko tisíc ďalších hodín. 
 Niekde v polovici, som odkrojil sotva milimetrový plátok. Ivan myslel, že chcem švindľovať a sledoval ma s prižmúrenými očami. Dohoda znela, raz ja, raz on rovnakým dielom. Zobral som ten plátok medzi prsty. Vo fáze jedenia je to dovolené, ba priam sa vyžaduje, konzumovať prstami. No je zakázané prsty oblizovať. Tuk musí byť viditeľný, prsty sa musia  lesknúť, slanosť delikatesy musí pokožku mierne štípať. No plátok som si nevložil do úst. Položil som si ho na zápästie. 
 „Keď sa rana zapáli a už nič nepomáha, takto priložíš na kožu. Previažeš obväzom a do rána je rana čistá." Povedal som a spolu sme sledovali, ako mi plátok pomaly priľnul na pokožku. „Je to liek." Podal som mu nôž. Odkrojil tiež jemný, milimetrový plátok. Presne podľa stanovených pravidiel a vložil si ho do úst. 
 Zjedli sme ju celú. S chlebom, trochou horčice a feferónkami. Nezapíjali sme, to sa nerobí. Ivan ide o pár dní zas na Oravu, za dcérou. Sľúbil, že donesie ďalšiu. 
   

