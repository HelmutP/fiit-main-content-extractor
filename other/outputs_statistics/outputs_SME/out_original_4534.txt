
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radoslav Procházka
                                        &gt;
                Nezaradené
                     
                 Prší a vonku sa zotmelo... 

        
            
                                    14.4.2010
            o
            11:21
                        (upravené
                14.4.2010
                o
                11:47)
                        |
            Karma článku:
                20.80
            |
            Prečítané 
            14243-krát
                    
         
     
         
             

                 
                    S Jozefom Kanderom, (bývalým) sudcom Najvyššieho súdu, ktorý včera abdikoval, som sa zoznámil v čase, keď sme obaja boli zamestnancami Ministerstva spravodlivosti. Nijak zvlášť sme sa nezblížili, ak by sme sa dnes stretli na ulici, povedali by sme si ledva dobrý deň a bez pristavenia kráčali obaja ďalej.
                 

                 Opakovane som zažil dr. Kanderu, ako nesúhlasil so svojim vtedajším nadriadeným, bývalým ministrom Lipšicom, v priamej konfrontácii, z očí do očí. Nebolo nás takých v zbore, nazvanom Operatívna porada ministra, na húfy, ale príklady naozaj tiahnu, vedeli sme sa viacerí o veci pohádať nielen medzi sebou, ale, pri zachovaní štábnej kultúry, aj s ministrom. Napokon, niekedy mu dal Lipšic, rovnako ako iným nositeľom nesúhlasu, za pravdu, inokedy si ho iba vypočul a urobil to po svojom. Presne tam to aj skončilo, kancelária mu zostala tá istá, plat, predpokladám, takisto, žiadne statusové likvidácie, jednoducho ďalej rozprával a robil to, čo si myslel, že vecne sedí.   Kanderovo rozhodnutie odísť z justície chápem, ale napriek tomu som z neho naozaj zhrozený, pretože ho nedokážem čítať inak ako tak, že ten tlak, ktorému bol, spolu s mnohými inými, vystavený, je účinný, že metódy, kvôli ktorým odchádza, sú naozaj účelné a funkčné, a že  sa úspešne napĺňa scenár vystrnadiť, vytlačiť z dôležitých pozícií nositeľov autonómnych postojov, ktorí pred bezvýhradnou osobou lojálnosťou uprednostňujú vernosť vlastnej predstave o stavovskej povinnosti a cti.   Ak odchádza človek, ktorý sa - podľa mojich informácií, intuície a odhadu - ako sudca nemal dôvod niečoho báť, človek, ktorého profesné renomé je fakt bez poškvrny, tak čo iné mám cítiť, ako obavu z implózie celého stavu. Ak sa poberú aj ďalší, posledný nech zhasne, lebo to, čo bude robiť ten zvyšok, sa aj tak najlepšie robí potme.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (43)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radoslav Procházka 
                                        
                                            Úradnícka vláda ako plán Ficovho návratu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radoslav Procházka 
                                        
                                            Slabá vláda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radoslav Procházka 
                                        
                                            Simpsonovci a Hlinovo hrdinstvo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radoslav Procházka 
                                        
                                            Kto chce ešte po ústach?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radoslav Procházka 
                                        
                                            ... kde je sever
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radoslav Procházka
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radoslav Procházka
            
         
        radoslavprochazka.blog.sme.sk (rss)
         
                        VIP
                             
     
         Zakladateľ Siete. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    92
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8165
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




