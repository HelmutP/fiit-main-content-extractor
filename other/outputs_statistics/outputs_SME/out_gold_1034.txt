

 


 Policjaný portrét teroristky, ktorá odpálila nálož. FOTO ČTK 
 Moskva 31. augusta (TASR) - Výbuch, pri ktorom dnes v širšom centre Moskvy prišlo o život najmenej osem ľudí, bol samovražedným atentátom a spáchala ho žena, informovala Federálna bezpečnostná služba (FSB). Zranených bolo 31 osôb, vrátane detí. Stav najmenej desiatich zranených je vážny. 

 Ruské úrady zatiaľ z atentátu neobvinili čečenských separatistov, i keď útok nesie viaceré znaky im pripisovaných operácií.  

 Veľmi silný výbuch zaznamenali v bezprostrednej blízkosti auta značky Lada zaparkovaného neďaleko vchodu do stanice metra Rižskaja a obchodného centra na Triede mieru v severnej časti Moskvy. Na základe toho sa objavila hypotéza, že 
bombová nálož mohla byť nastražená v zaparkovanej lade. Podľa polície sila výbuchu zodpovedala náloži s 300 až 400 gramami TNT s kovovými úlomkami.  

 Verziu o samovražednej atentátničke potvrdzujú aj výpovede svedkov, ktorí pre televíznu spoločnosť NTV uviedli, že videli ženu, ktorá smerovala ku vchodu do metra. Keď zbadala, že tam policajti okoloidúcim kontrolujú doklady a tašky, otočila a vtedy sa ozval výbuch.  

 Stanica Rižskaja je uzavretá a súpravy metra cez ňu prechádzajú bez zastavenia. Premávka po okolitých cestách sa vážne skomplikovala. 


