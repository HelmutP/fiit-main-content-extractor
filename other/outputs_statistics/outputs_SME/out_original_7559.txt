
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Bašár
                                        &gt;
                Nezaradené
                     
                 Sulík kradne alebo ako Fico trafil prstom do... 

        
            
                                    29.5.2010
            o
            20:37
                        |
            Karma článku:
                13.19
            |
            Prečítané 
            1815-krát
                    
         
     
         
             

                 
                    Súčasný slovenský premiér, Robert Fico, už osočoval kadekoho. Obviňoval z korupcie, kradnutia, netransparentnej privatizácie či šamanstva. Okrem toho sa vždy dokázal pochváliť tým, ako chráni národnoštátne záujmy, pomáha dôchodcom či stavia diaľnice. Doteraz mu jeho taktika pomerne dobre vychádzala, veď pôsobil sebavedomo. Kto by tak aj nepôsobil s preferenciami pravidelne nad tridsaťpäť percent. Nedávno však svoj objekt hanobenia veru netrafil a tak asi pán premiér potrebuje kurz transparentnej súťaže. Takže pán premiér, dávajte pozor:
                 

                   
 Najprv si zadefinujeme, čo transparentnou súťažou nie je. Nuž nie je ňou určite vyvesenie tendra na nástenku na ministerstve. Nie je ňou ani predaj teplého  vzduchu pod cenu. Čo je transparentné by malo byť aj férové a za férové veľmi nemôžeme považovať ani hrubé osočovanie iných politických strán, či už z rozkrádania tohto štátu alebo kradnutia peňazí invalidom. Okrem toho pán premiér, nie je veľmi férové vkladať nič netušiacim cestujúcim výbušniny do batožiny. Moja mamina mi tiež vravela, že sa nemám hrabať cudzím vo veciach. To som ale trošku odbočil.   Za transparentnú súťaž môžeme naopak považovať osemnásť súťažiacich firiem o daný tovar, či zverejnenie všetkych údajov o obchode na internete, kde sú prístupné každému občanovi. Zaujímavé na tomto vašom ťahu je aj to, že tento predaj je starý tuším štyri roky, prečo ho teda vyťahujete teraz? Máte nebodaj pocit, že sa stolička pod vami otriasa? Že skladanie novej vlády možno nebude celkom vo vašich rukách?   Iste, doteraz šlo všetko ako po masle. Bohapusto ste nadávali každému koho ste stretli, novinárom za prílišnu investigatívnosť, opozícii za to, čo spravili, keď tu boli pred vami, koalícii za to, že vám kazia meno. Len veľmi málo tvrdení bolo postavených na konkrétnych a dokázateľných faktoch, no počúvali sa dobre. Bežný občan nemá veľký záujem o to, či bol vzduch predaný pod, či nad cenu. Aj váš program sa dobre číta, samé pozitívne správy, nikde žiadne problémy. Keď sa človek ale pozrie lepšie na všetky vaše tvrdenia, príde na to, že sú postavené na vode, že tam chýbajú akékoľvek dôkazy. A tu pre vás prichádza kameň úrazu.   Koniec vášej neobávanej hegemónie sa nezadržateľne blíži. Nedávno si z koalície urobila trhací kalendár partia opozičných politikov na TA3-jke teraz vám zase Sulík ukázal, že ste trafili prstom do... Čím nás ešte prekvapíte, akej slamky sa ešte chytíte? Sám som veľmi zvedavý.   Verím však v jedno, a to že vám spoločne v štýle Rytmusa budeme môcť dvanásteho júna povedať: „Tak to pochop už je konec Robert!“  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Bašár 
                                        
                                            Prvý krok v kampani a Figeľove KDH sa okamžite mýli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Bašár 
                                        
                                            Anciáša tvojho ministerského
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Bašár 
                                        
                                            Keby tak Fico radšej mojim spolužiačkam peniaze dal
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Bašár 
                                        
                                            Básnické umelecké prostriedky na plagátoch SMERu, SNS a ĽS-HZDS
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Bašár 
                                        
                                            Sklerotik premiér alebo pokračovanie integrácie obmedzených v NRSR
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Bašár
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Bašár
            
         
        basar.blog.sme.sk (rss)
         
                                     
     
        študent
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2409
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dan Ariely - Jak drahe je zdarma
                                     
                                                                             
                                            Tim Harford - The Undercover Economist
                                     
                                                                             
                                            Tim Harford - The Logic of Life
                                     
                                                                             
                                            Steven D. Levitt - Superfreakonomics
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Strážme si stámilióny, ktoré sme požičali Portugalsku
                     
                                                         
                       Pár slov pre pravicu
                     
                                                         
                       Koľko hladných krkov mám na krku?
                     
                                                         
                       Zážitky Číňana v Európe
                     
                                                         
                       Prečo opúšťam Slovensko
                     
                                                         
                       Vláda ide bojovať za pracovné miesta. Podá demisiu?
                     
                                                         
                       List Karolovi: Zažaluj si svojho spisovateľa
                     
                                                         
                       Bratislavské trolejbusy a Kaliňákove popáleniny
                     
                                                         
                       List Karolovi: Len si tak opovrhnúť prezidentom
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




