

   
 Pod lúčmi zapadajúceho slnka a poryvmi večerného chladivého vetra  ukazuje svoj tanec rozkvitnutá višňa. Roztancované konáre vôní, radosti a krásy. 
 Keď ideš okolo, môžeš v týchto chvíľach začuť jej divoký spev.  Keď postojíš a naladíš sa, možno ti višňa ukáže aj svoju tvár. Tvár krásnej dievčiny, ale iba na krátky času mih! 
   
 ((visna)) 
   

