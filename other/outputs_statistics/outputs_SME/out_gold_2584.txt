

 Ústie Tichej doliny minulú jeseň: 
  Zobraziť veľký obrázok 
 Všimnite si, že v podraste vyschnutých, lykožrútom napadnutých stromov, rastie bujná vegetácia tzv. pionierskych drevín. O takúto obnovu lesa sa po kalamite postarala sama príroda, naviac zadarmo. 
  Zobraziť veľký obrázok 
 Pred pár dňami som sa náhodou dostal ku fotografii z ochranného pásma Národného parku Muránska planina a tak sa mi hneď vybavilo pred očami, ako asi bude vyzerať ústie Tichej doliny, ak prejde schválením súčasný návrh zonácie TANAP-u. 
  Zobraziť veľký obrázok 
 Je až zarážajúce k akej devastácii prírody môže dôjsť beztrestne pod zámienkou "ochrany" prírody pred lykožrútom. 
  Zobraziť veľký obrázok 
 Napadnuté stromy v okolí nasvedčujú tomu, že ani takáto masívna ťažba nezabránila lykožrútovi v šírení sa do okolia: 
  Zobraziť veľký obrázok 
 Ak chcete i Vy prispieť svojím hlasom k tomu, aby si nielen Tichá dolina zachovala svoju tvár prírody nezdevastovanej ťažbou, príďte prejaviť svoj nesúhlas s novou zonáciou na  Námestie SNP v Bratislave dňa 14.3.2010 (nedeľa) o 15-tej hodine. 

