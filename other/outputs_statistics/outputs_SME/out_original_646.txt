
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lubos Adamka
                                        &gt;
                O láske, pocitoch, srdci...
                     
                 Dieťa – jednoduchý spôsob ako si pripútať chlapa 

        
            
                                    21.5.2008
            o
            22:17
                        |
            Karma článku:
                15.56
            |
            Prečítané 
            14208-krát
                    
         
     
         
             

                 
                    Keď mi kamarát oznámil, že čakajú s priateľkou dieťa, bol som trochu v šoku, pretože sa poznajú krátko a nikomu z nás, najbližších kamarátov ju ešte neukázal, ale vedeli sme, že dieťa chce a teší sa naň, takže sme sa tešili všetci s ním, až kým...Včera mi po dlhej dobe prišla od neho sms: „Ahoj, nedáme kávu? Najlepšie hneď" Odpísal som, že jasné, veď odkedy sú s frajerkou tehotní som ho ani nevidel a nevedel som, či sa mám tešiť, alebo sa báť. Predsa len, keď sa vám niekto ozve po dlhej dobe je to také divné. A aj v tomto prípade sa moje zlé tušenie naplnilo. Bohužiaľ.
                 

                 
  
  Stretli sme sa v pube kde sme celá partie chodievali hrávať biliard, sadli sme si do boxu a objednali si. Ivan vyzeral strašne, akoby týždeň nespal a nejedol. Zľakol som sa a čakal, čo mi povie. Nemal sa k slovu, len hľadel do chladnúcej kávy a tak som prevzal iniciatívu a opýtal sa ako sa má. Táto otázka prelomila priehradu a spustila potopu sĺz a slov. Pomedzi vzlyky som počúval jeho príbeh...   S priateľkou sa zoznámil a dal dokopy v priebehu týždňa. Bol to vzťah založený skôr na posteli ako na niečom hlbokom a zmysluplnom. Mal za sebou ťažký rozchod s perfektnou ženou. Dodnes neviem, čo sa vlastne medzi nimi stalo a prečo sa rozišli. Ale skrátka, po rozchode nevedel čo so sebou a tak vhupol do tohto nezmyselného vzťahu bez budúcnosti. On, inteligentný a vzdelaný chlap túžiaci po rodine, potrebuje ženu, s ktorou môže občas z tej postele aj vyliezť a rozprávať sa, variť, predstaviť ju rodine, skrátka zaoberat sa aj inými vecami ako len sex, sex a dookola sex. To sa s touto nedalo, a on po tom na začiatku ani netúžil, chcel si len zasexovať a zabudnúť na bývalú, ale toto človeka prestane baviť a tak im to začalo po pár mesiacoch škrípať. Mozog sa mu vrátil z nohavíc do hlavy a začal uvažovať nad tým, že sa prestane schovávať pred problémami a urobí si poriadok so životom. Súčasťou toho bol aj koniec tohto nezmyselného vzťahu. Bol každým dňom chladnejší a chystal sa s frajerkou rozísť, ale ona mu zmenila plány...     Raz večer prišiel domov, kde ho čakala ona a celá šťastná mu oznámila, že je tehotná. Bol to pre neho šok, zmenilo mu to život naruby, ale interrupcia neprichádzala do úvahy a po rodine predsa túžil. Síce s niekým úplne iným ako bola táto žena, ale šmyklo sa a bolo to koniec koncov aj jeho dieťa. Zosumarizoval si situáciu, uvedomil si, že toto už rozchod nevyrieši a tak začal plánovať rodinu a presviedčať sa, že je šťastný. Na potomka sa úprimne tešil a tie okolnosti okolo boli realita, ktorú nevedel zmeniť a tak ju prijal a bol ako tak šťastný. Aspoň si to myslel.     Dni ubiehali, mesiace tiež a prišiel začiatok štvrtého mesiaca. Tešil sa, že dieťa bude už väčšie, bude kopať a že on bude môcť ísť s frajerkou na sono a uvidí svojho malého miláčika. Ale...     V jeden deň prišiel domov z práce a zbadal frajerku sedieť na gauči v slzách. Zľakol sa a spýtal sa čo sa deje. Povedala mu, že ráno jej prišlo zle v práci a začala krvácať. Kolegyňa ju odviezla okamžite do nemocnice, ale tam lekár len skonštatoval, že potratila a už s tým nič nevedia urobiť. Vraj asi nebolo niečo v poriadku, že mala prísť skôr, ale bohužiaľ, už je neskoro, že... Povedal jej, že ju pošle na vyšetrenia a ak budú nejaké komplikácie, musia si ju nechať na pozorovaní. Ale ak bude všetko v poriadku, v rámci možností, môžu ju poslať domov, že asi jej bude v tejto situácii lepšie doma s rodinou. Na vyšetreniach bolo všetko v poriadku, poslali ju domov s tým, nech sa týždeň nenamáha a inak môže normálne žiť ďalej a po čase sa pokúsiť o ďalšie dieťa, keďže vyšetrenia neukázali žiadnu zjavnú príčinu, ktorá by jej bránila otehotnieť, alebo dieťa v poriadku donosiť.     Kamarát si usrkol z kávy a pokračoval. „Vraj mi chcela zavolať, ale v tom zmätku si nechala v robote mobil a nevie moje číslo naspamäť. A prišla domov len pred chvíľou, tak sa rozhodla mi to povedať osobne"     Sedel som tam a nevedel, čo mám pri pohľade na toho zúboženého človeka robiť. Nemám deti, ale viem si predstaviť, aká to musí byť bolesť, prísť o dieťa, keď po ňom túžite.     Ivan po chvíli hovoril ďalej: „Plakali sme, ona bola zničená, ja ešte viac. Ale musím priznať, svojím spôsobom sa mi uľavilo - dieťa chcem, ale ešte na to nebol najvhodnejší čas."     Hovoril ďalej, ako si po pár dňoch uvedomil, že ju chcel opustiť, že sa chcel rozísť. Uvedomil si, že za ten čas, čo čakali dieťa sa na jeho pocitoch nič nezmenilo a bol s ňou len z povinnosti. A je aj teraz, ale nemôže sa s ňou predsa rozísť, keď potratila jeho dieťa. Bolo by to od neho riadne svinstvo. Tak s ňou ostal a nejako žil. Povedal si, že nateraz to je takto a neskôr sa uvidí, čo ďalej.     Všetko nejako išlo, jemu liezlo na nej na nervy všetko a teraz si to uvedomoval omnoho viac ako vtedy, keď chcel ich vzťah ukončiť. Asi to bolo preto, že teraz vedel, že sa s ňou rozísť nemôže. Ale bolo tu aj niečo iné, čo si uvedomil až teraz, s odstupom času. Nechápal, ako to, že to nevidel hneď, ako ho mohla tak klamať.     Počúval som a nevedel som, o akom klamstve to hovorí. On zrazu pozrel na mňa tým svojim zničeným pohľadom. „To dieťa neexistovalo. Vymyslela si ho, aby si ma udržala. Cítila, že nám to škrípe a vedela, že otehotnieť naozaj nie je také ľahké a nejde to hneď a že dieťa ešte nechce. Ale potrebovala nejako zariadiť, aby som s ňou ostal - som relatívne dobre zarábajúci, mám isté spoločenské postavenie, byt, vedela, že na interrupciu ju nepošlem a že som chlap, ktorý sa o ňu postará a nenechá ju. Tak to využila a dieťa si vymyslela."     „Mala to fakt dobre premyslené, chcela ma také 4 mesiace držať v tom, že čakáme dieťa. Moje znalosti z gynekológie sa obmedzujú na to, že žena má menštruáciu raz za mesiac. A dosť. Naozaj nemala ťažké ma oklamať. Aby som si nič nevšimol, tak si menštruáciu vynechala pomocou antikoncepcie, iba raz keď som bola na služobnej ceste využila moju neprítomnosť a vysadila ju. Nebude si predsa ohrozovať zdravie."     Počúval som a nevychádzal z údivu. Myslel som, že horšie to už nebude, ale Ivan pokračoval: „Nikdy som s ňou nešiel k doktorovi, vždy mala kontrolu vtedy keď som ja bol v práci a keď náhodou nie tak vravela, nech nejdem, vraj tam budeme dve hodiny čakať a potom ju len vyšetria a pôjde domov, na sone by vraj ešte nebolo nič vidieť. Vravel som si, tak fajn, pôjdem s ňou keď už bude dieťa väčšie. Lenže ona to mala premyslené, mala naplánovaný potrat, vymyslela to tak, aby som s ňou nemohol ísť do nemocnice a aby mi to nebolo divné. Naozaj mi ani nenapadlo, že po potrate musíš ostať aspoň 24hodín v nemocnici, ak nie viac. Využila moju neznalosť a mala takmer 4 mesiace na to, aby si to vymyslela. Kurva. Vraj keď potratí, má ma zabezpečeného ešte aspoň na rok, lebo som taký idiot a neopustím ženu, ktorá čakala moje dieťa. A potom možno už otehotnie aj naozaj a nechá sa živiť. Zarábam dobre a ona vedela, že o svoje dieťa by som sa postaral. A aj o jeho matku. Vravela, že so mnou nechce žiť, vraj som namyslený idiot, ale to jej nebráni žiť z mojich peňazí."     Opýtal som sa: „A toto všetko ti povedala? Priznala sa?"     „Nie, nie, prišiel som dnes skôr domov z práce a otvoril som dvere potichu, že ju prekvapím. Ale akurát telefonovala s nejakou kamarátkou a vypočul som si ich rozhovor. Stál som v predsieni a počúval ako sa smeje z toho, že som jej to celé zožral. Stála v mojom byte a robila zo mňa úplného debila."     „A čo si urobil potom?"     „Zložila a s ironickým úsmevom sa otočila. Zbadala ma a úsmev jej zamrzol. Doteraz mám pred očami ten jej ksicht v momente keď sa otočila. Prišlo mi na zvracanie, pokúšala sa mi to vysvetliť, ale nepočúval som." Potom som ju vyhodil z bytu, zbalil jej veci a tašku hodil za ňou a napísal ti.     Nemal som slov, nevedel som ani čo mu povedať. A tak pokračoval on: „Vieš, asi som to celý čas tušil, ale... toto nie je vec, ktorá ti len tak napadne. A mohol mi byť podozrivý ten potrat, ale som asi naivný debil a nedošlo mi to. Keby som ju nepočul, asi sa o tom v živote nedozviem a ona si spokojne žije a nechá ma, aby som jej ten život platil a ešte sa aj cítil ako najväčší sviniar. Neviem, čo teraz robiť. Som zničený, nechápem, ako mohla takéto niečo urobiť, ako vôbec niekto môže takéto niečo urobiť. Nechápem to, ale asi mi svojím spôsobom strašne odľahlo. Že môžem urobiť to, čo som chcel, začať odznova bez nej. A mať rodinu so ženou, ktorá si zaslúži moju lásku a bude ma milovať naozaj." Keď toto povedal, zablyslo sa mu v očiach a dokonca sa usmial. Ešte chvíľu sme debatovali, potom sa zdvihol a odišiel. S prísľubom, že sa bude znovu zúčastňovať našich pánskych jázd a frajerku nám príde ukázať na schválenie hneď ako sa nejaká na neho ulakomí.     Asi nikdy nepochopím, ako môže niekto niečo takéto urobiť. Asi som staromódny, keď verím na lásku. Asi som idiot, keď verím na dôveru. Asi som úplný blázon, ale nepochopím, ako môže niekto predstierať tehotenstvo, ako môže niekto tak ublížiť človeku, ktorému tvrdí, že ho ľúbi. Ale na druhej strane som rád. Som rád, že kamarát na to prišiel, som rád, že to malé nebolo, pretože takto môže mať rodinu s niekým, kto o to bude naozaj stáť. Som rád, pretože je šťastný on. Aj keď asi chvíľu potrvá, kým sa s tým vyrovná a kým bude schopný začať nejaký vzťah, mať rodinu...A verím, že takéto ženy sú výnimkami, že existujú ženy, ktoré dokážu bezhranične ľúbiť...A že Ivan na takú natrafí. A že keď ju nájde, bude sa jej držať ako kliešť a nepustí ju. A bude šťastný.     A verím, že takéto mrchy budú všetky odhalené, aby boli bábätká len skutočné.     Predpokladám, že už sa ma nikto viac nespýta na môj status na ICQ:     „Women are the reason why falling in love is so DAMN hard..."  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (117)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubos Adamka 
                                        
                                            Úvaha o láske...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubos Adamka 
                                        
                                            Prečo je láska slepá a vždy ju sprevádza bláznovstvo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubos Adamka 
                                        
                                            Premýšľanie v noci...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubos Adamka 
                                        
                                            Aké to je, keď hviezda pošle sen...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubos Adamka 
                                        
                                            Jedno slovo, jeden úsmev, jeden bozk...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lubos Adamka
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lubos Adamka
            
         
        adamka.blog.sme.sk (rss)
         
                                     
     
        Taky nenormalne normalny chalan...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2502
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O láske, pocitoch, srdci...
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Paulo Coelho - Ako rieka, ktorá plynie
                                     
                                                                             
                                            Gabriel Garcia Márquez - Sto rokov samoty
                                     
                                                                             
                                            Sergio Bambarén - Delfín
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Desmod - Symfónia Prázdna
                                     
                                                                             
                                            Nelly Furtado - What I Wanted
                                     
                                                                             
                                            Lenny Kravitz - I'll Be Waiting
                                     
                                                                             
                                            GooGoo Dolls - Before Its Too Late
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Deeejnuskin blog
                                     
                                                                             
                                            Petin blog
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




