
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Šimčík
                                        &gt;
                Británia
                     
                 English Defence League 

        
            
                                    30.5.2010
            o
            12:17
                        (upravené
                30.5.2010
                o
                12:48)
                        |
            Karma článku:
                5.03
            |
            Prečítané 
            1285-krát
                    
         
     
         
             

                 
                    Nastupujúca hrozba pre multikulturárnu spoločnosť vo Veľkej Británii. Novovzniknuté britské krajne pravicové hnutie plánuje v letých mesiacoch vyvolať rasové nepokoje.
                 

                 
  
   English Defence League (Anglická obranná liga) je najvýraznejším pravicovým pouličným hnutím v Británii od éry National Frontu v 70. rokoch minulého storočia. Podľa reportéra denníka The Guardian, ktorý niekoľko týždňov pôsobil utajene vo vnútri hnutia,  EDL plánuje najbližších týždňoch pochodovať v najsilnejších moslimských komunitách v Británii.  EDL vznikla v meste Luton minulého roku ako reakcia na demonštráciu malej extrémne skupine extrémistov počas pochodu vracajúcich sa vojenských jednotiek. Odvtedy chaotická zmes ľudí sústredená okolo futbalových chuligánov, bola schopná spojiť tisícky ľudí v boji proti "islamskému extrémizmu". Hnutia sa síce označuje ako mierumilovná a nerasistická organizácia, podľa miestnych antifašistov jej členmi sú aj bývalí príslušníci krajne pravicových hnutí National Front, Combat 18 alebo BNP. Hoci jadrom hnutia je belošská mládež, často podgurážená alkoholom alebo drogami, ich islamofobický odkaz dokáže zoskupiť zvláštnu koalíciu ľudí -  od pravicových kresťanských hnutí až po aktivistov za práva gayov (práve táto divízia EDL združuje asi 115 členov).       Minulý rok EDL spolu s inými podobnými hnutiami na "konferencii" v Švédsku vyhlásilo, že je potrebné vybudovať hnutie anti-džihádu, vyjsť na ulice a začať budovať spojenectvá s drsnejšími hnutiami ako sú napr. futbaloví chuligáni.  Podľa akademika Mathewa Goodwina dôvodom prečo je islamofobický odkaz EDL pre ľudí príťažlivý je že na rozdiel od 70. rokov minulého storočia kedy National Front sa spájal s antisemitizmom, dnes sú niektoré sekcie britských médií a spoločnosti relatívne sympatické islamofóbii. V médiách nieje ťažké nájsť hostilné názory voči moslimom alebo islamu. Tento stav je úplne odlišný k 70. rokom kedy každý denník alebo politik odsudzoval antisemitizmus National Frontu.  Hnutie má momentálne okolo 3000 členov a okolo 100 online divízii ako napr. novovzniknutá divízia ozbrojených síl s 800 členmi. Minulým mesiac v meste Dudley demonštranti EDL strieľali na policajtov rakety, strhli bariéry a pokúšali sa napadnúť antirasistických protestantov a Ázijcov. V najbližších týždňoch hnutie plánuje protesty v Newcastle, Cardiffe a Bradforde.   Zdroj: guardian.co.uk    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Ako Maťo Ďurinda reggae hral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O retre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O hmle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O nechcených vianočných darčekoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Keď jeden chýba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Šimčík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Šimčík
            
         
        simcik.blog.sme.sk (rss)
         
                        VIP
                             
     
        Agropunk, vraj aj Mod, stratený ako working class hero na východoslovenskom vidieku.  Rád chodím na futbal, koncerty, do pubu, kostola a z času na čas aj na disco :)

"Myslím, teda slon"


  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    186
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názory
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Subkultúry
                        
                     
                                     
                        
                            Británia
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Čo sa domov nedostalo
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Roger Krowiak
                                     
                                                                             
                                            Rudolf Sloboda - Do tohto domu sa vchádzalo širokou bránou
                                     
                                                                             
                                            Subculture - The Meaning Of Style
                                     
                                                                             
                                            F.M. Dostojevskij - Idiot
                                     
                                                                             
                                            NME
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Arctic Monkeys
                                     
                                                                             
                                            The Gaslight Anthem
                                     
                                                                             
                                            Slovensko 1
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            BBC Radio 2
                                     
                                                                             
                                            BBC Mike Davies Punk Show
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tatran Prešov
                                     
                                                                             
                                            Bezkonkurenčný Dilbert
                                     
                                                                             
                                            flickr
                                     
                                                                             
                                            discogs
                                     
                                                                             
                                            ebay.co.uk
                                     
                                                                             
                                            Banksy
                                     
                                                                             
                                            Kids And Heroes
                                     
                                                                             
                                            Roots Archive
                                     
                                                                             
                                            West Ham United
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            denník Guardian
                                     
                                                                             
                                            denník Pravda
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




