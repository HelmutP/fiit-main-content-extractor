
 Systém je teraz iný. Treba sa objednať. Naštastie za objednanie sa neplatí, objednáte sa na presný deň a hodinu a idete. Aké by to mohlo byť fajn, keby...

Keby sa dalo objednať telefonicky. Na telefónnom čísle totiž dlhodobo nikto nezdvíha. Tak sa teda treba objednať osobne. 12:00 a ja stojím zase raz v plnej čakárni. Chvíľku počkám, keď sa nič nedeje skúsim zaklopať. Až potom sa dozvedám, že síce sa má otvárať o dvanástej, ale že dnes doktor bude meškať a príde až o druhej. Oblieva ma studený pot, čo je našťastie v týchto horúčavách celkom príjemné. A zostávam čakať.

Toto už nie je o tom, že je veľa pacientov, že sa nestíha. Toto je vyslovené hádzanie fekálií na pacientov - zákazníkov. Najhošie na tom je, že mi nič iné neostáva. Ak by sa toto dialo v súkromnej firme, tak sa rozlúčim a idem ku konkurencii. Tu však nemám na výber. a potvrdzuje sa tu pravidlo - zdravie si za peniaze nekúpite.

PS: stále čakám 
