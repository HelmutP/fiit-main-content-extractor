

 
Je sedem hodín ráno nastupujeme na vlak pre zamestnancov elektrárne. Neplatíme žiadne cestovné, vlak má len jednu zastávku - Černobylskú elektráreň. Počas tridsiatich minút prechádzame menším výbežkom Bieloruska. V okolí sa sporadicky objavujú tabuľky so znakom rádioaktivity, celé územie je bažinaté s menšími stromami.
 
 
Po príchode do elektrárne sa nachádzame v uzavretom priestore, na jeho konci je vojenská kontrola a Mitch ukazuje naše povolenie. Čaká na nás mikrobus ktorým sa rýchlo presunieme do Pripjate, počas celej exkurzie som meral radiáciu. Najvyššia bola z poškodenej strany elektrárne v spádovej oblasti. Tadiaľ vedie aj cesta do Pripjate, hodnoty sa pohybovali od 10 do 22 mikrosievertov za hodinu.
 
 
Pred Pripjaťou je vojenská stanica, nefotíme. Prechádzame dlhou alejou až na hlavné námestie, všetky cesty boli dekontaminované, pohybovať sa po tráve Mitch neodporúča a potvrdzuje to aj dozimeter. Miera rizika je relatívna, kontaminácia hrozí pri fyzickom kontakte - ak sa rôzne látky dostanú na pokožku alebo do tela je veľmi ťažké sa ich zbaviť a môže to trvať aj niekoľko rokov.
 
 
((pripjat))
 
 
Nemali sme strach z radiácie lebo jej hodnoty sa pohybovali v rozmedzí od 0,3 do 5 mikrosievertov, väčším nebezpečenstvom sú chátrajúce budovy a prepadliská v cestách a trávnikoch. Raz začnú padať aj paneláky... Nenarazili sme na žiadne zmutované zvieratá ani podivné úkazy, faunu v meste tvorí zväčša vtáctvo, Mitch spomenul aj divé svine a vlky, no tie sme nestretli. Väčšina žije v okolí Červeného lesa, miesta ponad ktoré prechádzal rádioaktívny mrak. Osobne ma veľmi zaujalo detské ihrisko s ruským kolom, kolotočmi, hrdzavým autodrómom a rozpadnutými lavičkami. Je až nepredstaviteľné že ľudia museli z mesta odísť prakticky zo dňa na deň, v bytoch ostalo všetko...  
 
Po dvoch hodinách nás už na dohodnutom mieste čaká mikrobus a presúvame sa k pamätnému miestu priamo pred sarkofág. Jeho vzdialenosť bola asi sto metrov. Je to obrovský kolos zakrytý hrdzavými plechmi. Vedľa pamätníka je prezentačná miestnosť a veľmi podborným modelom elektrárne, hlavne bloku 4. Tu sme dostali informácie o havárii, jej priebehu a súčasnom stave elektrárne, pani nám dovolila prejsť aj na otvorený balkón z ktorého bol najlepší výhľad na sarkofág, nemohli sme však fotiť. Na tomto mieste som nameral rekordné hodnoty radiácie, až 20 mikrosievertov. Prezentačná miestnosť disponovala aj vlastným dozimetrom, tento ukazoval len 9, na moju otázku prečo mi nevedeli odpovedať...
 
 
((elektraren)) 
 
Pred sarkofágom sme sa zdržali maximálne 10 minút a pešo sme prešli celé okolie elektrárne až k hlavnému vchodu. Do vnútra sme museli prejsť cez detektor kovov. V miestnosti biologickej ochrany sme dostali jednorázové návleky a plášte. Presunuli sme dlhou chodbou až do blokovej dozorne číslo jedna. Chodba ktorou sme šli vedie cez všetky bloky až k tomu štvrtému, na jej koniec som nedovidel... ale lákalo nás sa tam ísť pozrieť. Mitch poznamenal, že rozšírená exkurzia elektrárne je povolená len vedeckým týmom... mám z toho v hlave chrobáka. Operátor smeny, ktorý celý deň kontroluje vypnuté prístroje nám na úvod prichystal planý poplach :)) Všetko blikalo a pípalo ale nestihol som to nafilmovať. Zaujímavosťou bola obrazovka zachytávajúca vrch reaktora a navážací stroj , nerozumiem ako si nemohol počas katastrofického testu operátor nevšimnúť že sa mu nadvihujú olovené bloky na obrazovke. 
 
Konečne obed 
 
 
Mitch nás pozýva do miestnej kantíny, prichádzame tesne pred zatvorením ale stále je na výber z množstva jedál. Poobede prechádzame starým zelezničným mostom vedľa nedostavaného bloku 5 – podľa programu sme mali ísť aj sem ale nevychádzal nám čas. Zastavili sme sa aspoň na moste a hádzali chlieb obrovským sumcom, jeden vyzeral ako žralok. Je tam kontaminovaná voda a preto ich nikto neloví... Pomaly sa presúvame mikrobusom cez mesto Černobyl, je tu množstvo zarastených a opustených domov. Ku koncu absolvujeme v elektrárni ešte výstupnú radiačnú kontrolu v špeciálnom prístroji, všetko prebehlo hladko a odchádzame vlakom smerom na Slavutich. 
 
Bola to namáhavá ale hlavne poučná exkurzia... oblasť už nie je taká nebezpečná ako kedysi. Zo zóny je zakázané brať akékoľvek veci a pokiaľ človek dodržiava pravidlá, nič nehrozí.
 
 
10.01.2009 - Pridal som fotografie z Pripjate  a Černobylu , fotené kamerou Panasonic  
 

