
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marianna Kawaschová
                                        &gt;
                Nezaradené
                     
                 Okradnú vás a vy ani neviete kedy 

        
            
                                    8.5.2010
            o
            21:35
                        (upravené
                8.5.2010
                o
                22:14)
                        |
            Karma článku:
                6.32
            |
            Prečítané 
            422-krát
                    
         
     
         
             

                 
                    Presne to sa stalo včera aj mne. Keď som sa v jednom nemenovanom bare v Bardejove lúčila so strednou školou aj s mojimi spolužiakmi. Kedže sme neboli doma, okolo 18 nás pochytil hlad a tak ma poslali na nákup do potravín.
                 

                 Vzala som si igelitku, peniaze a mohlo sa ísť. Nákup som spravila a peňaženku som si dala do tej igelitovej tašky. Po návrate do baru, som dala kamarátke tašku a ja som medzi tým išla na záchod a cestou som sa ešte porozprávala s kamarátom. Keď som sa vrátila ku triede zabudla som, že peňaženku som dala do tašky. Spomenula som si na ňu až o 21:00, keď sme sa presúvali do druhého baru.   Prvé pocity, ktoré na mňa prišli, keď som zistila, že nemám peňaženku, bolo zúfalstvo, beznádej a smútok. Nevedla som čo robiť, slzy sa mi valili dole lícami. Prvá vec či mi napadla bolo, že mám zatelefonovať jednej mojej kamarátke. Tak som to aj hneď vykonala a ona do pár minút bola pri mne. Vysvetlila som, čo sa stalo a vrátili sme sa do baru, kde sme ju už bohužial nenašli. Taktiež sme prehľadali smetné koše a v jednom z nich sme našli našu igelitku. Len čo som ju uvidela, mala som pocit nádeje, že nie je všetko stratené. No len čo som ju otvorila a zistila, že tam nie je, opäť nastal pocit zúfalstva. Mala som chuť kričať, niekoho zbiť, ale moja kamarátka stála stále pri mne a ukľudňovala ma.   Neostalo mi už nič iné, len zavolať policajtov. Vtedy som nebola schopná telefonovať a tak ma kamarátka opäť podržala a zatelefonovala ona. Na policajtov sme čakali pol hodinu a z mojich úst vyšli toľko nadávok, koľko som za celý život nevyslovila. Keď policajti prišli, vzali mňa aj moju kamarátku na políciu. Tam som ohlásila krádež, zrušila občianku a poslali ma domov. Bolo ešte len pred 23 a ja som bojovala sama so sebou. Na jednej strane som sa chcela ísť ďalej baviť s mojimi spolužiakmi, ale moje svedomie mi to už ndovolilo. Nemohla som na to len tak zabudnúť a tváriť sa, že sa nič nestalo. Preto svedomitosť vo mne vyhrala a ja som išla domov.   Noc bola zničená a pri tom to mal byť krásny zážitok na celý život. Ale poviem vám, bude to zážitok. Do 21 to bude nádherný zážitok, lebo som sa vážne skvele zabavila a po 21 to bude zážitok, ktorý mi prinesie ponaučenie na celý život. Pochopila som, že aj keď som si na veci dávala stále pozor, bolo to málo. Taktiež som sa naučila, že už nemôžem byť tak roztržitá ako kedysi.   Veľa ľudí tvrdí, že len blbec sa učí z vlastných chýb, ale ja si myslím že práve z vlastných chýb sa poučíme najviac. 5 ľudia okolo nás môžu spraviť tú istú chybu, a my sa z nej predsa nepoučíme toľko, ako keď tu chybu spravíme my. Ja som sa zo včerajška veľmi poučila a už teraz viem, že si budem dávať na svoje veci oveľa väčší pozor. Veď všetko zlé, je predsa len na niečo dobré... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Kawaschová 
                                        
                                            Na čom skutočne záleží ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Kawaschová 
                                        
                                            Homosexuáli, aj oni chcú ľúbiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Kawaschová 
                                        
                                            Krič kým vládzeš, na pódium prichádza IneKafe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Kawaschová 
                                        
                                            Stratila som sa na ceste môjho života ... nájdem ju?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Kawaschová 
                                        
                                            Muž, ktorý mi dal iný pohľad na život
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marianna Kawaschová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marianna Kawaschová
            
         
        kawaschova.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    396
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




