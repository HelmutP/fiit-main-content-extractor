
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lukáš Jakubička
                                        &gt;
                Cestovanie
                     
                 Káhirské dobrodružstvo: Doprava 

        
            
                                    3.6.2010
            o
            10:10
                        |
            Karma článku:
                8.01
            |
            Prečítané 
            1483-krát
                    
         
     
         
             

                 
                    Doprava v Káhire je kapitola sama o sebe. Ak hovoríme o Káhire ako o  najznečistenejšom meste na svete, hovorme aj o príčine - o miliónoch  áut, ktoré sa každodenne premávajú večne upchatými cestami. Emisná  kontrola je tu neznámym pojmom. Často krát vidíte kúdeľ čierneho dymu  valiaceho sa z výfuku 30 ročného „žiguláka", ktorý sa hrdí nápisom TAXI.  Zlé jazyky hovoria, že ak bude počet dopravných prostriedkov rásť  rovnakým tempom ako za posledné roky, Káhira sa o 15 rokov zastaví.  Nastane stav, keď sa bude plocha všetkých áut rovnať ploche cestných  komunikácií.
                 

                 Pravidlá a organizácia dopravy   Jazdí sa na pravej strane a to je jediná vec, ktorá je rovnaká ako doma na Slovensku. Doprava sa pohybuje zväčša v jednom smere, preto tu nenájdete veľa križovatiek. Ak chcete odbočiť do uličky po ľavej strane, tak si musíte počkať na otočku na špeciálnom mieste, ktorá vás vyvedie do opačného pruhu. Niekedy to vyžaduje aj pol kilometrovú nadchádzku. Dva protichodné pruhy delí namiesto plnej čiary asi 40 cm vysoký obrubník. Semafory nájdete len na veľkých križovatkách, no nie všade ich aj rešpektujú. Často vidím blikať naraz oranžové a zelené svetlo a pýtam sa sám seba čo to vlastne znamená :) Policajti sú tu obvykle tí, ktorí riadia dopravu. Čudným mávaním ruky spôsobom „ku mne" dávajú autám najavo, aby si švihli.   Je normálne ak na ceste s tromi pruhmi nájdete päť áut stojacich tesne vedľa seba; spätné zrkadlá si neraz rozdávajú vášnivé bozky. Obvyklými dopravnými prostriedkami sú okrem áut, mikrobusov, autobusov a motoriek aj kone s prívesom, sem tam nejaký ten somárik s vozíkom plným melónov. Musím uznať, že kapacita dopravných prostriedkov je využitá na maximum. Vidieť štvorčlennú rodinku na jednom mopede nie je zvláštnosťou.   Trúbenie je spolu s arabčinou oficiálnym jazykom Káhiry a to myslím vážne. Vedia pomocou trúby nadávať povedať „Milujem ťa", „choď do čerta" a iné často používané slovné spojenia. V kombinácii so svetelnými signálmi je to hotové umenie. Koniec koncov, myslím že tu platí iba jedno pravidlo - má prednosť ten, kto trúbi hlasnejšie.   Prechádzanie cez cestu   Adrenalín. Žiadny bungee jumping alebo niečo podobné, stačí ísť do Káhiry a prejsť pár krát cez cestu! Chodci sú tu ozajstnou súčasťou dopravy. Nie je nezvyčajné nájsť ľudí prechádzať z jednej strany diaľnice na druhú. Odvaha a načasovanie sú základné parametre úspechu. Niekedy musíte zastaviť v polovici cesty, pretože nestíhate prebehnúť na druhú stranu. Autá okolo vás premávajú a vy čakáte na ďalší správny  moment dokončiť vašu misiu :)   Pozrite si video môjho poľského kamaráta Krzycha, odporúčam aj jeho foto blog.   Verejná doprava   „Nezastavujeme, máme spoždení" tu funguje na dennom poriadku. Ľudia nastupujú za jazdy do preplneného autobusu. Vidieť ľudí trčať z dverí autobusu rútiaceho sa 50tkou cez mesto nie je žiadna rarita. Autobus a metro sú lacnou alternatívou prepravy. Keď som bol v Káhire prvý krát (r. 2006), zažil som v metre hneď dva menšie kultúrne šoky. Prvým bol fakt, že nikto nečakajú pokiaľ  ľudia vyjdú z vagónu, všetci sa naraz začnú naraz hrnúť dnu i von. Druhým bol fakt, že vagóny v strede sú vždy vyhradené iba pre ženy a sú označené zelenými a červenými nálepkami.   Metro ostáva jednoznačne najrýchlejšou alternatívou. Škoda, že nie je kompletne dobudované - veľká časť Káhiry je stále nepokrytá.   Taxi   Taxíky sú veľmi rozšíreným dopravným prostriedkom. Delia sa podľa farby na čierne; biele a žlté. S vodičmi čiernych taxíkov zjednávate cenu (nemajú taxameter), do bielych iba nastúpite a požiadate ich nech zapnú taxameter (arabsky „Adaat"). Ak nepoznáte cestu ale viete približnú cenu, zoberiete si radšej čierny taxík a dohodnete sa na cene ešte predtým ako nastúpite. Ak vodič od vás pýta viac, pošlete ho ďalej a počkáte si na ďalší :) U bielych taxíkov je riziko, že vás povozia po rôznych uličkách, len aby nabehli nejaké tie peniažky na taxametri. Ak nepoznáte cestu, môžete nakoniec zaplatiť dvojnásobok v porovnaní s čiernym taxíkom. Približná cena za 10 minút jazdy (bez zápchy) je 5 egyptských libier, čo je cca 60 centov.   Jazda autom   Hrali ste niekedy hru Need For Speed? Ak existuje miesto na zemi, ktoré sa podobá tejto hre, tak je to Káhira! Obiehanie sprava? Žiadny problém! Slalom medzi autami? Žiadny problém! :) Prekážky na ceste? Áno - autobusy, ľudia, motorky, kone a somáre :) Ideálne miesto na adrenalínovú jazdu, nie však v rušných hodinách.   Predstavte si scénu z filmu, kedy zopár lupičov za bieleho dňa vykradne banku. Utekajú s peniazmi von z banky, nasadnú do auta a prefrčia ulicami mesta. V Káhire? Žiadna šanca! Tu prefrčíte tak akurát za prvú zákrutu a potom „frčíte" v zápche ďalšiu hodinu - policajti vás dobehnú aj keď pôjdu peši :)   Dobrodružstvo. To je myslím ten správny výraz pre akýkoľvek tunajší styk s dopravou. Obyčajný prechod cez cestu, zastavenie taxíka, nasadanie na autobus, jazda autom... to sú všetko veci, ktoré sú úplne odlišné ako tie naše, na Slovensku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Jakubička 
                                        
                                            Káhirské dobrodružstvo: Revolučné zápisky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Jakubička 
                                        
                                            Kahirské dobrodružstvo: Papuča
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Jakubička 
                                        
                                            Kahirské dobrodružstvo: Šesťmesačná story
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Jakubička 
                                        
                                            Volím, nevolím. Žijem, nežijem.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Jakubička 
                                        
                                            Káhirské dobrodružstvo: Na úvod
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lukáš Jakubička
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lukáš Jakubička
            
         
        jakubicka.blog.sme.sk (rss)
         
                                     
     
        Som ambiciózny mladý muž. Vyštudoval som Žilinskú univerzitu v Žiline. Už počas školy som začal pracovať v neziskovom sektore, kde som zotrval šesť rokov. Následne som rozbiehal projekt návratu talentovaných Slovákov naspäť na Slovensko. Jeden rok som pracoval a žil v Káhire a Arabskú jar som zažil na vlastnej koži. Momentálne som naspäť na Slovensku. Žijem v Bratislave a robím to, čo ma baví. Zaujímam sa o rozvoj ľudského potenciálu, spoločnosti a o nové technológie. Spolieham sa na zdravý sedliacky rozum a verím v silu jednotlivca. Rád športujem, cestujem a čítam literatúru na rozvoj osobnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1656
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Myslienky
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




