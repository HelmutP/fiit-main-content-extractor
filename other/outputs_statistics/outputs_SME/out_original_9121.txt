
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ingrid Galbavá
                                        &gt;
                Život
                     
                 Je po voľbách - zabudnite ! 

        
            
                                    18.6.2010
            o
            10:44
                        (upravené
                18.6.2010
                o
                11:09)
                        |
            Karma článku:
                5.34
            |
            Prečítané 
            831-krát
                    
         
     
         
             

                 
                    Prší. Ešte nie je upratané a opravené to, čo spôsobili dažde a z nich vyplývajúce povodne a už je tu nová hrozba, ďaľšie predpovede, ktoré hovoria o zvýšení rizika povodní. Deň, čo deň máme možnosť sledovať v správach stav príbytkov zasiahnutých oblastí, stav ciest, bezmocnosť až zúfalosť ľudí, ktorí prišli o strechu nad hlavou a ktorým nemá kto pomôcť. Tí, čo už pomoc dostali, sú takisto nespokojní, pretože rozdelenie peňažných príspevkov vraj nebolo spravodlivé a príspevky dostali aj takí, ktorí by sa zaobišli aj bez nich, na úkor tých, ktorí túto pomoc skutočne potrebujú.
                 

                Príroda je zatiaľ hádam jediná , ktorá nepodlieha politickým vplyvom. Snáď len skutočnosť, či ju politici budú, či nebudú chrániť je pod týmto vplyvom. Rozkázať vetru a dažďu sa im však zatiaľ nepodarilo. A tak milý dážď prší, kedy chce, bez ohľadu na to, či sa blížia voľby, či práve prebiehajú, alebo je po nich. A tak je veľkou smolou tých, čo bývajú v ohrozených miestach, že sa im udiala škoda práve v čase, keď jedna vláda zdržuje postavenie inej a tá len trpezlivo čaká a robí všetko preto, aby mohla prevziať žezlo a chopiť sa povinností. Iróniou osudu je fakt, že zdržuje práve tá vláda, ktorá sa celý čas hrdí prívlastkom sociálna. Dokonca si dovolí o sebe tvrdiť, že je "SILNOU sociálnou vládou". Logicky by preto volič očakával, že práve táto vláda, kým je ešte pri moci, lebo si myslí, že tam má ešte čo dokázať, by mala ukázať svoju silu a sociálnosť práve v takýchto situáciách, keď je na jej milosť a nemilosť volič odkázaný, pretože sa dostal do úzkych. Lenže, to by najprv muselo byť pravdou, že odchádzajúca vláda je SKUTOČNE takou , za akú sa celý čas vydáva. Keby volič nemal krátku pamäť, práve podobné situácie by ho mali presvedčiť o tom, kto to s ním myslí skutočne dobre a kto sa len tvári ako spasiteľ. Keby totiž jednal v záujme občanov, to najmenej, čo by pre nich mohol urobiť, by bolo aspoň uvoľniť priestor tým, ktorí majú záujem pre ľudí niečo spraviť, keď už ten záujem nemá on sám.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Galbavá 
                                        
                                            Strážca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Galbavá 
                                        
                                            Pozor na číslo 7604
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Galbavá 
                                        
                                            Dochádzkový bonus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Galbavá 
                                        
                                            Slovenské a kórejské firmy na Slovensku.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Galbavá 
                                        
                                            Kiskov „útok“ na Fica.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ingrid Galbavá
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ingrid Galbavá
            
         
        ingridgalbava.blog.sme.sk (rss)
         
                        VIP
                             
     
         Vraj, čo ma nezabije, to ma posilní... 

 .... nie sú aj tie infarkty dáko limitované ? 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    182
                
                
                    Celková karma
                    
                                                5.37
                    
                
                
                    Priemerná čítanosť
                    1489
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vzťahy
                        
                     
                                     
                        
                            Rodinné peripetie
                        
                     
                                     
                        
                            Pohľady z druhej strany
                        
                     
                                     
                        
                            Život
                        
                     
                                     
                        
                            Priatelia
                        
                     
                                     
                        
                            Panské huncúctva..
                        
                     
                                     
                        
                            Veľký boj malej poštárky
                        
                     
                                     
                        
                            Moja maličkosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




