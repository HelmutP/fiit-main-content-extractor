

 Fashion Week Miláno, na mólo vchádza Abbey Lee Kershaw a s ňou aj príval leta, radosti a hlavne farieb.Mini šaty ako zo 70.rokov, samozrejme prispôsobené dnešnej dobe. Úzke strihy, rôzne potlače, ktoré kombiáciou farieb len vyzdvihujú pastel a to všetko milé dámy z elastického materiálu. 
 Prehliadka mala skutočne veľký úspech, bola dynamická a vo svojej podstate prezentovala mladú ženu v lete 2010. Ja osobne môžem túto prehliadku pozerať aj tisíc krát a stále mi zodvihne náladu a chuť obliecť si už konečne šaty, platformy, listovú kabelku a vyraziť do dvadsať stupňovej letnej noci. Veď posúďte sami. 
   
   

