

 Ja vim. Je to hlupy vtip. Ale co nam ještě jineho zbyva? Kaj už neni žadne pomoci, tam zustava jen černy (v dnešnim slengu "nekorektni" humor). Vodu nezastavime. Ať zrobime cokoliv, jak raz přitekla, někudy musi odtect. Je to naše vina, že zme si baraky postavili zrovna okolo Odry, Ostravice, Bečvy, Porubky.... No ja, ale komu by se tenkrat v pravěku chtělo vrtat studně v kopcach, když v řekach tekla taka fajna, kvalitni pitna voda s vyvaženym ph, jemně perliva? 
 Už vim od sedumadevadesateho, že jak v Beskydach a Jesenikach tři dny v kuse chčije, pak je zle. Vikend na Pustevnach a v Trojanovicach nestal za nic. Jak sem viděl ty kvanta vody, co se vali dule, věděl sem, že ty male knajpy okolo Odry a Opavice budu brzo putovat do Polska. A taky že ja. Letošni cestovani do Dělohova na loděnicu bude asi smutne, bo než se to spravi, bude zase podzim... 
 Ale na druhe straně bysem ten svět neviděl zas tak černě. Pamatujete? Před čtrnacti rokama Eurotel snižil ceny telefonu na tisicovku a ja sem v ruce držel užasny, neuvěřitelny, vymakany telefon Dancall, kery měl dvě funkce. SMS a volani. To byl fenomen, kurňa! Co fenomen, to byl zlaty kus plastu, bo jen diky němu zme při povodňach zustali propojeni ze světem. 
 Dneska? Tydlifon je samozřejmost. Kdo nema aspoň dvě simky, večko a dotykač, je luzr jak cyp. Uboha socka. Ale až odteče voda pryč, ukaže se novy fenomen. Fejsbuček. 
 Diky fejsbučku a diky ne-obyčejnemu synkovi z Noveho Jičina (Pozor! Neplest si s obyčejnym člověkem od Jury) su dneska desetitisice lidi z postiženych oblasti onlajn informovani o tom, co se robi kolem te hlupe vody. 
 Staři lidi nad třicet byli ještě v patek natvrdli a řikali, že fejsbuček je jen hračka pro děcka. 
 Tuž jasne, že to je hračka pro děcka. Tajak mobil, tajak televiza, tajak komp. Ale je to hračka, kera vam v připadě krize može zachranit život. Anebo přinejmenšim ulehčit, bo žadne noviny, žadne radyjo, žadna televiza nezrobila a nerobi lidem taku službu, jak to zrobil fejsbuček. 
 Jak to zrobily stranky "Voda aktualně - Zpravy" od novojičinskeho synka, kery se jmenuje Lukaš Dobeš. V nedělu večer zrobil stranky, pozval par kamošu, a fčil je na jeho strankach přes třicet tisic lidi, keři se vzajemně informuju o tym, co se pravě robi v jejich okoli! Enem ty fotky a videa, co tam su, to je drsne se na to aji divat. 
 Ale každopadně - diky synku, za všecky, komu tvuj napad pomohl. Až budeš Na Mexiku, zvu tě na jedno. 
 Kurňa, a maš už vubec osumnact? :-) 
 
 PS: Tuž a pro ty z vas, keři ste už staři, bo je vam třicet a vyš: 
 1. Kliknete na fotku teho brylateho cypa tu vpravo dule. 
 2. Vyplnite tych par řadku, na kere se vas fesbuček zepta 
 3. Nahoře do vyhledavača zadate nazev "Voda aktualně - Zpravy" A ste onlajn. 
   
   

