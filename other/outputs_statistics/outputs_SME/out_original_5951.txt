
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Sipták
                                        &gt;
                Zábava
                     
                 Bathory, alebo je slovenskému návštevníkovi jedno na čo ide do kina 

        
            
                                    6.5.2010
            o
            9:55
                        (upravené
                6.5.2010
                o
                13:56)
                        |
            Karma článku:
                3.30
            |
            Prečítané 
            531-krát
                    
         
     
         
             

                 
                    Po dlhej dobe som sa konečne odhodlal k pozretiu filmu, ktorý mal na Slovensku obrovský úspech. Samozrejme sa jedná o film Bathory. Film už má skoro 3 roky a prezretie tohto filmu som stále odkladal z viacerých dôvodov. Hlavným dôvodom boli negatívne recenzie. Ale prišiel čas aj na tento film. Aký bol zážitok?
                 

                 Zážitok bol nulový. V skratke je film úplne o ničom. Pôsobí neskutočne lacno a celú dobu som mal pocit, že sa dívam na televízny film. Chabí príbeh, ktorý vás nudí a je vám jasné ako to bude. Neskutočne dlhé a opakujúce sa dookola tie isté udalosti. A ta takto môžem pokračovať...   Nejdem tu ale rozoberať celý film, chcem sa len pozastaviť nad mediálnou masážou, ktorá zblbla ľudí. Noviny písali chválu, ľudia chodili masovo na tento film do kina, sám som to videl na vlastných očiach. Ale recenzie vo filmových časopisoch a na internete sa pohybovali okolo 40-50 percent (100 je maximum). Práve tieto recenzie ma presvedčili aby som kino nenavštívil. A myslím, že som urobil dobre.   Otázka znie: Je slovenskí divák naozaj taký filmový neandertálec, že mu stačí plagát a trailer, kde sa   spomenie, že film je slovenským(no až na Jakubiska a pár lokalít tam nič iné slovenské nie je) veľkofilmom a dobrý film a ľudia bezmyšlienkovite idú na film do kina? To je také ťažké si napríklad v Tescu prelistovať filmový mesačník, napríklad Cinema a kuknúť na hodnotenie? Dať si stránku Česko-slovenskej filmovej databáze a uvidieť, že hodnotenie  sa pohybuje okolo 50 percent? A podobne?  Asi je ľuďom na Slovensku jedno na čo idú do kina, pravdepodobne tu máme veľa peňazí. V Českej republike kde študujem, keď sa pýtam známeho či ide na ten alebo ten film, odpovie ,, Nie, že vraj to nie je dobre, aspoň podľa recenzií,, . Na Slovensku som sa známeho pýtal, predtým ako išiel na Bathory, prečo na to ide, že to údajne je hlúposť. Čo odpovedal? ,,Je to od Jakubiska a o Bathory to bude dobré,, . Nie je to určite jediný prípad, vyhľadajte štatistiky a uvidíte, že Bathory bol masový trhák, aspoň v našich Česko-slovenských končinách.   Návštevníci kina na Slovensku sa aj napriek potláčaniu svojho názoru spálili. Je to vidieť na filme Jánošík, ktorý bol oproti Bathory prepadák. Ľudia aj keď tvrdili, že film Bathory bol skvelý vnútorne vedeli, že to bola hlúposť, vyhodené peniaze a zabitých 150 minút. U Jánošika boli už omnoho opatrnejší. Je však škoda Jánošíka, ten obstál omnoho lepšie. O ňom však nabudúce.   A čo vy? Tiež si najprv o filme trošku niečo zistíte alebo dáte čisto len na reklamu? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Sipták
            
         
        martinsiptak.blog.sme.sk (rss)
         
                                     
     
        Som mladý človek, ktorý si rád užíva život a všetko čo k tomu patrí.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    531
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Skúsenosti
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




