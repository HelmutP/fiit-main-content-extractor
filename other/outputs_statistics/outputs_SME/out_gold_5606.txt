

 Nie som odborník, a preto tieto tvrdenia neviem ani potvrdiť, ani vyvrátiť,, ponúkam však svoju skúsenosť,, 
 je veľmi chutný,, skvelá náhrada osviežujúcich nápojov na leto,, posilňuje imunitu,, a určite čistí ľadviny a močový mechúr, lebo je mierne močopudný,, 
 ako si takýto vodný kefír pripraviť?,, tu je návod,, ja to robím vo väčšej, asi trojlitrovej fľaši,, takže potrebujem trojlitrovú fľašu, kefírové huby, cukor, sušené hrozienka (ja používam bio) a citrón,, 
 postup je veľmi jednoduchý,, do fľaše dáme kefírové hubky,, množstvo asi tak podľa obrázka,, 
  
 pridáme šesť vrchovatých polievkových lyžíc cukru, hrsť sušených hrozienok a jeden citrón,, pokiaľ nepoužívate citrón bio, tak je lepšie vrchnú žltú šupku oškrabať,, ja ju okrajujem nožom, aby tá biela časť na citróne ostala,, no a celé to zalejeme studenou vodou, premiešame a necháme cca 24 hodín stáť,, potom znovu premiešame a necháme ešte ďalších 24 hodín postáť,, potom pozbierame hrozienka, ktoré vyplávajú na povrch, vezmeme aj citrón, z ktorého môžeme vytlačiť do nápoja šťavu a nápoj precedíme,, hubky vypláchneme (tak, že keď je nápoj precedený, napustime do nich vodu, premiešame a znovu precedíme,, tú vodu samozrejme vylejeme,, to môžeme pár krát zopakovať,,),, keď sú hubky prepláchnuté, môžeme naložiť nový kefír,, 
 hubky sa dobre množia, tak ak ich je už viac, ako sme si na začiatku uviedli, tak ich jednoducho vyhoďte, dajte do záhrady ako prírodne hnojivo (ja to takto využívamJ), alebo podarujte niekomu ďalšiemu, aby mohol aj on vychutnávať tento skvelý nápoj,, 
 PS: ak ste z okolia Košíc, rád vám na začiatok tieto hubky podarujem,, stačí napísať správu do diskusie,, 
   
 Štefan Glova 

