

 Išlo o karosérie starých áut a samozrejme aj ich jednostopových príbuzných. Svedkom tejto nádhery som bol dnes ráno v Nitre, kde sa stretli očarujúce, dnes už historické vozidlá. Pod kapotami týchto deduškov a babičiek motorizmu určite stále drieme mladícke športové srdce, pretože okolo desiatej sa všetky vydali na okružnú jazdu do okolia Nitry, samozrejme  v zmysle olympijského hesla „nie je dôležité zvíťaziť, ale zúčastniť sa!".  Ešte pred tým si ich však obdivovatelia mohli poobzerať a tak som učinil aj ja. Ľutujem, že nemám pri sebe fotoaparát, a preto vyťahujem aspoň svoj mobilný telefón a urobím pár fotiek. 
  
 Jedno vozidlo ma zvlášť zaujalo a tak ho obdivujem o čosi dlhšie. 
  
 Počas môjho očarenia sa zastaví pri mne ďalší okoloidúci divák a položí mi takúto otázku: „Nechcete to auto predať?". V tom momente ma nič rozumnejšie nenapadne iba nasledovná rázna odpoveď : „Nie !". Odpovedal som sťa hrdý vlastník, pýšiac sa cudzím perím a s vnútorným presvedčením, že by som ho nepredal aj keby bolo skutočne moje. V tom momente ma napadol popevok jednej repeťáckej pesničky, „nemám auto, nemám motorku...". 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 No a potom sa už dali veteráni na štart, zavrčali motory, celý priestor sa zadymil, bolo citeľné, že títo motoroví starí rodičia už nespĺňajú emisné limity, ale pre ich zvláštne krásnu ladnosť tiel a kostier im to je odpustené. S veľkou vervou sa pustili do zápasu, keď už nič iné, aspoň dokázať svojim majiteľom, že čas a peniaze do nich investované boli naozaj dobrými investíciami. Je odštartované, no niektorí nezvládajú dlho očakávaný štart už na prvej svetelnej križovatke, a tak akoby vystavené stresu našej doby, zlyhávajú už v začiatkoch „pretekov", niektorým pomôže ešte roztlačenie, no niektorým už ani to. 
   

