
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Adriana Bagova
                                        &gt;
                Ľudia v nás - rozhovory
                     
                 Spomienky, ktoré by sme si mali pamätať-rozhovor s Yehudom Lahavom 

        
            
                                    29.7.2009
            o
            7:30
                        |
            Karma článku:
                8.57
            |
            Prečítané 
            2423-krát
                    
         
     
         
             

                 
                    Možno si spomínate, ako v novinárskom mori príspevkov v rádiách, už pred desaťročiami vystúpil do popredia vynikajúci komentátorský hlas z Izraela, ktorý perfektnou slovenčinou hodnotil tie najzávažnejšie udalosti Blízkeho východu. Izraelsko - arabský konflikt, vojnové udalosti ale aj mnoho miestnych zaujímavostí prichádzalo priamo z Izraela spolu s prenikavou dávkou jasnozrivosti, múdrosti a nadhľadu. Prinášal nám ich izraelský korešpondent Slovenského rozhlasu a dopisovateľ ďalších médií, pán Yehuda Lahav, ktorý mi láskavo odpovedal na moje otázky. Jeho brilantné odpovede tu práve prinášam.
                 

                 
Yehuda Lahav s manželkou Mariou a vedúcim zahraničného oddelenia Mestskej rady Rishon Lezion Jicchakom Orenom ako delegácia z partnerského mesta na prijatí u primátora PrešovaFoto: PhDr.Michal Kaliňák
   Možno neviete, že osud tohto muža sa podobá na takmer filmový príbeh, ak by to však, žiaľ, nebola pravda ale film. Yehuda Lahav je náš rodák, vlastným menom Štefan Weiszlovits a narodil sa 17.1.1930 v Košiciach. Krátke detstvo prežil v Prešove a spomienky na osud jeho rodiny i jeho samého počas holokaustu predstavujú to, čo by sme si mali uchovať v spomienkach i my čo najdlhšie. Ako jediný člen rodiny sa iba prezieravosťou rodičov vyhol holokaustu. Bola to najmä zásluha jeho otca, ktorý bol lekárom a videl prvé transporty žien do Osvienčimi. Preto ho, iba niekoľko týždňov pred deportáciou do koncentračného tábora, poslali rodičia s falošnými dokladmi k príbuzným do Budapešti, kde prežil s množstvom problémov vojnu.  Obaja rodičia mu zahynuli v koncentrákoch a po vojne ostal v pätnástich rokoch celkom sám. Spomienok na rodičov mu zostalo naozaj veľmi málo. Kým sa však po vojne vrátil do Prešova, ktorý sa takto pre neho navždy zmenil, sprevádzal ako mladučký tlmočník Červenú armádu pri jej ťažení z Budapešti na Berlín a koniec vojny slávil napríklad ako osloboditeľ Prahy. Tieto udalosti zaznamenáva vo svojej knihe Zjazvený život, ktorá je zhusteným, ale vypovedajúcim záznamom takmer ôsmych desaťročí jeho života.           Po návrate do Prešova sa angažoval v sionistickom mládežníckom hnutí a odišiel budovať Trať mládeže. V 1949 roku odišiel do Izraela, kde začal žiť v izraelskom kibuci Merchavja. Po odchode z kibucu sa stal redaktorom komunistických novín. Celý ďalší profesionálny život pracoval ako novinár v izraelských novinách. Stal sa spravodajcom z Blízkeho východu a pôsobil aj ako zahraničný redaktor v krajinách strednej a východnej Európy. Ako novinár komentoval aj našu nežnú revolúciu v novembri 1989. Hoci v súčasnosti je už na dôchodku, stále pôsobí ako spravodajca pre viaceré slovenské médiá. Tunajšia verejnosť ho mala možnosť vidieť aj na prezentácii jeho kníh, z ktorých tu často spomínam najmä knihu Zjazvený život. Ďalej napísal knihy Odsúdení spolu žiť a Odvrátená tvár konfliktu.                     História holokaustu a vôbec osud Židov na Slovensku ma už dlho zaujíma. Považujem ju za jednu z kľúčových v našej histórii. To ma priviedlo nielen k myšlienke napísať rozhovor s pamätníkom týchto udalostí na Slovensku, ale aj k obavám, ako sa ja, čo som neprežila holokaust môžem rozprávať s mužom, ktorého osud tak strašne skúšal. Ešte nikdy som asi tak dlho neuvažovala o tom, ako napísať článok na tak závažnú tému akou je holokaust. Popísalo sa o ňom veľmi veľa, ale povedalo stále veľmi málo. Mám pocit, že jediný správny záver bol ten, ktorý som aj zrealizovala, že skrátka skúsim napísať otázky a záleží predsa najmä na odpovediach. Až čas ukáže, či som sa smela pýtať...       AB: Priznám sa, že najviac zo všetkého ma zaujíma ako dokážu Židia nám ostatným vôbec odpustiť to, čo sa stalo za 2.svetovej vojny? Nie je sa ľahké na to pýtať a ešte ťažšie asi odpovedať, ale Vy sám ste prišli v holokauste nielen o celú najbližšiu rodinu, ale zabili vám vlastne celú minulosť s detstvom. Zomrelo takmer celé Vaše rodné mesto, celý známy svet, ktorý patril predsa aj vám židovským rovnako ako slovenským deťom.To je  pre mňa na holokauste to úplne najstrašnejšie, že to nie je len osamotený hoci strašný zločin ale takmer "dokonané" vyhladenie so súhlasom okolia. Že svet ako ste ho poznali zrazu úplne prestal existovať. Keď som bola mladé dievča, bola som presvedčená, že takú krivdu sa odpustiť proste nedá a nemá, aj keď ma viera učila inak.Čo vás učí o odpúšťaní vaša osobná viera a ako sa vám to vôbec darí?   YL: Nemôžem a ani nechcem odpustiť ľuďom, ktorí mi vzali rodičov a spôsobili moju osobnú tragédiu a tragédiu tak mnohých nevinných ľudí. Neverím však v „dedičný hriech", nemyslím si, že potomkovia  vrahov nesú zodpovednosť za činy ich otcov či dedov a preto si nemyslím, že im mám čo odpúšťať. Mám k nim rovnako normálny vzťah, ako ku každému inému. Samozrejme, výnimku tvoria tí súčasníci, ktorí popierajú zločiny fašistického obdobia a dokonca ich ospravedlňujú. Takíto ľudia nielen že si nezaslúžia odpustenie - zdá sa mi, že v istom zmysle sú dokonca horší ako ich predchodcovia. Tamtí ešte nemali historickú skúsenosť a snáď ešte nevedeli presne, aké dôsledky budú mať ich činy - zatiaľ čo ak niekto dnes obhajuje a propaguje nacistické myšlienky a šíri národnostnú a rasovú nenávisť, musí vedieť, kam to povedie. Preto som voči nim absolútne neznášanlivý. A bez mučenia priznávam: každý nacionalizmus je mi odporný, ale zo všetkých najodpornejší je mi môj „domáci", čiže izraelský alebo židovský nacionalizmus.   AB: Čo podľa Vás môžeme my obyčajní ľudia spraviť preto, aby sa nemohla história nenávisti k Židom či iným menšinám opakovať? Viete, v Rusku kde som 5 rokov študovala som mala priateľku, ktorá bola Židovka. Zoznámili sme sa v nemocnici a Aňa pre mňa predstavovala úplne iný pohľad na svet a aj inú kuchyňu (jej babka nám vyvárala gefillte fisch a aj ďalšie židovské jedlá) a tak osud rozhodol, že som začala Židov obdivovať za ich neochvejnosť v presile majoritnej spoločnosti a neskôr som citlivo vnímala sústavné hádzanie viny za čokoľvek v bežnom ruskom zmýšľaní práve na Židov. Niečo podobné ako sa u nás teraz hádžu problémy na Maďarov ale oveľa silnejšie. Ako riešiť predsudky, keď je zrejmé, že nikto nie je vinný za všetko a najľahšie sa v živote vyhovára na iných?   YL: Antisemitizmus je iba krajný prípad národnostnej neznášanlivosti. Stal sa ním práve preto, lebo skončil v Osvienčimi. Ale v podstate je rovnaký, ako každý iný národnostný či rasový predsudok a nenávisť, či už je namierený proti Židom, Maďarom, Slovákom, Romom alebo Arabom. Predsudky a nenávisť je treba prekonávať, a to trpezlivo a neprestajne, lebo sú väčšinou veľmi hlboko zakorenené a ich objekty - teda Židia, Arabi, Maďari alebo Romovia - slúžia vždy ako údajní „vinníci" všetkých ťažkostí. Osobne sa už desiatky rokov snažím v mojej krajine vytvárať ovzdušie porozumenia medzi Židmi a Arabmi a pokračujem vo svojom úsilí, napriek tomu že viem, že vyvolávať nenávisť a nedôveru je omnoho ľahšie, než napomáhať spolupráci medzi národmi. Pre vzbudenie nenávisti medzi ľuďmi predsa stačí jeden atentát, ale čo všetko je potrebné pre ich zblíženie!    AB: Hovorí sa toho veľmi veľa o Mosade, CIA a ich negatívnej úlohe. Ja osobne som však Mosad a Goldu Meierovú ako silnú ženu na čele Izraela obdivovala za dolapenie vojnových zločincov a nebola som sama. Aj vy spomínate vo vašej knihe Zjazvený život párkrát Mosad, pripadá mi tam váš pohľad vtipný a kritický. Myslíte si, že by bez tajných služieb mohol Izrael v prostredí sústavného zápasu o existenciu prežiť? Ozaj a je naozaj taký dobrý ako sa vraví? :)   YL: Každý štát asi potrebuje tajnú službu, Izrael netvorí výnimku. Vždy však hrozí nebezpečenstvo, že tajná služba sa „osamostatní", nadobudne príliš mnoho vplyvu a vyhne sa zákonnej kontrole svojej činnosti. Izraelský Mosad v tom ohľade netvorí výnimku - hoci je treba povedať, že vo svojej činnosti zaznamenal aj veľmi významné úspechy. O niektorých - ako dopadnutí Eichmanna - sa môže hovoriť, o iných nie. V posledných rokoch sa kontrola Mosadu upevnila, čo je pre demokratické zriadenie dobré.   AB: Najfascinujúcejšou časťou vašej knihy Zjazvený život je záver, keď píšete o tom, že si myslíte, že tí správni ľudia majú vždy v popredí láskavý humor. ( Inak ja milujem vtipy aj o Židoch najmä tie s prenikavou dávkou múdrosti). Píšete aj o tom, že cítite podporu svojich mŕtvych rodičov pri všetkých závažných životných otázkach, hoci osud vám nedoprial ani tých spomienok na nich viac ako priehrštie. Ako ste dokázali prežiť tak ťažký osud? Ako sa viete opäť smiať? Čo vás previedlo cez úskalia smútku a beznádeje? Myslím, že by ste nám všetkým mohli poslúžiť práve tým receptom ako prežiť úspešný život napriek tomu, že ste museli prekonať tie najťažšie osudné skúšky. Platí teda to, že tí najlepší sú najviac skúšaní lebo to zvládnu?   YL: Neviem vám povedať, ako sa mi podarilo zachovať zmysel pre humor, ale aj naďalej mám pocit, že bez humoru by sa mi žilo omnoho ťažšie. Moja skúsenosť je, že humor majú väčšinou ľudia so širším obzorom, otvorení  pre iné než ich vlastné  názory, s  liberálnym prístupom k ľuďom a veciam. Humor chýba tým, ktorí fanaticky veria v akúsi jedinú a výlučnú pravdu, či už náboženskú alebo národnú. V našich (izraelských) krajne pravicových nacionalistických publikáciách často čítam zúrivé nadávky na protivníkov a ich posmešné pomenovania - ale nespomínam si na žiaden prípad takého Švejkovského alebo ak chcete Šalom Alechemovskeho humoru, očisteného od nenávistného nánosu. Snáď nie je náhoda, že medzi Židmi je tak rozšírený humor namierený do vlastných radov. Aby sa človek smial sám sebe, musí mať pocit vlastnej sily a  pravdy. Neexistencia humoru v nacionalistickom a nábožensko-fanatickom tábore vyviera podľa môjho názoru z pocitu slabosti.   Pokiaľ ide o duchovný vzťah k mojim rodičom, viem, že to je sebaklam: ja sa ich na nič nemôžem opýtať a oni zo vzdialenosti 65 rokov by nemohli moje otázky pochopiť a na nič by nemohli odpovedať. Ale mám dobrý pocit, keď si namýšľam, že sa s nimi radím. Tak mi, prosím, ten dobrý pocit ponechajte. Veď nič iného mi po rodičoch nezostalo.       AB: Tiež sa vás chcem spýtať, pán Lahav, ako sa pozeráte na terajšiu svetovú ekonomickú krízu. Zažili ste, ako ja, komunizmus ale aj dačo čo som ja vždy len obdivovala, idealizmus v kibucoch spolu s tvorením Izraela ako ostrova pre zachránených Židov z celého sveta. Spoznali ste fašizmus v jeho najhoršej podobe. Videli ste vývoj Slovenska a východnej Európy. Viete toho ako novinár veľmi veľa, zaujíma ma preto ako to vidíte Vy s realitou sveta v čase, keď sa prehlbuje kríza? Sú potrebné radikálne zmeny? Pomohli by svetu ostrovy vzájomnosti, nezištnosť a delenia s ostatnými bez hlúpych reklám a ziskuchtivosti, trebárs ako ste opisovali Váš život u spomienok z kibucov?   YL: Nie som ekonóm a netvrdím, že som predvídal terajšiu krízu. Ale všeobecne som bol presvedčený, že stály bezkrízový vývoj nie je možný. Podľa môjho názoru je poučné, že v zemi absolútnej slobody podnikania sa pokúšajú riešiť krízu prostriedkami pripomínujúcimi socialistické metódy - a na druhej strane, v krajinách plánovaného hospodárstva (a aj v izraelských poľnohospodárskych komúnach - kibucoch) zavádzajú konkurenciu a slobodné podnikanie. Zdá sa mi, že svetové hospodárske problémy sa nemôžu riešiť podľa akéhosi vopred stanoveného jednotného receptu.   V kibucoch prebiehajú zásadné zmeny a bývalé stopercentné komúny postupne zanikajú a premieňajú sa. Veľmi mnohí to ľutujú - ale zdá sa, že obklopené rozvinutým kapitalistickým hospodárstvom kibuce jednoducho neboli schopné udržať svoju pôvodnú spoločenskú štruktúru komúny.       AB: Zaujíma ma aj Váš názor na osud Izraela, ktorý stojí osamotený medzi krajinami často s protiľahlými zámermi, neviem, či niekto môže viac rozumieť tomu, či je myšlienka zjednotenej Európy lepšia ako samostatnosť a nezávislosť, po ktorej niektorí ľudia stále viac volajú, ako obyvateľ Izraela s nadhľadom aký je Vám vlastný. Čo si Vy myslíte o myšlienke ale aj úlohe EÚ vo svete, bude rásť na miesto vplyvu USA?   YL: Viete, nie som veľmi rád, keď niekto z Európy, zo vzdialenosti 3000 km, dáva „zaručene dobré rady" Izraelu, akú politiku má viesť. Nechcem popadnúť do rovnakej chyby s opačným znamienkom a radiť, ako usporiadať európske záležitosti. Zdá sa, že globalizácia neponecháva mnoho iných možností, než zjednotenie vo väčších celkoch. Zdá sa mi tiež, že spoločné členstvo - napríklad - Maďarska a Slovenska v Európskej Únii je istá záruka, že aj keď medzi nimi vznikne konflikt, bude mať iný priebeh ako v roku 1938/39. Ale nie som si istý, či sa nemýlim.       AB: Vo vašej knihe bol zaujímavý aj pohľad na konflikt s Arabmi a podporu komunistického Ruska Arabom proti Izraelu. Inak otázka komunizmu, politika ZSSR nás zaujímala oboch. Rusi Izraelu tiež teda práve nepomáhali. Prečo podľa Vás tento zničujúci izraelsko - arabský konflikt stále existuje? Je pre mňa nepochopiteľný a desivý. Vo svete sa však veľa hovorí o nadštandartnej americkej podpore Izraela. Čo si myslíte, má Izrael, podľa mňa ostrov v nepriateľskom mori, reálnu šancu ustáť to napätie dookola seba? Vyžiada si to, podľa vás úplnú zmenu prístupu alebo sa  s arabsko - izraelským konfliktom dlhodobo nezlepší?   YL: Tak teda, v záujme historickej pravdy: predtým než Sovietsky zväz začal podporovať Arabov, v roku 1948 (spolu s Československom) veľmi účinne diplomaticky i vojensky podporoval Izrael. To podľa zahranično-politického záujmu, ako ho v Moskve interpretovali.    Izraelsko-palestínsky konflikt je ťažko riešiteľný, pretože - to je paradox - obe strany majú pravdu, alebo ako povedal izraelský spisovateľ Amos Oz „právo stojí proti právu". Oba národy majú oprávnený nárok na celé sporné územie. Je síce možné dosiahnuť kompromis (a ten sa už rysuje), ale vždy sa na oboch stranách nájde nejaká menšia či väčšia skupina, ktorá odmietne vzdať sa niečoho, čo považuje (právom) za oprávnený nárok. Vždy hovorím: Som optimista na dlhšiu vzdialenosť. Dôraz je na oboch častiach tejto vety.   AB: Na záver otázku, po všetkom čím ste prešli, po tých rokoch v novinárskom prostredí, čo robí novinára výnimočným? A môže, podľa Vás, prežiť pri novinárčine aj čestný idealista? Má človek písať aj keď je úplne zrejmé, že jeho názory nie sú podľa chuti väčšiny?   YL: Novinára nerobí výnimočným nič iného, než čo robí výnimočným ktoréhokoľvek človeka. Je dobré, ak novinár je idealista, ale predovšetkým má byť čestný človek, ktorý čitateľom predkladá fakty, nič neskrýva a vyvaruje sa každej jednostrannosti. Ak píše o tak zložitom konflikte, ako je palestínsko-izraelský, musí byť pripravený na to, že obe strany od neho očakávajú, že sa postaví na ich stranu a že jeho prístup voči nim bude jednostranne pozitívny - a keď ich sklame a uvedie objektívne fakty bez ohľadu na to, komu slúžia, dočká sa kritiky a obvinení zo „zradcovstva" aj z tej, aj z druhej strany. Útechou mu môže slúžiť, že práve to je dôkaz, že svoju úlohu splnil čestne.   Rozhovor bol uskutočnený 25.7.2009 a bol autorizovaný.   Foto: PhDr.Michal Kaliňák     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Bagova 
                                        
                                            Rusáci sú opäť tu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Bagova 
                                        
                                            Bizarné zvyky na Slovensku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Bagova 
                                        
                                            Na zdarovje druzja!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Bagova 
                                        
                                            Ako sme si užili na ulici Bratislavy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adriana Bagova 
                                        
                                            Nič nie je ako bývalo - zmena vo výrobe olivového oleja
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Adriana Bagova
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Adriana Bagova
            
         
        bagova.blog.sme.sk (rss)
         
                                     
     
        O mne by mal vypovedať text, tak len dúfam, že aj dačo podnetné.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    36
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2656
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Čo mi nedá spávať
                        
                     
                                     
                        
                            Čo sa deje u nás vonku
                        
                     
                                     
                        
                            Ľudia v nás - rozhovory
                        
                     
                                     
                        
                            Môj takmer súkromný život
                        
                     
                                     
                        
                            Spomienky na budúcnosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Rozhovor o knihách s Dadom Nagyom
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Erica Jong - Padáky a polibky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Bubamara - Goran Bregovic
                                     
                                                                             
                                            Palya Bea - Tchiki tchiki (Transylvania)
                                     
                                                                             
                                            Tony Gatlif - Promesse
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Skrátka Humno a Rado Ondřejíček
                                     
                                                                             
                                            Blog zaujímavej ženy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje obľúbené SMEčko
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




