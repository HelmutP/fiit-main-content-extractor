

 Musím súhlasiť, že život spred roka 1989 nebol veľmi ľahký. Moji blízki členovia rodiny by vedeli rozprávať celé dni. Otec o tom ako schovávali zvieratá na svojom statku, aby sa im aspoň niečo ušlo pod zub... 
 V tejto súvislosti mi nedá spomenúť jednu humornú situáciu, kedy bol môj dedko vo väzení za to, že neodviedol všetky povinné alimenty (v podobe zvieracích produktov - vajcia, mlieko). Po pár týždňoch ho prepustili, pretože súdruhovia si uvedomili, že o hospodárstvo sa nemá kto starať a sú ešte viac stratoví.... 
 Výplody premiéra ma nútili pouvažovať na tými kladnými stránkami komunizmu: Je pravdou, že naši predkovia sa starali o výstavbu budov, hrádzí, korýt potokov, mostov, chodníkov... ... na to musia byť patrične hrdí, aj keď to do značnej miery nebolo až také dobrovoľné,. ako to na prvý pohľad a najmä v československých propagandistických seriáloch vyzeralo. 
 Naozaj som ale na nič viac pozitívnejšie neprišiel. Alebo mal na mysli premiér tieto hodnoty? 
   
 
 
udavačstvo, neúcta človeka k človeku (koľkí ľudia sedeli len za to, že niečo niekde spomenuli, koľkí boli posunutí na úplný okraj spoločnosti so značkou ROZVRACAČ REPUBLIKY?) 
 neúcta k slobode názoru, prejavu  
 
popieranie a pohŕdanie ľudí s duchovným životom (najmä ľuďmi s náboženským pohľadom na svet, ktorí sa nemohli dostať na vysoké štátne a pracovné pozície....ich život sa akosi vymykal materiálne videnému svetu) 
 
závisť (komunizmus chcel ľudí vyrovnať a zblížiť, ale namiesto toho ich čoraz viac odcudzil a rozdelil) 
 
majetníckosť (málokto z lepšie postavených  v tej dobe nemal tendenciu rabovať závody a stavať si chalupy na úkor iných) 
 
falošný pocit komunity (veľkolepé oslavy prvého mája a podobné akcie, ktoré mali umelo vytvoriť ilúziu veľkej šťastnej komunity...pritom väčšina Ľudí bola vo vnútri nešťastných) 
 
latentný rasizmus (tým aké výhody mali najmä rómske menšiny. Komunizmus ich chcel umlčať, lebo si nevedel rady....vychoval tým v nás skrytý rasizmus... koľko Slovákov je rasistami??- nechcem to ani vedieť) 
 atď. 
 
 To sú hodnoty, ktoré dodnes zanechali stopu v ľuďoch. Ľudia si závidia, porovnávajú. Ak máte nové auto ste zlodej (pritom ani nemusí byť vaše), ak pozdravíte a prihovoríte sa, tak ste hneď podozrivý. Pretože v ľuďoch ostal ten strach z udavačstva, strach z cudzích... 
   
 Samozrejme sa Ľudia nemôžu hanbiť za to, že tvrdo pracovali. Nemôžu za to, že žili pred rokom 1989. Nemôžu sa hanbiť, ak majú v sebe pocit, že robili všetko tak dobre ako len vedeli. 
   
 Hanbiť by sa mali ale ľudia, ktorí na práci obyčajných budovali svoje kariéry, vstupom do komunistickej strany hľadeli len na svoje výhody a nejaké budovanie rovnocennej spoločnosti mali u riti..... však pán premiér... 
   
 A najhoršou vecou, ktorú v ľuďoch komunizmus vybudoval je to, že v nich potlačil schopnosť starať sa sám o seba, veriť si...naučil ich slepo veriť spasiteľom ich života, ktorými sú nenažraní politici. Preto sa ľudia ako Mečiar a Fico tak ľahko dostanú k tak veľkej moci a takmer úplne bez zásluh. 
   
 Tieto negatíva sú kde pán premiér? Viem nemôžte ich odkryť. Sr*li by ste si do huby. 
   
 ... ale aj to by už bolo chutnejšie ako ŽIVENIE FALOŠNEJ NOSTALGIE V ĽUĎOCH, ktorí vás vyniesli na piedestál popularity... 
   
   
   
   
   
   

