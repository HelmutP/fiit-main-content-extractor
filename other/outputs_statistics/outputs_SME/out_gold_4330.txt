

 Tma, ktorá pretne svetlo 
 a všetko predýcha. 
   
 Chvejem sa 
 ako nedopísaný papier 
 z tvojho plameňa. 
 Srdce mám na mraky. 
   
 Sú noci, 
 keď rozpíšem vlastnú krv 
 a samota sa ostrí na hrane. 
   
 Obzriem sa za seba. 
 Som iba pahreba. 
   
 Z cigarety odklepnem 
 posledný verš. 
   

