

 Keby toto videl Remarque... 
 Nepísané pravidlá, nevyslovené... 
 Pravdy o tom, ako to chodí na svete. 
 Vraj keď poviete niečo pekné o žene, 
 smeje sa v duchu, ach vie, že  jej klamete.  
   
 Chcete byť úprimný a predísť klamstvám,  
 to v dnešnom dnešku je prostotou naivných! 
 Pravda je tabu, a lož sa fláka svetom. 
 Neviem, čo poviem raz svojím vlastným deťom. 
   
 Povojnové povahy dávno sú už preč,  
 A tí, či dožili, sú vraj starí, hlúpi... 
 Remarque keby toto videl, stratil by reč, 
 toľko je na svete teraz ľudí skúpych.  

