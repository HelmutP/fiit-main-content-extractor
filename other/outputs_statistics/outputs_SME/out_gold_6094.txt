

 Povedal si, trénovať, a tak chodíme a skúšame. Možnosti, smery a snáď aj samých seba. Či to spolu ešte stále zvládame. Potom sa len tak prechádzame po okolí, akoby jeho lepšie poznanie nám pomohlo lepšie spoznať aj cudzie miesta. Sme doma, ale mapy sa otvárajú čoraz častejšie, cudzie názvy sa skloňujú čoraz nástojčivejšie. Sledujeme správy, fantazírujeme, cestujeme na krátke vzdialenosti a spievame nahlas (aj keď nám spev nejde, zabúdame slová, alebo si ich vymýšľame, no kašľať na to, hovoríš). 
   
 Začíname nanovo, nové začiatky voňajú nervozitou a knihami, odrazu je na všetko menej času, aj na tréning, aj na listovanie vo farebných bedekroch, aj na nás. Tak to je, a inak to ani nechcem. 
   
 Všetko je rozmazané, ako lomo videnie, ako staré fotografie, ako mesto, v ktorom žijeme. 
  
   
 Ešte toto a toto, a takto. 
   

