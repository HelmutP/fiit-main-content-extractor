
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Júlia Hubeňáková - Bianka
                                        &gt;
                Fejtóny
                     
                 Zmanipulované voľby, fuj! 

        
            
                                    12.6.2010
            o
            15:04
                        (upravené
                13.6.2010
                o
                18:47)
                        |
            Karma článku:
                9.84
            |
            Prečítané 
            2319-krát
                    
         
     
         
             

                 
                    Fakt sa to stalo. Skeptici neuveria a tí, čo sú pri mojich článkoch zo zásady proti, mi zas raz virtuálne poklopkajú po čele. Ale čestné slovo, že  sa to stalo a voľby v mojej rodnej obci, Novej Ľubovni, boli zmanipulované.
                 

                 Od rána vyhrávala v dedinskom rozhlase dychovka. V pochodovom tempe sme ráno zjedli praženicu z domácich vajec, v pochodovom tempe sme plákali bielizeň na potoku i zametali chodník na priedomí. Až potom sme sa celá rodina vyštafírovali a hromadne sme šli voliť. Chýbal otec. Bol totiž členom volebnej komisie, tak sme šli bez neho. Raz dva, ľavá, šinuli sme si to dole dedinou, až do budovy miestneho národného výboru. Nepomýlila som sa, nebojte. Mám na hlave šiltovku a aj keď pripeká, dobre viem, čo chcem povedať.   V hlavnom vchode MNV hrá naša dedinská ľudová kapela - Pasternoci. Fidli fidli, ťahajú sláky na goralských husličkách. Široký úsmev  a strieborný zub ujka Valigurského, primáša, si pamätám dodnes. Vchádzame do sály. Červené karafiáty, červené koberce...a možno mám len krvou podliate oči. Prichádzame ku komisii, vyzdvihnúť si lístky. Komisia jednohlasne klopí zrak. Asi nás nevidno. Ani nášho otca nevidno, ten je až pod stolom. Nevedno, ktorý člen komisie vyriekne ortieľ: „Vy už nemôžte voliť, váš otec už dal do urny lístky za vás. Máte odvolené."  Ako si zachovať dekórum?! Pod stôl sa nás už viac nevojde. Sťaby na povel, spravíme otočku na opätku a dôstojne míňame strieborný zub ujka Valigurského.   V televíznych novinách sa dozvedáme, že naša dedina už o 13,30 volila na 100 percent. Keby tak vedeli...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (39)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            O čistote srdca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Pán Matovič, hlbšie sa už klesnúť nedalo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Pán predseda, blahoželám
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Letný čas:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Garancia vrátenia peňazí. To určite!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Júlia Hubeňáková - Bianka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Júlia Hubeňáková - Bianka
            
         
        hubenakova.blog.sme.sk (rss)
         
                                     
     
        Radkova manželka a mama Miša, Veroniky a Raďa.










        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    352
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2465
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodinné striebro
                        
                     
                                     
                        
                            Bakalári
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Pokrútené paragrafy
                        
                     
                                     
                        
                            Moje bá-sničky
                        
                     
                                     
                        
                            Politika a ja
                        
                     
                                     
                        
                            Report
                        
                     
                                     
                        
                            Moje povedačky
                        
                     
                                     
                        
                            Evanjelium podľa...
                        
                     
                                     
                        
                            Fotofejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Rozhovor v časopise Zrno
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Predstavujem vám moje knihy
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Viem, viete, vedia :)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Verbum Domini
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




