

 Kde je teda pravda?Asi sa zhodneme,že niekde uprostred.Z môjho skromného pohľadu trochu viac na strane viniacej nárast obyvateľov.Globálne otepľovanie určite nie je výmyslom vedcov a zelených.Do akej miery je  však spôsobené činnosťou človeka alebo prirodzeným vývinom Zeme?A v akej miere vplýva na rôzne prírodné katastrofy?Odpoveď je nejasná a vedú sa o nej rozsiahle diskusie. 
 Na podporu môjho názoru,že väčšia "vina" je na strane zväčšujúceho sa životného priestoru ľudstva na všetkých kontinentoch použijem záplavy,ktoré zasiahli írsky ostrov.Nevyhli sa ani mestu Roscommon v strede ostrova v ktorom žijem a ktoré je centrom rovnomenného okresu.Napriek skutočnosti,že v rozlohe zaplavených území patrí okres Roscommon k najpostihnutejším , v "tabuľke" predpokladaných škôd je niekde na spodu.Spôsobené je to hlavne jeho nízkou hsutotou osídlenia.Zaplavené sú väčšinou iba osamotené farmy.V samotnom meste je môj názor ešte lepšie čitateľný. 
 Tu treba uviesť,že osídlenie sa v moderných dejinách sa začalo vyvíjať okolo 8.st.n.l a pri dostavaní hradu v koncoch 13.st. sa už píše o meste.To , čo sa infraštruktúry týka,sa tak vyvíjalo asi 10 storočí.V dnešnej podobe je Roscommon typickým írskym mestom .Z architektúry sa dá krásne vyčítať ako sa z cca 1500 obyvateľov na konci 19.st. rozrástlo na dnešných 9.tis.Hlavná ulica mierne stúpa a je zakončená malým námestím.Z nej sa zhruba v jej polovici odpája ulica vedúca ku kostolu, Church street.Obe sú v miernom stúpaní.Na ne sa potom napájajú ďaľšie ulice,ktoré boli dobudované v 17. a 18.st.Celé širšie centrum je zakončené knižnicou a budovou súdu dostavaných na konci 19.st. 
 Všetky ostané budovy z tejto doby sú od hlavnej ulice vzdialené  viac ako 500 metrov.Rovnako i najstaršie obytné štvrte z 20.-40.tych rokov minulého storočia sú vzdialené takmer kilometer a tiahnu sa doslova kilometre ako jedna dlhočizná ulica na rôzne svetové strany.Medzi nimi zostávajú (skôr zostávali) polia  pre dobytok.Prečo sa ľudia v minulosti rozhodli bývať i niekoľko kilometrov od centra keď hlavná ulica na zvažujúcej sa strane ústila  na nevyužitú lúku?Nie nebolo to iba tým,že každý mal neaký dobytok ,ktorý sa pásol za domom.Veľmi dobre vedeli kde sa po veľkých dažďoch tvoria záplavové jazerá.A preto ešte 15-20 rokov späť sa hned vedľa hlavnej ulice pásli na zelenej lúke ovce. 
 Tie zelené lúky začali byť v ére keltského tigra veľmi zaujímavé pre rôznych developerov a podnikateľov.Boli prakticky v centre a bez zastaralej infraštruktúry,ktorú treba pracne a draho upravovať.Dokonalý sen ak máte peniaze na podnikanie.Zelená lúka v srdci rozvíjajúceho sa mesta v krajine s obrovským hospodárskym rastom.Pred 5 rokmi keď som sem dorazil zostalo zopár posledných parciel.Dnes je Roscommon pekne vyplnený a prvé ovce sú minimálne kilometer od hlavnej ulice na každý smer.Všetky zelené lúky vyplnili rôzne biznisy.V dnešné dni je väčšina z nich zatvorená.Spoločný dôvod.Sú zapalvené. 
 Prešiel som si teraz cez víkend celé mesto.Okrem týchto biznisov ,ktoré boli vybudované v posledných 2Otich rokoch je zatopených pár záhrad.I tie sú v štvrtiach,ktoré vznikli v stavebnom boome v posledných rokoch.Všetko na zelených lúkach,ktoré však  storočia zostávali zelené z neakej príčiny.A tou vo väčšine prípadov bolo sporadické zatápanie.Írsko nemá údolia do ktorých steká voda z okolitých kopcov a keď jej je v rieke veľa tak tá sa vyleje.Tu voda steká do najnižšie položených miest a keď sa netíha vsiaknuť vytvára jazierka a jazerá.Viem je to trochu zložitejšie ale ľudia pozorovaním zistili kde sa tieto záplavové oblasti nachádzajú a vyhýbali sa im.Až do nedávna. 
  
 Tu vyúsťuje hlavná ulica.Na ľavej strane za múrom sa stavia štátne zdravotné centrum.Momentálne však všetky stroje stoja v 40cm vody.Budovy v pozadí sú všetky vytopené.Pred 7 rokmi tam nebolo nič. 
  
 Tieto predajne sú na pravej strane cesty z prvej fotografie.Tiež záležitosť posledných 10 rokov.LYNCH flooring vyšiel tento týždeň v lokálnych novinách celostranový inzerát na akciu na plávajúce podlahy... 
  
 Na pravej strane od kolov je maličký potôčik.Za normálneho počasia.V pozadí je gaelic štadión,ktorý bol vystavaný v 70tych rokoch. 
  
 Pôvodne ho vystavali na tomto mieste,ktoré však v zime (keď sa gaelic súťaže nehrajú) pravidelne zatápalo.Táto fotka je z minulého roku. 
  
 Táto je zo včera. 
  
 Vyššie spomenuté zdravotné centrum chceli pôvodne vybudovať na tejto lúke za mestom.Proste z kaluže do ešte väčšej. 
  
 Severná priemyselná zóna.Nad ňou sa tiahne ulica nazvaná The Walk asi 3km ale nikde nezostúpi do údolíčka.Pred 15 rokmi tam okrem polí nebolo nič. 
  
 "Výhodná" poloha pre pumpu na výpadovke. 
 Takto by som mohol pokračovať s možno 10 - 15 ďaľšími biznismi v našom meste.Rodinný dom nie je komplet zaplavený ani jede.Nedá mi ešte nespomenúť ako perfektne zvládajú Íri záplavy.Žiadna panika alebo chaos.jasne vidieť,že majú s takýmito situáciami bohaté skúsenosti.Každý okres na oficiálnej stránke uvádza,ktoré cesty sú zatopené a neprejazdné.Obchádzky sú najskôr vyznačené až potom sa cesty uzavrú...ale vždy v dostatočnom predstihu aby nevzniklo nebezpečenstvo.V piatok sprístupnili na 2 miestach nedokončené úseky diaľnice medzi Dublinom a Galway na západe.Rýchlo operatívne bez zbytočných diskusií.Vláda na mimoriadnom stretnutí konštatovala,že nie je ani moc do čoho zasahovať,pretože mestá a okresy situáciu zvládajú najlepšie ako sa dá.Na zásahy vrtuľníkmi prišlo doteraz iba minimálne.Všade kde to bolo nevyhnutné evakuácia prebehla vo veľkom predstihu.Médiá informujú o 20 až 40 ročnej vode.Najpostihnutejšie okresy sú Cork a Galway.Pár fotiek ,ktoré vám viac priblížia rozsah zápalv si pozrite tu. 
 Ja som pri mojej ceste do Athlonu (najväčšie mesto vo vnútrozemí, 30tis.) spravil pár fotiek najväčšej írskej rieky Shannon,ktorá rozdeľuje mesto na 2 časti. 
  
 Záber z auta.Celé okolie rieky vytvára jedno veľké jazero. 
  
  
  
   

