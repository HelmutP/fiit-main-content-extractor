
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kotrčka
                                        &gt;
                OpenSource
                     
                 "Výsledky" vládnutia SMER-u 

        
            
                                    1.5.2010
            o
            12:20
                        |
            Karma článku:
                11.47
            |
            Prečítané 
            409-krát
                    
         
     
         
             

                 
                    SMER-SD prezentuje aj pomocou reklamy na sociálnych sieťach body, ktoré považujú za úspechy "vlády pod vedením strany SMER-SD". Nuž, tu je zoznam s mojim stručným komentárom.
                 

                 20 NAJDÔLEŽITEJŠÍCH VÝSLEDKOV 
       Slovenská ekonomika bude podľa Európskej komisie v  roku 2010 rásť najrýchlejšie z krajín EÚ. - to je ešte otázne. Ale aj keby, prezentovať možný (opakujem, MOŽNÝ) budúci rast ako PRVÝ najdôležitejší výsledok tejto vlády - hmmm.        Posilňujeme sociálnu ochranu, ľudia neplatia účet za  krízu:   - vianočné príspevky pre dôchodcov, lacnejšie lieky, vyššie  dôchodky,   - 830 eur pri narodení prvých troch detí, zvýšenie rodičovského  príspevku, výhodné hypotéky, mladomanželské pôžičky. - možno by bolo dobré povedať, že toto všetko sú jednak omrvinky oproti neefektivite nakladania so zverenými prostriedkami a jednak na úkor zadĺženia štátu. Veď prečo by sme sa mali obmedzovať my, keď sa môžu obmedzovať naše deti, nie?        Posilnili sme ochranu zamestnancov na pracovisku. - dajme tomu, aj keď skôr sa posilnila ochrana odborárov (ktorí vo voľbách 2006 podporovali hádajte koho).        Neprivatizujeme, znižujeme dane najmä pre ľudí s  nižšími príjmami, takisto aj DPH. - aj vďaka neprivatizácii dávame do stratových štátnych podnikov násobky toho, čo je uvedené v bode 2. Okrem toho, nechcel niekto tak náhodou znížiť DPH na potraviny? Toľko k tomuto bodu.        Znižujeme počet ministerstiev aj úradníkov. - asi žijem v inom štáte, zatiaľ som nepostrehol, že by TÁTO vláda vládla pomocou nižšieho počtu ministerstiev. Počet úradníkov ani nejdem komentovať, na to sú po webe grafy a štatistiky, potvrdzujúce opak.        V boji proti kríze sme udržali 160-tisíc pracovných  miest. - neviem sa zodpovedne vyjadriť, možno nejaké naozaj udržali, toto číslo sa mi ale zdá byť prestrelené.        Euro prináša investície a pracovné miesta. - zásluha najmä bývalej vlády, sám súdruh predseda predsa v roku 2006 hovoril o € veľmi nejednoznačne, až prišiel na koberček pred guvernéra NBS.        Ohlásení investori vytvoria tisíce pracovných miest. - bohužiaľ, vďaka nekonaniu vlády je ich násobne menej ako za predchádzajúcich vlád.        Udržali sme ceny energií, znížili daň a cenu nafty. - spotrebná daň na naftu klesla až po tom, ako dopravcovia odmietli držať ústa (čo ich teraz stojí aj pracovné miesta, prípadne zníženie objednávok). Okrem toho, pred voľbami som počúval o znížení spotrebnej dane na palivá, nie len na naftu.        Znižujeme ceny liekov, zrušili sme poplatky. - moja starká platí za zhruba rovnaké lieky podstatne viac, než je to v roku 2006 + inflácia.        148 km nových diaľnic prinesie rozvoj do regiónov. - väčšina začatá minulou vládou + diaľnice pekne zacvakajú naše deti a budúce vlády.        Počet nehôd na cestách je na historickom minime. - nazvime to pravým menom - vďaka zákonu sa menej nehôd ohlasuje na polícii.        Modernizujeme školstvo, zvyšujeme platy učiteľov,  podporujeme vedu a výskum. - máme najmodernejšie školstvo, účí sa bez učebníc a titul sa dá získať za max. pol roka.        Dostavali sme budovu Slovenského národného divadla,  rekonštruujeme Bratislavský hrad. - SND sa dostavalo primárne za peniaze minulej vlády - ale súhlasím, kto položí poslednú tehličku, ten to predsa "dostaval". Hrad sa obnovuje draho - veď aj kamaráti musia zarobiť, nie? Je predsa nejaká tá kríza.        Vstupom do Schengenu padli hranice. - súhlasím aj s tým, že veľa práce urobila táto vláda. Vstup do SP však pokračovaním integrácie do EÚ, čo je zásluha minulej vlády.         Slovensko je rešpektovaný partner v Európe a vo  svete. - platí zhruba text bodu 15.        Ochránili sme slovenský jazyk, odmietli  veľkomaďarský nacionalizmus. - chlieb nezlacnel, "veľkomaďarský nacionalizmus" nepociťujem aj napriek tomu, že bývam na južnom Slovensku.        Staviame národné štadióny, ako aj 600 ihrísk v  mestách a obciach. - cena štadiónu je zhruba 2-3x vyššia ako v okolitých krajinách. Nevadí, my na to máme, cesty v regiónoch počkajú.        Kontrola verejných obstarávaní je v rukách opozície,  vyvodzujeme zodpovednosť pri zlyhaní ministrov. - do troch minút, súdruh premiér? S rovnakým metrom na všetkých ministrov, súdruh premiér?        Na Slovensko sme priniesli stabilitu. - nepociťujem, neviem sa vyjadriť. Podľa mňa však nie väčšiu, ako za minulej vlády.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Správy STV/RTV:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Červená tabuľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            02 je drahé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Dlhodobý problém 02
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Prečo nie je 02 môj hlavný operátor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kotrčka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kotrčka
            
         
        kotrcka.blog.sme.sk (rss)
         
                                     
     
        25/184/72 - narodený v deň, keď má meniny Adolf, IT, hamradio a cyklofanatik...

 
  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    63
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1383
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            OpenSource
                        
                     
                                     
                        
                            Fotografovanie
                        
                     
                                     
                        
                            Cestovanie a iné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sex na pracovisku
                     
                                                         
                       Má Ficova vláda prepojenie na scientológov?
                     
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Privatizácia Smeru
                     
                                                         
                       Cargo – obrovské dlhy, vysoké platy a pokútne spory
                     
                                                         
                       Na ceste po juhovýchodnom Poľsku
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Počiatkov pochabý nápad: zadržiavanie turistov za 30 EUR
                     
                                                         
                       "Rómsky problém"
                     
                                                         
                       Rómska otázka – aké sú vlastne fakty?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




