
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Šimčík
                                        &gt;
                Británia
                     
                 Sú Slováci inteligentnejší ako Briti? 

        
            
                                    21.3.2010
            o
            15:34
                        (upravené
                28.3.2010
                o
                23:13)
                        |
            Karma článku:
                7.17
            |
            Prečítané 
            1978-krát
                    
         
     
         
             

                 
                    Slobodne vyjadrený názor a následná diskusia patria k základným pilierom demokracie. Nepreháňame to však trochu?
                 

                 
  
   Kedže som už dlhšiu dobu uviaznutý tu na Ostrovoch, patrí sa mi ako pseudointelektuálovi aby som sa zaujímal nielen o to čo sa deje doma ale aj tu. Hoci v práci máme každý deň  k dispozícii množstvo denníkov s pochybným obsahom (Mirror,Mail,Express,Sun), z času na čas navštevujem aj bohatú elektronickú verziu (považovanú za najväčšiu na svete) denníka The Guardian.   Okrem viditeľne výššej kvality a rôznorodosti článkov, najväčším rozdielom, ktorý som postrehol je množstvo diskusných príspvekov. Kým na sme.sk sa príspevky k politickým článkom počítajú rádovo v stovkách, na guardian.co.uk je to priemene medzi 50 až 100, pričom téma musí byť skutočne pálčivá.  Keď však tieto čísla  porovnáme s populáciou oboch krajín, tj. 5 mil. k 62 mil., výsledok ma navádza k pocitu, že som niekde urobil pri rátaní chybu.   Alebo sú skutočne Briti takí nevzdelaní alebo obmedzení, že nedokážu viesť diskusie a adekvátne reagovať na články? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Ako Maťo Ďurinda reggae hral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O retre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O hmle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O nechcených vianočných darčekoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Keď jeden chýba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Šimčík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Šimčík
            
         
        simcik.blog.sme.sk (rss)
         
                        VIP
                             
     
        Agropunk, vraj aj Mod, stratený ako working class hero na východoslovenskom vidieku.  Rád chodím na futbal, koncerty, do pubu, kostola a z času na čas aj na disco :)

"Myslím, teda slon"


  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    186
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názory
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Subkultúry
                        
                     
                                     
                        
                            Británia
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Čo sa domov nedostalo
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Roger Krowiak
                                     
                                                                             
                                            Rudolf Sloboda - Do tohto domu sa vchádzalo širokou bránou
                                     
                                                                             
                                            Subculture - The Meaning Of Style
                                     
                                                                             
                                            F.M. Dostojevskij - Idiot
                                     
                                                                             
                                            NME
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Arctic Monkeys
                                     
                                                                             
                                            The Gaslight Anthem
                                     
                                                                             
                                            Slovensko 1
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            BBC Radio 2
                                     
                                                                             
                                            BBC Mike Davies Punk Show
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tatran Prešov
                                     
                                                                             
                                            Bezkonkurenčný Dilbert
                                     
                                                                             
                                            flickr
                                     
                                                                             
                                            discogs
                                     
                                                                             
                                            ebay.co.uk
                                     
                                                                             
                                            Banksy
                                     
                                                                             
                                            Kids And Heroes
                                     
                                                                             
                                            Roots Archive
                                     
                                                                             
                                            West Ham United
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            denník Guardian
                                     
                                                                             
                                            denník Pravda
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




