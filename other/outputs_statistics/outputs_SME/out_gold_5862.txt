

 Preto som si položil už dávnejšie aj sám otázku, prečo sú vlastne náckovia krajná pravica, keď nastoľujú ľavicové témy. Nie som politológ, preto som sa spýtal jedného z nich: 
 "Toto je iba jedna dimenzia - ekonomicka (silný štát, "sociálna" rétorika, paternalizmus). Ale v prípade (neo)nacizmu sú dôležité aj iné rozmery, najmä rozmer kultúrno-politický - radikálny nacionalizmus, rasizmus, sociálne ochranárstvo, izolacionizmus, reakčnosť - toto nie sú ľavicové hodnoty. Takže by som rozhodne neoznačoval náckov za krajnú ľavicu, to by vnieslo do diskusie iba zmätok." 
  Páčil sa mi názor diskutéra Kemera, ktorý napísal: 
 "Dá sa to zjednodušiť aj na konštatovanie, že nacisti sú zmes toho najhoršieho z oboch táborov." 
   
 Len neviem, či národným populistickým ľavičiarom chýbajú tie atribúty, ktoré politológ prisúdil krajným pravičiarom odlišujúce ich od kolegov z ľavého okraja. Skúste nájsť rozdiely: 
   
 Citáty z článku kandidáta na poslanca do NR SR Mariána Mišuna za Kotlebovu stranu:  
 "Osobne si myslím, že v prvom rade Rómovia by mali byť sebakritickejší. Prečo sústavne hľadajú pôvod vlastnej traumy v Slovákoch?" 
 alebo 
 "Slováci by sa zase mali prestať obviňovať samých seba za to, ako dnes väčšina neprispôsobivých Rómov žije. Veď nik z nás im nekáže, aby neposielali svoje deti do školy, aby živorili v špine, ničili svoje ľahko nadobudnuté byty. O kriminalite ani nehovoriac." 
   
 A tu je pár výrokov vicepremiéra pre ľudské práva a menšiny Dušana Čaploviča z rozhovoru v denníku SME: 
 „Samozrejme, ale mnohí sa nachádzajú v sociálnej situácii, do ktorej sa dostala časť rómskej menšiny aj vlastnou vinou."  alebo 
 „Je to aj chyba spoločnosti, ale prečo sa niektorí Rómovia vedeli z tohto prostredia vymaniť a integrovať sa? Ale vy si vyberáte len tých 690 osád." 
 alebo 
 „ Je zvláštne, že skoro každý útok na Róma sa považuje za rasizmus, ale útok Rómov na občana väčšinového národa už rasizmom nie je. To odmietam. Pred právom je každý občan rovný."  alebo 
 "Ak Čaplovič nebude platiť za elektrinu a vodu, tak nech ho odpoja. Keď vypnú Rómov, tak je z toho problém." 
   
 ...Tak ako? Našli ste rozdiely? 
   
 Samozrejme je hlúposť spájať týchto dvoch pánov, hádam by ste neverili tomu, že v ich názoroch sa dá nájsť niečo spoločné!!! Veď Čaplovič sa pri každej možnej príležitosti dištancuje od extrémizmu a vyhlasuje mu nulovú toleranciu a Mišun tento policajný štát a jeho vládu nemôže ani cítiť. 
   
   

