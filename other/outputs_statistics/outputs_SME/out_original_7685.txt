
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Stará dobrá víchrica 

        
            
                                    31.5.2010
            o
            15:52
                        (upravené
                31.5.2010
                o
                15:58)
                        |
            Karma článku:
                3.99
            |
            Prečítané 
            684-krát
                    
         
     
         
             

                 
                      Priznávam sa celkom dobrovoľne, že tak ako vždy predtým sa aj teraz pravidelne pozerám na televízny program Let´s Dance a hoci v ňom neúčinkuje Zuzana Fialová, ktorá mi bola synonymom tanečnej dokonalosti v technike, výraze a predovšetkým v celkovom dojme, je to aj tentoraz skvelá zábava. Jasné, opakovanie relácie je tak trošku ako obohratá platňa, mnohé tu už bolo, niečo sa skôr paroduje, než sa hľadá nový aspekt hodnotenia, prípravy či realizácie a v konečnom dôsledku sa dá tvrdiť, že je to zasa predovšetkým na účinkujúcich.
                 

                     Pochopiteľne, sú to tí, čo tancujú a ich špecializácia má dva aspekty. Jedných vybrali spomedzi známych osobností a tí druhí sú profesionálni tanečníci, takže plánovane vzniklo spojenie amatérov s majstrami remesla a keďže to všetko sledujeme v opakovaných reprízach, v mnohom sme poučení a vieme, čo treba očakávať. Voľakedy ma Zuzana Fialová očarila, napísal som o nej na blogu a  dodnes pamätám, koľko súhlasu v tomto smere ma zastihlo. A nielen mužov, lebo Zuzana je v dobrom slova zmysle synonymom vampa, ale veru aj žien, ktoré sa dokázali povzniesť nad niečo ako je žiarlivosť či závisť a tak trošku som sa tešil, komu budem fandiť v tomto pokračovaní.   Žiaľ, začiatok bol všednejší, nikto ma neoslovil, nikoho som nekriticky neobdivoval a hoci som držal palce Andymu Hricovi, aby po telesnej aktivite úspešne chudol, to bolo naozaj všetko. Potom však, ako súťažné kolá pribúdali, začal som mať slabosť pre dievčenskú skromnosť a dostatok odvahy mladučkej Nely Pociskovej, ktorej viacnásobný laureát tanečných súťaží Modrovský pripravoval úžasný priestor vyniknúť a čuduj sa svet, mojou najväčšou záľubou sa stalo vystúpenie dámy speváckeho neba Slovenska z minulosti Evy Mázikovej!    Má niekoľko daností, celkom špecifických a okrem toho že je to žena skutočne veľká – ako speváčka i výškou – patrí medzi krásavice. Predovšetkým predtým, než sa stala hviezdou tejto série Let´s Dance, veď už odpočítala šesť dekád svojho bytia na tejto zemičke, ale aj tak si predikát obdivuhodného ženstva obhajuje bezproblémovo aj dnes. Pestovaná, vznosná, s nádherne dlhými a pekne tvarovanými nohami, úžasným poprsím so značkou „in natura“, pestovanou tvárou ktorej dominuje hriva bujných vlasov a krásnym zadočkom, s ktorým je podľa svojho vyhlásenia spokojná aj dnes! To je vonkajšia, dobre viditeľná fasáda a som si istý, že väčšina chlapov ju aj tak vidí tak, ako ju vnímal Jozef Króner v tom sude kapusty, alebo  obecenstvo v televízii či na koncertoch, keď nám spievala svoj repertoár.   Tá druhá podoba Evy Mázikovej je však ešte úžasnejšia! Verejne známa, úspešná, krásna, vždy v strede záujmu verejnosti, prajníkov i tých zlých a predsa sa jej darilo unikať rečiam, klebetám a ohováraniu, alebo tlaku bulváru. Možno preto, že žila cnostne, podľa etických zásad dobrých ľudí, som si však istý že predovšetkým preto, že jej životným priestorom sa stalo Nemecko, jeden stály životný partner a bytie v každom ohľade transparentné.   Pamätáte sa, z času na čas nám prišla zaspievať, stala sa zasa hviezdou Repete a aj koncertných pódií a tak, ako jej pribúdali rôčky, dala sa nám spoznávať. Je, prepáčte mi ten výraz, na „atómový pohon“, má nevyčerpateľné zásoby energie, optimizmu, túžby vynikať, nedať sa, presadiť sa a nestáť za žiadnu cenu v kúte, čo v primeranej miere zvýrazňuje či tak trošku aj preháňa. Cítite, ako potrebuje všetkému okolo seba vtlačiť svoj charakter, svoju vôňu či priamo vizáž a tak, ako súťažné kolá napredovali, v Let´s Dance jej bolo stále viac!   Organizovala takmer všetko na „vlastné brdo“, režisér, produkcia či ostatná partia podieľajúca   sa na priamom prenose všetko s pokorou akceptovala a hoci to naozaj nebolo vždy kóšer a niekedy bolo všetkého až priveľa, ľudia sa bavili a bavia! Eva Máziková prešla so finále, hoci bola najstaršou účinkujúcou a verte mi, aj keď to možno naozaj nevyhrá, rozlúči sa s nami príťažlivo a interesantne! Porotca Bednárik jej dal za salsu desiatku, známku snov, český tanečník svetového rangu Chlopčík má pre jej vystúpenia naozaj slabosť a obe dámy z poroty ju akceptujú s porozumením, čo je jednoducho skvelé!   Je mi jasné, že takýto priestor v zábavnej relácii určenej veľkému množstvu obecenstva sa nedá akceptovať ako všeobecný princíp televíznej tvorby. Skôr naopak, pravidlá hry bývajú jasné a každý má robiť naozaj iba to, čo má, čo je jeho povinnosť a aké má možnosti, no tentoraz to dopadlo ináč. Moje presvedčenie, že Eva Máziková všetko okolo seba prevalcovala z vlastného rozhodnutia je platné iba potiaľ, kým pripustím veľkorysosť a pochopenie všetkých dôležitých, pre ktorých sa táto erupcia energie, svojvoľnosť i vlastná originalita speváčky stala zdrojom zábavy väčšiny aj s prižmúrením očí, ba možno aj so škrípaním zubov. Zábava je však najdôležitejšia, to platí prvoplánovo a ľudským chúťkam niekedy netreba rozumieť!   Bolo finále, napokon zvíťazila mimoriadne príťažlivá Nela Pocisková, miláčik všetkých, ale na striebornom piedestáli sa usadila pani Eva, ako ju volal šarmantný porotca z Čiech Zdeněk Chlopčík a vychutnala si to nielen s noblesou Kráľovnej zo Sáby, ale aj štedro ako skvelý mecenáš. Pečené prasiatko podávané v produkcii jej „pážat“ bolo pastvou pre oči a som si istý, že pečať, čo tomuto ročníku tanečnej súťaže vtlačila akurát svojou aktivitou, je akceptovateľná.   A tak má Eva Máziková zaručené, že táto séria Let´s Dance bude spojená aj s jej menom, s jej energiou, s jej vlastnými mizanscénami a  v pamäti nám zostane aj ona sama. Ako obdivuhodná žena s celkom osobitými danosťami. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




