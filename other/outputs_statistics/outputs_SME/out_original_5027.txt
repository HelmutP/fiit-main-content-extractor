
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nika Kollárová
                                        &gt;
                Nezaradené
                     
                 Káva, sukňa, múza a zákon prírody 

        
            
                                    22.4.2010
            o
            10:12
                        |
            Karma článku:
                5.52
            |
            Prečítané 
            1193-krát
                    
         
     
         
             

                 
                    Ráno som sa poliala kávou a tak čakám kým mi vyschne sukňa. Keď človek nosí rifle celý život a ešte viac a príde pekný deň, keď má chuť obliecť si sukienku, tak sa samozrejme musí poliať kávou. V paralelnom vesmíre, kde som Škót s chlpatými nohami a pokrivenou tvárou od vetra som si dnes pravdepodobne obliala kilt.
                 

                 Moja prvá reakcia smerovala na dobrú kávu na stole, na zemi...a na mojej sukni!!! (Ktorá je vlastne vraj tričko, ale tak..kupovala som ju/ho/to v krajine Guinnessu, kde je priemerná váha čajočiek vyššia ako na Slovensku, tak som sa mohla aj seknúť.) Postavila som si vodu na kafe, vodou vyšúchala sukňu, zavesila na „žinku" (rozumej „šňúrik natiahnutý cez izbu") a pijem to kafe. Ak budem meškať do školy...   Áno, obvykle sukne na mne nevidno. Skejtové (píšem ako počujem) boty sú totiž až príliš pohodlné a pri horskom zleze dole kopcom z intráku necítim každý kamienok, plechovku, skrutku, vrchnák ako v balerínkach. Tie sú dobré ako papuče. A k skejtovým botám sa veľa sukien nehodí. Skúste chodiť v pohodlnej obuvi a mať oblečené niečo nepohodlné. Skúste byť pohodlným človekom a obliecť si niečo nepohodlné. Skúste si obliecť seba. Klikám na tretiu možnosť. Odoslať. Ďakujeme za Vašu dnešnu voľbu. Prajeme krásny deň.   Moja prvá múza. Matróna. Dokonalosť sama o sebe. Romantizmus v 21. storočí. Aristokracia bez titulu a na vidieku. Každý pondelok ráno o 5 som ju vídavala na zástavke. Tá grácia. Nádherný príklad toho, na čo by som sa v živote nepozrela ani keby ma obkľúčili 30 predavači kradnutých parfémov na prázdnej zástavke, poviazali ma a násilím mi otvorili okále, že, kukaj, ne, na ty hadry. Čo je vlastne nereálna situácia. Ako môže byť niekto na prázdnej zastávke, chápete. Sú veci, ktoré by som si naozaj neobliekla. Ale lejdy de ona to nosila, lebo skutočne bola lejdy de ona. Pre svoju karmu, pre svoju osobnosť, ktorá z nej žiarila aj o 5 ráno bola mojou prvou múzou.   Oblečenie, podľa môjho skromného názoru bez akéhokoľvek titulu, ktorý by mu dodal vierohodnosti a váhy, má dopĺňať tú karmu/vyžarovanie/osobnosť. Zákon prírody, ktorý neoklameme.   Už mi vyschla sukňa, tak končím tento článok Nechce-sa-mi-písať-seminárku-radšej-urobím-niečo-zbytočné-a-napíšem-o-ničom-ktorý-je-škoda-čítať-tak-nečítajte. Aj Škótovi už vyschol kilt, predpokladám, ak mu ho ten vietor neurval zo žinky. Čas ísť na mestskú hromadnú dopravu a obzerať múzy alebo si zahrať na gajdách.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nika Kollárová 
                                        
                                            Peňazí ako pliev
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nika Kollárová 
                                        
                                            Film o (Ne)Filme
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nika Kollárová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nika Kollárová
            
         
        nikakollarova.blog.sme.sk (rss)
         
                                     
     
        redhead mindcracker
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    889
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ficova vláda podporuje barbarské vysušovanie Slovenska
                     
                                                         
                       Zaprdení sudcovia dostali od Fica zelenú
                     
                                                         
                       Banda farizejov podporuje birmovaného komunistu
                     
                                                         
                       Kiska for president!
                     
                                                         
                       Pre istotu po lopate: volím Kisku! (Malá recenzia pridaného letáku)
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Čím sa kandidát Fico zabudol pochváliť
                     
                                                         
                       10 otázok pre Roberta Fica na TA3
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




