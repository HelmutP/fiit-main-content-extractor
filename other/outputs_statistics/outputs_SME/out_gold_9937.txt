

   
 DÁŽĎ 
 Tak veľmi dážď už prší 
 v tej mojej tichej duši. 
 Kto trápiť chce ma, 
 len toľko poviem: 
 Som len žena. 
 Mýlia sa všetci čo ma súdia, 
 ale nech, veď sú to iba ľudia. 
 Vraj podľa seba posudzujú iných 
 no v sebe nemám kúska špiny. 
 Mám myseľ čistú 
 a som len kus prázdneho listu. 
 Neznášam závisť, zlobu 
 a stále zdá sa, že vybrala som 
 tú nesprávnu dobu na žitie 
 Tak všetci, čo ste bez  viny 
 hoďte tým pomyselným kameňom. 
 A zlobu rýchlo prežrite, 
 pred blížiacim sa súdnym dňom. 
 Veď, možno prvá, hodím kameňom... 
 Ach, ako ľahko sa to hovorí, 
 možno mi chýba, trochu pokory... 
   
 Mona Alberti 
   

