
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Oružinský
                                        &gt;
                Nezaradené
                     
                 Krok 1 budúcej vlády: Nezávislé a transparentné výberové konania. 

        
            
                                    18.6.2010
            o
            12:35
                        |
            Karma článku:
                9.19
            |
            Prečítané 
            1293-krát
                    
         
     
         
             

                 
                    Práve som sa dočítal, že stovky až tisíce politických nominantov súčasnej vlády príde o prácu. Je to pomerne logická úvaha - po tej invázii politických nominantov, ktorú sme zažili v posledných 3-4 rokoch.
                 

                 Koalícia Smer-SD, ĽS-HZDS a SNS sa s ľuďmi, ktorí čo i len trochu brzdili ich zámery, nebabrali. Výsledkom toho bol exodus ľudí s aspoň štipkou svedomia zo štátnej a verejnej správy aký nepamätajú ani ľudia, čo zažili na vlastnej koži normalizáciu začiatkom 70-tych rokov minulého storočia. Väčšina politických nominantov bola do štátnej a verejnej správy nasadená s jasným zadaním: Rozbiť, čo sa dá rozbiť. V zruinovanej spoločnosti sa rabuje najlepšie.   Potreba obnovenia civilizovanejších vzťahov v spoločnosti si priam pýta radikálnu zmenu - teda výmenu „príšelcov", ktorí na svojich teplučkých postoch nemajú čo robiť. Problém je však v tom, že okrem ministrov, by mali byť všetky ostatné pracovné pozície pozíciami odbornými, nie politickými.   Ak budúca vláda vymení väčšinu politických nominantov za „svojich", urobí obdobnú vec ako urobila Ficova vláda. Urobí vlastne to isté. Možno to bude vyzerať navonok miernejšie, ale podstata bude tá istá. Hulvátsky prístup nahradí bielogolierový prístup - avšak stále to bude ten istý prístup - akurát s iným prívlastkom, inak zabalený.   Preto je dôležité, aby zmena, ktorú nám dnešní zostavovatelia budúcej vlády pred voľbami sľubovali, bola naozaj zmenou - čiže zmenou radikálnou - t.j. od koreňov problémov. (Pre tých, čo nemajú radi slovo radikálny - zmenou zásadnou a principiálnou.)   Zásadným problémom štátnej a verejnej správy je, že je prepolitizovaná. Úradníci zabudli, že štátny správa je štátna služba - služba štátu, služba spoločnosti. Mnohí však neslúžia spoločnosti, neslúžia Ústave Slovenskej republiky a zákonom Slovenskej republiky, neslúžia morálke a cti. Namiesto toho sa správajú amorálne a bezcharakterne - hľadajú v zákonoch diery - diery, cez ktoré si naplnia svoje vrecká sponzori politických strán. Štátna a verejná správa hlboko prerastená partajnými záujmami je zásadný problém správy veci verejných na Slovensku.   Budúca vláda by mala jasne zmeniť tieto „pravidlá hry". Ak pani Radičová - verím, že budúca predsedkyňa vlády - presadzuje slušnosť a úctu v politike a spoločenskom živote, mala by tieto svoje slová premietnuť aj do konkrétnych skutkov, ktoré spoločenský život ozdravia. Má na to predsa jasný mandát a skvelý potenciál.   Základným ozdravným opatrením je zmena nominácii do štátnej a verejnej služby. Ľudia v tejto sfére by nemali byť do svojich pozícii nominovaní, ale starostlivo vyberaní. Podľa najlepšieho vedomia a svedomia.  Znie to možno naivne, ale pokiaľ sa budeme vyhovárať, že je to nereálne, potom nemôžeme očakávať skutočnú zmenu. Potom skutočnú zmenu nechceme, len sa tvárime, predstierame, že ju chceme. Točíme sa v bludnom kruhu, neschopní vykročiť na cestu, o ktorej sme v hĺbke svojej duše presvedčení, že je jedinou správnou, pravdivou cestou. Mnohí sa v nej točíme práve preto, že nie sme zásadoví, ale sme obyčajní oportunisti. Čakáme na svoju príležitosť, a potom berieme všetko.   Aby štátna a verejná správa bola funkčná a efektívna, ľudia, ktorí v nej pracujú, ktorí pracujú pre nás, musia prejsť nezávislými a transparentným výberovými konaniami. Musia preukázať v prvom rade svoju odbornosť, vedomosti a skúsenosti, ale tiež lojalitu voči štátu. Nemýľme si lojalitu voči štátu s lojalitou voči konkrétnej politickej strane. Ľudia v štátnej a verejnej správe by mali byť maximálne nestranní. Mali by rozhodovať na základe dohodnutých pravidiel, odborných argumentov a v prospech občanov.   Som presvedčený, že nezávislé a transparentné výberové konania nie sú len naivnou predstavou. Spôsob akým prebiehali mnohé tzv. výberové konania za Ficovej vlády (ak vôbec prebiehali) je zároveň dôkazom, že aj malé vylepšenia procesov môžu priniesť významné zlepšenie. Jednoducho povedané: Ficove „výberové" konania bol tak o ničom, že pojem „výberový" bol len nálepkou na vopred zmanipulovaných procesoch. Ukážkovým príkladom sú napríklad „výberové" konania na riaditeľov národných parkov, inými slovami - trápne divadlo. O tom, kto bude novým riaditeľom rozhodovala komisia výrazne majoritne zložená z čelných predstaviteľov rezortu životného prostredia, t.j. podriadená ministrovi - politickému nominantovi. Mnohé nominácie na riaditeľov národných parkov boli vopred jasné.   O zložení výberovej komisie nemôže rozhodovať minister. Aby mohli byť výberové konania považované za nezávislé a transparentné, musí ich realizovať subjekt, ktorý na to má prirodzené predpoklady. To však samotné ministerstvá alebo iné úrady samy o sebe nie sú. Určite nie dnes.   Aby sme sa posunuli smerom k väčšej nezávislosti a transparentnosti výberu uchádzačov o pracovné pozície v štátnej a verejne správe, je do tohto procesu nevyhnutné v maximálnej miere zapojiť renomované zahraničné personálne agentúry. Ich postavenie na trhu je omnoho väčšou zárukou kvalitných výberových konaní, než proklamácie politikov. Ak politikom ide naozaj o zmenu, dajú to jasne najavo. Ak nie, dajú to tiež jasne najavo. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Vládnu klamári hlupákom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Sociálna demokracia alebo stranícky centralizmus?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Dodrží premiér Fico svoje vlastné pravidlá?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Postaví sa "dolných" 10 tisíc svojvôli "horných" 10 tisíc?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Už od vás vaša škola pýtala mydlo a toaletný papier?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Oružinský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Oružinský
            
         
        oruzinsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        1/∞
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    120
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2310
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inžinieri bojujú...
                        
                     
                                     
                        
                            ...šírime strach a paniku
                        
                     
                                     
                        
                            kauza Tichá a Kôprová dolina
                        
                     
                                     
                        
                            ÚLOŽISKO NEBEZPEČNÉHO ODPADU
                        
                     
                                     
                        
                            Hyperboly
                        
                     
                                     
                        
                            POLITIKA
                        
                     
                                     
                        
                            PYROEKOLÓGIA
                        
                     
                                     
                        
                            Zo života cicavcov...
                        
                     
                                     
                        
                            OCHRANA PRÍRODY
                        
                     
                                     
                        
                            LZ The Bark Beatles
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Eoin Duignan
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




