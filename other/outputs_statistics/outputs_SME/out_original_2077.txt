
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tatiana Melisa Melasová
                                        &gt;
                Všeobecné
                     
                 Deti a rodičia drogy spolu nezničia (aspoň u nás) 

        
            
                                    19.1.2010
            o
            7:53
                        (upravené
                19.1.2010
                o
                9:20)
                        |
            Karma článku:
                15.98
            |
            Prečítané 
            5713-krát
                    
         
     
         
             

                 
                    Denne sem chodia nešťastní rodičia, pretože ich dieťa berie drogy. Sama nie som rodičom a preto sa mi ťažko radí v týchto veciach. Niekedy ma Vlado zavolá do kanclu a tak si vypočujem ich problémy. Väčšinou tam iba tak sedím a hrám sa s dierkou na teplákoch. Počúvam, ale jediné, čo im môžem povedať je, ako to bolo v mojom prípade. Teda v prípade fetujúceho dieťaťa. Z pohľadu mojich rodičov to komentovať veľmi nemôžem, pretože čo robili na moju záchranu, ma vtedy nezaujímalo. Mala som iné priority. A dnes sa to bojím opýtať, aby som sa náhodou nedozvedela pravdu.
                 

                 Raz mi mama povedala, že zas keď som odišla z domu bez stopy, sa s otcom o tom ani nemohli baviť, lebo tak veľmi ich to bolelo. A to som sa nepýtala! Takže si nechcem ani predstaviť, čo by bolo, keby som na konkrétne otázky dostala konkrétne odpovede. Niektoré veci je lepšie nevedieť. Aspoň v mojom prípade určite.  Veľa rodičov nemá ani poňatia o tom, čo obnáša fetovanie. Hlavne, keď sa jedná o ich dieťa. Po dlhom čase si všimnú „prvé“ príznaky, že niečo nie je v poriadku. Kým prídu na to, že by v tom mohli byť drogy, ich milé dieťa v tom lieta až po uši. V prvom momente sú síce nahnevaní ale potom väčšina aj tak začne hľadať chybu v sebe. Hlavne vtedy, keď majú ešte aj iné dieťa, ktoré sa im naopak „vydarilo“. Po hlave im chodí otázka: „ Kde sme spravili chybu, veď sme ich vychovávali rovnako?!“ Toto je presne aj môj prípad. Mám staršieho brata, ktorý patrí práve k tým vydareným deťom. Celý život, čo ho poznám patrí k tým šťastlivcom, ktorí preplávajú životom bez väčších problémov a všetkého, čoho sa dotkne sa mu podarí. Ozajstná pýcha rodičov. Ja som presný opak, teda tá nevydarená. Všetkého, čoho som sa dotkla, som kompletne dosrala. A to nad očakávanie všetkých, to mi verte! Moji rodičia si veľakrát mysleli, že viac sa to pohnojiť už nedá, ale ja som ich vždy prekvapila a dala im hlavu ešte do väčšieho smútku.  To, že feťáci žijú v sebaklame neplatí len o nich, ale aj o ich rodičoch. Často si nepripúšťajú, že práve ich decko by malo skončiť fatálne. Veria, že to zvládne a všetko to pripisujú pubertálnemu výkyvu, zlým partiám a podobne. Hľadajú stopy po vpichoch a kým ich nenájdu, tak je všetko v poriadku, ešte to nebude také vážne, to ho prejde a podobne. Keby moja mama mala hľadať stopy, tak by sa teda nahľadala aspoň osem rokov. Stopy po vpichoch som nemala prvých osem rokov fetovania a môj stav bol vážny už po roku.   V šestnástich som začala chodiť k psychiatrovi. Bola to prvá snaha o moje vyliečenie. Jeho meno nespomeniem, lebo ešte stále má prax v našom meste a toto by nebola preňho veľmi dobrá reklama. Moje liečenie u tohto doktora spočívalo v tom, že som prišla do ordinácie a nadiktovala mu, aké tabletky si prajem. To, že mi predpisoval množstvá ako pre koňa, ho vôbec nezaujímalo. Na začiatku mi dal lieky, ktoré sa nemôžu brať dlhšie ako sedem dní, lebo spôsobujú závislosť. To mi pravdaže zabudol oznámiť. A tak som si okrem heroínovej závislosti, pridala ešte aj závislosť od liekov, ktoré som zapíjala chľastom. Potom mi už nezaberali a tak som mu diktovala iné, o ktorých som si zistila zloženie, aby mi čo najviac zašľapali.  Veľkú hlavu si z toho nerobil, iba sa ma opýtal, či si z toho robím „kokteil“. Povedala som mu, že áno a on mi to odobril. Práve preto mu veľmi pekne ďakujem, lebo som mohla dlhé roky fetovať v podstate za pár korún.   Raz ho zo mňa trafil šľak, lebo sme sa pohádali pre tabletky a museli ho vynášať na nosidlách. Sestrička mi potom vyhovela. Rada som k nemu chodila, pretože mi predpisoval kopu drog a v čakárni bola sranda. Staré babky mi líčili ako sa pomočujú a alkoholikom som čítala ich správy o absolvovaní protialkoholického liečenia, lebo oni sami už videli dvojmo a nevedeli to prečítať. A to neboli doma ani dve hodiny. Čo viac k tomu dodať?!  Ale napriek tomu to viedlo v našej rodine k totálnemu uspokojeniu. Naši boli spokojní, že si liečim svoju závislosť a ja som bola spokojná, že mám oblbováky. Akurát, že to neriešilo absolútne nič. Aj tak si myslím, že moji rodičia spravili ako najlepšie vedeli. Ja sama by som nevedela ako sa zachovať na ich mieste. Mali smolu, že si vylosovali asi to najhoršie feťácke decko zo všetkých. Poriadne som ich potrápila, poodrbávala, povydierala  a ešte všeličo iné, o čom sa mi nechce ani písať. Všetko hodili za hlavu a sú ochotní aj naďalej pri mne stáť a pomáhať mi. Aj keď otec sa chystá umrieť čo najskôr, aby sa nestal bezdomovcom, pre moje dlhy. A statočne nahovára aj mamu, aby spravila to isté. Dúfam, že si to ešte premyslia.  A tak milí rodičia, jediné čo vám s určitosťou môžem poradiť je, aby ste si nenechali skákať po hlave od svojich ratolestí, či sa to už týka drog alebo nie. Veľmi múdro to neznie, ale zakladá sa to na pravde. A nedôverovať im, lebo aj tak iba stále klameme kvôli vlastnému prospechu. To je jediné, čo nás zaujíma. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (57)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcii (3)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcií 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy  o prostitúcii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Verona
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Nikdy sa všetkým nezavďačíš
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tatiana Melisa Melasová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tatiana Melisa Melasová
            
         
        melasova.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8108
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Všeobecné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            DROGY-SOS
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




