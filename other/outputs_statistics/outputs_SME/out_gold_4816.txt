

   
 Úvod jeho stránky by mohol veľa naivných študentov zmiesť predovšetkým názvom „ Univerzita pre MODERNÉ SLOVENSKO“. Pravdu povediac, na začiatku sa mi zdalo, že ide o dôveryhodný projekt, keďže na bočných lištách som zaregistroval niekoľko odkazov nielen na Miklošovú literatúra, ale aj na odborné ekonomické publikácie, ktoré ponúka táto stránka zadarmo na stiahnutie. Avšak hneď po registrácií a vypočutí si prvej virtuálnej prednášky, moje predstavy o určitom druhu výučby sa stratili.  
 Veď len čo to môže byť za Univerzitu, ktorá má byť založená na 6 minútovom táraní (v ich ponímaní odborná prednáška ) o globalizácií, ktorá nemá nič spoločné s vecnou tématikou, a ktorej podstatou sú rozprávky o úžasnom vplyve automobilového priemyslu na Slovenskú ekonomiku, ktorú k nám dotiahlo práve SDKú. ( asi autor prednášky zabudol na to, že práve automobilový priemysel bol najviac postihnutý hospodárskou krízou a je nebezpečné pre akýkoľvek štát viazať si celú ekonomiku len na jeden sektor, ktorý je v súčasnosti najviac rizikový ). 
 Po účelovej prednáške sa presuniete na test, ktorý vôbec nemá nič spoločné s predvolebnou kampaňou, a už vonkoncom sa nesnaží poukázať na svoju SDKú-ovskú dokonalosť. Príkladom toho je aj jedna z otázok z testu: 
 
 
Príkladom      toho, ako Slovensko dokázalo svoju konkurenčnú výhodu využiť v prostredí      globálneho trhu je, že:  
 
 Slovensko       je v súčasnosti na prvom mieste na svete vo výrobe automobilov na       obyvateľa 
 
 
   
  
 
 dokážeme       si urobiť celosvetovú hanbu, keď do batožín civilistov pribaľujeme       výbušniny 
 predávame       emisné kvóty lacnejšie ako susedné krajiny 
 
 
 
  Asi podobnou formou je vytvorený dotazník pozostávajúci z 10 „náročných otázok“ , ktoré demagogicky odpovedajú na účelovo dávané primitivné otázky. 
 Zrejme pán Mikloš je až tak naivný, že si myslí, že všetky univerzity majú úroveň ako napríklad Bratislavská vysoká škola práva, ktorej licenciu na výučbu dala práve ich vláda. Ja len dúfam, že každý triezvo uvažujúci študent si uvedomuje, že to celé divadielko je len trápnou, hoci originálnou predvolebnou kampaňou a opäť raz nenaletíme na trápne divadielko SDKú na odlákanie pozornosti od ich nelegálneho financovania ich strany,  od podrobností, kedy prešustrovali 7 mld. pri nepoistení kurzového rizika pri predaji "zlatej sliepky" Slovenska- SPP, od prípadu, kde pod cenu sprivatizovali majetok za 330 mld. Sk, od predaja menšiny v strategickom podniku a s ňou aj manažérsku kontrolu, ako sa pomýliť pri výpočte rozpočtu a potom zvyšovať cenu benzínu, ako raketovo zvyšovať ceny energií, aby sa zahraničným monopolom vrátili náklady za 5 rokov. 
 Ja nezabúdam, a dúfam, že aj iní ľudia opäť nenaletia ľuďom ktorí nás už raz sklamali… 
   

