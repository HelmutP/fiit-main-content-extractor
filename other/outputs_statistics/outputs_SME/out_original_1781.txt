
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Javurek
                                        &gt;
                Slovensko
                     
                 Abrahám 

        
            
                                    13.10.2009
            o
            22:17
                        |
            Karma článku:
                11.25
            |
            Prečítané 
            4410-krát
                    
         
     
         
             

                 
                    Na úvod pripomínam, že nepôjde o praotca Abraháma, od ktorého Biblia odvodzuje židovský národ, ale o peknú dedinku neďaleko  Sládkovičova.
                 

                 Podľa webovej stránky obce tam žije 1111 obyvateľov, ak sa medzičasom niečo nezmenilo. Obec leží na juhovýchodnom okraji Trnavskej pahorkatiny. Celú oblasť možno charakterizovať ako kultúrnu step prevažne odlesnenú. Ja by som jednoducho povedal, že leží na rovine.   Archeológovia tvrdia a majú to doložené vykopávkami, že to územie bolo osídlené už od praveku. Ostatné historické podrobnosti si môžete nalistovať na stránke obce. Ak niekoho viac zaujíma príbeh patriarchu Abraháma, nájde ho na stránke "uniba".   My sme sa do Abrahámu dostali odbočkou z diaľnice Trnava - Nitra na Vlčkovce, potom cez Opoj a Majcichov. V Majcichove sme sa chceli pozrieť  do Vlčkovského hája za účelom "zberu" jesenných motívov, ale zabudol som doma podrobnejšiu mapu, tak sme šli ďalej do Abrahámu, ktorý som mal vopred trochu lepšie zmapovaný na google.maps.      Abrahám má namiesto hlavných ulíc pekné aleje, v strede dediny je kostol, ktorého počiatky spadajú do roku 1561. Na mieste pôvodného dreveného kostola dal v roku 1782 gróf Esterházy postaviť dnešný kostol v slohu neskorého baroka. V roku  1934 bol rozšírený o bočné lode.      Našim cieľom bol Abrahámsky park s kaštieľom grófa Esterházyho, ak sa to tak ešte dá  povedať... Park je súčasťou Abrahámskeho lesa. Ku kaštieľu to bola od kostola asi hodinová prechádzka. Orientačným bodom na okraji lesa je maximálny mobilný signál.        Typická krajina okolo lesa.      Lesné cesty sú tu vyasfaltované, pri nedeľnej vychádzke to vôbec nevadí.      Spadnuté listy pokrývajú asfalt.      Farebné detaily jesene... postupný prechod zelenej.      Akumulovaná červená.      Odtiene žltej.      Suché ratolesti svedčia o sporadickej, alebo  žiadnej starostlivosti o park.      Pod nohami nám škrípali žalude...      Slnečná žiara, tisíce stupňov Celzia...      Cestou ku kaštieľu cesta zarúbaná.       Jesenný celok.      Secesný kaštieľ, postavený v roku 1899 podľa plánu architekta E. Lechnera. Pôvodne tam boli dva kaštiele, celý areál je viditeľný na satelitnej mape. V realite už zarastá náletovými drevinami.       Dnes je kastiel vykradnutý a takmer ruinou, chodia  tam strielat paintballisti...      Staré platany a stará bolesť v podobe zahadzovania odpadkov všade naokolo. Tip na doplnkovú aktivitu paintballistov...    Kaštieľ bol niekoľko rokov využívaný DFN Bratislava ako detská ozdravovňa.  Starosta obce M. Vrbovský píše na internete, v diskusii ku kaštieľu, o jeho pohnutej histórii.   "...situácia okolo kaštieľa by mala byť pučením a výstrahou pre nás všetkých, ako sa pristupuje k pamätihodnostiam na Slovensku. Za všetkým treba vidieť ľudí a v tomto konkrétnom prípade má hrubé maslo na hlave bývalé vedenie Detskej fak. nem. BA, ktoré nechalo objekty po skončení prevádzky liečebne v roku 1996 bez dozoru schátrať a rozkradnúť. Následne liečebňa i s parkom bola predaná hoci park je vo štvrtom stupni ochrany čo je v rozpore zo zák. 543/2002. Potom liečebňa čiastočne vyhorela, park chátra. V súčastnosti je predaj riešený prokuratúrou, pre porušenie zákona." (zdroj)   Na záver ešte jedno pozitívne prekvapenie z Abrahámu, pri areáli družstva je predajný automat na pravé kravské mlieko. Výborné.       Abrahámovský mliečny automat dezinfikuje priestor a dávkuje 1 l, alebo 0,5 l mlieka do fľaše.    ... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Vieme prví, len či pravdu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            November
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Moja prvá fantastika
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Papierové mestá
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Spisovatelia v Smoleniciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Javurek
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Javurek
            
         
        jozefjavurek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Neoznačené fotografie na blogu výlučne z vlastnej tvorby (c). Inak je všetko vo hviezdach...
 
"štandardný už tradičný bloger, s istou mierou poctivosti" (jeden čitateľ)
 
 


Click for "Your New Digest 1".
Powered by RSS Feed Informer

počet návštev:







 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    650
                
                
                    Celková karma
                    
                                                4.59
                    
                
                
                    Priemerná čítanosť
                    2950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Český raj
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Myšlienky, názory
                        
                     
                                     
                        
                            Blogovanie
                        
                     
                                     
                        
                            Technika a technológia
                        
                     
                                     
                        
                            Domácnosť
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            EÚ
                        
                     
                                     
                        
                            Hory, lesy
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




