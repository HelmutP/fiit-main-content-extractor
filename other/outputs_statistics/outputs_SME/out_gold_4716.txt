

 ((pesnicka))  
 Po všetky dni si pestoval svoje kučeravé vlasy, dokonalé strnisko, thethovačky na ramene, bielo žiarivé zuby a úsmev číslo 9, ktorým balil čajočky. Osamelý Muž sa raz dostal vo svojom putovaní životom do krajiny večnosti. Milióny možností a on stále obdivoval seba. Lesnej víle bolo ľúto jeho mladého života, tak mu poslala naproti životnú Pravdu.    
 Pravda spolu s Mužom putovala krajinou od vekov na veky, ukazovala mu ľudské osudy, lásku, nekonečné rieky a jazerá, západ Slnka, úskoky smrti, klamstvá slastí, údolia múz a obety milovaným. Muž sa počas vekov zmenil. Zaľúbil sa do Pravdy. Bol ochotný ostrihať svoje kučeravé vlasy, nechať si bradu, nebieliť si zuby a smiať sa už len úsmevom 1, ba dokonca zaplakať. V podstate by za ňu dal všetko. 
  
 No takto to nefunguje. Prišli na koniec krajiny večnosti a Pravda sa s ním musela na veky rozlúčiť. Muža to zlomilo. Zlomilo sa v ňom všetko aj dôvera, ktorej ho učila Pravda celé veky. Zlomila mu srdce. Jediným slovom. Zbohom. 
  
 Zničený, osamelý Muž prišiel do krajiny divov. Blúdil, trúchlil sám a divy si s ním robili čo chceli. Samé divné veci. Tu sa nad ním zľutovala kráľovná, Zakliata morská špargľa. Táto nešťastná stvora, hoc bola kráľovná nesmela sa pohnúť od Zlovestnej prírody. 
  
 Tá ju mala na príkaz Pravdy strážiť, pretože raz veľmi ublížila jej synovi, zlomila mu srdce; kým nebudú všetky divy na Zemi objavené a spočítané. A tak sa Muž zotavoval pri Zakliatej morskej špargli. Tá mu hovorila príbehy vymyslené, zo sveta, vlastného života. Ľúbila ho, odkedy vkročil do divnej krajiny, no on v nej len prežíval, zo dňa na deň a ničoho si nevšímal, neprejavoval vďaku ani ľútosť. Bol zničený, ubolený, sklamaný.  Jednej jari, keď Zlovestná príroda zakvitla zvlášť pekne, sa Muž prebral k životu. Na svedomí to ale nemala Zakliata morská špargľa, vstal na nohy, poďakoval a odišiel bez otočenia, bez zamávania. Zakliata morská špargľa umrela žiaľom za láskou. 
  
 Možno si poviete, veď to ju vlastne oslobodilo. Nie je to tak, lebo kto ľúbi skutočne, je ochotný čakať aj na veky, aj v krajine divov, hoci len pre jeden pohľad.  Muž odišiel za Klobúkovou virtuózkou do Sveta. Tá mu počarovala. Zaľúbil sa. 
  
 Do jej smiechu, divokosti, neskrotnosti, vetru vo vlasoch, jej hudby, jej Sveta, do jej obratnosti, do jej falší a rozkošností. Hoci bola stále inde, hoci ho stále nechávala čakať, hoci svet bol zlý a každú chvíľu v ňom videl plačúcich, poslušne sedel a čakal svoju neúnavnú Virtuózku. 
  
  
 Zbadala to Pravda a riekla si: Ah Muž, ty hlúpy, či si nevidel koľko lásky ti dávala Zakliata morská špargľa??Či si nevidel? presne tak, ako ja, ja hlúpa? Prečo si len išiel do Sveta, kde moje kompetencie dnes už neplatia? Beda ti. Nevidíš že Klobúková virtuózka má len seba a celý svet? 
  
 Muž čakal a čakal na svoje šťastie, na svoju lásku, ktorá už ale nikdy neprišla, až úplne praskla ako bublifuková bublina. 
  
 ...kdopak tu rozpozná, která je Lež, která Pravda, až budou obě dvě donaha vyslečený... ...jistě, že na světě nakonec zvítězí Pravda, ale až dokáže to, co dokáže Lež... ...někdo tě vyslíkne, řeknou ti, že ti to patří, a dřív, než se naděješ, nosí tvé kalhoty...Lež...  (Jaromír Nohavica) 
 foto: ja 

