

 Často rozmýšľam o veciach, ktoré neviem ani len definovať, nieto, aby som im rozumela. Taký  vesmír, poézia, matematika, tak tam môžem rozmýšľať do zbláznenia a výsledok?! Žiaden, ale aspoň si cibrím rozum, aj keď mi je jasné, že túto krížovku nikdy nevylúštim. 
 Nám laikom a diletantom je hej, veď to nemáme v popise práce, aby sme dosahovali výsledky.  Niekedy si poviem, funguje to aj bez vysvetlenia, tak čo. Na niektoré otázky aj tak neexistujú odpovede a možno je to tak lepšie.  Einstein,  veď hej , relativitu času prezradil, ale vraj prišiel ešte na niečo, ale to  „to" pre istotu nikomu nepovedal, lebo by sme  vraj mohli z toho zošalieť:) 
 Jedna vec ma, skutočne, veľmi zaujíma v  poézii. Ja sama som raz napísala jednu báseň, ale je taká originálna, že je nezaraditeľná a aj  nezverejniteľná.   Samozrejme je nezrozumiteľná aj pre mňa, ale ten pocit, keď si ju s hrdosťou a dojatím  čítam, mám presne taký istý ako keď som ju písala, pri plnom vedomí a triezvosti. Takže možno to aj je ozajstná báseň a som prvá, ktorá vymyslela tento spôsob písania. 
 A tu je súvislosť  matematiky a poézie, čo  ma zaujíma.  Či najprv niekto pekne poukladal  myšlienky s určitým počtom slov, spôsobom a príjemne zneli, preto  to niekto po ňom  zopakoval, a ostatní len doplňujú  slová podľa daného vzorca.  Čo iné je jamb, alebo haiku ,alebo čojaviemčo , aha, zase matematika s myšlienkou. 
 Možno by  najlepšiu báseň napísal počítač. Zadali by sme mu formu  aj obsah a vybavené. Ani by som sa nečudovala, keby to niekto skúšal , ale to by bol určite matematik s dušou poeta. Lebo základom každej básne  od jednoduchej detskej riekanky,  po zložité a umelé  tvary ,  je práve duša básnika ukrytá v básni. 
 Dobre je, keď čitatelia nájdu svoju spriaznenú básnickú dušu. Básne sú ako liek, nie sú na recept, ale keď vás bude bolieť duša vyskúšajte, nemusíte trikrát denne, ale aspoň raz začas, podľa potreby. Upozorňujem, že môžu byť aj silne návykové a niekto nemôže bez nich žiť. 
   

