
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Červeň
                                        &gt;
                Duchovné témy
                     
                 Mesiac ruženca na Luniku IX 

        
            
                                    7.10.2005
            o
            10:33
                        |
            Karma článku:
                11.31
            |
            Prečítané 
            4746-krát
                    
         
     
         
             

                 
                    Katolíci prežívajú október ako mesiac ruženca. Vo všetkých kostoloch prebiehajú ružencové pobožnosti, kde sa ľudia modlia túto rozjímavú modlitbu. My tento mesiac prežívame trošku ináč. Kedže sme na misiách, využívame evanjelizačný náboj, ktorý v sebe tajomstvá ruženca majú. Chcem o tom napísať práve dnes, lebo si pripomíname Ružencovú Pannu Máriu.
                 

                 
  
  Vďaka pápežovi Jánovi Pavlovi II. sú tajomstvá ruženca úplné. Mne osobne vždy chýbali tie, ktoré by niečo hovorili o Ježišovom pôsobení. Boli iba tajomstvá radosti o tom, ako prišiel Syn Boží na svet a jeho život do dvanástich rokov. Potom  podobne ako v biblii bola odmlka. Tá sa však v ruženci na rozdiel od biblie jeho verejným vystúpením neskončila. Odmlka pokračovala až do chvíle, keď začal trpieť. Boli tajomstvá utrpenia a končilo sa tajomstvami zmŕtvychvstania. Teraz je to už kompletné, aj s Ježišovým verejným životom. Lebo ruženec je vlastne modlitba, ktorá rozjíma nad Bibliou. Ak prebieha mesiac ruženca u nás? Od začiatku mesiaca sme si rozdelili tajomstvá tak, aby sme si dvadsať tajomstiev pripomenuli na dvadsiatich svätých omšiach. Kázeň je vždy zameraná na jedno tajomstvo, ktoré vysvetlíme a hneď po vysvetlení sa všetci spoločne pomodlíme desiatok ruženca, ktorý pozostáva z modlitby Otče náš a z desiatich modlitieb Zdravas Mária. Desať Zdravasov však nerecitujeme, ale spievame. Takýto spôsob sa nám všetkým páči viac. Na konci je modlitba Sláva Otcu a modlitba Ó Ježišu, v ktorej prosíme o odpustenie hriechov o ochranu pred pekelným ohňom a v ktorej je aj prosba o duše, ktoré najviac potrebujú Božie Milosrdenstvo. Celé sa to končí tým, že na nástenku pripevníme obrázok, ktorý symbolizuje jednotlivé tajomstvo. Tajomstvá máme rozdelené do štyroch skupín a vždy sa modlíme za jeden kontinent. Vždy si spomenieme aj na tých, ktorí nám pomáhajú a modlitbou im vyprosíme pomoc od Boha. Okrem toho každý večer sa pravidelne modlievame desiatok ruženca, kde vždy pred modlibou pripomenieme, za koho sa modlíme. Modlíme sa aj za Vás, za všetkých, ktorí nám fandíte, myslíte na nás a pomáhate nám. V noci pred spaním - okolo polnoci - sa zvyknem ešte poprechádzať po Luniku s ružencom v ruke. Modlím sa vtedy za celý svet aj za Vás. Neverili by ste, aký tu je vtedy pokoj...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Fico vymetá všetky kúty aj v osadách...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Budú Fica oblizovať aj zozadu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Napíš europoslancovi, nech neblbne...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Ukrajina potrebuje EÚ ale aj EÚ potrebuje Ukrajinu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Skončí postávanie pred kostolom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Červeň
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Červeň
            
         
        cerven.blog.sme.sk (rss)
         
                        VIP
                             
     
        Mám rád život s Bohom a 7777 ľudí. S obľubou lietam v dobrej letke, no nerád lietam v kŕdli. Som katolícky kňaz, pochádzam z dediny Klin na Orave. Farár vo farnosti Rakúsy. (pri Kežmarku)
...........................................


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1475
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3095
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            BIRMOVANCI
                        
                     
                                     
                        
                            ŠTIPENDISTI
                        
                     
                                     
                        
                            7777 MYŠLIENOK PRE NÁROČNÝCH
                        
                     
                                     
                        
                            Aké mám povolanie?
                        
                     
                                     
                        
                            Akú mám povahu?
                        
                     
                                     
                        
                            Cesta viery
                        
                     
                                     
                        
                            Duchovné témy
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Fotografie prírody
                        
                     
                                     
                        
                            Fotografie Rómov
                        
                     
                                     
                        
                            História mojej rodnej obce
                        
                     
                                     
                        
                            Kniha o Rómoch
                        
                     
                                     
                        
                            Lunikovo
                        
                     
                                     
                        
                            Mladí vo firme
                        
                     
                                     
                        
                            Náčrt histórie slov. Saleziáno
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Pastorácia Rómov
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Pokec s Bohom
                        
                     
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Sociálna práca
                        
                     
                                     
                        
                            Sociálna náuka Katolíckej cirk
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Vedeli sa obetovať
                        
                     
                                     
                        
                            Vzťah je cesta cez púšť
                        
                     
                                     
                        
                            Zábava
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štipendium pre chudobné zodpovedné dieťa
                                     
                                                                             
                                            Prečo si firmy neudržia mladých ľudí?
                                     
                                                                             
                                            Dejiny mojej dediny
                                     
                                                                             
                                            Vzťah je cesta cez púšť
                                     
                                                                             
                                            Akú mám povahu?
                                     
                                                                             
                                            7777 MYŠLIENOK PRE NÁROČNÝCH
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sociálny kódex Cirkvi - Raimondo Spiazzi
                                     
                                                                             
                                            Sociálna práca - Anna Tokárová a kolektív
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Free Download MP3 ruzenec
                                     
                                                                             
                                            MP3 ruženec
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Krištofóry - katolícky liberál
                                     
                                                                             
                                            Peter Herman - realistický idealista
                                     
                                                                             
                                            Michal Hudec sa nebojí písať otvorene
                                     
                                                                             
                                            Mária Kohutiarová - žena, matka, človek
                                     
                                                                             
                                            Branislav Sepeši - lekár o etike
                                     
                                                                             
                                            Katrína Janíková - neobyčajne obyčajná baba
                                     
                                                                             
                                            Martin Šabo - šikovný redemptorista
                                     
                                                                             
                                            Juraj Drobny - veci medzi nebom a zemou
                                     
                                                                             
                                            Peter Pecuš a jeho postrehy zo života
                                     
                                                                             
                                            Miroslav Lettrich - svet, spoločnosť, hodnoty
                                     
                                                                             
                                            Karol Hradský - témy dnešných dní
                                     
                                                                             
                                            Salezián Robo Flamík hľadá viac
                                     
                                                                             
                                            Salezián Maroš Peciar - fajn chlap
                                     
                                                                             
                                            Motorka a cesty: Martin Dinuš
                                     
                                                                             
                                            Ďuro Čižmárik a jeho mozaika maličkostí
                                     
                                                                             
                                            Paľo Medár a scientológovia
                                     
                                                                             
                                            Pavel Škoda chodí s otvorenými očami
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Modlitba breviára
                                     
                                                                             
                                            Nezabíjajte deti!
                                     
                                                                             
                                            Občiansky denník
                                     
                                                                             
                                            Dunčo hľadaj
                                     
                                                                             
                                            Správy z Cirkvi
                                     
                                                                             
                                            Prvý kresťanský portál
                                     
                                                                             
                                            Lentus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voľby nemožno vyhrať kúpenými hlasmi najbiednejších Rómov!
                     
                                                         
                       Jednoduchý rozdiel medzi kandidátmi na prezidenta.
                     
                                                         
                       Doprajme Ficovi pokoru!
                     
                                                         
                       Fico vymetá všetky kúty aj v osadách...
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                                                         
                       Vďakyvzdanie
                     
                                                         
                       Treba homofóbiu liečiť?
                     
                                                         
                       Procházka, Kňažko, Kiska... a jediný hlas
                     
                                                         
                       Napíš europoslancovi, nech neblbne...
                     
                                                         
                       Prezidentské voľby: Prečo budem voliť Procházku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




