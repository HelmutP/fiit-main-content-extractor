
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Lukáč
                                        &gt;
                Môj priateľ Skúkam
                     
                 Skúkam - príbeh rodu 

        
            
                                    22.1.2010
            o
            7:28
                        (upravené
                22.1.2010
                o
                8:13)
                        |
            Karma článku:
                17.70
            |
            Prečítané 
            3788-krát
                    
         
     
         
             

                 
                    Stál na skale a pozoroval jedľovobukové lesy pod sebou. Bolo ich veľa. Veľa to nie je desať ako prstov na rukách a ani dvadsať ako je prstov na rukách a nohách. Veľa je viac ako je prstov na všetkých ľuďoch, ktorých kedy v živote stretol. Veľa je viac ako je peria na všetkých orloch, ktoré kedy videl a šupín na pstruhoch, ktoré kedy ulovil. Toľko stromov bolo všade, kde dovidel on, Kor, neandertálec, 110 000 rokov pred tým, než sa krajina, kde sa práve nachádzal, začala volať Európou.
                 

                Zoskočil do trávy, ale nebol to práve šťastný dopad. Pravá noha sa mu zvŕtla a Kor ostal ležať na hrebeni medzi brezami. Skúšal vstať, ale nešlo to. Nebolo to bohvie čo. Bol síce august, noci boli teplé, ale zajaca, ktorého ulovil, a ktorý sa mu hompáľal za opaskom, bolo dobré na večeru trochu upiecť. V takom stave mohol o ohni len snívať. Ľahol si na chrbát a čakal až vyjdu prvé hviezdy. Kdesi nad protiľahlým svahom sa mu zdalo, že už vychádzajú. Spozornel, zaostril... neboli to hviezdy. Vo vysokej tráve žltých ľubovníkov svietili očká malého vĺčka. Kor o ňom vedel už dávnejšie. Pravdepodobne prišiel o svojich rodičov a teraz sa potuluje z dolinky do dolinky, sem tam hrdinsky uloví nejakú žabu, sem tam sa naje čučoriedok a to je asi tak z jeho letnej stravy všetko. „Zimu určite neprežije“, pomyslel si Kor. Vĺčko sa skuvíňajúc priblížil a začal okolo Kora krúžiť. Raz, dvakrát. Uprene sa pozeral nie na Kora, ale na mŕtveho zajačika, ktorý ležal vedľa neho. Keď si to Kor všimol, z neznámeho dôvodu, zo záchvatu nečakaného altruizmu, rozsekol ostrým kameňom zajaca na polovicu a jednu časť hodil ďaleko pred seba. V tráve zmizla polovica zajaca i mladý vlk. Na druhý deň sa vlk objavil opäť. Už sa odvážil bližšie a nekrúžil okolo Kora, ale si ľahol asi tri metre od neho a dosť pozorne ho sledoval. Kor to nevydržal a o chvíľu letela do trávy druhá polovica zajaca. „Aj tak mi bude zavadzať,“ pomyslel si Kor a začal sa v tráve plaziť k svojmu táboru. Nebolo to jednoduché, tábor bol vzdialený asi pol dňa cesty pre zdravého chlapa od miesta, kde si poranil nohu. Plazením, sem tam aj skackajúc o jednej nohe, tak všelijako sa za jeden a pol dňa dostal medzi svojich. Táto udalosť poznamenala Kora na celý život. Noha sa zahojila, to nebol taký veľký problém. Ostal síce krívajúci až do konca svojho života, ale dalo sa s tým žiť. Jeho život zmenil malý vlk, ktorý sa o tri dni priplazil k jeho ležovisku a začal mu v noci olizovať tvár. Zostali do konca života spolu. Vlk sirota a človek kalika. „Áno, možno to presne tak bolo,“ pomyslel som si, keď som jedného augustového večera stál, spolu so Skúkamom na „Oltar kameni“, význačnom mieste Čergovského pohoria, posvätnom kameni mnohých civilizácii. História nie je fyzika alebo matematika. Udalosti v minulosti sa nedajú jednoducho zrekonštruovať tak, ako sa dá vo fyzike vypočítať, kde stáli biliardové gule pred tým, než do nich udrelo tágo. V histórii sa dajú rozprávať len príbehy. A toto je môj príbeh. Science, pravdepodobne najvýznamnejší vedecký časopis sveta, uverejnil v roku 1997 prevratnú hypotézu, vychádzajúcu zo skúmania DNA veľkého množstva psov a vlkov. Výsledkom analýz bolo poznanie, že k domestifikačným udalostiam v prípade vlka došlo len dvakrát v histórii a tá prvá, že sa udiala pred 100 000 až 135 000 rokmi. Teda človek žije s vlkom – psom podstatne dlhšie ako sa doteraz predpokladalo. Veľa sa od tej doby udialo. Po svete behá v súčasnej dobe asi tridsať podruhov vlka a vyše tristo rôznych plemien psov. Niekoľko plemien psov vzniklo opätovným krížením vlka a psa, v snahe vrátiť psom časť pôvodných vlastnosti vlka. Skúkam je potomkom jednej takejto vývojovej vetvy, na konci ktorej stojí medzinárodne uznané plemeno československý vlčiak. Brita, Argo, Šarik a Lejdy sú mená štyroch vlkov, ktorými začína rodokmeň Skúkama a ostatných československých vlčiakov. Tieto technické fakty neboli pre mňa, na Oltar kameni, podstatné. Stál som na skale a pozoroval jedľovobukové lesy pod sebou. Bolo ich ešte veľa. Niekde. Niekde bolo jedlí len toľko, koľko mám prstov. Na jednej ruke. Osvietený oranžovým svetlom zapadajúceho slnka som myslel na státisíce podobných dvojíc človeka a psa, ktoré stáli na kopcoch v minulosti.  Myslel som na množstvo príbehov, na nekonečnú rozmanitosť života, ktorá nás dáva dokopy s našimi najbližšími priateľmi z divočiny, vlkmi. A prišlo mi celkom ľúto, že neberieme do úvahy fakt, že nie sme na tom svete sami a musíme si navzájom pomáhať. Tak, ako pomohol Skúkam mnohokrát mne a ja jemu. Úplne zbytočne, bezdôvodne, bolo len v posledných desaťročiach na Slovensku zastrelených viac ako tritisíc vlkov. Tak sme tam so Skúkamom stáli a bolo nám do plaču. Nám, potomkom vlka – siroty a človeka – kaliky.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (53)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Spáľme kostoly?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Fialová farba Veľkej Fatry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Microblog
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Veľkonočný príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Fliačik na Sibíri
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Lukáč
            
         
        jurajlukac.blog.sme.sk (rss)
         
                        VIP
                             
     
        Elektronik, ktorý sa zamiloval do divočiny a ako východoslovenský chmurnik predpovedá počasie na každý víkend
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    124
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6651
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovenská divočina
                        
                     
                                     
                        
                            Môj priateľ Skúkam
                        
                     
                                     
                        
                            Drevorubači a poľovníci
                        
                     
                                     
                        
                            Stromy
                        
                     
                                     
                        
                            Na hranici zdravého rozumu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prečo budú tento rok povodne
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Flegr: Zamrzlá evoluce
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zavýjanie vlkov v lese
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vichodňarska dzifka
                                     
                                                                             
                                            Dievča so srdcom a rozumom
                                     
                                                                             
                                            Adam Wajrak - skutočný novinár
                                     
                                                                             
                                            trochu lásky
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            John Nash - môj matematický guru
                                     
                                                                             
                                            Anton Markoš - môj bakteriálny guru
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Spáľme kostoly?
                     
                                                         
                       Fialová farba Veľkej Fatry
                     
                                                         
                       Humbug zvaný Zachráňme slovenskú pôdu.
                     
                                                         
                       Mečiarovi pohrobkovia kántria lesy.
                     
                                                         
                       Slovenský Biomasaker motorovou pílou
                     
                                                         
                       Jak on může vědet, že strana bé půjde zrovna takhle?
                     
                                                         
                       Svetový objav poslanca Smeru
                     
                                                         
                       Diletanti na bratislavskej radnici
                     
                                                         
                       Študenti, do parlamentu nechoďte!
                     
                                                         
                       Prečo Rytmus môže to, čo nemohol Mikla ?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




