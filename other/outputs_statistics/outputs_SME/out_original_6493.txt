
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            tony antoniaci
                                        &gt;
                NAOZAJTAK
                     
                 deň bez áut 

        
            
                                    23.9.2010
            o
            10:00
                        (upravené
                6.10.2012
                o
                8:45)
                        |
            Karma článku:
                11.42
            |
            Prečítané 
            2848-krát
                    
         
     
         
             

                 
                    "Bratislavu niektorí ľudia nazývajú aj Autoslavou. To, že je v hlavnom meste priveľa áut, pociťuje každý, kto chce zaparkovať."
                 

                 Pomenovanie Autoslava sa udomácnilo medzi niektorými občanmi hlavného mesta, po tom, čo videli, že námestia a chodníky sú plné áut. A tuto, odrazu, nejaký redaktor z televízie vyhlási, že je to presne naopak, že Autoslava je Autoslavou preto, že autá nemajú kde parkovať a to je najväčším problémom, nie to, že parkujú na chodníku.  Keď sa parkovanie značkou na chodníku zruší, občan dostane papuču a ešte sa aj čertí, že on by aj mal kde parkovať, ale chudák, musí platiť. A potom, že návštevy nemajú kde parkovať, hoci na druhej strane cesty sa nachádza parkovisko, ktoré zíva prázdnotou. Kto by však tak ďaleko chodil, keď predtým sa vraj bežne na chodníku parkovalo. Chodník nie je predsa od slova chodiť a výsostný priestor pre chodcov je vlastne odkladací priestor pre motoristov.    Väčšina ľudí na otázku, čo pekného si zarobené peniaze kúpia, odpovie - predsa auto. Tento sen je podporovaný reklamami, ktoré sú plné krásnych bezpečných a tichých automobilov, ktoré sa rútia cez ľudoprázdnu krajinu alebo mesto. Ale toto mesto, či krajinu je najskôr nutné zabetónovať cestami a parkoviskami.  Niekedy sa zopár iniciatívnym ľuďom zazdá, že je mestského priestoru zabetónovaného priveľa a zorganizujú tzv. park(ing) day. Akcia spočíva v premene parkovísk alebo jednotlivých parkovacích miest, samozrejme, umiestnených na ceste, na mestský mobiliár, ostrovčeky zelene a hracie plochy pre deti. Tuto, u nás, naopak, sú obyvatelia ochotní často obetovať aj samotné chodníky. Priorita áut pred všetkým, je totiž zakódovaná v našom kolektívnom podvedomí a to aj v miestach, kde nie sú prirodzeným prvkom.    Jav, kedy modernejšie a rýchlejšieho sposôby dopravy nahrádzajú staršie pomenoval kanadský dopravný inštitút ako konvenčný model dopravného plánovania. Podpora cyklistiky a chôdze predstavuje krok späť a neprisudzuje im dôležitosť. Zdržanie verejnej dopravy a bariéry pre peších nie sú chápané ako negatívum.  Včera bol deň, ktorý má na problémy v doprave v mestách upozorniť a ponúknuť alternatívy, mnohými ľudmi tradične odignorovaný, tzv. deň bez áut.   Tí, ktorým sa podarilo posunúť kamsi ďalej, než je šedý priemer, usmiať sa, objaviť chôdzu a bicykel, samozrejme vrámci možností, a majú zmysel pre poriadok, sa aj dnes mohli na chodníkoch stretnúť s množstvom áut, ktorých parkovanie je odobrené zákonom a tolerované v praxi.  Odvolávať sa vo všetkých možných prípadoch na jeden a pol metrovú dogmu je škodlivé a nabúrava ľudskú dôstojnosť napríklad aj matke s kočíkom, ktorá sa stala, citujem, čubkou, pričom jej previnenie spočívalo iba v tom, že stála na širokom chodníku a neumožnila spoluobčanovi parkovanie.   Pocit spomínanej ženy sa dá zrovnať s dojmami prírody a športuchitivého človeka, ktorý po návrate z hrádze, lesa, hôr, vidí, ako rovnako rovnako silné auto, aké ho dnes takmer zmietlo z lesnej cesty, brázdi v televíznej reklame horské doliny i čistiny.  Krútim hlavou pri spomienke na lavičku, z ktorej som sledoval vodiča jazdiaceho po trávniku, telefonát policajtovi, ktorý mi do slúchadla odvrkol, spochybnil, či viem vôbec dokázať, že pod snehom sa skutočne nachádza tráva a pohľad na lesnú čistinku odstaveným autom v reklame po príchode domov.            Ale inak je všetko v poriadku.   karikatúry: Andy Singer 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (475)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        tony antoniaci 
                                        
                                            návraty pod poľanu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        tony antoniaci 
                                        
                                            kamenné námestie živé aj bez búrania, povedzte predstavu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        tony antoniaci 
                                        
                                            nové podhradie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        tony antoniaci 
                                        
                                            nedožitá bystrá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        tony antoniaci 
                                        
                                            berlín
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: tony antoniaci
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                tony antoniaci
            
         
        antoniaci.blog.sme.sk (rss)
         
                        VIP
                             
     
          
2005

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    108
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3740
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            INÉNIEČO
                        
                     
                                     
                        
                            MINÚTAŽIVOTA
                        
                     
                                     
                        
                            KRAJINOULET
                        
                     
                                     
                        
                            NAOZAJTAK
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nález babieho leta vo Veľkej Fatre
                                     
                                                                             
                                            Nové Podhradie
                                     
                                                                             
                                            Cyklisti a chodci. Komu patrili a budú opäť patriť mestá
                                     
                                                                             
                                            Čudáci bez plynu a brzdy
                                     
                                                                             
                                            Môj priateľ Skúkam
                                     
                                                                             
                                            Grc
                                     
                                                                             
                                            Endogénna diferencia
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            karol kaliský
                                     
                                                                             
                                            ivan rias
                                     
                                                                             
                                            milan barlog
                                     
                                                                             
                                            viktor holman
                                     
                                                                             
                                            roman daniel baranek
                                     
                                                                             
                                            miroslav lisinovič
                                     
                                                                             
                                            juraj lukáč
                                     
                                                                             
                                            zora pauliniová
                                     
                                                                             
                                            tomáš slaninka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chopok práve teraz!
                                     
                                                                             
                                            Cyklokoalícia
                                     
                                                                             
                                            Lepšia doprava
                                     
                                                                             
                                            Milota Havránková
                                     
                                                                             
                                            pressburg embassy
                                     
                                                                             
                                            turistická mapa
                                     
                                                                             
                                            Vysoká pri Morave
                                     
                                                                             
                                            iMHD
                                     
                                                                             
                                            hiking
                                     
                                                                             
                                            SHMÚ
                                     
                                                                             
                                            jednoducho EVA
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




