

  
 Ak má byť manželstvo vydarené, treba sa zamilovať veľa ráz, ale vždy do tej istej osoby. 
 (Mignon McLaughlin) 
  
 Láska musí mať stále pred očami to, čím sa vyznačuje spoločný manželský život, totiž oddanosť, výlučnosť a stálosť; a musí sa o to zreteľne a úprimne usilovať. Inak to nie je láska. (Karl Barth) 
  
 Manželská láska, ktorá prežije tisícoraké útrapy, je podľa mňa ten najkrajší zázrak. (François Mauriac) 
  
 Nejestvuje ani vzťah, ani spoločenstvo, ani družná skupina, ktoré by boli príjemnejšie a príťažlivejšie, ako je dobré manželstvo. (Martin Luther) 
  
 Vstúpiť do manželstva neznamená vzdať sa slobody, ale vymeniť osobnú slobodu za spoločnú. 
 (Pavel Kosorin) 
   
  
  
  
  
  
  
  
  
   

