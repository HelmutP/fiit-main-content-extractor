
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarina Pisutova
                                        &gt;
                africký život
                     
                 Horské gorily v Ugande 

        
            
                                    14.3.2010
            o
            14:43
                        (upravené
                14.3.2010
                o
                15:01)
                        |
            Karma článku:
                9.75
            |
            Prečítané 
            2350-krát
                    
         
     
         
             

                 
                    Niekto sa ma včera opýtal, či si myslím, že hodinka s horskými gorilami stojí za to... Podľa mňa určite.
                 

                 Národné parky v horskej oblasti pri hranciach Ugandy, Rwandy a Konga sú posledné miesta, kde žijú horské gorily. Áno, presne tie horské gorily, za ochranu ktorých kedysi položila život výskumníčka Diana Fossey, o ktorej bol v osemdesiatych rokoch natočený film Gorily v hmle a ktorej hrob môžete navštíviť vo Volcanoes National Park neďaleko mesta Musanze v Rwande.   Ale, čo je oveľa zaujímavejšie, v národných parkoch v Rwande a juhozápadnej Ugande môžete v džungli, v ich vlastnom prostredí bez plotov navštíviť rodinu horských goríl. Mnohé veci v Ugande nie sú dobre ba dokonca až vôbec zorganizované, ale turizmus okolo goríl, ako jeden z najdôležitejších príjmov štátnej kasy zorganizovaný je.   Musíte si od štátnej Uganda Wildlife Authority kúpiť povolenie na tracking goríl, ktoré sa vydáva na konkrétny dátum pre konkrétnu goriliu rodinu. Maximálne osem turistov môže vidieť jednu goriliu rodinu každý deň. V daný deň sa o siedmej ráno dostavíte na presne určené východzie miesto v národnom parku, kde Vám sprievodca presne vysvetlí ako sa pri gorilách správať, čo môžete a čo nemôžete robiť (nefotiť s bleskom, dodržať bezpečnú vzdialenosť od goríl, nekŕmiť ich a samozrejme neprovokovať). Môžete si aj najať nosiča, ktorý Vám na ceste za gorilami ponesie obed a tašku a ako som mohla vidieť v praxi, aj  Vás podľa potreby vytlačí do kopca.   Sprievodca potom Vašu skupinku zavedie džungľou na miesto, kde gorilia rodinka bola predchádzajúci deň a potom spolu s ním stopujete gorily, kým ich neobjavíte. Keď sa priblížite k zvieratám, ktoré pravdepodobne prežúvajú alebo driemu (vraj strávia jedením väčšinu času, kedy nespia), učupíte sa v poraste a máte k dispozícií hodinku na pozorovanie.   Ono povolenie na návštevu goríl stálo 350 USD, od júla 2007 sa poplatok zvýšil na 500 USD. V máji 2007 nás prišli do Ugandy navštíviť manželovi rodičia z New Yorku a v tom istom čase si prišli pozrieť Ugandu a náš projekt aj môj bratranec a sesternica. Tak sme sa vybrali do národného parku Bwindi v tom čase - a ešte stále za lacnejší peniaz spolu s nimi pozrieť gorily aj my.   Naša mladšia dcéra v osemnástich mesiacoch ešte stále nebola celkom odstavená, tak sme ju nemohli na niekoľko dní nechať "doma" na ostrove. Náš manažér Friday sa ponúkol, že sa s nami vyberie do Bwindi a kým my pôjdeme gorily navštíviť bude vo východzej dedine Buhoma strážiť deti. Absolvovať trek za gorilami môžu iba osoby staršie ako 12 rokov.   Náš trek nebol dlhý, gorily sme našli už po necelých dvoch hodinách predierania sa džungľou. Hodinka strávená čupiac v poraste a čumiac na gorily bola úžasná. Gorily sú na každonenných návšetvníkov dávno dobre zvyknuté a nevenovali nám žiadnu pozornosť. Niektoré driemali, niekotré sa napchávali vetvičkami a lístím, mláďatá sa klbčili.   Videla som predtým veľa rôznych zvierat vrátane primátov v zoologických záhradách. Videla som aj zvieratá v buši na safari. Ale človek to vníma ináč, keď ho od zvieraťa nedelí bezpečný plot, či dvere auta.   Pravdu povediac, moja šetrivá dušička mala pochybnosti o tom, či 350, respektíve 500 USD stojí za hodinové čupenie v džungli v spoločnosti rôznorodého hmyzu. Neviem to popísať, ale pre mňa to za to stálo. Klbčiace sa mláďatá sa mi skotúľali takmer na nohu, ledva som uskakovala a oná hodinka mi pripadala strašne krátka a strávila som ju takmer celú s otvorenými ústami, skoro nedýchajúc.   Jediný, kto tým dňom zrejme nebol celkom nadšený bol Friday, ktorého kým sme boli v džungli moje deti vymenovali sa dostihového koňa snažili sa ho presvedčiť, že má skákať cez prekážky. Ale na skákanie sa presvedčiť nedal a dodnes tvrdí, že deň prežil vo fyzickom aj psychickom zdraví.            

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Od akého veku učiť deti pracovať s počítačmi?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Na Slovensku je skvele. Čo tam po Amerike…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            V Amerike dobre, doma najlepšie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Ako sme vianočný papier recyklovali
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Super Bowl
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarina Pisutova
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarina Pisutova
            
         
        pisutova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Komunitný projekt v Ugande (www.lakebunyonyi.net), americký manžel, dve dcéry, online vzdelávanie a chuť cestovať a objavovať nové veci...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    127
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4325
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            africký život
                        
                     
                                     
                        
                            americký život
                        
                     
                                     
                        
                            len tak kecam
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Martin Meredith - The State of Africa
                                     
                                                                             
                                            Ryszard Kapucsinski - Shadow of the Sun
                                     
                                                                             
                                            Jon Krakauer - Into The Wild
                                     
                                                                             
                                            Tim Robbins - Even Cowgirls Get the Blues
                                     
                                                                             
                                            Giles Foden - The Last King of Scotland
                                     
                                                                             
                                            Thor Hansen - The Impenetrable Forest
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sorry Bože, sorry ľudia alebo novoročné čistenie duše
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                                                         
                       Národ zlodejov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




