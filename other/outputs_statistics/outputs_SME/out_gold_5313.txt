

 Dnes v vraj v Turecku vypočúvali osem školákov vo veku 13 až 14 rokov, ktorí uniesli trojročné dievčatku. Zneužili a potom ju ťažko zranené nechali v lese, následne zomrela. 
 Nedokážem to pochopiť, prijať fakt a dobu, v ktorej sa takéto veci dejú. Pre mňa trinásť ročný chlapec alebo dievča je ešte dieťa. Nie malé dieťa, ale  dospievajúce dieťa, ktoré je už rozumné. Dieťa vo veku, keď rozoznáva dobro od zla, keď vie rozlíšiť a pochopiť následky svojich činov. Ale je to len stále dieťa, osobnosť ktorá ešte nie je zrelá. 
  Rozmýšľam nad tým, čo sa asi odohrávala v ich hlavách. Čo chceli zažiť, vyskúšať, čo ich motivovalo k takémuto brutálnemu činu. Koľko krutosti potrebuje mať niekto v sebe, aby dokázal ublížiť trojročnému dievčatku? Boli to všetko narušené osobnosti, psychopatické? Pochádzali vraj z jedného mesta ak nie mestečka. Koľko percent tej dediny a ich rovesníkov sú potom psychopati? Alebo boli takí krutí, lebo boli v skupine? 
 Možno zbytočné otázky, a možno ani tak veľmi nie. Lebo si myslím, že čoraz častejšie sa deti stávajú brutálnymi zločincami. A nemyslím si, že predošlá generácia bola rovnaká len mennej medializovaná. Myslím si, že sa niečo zmenilo. Zmenil sa svet. Agresivita, nenávisť, odmietnutie, zlo sa zakorenilo v človečine viac, než kedykoľvek predtým. 
  Myslím si, že spoločnosť zlyháva, keď nie je schopná ochrániť svoje deti, poskytnúť im ochranu pred nebezpečenstvom a krutosťou ľudského srdca. 
 Ale ak deti , ktoré vychováva sa stanú vrahmi a násilníkmi, kým sú ešte deťmi, zlyhalo všetko. Vtedy je jedno aký je HDP, štátny dlh a podobné ukazovatele. Budúcnosť národa, spoločnosti, ak chcete tak človečiny už je dávno je v troskách. V ruinách a na ceste, ktorá ma jasný koniec. Záhubu. 
  Nie je to apokalyptické videnie sveta. Len obyčajným zamyslením sa, nad svetom, ktorý ma obklopuje, ohrozuje. 

