
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrej Grznár
                                        &gt;
                fotočlánky
                     
                 Alkoholici 

        
            
                                    26.3.2010
            o
            9:00
                        (upravené
                26.3.2010
                o
                1:50)
                        |
            Karma článku:
                10.43
            |
            Prečítané 
            2020-krát
                    
         
     
         
             

                 
                    Mám rád cestu vlakom. Naozaj. Nie preto, že WéCka smrdia často tak, že ich až cítiť do kupéčka cez zatvorené dvere či kvôli tým  umytým oknám cez ktoré uvidíte akurát tak h... hrozné nič... Ale hlavne kvôli  ľudom a prostrediu naokolo...
                 

                 A  aby ste cvičili aj svoje očné zmysly, tak pripájam aj zopár fotiek,  ktoré som cvakol na poslednej ceste vlakom :)      B R A T I S L A V A - niekto to tam "krásne" vyškrabal rovno do okna :) Obrys starčeka prechádzajúceho cez slnečný lúč zapadjúceho slnka vo vagóne popísanom grafiti. Fotku som si nazval "Bratislavské peklo". Ten nápis a atmosféa vo vnútri kupéčka hovoria za všetko :)   V deň, ktorý cirkev nazýva raz do roka zeleným, teda vo štvrtok, som tak ako každý týždeň cestoval domov zo školy. Išiel som zrýchleným spojom podvečer domov, pretože ten hore nám nadelil opäť fajnové tridsiatky, a tak podvečer sa mi cestovalo lepšie. Ujo revízor ma vždy vyhreší, keď ostanem stáť v strede vlaku, a tak poslušne nastupujem do prvého vozňa s nádejou, že snáď bude nejaké to miestečko voľné. A bolo. Keby len jedno ! Celé kupéčko voľné. Jak fajn ! hovorím si v duchu.         Po pár minútach ako som sa usadil však prichádzajú aj tí, ktorí nedali na radu uja revízora a tak hľadajú miesto vpredu.  Periférne si všímam, že niekto ide okolo, ale nevenujem tomu moc veľkú pozornosť a nasadené sluchátka nechávam v ušiach nech vyhrávajú hudbu ďalej. V tom sa otvoria dvere a usmiaty pánko na mňa mávne.   Prepáčte mladý pán, je tu voľné ? slušne sa spýtal s neustálym úsmevom na tvári.   Ale samozrejme, nech sa páči. Odpovedal som slušne aj ja. Veď prečo aj nie, v celom kupé som sedel len ja...   V tom sa pozrel na pána za ním, na svoje tri fľaškové pivká, ktoré držal v ruke a dodáva s nadšením: Ale my sme alkoholici, nevadí ?   Nechcel som byť neslušný, ale proste som sa musel smiať :D   Kľudne si sadnite, dodávam s veľkým úsmevom na tvári. Vyzerali slušne, a na alkoholikov teda nejako extra nevyzerali. Jedine tie tri fľaškové pivká, ktoré držali v ruke ich prezrádzali. V kľude sa usadili, pripravili pivká a štrngli. Prvý spomínaný pán však neodpil.   Prepáčte, že sme takí nevychovaní, nedáte si ? Mám tu ešte ďalšie dve.    Nie, ďakujem pekne. Odmietam s ďalším prekvapeným úsmevom na tvári.   Po chvíľke, ako si odpijú z fľašky a prehodia pár slov ešte o borovičke, ktorú si dajú potom vonku v bare, začne ich nekonečná konverzácia. A nie ledajaká. Štvortaktové motory. Technológie, ako, kde a kto čítal všetko v novinách a videl v televízii. Inteligentný rozhovor. Žiadne neogabané rozhovory plné sprostých slov. Aspoň tak som je to zväčša, keď počujem podnapitých ľudí. Odborné názvy sa len tak sypali !   Dlho so mnou necestovali. Hneď na ďalšej zastávke vystupovali. Ešte pár pekných slov ako sa tešia na tu borovičku v staničnom bare, pripomienka, že pivo je fakt fajn a že by som ho často nemal odmietať, a už aj boli fuč.   Hlavou mi tak prebehla myšlienka, koľko ľudí chodí dnes po svete oblečených v tých najlepších „sáčkoch“ a tvária sa, že zožrali všetku múdrosť sveta. Ale keď príde rad na nich, tak pravdu o sebe Vám medzi tými dverami na kupéčku nikdy nepovedia...      Slnko niekedy dokáže odohrať naozaj veľké  divadlo kdekoľvek :)        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Grznár 
                                        
                                            Praha a Konstanz - Street foto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Grznár 
                                        
                                            Aspoň na pol dňa.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Grznár 
                                        
                                            Chcem predĺžiť čas, pretože dva dni je málo.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Grznár 
                                        
                                            Andy in wonderland
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Grznár 
                                        
                                            Všetci ma tu volajú Merci !
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrej Grznár
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrej Grznár
            
         
        grznar.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1151
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            fotočlánky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dale Carnegie
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Krasne fotocky :)
                                     
                                                                             
                                            Zaujimave Foto
                                     
                                                                             
                                            Skvele pisuci ucitel :)
                                     
                                                                             
                                            Lukas Zelenik
                                     
                                                                             
                                            Zajacek
                                     
                                                                             
                                            Jeden z tych THE BEST...
                                     
                                                                             
                                            Vtipne foto a super článočky :)
                                     
                                                                             
                                            Velmo pekne píše...
                                     
                                                                             
                                            _intony antoniaci - super foto
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chcete dať svoje deti k skautom? Prečítajte si najprv toto.
                     
                                                         
                       Oplatí sa platiť odvody do sociálnej poisťovne?
                     
                                                         
                       Otrasné - Speváčka skupiny „Pussy Riot“ Nadežda Tolokonniková...
                     
                                                         
                       Môj sused Tibi
                     
                                                         
                       Krátko o dôležitosti hygieny v letných mesiacoch
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Bol som v Bille
                     
                                                         
                       Fico naznačil, že je idiot
                     
                                                         
                       Ministerka nemá problém s kotolníkom, ale s redaktorkou Markízy
                     
                                                         
                       Nie vy, ale ja som štát!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




