

 Premiér Mikuláš Dzurinda bol jediným hosťom politickej diskusnej relácie O päť minút dvanásť na STV, hoci doteraz boli hosťami v relácii vždy štyria politici, ktorým otázky kládol jeden moderátor. 
 "Nemáme  informácie, či ide o nejakú formu exkluzivity pre premiéra, ale je to otázka na STV, prečo zvolila takýto nezvyčajný formát," povedal Michal Dyttert, hovorca predsedu parlamentu Pavla Hrušovského. Hrušovský ponuku na samostatnú reláciu od STV nedostal. 
 Béla Bugár, predseda SMK povedal, že reláciu pozeral a "aj som sa čudoval, že je tam sám". Nemyslí si,  že bola vhodná, "ale je to na STV a premiérovi, ako sa rozhodli". Bugár si ešte pamätá obdobia, keď mal premiér pravidelne svoju reláciu vo verejnoprávnej televízii (počas vlády Vladimíra Mečiara mala STV reláciu "Ako ďalej, pán premiér?" pozn. red.). 
 Podľa šéfredaktora spravodajstva a jedného z dvoch moderátorov Ivana Jandu to bol  zámer STV, pretože "na konci roka sme chceli mať ako hosťa premiéra a chceli sme mať na neho viac času". Samotný premiér si podľa Jandu nekládol takú podmienku, aby bol sám. 
 Premiéra pozývala na nedeľu do svojej politickej diskusnej relácie Sedmička aj televízia JOJ. "Dzurinda odmietol pozvanie s tým, že nie je vhodný čas na to, aby chodil do  diskusií, kde je viac politikov, kde sa hádajú a robia divadlo pre ľudí," povedal Aleš Krátký, dramaturg relácie. Premiér podľa neho dodal, že vo volebnom roku si vraj rád sadne aj so svojimi politickými rivalmi, ale teraz v tom nevidí zmysel. 
 Daniel Krajcer, moderátor relácie SITO na Markíze, povedal "že premiér už rok a pol odmieta pozvanie do relácie". Na  dôvody takéhoto konania sa podľa neho treba spýtať Dzurindu. 
 Politická strana Smer vydala vyhlásenie, v ktorom kritizuje STV za to, že poskytla exkluzívny priestor premiérovi a upozornila na fakt, že "premiérovi kládla otázky manželka iného predstaviteľa SDKÚ". Moderátorka Ľuba Oravová je manželkou Branislava Oravu, bývalého poslanca SDKÚ. Ten  sa však členstva v strane vzdal potom, ako začal pracovať pre spoločnosť, ktorá má na starosti investíciu Kia. 
 Členom vedenia STV je Branislav Zahradník, ktorý je aj poslancom miestneho zastupiteľstva za SDKÚ. "Pozvanie premiéra je rozhodnutie spravodajstva a nie je prvýkrát, keď sme mali len jedného hosťa," povedal Zahradník. Podobnú exkluzivitu mali  doteraz prezidenti Rudolf Schuster a Ivan Gašparovič. 
 Zahradník povedal, že aj v budúcnosti bude hosťom relácie iba jeden politik a "ak bude premiérom Robert Fico, pozveme samého aj jeho". 
 Moderátorka Oravová robí podľa Zahradníka svoju prácu profesionálne, podobne ako moderátorka TA 3 Beáta Oravcová, ktorá je priateľkou ministra spravodlivosti Daniela  Lipšica.	 (rk) 
   

