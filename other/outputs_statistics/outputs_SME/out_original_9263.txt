
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Menyhertova
                                        &gt;
                Nezaradené
                     
                 Semafóry, nie su fóry! 

        
            
                                    20.6.2010
            o
            15:48
                        (upravené
                20.6.2010
                o
                16:34)
                        |
            Karma článku:
                3.99
            |
            Prečítané 
            755-krát
                    
         
     
         
             

                 
                    "Je červená, nevidíš?"  "No a ? Niejde žiadne auto, tak čo!" Hm, dialóg  na zamyslenie. Na čo potom je ten semafór?
                 

                  Už od škôlkárskych čias sa učíme, pravidlám cestnej premávky. V zmysle, ako prechádzať cez zebru. "Prechádzaj len na zelenú, obzri sa v ľavo, v pravo a čo najrýchlejšie prejdi." hovorievala moja mamina. Nič ťažké pre normálneho človeka. Prax bohužiaľ ukazuje, že mnohí ľudia si z prechodov cez cestu urobili adrenalínový šport. V našom meste je tiež pár takých prechodov, ktoré považujem za vyslovené rizikové. Veľká dvojprúdovka v špičke na prasknutie, vodiči sú nervózni. Chodci sa bezhlavo vrhajú cez cestu, nedívajúc sa ani, či nieje nablízku nejaké vozidlo: "Veď mám predsa prednosť, som chodec!" Áno, ale dokedy!   Je červená. Cez cestu prechádza starší pán a za ním mladé dievča len tak dobehuje. Autá trúbia, jedno prudko zabrzdí, a šofér vypľuvne pár nevyberaných hneď na začiatok. Na druhej strane ich už čakajú policajti  odetí v reflexných vestách. Keby sa lepšie dívali, určite by videli aspoň ich. Dedko sa rozčúlene domáha svojích práv: "Chrapúň akýsi, skoro ma zrazil tuto, videli ste to ? Aj číslo ŠPZ som si zapamätal aby ste ho mohli pokutovať!" hromžil starký.   "Strýco, na červenú ste prechádzali, možete byť vďačný Bohu, že vás ten dotyčný chrapúň neztrazil"   " Ale ja som chodec, mám prednosť", ohradil sa starký. "Áno, ale na semafóre je červená. A tá platí pre každého a bez diskusie. Ešte aj pokutu vám kľudne môžeme dať.   " Joj, prepáčte...."   Prepáčili.   Nie som motorizovaný občan. Viem ale, že ak nebudeme my chodci doržiavať pravilá tak nehodovosť na cestách bude ešte horšia ako doteraz. Tých pár sekúnd, ktorých trvá presvedčiť sa či nesvieti na semafóre červená, či nieje na vozovke auto, mi za moj život stojí. Apoň chcem mať istotu, že som pre bezpečný prechod cez cestu urobila zo svojej strany všetko, čo som mala.  Bežne som svedkom aj toho, že vodiči na cestách sú bezohľadní, najmä tí vo veľkých nablízkaných autách, ale to už je na iný príbeh....................................... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Mužolapka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Rytmus a jeho kecy....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Závisť, alebo prehnané ego?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O  cintoríne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O delení ministerstiev...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Menyhertova
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Menyhertova
            
         
        menyhertova.blog.sme.sk (rss)
         
                                     
     
        som človek s chybami.....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    971
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Výnimočná
                                     
                                                                             
                                            Timea Keresztényiová :Sama mama
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lara Fabian
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




