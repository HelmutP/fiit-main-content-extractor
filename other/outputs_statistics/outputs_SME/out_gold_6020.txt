

 Po nebi sa túla bezstarostným letom, nad stromami krúži, letí šírym svetom. Vietor sa s ním nežne, opatrne hrajká, teší sa vždy z letu papierový šarkan. 
 Nevídanú voľnosť jeho kroky majú, vtáky sa s ním zavše v pretek vydávajú. Radosť má z tej milej, vtáčej spoločnosti, však dnes záujem nemá o oblačný dostih. 
 Dnes sa nechce nikam nervózne ponáhľať, miesto toho s vetrom na nebi vystrájať. Nechať sa unášať jeho dychom bystrým, kristiánku kresliť medzi padajúcim lístím. 
 Vychutnávať si chce túto voľnosť sladkú, vie že taká chvíľa máva nohu vratkú.  Užíva si teda vzácnu vetra priazeň, pokiaľ príbeh skončí a pristane na zem. 
 Ani zem však jeho pokoj nezahasí, ostane v ňom navždy spomienka na let krásny. Zvláštny pocit blaha do okolia rozdáva, nech sa celý náš svet radostnejším stáva. 
   

