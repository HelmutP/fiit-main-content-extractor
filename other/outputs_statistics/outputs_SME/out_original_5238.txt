
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Krajmerová
                                        &gt;
                Súkromné
                     
                 Aký bol testovací deň na EXPO Šanghai  2010? 

        
            
                                    25.4.2010
            o
            19:55
                        (upravené
                25.4.2010
                o
                20:12)
                        |
            Karma článku:
                4.19
            |
            Prečítané 
            727-krát
                    
         
     
         
             

                 
                    Hoci sa EXPO brány  v čínskom  Šanghaji oficiálne otvoria až 1. mája, testovací deň na tejto svetovej výstave sa konal už v poslednú aprílovú sobotu. Čo vôbec naši organizátori neočakávali - bolo  neuveriteľné množstvo ľudí. Číňania vzali náš pavilón doslova útokom -  a nič ich nezastavilo. Dav vyrazil dvere a vrútil sa dovnútra slovenskej expozície. Slováci museli privolať čínsku ochranku, aby 20 tisíc návštevníkov nášho pavilónu  ako-tak spacifikovali. O čo mali Číňania najväčší záujem?
                 

                 
  
   Podľa slov generálnej komisárky slovenskej expozície Ivany Magátovej, ktorá so svojím tímom spolupracovníkov ani vo sne neočakávala takúto návštevnosť - Číňanov veru slovenský pavilón zaujal. „Mali sme otvorené od 10. hod. do 20. hod. večer. Záujem bol najmä o historické kóje, kde sa čínski návštevníci s nadšením fotili. Obdivovali krátky film o našej krajine, prírode a najmä slovenských mestách. Mnohí sa chceli fotografovať aj  s našim personálom, lebo Európania sú pre nich zrejme takí exotickí, ako Číňania pre nás. Tiež ich bavila naša fontána, kde mohli hádzať mince a želať si naplnenie svojich snov a túžob. Stručne povediac, tešili sa úplne všetkému," povedala I. Magátová.   Stala sa však i taká vec, že dvaja návštevníci  si zabudli v slovenskom  pavilóne tašky. Boli z toho doslova nešťastní - ale ich tašky sa pravdaže našli. Všimol si ich jeden zo slovenského personálu a odložil ich. Keď zábudlivci zistili, že sa  ich veci našli, tak náš personál  objímali, hladkali, podávali všetkým zamestnancom pavilónu ruky a ďakovali im. Samozrejme,  boli tam aj privolaní policajti, ktorí skontrolovali,  čo sú to tašky Číňanov, ktorí si ich v našom pavilóne zabudli.   Resumé   Svetová výstava EXPO v Šanghaji sa začne 1. mája a bude trvať 6 mesiacov, t.j. do 31. októbra 2010. Očakáva sa  90 mil. návštevníkov a viac - ale veď iba Čína má viac ako 1,3 miliardy obyvateľov. Keby do slovenského pavilónu malo prísť každý deň spomínaných 20 tisíc ľudí, tak by to bola obrovská propagácia našej krajiny - veď by náš pavilón  mohlo navštíviť až 3,6 milióna návštevníkov! Celému organizačnému štábu želáme do Šanghaja veľa elánu a trpezlivosti.                             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Voniaš ako...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Týrané Slovenky, nedajte sa !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Spod plesovej krinolíny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Prírodný detox ľudského organizmu zvládne každý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Rok čínskeho draka má optimistický charakter
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Krajmerová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Krajmerová
            
         
        krajmerova.blog.sme.sk (rss)
         
                                     
     
        Autorka v júli 2012 zomrela. Blog nechávam na jej pamiatku. Dcéra Katka
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    235
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2101
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voniaš ako...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




