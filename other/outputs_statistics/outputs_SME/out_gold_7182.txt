

 S dokumentami odhaľujúcimi tento nevydarený obchod prišel včera britský denník The Guardian. 
 Juhoafrické tajné dokumenty zverejnené v knihe amerického akademika Sasha Polakow-Suranskom podávajú dôkaz o izraelskom jadrovom programe, ktorý nieje doteraz Izraelom oficiálne potvrdený. 
 Podľa neho 31. marca 1975 premiér JAR Pieter Willem Botha požiadal izraelského premiéra Šimona Peresa o poskytnutie  rakiet Jericho. Ten mu na žiadosť odpovedal ponukou zbraní v troch „veľkostiach", ktoré Polakow-Suransky pokladá za konvenčné, chemické a jadrové. 
 Vtedajšia JAR s rasistickým režimom chcela vlastniť jadrové zbrane ako odstrašujúci prostriedok a potenciálnu zbraň voči susedným štátom. Obchod nakoniec stroskotal na nedostatku penazí . 
 Dokumenty potvrdrzujú skoršie vyjadrenia bývalého námorného veliteľa JAR Dietra Gerhardta, ktorý tvrdil že medzi JAR a Izraelom bola dohoda, zahŕňajúca poskytnutie rakiet Jericho so „špeciálnymi hlavicami". Tie označil Gerhardt ako jadrové hlavice. 
 Peresová hovorkyňa uviedla, že správa je nepodložená dôkazmi a nikdy nepriebiehlo toto rokovanie. Pravosť dokumentov nekomentovala. 
   
 Zdroj: 
 http://www.guardian.co.uk/world/2010/may/23/israel-south-africa-nuclear-weapons 
   
   

