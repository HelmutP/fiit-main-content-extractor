

 Súčasťou výskumu je interview (telefonicky, cez Skype alebo osobne, ak to bude možné) s vybranými firmami.  Interview otázky sa budu "točiť" okolo týchto tém: 
 
 Dôvody, prečo firma      využíva Enterprise2.0  
 Aké nástroje a aplikácie využíva a      ktoré staré spôsoby tým nahradila 
 Aký vplyv malo zavedenie      týchto nástrojov do praxe na organizačnú štruktúru 
 Ako sa tým zmenila kultúra      a spôsob práce vo firme 
 Aké ťažskosti boli pri      používaní týchto nástrojov? (napr. legálne      bariéry)
 
 Aké ste mali náklady      spojené s implementáciou nových technológií
 
 Aké boli tieto náklady v      porovnaní s tradičným IT projektom
 
 Aké sú podľa vás výhody      používania web2.0 technológií
 
 Aké sú plány do budúcna s      ďalším využívaním Enterprise2.0 nástrojov
 
 
 Podmienkou na výber medzi prípadové štúdie, je že minimálne 20 zamestnancov firmy sa interne podieľalo na Enterprise2.0 projektoch. Interview bude trvať cca 90 minút a nasledovať bude pravdepodne ešte dodatočná emailová komunikácia s doplňujúcimi otázkami. 
 Blog dedikovaný projektu nájdete tu, všimnite si linky na pravej strane s vybranými blogmi týkajúcich sa Enterprise2.0. 
 Tak ako poznáte slovenské Firmy2.0? Robíte v takej? Vlastníte takú? 
 Dajte mi prosím vedieť emailom na senda5 (at) yahoo.com 
 Ďakovala. 
 P.S.: Ako bonus prikladám link na 10 najsledovanejších ľudí v Enterprise2.0 svete. 
   

