

 Nemám na neho veľa spomienok. Zomrel, keď som bola ešte malá. O dedkových salónkach som už písala tu. 
 Raz si pomýlil strúčiky cesnaku so strúčikmi gladiol. Očistil a poriadne pošúchal po poriadne vysmažených hriankach. Neskôr mu prišlo zle, ale do nemocnice nešiel. Nám to stručne okomentoval: "Ta kukam do žveredla, oči vypuľenje jak žaba!" Prežil. Poučil sa a už si cesnak vešal priamo ku klobáskam a slanine. 
 Vždy večer, keď nás mama uspávala, šeptala si popod nos nejake slová... Veľmi sa mi to páčilo. Raz som sa jej na to opýtala. Dozvedela som sa, že tie isté slová si šeptal pri posteli aj dedo. Vždy po každom dni, keď sa vrátil z práce a z roboty okolo statku si kľakol k posteli a nasledoval prúd slov. Mama sa ich naučila, hoci im nerozumela:  
 Amen, amen, daj to Pane, naj še nám to šicko stane my še Tebe poručame, i s pracu i s celom i s dušičku, čo ju choľem mame. Nemame komu, lem Tebe Pane Ježišu Krisce, jak Otcu laskavomu, aby ši nas račil zachovac, zavarovac od moru, od vetra, od hladu, od vojny, od drahoty, od šlepoty, od šickej nahlej i nespodzievanej šmerci našej. Amen. 
 Dedo bol baníkom. A tak sa modlil aj k svojej patrónke. 
 Barboričko svata, mučiteľničko božska, kto tebe verne služi, bez tebe zomierac nemože. Daj to Pane Bože, žeby sme bez tebe nezomierali, svaty sakrament prijimali a pri našej duši net žadnej pokuty... lem tota jedna ružička prekvitaca od Pana Boha vydaca. 
 Kaplan Boha pestuje, moja dušička še raduje. V nedzeľu rano slunko vychodzi Panna Maria synačka za ručku vodzi. Odvedla ho na kazanec, z kazanca na rozpor a Ty Peter vež Ty kľuče, idz opatric tote hriešne duše. Lem totu jednu ne - co še s otcom i s maceru bila, kervavila. Ja še nebila nekervavila. Povodz ju po kamienču, po bodľaču.Kto še budze totu modlitbičku modlic, v piatok do pošnika, v sobotu do frištika, v nedzeľu do ranšej omši budu mu rajske vrata otvorene a pekelne zarmucene. 
  Až časom si uvedomujem, aké dedičstvo vlastním... 

