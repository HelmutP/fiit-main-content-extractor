
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Trnovcová
                                        &gt;
                Verejné
                     
                 Citáty5 

        
            
                                    4.5.2010
            o
            15:17
                        (upravené
                4.5.2010
                o
                17:22)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            286-krát
                    
         
     
         
             

                 
                    
                 

                     Humanizmus   ,,Začiatok našich vedomostí sa spustil vtedy, keď si ľudia uvedomili, že dôkazy o našom stvorení bohom alebo bohmi sú nedostačujúce a uvedomením si, že my sme zodpovední sčasti za náš vlastný osud. Ľudské bytosti môžu docieliť dobrý život, ale cesta k tomu je pestovanie hlbokej inteligencie a odvahy- nie slepej viery a poslušnosti- a toto pravdepodobne raz aj dokážeme.“ Paul Kurtz   ,,Vravíte, že ešte stále potrebujeme náboženstvo? Potom ja na to odpovedám, že práca, zdravie, štúdium a láska tvoria náboženstvo... Väčšina náboženstviev pokladá lásku muža k žene a ženy k mužovi za niečo zlého... Vravia, že chorobu na nás zosiela boh. Teraz to všetko odmietame a tvrdíme, že to čo vám poskytne všetko dobré je: zdravie, práca, štúdium a láska.“ Elbert Hubbard   ,,Ak chcem definovať v krátkosti humanizmus 20. storočia, povedal by som, že je to radostná služba v záujme zvýšeného dobra celého ľudstva v tomto prirodzenom svete a zastávajúca metódu rozumu, vedy a demokracie.“ Corliss Lamont   ,,Najstrašnejšia zbraň proti omylom každého druhu je rozum. Nikdy som nič iného nepoužíval, a dúfam, že ani nepoužijem.“ Thomas Paine   ,,Inteligencia vedená láskavosťou je tá najvyššia múdrosť.“ Robert Ingersoll   ,,Nie je dostatok lásky a dobroty na svete na to, aby nám to dovolilo prenášať tieto city na nejakú imaginárnu osobnosť.“ Friedrich Nietzsche   ,,Neexistuje žiaden dôkaz, že by boh zasahoval do záležitostí človeka. Ruky vztýčené k nebu sú zbytočné. Z oblakov nám nikdy nepríde žiadna pommoc.“ Robert Ingresoll   ,,Ak sa má podvod odstrániť, musia to urobiť ľudia. Ak sa majú oslobodiť otroci, ľudia ich musia oslobodiť. Ak sa majú objaviť nové pravdy, ľudia ich musia objavovať. Ak chceme, aby nahí boli oblečení, aby sa hladní najedli, aby sa uskutočňovala spravodlivosť, aby sa dostávala odmena za prácu, aby sa povery vynali z mysli, aby sa bezbranným dostalo ochrany, aby právo napokon triumfovalo- to všetko musia dosiahnuť iba ľudia.“ R. Ingresoll   ,,Rovnako by sme mohli žiadať, aby niekto nosil šaty, ktoré boli preňho ušité, keď bol ešte malý- ako by sme mali trvať na tom, že civilizovaná spoločnosť má navždy ostať pod režimom, ktorí vytvorili naši barbarskí predkovia.“ Thomas Jefferson   ,,Človek nesmie posudzovať rozum podľa meradiel tradície, ale naopak musí posudzoavať tradície podľa meradiel rozumu.“ Lev Tolstoj   ,,Keď som prišiel k presvedčeniu, že vesmír je prirodzený- že všetci duchovia a bohovia sú iba mýtus, vstúpila do mojej mysli radosť zo slobody...bol som slobodný- mohol som slobodne myslieť, vyjadrovať svoje myšlienky...slobodný, aby som mohol žiť pre seba a tých, čo som miloval, slobodný na to, aby som skúmal, aby som si mohol vytvárať domnienky, sny a nádeje...slobodný na to, aby som mohol zavrhnúť všetky ignorantské a kruté vierouky, všetky ,,inšpirované“ Písma, ktoré vytvorili divosi...oslobodený od pápežov a kňazov...oslobodený od posvätných omylov a lží...od strachu z večného mučenia...od čertov, duchov a bohov.Nebolo už viac zakázaných miest v oblasti myslenia...prestalo kráčanie po stopách iných...nebolo už treba sa klaňať, podlizovať sa, plaziť sa alebo vyjadrovať lživé slová.“ Robert Ingresoll   ,,Hodnoty, vedy a demokracie vzájomne súvisia, často sú nerozlíšiteľné. Veda a demokracia vznikli- v ich občianskom vtelení- v rovnakom čase aj v rovnakých miestach. Grécko v 7. a 6. storočí p.n.l. Veda naďalej prekvitá a v skutočnosti vyžaduje slobodnú výmenu myšlienok; jej hodnoty sú v rozpore s utajovaním. Veda si neosobuje špeciálne výhodné postavenie a nenárokuje si žiadne privilégiá. Rovnako demokracia ako veda povzbudzujú k predkladaniu nekonvenčných názorov a k živej debate. Obe požadujú rovnako zvažovanie, súvislé dôkazy, prísne štandardy, príčinnosti a počestnosť.“ Carl Sagan     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            La Maladie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            L´amour
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            La Solitude c´est la mort
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            Anketa roka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            Citáty4
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Trnovcová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Trnovcová
            
         
        trnovcova.blog.sme.sk (rss)
         
                                     
     
        Študentka, čo chce vyjadriť svoj názor.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    467
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Verejné
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




