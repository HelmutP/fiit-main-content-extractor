

 O tom, že Smeru natieklo do topánok a Fico evidentne znervóznel niet pochýb. Ako sa ale dalo predpokladať, Fico svojou charizmou a rečníckymi konšpiráciami dokáže prekryť takmer každý škandál svojej strany. Jeho voliči mu samozrejme uveria a neriešia... 
 Navyše má Fico zase raz šťastie. V podstate ho má celých 10 rokov svojej existencie: 
 - profitovalo z nesúhlasu ľudí s niektorými tvrdými reformami vlád z rokov 1998-2006 - dostal do rúk rozbehnutú ekonomiku plnú investorov  - zožal úspech pri prechode na Euro a začlenení sa do Schengenu - všetky zlé ekonomické ukazovatele zvalí na finančnú krízu 
 a v súčasnosti, kedy vychádza na povrch poriadny škandál, príde s "geniálnym" zákonom maďarská vláda 
 Fico tak môže zabiť dve muchy jednou ranou, dokonca až tri muchy: 
 1. odkloní pozornosť od spomínaného škandálu 
 2. odstaví SNS, prebere jej voličov tým, že jej prebral jedinú tému, čo SNS má  
 3. môže zvýrazniť boj proti možnej účasti SMK vo vláde 
 etc.  
 Práve posledný bod je hlavnou myšlienkou tohto blogu. 
 Smer vždy vyhrával na základe útokov voči iným stranám, iba vždy menil obsah útoku. 
 V roku 2006 to boli "neľudské" reformy, škandály vtedajšej vlády (kupovanie poslancov atď.). Neskôr sa postupne prechádzalo na maďarskú kartu. 
 Fico už teraz nemôže poukazovať na škandály SDKÚ, KDH a podobne, pretože je sám namočený v oveľa hlbších bažinách.  Síce jeho konšpirácie (ako aj tá včerajšia) stále zaberajú na jeho pevné voličské jadro, opozičné strany majú množstvo protiargumentov. 
 Strana Smer nikdy nemala a podľa mňa ani nebude mať jasné ideologické vymedzenie a programovú víziu. To populistické strany nemusia. Im stačí hrať na aktuálnu tému.  Preto podobných billboardov s témou maďarskej karty bude pred voľbami pribúdať. Takisto sa bude zväčšovať aj intenzita osočovania, obviňovania a kreácie konšpiratívnych teórií. 
 Agresivita bude gradovať. 
 Gradovať bude, pretože Smeru chýba kreativita. Jej jedinou podobou sú Ficove konšpiračné teórie, ktoré dokáže stvoriť takmer zo všetkého (klobúk dole..). 
 Bohužiaľ, tým vyše 30 percent voličov stačí aj tak málo... 

