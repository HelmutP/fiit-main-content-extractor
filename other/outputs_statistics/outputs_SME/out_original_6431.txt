
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marie Stracenská
                                        &gt;
                o kadečom
                     
                 Floskuly, prefláknuté výzvy a sľuby 

        
            
                                    13.5.2010
            o
            13:41
                        |
            Karma článku:
                5.16
            |
            Prečítané 
            784-krát
                    
         
     
         
             

                 
                    Bezplatné! Pre všetkých! Pre vás! Pre nás! Mať sa lepšie! Šťastná budúcnosť! Zmena! Kontinuita! Dokázali sme veľa! Ešte len dokážeme! Ukážeme, že to funguje! Toto nechceme a tamto chceme! Ste pre nás dôležití! Vyberte si nás! Sľubujeme...!  
                 

                 Nedá sa tomu vyhnúť. A lezie mi to na nervy. Som občianka, je pred voľbami, som rada, že mám zas po čase právo na to, prejaviť aj ja svoj postoj, k tomu, ako to tu je. Ale - úprimne - bez toho predvolebného humbuku by som sa zaobišla.   Pred týždňom som sa hrala s priateľmi hru. Tvárili sme sa ako predstavitelia vymyslených politických strán, s programami fest pritiahnutými za vlasy. A snažili sme sa získať okolie a presvedčiť ho (aj seba), že sme tí lepší. Argumentovali sme, deklamovali, diskutovali, vykrúcali sa, prihovárali. Tí kamaráti mali okolo dvadsiatky a ich reálna skúsenosť s politikou je nulová. Hra mala pocvičiť ich schopnosť argumentovať, vystupovať pred ostatnými, bojovať s trémou a neistotou a iba trochu sa aj zamyslieť nad tým, v čom žijeme. Keď som ich tak počúvala, stačilo len privrieť oči a ani jeden z nich, totálnych laikov, nemal ďaleko od skutočných politikov. Tých politikov, ktorí nás na brífingoch a tlačových besedách presviedčajú o svojej pravde. Hovorili slová z plagátov a mítingov - a ani sa nemuseli veľmi namáhať. Stále sa opakujúce sa floskuly, výzvy, apely, neskutočné politické klišé - máme ich okukané všetci. U kamarátov som kvitovala, ako sa v pomerne neľahkej situácii vynašli. Či by som pochválila politikov? To si taká istá nie som. Slová ako cez kopírák, naťahovačky a prázdne spory, zneužívanie a využívanie, hra na city aj na nadradených... Nejako mi čas pred týmito voľbami nejde po chuti. Príliš veľa ma toho neoslovuje. Prepáčte.       Ešte pár týždňov, vravím si. Ešte pár týždňov tvárí a postáv na bilbordoch s rozprávaním o tom, ako bude lepšie. A potom ich prekryjú. Hm. Nahradia ich iné výzvy. Na lacnejšie, rýchlejšie, voňavejšie, čistejšie, väčšie, zaujímavejšie... Ani to nebude úplná výhra. Len, tie druhé sľuby budú aspoň trochu hmatateľné. A ich overenie v praxi nemá spravidla také dlhodobé dôsledky. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Vianoce vo štvrtok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Nepoznám žiadne dieťa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Ranná káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Cez hrádzu zrýchlene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Poschodová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marie Stracenská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marie Stracenská
            
         
        stracenska.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som žena. Novinárka, lektorka a konzultantka. Mama nádherných a šikovných dvojčiat. Manželka, dcéra, sestra, priateľka. Neľahostajná. Mám rada svet. Rovnako rýchlo sa viem nadchnúť, potešiť a rozosmiať ako pobúriť a nahlas rozhnevať. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    701
                
                
                    Celková karma
                    
                                                12.18
                    
                
                
                    Priemerná čítanosť
                    1779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o deťoch
                        
                     
                                     
                        
                            o vzťahoch
                        
                     
                                     
                        
                            o kadečom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Moja rozprávka
                     
                                                         
                       Zaťovia
                     
                                                         
                       Nápadník
                     
                                                         
                       Univerzitná nemocnica vyriešila problém Richterových prezervatívov
                     
                                                         
                       Obrovská stará dáma
                     
                                                         
                       Tutoľa máme kopčok
                     
                                                         
                       Bojovníčka
                     
                                                         
                       Vifon
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




