
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Novota
                                        &gt;
                Nezaradené
                     
                 O náboženstve, kňazoch a občianskej zodpovednosti 

        
            
                                    9.6.2010
            o
            9:35
                        (upravené
                9.6.2010
                o
                9:45)
                        |
            Karma článku:
                9.08
            |
            Prečítané 
            1262-krát
                    
         
     
         
             

                 
                    Trochu oneskorená reakcia na výzvu spišských kňazov k nastávajúcim voľbám.
                 

                 Nie je tajomstvom, že prvoradým cieľom žiadneho náboženstva nie je podnecovať ľudí k samostatnému a kritickému mysleniu. Skôr naopak, náboženstvá podávajú hotové riešenia. Náboženstvá milujú odpovede. Aspoň tie svoje.   Keď sa katolícky kňaz na bohoslužbe prihovára k svojim ovečkám, netrápi ich otázkami s otvoreným koncom. Otázky vždy konvergujú v podstate k jedinej dileme: nebo alebo peklo? Ak sú vaše vlastné odpovede v súlade s tým, čo hovorí kňaz, máte šťastie, ak nie... zvykajte si na vysoké teploty. Kňaz pritom vystupuje aj v úlohe akéhosi navigátora, ktorý vás má priviesť k odpovediam, za ktoré dostanete cukrík a nie polepšovňu.   Vzorec protikladu večného blaha a večného zatratenia väčšinou funguje. Prinajmenšom do tej miery, že ľudia s "nesprávnymi" odpoveďami radšej mlčia.   Existujú však otázky, ku ktorým by sa kňazi vyjadrovať nemali? Z hľadiska náboženstva je situácia pomerne jasná. Biblia nie je iba kniha plná fantastických postáv, ale aj univerzálny manuál k univerzu - obsahuje všetky odpovede asi tak, ako hárok papiera obsahuje všetky zvieratká origami. Stačí mať šikovné prsty a voilà...   Preto neprekvapuje počin skupinky slovenských kňazov, ktorí sa rozhodli spoločnými silami odpovedať na otázku, ktorúžeto politickú stranu by mali túto sobotu voliť ich ovečky. Nie je podstatné, na ktorú stranu padla ich šťastná ruka, podstatné sú otázky, ktoré tento ich krok vyvoláva.   V prvom rade je to otázka hraníc uplatňovania kňazskej autority.  Nebolo by čudné, ak by vám váš kňaz po tom, čo ste dovŕšili vek dospelosti, “odporučil” nejakého konkrétneho človeka, s ktorým máte vstúpiť do manželstva? Každý intutitívne cíti, že takýto kňaz, ľudovo povedané, "lezie kam nemá".   Hoci je vyššie popísaná modelová situácia analogická k situácii, kedy si veriaci majú voliť ľudí, ktorí budú najbližšie štyri roky do značnej miery ovplyvňovať ich životy, “odporúčanie” spišských kňazov už vonkoncom až také rozhorčenie nevyvoláva. Prečo? Je rozhodnutie o voľbe konkrétnej politickej strany niečo, čo by mali ľuďom servírovať samotní kňazi? Nemali by skôr svojich veriacich viesť k tomu, aby sa sami vedeli správne rozhodnúť (tak ako je to v prípade manželstva)? Nemali by ich skôr systematicky vychovávať k čomusi takému, ako je "občianska zodpovednosť"?   Výzva kňazov môže svedčiť o tom, že nedôverujú svojim ovečkám. (Pripomína to situáciu zo školy, keď učiteľka radí svojim žiakom pri teste, na ktorý ich sama pripravovala.) Alebo to môže znamenať, že oni sami ako pastieri zlyhali, pretože nakoniec aj tak musia odpoveď dotiahnuť za svojich žiakov.   Každopádne, v pritakaní kňazom žiadna občianska zodpovednosť nie je. Je v ňom buď presvedčenie o tom, že hovoria pravdu (u tých, ktorí sa vedia rozhodnúť sami a urobili by to aj bez ich odporúčania), alebo strach (u tých, ktorí sa rozhodnúť nevedia a sú voči diktátu kňazov bezbranní) alebo len obyčajné pokrytectvo. To je jeden problém. Druhý spočíva v tom, že ľudia by mali pristupovať k politike zodpovedne aj mimo volieb. A to len tak nejaké “odporúčania” nevyriešia.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (54)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Novota 
                                        
                                            Hrúzy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Novota 
                                        
                                            Krivošíkove prízraky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Novota
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Novota
            
         
        milannovota.blog.sme.sk (rss)
         
                                     
     
        Narodil som sa v Trnave, žijem v Bratislave, mám rád zdravý rozum a nemám rád, keď ľudia zneužívajú jeho nedokonalosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1258
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




