

 K puku sa dostal Timonen a nahodený puk smeroval k ľavej žŕdke našej bránky, kde sa presúval Halák. Puk sa obtrel o Cháru a smeroval vedľa pravej žŕdky. Na bránkovisku stál Hagman. S nadsadením povedané, ak by nemal o číslo väčší dres, puk by v bránke neskončil. 
 Tu sa zlomil zápas. Vzápätí nám dal Jokinen dva rýchle góly po chybách našej obrany. Halák vyriešil v zápase množstvo zložitých situácií. Prvý gól Sala chytiť mal, možno ho pomýlila snaha Hagmana o teč. 
 V druhej tretine sme využili dve presilovky. Počas zvláštneho 4-minútového vylúčenia Radivojeviča (2 minúty áno, prečo 4 neviem) sme dokonca strelili gól v oslabení (na turnaji skórovali v tejto hernej situácii iba Nóri proti USA a teraz my proti Fínom). Všetko hralo v náš prospech, Fíni boli na kolenách. Pomohli sme im našimi chybami. 
 Inkasovali sme 9 menších trestov, Fíni 7. Využili sme dve presilovky, Fíni tri. Pred zápasom sme boli najlepší v hre v oslabení... V rovnovážnom stave padol jediný gól, Jokinen ním vyrovnal na 3:3. 
 V štúdiu po zápase hovoril Golonka niečo o rozhodcovskej lobby. Je pravdou, že zápasy na ZOH rozhodovali spolu s rozhodcami IIHF aj rozhodcovia NHL a pri zápase s Fínskom bola dvojica hlavných rozhodcov z NHL. Napriek tomu Golonkovým slovám celkom nerozumiem: 




 IIHF - OFFICIATING COMMITTEE (rozhodcovská komisia IIHF)  


   


   




 Juraj Siroky 


 Chairman 


   






 Konstantin Komissarov 


 Secretary 


   








   






 Reto Bertolotti 


 SUI 


   






 Paval Halas 


 CZE 


   






 Jarmo Jalarvo 


 FIN 


   






 Matt Leaf 


 USA 


   






 Gerhard Lichtnecker 


 GER 


   






 Bob Nadin 


 Honorary 







 Zápas nám však rozhodcovia neprehrali. Faulovali naši hráči. 2x vysoká hokejka, 1x zlé striedanie. Ku koncu nám rozhodcovia aj trochu pomohli, keď Hossa prifilmoval pád po faule Pitkänena. 
 Počas nášho záverečného tlaku sme nastrelili žŕdku, Zedník trafil zázračne lapačku, fínsky hráč zaľahol puk pred bránkoviskom, Handzuš netrafil poloprázdnu bránku. V týchto chvíľach sme jednoducho šťastie nemali, Dopita, Patera ani Moravec slovenské občianstvo nemajú. 
 Naše góly na ZOH dávali najmä Hossa, Demitra a Gáborik (dostávali aj najviac priestoru, čo je vzhľadom k ich kvalitám úplne samozrejmé). Viac priestoru v zlomových situáciach mal jednoznačne dostať Šatan. Mal byť na ľade počas nášho záverečného tlaku, mal dostať viac šancí v druhej presilovkovej formácii namiesto Pálffyho, pre ktorého boli vancouverské bránky zakliate. Filc, Hossa a Pokovič sa rozhodli inak. 
   
 Na ZOH 1994 sme vyhrali základnú skupinu. V štvrťfinálovom zápase s Ruskom prebiehala 7.minúta predĺženia. Žigmund Pálffy našiel prihrávkou od mantinelu Petra Šťastného osamoteného medzi kruhmi. Jeho strela skončila v lapačke Zujeva. Prakticky z protiútoku sme inkasovali gól. Vtedy som videl u Viktora Tichonova (3-násobného víťaza ZOH a 9-násobného majstra sveta) prvý raz (a naposledy) ako sa naozaj srdečne smeje, od radosti z víťazstva. 
 Na ZOH 1998 sa hrali prvé dva zápasy kvalifikácie v čase, keď naši hráči v NHL museli hrať súťažné zápasy za kluby. Poslali sme európsky výber doplnený v záverečnom zápase s Kazachstanom o Bondru a Švehlu. Práve v tomto zápase si urobili z našej obrany trhací kalendár Jevgenij a Alexander Koreškovovci. Pálffy, Stümpel, Demitra, Zedník, Šatan sedeli v hľadisku a pozerali na ľad, na ktorý nesmeli nastúpiť. 
 V kvalifikácii na ZOH 2002 sa opakovala situácia spred 4 rokov. Generálnemu manažérovi Petrovi Šťastnému nevyšiel experiment s uvoľňovaním jednotlivých hráčov z klubov NHL na prvé dva zápasy kvalifikácie. V prvom zápase s Nemeckom Šatan, Handzuš, Bartečko a od polovice zápasu aj Pálffy nestrelili ani gól. V ďalšom sme ich dali Lotyšom 6, toľko sme aj inkasovali. Zdeno Chára letel na tento zápas, aby sa nakoniec Šťastný a Filc rozhodli nevyužiť jeho služby (inak by v záverečnom zápase nemali dostatočný počet korčuliarov). 
 ZOH 2006, Slovensku sa tesne pred turnajom zranil Handzuš. V základnej skupine porážame Rusko, Lotyšsko, USA, Kazachstan a Švédsko. Vo štvrťfinále prehrávame s Českom 1:2, keď v záverečnom nápore nedotlačili Gáborík ani Bondra puk ležiaci pár centimetrov pred bránkovou čiarou. 
 ZOH 2010 skončili pre Slovensko sklamaním z celkového umiestnenia. Pretože bronzová priečka bola veľmi blízko. 
 Slovensko hralo v histórii ZOH 3x v plnej zostave. V Lillehammeri, v Turíne a vo Vancouveri. Počas týchto troch olympiád sme odohrali 19 zápasov. Dvanásť víťazných, dvakrát sme remizovali a päť zápasov sme prehrali. Z tých najdôležitejších sa nám podarilo vyhrať iba v tomto roku so Švédskom. Zase raz sme hrali odohrali vynikajúce zápasy, opäť sa o nás pochvaľne vyjadrovali hokejoví odborníci a uznávaní novinári. Zistili sme ako chutí boj o medailu. Medailová radosť však patrí opäť iným krajinám. 
 S nasledujúcou vyjadrením najproduktívnejšieho hráča ZOH 2010 (pred finálovým zápasom) nesúhlasím, ale asi najlepšie vyjadruje pocity hráčov po zápase s Fínskom: 
 Pavol Demitra: "Je úplne jedno, či skončíš štvrtý alebo posledný..." 
   
 Fínsko - Slovensko 5:3 
 (kliknutím na výsledok sa zobrazia štatistiky zápasu z iihf.com) 

