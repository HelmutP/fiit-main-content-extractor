

  
Obr. 1. Bežne dopravné značky, ktorých výklad zvládne každý vodič. 
 Ak tieto dopravné značky nepoznáte pomôžem vám textom z vyhlášky: 
 
Vyhláška 225/2004 Z.z. ktorou sa vykonávajú niektoré 
ustanovenia zákona o premávke na pozemných komunikáciách 
(17) K značkám č. B 30 a č. B 31: 
Na značkách Zákaz státia (č. B 30) a Zákaz zastavenia (č. B 31) môže byť v 
spodnej časti červeného kruhu vyznačený čas, po ktorý zákaz platí. 
(9) K dodatkovej tabuľke č. E 9: 
Dodatková tabuľka Druh vozidla (č. E 9) obmedzuje platnosť značky, pod ktorou je 
umiestnená, na vyznačený druh vozidla; tento druh vozidla sa vyznačuje symbolmi 
zo zákazových značiek. 

   
 Teraz si vyskúšajte ako zvládnete teóriu. Čo znamenajú nasledujúce dopravne 
značky? 
 
 
Obr. 2. Tri zákazové dopravne značky. Prvá platí pre 
nákladné autá, druhá pre traktory a tretia pre záprahové vozidlá. 
 Ospravedlňujem sa za malý chyták u prostrednej značky, nakoľko nejde o zákaz zastavenia ale iba o zákaz státia. Ale o to by ste mali ľahšie zvládnuť prax v uliciach z nasledujúcich fotografií. 
   
 
 
Obr. 3. Bežná zákazová dopravná značka s dodatkovou tabuľkou. Poznáte jej 
význam? 
 
 
Obr. 4. Bežná zákazová dopravná značka s dodatkovou tabuľkou. Poznáte jej 
význam? Viete pre aké vozidlá zákaz platí? 
 Do nového roka 2009 prajem čitateľom a lúštiteľom takýchto hádaniek všetko 
najlepšie. 

