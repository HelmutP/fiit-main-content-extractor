
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Sýkora
                                        &gt;
                Na Slovensku je to tak...
                     
                 Hlavným problémom bilbordu SNS nie je rasizmus 

        
            
                                    7.5.2010
            o
            21:30
                        (upravené
                7.5.2010
                o
                22:02)
                        |
            Karma článku:
                7.48
            |
            Prečítané 
            1093-krát
                    
         
     
         
             

                 
                    Tak ako SNS úspešne realizovala megazlodejské tendre počas svojho vládnutia a jej predseda je megaprimitív, tak je aj toľko diskutovaná bilbordová kampaň ... megaúspešná. Áno, megaúspešná.
                 

                 Predovšetkým preto, že v médiách sa o „strake“ SNS (Shooty) hovorí, Jano je hviezda a ako už je v dnešnej dobe zvykom – témou nie sú reálne problémy, ale len povrchné záležitosti okolo.    V prvom rade je obsah bilbordu presne charakterizovaný tým, že zo strany SNS by nás takéto názory nemali prekvapovať. A keď použili podobne agresívnu kampaň „proti Maďarom“, nikto sa až takto výrazne nad tým nepozastavoval.    Provokatívna myšlienka ma zvádza k úvahe, či situácia ilustrovaná na inkriminovanom bilborde nie je neporovnateľne pálčivejšia (a nanešťastie aj bližšia realite) ako údajný maďarský iredentizmus a podobné úvahy.    Ďalšiou vecou je, ako by sa v prípadnom prieskume vyjadril „pospolitý ľud“, či sa s posolstvom stotožňuje. Ešte pozitívnejšie by z hľadiska úspešnosti kampane určite dopadlo vnímanie cieľovej skupiny - tej, ktorú SNS potrebuje zmobilizovať pred blížiacimi sa voľbami. Individuálnym názorom podla mňa zostáva, či je kampaň rasistická alebo (ne)etická. (Ja osobne ju za rasistickú nepovažujem, ale takéto manipulatívne poňatie kampane je neetické.)    Zaujímavým rozmerom je, ako pôsobia na „priemerných ľudí“ rozhovory s Rómami v osadách. (Budem sa súdiť „o tisíc, čo aspoň 10-tisíc eur.“) Osobne si myslím, že v prvom rade utvrdzujú v ľudoch predsudky. A to ešte nepozerám „tlačenky“ v televízných správach, ktoré sú na Slovensku oveľa manipulatívnejšie ako iné médiá.   Paradoxom doby je, že pokiaľ niekto nesúhlasí s väčšinovým názorom, je automaticky považovaný za rasistu. Bez ohľadu na argumenty. V tomto by som (dokonca!) dával skôr za pravdu SNS, že rasista je ten, čo rasizmus v tom bilborde vidí. Veď keby tam bol „biely“ (nemám rád toto nálepkovanie) a „message“ by bola rovnaká, neviem, či by to niekto považoval za problém. Nehovoriac o tom, že kto by sa v multikultúrnej spoločnosti pozastavoval nad tým, či je na bilborde černoch, Aziat alebo Hispánec?  Na druhej strane za ironické považujem to, že mnohí by sa stotožnili s myšlienkou „Stop zneužívaniu sociálneho systému“, čo je v podstate oveľa kultivovanejšia alternatíva k sloganu SNS. V tomto ohľade to je z „marketingového hľadiska“ skutočne majstrovské dielo. Plus to ma pridanú hodnotu bezplatného masívneho pokrytia značkou SNS v médiách.   V konečnom dôsledku môže byť prehnaná politická korektnosť, spolu s prebujnelým sociálnym štátom (ha, ha, ha - dnes o tom po príklade Grécka hovorí už každý), cestou k ohrozeniu Európy ako takej.    K podstate problému: hlavnou otázkou novinárov by malo byť, čo spravila SNS za štyri roky vo vláde, aby sa riešil rómsky problém a vo všeobecnosti problematický sociálny systém ako taký – dnes používaný ako úderka v kampani. Ale ja vlastne poznám odpoveď: nič. Absolútne nič.    A presne to je jeden z troch najväčších problémov súčasnej vlády – ideová výprazdnenosť. Ako riešiť deficit verejných financíí, nepriazdnivý demografický vývoj či situáciu v zdravotníctve? Skrátka, čo s našim Slovenskom - ktorým sa toľko oháňajú - ďalej? Z fráz (sociálny štát), demagógie (ataky druhého piliera), rozkrádania (nástenkový tender a iné) a lží (kauza Hedviga Malinová a ďalších XYZ tlačoviek) sa to nedozvieme. A nikam to Slovensko ako krajinu neposunie.   Zvyšnými top problémami sú Štefan Harabin a korupcia. Ale predovšetkým ideovou vyprázdnenosťou je charakteristická súčasná vláda Smeru a ich prehnitých koaličných partnerov. Toto by podľa mňa mala byť zbraň novinárov voči kontroverznej kampani SNS a jej hulvátskych predstaviteľov.   P.S.: A určite by sme sa nemali tváriť, že tu načrtnutý problém - zneužívanie sociálneho systému - neexistuje. Možno by sa dokonca aj štatisticky potvrdilo, že je do veľkej miery spojený s rómskym etnikom (čo osobne nepovažujem za kľúčové), ale k tomu sa dá opäť len dodať - zamerajme sa na skutočné problémy a hľadajme ich riešenia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Nosme si do supermarketov vlastné „sáčky“
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Daruj darček
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Neľudské zlo v koncentračnom tábore Sachsenhausen
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Malé múzeum Stasi, tajnej služby z čias NDR
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Odtíňať ruky fajčiarskym prasatám
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Sýkora
            
         
        matejsykora.blog.sme.sk (rss)
         
                                     
     
        Človek. Aspoň sa snažím.
 Občas trochu stručný a chladný,
 inokedy zas obšírnejší a citlivý.

 Facebook 
 LinkedIn 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    151
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1173
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Občianske združenie BERKAT
                        
                     
                                     
                        
                            Potrebujeme (r)evolúciu?
                        
                     
                                     
                        
                            Život (je krásny)
                        
                     
                                     
                        
                            Svet ľudí sa nás týka
                        
                     
                                     
                        
                            Na Slovensku je to tak...
                        
                     
                                     
                        
                            Adjumani, Uganda
                        
                     
                                     
                        
                            Írsko, Dublin
                        
                     
                                     
                        
                            Londýn, Veľká Británia
                        
                     
                                     
                        
                            Randers, Dánsko
                        
                     
                                     
                        
                            New York, USA
                        
                     
                                     
                        
                            Triedenie odpadu
                        
                     
                                     
                        
                            Business
                        
                     
                                     
                        
                            Užitočné
                        
                     
                                     
                        
                            Festival Jeden svet
                        
                     
                                     
                        
                            Festival Pohoda
                        
                     
                                     
                        
                            SOS Povodne 2010
                        
                     
                                     
                        
                            STOP ZLU
                        
                     
                                     
                        
                            Energia naša každodenná
                        
                     
                                     
                        
                            Každý iný - všetci rovní
                        
                     
                                     
                        
                            Aktualizované z archívu
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo by som nešiel do povstania ?
                     
                                                         
                       TOP 10 výrokov politikov v roku 2013
                     
                                                         
                       Dobrý projekt jedna krádež nezastaví
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




