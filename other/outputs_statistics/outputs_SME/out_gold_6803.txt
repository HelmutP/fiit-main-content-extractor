

 O čo ide?  
Profesionálny vodiči majú striktne definovaný maximálny pracovný čas, maximálnu 
pracovnú pohotovosti, prestávky v práci, minimálnu dobo odpočinku. Okrem toho 
zákon definuje aj kontrolné mechanizmy vrátane inšpekcie. Rieši to zákon 
462/2007 Z.z. o organizácii pracovného času v doprave. 

 Od 15.04.2010 na základe zákona 144/2010 k tomu pribudol v rámci harmonizácie 
zoznam porušení osobitých predpisov ES 561/2006, EHS 3821/85, ES 2135/98 kde sa 
uvádza:
 

http://jaspi.justice.gov.sk/jaspidd/vzory/010144Pr1.pdf 
(text hľadaj na konci dokumentu) 

 
Ešte šťastie, že predpisy EU sa do našich zákonov nepreberajú 1:1. V našom 
zákone bol ponechaný príslušný text v zmysluplnom tvare: 
 
EU:  
Veľmi závažné porušenie predpisov: "súvislosť medzi mzdou a prejdenou vzdialenosťou alebo množstvom dopraveného tovaru".
 
 
SK: 
(4) Inšpektorát práce uloží pokutu od 50 000 Sk do 500 000 Sk 
a) právnickej osobe alebo fyzickej osobe - podnikateľovi, ktorá poskytne 
vodičovi motivačnú prémiu, príplatok k mzde alebo inú peňažnú alebo nepeňažnú 
výhodu za prejazdenú vzdialenosť alebo za prepravené množstvo tovaru alebo osôb, 
ak sa tým ohrozila bezpečnosť cestnej premávky alebo motivovalo porušenie 
pravidiel pracovného času, času jazdy, času pracovnej pohotovosti a prestávok 
v práci alebo dôb denného alebo týždenného odpočinku zo strany vodiča, 

 Záver: 
Je načase aby vedenie EU sa začalo zaoberať niečím zmysluplným a potrebným, ako napríklad vyrovnané štátne rozpočty a nie zavadzaním praktík o nezásluhovom odmeňovaní za prácu, z ktorých Slovensko už dávno vyrástlo. 

