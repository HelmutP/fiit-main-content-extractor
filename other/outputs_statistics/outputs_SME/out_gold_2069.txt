

 Pochádzaš z Kežmarku. Aká bola tvoja cesta na bratislavskú VŠVU? 
   
 Už odmalička som vedela, že chcem byť „umelkyňa ako moja mamka".  Tá ma našťastie v mojom smerovaní odjakživa podporovala. Od ZUŠ až po vysokú školu ma aj pedagogicky viedla a teda asi ona mi udala smerovanie k textilu (ona sama skončila VŠVU v ateliéri  voľnej maľby a gobelínu u prof. Matejku).  Jednoducho som vedela, že VŠVU je môj cieľ a dodnes si pamätám, aká prekvapená a šťastná som bola, keď ma tam vzali na prvý pokus. 
 Prečo si si vybrala práve textil? Čo ťa lákalo na tomto odbore? 
   
 Vlastne moje úplné textilné začiatky siahajú do skorého detstva, keď som všetky členky našej rodiny nútila nech ma učia vyšívať, háčkovať, štrikovať, tkať... Na ZUŠ som mala najradšej textilné techniky, na strednej škole som študovala textilnú tvorbu, takže výber odboru na VŠVU bol jasný. Na škole som sa však trošku stratila a nevedela si vybrať smerovanie- či ísť smerom voľnej tvorby, či cestou textilného dizajnu. Nakoniec to u mňa vyhral dizajn, pretože si myslím, že je to väčšia výzva. 
 Čo konkrétne robí študent textilného dizajnu počas štúdia? 
   
 Študenti textilného dizajnu prejdú prvým ročníkom, ktorý je rovnaký pre všetky odbory, to znamená, že absolvujú štyri kurzy ľubovoľného výberu.  V každom ročníku potom dostávajú témy, na ktoré reagujú. Témy sú napríklad návrh a realizácia koberca, dezénu pre interiér, či módnych doplnkov. Tu sa nevyhnem kritike ateliéru textilného dizajnu na VŠVU, ktorý žiaľ väčšinou viedli umelci, ktorí nikdy neboli vystavení problémom textilného dizajnéra, ktorý sa textilným dizajnom aj živí. Na škole chýba výučba základných vecí, ktoré každý dobrý dizajnér musí ovládať- napríklad už len povinná výučba grafických programov, bez znalosti ktorých sa nedá ani tvoriť a pri hľadaní si práce v odbore by vás ani na pohovor nezavolali. Našou slovenskou výhodou je vysoká kreativita a estetické cítenie, no keď sa má dizajnér živiť dizajnom, je mu kreativita na nič, ak ju nevie predať. Ďalším problémom je nedotiahnutosť navrhnutého projektu, pretože študentom nie je poskytnutá adekvátna odborná pomoc. 
 Zrejme preto si sa rozhodla vyskúšať zahraničnú stáž... 
   
 Prvú reálnu výučbu textilného dizajnu som zažila vo Fínsku, ktoré je bezpochyby Mekkou textilného dizajnu, viď najúspešnejšia a jedinečná firma svojho druhu Marimekko, kde som bola na stáži v roku 2006 na University of Lapland. Tam sa kládol dôraz na dôležitosť prezentácie a predajnosti výrobku, kvality spracovania.  Šlo o to, aby projekt, ktorý navrhnem mal cieľovú skupinu a vedel sa predať. 
   
 Aké boli tvoje začiatky po škole? 
   
 Škola ma veľmi zdeprimovala a vôbec som si po tých šiestich rokoch neverila. Preto som na rok odišla späť do Kežmarku, pracovala som v reštaurátorských štátnych ateliéroch a popri tom som začala budovať moju súčasnú značku Puojd. Reštaurovanie naozaj nebolo mojim cieľom, a cítila som, že všetky veci ohľadom textilného dizajnu sa stále musím učiť. Preto som sa všemožne snažila opäť vycestovať a zamestnať sa v odbore. Podarilo sa mi získať štipendium a miesto v helsinskej skupine dizajnérov. Strávila som v Helsinkách krásnych šesť mesiacov a naučila som sa od tunajších dizajnérov za tú krátku dobu viac, než na škole. Návrat na Slovensko pre mňa znamenal zamestnanie sa vo firme, ktorá nemala nič spoločné nielen s textilom, ale ani s umením. No postaviť sa na nohy bolo treba a aspoň som si zažila niečo, čo viem, že už nedopustím. Od septembra 2009 sa živím značkou Puojd. Zaklopem, ide mi to nad moje očakávania. 
  
 Textilný dizajn sa dá pomerne ľahko uplatniť v reálnom  živote. Musela si niekedy robiť ústupky kvôli spotrebiteľskému trhu?  
   
 Som rada, že sa pýtaš túto otázku, pretože je v nej kus pravdy. Áno, textilný dizajn sa dá ľahko uplatniť v reálnom živote. A mal by to byť cieľ každého dizajnéra. Ako príklad uvediem jedinú skutočnú textilnú značku Popular, ktorá má zamestnancov a vysoko kvalitné produkty. Puojd aj Popular sú značky, ktoré dokazujú, že sa dá nájsť zlatá stredná cesta a stále si udržať úroveň vysokej kvality. Dizajnér musí mať svoju cieľovú skupinu, ja som si vybrala bežných ľudí, ktorí majú zmysel pre humor. Myslím si, že práve pre tento výber ústupky nemusím robiť. 
 Puojd je dnes už známa značka. Čo bolo prvým impluzom na jej vytvorenie? 
   
 Prvý impulz som dostala na výlete v UK, keď som v obchodoch často nachádzala nejaký vtipný suvenír s anglickou vlajkou, od pier až po posteľnú bielizeň. Povedala som si- prečo nie iný slovenský suvenír ako valašky a modrotlače? A tak vznikla moja bakalárska práca inšpirovaná slovenským znakom. S odstupom času si priznávam chyby, ktoré som v nej urobila, no napriek tomu sa celá kolekcia vtedy predala a to mi dalo presvedčenie, že v tom musím pokračovať. I keď školské hodnotenie dopadlo katastrofálne. 
 Z čoho vznikol názov „puojd"? 
   
 To je strašne dlhý príbeh, bolo to ako pôrod. Hľadala som slovensky znejúce slovo, nakoniec sa mi zapáčil pôjd čiže povala. Kvôli internetu som to upravila na puojd. Prvý slogan bol „najlepšie veci nájdeš na puojde". 
   
 Aké sú ohlasy na tvoju značku? Máš ohlasy aj zo zahraničia? 
   
 Keď som sa pustila do slovenského znaku, bolo mi jasné že mi bude väčšina ľudí vyčítať nacionalizmus. No moji zákazníci sú ľudia s nadhľadom a chápu smerovanie a ideu Puojdu. Puojd navyše podporuje slovenskú produkciu, výrobky sú vysokej kvality a vyrobené na Slovensku, takže pozitívnych ohlasov je veľa. Zatiaľ sa na zahraničie neorientujem, no moji zahraniční známi u mňa nakupujú. Dokonca som dostala ohlasy napríklad z Talianska, Fínska a Dánska od ľudí, ktorých nepoznám, že by radi nosili Puojd i keď nikdy na Slovensku neboli. 
  
   
   
  
 Minulý týždeň prišiel Puojd s novými „bombastickými" tričkami... 
   
 Bombatričká vznikli ako reakcia na bombastický incident, ktorý sa nedávno stal. Sama som najprv uvažovala, či je dobré púšťať sa do niečoho zaváňajúceho kontroverznosťou. No v dobe, keď sa dejú takéto silné veci, o ktorých hovorí nielen celý národ, ale aj celý svet, ako dizajnér idúci s dobou musím zareagovať. Ja to však beriem s humorom a rada odľahčujem vážne témy. Prečo sa na sebe trošku nezasmiať? Dostala som samozrejme aj negatívnu odozvu, ktorá však nevedela argumentovať ničím iným, ako zlou reklamou Slovenska. No neviem, či moje tričká budú hlavnou témou v New York Times :) 
 Sleduješ slovenský dizajn a dizajnérov? Koho práca sa ti páči? 
   
 Neviem, či je korektné menovať, no niektoré mená veľmi rada spomeniem, pretože prácu týchto ľudí si veľmi cením a často ich nikto na Slovensku ani nepozná. Čo sa týka môjho odboru, tak v ňom určite vyhral boj Juraj Straka, ktorý už niekoľko rokov pracuje ako návrhár vo francúzskom Lyone vo firme BUCOL Holding Textile Hermes, ktorá navrhuje látky pre svetové značky ako YSL a podobne. Jeho práca sa už niekoľkokrát prezentovala na najvýznamnejších módnych podujatiach. Pokiaľ viem, je jediný slovenský dizajnér s takýmto úspechom a to je len na začiatku kariéry. Ďalej si veľmi cením nasadenie a cieľavedomosť dizajnérov, ktorí založili Denamit. O Denamite by sa však dal napísať ďalší článok, preto len spomeniem mená Lenka Sršňová, ktorá je bezpochyby najproduktívnejšia slovenská módna návrhárka a tiež je vo svojom odbore na Slovensku nedocenená, hoci má skúsenosť s prácou pre módnu značku svetového rozmeru v Paríži. Šperkárka Radka Kovačíková je pre mňa dizajnérka s nehraničnou kreativitou a zmyslom pre humor, v odobre šperku sa nedá nespomenúť neuveriteľne všestrannú Bety Majerníkovú, v keramike Simonu Janišovú, Lindu Vikovú, Markétu Novákovú, z produkt dizajnu Comunistar alebo Sylviu Jokelovú, za sklo musím spomenúť Patrika Illa, ktorý je medzinárodne uznávaný dizajnér.Grafický dizajn má zastúpenie viacerých silných mien a ja som pyšná, že práve Puojd s niektorými z nich mohol spolupracovať. Stránku Puojdu navrhol Ondrej Jób a logo mám od Jána Filípka. Možno to teraz vyzerá tak, že som spomenula len svojich kamarátov, no naša komunita nie je až taká veľká, aby sme sa nepoznali a nepomáhali si. 
   
  
   
   
  
  
   
 Oficiálna stránka: puojd.sk  
 Puojd nájdete aj na Facebooku: puojd 
   
   
   

