

 Keď slnko ledva stihne zájsť za kopec a komáre vychádzajú na lov potravy tak naša ulica ožíva. Ľudia vychádzajú von, sadajúc na lavičky a začína sa spravodajská hodinka. Internet je šuviks popri tom čo sa človek na lavičke dozvie. 
 Čo kto varil, čo sa nakúpilo. Kto sa narodil, kto s kým chodí, svadby na spadnutie, komu zvonil umieračik. 
 Starenky hundrú na mládež. Ale len na tú cudziu. Mládež v rodine je tabu, poprípade sú všetci šikovní anjelici. Tí anjelici za záhradou za stohom potajme fajčia a obkukávajú dievčatá. Sú v bezpečí, stárež sedí pred domom a z chuti obkecáva každého neprítomného. 
 "Si predstavte, tá frnda Mara má zas iného!" 
 "Jój, vravím vám, tej sa o tom asi aj sníva!" Prichádza Mara a ženy stíhnu. Zrazu sa jedna ozve: 
 " Vieš Mara, myslím že by si to mala vedieť ale Kata ťa včera hrozne poohovárala. Škoda že tu nie je, urobila by si s ňou poriadok." Mara po chvíli odchádza a ženy na seba významne mrknú. 
 "Júj, ňimfomarka je to, ňimfomarka!" 

