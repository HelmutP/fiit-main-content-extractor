

 Nástupca osvedčeného Su-7 prebral takmer nezmenený trup po svojom predchodcovi a hlavnou otázkou zostalo vyriešenie pohyblivého krídla. Vsadenie celého pohyblivého krídla by znamenalo celkovú prestavbu trupu a krídla a preto bol zvolený druhý variant – čiastočne meniteľná geometria krídla. To znamenalo ponechať pohyblivú iba vonkajšiu časť krídel, ktorá by sa pohybovala podľa charakteru letu a s trupom zvierala podľa potreby uhol 28, 45 alebo 62 stupňov. Meniteľná geometria krídla zabezpečuje lietadlu dlhší dolet a pri vzlietaní alebo pristávaní mu umožňuje operovať na kratšej dráhe. 
   
 Prvý let skúšobného lietadla Su-7IG sa uskutočnil 2. augusta 1966 a tento stroj pri ňom dosiahol výrazne lepších letových vlastností ako starší Su-7. Preto po odstránení drobných chýb mohlo dôjsť v roku 1970 k zahájeniu sériovej výroby prvého sovietskeho lietadla s meniteľnou šípovitosťou krídel pod označením Su-17. Prvý let verzie Su-17 pilotovaný E. K. Kukushevom sa datuje k 1.6. 1969. 
   
  
 
(Foto: Suchoj Su-17. Zdroj: http://landman.vif2.ru/pages/SU-17-1.htm) 
 
   
  
 
(Foto: Suchoj Su-17. Zdroj: http://www.gsvg.ru/arms.html) 
 
   
  
 
(Foto: Suchoj Su-17. Zdroj: http://www.aviapanorama.narod.ru/journal/2003_2/su-17.htm) 
 
   
  
 (Foto: Suchoj Su-17. Zdroj: http://www.aviapanorama.narod.ru/journal/2003_2/su-17.htm) 
   
 Su-17 sa vyrábal v mnohých modifikáciách. V exportnom prevedení sa tento typ označoval ako  Su-20 a Su-22. A práve Su-22 bolo zavedené aj do československej armády. Stalo sa tak v roku 1984 a boli nám dodané verzie Su-22M-4 (jednomiestna verzia) a Su-22UM-3 (dvojmiestna cvičná verzia) celkovo v počte 60 lietadiel. Výroba toho úspešného stíhacieho bombardéru sa skončila v roku 1990. Všetkých verzií typu Su-17 bolo vyrobených 2867 kusov a z toho bolo 1165 exportovaných do viacerých krajín sveta. 
   
  
 
(Foto: Suchoj Su-20 egyptského letectva. Zdroj: http://www.egyptdailynews.com/egyptian%20airforce.htm) 
 
   
 Po rozdelení Československa pôsobili Su-22 aj v slovenských ozbrojených silách. Z leteckých základní v Česku postupne preleteli na základńu v Malackách 3 dvojmiestne verzie Su-22UM-3 a 18 jednomiestnych Su-22M-4. Slovenská armáda sa však stretávala (a stále stretáva) s výrazným nedostatkom financií, čo sa prejavilo na nízkom počte nalietaných hodín a slabých možnostiach údržby strojov. Slovensko si nemohlo dovoliť prevádzkovať všetky lietadlá zdedené po ČSFR a tak došlo po prijatí Koncepčného výhľadového dokumentu Model 2001 k výraznej redukcii stavov letectva. Pre nedostatok financií sa uvažovalo iba s defenzívnym použitím vzdušných síl na obranu teritória. Na udržanie prieskumnej a údernej spôsobilosti reprezentovanej lietadlami Su-22 a Su-25 sa rezignovalo. Letu schopné Su-22 sa preto v rokoch 1999 a 2001 odpredali do Angoly (väčšina po generálnej oprave v Trenčíne). Niektoré Su-22 zostali aj po vyradení na Slovensku ako múzejné exponáty alebo učebné pomôcky. 
   
  
 (Foto: Slovenský Suchoj Su-22M-4. Autor: Kamil Pankuch)   
   
  
 
(Foto: Suchoj Su-22M-3K na letisku Malacky - Kuchyňa. Autor: Aleš Hottmar) 
 
   
  
 
(Foto: Suchoj Su-22M-4 na letisku Malacky - Kuchyňa. Autor: Aleš Hottmar) 
 
   
 Verzie: 
   
 - Su-17 - Prvá sériová verzia vyrobená v malom počte kusov. 
 - Su-17M -  Do tejto verzie bol namontovaný výkonnejší motor AL-21F-3. 
 - Su-17M-2 - Mierne zpravený trup, namontovaný radar k monitorovaniu terénu, laserový vyhľadávač. 
 - Su-17M-3 - Ďalšie konštrukčné zmeny, schopnosť odpaľovať rakety vzduch-vzduch. 
 - Su-17UM-2 - Dvojmiestna cvične-bojová verzia odvodená od Su-17M-2. Má jeden kanón v koreni pravého krídla. 
 - Su-17UM-3 - Zdokonalená predchádzajúca verzia vybavená dopplerovským navigačným systémom, väčšími palivovými nádržami a väčšou kýlovou plochou. 
 - Su-17M-4 -  Táto verzia nesie bohaté prostriedky pre vedenie REB (rádioelektronického boja), má púzdra pre 4 kanóny ráže 23 mm, z ktorých dva môžu strieľať dozadu. 
 - Su-20 - Exportná verzia Su-17M s horším prístrojovým vybavením. 
 - Su-20R - Prieskumná verzia tohto určeného pre export. 
 - Su-22 - Exportná verzia Su-17M-2, vybavená lepším motorom s možnosťou odpaľovať rakety vzduch-vzduch. 
 - Su-22U - Exportná verzia Su-17UM-2. 
 - Su-22M-3 - Exportná verzia Su-17M-3 s väčším objemom palivových nádrží a väščou kýlovou plochou. 
 - Su-22UM-3K - Exportná verzia Su-17UM-3. 
 - Su-22M-4 - Konečná exportná erzia odvodená od Su-17M-4. 
 
   
  
 (Foto: Český Suchoj Su-22) 
  
 (Foto: Suchoj Su-22 poľských ozbrojených síl) 
   
 Technické dáta: 
 
 Rozpätie: 10,03/13,7 m 
 Dĺžka: 19,02 m 
 Hmotnosť prázdneho lietadla: 10 640 kg 
 Maximálna rýchlosť: 1850 km/h 
 Dolet: 1400 až 2550 km (podľa výzbroje) 
 Dostup: 15 200 m 
 Stúpavosť: 230 m/s 
 
   
 Bojové nasadenie: 
 Verzia Su-17 používaná výhradne v Sovietskom zväze bola nasadená pri ruskej invázii do Afganistanu (Afganistan použil stroje Su-22M-4). Vývozné verzie označené ako Su-20 a Su-22 boli použité počas vojny medzi Irakom a Iránom (1980 - 1988) na oboch stranách konfliktu, počas vojny v zálive v 1991 na strane Iraku a dva stroje Su-22 vojenského letectva Líbie boli zostrelené počas amerického vojenského cvičenia lietadlami F-14 Tomcat. (viac podrobností o tomto konflikte medzi Líbiou a USA tu: http://www.valka.cz/newdesign/v900/clanek_10830.html ) 
   
 Užívatelia: 
 
 Arménsko 
 Afganistan 
 Alžírsko 
 Angola 
 Azerbajdžan 
 Bielorusko 
 Bulharsko 
 Česká republika 
 Československo 
 NDR 
 Egypt 
 Nemecko 
 Maďarsko 
 Irak 
 Irán 
 Líbia 
 Severná Kórea 
 Peru 
 Poľsko 
 Rusko 
 Slovensko 
 Sovietsky zväz 
 Sýria 
 Turkmenistan 
 Ukrajina 
 Uzbekistan 
 Vietnam 
 Jemen 
 
   
 
 
 
 
 DOPORUČUJEM! KOMPLETNÝ ZOZNAM TYPOV LIETADIEL ČS ARMÁDY V OBDOBÍ 1945-1992! 
 
 
 http://culak.blog.sme.sk/c/185489/Kompletny-zoznam-lietadiel-v-ceskoslovenskej-armade-v-r-1945-1992.html 
 KOMPLETNÝ ZOZNAM TYPOV LIETADIEL SLOVENSKEJ ARMÁDY OD ROKU 1993 
 http://culak.blog.sme.sk/c/185889/Kompletny-zoznam-typov-lietadiel-slovenskej-armady-od-roku-1993.html 
 
 
 
 
   
 Zdroje článku: 
 http://www.airbase.cz/cms/?q=sukhoi-su-22-fitter 
 http://www.military.cz/russia/air/suchoj/Su-22/su-22.htm 
 http://cs.wikipedia.org/wiki/Suchoj_Su-17 
 http://en.wikipedia.org/wiki/Su-22 
 http://www.valka.cz/newdesign/v900/clanek_10830.html 
 http://www.lietadla.com/index.php?str=kategoria&amp;idkat=1 
 
 
 

