

 JÚĽŠ sa nachádza v Bratislave v historickej časti mesta na Panskej ulici. Ako inak, na priečelí má pamätník Ľudovít Štúr a na blízkom námestí Anton Bernolák. Po Martinovi Hatalovi, ktorý skomplikovaním pravopisu zahatal ďalší vývoj, najmä elektronické spracovanie jazyka nie je žiadnej čestnej pamiatky. 

 V deň otvorených dverí som sa zameral na dve veci. Prvou bola Prezentácia Slovenského národného korpusu, hlavne to čo si mám pod týmto termínom predstavovať a druha opýtať sa ako to bolo v roku 1951 pri poslednom 
zjednodušení pravidiel Slovenského jazyka. 

 Prvé veľké prekvapenie som zažil už pred začatím prezentácie, odhadoval som, že pôjde o množstvo kníh a iných papierových podkladov. Lenže ide o elektronickú databázu navyše vytvorenú na unixových strojoch v kódovaní UTF-8. Základom korpus Slovenského jazyka je obrovská plnotextová  databáza, ktorá obsahuje cez 350 miliónov slov s najrôznejších zdrojov. Počnúc knihami, väčšinou beletriou, článkami z novín  a časopisov vrátane miestnych a aj odborných, pričom čim ďalej je viac textov z internetových serverov, blogov a internetových diskusii. 
 Korpus Slovenského jazyka je vlastne zrkadlo, ktoré odráža reálne používaný Slovenský jazyk zhruba od roku 1953 až po prítomnosť, pričom každý text má záznam nielen pôvode a čase vzniku ale aj príslušnosť ku konkrétnemu územiu Slovenska. 

 Tento systém slúži k štúdiu jazyka, je podkladom pre definovanie nových spisovných slov a pravidiel ich písania. Je v ňom možne okrem vyhľadávania rarít, alebo postupnosti vývoja slov, určovať aj ich príslušnosť k určitej komunite a aj zistiť napríklad aj to ako je to s používaním cudzojazyčných slov v pôvodnom alebo zdomácnenom tvare, ktorého sa veľmi obávajú takzvaní ... 
(označenie týchto ľudí som si nezapamätal ale určite ho niekto v diskusii 
spomenie). 

 Okrem tejto databázy existuje aj iná databáza, ktorá obsahuje len aktuálnu spisovnú slovnú zásobu. Táto databáza by sa dala použiť napríklad pre certifikovanú kontrolu pravopisu pred zverejnením článku v novinách alebo na blogu. Lenže sa tu narazilo na problém autorských práv a komerčného použitia. Pre ich momentálnu neriešiteľnosť je prístup k takejto službe v nedohľadne. 

 Padla reč aj o novom zákone z dielne MŠ o vysokých pokutách za prehrešky voči Slovenčine. JÚĽŠ sa na to pozerá úplne inak ako je to prezentované politikmi a v novinách. Nejde o to aby ľudia sa vzdali svojho nárečia, prišlo by sa o jedinečnosti Slovenského jazyka a ustrnul jeho vývoj. Účel je aby štátne orgány používali spisovný jazyk. Čiže zákony, nariadenia a ostatné štátne písomností od NRSR, cez ministerstva, súdy a špecializovanú štátnu správu, vrátane samosprávy používali v úradnom styku spisovnú formu Slovenského jazyka v písomnej aj hovorovej forme. 

 Starší ľudia si iste pamätajú, že pred rokom 1951 sa v slovesách písalo aj tvrdé y.  Napríklad keď ženy robili bolo jedno a keď muži robili bolo druhé. Keď muži aj ženy robili spolu bol z toho hlavolam ako keď oba víri zmiznú. 

 Práve v roku 1951 sa prijala posledná významná zjednodušujúca zmena pravidiel gramatiky slovenského pravopisu. Hoci po jazykovej stránke bolo pripravené zjednodušenie pravopisu v oveľa širšom rozsahu, okrem iného sa malo zjednodušiť písanie i/y aj v prídavných menách, bohužiaľ nenašlo v 
zbore povereníkov dostatok politickej vôle, preto sa zaviedla len časť pripravených zmien. 

 Odvtedy boli ešte dve menšie korekcie a to v roku 1968 a 1991, ale pri žiadnej sa už nenašla ani taká politická vôľa a spoločenský konsenzus ako v roku 1951. Nuž politika a pravidla Slovenského jazyka sú oveľa 
previazanejšia než sa na prvý pohľad zdá. 

 Z Jazykovedného ústavu Ľudovíta Štúra som odchádzal s dobrým 
pocitom a skorigoval som názor naň. Teraz už viem, že sa Slovenskému jazyku 
venujú s adekvátnou vážnosťou a rozumným pohľadom do budúcna s využitím elektronického spracovania informácii. 

 Fotografie z dňa otvorených dverí: 

  
Obr. 1. Budova Jazykového ústavu Ľudovíta Štúra Slovenskej akadémie vied. 

  
Obr. 2. Anton Bernolák 

  
Obr. 3. Ľudovít Štúr. 

  
Obr. 4. Navigácia na schodisku ústavu. 

  
Obr. 5. Výstižné znázornenie princípu korpusu jazyka. 

  
Obr. 6. Vstup do oddelenia korpusu. Klopanie 
prísne zakázané, ako v škole alebo u doktora. Až na druhý krát som si to 
poriadne prečítal ;)  

  
Obr. 7. Z programátorského hľadiska veľké a príjemné 
prekvapenie. V JÚĽŠ vedia aj Unix a ich systémy dokážu pracovať aj v čistom 
textovom móde, čo ocenia nielen programátori ale napríklad aj ľudia s 
postihnutím zraku. 

  
Obr. 8. Slovenský národný korpus tak 
ako je vidieť v klasickom internetovom prehliadači. 
 Hodili by sa aj ďalšie obrázky, ale téma bola na toľko zaujímavá, že som 
ďalej zabudol fotografovať. Keďže jazyk, je nutná podmienka aj pre písanie 
blogov, určite sa k tejto téme ešte neraz vrátim. 

