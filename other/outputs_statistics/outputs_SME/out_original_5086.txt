
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Jablonská
                                        &gt;
                Intimita
                     
                 A tak sa červenám... 

        
            
                                    23.4.2010
            o
            0:17
                        (upravené
                30.10.2010
                o
                3:45)
                        |
            Karma článku:
                4.69
            |
            Prečítané 
            639-krát
                    
         
     
         
             

                 
                    
                 

                 „Napíš!"   „Napíš niečo odvážne..."   šepká krehký hlások v mojej hlave.   „Niečo plné vášne."       „Pointy nechaj do iných básní,   zhasni,   a prosím, prosím, snáď,   poď sa na chvíľku červenať."       „Píš!"   „Píš o zrýchlenom dychu.   O slastnom vzdychu.   Na chrbte perlí sa pot,   zasyčí knôt   a tma..."       „Potom napíš o neposlušných prstíkoch,   čosi o zipsoch a gombíkoch,   o dvoch hladných perách a jazykoch."       „Spomeň detskú náruživosť,   pocit (ú)plnosti,   zimomriavky až do špiku kostí   a nasledovnú pohybovú živosť."       „A keď už horúca dosť,   napíš o výkriku.   Hlboko zaryté nechty,   ako zastavil sa vesmír,   vody oceánov klesli,   oslavné melódie vzduchom sa niesli   a jedna duša v tej druhej na chvíľu sa zrkadlila."       „No ták!"   „Bezočivá skús byť,   stud a výchovu zahodiť,   predsa stačí iba na chvíľku..   A píš!   Píš o ľahkosti dotyku,   skrátka   o kráse pohlavného styku."     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Jablonská 
                                        
                                            Čosi celistvé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Jablonská 
                                        
                                            Óda na dotyk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Jablonská 
                                        
                                            Ja otáznik...a ty pod otáznikom bodka.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Jablonská 
                                        
                                            Splývam...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Jablonská 
                                        
                                            Proces
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Jablonská
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Jablonská
            
         
        katarinajablonska.blog.sme.sk (rss)
         
                                     
     
        Ktosi neprestajne hľadajúci. Vo svete dokonalosť a originalitu, inšpiráciu, nadšenie, ideálnu kombináciu hlások, slov, tónov či myšlienok. No a v ľuďoch dokonalosť a originalitu, inšpiráciu, nadšenie, ideálnu kombináciu slov, tónov či myšlienok. Nakoniec v sebe hľadá dokonalosť a originalitu, inšpiráciu, atď...... :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    261
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Intimita
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




