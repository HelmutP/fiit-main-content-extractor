
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Janka Bernáthová
                                        &gt;
                Nezaradené
                     
                 Veľkonočný koncert Martina Babjaka 

        
            
                                    6.4.2010
            o
            19:31
                        (upravené
                6.4.2010
                o
                19:59)
                        |
            Karma článku:
                6.54
            |
            Prečítané 
            759-krát
                    
         
     
         
             

                 
                    Nezabudnuteľný spevácky ,,toreador"v Banskej Štiavnici
                 

                 
  
   Veľkonočné sviatky vyvrcholili veľkolepou oslavou radosti zo života. Radosti, ktorá si našla  miesto v ľudských srdciach a aj vďaka skvelému umeleckému zážitku z koncertu svetoznámeho  barytonistu Martina Babjaka a klavírneho virtuóza Daniela Buranovského, učinila 5.apríl 2010 pre mnohých obyvateľov i návštevníkov Banskej Štiavnice ešte výnimočnejším.   Životodárne kvapky jarného dažďa boli tichými spoločníkmi krokov, smerujúcich k Evanjelickému chrámu, ktorý sa pred 16.hodinou zaplnil do posledného miesta. Po privítaní a úvodných slovách vedúceho oddelenia kultúry Ing. Rastislava Marka, predstúpili pred publikum dlho očakávaní hostia. Už pri prvom potlesku očarili svojou srdečnosťou a láskavou bezprostrednosťou. Následne sa  k chrámovým klenbám vzniesli melódie troch sakrálnych skladieb Pieta Signore od Stradellu, Otčenáš od M.Š.Trnavského a Ave Maria od L.Luzziho. Hudba, ktorá svojou vznešenosťou umocňovala vnútornú radosť i pokoru nad zázrakom zmŕtvychvstania Ježiša Krista. K slovenským ľudovým tradíciám sa potom nežne priklonili tóny piesne M.Š.Trnavského Keby som bol vtáčkom a Bačovské piesne od E.Suchoňa. Sympatický spevák všetky skladby sám veľmi originálne a vtipne uviedol. Pri piesni Ja som bača veľmi starý sa dokonca pochválil, že jeho šediny sú už pravé, aj keď pôsobil veľmi sviežo a vitálne. Z malebných kútov Slovenska preniesol poslucháčov  prostredníctvom čarovných tónov a nádherne sfarbeného hlasu pod horúce talianske slnko.(Talianska hudba je Martinovi Babjakovi veľmi blízka, v r.1992 sa stal laureátom svetovej speváckej súťaže Luciana Pavarottiho.) Jeho spev  v mnohom pripomínal more. Slnečné piesočné pláže s nežným osviežujúcim vánkom, ale aj dvíhajúce sa vlny, príboje, i morské búrky, ktorým vznešené priestory chrámu, sviece, lustre i oltárna maľba, pridávali akýsi nádych osudovosti. No rozbúrené vlny sa opäť utíšili do láskyplnej melódie známej filmovej piesne Mama, pri ktorej  v nejednom oku zvlhla spomienka. Veľké spevácke a herecké majstrovstvo Martina Babjaka sa akoby  zhlboka nadýchlo pri áriách svetoznámych opier. V perfektnej ruštine odznela ária z Čajkovského Pikovej dámy, ktorú vystriedal radostný, až komediálny Figaro  Rossiniho Barbiera zo Sevilly, no smelo si dovolím  tvrdiť, že banskoštiavnické publikum si úplne podmanil odvážny a  temperamentný toreador z Bizetovej opery Carmen v jeho jedinečnom podaní.(Postava Escamilla  mu v r.1995 priniesla jednoznačné víťazstvo v medzinárodnej speváckej súťaži Totti dal Monte,Tal.) Úprimný obdiv a vďačnosť publika sa odzrkadľovali v gradujúcom potlesku a obaja umelci naň reagovali vtipne, s neskrývanou radosťou a uznanlivými pohľadmi na hodinky. ,,Vy ste takí zlatí, že by sme si vás najradšej vozili so sebou autobusom" poznamenal sympatický spevák. Neodpustil si dokonca hneď niekoľko vyznaní nášmu krásnemu mestu, samozrejme s milou žartovnou poznámkou, že tu sa chodí len hore alebo dole, a ísť rovno za niekým by bol asi problém. Diváci v prvých radoch pri tom asi neprehliadli figliarske vejáriky drobných vrások v kútikoch očí, ktoré sa smiali spolu s ním a veľmi mu pristali. Martin Babjak štedro rozdáva životný optimizmus plným priehrštím. Po veselej muzikálovej piesni Ja sa dnes dopoludnia žením a Piesni tuláka od R. Piskáčka nasledovala krátka, a tak trochu neobratne predstieraná, rozlúčka. ,,Keďže sme s úspechom rátali," poznamenali skromne umelci ,,pripravili sme si pre vás jeden regulérne vytlieskaný prídavok." Jeden len z toho dôvodu, lebo sa vraj nepatrí byť dlho.,,Umelec by mal vyčerpať tému a nie publikum." A tak symbolickú záverečnú bodku za mimoriadne vydareným podujatím ,,vypálila" ohnivá Granada od Augustína Laru. Diváci stáli a nadšene tlieskali v jej rytme. Nezabudnuteľný spevácky ,,toreador" Martin Babjak  na rozlúčku ešte s úsmevom zamával kyticou ohnivočervených ruží.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Bernáthová 
                                        
                                            Tváre našich dní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Bernáthová 
                                        
                                            Svitanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Janka Bernáthová 
                                        
                                            Lekárska správa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Bernáthová 
                                        
                                            Slnečná sonáta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Janka Bernáthová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Janka Bernáthová
            
         
        bernathova.blog.sme.sk (rss)
         
                                     
     
         Sme darom svetla, ktoré rozdávame, 

 aj ktorého sme odrazom... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    211
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    502
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Myslím, že už približne tuším, koľkí z vás by mi mohli raz odpustiť
                     
                                                         
                       Iba dozrievam
                     
                                                         
                       Svitanie
                     
                                                         
                       Zbytočný
                     
                                                         
                       Holuby
                     
                                                         
                       Lekárska správa
                     
                                                         
                       Po stopách básne
                     
                                                         
                       Cap á l’Est 2014 s vášnivým ohňom flamenga
                     
                                                         
                       Stáva sa
                     
                                                         
                       Moje túžby sú motýle
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




