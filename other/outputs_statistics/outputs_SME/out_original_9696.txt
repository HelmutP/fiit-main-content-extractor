
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Fulajtárová
                                        &gt;
                Rozhodnuté
                     
                 Obyčajní ľudia... to sú akí? 

        
            
                                    27.6.2010
            o
            11:45
                        (upravené
                27.6.2010
                o
                12:20)
                        |
            Karma článku:
                3.84
            |
            Prečítané 
            531-krát
                    
         
     
         
             

                 
                    Dlho, celé roky som si myslela, že sú to takí istí ľudia, ako som ja, resp. celá moja rodina. Ľudia, ktorí celé roky chodia do práce, keď chcete aj podnikajú, zarábajú pre seba, žijú tak, ako sa im darí - niekedy lepšie, inokedy menej dobre. Ľudia s normálnymi problémami, ktorých občas trápi choroba, neúspech, nepochopenie či dokonca aj závisť iných. Skrátka, obyčajní ľudia. Takto vnímam seba, mojich blízkych, susedov, ale aj tých ostatných mne trochu blízkych z komunálnej politiky, ďalej lekárov, učiteľov a rôznych iných, s ktorými skoro denne prichádzam do styku. Skrátka, obyčajní ľudia!
                 

                     Tak nejak som začala vnímať aktivity a prezentáciu skupiny tých, ktorí si prepožičali názov svojej malej a novorodiacej organizácie a začali vystupovať ako celkom Obyčajní ľudia. To „celkom" som vnímala zo začiatku úplne prirodzene, tak, ako seba. Celkom, či úplne obyčajní ľudia, toto pomenovanie skoro v každom z nás evokuje myslenie tej najväčšej skupiny ľudí, ktorí žijú tak, ako ich poznáme celé roky.   Obyčajní ľudia však v sebe nesú rokmi nadobudnuté poznanie, že nech robia čokoľvek, ťažko zmenia chod spoločnosti, sú preto obyčajní a takí, ako som ja a nemajú potrebu o tom, že sú celkom obyčajní, rozprávať takmer rok ostatným cez média. Kto túto potrebu má a hlavne má na to možnosti, teraz naozaj myslím tie finančné, lebo je to v podstate reklama, už nie je obyčajným človekom, ale... oni si hovoria občianske hnutie. Je to už čosi organizované, skupina ľudí s jasným cieľom dosiahnúť v živote niečo, po čom asi celé roky túžili. Byť ktosi, kýmsi a nie iba obyčajnými ľuďmi. A podarilo sa! Dosiahli svoj cieľ, ku ktorému im pomohli mnohí, ktorí si mysleli, že naozaj navždy zostanú obyčajnými ľuďmi.   Dnes, keď sú už reprezentanti istej parlamentnej strany a svoje dosiahli, ich rétorika sa začína rozchádzať s ich predvolebnými úvahami a myšlienkami. V čom sú iní, ako predtým, to by bolo na dlho. Mňa však akosi oslovilo ich tvrdenie, že prečo sa majú vymieňať všetci ľudia na vedúcich postoch, len preto, že sú členmi predchádzajúcej vládnej koalície. Ale vtedy tvrdili, že všetci tí, ktorí rozhodujú - ja som si to spájala s akýmkoľvek miestom či už v štátnej správe alebo v samospráve, sú jednoducho manipulovaní myslením svojich straníckych lídrov. A dnes? Vraj je to individuálne a nemusí to tak byť. Vtedy to nevedeli, že nie každý je predovšetkým zástupca politickej strany, že v prípade aj ak áno, že môže byť aj dobrým odborníkom? Akože na to prišli až teraz?   Druhým problémom, ktorý ma v ich občianskom hnutí oslovil, bolo, že vedeli, s akou stranou sa spolčili a v prípade, že uspejú (o čom boli presvedčení), budú musieť ľuďom vysvetľovať, že prečo práve táto politická strana. Že by to bolo práve preto, lebo táto strana sľubovala mladým neskúseným voličom „ich modré z neba" a tým dlho „utláčaným" kus „ľudskej slobody" podľa ich myslenia. Súdný volič, ktorý pozná pomery na Slovensku, musel dopredu vedieť a počítať s tým, že ani jeden z daných sľubov táto strana nesplní. Dokonca ani nemala záujem v koalícii o týchto predvolebných sľuboch rokovať, vyjednávať.   Vraj aj toto sú problémy obyčajných ľudí, ktorí žijú v našej spoločnosti a z ktorých daní bude platená aj táto strana. Ale títo ľudia sú obyčajní ľudia a nie Obyčajní ľudia, teda nezávislé občianske hnutie.   Najväčším problémom daného hnutia, ktoré mi rezonuje v hlave a akosi sa ho neviem zbaviť, je že jeden z lídrov tohto hnutia pán Matovič má ponuku predať sa za 20 miliónov €. Ak je táto informácia pravdivá a nie je to iba ďalšia ich kampaň, mali na dané osoby, resp. spoločnosť, podať trestné oznámenie za vydieranie a hlavne za snahu narušiť politickú situáciu v našej krajine. Ak je to pravda.?Ak pán Matovič to odmieta a tvrdí, že to bola iba žart, resp. neviem čo iné, je možné, že si to iba vymyslel, alebo bol zneužitý vraj dobrým kamarátom, a potom...  Alebo? Že by to bol iba začiatok ich ďaľších rozhodnutí? Pán Matovič prostredníctvom tlače celý rok ubezpečoval svojich potencionálnych voličov, že ak sa oni dostanú do parlamentu a nebude to v našej spoločnosti tak, ako si oni predstavujú, resp. tak, ako to oni sľúbili, nebudú podporovať hlasovanie v parlamente a v krajnom prípade odídu z koalície. To by však malo za následok nové predčasné parlamentné voľby a ... celý ten cirkus by sa mohol začať  odznova.   Naozaj nechcem nikoho obviňovať a súdiť. Ale matematika nepustí. Ak je naozaj pravdou, čo zverejnil pán Matovič, že ktosi im ponúka sumu 20 ... , je to delené štyrmi veľmi slušná suma pre jednotlivca. A možno, aj keď nie jemu osobne, možno by to hnutiu ako takému stálo za porozmýšľanie, či túto ponuku neprijať. Určite to nebude tak, že povedia sebe i národu, že sa predali, ale... postupom času, určite nie hneď, sa budú čoraz viac odvolávať na svoje svedomie, že nemôžu podporovať rozhodnutia svojich koaličných partnerov a po nejakom čase sa rozhodnú rezignovať na post poslanca vládnej koalície. Dokonca, v očiach tých, pre ktorých vznikli, sa stanú hrdinovia, a obyčajní ľudia budú z ich rozhodnutím súhlasiť. Ja, obyčajný človek, mám však obavu, že sa to môže stať a ... že sme znovu komusi naleteli. Komusi, komu vôbec nešlo o obecné blaho nás obyčajných ľudí, ale o to, aby si ktosi zase v živote polepšil.   Ak títo obyčajní ľudia majú v budúcnosti tendenciu nesúhlasiť s vyjadrením vlády a v istom okamihu seknúť so spoluprácou, prosím, nech tak urobia čo najskôr, nech sa vzdajú mandátu, aby na ich miesto mohli nastúpiť náhradníci. V prípade, že tak plánujú urobiť neskôr, už im nikto v budúcnosti nebude veriť, že sa nedali kúpiť.   Ja osobne sa začínam báť ľudí, ktorí tvrdia, že čo oni zmôžu, lebo sú len úplne obyčajní ľudia a na ich mienke predsa nezáleží.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            ... a osoba invalidná :  - časť II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Osoba zdravotne ťažko postihnutá a osoba invalidná :  - časť I.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Práca: právo alebo povinnosť? - časť II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            P r á c a :  právo alebo povinnosť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fulajtárová 
                                        
                                            Pracovať aj napriek ochoreniu  (psychické)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Fulajtárová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Fulajtárová
            
         
        fulajtarova.blog.sme.sk (rss)
         
                                     
     
        Vraj som filantrop, sociálne orientovaná a nadovšetko si cením možnosť slobodne sa rozhodnúť ...

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    99
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1129
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ... čo sa ma bytostne dotýka
                        
                     
                                     
                        
                            Kniha a ja
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            2- (dva mínus)
                        
                     
                                     
                        
                            Rozhodnuté
                        
                     
                                     
                        
                            ŠŤASTIE SI TY
                        
                     
                                     
                        
                            Pacientska advokácia/dôverníct
                        
                     
                                     
                        
                            Obyčajné, ale s pomlčkou
                        
                     
                                     
                        
                            môžem? Môžem, som naša Vikinka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Koruny alebo eurá?
                                     
                                                                             
                                            Žalujem reklamu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dominik Dán:  V š e t k o
                                     
                                                                             
                                            Štefan Klein: Vzorec šťastia
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Slovenské fórum
                                     
                                                                             
                                            HAV Internetové noviny
                                     
                                                                             
                                            Psia duša
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ester
                     
                                                         
                       Aj o tomto môže byť Valentín
                     
                                                         
                       Quo vadis, Cirkev?
                     
                                                         
                       Chlad láske nesvedčí
                     
                                                         
                       Jemný dotyk so smrťou
                     
                                                         
                       Ľúbime ťa, prepáč ...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




