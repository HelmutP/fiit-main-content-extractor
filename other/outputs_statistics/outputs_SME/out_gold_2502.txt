

 Teta Belousovová dáva trestné oznámenie na redaktora SME Tomáša Hudáka za jeho vtipkovanie o trojfarebných prikrývkach a anténach zapichnutých v trojkopčekovej šmolkovskej zmrzline, ktorým reagoval na schválenie zákona na podporu vlastenectva. Nie je jasné, akého trestného činu sa mal chudák Hudák podľa pani exučiteľky dopustiť. Ako jediný snáď prichádza do úvahy trestný čin výtržníctva podľa § 364 Trestného zákona: 
 "(1) Kto sa dopustí slovne alebo fyzicky, verejne alebo na mieste verejnosti prístupnom hrubej neslušnosti alebo výtržnosti najmä tým, že 
 a) napadne iného, 
 b) hanobí štátny symbol, 
 c) hanobí historickú alebo kultúrnu pamiatku, 
 d) hrubým spôsobom ruší zhromaždenie občanov alebo priebeh športového alebo kultúrneho podujatia, alebo 
 e) vyvoláva verejné pohoršenie vykonávaním pohlavného styku alebo vykonávaním sexuálneho exhibicionizmu alebo iných patologických sexuálnych praktík, 
 potrestá sa odňatím slobody až na tri roky." 
 Je Hudák výtržník? Nezmysel. Akej hrubej neslušnosti alebo výtržnosti sa, preboha, dopustil? Že žartoval o hlúpom zákone z dielne dvojice poslancov, ktorí sa dokážu podpísať jednou a tou istou rukou? To snáď nie. Veď špičky SNS vyvolávajú verejné pohoršenie verejným vykonávaním svojej nacionalistickej politiky, nástenkovými tendrami, emisiami a ďalšími patologickými praktikami, ale z výtržníctva ich ešte zatiaľ nikto neobvinil. Na Slovensku je však možné všeličo. Preto by som pre istotu chcel k celej veci povedať sám za seba ešte toto: 
 Slovenská vláda sa nám tento rok postará asi o jeden z najpamätnejších prvých aprílov v histórii našej zvrchovanosti. Štátne školy totiž zaplaví čudnými trojfarebnými prikrývkami, portrétmi bielej antény zapichnutej v trojkopčekovej šmolkovskej zmrzline a kópiou dosť nepodarenej básničky o zlom počasí nad Tatrami. Tento vtip však nemá len pobaviť. Má najmä prebudiť a obrodiť. 
 Veď, ale úprimne, čo už môže byť väčší pocit slasti, ako môcť si počas vyučovania vypočuť tú nášmu srdcu najbližšiu pieseň. Akurát školáčikovia si možno povzdychnú, že prečo je taká krátka. Veď keby bol kedysi Janko Matuška pripísal trošku viac tých strof, mohli by počúvaním hymny zabiť aj celú prvú hodinu. 
 Máme však aj trochu obavy. V Guantaname totiž zvykli práve týmto spôsobom, teda púšťaním otravnej hudby mučiť väzňov. No a ktosi kdesi vyrátal, že priemerný žiak, kým opustí brány tých základných školských zariadení, bude si musieť vypočuť našu hymnu najmenej šesťstokrát. 
 Predchádzajúce tri odstavce sú doslovným prepisom toho, čo povedal Tomáš Hudák a čo tak strašne pohoršilo Annu Belousovovú. Ak sa tým dopustil Tomáš Hudák akéhokoľvek trestného činu, tak som sa ho práve publikovaním tých istých slov pod mojím vlastným menom dopustil tiež. A teda orgány činné v trestnom konaní by mali voči mne v tejto veci postupovať rovnako ako voči Hudákovi. 
 Nejde o žiadny trestný čin, ale sicher je sicher. Zavrieť Hudáka by bola škoda, ale v tej base by s ním mohla byť sranda. Nemohol som odolať. 

