
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Nerob si starosti 

        
            
                                    30.5.2010
            o
            6:25
                        |
            Karma článku:
                8.79
            |
            Prečítané 
            2009-krát
                    
         
     
         
             

                 
                    Nuž neviem, milí moji blogeri, či vám tieto slová voľačo hovoria, ale ja som si ich v sobotu ráno zaspieval s náramnou radosťou. Aby ste vedeli, má to takéto pokračovanie – nerob si starosti Miško, nie si ešte starý pán a naozaj sa mi všetko vydarilo tak, že to bolo zaslúžené a veľmi príjemné. Ako sa hovorí, zdvihlo mi to náladu až k nebíčku a to už je čo povedať. A všetko sa začalo celkom nenápadne a zničoho nič, lebo ešte ani vstávanie neveštilo nič mimoriadne. To viete, ak začína zaslúžilý dôchodca deň desaťminútovou rozcvičkou, aby mu ako sa hovorí „neopadli svaly“ a tridsiatkou kĺzavých podporov s izorolom, aby tie roky ľahko uniesol, ani sa nie je na čo tešiť, pravda? To až potom, keď som sa v kuchyni postavil pred chladničku a rozmýšľal, či začnem deň jogurtom, samozrejme nízkotučným, alebo si pôjdem kúpiť to senzačné celozrnné pečivo ku ktorému si dám šunku a syr a k tomu kávu z „aparátu“, ako volám ten maďarský zázrak v ktorom si ju varím vari dvadsať rokov, som sa náhodou pozrel z okna. A to bol začiatok!
                 

                     Svietilo slniečko, ešte nízko, lebo bolo pred šiestou a podo mnou po chodníku kráčalo úžasné stvorenie. Strojné, štíhle, s úžasnými nôžkami a verte mi, nebol to pohľad z veľkej diaľky, videl som dobre, bývam iba na druhom. V svetlom plášti, prepásanom naozaj pevne, aby vynikal tenučký driek a  úžasné boky, chôdza ako po prehliadkovom móle voľakde na prehliadke v Paríži či v Miláne a všetkému dominovala pyšná hlávka s hrivou iskrivých medených vlasov. Samozrejme dlhých, aby bol výsledný efekt čo najúžasnejší! Nič nebolo samozrejmejšie, ako oblok otvoriť, akože vetrám a hľadieť s nemým úžasom ako sa ponúka na obdiv aj z iného uhla môjho pohľadu. Samozrejme, bolo to anonymné, nemala o mne ani potuchy a keď začala odomykať trafiku, čo je búdka klasického formátu kúsok pod mojimi oknami, celkom som zdrevenel. Bože, veď je to naša trafikantka!   Šokujúce zistenie, veď sa s ňou vídam deň čo deň, vždy takto ráno kupujem SME a občas aj žuvačky, voľačo pekné jej poviem, ale v tej zle osvetlenej kutici ju vlastne ani nie je vidieť. Ba ani tie vlasy som si nikdy nevšimol, hoci dnes ráno akurát tie dominovali celej ulici. Po chvíli sa vynorila, začala snímať tie plechové kryty na výklade a veru aj bez toho elegantného plášťa bola pastvou pre oči. Chvíľočku neskôr, tak ako inokedy, som do okienka nazrel s očakávaním a celkom napätý som ju pozdravil. Okamžite mi podávala noviny, pripomenula mi povinnosť zaplatiť 55 centov a neodolal som pokušeniu oprášiť trošku starých zvykov.   Dnes ráno, hovorím, som videl nádhernú ženu kráčať pod našimi oknami, v úžasnom plášti a s ešte úžasnejšou hrivou vlasov, svietilo na ňu slniečko, ako keby bola na javisku v hlavnej úlohe a bol som si istý, že je to prinajmenej voľajaká riaditeľka či vysokopostavená úradníčka. A potom ste vliezli do tejto búdky a boli ste to vy! Nazrel som dnu celkom zvedavo, ba som sa do toho okienka tak trošku až vopchal, ale bol som sklamaný. Zlé svetlo, skôr šero, ani tá žiarivá plamenná hriva nesvietila a s prekvapením v očkách neznámej farby na mňa hľadela tvárička s celkom milým úsmevom.   Naozaj, zmohla sa na otázku a tak som smelo pokračoval. Naozaj, nebol by som vás spoznal, jednoducho ste boli presne tým zjavom, čo vzkriesi aj starého chlapa, preberie ho z letargie a dovolí mu pripomenúť si, čo všetko krásne na tomto svete je. Zasmiala sa, nahlas, ako sa hovorí hrkútavo a z hĺbky tej búdky sa ozvalo nástojčivé – počkajte! A vzápätí vyšla von. Strojná, naozaj, tie vlasy úžasné, zrejme čerstvý kaderník či vlastný kumšt s dobrou farbou, jemný mejkap, oči zelené ako jablká sorty golden a postavička napriek jasnej zrelosti ako lusk. Bez váhania ma zdrapla, pobozkala, obomi rukami mi potuľkala moju takmer dohola vystrihanú hlavu a dostal som druhý bozk. Pekný, naozajstný, štedrý a potom ako tringelt aj vyznanie.   Bože, akú ste mi urobili radosť! Velikánsku, mám chuť žiť, dnes mi bude krásne! Dych mi síce nevyrazilo, ale pri dlhšej vete by som sa bol zakoktal, tak som iba lakonicky jej chválu opätoval - milá pani, vy mne vari ešte väčšiu. Bozkávam, pozdravil som sa tak ako inokedy, no ako vidíte bolo to celkom mimoriadne, naozaj senzačné ráno. Potešte sa aj vy, svet je občas naozaj štedrý.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




