
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Tauber
                                        &gt;
                Nezaradené
                     
                 4 geniálne správy 

        
            
                                    13.6.2010
            o
            8:52
                        (upravené
                13.6.2010
                o
                9:30)
                        |
            Karma článku:
                14.72
            |
            Prečítané 
            2066-krát
                    
         
     
         
             

                 
                    Že je zmierlivá? Schopnosť dohodnúť sa a nehulákať na všetkých je plus, ak to nezachádza za isté hranice. Že je žena je tiež plus. Že afektuje? Každý má nejakú chybu. Možno s tým prestane, keď bude na premiérskom poste a nadobudne istotu a sebadôveru.
                 

                 1) HZDS mimo parlamentu. Mečiar skončil. Ten Mečiar, ktorý tu bol dlhé roky najväčším postrachom, veľkým čávom, ktorý chcel vystrkať všetkých čo sa mu odmietli klaňať za hranice, do basy, ktorý podľa viacerých indícií (nepáči sa mi táto prezumpcia neviny) dal zabiť Remiáša, podľa zase iných indícií stál za akciami Neutrón či Dežo, ktorých cieľom bolo okrem iného postaviť proti sebe bielu väčšinu a rómsku menšinu v stredenej Európe. Ten Mečiar, ktorého sme sa ešte pred pár rokmi všetci istým spôsobom báli. Zrazu je z neho nikto. Bezvýznamný nikto. Veľmi dobrý pocit.  2) SNS len s odretými ušami v parlamente. Jasné že ešte lepšie by bolo jačavú Anču stratiť už naveky z dohľadu úplne, ale ani toto nie je zlé. Keď si pozrieme výsledky extrémistických, nacionalistických strán v iných európskych krajinách, aj v tých akože západných, musíme vidieť že táto forma politického angažovania sa je na vzostupe. Z tohto hľadiska takýto slabý výsledok pre SNS je malý zázrak. O bod viac pre Slovensko. Áno, tá podpora sa len presunula k Smeru, ktorý vie SNS-ákov v mnohých veciach dokonalým spôsobom zastúpiť, ba posunúť to ešte ďalej. Ale ten Smer podľa všetkého nezostaví vládu, a ak sa nestane nič zlé, bude prinajmenšom 4 roky odstavený od moci. Keby Pánboh dal že by aj dlhšie.  3) Úspech Mostu, Béla na výslní, s fajn ľuďmi typu Švejnar, Nagy, atď okolo seba. A úplne najviac, vykrúžkovanie OKS-ákov na popredné miesta. Ľudia, ktorých už všetci odpisovali, že to sú tí akože namyslení fundamentalisti, Česi by povedali pravoláskaři, inými slovami zadubení idealistickí priaznivci Novembra 89 a Havla, čo je na Slovensku dlhodobo nepriateľ číslo 1 - Zajac, Šebej, Dostál: všetci v parlamente, veľká radosť. Nie nadarmo práve do nich hneď na prvej povolebnej tlačovke skákal Fico - práve oni sú presný protipól k jeho vypočítavej odludštenej politike, ktorej ide len o naháňanie percent.  4) A nakoniec pre mňa úplne najlepšia správa: Strana, ktorú volím odkedy je na svete, a ktorú s výnimkou zopár Dzurindových čudných výpadov typu skupinka či besnenie v otázke Kosova žerem všetkými desiatimi, je faktickým víťazom volieb, podľa všetkého bude zostavovať vládu a bude mať v nej najsilnejšie slovo. Tá strana, do ktorej som sa pred týmito voľbami úplne zamiloval, pretože štyri z prvých piatich miest na kandidátke obsadila ľuďmi ktorých si hlboko vážim. Tá strana pre mňa vôbec nebola menším zlom, ale dobrom, ktoré som podporil z hĺbky svojho presvedčenia. A v prvom rade, Radičová, do ktorej sa už 3 roky všetci navážajú, že nebude dobrá prezidentka, premiérka, predsedníčka strany. Prečo by nebola? Že je zmierlivá? Schopnosť dohodnúť sa a nehulákať na všetkých je plus, ak to nezachádza za isté hranice. Že je žena je tiež plus. Že afektuje? Každý má nejakú chybu. Možno s tým prestane, keď bude na premiérskom poste a nadobudne istotu a sebadôveru. Vo chvíľach, keď ju cíti, býva úplne iná - afektovanie je razom preč, je milá, prirodzená, a dokáže dať veľmi elegantným spôsobom do poriadku úplne každého, a ešte tak, aby sa nikto neurazil. Tak ako pred pár hodinami, keď stopla (po sebe druhú) najvýznamnejšiu persónu povolebných vyjednávaní, ktorej sa ktovieprečo zatúľali myšlienky až k Johnnymu Cashovi: V podľa mňa pamätnej vete "Riško, ty drž balónik a čakaj" nebolo v tej chvíli ani trochu nadutosti, nadradenosti. Len ukážka, ako by sa dal v krajine spraviť poriadok trochu iným, ženským? spôsobom. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Tauber 
                                        
                                            Jak malé deti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Tauber 
                                        
                                            Dzurinda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Tauber 
                                        
                                            Alojz!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Tauber 
                                        
                                            Havel, Fico a Marián Varga
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Tauber 
                                        
                                            Díky Iveta, Lucia &amp; spol:-)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Tauber
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Tauber
            
         
        tomastauber.blog.sme.sk (rss)
         
                                     
     
        Normálny človek, aspoň niekedy
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            chuck palahniuk: strašidla
                                     
                                                                             
                                            pavol rankov: stalo sa prvého septembra
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            n.o.h.a.
                                     
                                                                             
                                            kosa z nosa
                                     
                                                                             
                                            deadline
                                     
                                                                             
                                            radio 1
                                     
                                                                             
                                            radio nova
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




