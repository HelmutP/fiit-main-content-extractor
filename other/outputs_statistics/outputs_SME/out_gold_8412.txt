

 Premýšlanie bolí. Ľudia sa ľahšie rozhodujú, ak môžu niečo porovnať. Nevieme, koľko by malo stáť auto, ale vieme, že to so silnejším motorom by malo byť drahšie. Je dokázané, že keď dáte ľuďom na výber z troch vecí, vyberú si prostrednú (z pohľadu parametrov, ceny..) 
 Aká výška platu je uspokojivá? Niektorí ľudia sú spokojní s platom, kým nezistia, že ich kolega zarába viac. Alebo, ako raz  poznamenal H.L.Mencken: Muž je so svojím platom spokojný vtedy, keď zarába viac ako manžel sestry jeho manželky. Väčšina ľudí nevie čo chce, kým to nevidí v kontexte s inými vecami. 
 Ľudia si často svoj výber zjednodušujú. Pekný príklad uviedol Dan Ariely [1], ja ho trochu zmením. Predstavte si že máte na výber tri možnosti predplatného SME (ponuka je fiktívna a vymyslaná): 
 1) Internetová verzia: 59€ 
 2) Papierová verzia: 125€ 
 3) Internetová a papierová verzia: 125€ 
 Na prvý pohľad to vyzerá ako omyl. Kto by si kupoval papierovú verziu, keď za rovnakú cenu môže mať aj internetovú? Odpoveď: Nikto. Ponuka v strede slúži ako chyták-návnada. Ťažko sa zákazníkovy rozhoduje, či si objednať internetovú alebo papierovú verziu. Je ťažko porovnateľné, čo je výhodnejšie. Posledné dve ponuky sa porovnávajú naopak veľmi ľahko. Výskum ukázal, že zákazník si väčšinou zvolí ľahšiu možnosť rozmýšlania: odstráni prvú možnosť a rozhoduje sa medzi dvomi ľahko porovnateľnými. Tak si zvolí v tomto prípade tretiu možnosť. Navyše to vyzerá tak, že dostane internetovú verziu v podstate zadarmo. A nie je pre zákazníka nič lepšie, ako dostať niečo zadarmo. Podľa prieskumu si zvolilo možnosť 
 1) 16 ľudí 
 2) nikto 
 3) 84 ľudí. 
 Čo sa však stane ak z ponuky odstránime možnosť 2 (ktorú aj tak v prvom prípade nikto nevyužil)? Výsledky sú odlišné: 
 1)  68 ľudí 
 2) nikto 
 3)  32 ľudí 
 Možnosť 2) nám slúži teda iba ako "návnada", ak chceme, aby si viac ľudí objednalo drahšiu, papierovú verziu. 
 Ako ďaľší príklad nám poslúžia cestovné kancelárie. Máme si vybrať z dvoch možností: 
 Paríž - v cene 100€: letenka, ubytovanie, polopenzia 
 Rím - v cene 100€: letenka, ubytovanie, polopenzia 
 Ak sa vám obidve mestá páčia rovnako, je to ťažké rozhodovanie. Preto si zázazníci vyberú približne rovnako, teda Paríž vs. Rím 50:50. Čo sa ale stane, ak použijeme návnadu?: 
 Paríž - v cene 100€: letenka, ubytovanie,  polopenzia 
 Rím - v cene 100€: letenka, ubytovanie,  polpenzia 
 Rím - v cene 110€: letenka, ubytovanie,  plná penzia 
 Je ťažké objektívne posúdiť, či je výhodnejší Paríž alebo Rím. Ale ak máte za približne rovnakú sumu namiesto polpenzie plnú penziu, znie to lákavo. Preto ,aj keď si v tomto prípade nikto neobjedná druhú možnosť, zvýši sa nám pomer predaných zájazdov v prospech Ríma. 
 Aby sme neostali len v rovine peňazí a ekonomiky: systém "návnady" a porovnávania funguje napríklad aj pri výbere partnerov. Ak sa má muž rozhodnúť podľa krásy medzi tromi ženami, všetky sú relatívne pekné a dve sa podobajú, tak je dosť veľká pravdepodobnosť, že si vyberie krajšiu z dvoc podobných. 
 Keď si v prieskume ženy vyberali z dvoch sympatických mužov, výsledky dopadly približne v pomere 50:50. Keď však dali k jednému z týchto dvoch mužov niekoho podobného, ale napríklad s väčším nosom, pomer v prospech tohto muža sa rapídne zvýšil. 
   
 Iracionálnym a emotívnym správaním a vplyvom psychológie v ekonómii sa zaoberá behaviorálna ekonómia. 
   
   
 [1] Dan Ariely: Jak drahé je zdarma?
    
 
   
   

