
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dávid Králik
                                        &gt;
                Etika u mňa v triede :o)
                     
                 Otravná alergia 

        
            
                                    24.5.2010
            o
            15:15
                        (upravené
                24.5.2010
                o
                20:02)
                        |
            Karma článku:
                11.50
            |
            Prečítané 
            1610-krát
                    
         
     
         
             

                 
                    Alergia je riadna pliaga. Len tak, z ničoho nič máte plný nos, ba aj oči sa vám začervenajú a začnú slziť...
                 

                 Je piatok a teda je i etika...   Najskôr si každý z nás obkreslil ruku na čistý papier a podpísal sa na ňu. Potom začali „ruky“ kolovať po triede. Decká dopísali na každú "ruku" niečo o jej majiteľovi a poslali ju ďalej. Po chvíli, keď "ruky" prešli celé kolo, mal každý pred sebou znova "svoju ruku". Teraz už celú popísanú. Niektoré pomenovania boli značne nelichotivé a bolo zjavné, že čítanie ich obsahu bolo deckám nepríjemné. Aby bolo dielo skazy dokonané, rozdal som deckám papieriky (tie som vyrábal ja :O), kde som napísal mierne urážlivé básničky na adresu každého z nich. Teraz sa dostali do varu. Cítili sa urazené a nahnevané. Rýchlo som zasiahol. Ospravedlnil som sa s vysvetlením, že básničky som nemyslel vážne.   „Ale prečo si ich teda napísal?“ Nedávno som si všimol, že sa u mňa v triede rozmohlo vážne ochorenie, ktorým striedavo trpia temer všetci žiaci mojej triedy. Posmievanie. „Asi som chcel, aby ste si nabudúce, až sa budete niekomu posmievať, spomenuli, ako ste sa dnes cítili. Snáď si potom ľahšie predstavíte, ako sa v danom momente cíti ten, do koho sa navážate."     „Teraz si zopakujeme aktivitu s písaním do obkreslenej ruky, no keď budete vpisovať čosi na adresu svojho spolužiaka, myslite na to, ako ste sa cítili pred chvíľou...myslite na to, ako sa bude cítiť váš spolužiak, keď to bude čítať...“ Zopakovali sme a k môjmu potešeniu bola väčšina reakcií tentokrát ladená pozitívne, poprípade neutrálne. Nadávky a urážky sa takmer nevyskytli..Decká si potešene prečítali reakcie spolužiakov na svoju osobu, v triede bola družná atmosféra a všetci boli veselí...   ...vlastne všetci nie...všimol som si malého šušníka smutne stojaceho bokom s nakreslenou a popísanou rukou. Pristúpil som k nemu. „Čo sa stalo?“ „Ja som mu napísal – kamoš - a on mi napísal na ruku nadávku a ...“ vyhrkli mu slzy a hodil sa mi do náručia.  Aby ste mi rozumeli, mám decká rád, ale na takéto ťuťuli muťuli ma moc neužije. Každý má predsa svoju rolu, ja zastávam mužský princíp, na maminkovské pritúlenia sa v škole nájde pre decká dostatok kolegýň. Prekvapene som ho pohladil po hlave. Ktosi k nám pristúpil. „Dávid, ty plačeš?“ „Ja, nie. To je alergia...“        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Redakcia SME sa musela zblázniť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            To najlepšie nakoniec...už dnes
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Ignoranti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Čo už by mi len mohol ponúknuť prepážkový zamestnanec Tatra Banky?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Možno máte dieťa vo veku 9 až 99 rokov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dávid Králik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dávid Králik
            
         
        davidkralik.blog.sme.sk (rss)
         
                                     
     
        Bývalý (m)učiteľ na I. stupni ZŠ, ktorý sa platonicky priatelí s Jonathanom Livingstonom, so Siddhárthom a hobitmi. Znamenie vodnára ho predurčuje k uletenosti všetkými smermi - hudba, literatúra, šport ...  




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    930
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3168
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja malá fikcia
                        
                     
                                     
                        
                            Viera ako ju cítim
                        
                     
                                     
                        
                            Aj také sa mi stalo
                        
                     
                                     
                        
                            Moje malé postrehy
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Život je krásny
                        
                     
                                     
                        
                            Malé gramatické okienko
                        
                     
                                     
                        
                            Logopedické okienko
                        
                     
                                     
                        
                            Rozprávky (aj) pre dospelých
                        
                     
                                     
                        
                            Milovaťžofiu
                        
                     
                                     
                        
                            Nedeľná ... veď viete : o )))
                        
                     
                                     
                        
                            Biblia očami laika : )
                        
                     
                                     
                        
                            ...cestou ma vanie...
                        
                     
                                     
                        
                            šerm
                        
                     
                                     
                        
                            moje hudobné výlevy
                        
                     
                                     
                        
                            Etika u mňa v triede :o)
                        
                     
                                     
                        
                            Moja poviedkovňa
                        
                     
                                     
                        
                            Pracujem ako divý :o)
                        
                     
                                     
                        
                            baška
                        
                     
                                     
                        
                            (pre mňa) zaujímaví ľudia
                        
                     
                                     
                        
                            na slovíčko
                        
                     
                                     
                        
                            Nápadník
                        
                     
                                     
                        
                            Súťaž krátkych videí
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Veľmi (ne)chutná hmyzia knižka
                                     
                                                                             
                                            Niektoré rany sú príliš hlboké a nikdy sa skutočne nezahoja
                                     
                                                                             
                                            (pre mňa) Môj najlepší rozhovor
                                     
                                                                             
                                            Logopédia už aj vo vašej škôlke? Prečo nie :)
                                     
                                                                             
                                            Have a nice Day(vid:)
                                     
                                                                             
                                            Kam odchádzam zo školstva
                                     
                                                                             
                                            Chceli by ste ísť do neba, potom vedzte...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Najlepšia logopédka v Rači/ na svete
                                     
                                                                             
                                            FELIX
                                     
                                                                             
                                            iní
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




