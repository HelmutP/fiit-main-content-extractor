
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Slavomír Repka
                                        &gt;
                osobný názor
                     
                 Štyri možnosti po voľbách 

        
            
                                    7.6.2010
            o
            11:43
                        (upravené
                6.6.2010
                o
                19:17)
                        |
            Karma článku:
                13.12
            |
            Prečítané 
            1541-krát
                    
         
     
         
             

                 
                    1. Vládu zloží Fico so staronovými kumpánmi. 2. Vládu kompletne zloží opozícia. 3. Vládu zloží Fico v súčinosti s niektorou stranou terajšej opozície. 4. Opakované voľby
                 

                 1. Ak sa zopakujú volebné čísla z roku 2006, tak to vôbec nebude dobrým vysvedčením, ale na druhej strane, ak sa na to pozrieme optimisticky, tak v tomto prípade budeme mať veľmi blízko ku Grécu :-(.   2. Ak padne táto možnosť, tak sa môžme trocha potešiť, ale musíme si uvedomiť, že sme v posledných rokoch žili veľmi nad pomery a to značí, že musíme počítať s určitými nutnými zmenami (utiahnutie opaskov na určitú dobu).   3. Ak sa niekto nájde z dnešnej opozície a dokáže kolaborovať so SMER-om, musí si byť vedomý, že je to jeho posledný politický výkrik (kolaborant je v spolku vždy iba na špinavú robotu).   4. Ak by sme sa dostali číslami na hranicu (50:50 opozícia - koalícia), tak je ešte jedna možnosť, ktorú predstavujú opakované voľby.   Každý kto chce, aby u nás nepadla možnosť číslo 4, môže pre to niečo urobiť.   Prekonať svoju lenivosť, preorganizovať svoje činnosti, dvihnúť zadok z kresla a navštíviť volebnú miestnosť dňa 12.6.2010. Nebudem nikomu hovoriť, koho má voliť a hlavne prečo. Som presvedčený, že každý zodpovedný občan má svojho favorita, prípadne podporí (podľa jeho názoru) menšie zlo.   Čo tu napíšem s plný vedomím je, že ja budem voliť SDKÚ-DS. Budem ich voliť hlavne z dôvodu, že je to "partaj", ktorá sa už osvedčila, keď nás vytiahla z bahna mečiarovského vládnutia. Som si vedomý ich chýb, pochybení, zlých rozhodnutí, ale dávam im šancu, lebo ich práca môže byť aj mojím prospechom ako občana SR.   PS. Roky tvrdím (nadsadené): Kto nebol voliť, nemá právo kecať o zlej/dobrej vláde. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (66)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Ficova kvázi demisia je bohapustá vyhrážka
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Kam smeruje slovensko s vládou SMER-u
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Vrana k vrane sadá, alebo voľby sa blížia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Stanislav Šváby - môj starosta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            S Radom prichádza do éteru nová telenovela
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Slavomír Repka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Slavomír Repka
            
         
        repka.blog.sme.sk (rss)
         
                                     
     
         Nezaujíma ma, koľko stojí liter benzínu. 
Zaujíma ma, koľko zarábam ja. 
(kurňa, ten benzín je ale drahý) 

 V roku 2005 (február) som na blogu SME začínal článkom PAŠTEKÁRI 

 Viac o mne a mojej práci nájdete na týchto stránkach 

 Pozrite si aj ODPORÚČAME.EÚ prípadne poďte sem publikovať. 

 Obývam krásnu časť zeme, nazývanú Slovensko. Bývam v dosahu hlavného mesta, ale aj hory a vodu mám blízko.  
Mimo rečou som východniar ;-), paštekár.... 

 Résumé môjho "NAJ" na blogu SME nájtete TU 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    533
                
                
                    Celková karma
                    
                                                14.56
                    
                
                
                    Priemerná čítanosť
                    2579
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z pohľadu obchodníka
                        
                     
                                     
                        
                            PRE Consulting
                        
                     
                                     
                        
                            CESTOVNÝ RUCH
                        
                     
                                     
                        
                            postrehy z ciest
                        
                     
                                     
                        
                            osobný názor
                        
                     
                                     
                        
                            pohľady
                        
                     
                                     
                        
                            Analýzy
                        
                     
                                     
                        
                            AUTO - test
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Súťaž
                                     
                                                                             
                                            ODPORÚČAME.EÚ
                                     
                                                                             
                                            Paštekári
                                     
                                                                             
                                            Jahodná
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Aplikácia
                                     
                                                                             
                                            Rezervy vo firmách
                                     
                                                                             
                                            Najlepší obchodník
                                     
                                                                             
                                            Mystery shopping
                                     
                                                                             
                                            Dobré čítanie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Špina, bezpečnosť a tichá(?) koalícia SMER - SDKÚ v Starom meste
                     
                                                         
                       Osudové okamžiky
                     
                                                         
                       Zamyslenie nad jedným Rozsudkom v mene Slovenskej republiky
                     
                                                         
                       Stanislav Šváby - môj starosta
                     
                                                         
                       Slovakia Ring 24hod: Aké to je atakovať takmer 800 kilometrov na bicykli za jediný deň?
                     
                                                         
                       Pinocchio, čo sa už nezmestí do parlamentu
                     
                                                         
                       Bratislava chce opäť zlegalizovať 400 billbordov
                     
                                                         
                       S Radom prichádza do éteru nová telenovela
                     
                                                         
                       Keď plynár má lepšiu províziu za predaj elektriny
                     
                                                         
                       Zpráva ze života malých šestiletých hominidů
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




