

 Vy ktory sledujete trosku moje prihody z prace a aj mimo viete, ze pocas minuleho a tohto roku sa premavam medzi Barcelonou a Lodynom, a to velmi velmi casto. Aj ked hrozba mojho odchodu z firmy tu je,  pretoze nasa firma sa potaca v problemoch (nasi obchodnici nie su schopny predavat nase projekty, zaujimave ze?), ja som este stale zamestnana. Tak teda, pekne dalej pokracujem v lietani tam a spat. Cez tyzden v UK cez vikend doma. 
 Vcera som vam pisala, ze lety z Anglicka boli zrusene. Dnes je situacia rovnaka. Vraj sa zacne lietat az zajtra, ale to clovek nikdy nevie. Nasa spanielska skupinka mala dnes o 18:00 letiet do Barcelony. Uz vcera sme uvazovali ostat, pretoze situacia nevyzerala prave najlepsie. Stale sme ale dufali ze je to len taky zart ktory nam priroda spravila a ze vsetko bude v poriadku ked len budeme v to verit. 
 Najhorsie na tom je, ze buduci tyzden sme mali naplanovane byt v UK tiez, let mal byt v nedelu vecer. Takze ak by sme aj mohli letiet niekedy v sobotu vecer, neboli by sme doma tak najviac 24 hodin a podla nas by bola vyslovena blbost vracat sa domov za takych podmienok. Niekedy treba uvazovat racionalne. 
 Ten moj nemecky kolega, o ktorom som pisala, co ma v tuto sobotu svadbu si prave zabookoval uz v poradi 5 letenku. Tie predosle lety boli vsetky zrusene. Ak bude mat stastie, tak zajtra o 7:15 bude letiet smerom Berlin (drzte mu palce). Iny nemecky kolegovia sa dnes vybrali na letisko Stansted veriac v to, ze dnestne lety uz zrusene nebudu. Ked tam ale dorazili dozvedeli sa, ze vsetko je po starom a dnes sa lietat nebude. Rozhodli sa teda ze pojdu domov vlakom, ale tie su vypredane az do pondelka. :-) 
 Teraz po Londyne hladaju hotel. Ako mozte vidiet atmoska v nasom kancli je ako v blazinci. 
 Moj kolega Alberto je z toho uplne zniceny, v sobotu mal ist bezat "Hasicsky maraton". Cely rok o tom hudol, ze ako pobezi, a ze viaceri jeho kamarati ktory sa tiez zapisali uz od toho aj upustili. Vravel ze on je tvrdy chlap a ze sa nevzdal a vela trenoval. Dnes ma na sebe koselu, kravatu a oblek, na protest si obul jeho si tenisky. Simbolicky tak protestuje voci prirode. :-) 
 Verim ze vikend straveny na ostrovoch bude fajn. Sme tu dobra partia z prace, a mam tu aj znamych, takze sa nudit nebudem. Bude mi smutno za mojim "hobim", ale ved ta sopka nebude dymit cely tyzden! Ze nie?! 
 Majte pekny vikend... 
 Hasta la victoria! 
   
   

