

 PRAHA (ČTK) - Po dlhej chorobe zomrel v utorok v Prahe vo veku 70 rokov herec Josef Bláha, ktorý bol takmer 40 rokov členom pražského Divadla na Vinohradoch. Narodil sa 8. júna 1924 v Juhoslávii, pretože jeho matka bola Slovinka. Ako jedenásťročný sa s rodičmi presťahoval do Čiech. Televízni diváci si iste spomenú na postavu pána Brůžka, ktorú stvárnil v cykle Hříšní lidé města pražského, či na úlohu v známom seriáli Návštevníci. Svoj komediálny talent uplatnil najmä v komédii Neila Simona ...vstupte! alebo v rozprávke Dalskabáty, hříšná ves. Nakrútil viac ako päťdesiat filmových rolí. 

