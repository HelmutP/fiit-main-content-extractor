

   
  
   
 Neprítomnosť hôr a celkovo iba mierne zvlnený povrch ostrovov môže pôsobiť ako monotónny, uznávam. Zato však výhodou takejto krajiny je, že vám poskytuje ničím nehatený výhľad na najznámejšie dominanty ostrovov. 
   
  
   
 Lákavou vidinou pre návštevníka je určite aj možnosť vidieť iba pre Shetlandy typické zvieratá, ktoré sú, oproti ostatným pribuzným vo svete, menšie vzrastom. Sú nimi ovca shetlandská. 
   
  
   
 A poník shetlandský. 
   
  
   
 Pre Slováka, konkrétne, určite nie je každorannou realitou si vypočuť hádku týchto silných škrekľúňov. 
   
  
   
 Či sadnúť si v lete v meste na lavičku ("so slnečnými okuliarmi a Sandokanom na tričku"), doslova, a s tuleňom v hladáčiku. 
   
  
   
 Donedávna svetovým unikátom, ktorý žiaľbohu medzičasom stihol vyhynúť, bol cvrček shetlandský. Už nám tu na ostrovoch docvrlikal. Voda nám ho odniesla. 
   
  
   
 A tak svoje príjemné prechádzky miesto počúvania nebeskej hudby vypľňame hraním piškvoriek. 
   
  
   
 Či prechádzkou po jedinom shetlandskom lese, v ktorom sa ani stromu dotknúť nesmiete. Stupeň ochrany je tu jeden z najprísnejších v celom svete a dôsledne sa aj dodržiava, na rozdiel od Tatier. 
   
  
   
 Niet sa čomu čudovat, ak si uvedomíte, že oným povestným lesom prejdete z jedného konca na druhý za necelé dve minútky. 
   
  
   
 Shetlandské ostrovy preto kľudne môžu slúžiť aj ako memento pre súčasníkov, ako to môže v budúcnosti na svete vyzerať, ak si premrháme svoje prírodné suroviny a dopustíme, aby posledným stromom bol práve tento, o ktorý sa už roky rokúce starám a nedám na neho dopustiť. 
   
  
   
 Nakoniec však ešte vždy ostáva malé svetielko nádeje, že sa ľudstvo predsa len spamätá. 
   
  
   
 Vedzte však, že Shetlandské ostrovy sú jedným z najpohostinejších miest na svete. A všetci, čo sem prichádzajú v láske a mieri, tak spoznajú iba našu vľúdnu tvár. 
   
  
   
 Vítame vás u nás. 

