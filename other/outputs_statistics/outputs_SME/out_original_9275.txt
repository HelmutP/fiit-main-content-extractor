
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Bakša
                                        &gt;
                zločin a trest
                     
                 Policajné dôchodky ? Možné bezpečnostné riziko ? 

        
            
                                    21.6.2010
            o
            19:35
                        (upravené
                21.6.2010
                o
                19:41)
                        |
            Karma článku:
                8.43
            |
            Prečítané 
            1900-krát
                    
         
     
         
             

                 
                    Ako policajt priznávam, že dnešný systém výsluhových dôchodkov policajtov nie je spravodlivý. Ale každá  minca má dve strany. Jeho plošné zrušenie môže predstavovať pre Slovensko potencionálne bezpečnostné riziko
                 

                           Je pravdou, že nie je fér voči ostatným zamestnancom, ak ide niekto do dôchodku ako 33 ročný.  Každý, kto tvrdí, že dôchodky policajtov sú  nespravodlivé  má pravdu. Ale treba sa na tento problém pozrieť aj  druhej strany.              Je pravda, že dnes výška mnohých  policajných aj vojenských dôchodkov predstavuje  sumu nad 1000 €, ale zároveň je pravdou, že je mnoho policajných  a vojenských dôchodcov, ktorých výška zďaleka nedosahuje uvedené hodnoty, napriek tomu že odslúžili v ozbrojených zložkách aj viac ako dvadsať rokov. Dovolím si tvrdiť, že najvyššie dôchodky majú výsluhový dôchodcovia polície, ktorý väčšinu svojej policajnej praxe odslúžili v socializme.  Ich ohrozenie ( pri všetkej úcte ) nemožno porovnávať s ohrozením dnešných policajtov priamo vo výkone. Oni pracoval  v dobe, keď policajt respektíve príslušník ZNB mal autoritu, rešpekt a štruktúra páchateľov a kriminality sa nedá porovnať s dnešným stavom.  Naháňať disidenta, prípadne páchateľa rozkrádania majetku v socialistickom vlastníctve nemožno porovnávať  s nebezpečnosťou a brutalitou dnešných zločincov.             Policajt, ktorí majú dnes odslúžených 15 až 20  rokov nastupovali do polície v čase, keď o  prácu policajtov nikto nemal záujem. Nastupovali v čase, keď  sa policajtom nechcel stať pomaly nikto. Nastupovali so neznámeho prostredia, do prostredia nebývalého nárastu  kriminality, nových foriem zločinu a tiež do prostredia,  keď po   jednej z najväčších amnestií bolo na slobodu prepustených mnoho väzňov.             Je pravdou, že  mnoho ľudí išlo k polícii z dôvodu stability zamestnania a sociálnej istoty. Drvivá väčšina policajtov, ktorí dnes slúžia v Policajnom zbore však išla do zboru z presvedčenia, že pomôžu spoločnosti a táto práca respektíve ich predstavy o nej ich zaujímali.               Nie náhodou sa dlhé roky  nedarilo, a myslím si, že sa stále  nedarí, naplniť všetky voľné miesta v rámci Policajného zboru tak, aby boli všetky miesta obsadené.  Táto práca okrem  priameho  nebezpečenstva ( keď napríklad  nevycvičení policajti sú nasadený proti vrahom alebo  hliadka obvodného oddelenia nevie, kto ich čaká za dverami pri  zákroku proti agresívnemu výtržníkovi  ) zahŕňa rôzne obmedzenie od možnosti zvoliť  si lekára, nemožnosť  politickej či podnikateľskej aktivity a iné. Toto riziko si policajt do zboru uvedomoval rovnako ako xistenciu sociálneho systému.               Súhlasím s každým, že voči ostatným pracujúcim je postavenie policajných dôchodcov neférové a je potrebné tento systém zmeniť. Ale z hľadiska zákonnosti, rovnosti príležitostí, logiky a spravodlivosti  prichádza do úvahy jediné riešenie. Treba nastaviť nový dôchodkový a sociálny systém pre policajtov, ale tento systém treba zaviesť pre policajtov, ktorý nastúpia od účinnosti nového zákona.  Treba stanoviť jasné pravidlá ich sociálneho a dôchodkového zabezpečenia. Treba umožniť policajtom sporiť v druhom pilieru ( čo im teraz zákon neumožňuje), je potrebné im prispievať na sporenie v  treťom pilieri  ( čo im na rozdiel od vojakov  zamestnávateľ  neumožňuje ).               V tejto súvislosti mi nedá nespomenúť situáciu starostov obcí, z radov ktorých sa ozývajú  požiadavky o akúsi obdobu výsluhových dôchodkov po ukončení mandátu a pritom starostovia kandidovali a boli zvolení  do funkcií na vopred dané funkčné obdobie  na rozdiel od policajtov, ktorý prácu v polícii berú ako svoje celoživotné povolanie.                      Je nesporné, že verejnosť si spája policajtov hlavne s dopravnými policajtmi, ktorí ich obťažujú svojou prítomnosťou na cestách, prípadne príslušníkmi poriadkovej  polície, ktorí ľudí obťažujú pri udržiavaní verejného poriadku, ukladaní pokút v doprave  a pri podobných  nepríjemných situáciách.                 Je potrebné priznať, že v celom Policajnom zbore dnes vládne neistota. Po výsledkoch volieb kolujú zaručené fámy o plošnom rušení policajných dôchodkov .      V tejto súvislosti by si poslanci pri schvaľovaní tohto zákona mali uvedomiť aj jednoduchú skutočnosť.   Okrem policajtov, ktorí dávajú pokuty a „obťažujú ľudí v snahe zabezpečiť verejný poriadok “ je v polícii veľká  skupina odborníkov, ktorých skúsenosti a znalosti  sú nenahraditeľné.                  V prípade, že poslanci siahnu na policajné dôchodky  všetkých  dnes slúžiacich policajtov 90 percent policajtov, ktorí majú odslúžených v polícii 15 rokov dá výpoveď.     Policajta slúžiaceho na obvodnom oddelení je relatívne jednoduché nahradiť novým. Vychovať kvalitného pracovníka operatívnej služby, špecialistu alebo agenta je prakticky nemožné v priebehu niekoľkých rokov. Aj verejnosť by si mala uvedomiť, že mnoho policajtov pracuje na odhaľovaní organizovaného zločinu, detskej prostitúcie, extrémizmu či obchodovania s drogami a zbraňami.               Predpokladám, že v špecializovaných útvaroch pracujú dnes odborníci s niekoľkoročnou praxou  a skúsenosťami.  V prípade, že by väčšina z nich opustila rady Policajného zboru, by nebola ohrozená len funkčnosť  Policajného zboru. Týmto opatrením  by bola ohrozená samotná bezpečnosť štátu z dôvodu absencie  pracovníkov na odhaľovanie organizovaného zločinu či  policajtov pracujúcich v kriminálnom prostredí pod legendou.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bakša 
                                        
                                            Kto je vlastne nezávislý?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bakša 
                                        
                                            Tak oblečme si dresy, ale ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bakša 
                                        
                                            Šalie Úrad pre verejné obstarávanie  ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bakša 
                                        
                                            Správa ombudsmanky. Ukážka arogancie a pohŕdania.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bakša 
                                        
                                            Pán Procházka myslíte, že to bude stačiť ?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Bakša
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Bakša
            
         
        baksa.blog.sme.sk (rss)
         
                                     
     
         Momentálne  nezávislý kandidát na poslanca mestského zastupiteľstva v meste Sliač a inak ten, čo má vlastný názor a cíti potrebu sa s ním podeliť. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    125
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2463
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Samospráva
                        
                     
                                     
                        
                            Šport,jedlo, zdravie
                        
                     
                                     
                        
                            zločin a trest
                        
                     
                                     
                        
                            pocity
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Rímske právo
                                     
                                                                             
                                            Zločiny komunizmu
                                     
                                                                             
                                            Malý princ
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Tomáš Klus
                                     
                                                                             
                                            Leonard Cohen
                                     
                                                                             
                                            Karel Kryl
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




