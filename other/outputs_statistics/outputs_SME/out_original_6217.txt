
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Blažeková
                                        &gt;
                Nezaradené
                     
                 Jedna obyčajná návšteva lekára 

        
            
                                    9.5.2010
            o
            23:48
                        |
            Karma článku:
                9.49
            |
            Prečítané 
            1098-krát
                    
         
     
         
             

                 
                    Na návštevu lekára potrebujeme: stan, spací vak a medikament na nervy. K lekárovi sa vyberieme najneskôr deň vopred. Rozložíme si stan pred poliklinikou alebo nemocnicou. Budík si nastavíme na štvrtú hodinu rannú a pokojne zaspíme. Ráno sa do polikliniky alebo nemocnice prepašujeme, keďže dvere by mali byť zamknuté. Zapíšeme sa na hárok papiera, na ktorom už budeme asi desiaty. Ako sa tam zapísali tí pred nami - nikto netuší. Ale vzniká podozrenie, že papier sa popísal sám.
                 

                   
  Pohodlne sa usadíme do kresla. Mohli by sme si dať kávu, ale kvôli prípadnému odberu krvi si nedáme nič a čakáme. Na dverách skontrolujeme ordinačné hodiny. O siedmej príprava ambulancie a od ôsmej až do dvanástej ordinačné hodiny, potom pol hodinky na obed a asi do pätnástej opäť ordinačné hodiny.     Prešla siedma hodina. Čakáreň je plná nervóznych chorých ľudí. Vydýchaný vzduch nevnímame, pretože sme tak nefetovaný cudzími výparmi, že je nám všetko jedno. Okolo ôsmej príde sestrička. Nie je zadychčaná - vraj ide včas. O pol hodinky zavolá prvých ľudí, ktorí prišli „len“ na odber. Tí už pomaly zelení od hladu poslušne kráčajú aj traja naraz k sestričke. Okolo pol desiatej dorazí aj doktor. Hurá!    Ubehne pol hodina, keď sestrička víťazoslávne otvorí dvere a zakričí: „Prví traja!“   Samozrejme, že do miestnosti pre sestričku sa nahrnie minimálne desať ľudí. Sestrička rozdáva karty pričom spomína rodinné anamnézy - lekárske tajomstvo je fuč. Niekoľkým „dobrovoľníkom“ zmeria tlak a výsledok prezradí všetkým, aby si svoje hodnoty mohli porovnať - lekárske tajomstvo je fuč. Pani kyprejších tvarov skonštatuje: „Od čoho môžeš mať ty tak vysoký tlak?! Veď si ešte mladá!“ Pripojí sa pán v rokoch: „To je z toho, že len pred počítačom sedia!“      A tak kompletne poučený čakáme, kedy sa dostaneme na radu. Vtom zaklope pani, ktorá „len“ prezentuje nové lieky. Ako ich moderne nazývame - dílerka - bez vysvetlenia ide k lekárovi, kde sa zdrží pol hodinu a niekedy aj viac.      Medzitým pacienti preberajú svoje symptómy a určujú si diagnózy. Sestrička popritom zatelefonuje dcére či spravila skúšku a občas poradí nejaké lieky, ktoré by mohli zabrať.     Juchúúú, konečne sa dostanete na radu! Lekár sa na vás znudene pozrie a spýta sa: „Čo si prajete?“ Akoby sme my všetci boli lekári a vedeli čo potrebujeme. V skratke mu povieme čo nás trápi a on - bez vyšetrenia napíše - „dýchanie zostrené, dutiny voľné, ...“ Poviete si: „Super, môj lekár je lepší ako Dr. House, nepotrebuje ani fonendoskop.“ V tom vás napadne diagnóza, ktorú vám povedali ostatní pacienti, kým ste čakali pri sestričke. Lekár súhlasí a  skonštatuje, že takúto chorobu mal aj on a bolo to príšerné. Predpíše vám lieky a konečne idete domov. Tešíte sa, že vás neposlal k žiadnemu odborníkovi - lebo to by ste sa zdržali ešte viac.     Za vami počujete hlas: „Sestra, som unavený na dnes to bol posledný pacient!“ Ručičky na hodinách ukazujú pól dvanástej. V mysli si len poviete: „ Dúfam, že moje dieťa bude raz lekárom...“    V lekárni zistíte, že lieky, ktoré ste dostali nemôžete ako alergik na penicilín užívať. V tom vás schytí zlosť a kúpite si voľnopredajný liek. Pobalíte stan a poberiete sa domov s tlakom, ktorý by nezaznamenal žiaden tlakomer a chuťou napísať sťažnosť na Lekársku komoru. Avšak, čo ak by ste v budúcnosti potrebovali predpísať kvapky do nosa?      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Keď diabetes (cukrovka) "žerie" zaživa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Dobrá známka sa nerovná dobrá vedomosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Originalny pozdrav k Vianociam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Môj prvý kúpeľný pobyt a hra na hrocha
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Uhni kripel!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Blažeková
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Blažeková
            
         
        martinablazekova.blog.sme.sk (rss)
         
                                     
     
        Neviem kto som, čo som, kam kráčam, čí som posol, čaká ma smrť či spása, ale aj tak usmievam sa.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    48
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    513
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            študáci :o)
                        
                     
                                     
                        
                            mňa nevymyslíš, ja som
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď diabetes (cukrovka) "žerie" zaživa
                     
                                                         
                       Dobrá známka sa nerovná dobrá vedomosť
                     
                                                         
                       Môj prvý kúpeľný pobyt a hra na hrocha
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




