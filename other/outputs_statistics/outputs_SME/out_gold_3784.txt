

 Raz bude mladosť za mnou a ruka starca drží fotku 
 Posledný pohľad na minulosť keď sa blíži čas dať za vetou bodku 
 Tie momenty čo ostali dýchať navždy vo mne 
 Dnes ožili, prežívam ich znova ako vo sne 
 Sú zrkadlom toho kto konal tu v mojom mene 
 Sú výpoveďou o mne dnes dýcham iba pre ne 
 S nostalgiou vnímam ich a uvedomujem si 
 Že slzy človeka sú ako kvapky rannej rosy 
   
 Cez okno vidím červené zore, moja kniha sa zatvára 
 Moje Slnko zapadá a do mora sa vnára 
 Ktovie čo bude teraz,či raz vietor privedie ťa ku mne 
 Tak velmi si prajeme aby sme tam hore boli spolu 
 A prajem si aby sme nikdy nespoznali všetko 
 Aby stále na odpoveď prišlo ďalšie prečo 
 Aby spomienky na mňa ktoré Ti tu po mne ostanú 
 Boli krásne nech ťa raz zasa kumne odvanú. 
   
 Spomienky mi dnes šepkajú kým som bol, 
 chcem vás znova cítiť skôr než sa zmením na popol. 

