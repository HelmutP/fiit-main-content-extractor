
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gustáv Murín
                                        &gt;
                Politika
                     
                 Mafia na Slovensku - Osudy bossov Černákovej éry 

        
            
                                    9.9.2009
            o
            9:09
                        |
            Karma článku:
                12.37
            |
            Prečítané 
            9968-krát
                    
         
     
         
             

                 
                    Vo svojej najnovšej knihe píšem o mafiánskych bossoch 90. rokov na Slovensku. Všetci sú buď po smrti, na úteku alebo vo väzení. Ale na slobode nám tu doteraz slobodne nielen pobehujú, ale priam poletujú ich súputnici na našej politickej scéne...
                 

                  Príbehy obsiahnuté v chystanej knihe majú pozoruhodne všeplatné ponaučenie. Ich nositelia - Július Tóth a Peter Muszka, Róbert Holub, Holáň alias Gorila, Papa Joe, Ošo Kucmerka, Borženský a Kolárik, Alojz Kromka alias Čistič, Jozef Roháč alias Potkan, nájomný vrah Adamčo a nakoniec Mikuláš Černák - sa v 90. rokoch zviditeľnili tým ako sa brutálnym terorom a zastrašovaním pokúšali ovládnuť Slovensko.   A pritom sú v merítku dnešných "kšeftov" smiešni tým, ako zbíjali v "malom", ručne a namáhavo. Tam kde oni tasili zbraň, ich nasledovníci vyťahujú zlatú kreditnú kartu. Dnes stačí jeden vládny tender, kde vybraný minister v úlohe novodobého bieleho koňa síce neporuší zákon, ale poteší vopred vybrané firmy a tri miliardy prejdú do tých správnych rúk...   Ako v rozprávke      Najobľúbenejším hrdinom slovenských rozprávok je Hlúpy Jano, ktorý napriek svojej prostoduchosti nakoniec so všetkými vybabre a dostane za ženu princeznú a navrch aj pol kráľovstva. Už pred stáročiami, pradávno, tvorcovia rozprávok vedeli, že tá prvá polovica predsa nutne musí patriť Vladimírovi Mečiarovi! V 90. rokoch minulého storočia sme sa konečne dozvedeli aj to, kto je živým stelesnením toho rozprávkového hrdinu slovenského ľudu a kto nakoniec naplnil toto dávnoveké poslanie. Volá sa predsa - Ján Slota.   Výčiny tohto pána všetci dobre poznáme a len pre osvieženie pamäte som urobil v knihe malú rekapituláciu. Zaujíma ma ale niečo iné. Zatiaľ jediný, kto konečne nahlas vyjadril zhnusenie nad týmto zjavom na našej politickej scéne bol mladý podnikateľ a aktivista Alojz Hlina. A aj ten po čase priznal "necítim podporu a dochádzajú mi baterky".   Ako je možné, že také okázalé a zurvalé bačovanie jedného priožratého chmula sa deje pred očami celého národa celé roky a ten je ticho? Veď aj tí Česi sa zmohli aspoň na vajcovú kanonádu, ktorou vyjadrili svoje znechutenie nad obludnými praktikami Paroubka a jeho mafiánskych kamošov.    Tá naša Slota je už nejaký čas podozrivo ticho. Tak schválne, tipnime si. Kedy a ako skončí to jeho arogantné rozkrádanie nášho národného majetku a ako on sám?   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Mikuláš Černák – oslobodený Európskym súdom!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Dnes prišli Rusi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Záhada Tutanchmónovej výstavy - mimozemšťania v Bratislave?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Je toto ešte leto, alebo už monzún?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Hnoj pána Ondřejíčka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gustáv Murín
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gustáv Murín
            
         
        gustavmurin.blog.sme.sk (rss)
         
                                     
     
         Vraciam sa na tento blog a verím, že tak opäť stretnem starých známych a aj nových čitateľov. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    147
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2261
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Civilizácia
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Dlhovekosť
                        
                     
                                     
                        
                            Cesty
                        
                     
                                     
                        
                            Rádioaktívni - doplňujúce text
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Jazykové hlavolamy
                        
                     
                                     
                        
                            Rovnoprávnosť
                        
                     
                                     
                        
                            Paradigmy zdravia
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Partnerské vzťahy a rodina
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Čo sa deje
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prvá slovenská cestovateľská wikipédia
                                     
                                                                             
                                            Interaktívny román "Rádioaktívni"
                                     
                                                                             
                                            Moje texty v ďalších jazykoch
                                     
                                                                             
                                            Moje texty v angličtine
                                     
                                                                             
                                            Príbehy z ciest
                                     
                                                                             
                                            Viac o mne
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog o šťastí
                                     
                                                                             
                                            Blog antikvariátu Mädokýš
                                     
                                                                             
                                            Blog Cez OKNO
                                     
                                                                             
                                            Blog tajomneho medialnika
                                     
                                                                             
                                            Blog Pala Barabáša
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Video-blogy na TV Blava
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




