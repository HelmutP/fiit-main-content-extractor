

 Jeho vládu sprevádzala triedna nenávisť, miliardové kradnutia v priamom prenose, vytváranie vnútorných a vonkajších nepriateľov, pravidelné zlyhávanie polície,  bezprecedentný rozklad súdnictva a morálny úpadok v spoločnosti. A ako Fico končí? Novými bilbordmi zo strachu z SMK –  „DALI MOC SMK, UROBIA TO ZNOVA! Zastavte koalíciu SDKÚ-KDH-SMK!“ 
 Róbert Fico iste cíti, že sa v spoločnosti niečo deje. Podobne ako v roku 1998, keď sa občianska spoločnosť samovoľne začala politicky angažovať a stále viac a viac prejavovať. Posledné prieskumy ukazujú, že pravica bude mať reálnu možnosť vytvoriť vládu. Do toho prišiel Martin Shooty Šutovec, ktorý za necelý týždeň spontánne od ľudí vyzbieral vyše 1,5 milióna korún. Facebook ovládla opozícia, otvorené štrajky a prejavy občianskej nespokojnosti celú predvolebnú atmosféru len dokresľujú. 
 Nemám žiadne ilúzie o Csákyho SMK a ich postoj jedinej opozičnej strany ochotnej rokovať so Smerom už len vyvoláva úsmev na tvári terajšej SMK. No držme sa faktov. SMK bola vo vláde 8 rokov a za ten čas sa Slovensko dostalo z čiernej diery Európy (v ktorej bolo zásluhou HZDS, SNS a zakladateľov a privatizérov SMER-u) medzi elitné štáty EÚ a malo povesť stredoeurópskeho tigra. Preto si ani nekladiem otázku, či je väčší problém SMK vo vláde alebo SMER, HZDS či SNS. Odpoveď je jasná.  
 Verím, že po voľbách bude mať Slovensko opäť pravicovú koalíciu aj s našimi Maďarmi. 
 Možno Ficovi len krivdím. Chudák, možno musí robiť negatívnu kampaň, lebo po 4 rokoch na čele vlády nemá čo pozitívne po sebe odovzdať a ani povedať. 
  Good bye, Robo! 

