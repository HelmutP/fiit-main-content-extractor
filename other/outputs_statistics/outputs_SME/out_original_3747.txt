
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Jankes
                                        &gt;
                Slovník cudzích slov
                     
                 Dezilúzia 

        
            
                                    31.3.2010
            o
            23:45
                        |
            Karma článku:
                4.50
            |
            Prečítané 
            860-krát
                    
         
     
         
             

                 
                    -ie ž.  strata  ilúzie, rozčarovanie, sklamanie: trpká d.; zakúsiť d-iu - Krátky slovník slovenského jazyka 4 4., doplnené a upravené vydanie. Redigovali: J. Kačala – M. Pisárčiková – M. Považaj. Bratislava: Veda 2003.
                 

                 V mojom hľadaní nového zamestnania som pokračoval tromi pohovormi.  Odpovedal som na dve ponuky práce na internete a poslal som životopis do jednej firmy, ktorá síce neponúkala žiadne voľné miesto, ale zdala sa mi zaujímavá.  Prvá práca bol správca internetového portálu.  Mám dlhoročné skúsenosti s internetom. Štyri roky som robil návrhy jednoduchých internetových stránok s ich realizáciou a pravidelnými aktualizáciami.  http://jankes.blog.sme.sk/clanok.asp?cl=186953  Robil by som to z domu s občasnou návštevou centrály. No zadávatelia inzerátu ani neodpovedali na môj e-mail.  Druhou firmou bol operátor na DTP pracovisku. Títo sa nielen ozvali, ale mi napísali aj list, v ktorom mi vysvetlili, prečo ma nezavolajú na pohovor. Vyžadovali profesionálne ovládanie programu, v ktorom robia. Napísal som im, že akýkoľvek program sa dá za pár týždňov naučiť, no pre nich to nebol argument. Pritom počítače a počítačové programy sú jednou z oblastí, ktorým sa vo voľnom čase venujem takmer každý deň. Ale aspoň boli slušní. Možno je teraz na pracovnom trhu stovka záujemcov, ktorí majú v tom programe dlhoročné profesionálne skúsenosti a oni mali na výber.  Posledná bola firma, ktorá ma zaujala ich pracovnou náplňou a perspektívou. Pozvali ma na pohovor. Pýtali sa ma prečo chcem odísť z môjho terajšieho zamestnania, čo by som chcel robiť. Vyzeralo to nádejne, kým mi personalista nepovedal, že by boli radi, keby som dal v terajšej robote výpoveď, bol som tri - štyri mesiace nezamestnaný a oni by ma potom zobrali a so mnou by získali aj nemalé peniaze od štátu za vytvorenie nového pracovného miesta. Mám sa čo učiť. Toto by ma nenapadlo ani vo sne.  Ovládla ma mierna dezilúzia. Chcem obyčajnú robotu v obyčajnej firme. Som schopný učiť sa nové veci. Ponúkam zodpovednosť a všetko, čo som sa za 26 rokov v mojich zamestnaniach naučil. Zatiaľ ma nikto nechce. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Neverím, že sa tento rok preveziem po diaľnici medzi Dubnou Skalou a Turanmi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Štyri hodiny v Tatrách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Čaká sa tu dlho?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Bociany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Možno posledný sneh
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Jankes
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Jankes
            
         
        jankes.blog.sme.sk (rss)
         
                        VIP
                             
     
        Raz na to možno prídem.


  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    315
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1632
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovník cudzích slov
                        
                     
                                     
                        
                            Rozhovory
                        
                     
                                     
                        
                            Moje knihy
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Autozážitky
                        
                     
                                     
                        
                            cudzie perie
                        
                     
                                     
                        
                            Fototúlačky
                        
                     
                                     
                        
                            Kuchárska kniha pre úplných za
                        
                     
                                     
                        
                            mamonár
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            80 strán
                        
                     
                                     
                        
                            S kreslenými vtipmi
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ivan Štrpka: Rukojemník
                                     
                                                                             
                                            Pavel Hirax Baričák: 666 anjelov
                                     
                                                                             
                                            Ivan Jančár - Lee Karpiscak: Koloman Sokol 100 nepublikovaných d
                                     
                                                                             
                                            Inspire - magazine with a difference
                                     
                                                                             
                                            Zberateľ alebo zmluva s diablom
                                     
                                                                             
                                            Milan Kundera: Směšné lásky
                                     
                                                                             
                                            Egon Bondy: Mníšek, Vylitý nočník
                                     
                                                                             
                                            Rudolf Sloboda: Krv
                                     
                                                                             
                                            Rasťo Piško: Bohémska kolonáda
                                     
                                                                             
                                            Barry Miles: Charles Bukowski
                                     
                                                                             
                                            Dušan Mitana: Koniec hry - prvé vydanie
                                     
                                                                             
                                            Egon Bondy: Filosofické eseje sv.4
                                     
                                                                             
                                            Ivan Kadlečík: Škoda knižke nerozpredanej ležať
                                     
                                                                             
                                            Rudolf Sloboda: Rozum
                                     
                                                                             
                                            Rudolf Sloboda: Z denníkov
                                     
                                                                             
                                            Pierre Restany: Hundertwasser
                                     
                                                                             
                                            Liv Ulmann: Choices
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            London grammar
                                     
                                                                             
                                            Katarzia
                                     
                                                                             
                                            Korben Dallas
                                     
                                                                             
                                            Aminata Kamissoko
                                     
                                                                             
                                            Herbie Hancock Feat Corinne Bailey Rae - River
                                     
                                                                             
                                            Iva Bittova + Vladimír Václavek - Zvon
                                     
                                                                             
                                            Adele - Someone Like You (on 'Later Live with Jools Holland') -
                                     
                                                                             
                                            Grzegorz Karnas feat. Tomasz Szukalski
                                     
                                                                             
                                            Scott Dunbar - Sober
                                     
                                                                             
                                            Offret - Sacrificatio (1986) Andrei Tarkovskij / Part 1 / Eng. S
                                     
                                                                             
                                            Bach - Julia Hamari - Matthäus Passion - Erbarme dich
                                     
                                                                             
                                            Vladimír Václavek - Sen
                                     
                                                                             
                                            Lightnin' Hopkins - Goin' Back Home
                                     
                                                                             
                                            AG flek - Carpe diem
                                     
                                                                             
                                            Fred McDowell: Keep Your Lamp
                                     
                                                                             
                                            Seban Rozsa Buntaj - Come Together
                                     
                                                                             
                                            Scott Dunbar, ArtsWells Festival, Part 3
                                     
                                                                             
                                            Lenka Dusilová - Za vodou
                                     
                                                                             
                                            Stand By Me | Playing For Change | Song Around the World
                                     
                                                                             
                                            KRZAK - Blues dla nieobecnych
                                     
                                                                             
                                            Scott Dunbar One Man Band
                                     
                                                                             
                                            Bittová, Godár - Lullabies
                                     
                                                                             
                                            Vlasta Třšešňák - Atlant
                                     
                                                                             
                                            Sting - Shape Of My Heart
                                     
                                                                             
                                            Knopfler &amp; Clapton - Same old blues
                                     
                                                                             
                                            Tore Down - Chris Shepherd Band
                                     
                                                                             
                                            Iva Bittova - Uspavanka
                                     
                                                                             
                                            Chuck Beattie Band &amp; Kenny Dore
                                     
                                                                             
                                            Blind Boys of Alabama - Satisfied Mind
                                     
                                                                             
                                            'God Moves On The Water' BLIND WILLIE JOHNSON
                                     
                                                                             
                                            Eric Clapton Crosroads
                                     
                                                                             
                                            Suni Paz Performs Tierra Querida- Bandera Mia/Smithsonian Folkwa
                                     
                                                                             
                                            Guy Forsyth Band - Taxi
                                     
                                                                             
                                            Drum and Bass (Impressive street performer)
                                     
                                                                             
                                            Rihanna feat. Justin Timberlake rehab
                                     
                                                                             
                                            Justin Timberlake Vs Eminem
                                     
                                                                             
                                            Rupa &amp; the April Fishes "Une americaine a Paris"
                                     
                                                                             
                                            Audition (3)
                                     
                                                                             
                                            Fleret: Zafukané
                                     
                                                                             
                                            ČECHOMOR - Karel Holas - Rehradice
                                     
                                                                             
                                            Michal Prokop - Snad nám naše děti prominou..
                                     
                                                                             
                                            Michal Prokop - Kolej Yesterday
                                     
                                                                             
                                            Vladimír Mišík Variace na renesanční téma ( Večernice)
                                     
                                                                             
                                            Vladimir Mišík - Stříhali dohola malého chlapečka
                                     
                                                                             
                                            Knocking on Heaven's Door by Bob Dylan
                                     
                                                                             
                                            Prežijem
                                     
                                                                             
                                            Rachmaninov mal veľké ruky
                                     
                                                                             
                                            Kapela ze Wsi Warszawa - Live!
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Dášena
                                     
                                                                             
                                            zina veličková
                                     
                                                                             
                                            Anna Strachan
                                     
                                                                             
                                            Videoblog: Tanec je všetko
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Ivan Kadlečík
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Malá Fatra zo Strečna.
                     
                                                         
                       Stehno veľryby
                     
                                                         
                       Robert a jeho úžasný život v bubline
                     
                                                         
                       Konečne letím, alebo z Viedne do Quita (Ekvádor - 1. časť)
                     
                                                         
                       Suť
                     
                                                         
                       V úzkosti zo smrti
                     
                                                         
                       Päť užitočných otázok o Modrom z neba
                     
                                                         
                       Delová guľa Ruda Slobodu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




