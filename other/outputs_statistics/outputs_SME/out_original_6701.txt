
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Baričák
                                        &gt;
                KOSTARIKA (Cestopis)
                     
                 Z Panamy do Kostariky (6. časť) 

        
            
                                    17.5.2010
            o
            20:02
                        (upravené
                15.6.2010
                o
                10:14)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            2059-krát
                    
         
     
         
             

                 
                    Prečo meškám s knižným vydaním o potulkách Panamou a Kostarikou skoro tri roky? Všetko má svoj čas a všetko musí dozrieť. Tak písmenká, fotky, myšlienky, spomienky, ako aj okamih vypovedania skúsenosti ostatku svetu. Pustil som sa teda do dokončenia tohto cestopisu vtedy, keď sa o to prihlásilo samo srdce. Šeplo mi: „Paľko, je čas. Mám na to chuť. Teraz to spolu spravíme s láskou a ľudí to bude o to viac tešiť. Vycítia to, ver mi." Moje vnútro ma nikdy nesklamalo a ja mu neskonale verím. Takže po viacej ako roku a pol je tu ďalšie panamsko-kostarické putovanie, ktoré momentálne vyšlo aj knižne a tak som si sľúbil, že ho dokončím aj na mojom blogu.
                 

                     Príchod do San José   (Sobota 8. 12. 2007)   Ráno pred odchodom sa ešte znova snažím nafotiť pár tých smrtiacich žabiek v teráriu na verande nášho hotela. Včera večer som zaregistroval more skákajúcej farby ‒ modré, žlté, zelené, čierne či hnedé žabky. Hotelierka ma informovala, že na to, aby som zomrel, by som musel minimálne jednu zjesť. Odpovedám jej, nech mi jednu podľa jej vkusu vyberie. Smeje sa.   Plavíme sa späť do obitého malého prístavu Almirante s domami so strechami z hrdzavého plechu, stojacimi na drevených kuracích nohách. Potom máme v pláne dostať sa taxíkom ku kostarickým hraniciam do mestečka Guabito. Po ceste k taxíku prehovárame ďalších dvoch Európanov zo západnej Európy, nech idú tiež taxíkom a nie autobusom, že ich to vyjde lacnejšie. Neveria nám a silou-mocou sa držia naplánovanej cesty busom. Ľutujem, že neovládam angličtinu na takej úrovni, aby som im vysvetlil graf výšky hrubého národného produktu na obyvateľa v spojitosti s lakomstvom ‒ keď si dvaja chalaniskovia z bývalého východného bloku môžu dovoliť taxík a chlapci z Anglicka nie.      Áno, „socialistické nátačky" sú v Latinskej Amerike stále horúcim hitom. Ženy s nimi dokonca aj bežne cestujú.    Z taxíku prestupujeme priamo do diaľkového autobusu, mieriaceho do hlavného mesta Kostariky San José. O desať minút musíme vystúpiť a prejsť pasovou kontrolou. Najprv povinný pečiatkový výstup z Panamy a potom musí ísť každý peši po rozbitom moste na kostarickú stranu. V Paname je americký dolár rovnocenná mena s panamským balboa, ale Kostarika má svoje colóny. Sto amerických dolárov sa rovná približne päťdesiatim tisícom kostarických colónov. Meníme peniaze, sadáme do autobusu, zakladáme fiktívnu pahrebu a kladieme do nej imaginárne obete, nech autobus vydrží tých šesť hodín jazdy.          Hliadkujúci policajt tesne pred príchodom na kostarické hranice.           Svadobný obrad v rieke.        Vozovka je sprvoti rozbitá, ale potom už frčíme „diaľnicou" (jeden pruh tam, druhý späť, cesta je hocijaká, len nie rovná, ale stále lepšia ako tá predtým). Za oknami autobusu, občas pofŕkanými kvapkami dažďa, letia banánové plantáže, palmy, ošarpané osamotené domčeky, pri ktorých sedia spokojní domorodci vyhrievajúci sa na slniečku.   Zastavujeme sa pri nejakom domci kvôli ďalšej pasovej kontrole a všetci musíme vyjsť z autobusu von. Kupujeme si pitie, nasadáme, vodič cúva a dŕŕŕb ho do autobusu stojaceho za nami. Opäť vychádzame, lebo dnu je ako v peci a pozorujeme následky búračky. Dvaja domáci sa mi prihovárajú a varujú ma, že kým prídu policajti, môže to trvať dve až päť hodín. Pýtam sa ich, akí policajti by ešte mali prísť, keď už tu nejakí sú. Odpovedajú mi, že musia prísť iní, a tí sem tak rýchlo nedorazia. Sme vzdialení nejakú trištvrte hodinku od hraníc, a to je vraj bohom zabudnutý kraj.   Zhruba za tridsať minút všetci z druhého autobusu presadajú do nasledujúceho spoja. Majú šťastie, lebo idú len do blízkeho mesta, ale nás delí od hlavného mesta ešte nejakých päť hodín trmácania.      Tak kvôli tomuto „ťuku" sme tam stáli tak dlho...           Panamská pasová kontrola.           Do Kostariky sme museli prekráčať cez tento most.   Za necelé dve hodiny sa pohýname aj my. S Jiřím sme radi, že vodič nepustil domáce hity, lebo harmoniky máme dosť na päť rokov. Ak niekto vytiahne na Slovensku v krčme tento hudobný nástroj, neviem, či ho z nepríčetnosti nabudem mať chuť ubiť.   Vchádzame do San José. V meste sú dopravné zápchy, všade davy ľudí a špina. Jiří sa pýta spisovnou španielčinou, či avenue (v mestách majú ulice značené na americký spôsob ‒ avenue a calle, párne a nepárne, pričom nižšie čísla sa zbiehajú k centru mesta), po ktorej kráčame, ide do centra alebo nie. Nevieme totiž, či čísla narastajú alebo klesajú. Mladé dievčatá, ktorých sa pýta, mu nerozumejú. Od pleca sa do debaty snaží pridať zarastený, opitý bezdomovec. Vzniká celkom slušná groteska.   Nakoniec však odporúčaný hotel nachádzame. Desať dolárov na osobu a noc je dobrá cena. Nasvedčuje tomu špinavá kúpeľňa, nejaký chrobák vylieza spoza koša a ja sa bojím stlačiť vypínač, taký je špinavý. Byť tu s frajerkou, viem, čo mi povie. Ale keďže ideme z pralesa, sme s tým zmierení. Hocijaký dvojhviezdičkový slovenský hotel by bol v Latinskej Amerike ťažký komfort. Ešte raz začujem nejakého Nemca nadávať na kvalitu služieb našich hotelov, kúpim mu na vlastné náklady letenku do Strednej Ameriky.   Vybieham do mesta, v ktorom býva necelých tristopäťdesiat tisíc obyvateľov. Chcem stihnúť koncert symfonického orchestra v Teatro Popular Melico Salazar alebo v Theatro Nacional Costa Rica. V prvom sa končí nejaký koncert mladých konzervatoristov, v druhom už začal balet. Pýtam sa teda, či aj zajtra hrajú Čajkovského. Dostávam kladnú odpoveď, že o piatej. Radujem sa, lebo sa mám na čo tešiť.   Jirko zatiaľ vybavil agentúru, ktorá nás zajtra bude sprevádzať vulkánom Poás. Vraj im prízvukoval, že do pol desiatej ráno tam chceme byť, lebo ináč sa zatiahne a uvidíme akurát tak makové. Sľúbili, že sa nemusíme báť.   Do noci brázdim ulice najkozmopolitnejšieho mesta Strednej Ameriky a nasávam jeho atmosféru. Pár pekných námestíčok, ale ináč všade obchody s obuvou, oblečením, jedlom (mekáč, KFC, zmrzlina Häagen Dazs a pizza Hut sú snáď všade na svete), banky... proste klasika veľkomesta. Hektický pouličný ruch oživuje množstvo nevýrazných budov. Rieky chodcov si razia cestu cez úzke uličky, hluk vychádza z každého stánku s jedlom, more vodičov sa úporne snaží o samovraždu. Všade sa váľajú odpadky, bezdomovci. K spokojnosti turistov je tu slušný počet policajtov (aj na bicykloch či v malých autíčkach, ktoré vyzerajú ako niečo medzi medzi motokárou a malým transportérom). O šiestej je tma a o deviatej zostávajú na uliciach len divní „týpkovia", policajti a ja, biely zvedavý gringo.          Veru tak, v hlavom meste Kostariky San José to vyzerá, akoby tam ulice zametali len sochy.           Keby ste mi to náhodou s tým poriadkom neverili...      Bežná vec k videniu - pouliční čističi topánok.           Moletné ženy sú v Latinskej Amerike bohyne.       Sadám si do baru hneď vedľa nášho hotelíka a pijem vynikajúce domáce pivo Imperial. Neskôr som objavil aj značku Pilsen. Obidve sú ako cukrík a Jiří, chalanisko z krajiny piva, uznáva, že Panama so svojimi močkami Atlas a Panama ďaleko zaostáva. Jedine panamské pivo Balboa by sa dalo hodnotiť ako priemerné a celkom piteľné, ale na Imperial a Pilsen i tak nemá. V bare si píšem a čašníčky majú zo mňa šou. O jedenástej zatvárajú. Obyvatelia Latinskej Ameriky vstávajú skoro, a takisto chodia aj skoro spať. Žiaden extrémny nočný život nevedú. Nabieham teda na ich rytmus a idem na izbu. Jirko už chrápe svoj druhý sen, a tak sa aj ja snažím vnoriť čo najrýchlejšie do spánku. Keď zatváram oči, pristihnem sa pri myšlienke, že už sa začínam tešiť na milované Slovensko.       Buďte šťastní, Hirax       Aktivity Hiraxa:   Pondelok, 18. 5. 2010, Svet, Oficiálne vydanie Hiraxovej druhej    básnickej zbierky Nech je nebo všade, ktorá bude    pokračovaním priaznivo prijatého debutu More srdca. Knižočka bude    obsahovať ďalších, približne 90 básni, aforizmov, bájok, zamyslení a tak    ako pri prvých básniach bude ilustrovaná skvelými obrázkami od Janky    Thomkovej. Druhý titul bude panamsko-kostarický cestopis Úcta k    prírode a úsmev ako zmysel života. Väčšia časť knihy sa bude    venovať pobytu v pralese Darién u Indiánov z kmeňa Emberá. Cestopis  bude   obsahovať približne 130 plnofarebných fotiek a okrem Hiraxovho    autentického popisu písaného priamo zo srdca, bude titul niesť aj    teoretické časti o živote a zvykov spomínaných Indiánov. Obidve knihy    vyjdú u knižného vydavateľstva HladoHlas.   Streda, 19. 5. 2010 o 17:32, Žilina, kaviareň Kaťuša    (Mariánske námestie - medzi Tatra bankou a reštauráciou Voyage).    Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a Kostarikou s    hlavným dôrazom na pobyt v panamskej džungli Darién u Indiánov  Emberá.  V  tomto čase by mala byť vonka už aj knižná podoba tohto  panamského   cestopisu (a troška aj Kostarického :-)) s názvom "Úcta k  prírode a   úsmev ako zmysel života", ktorý vyjde začiatkom mája 2010.  Vstup jeden   úsmev.   Štvrtok, 20. 5. 2010 o 11:11, Sučany, gymnázium -    súkromná recitácia. Čítačka Pavla "Hiraxa" Baričáka, kde porozpráva o    svojich románoch a prečíta nejaké básne. Vstup jeden úsmev.   Štvrtok, 20. 5. 2010 o 18:32, Martin, kaviareň    Kamala. Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a    Kostarikou s hlavným dôrazom na pobyt v panamskej džungli Darién u    Indiánov Emberá. V tomto čase by mala byť vonka už aj knižná podoba    tohto panamského cestopisu (a troška aj Kostarického :-)) s názvom "Úcta    k prírode a úsmev ako zmysel života", ktorý vyjde začiatkom mája  2010.   Vstup jeden úsmev.   Utorok, 25. 5. 2010, Žilina, "Bez hraníc".    Slovensko-poľský literárny festival. Moje čítačky budú upresnené.   Streda, 26. 5. 2010, Banská Bystrica, verejné    čítanie na námestí v BB prebiehajúce od doobedňajších hodín do večera.    Predbežný čas mojej "čítačky-rozhovoru-dialógu" je od 16:30 do 17:30.   Štvrtok, 27. 5. 2010, Žilina, "Bez hraníc".    Slovensko-poľský literárny festival. Moje čítačky budú upresnené.   Streda, 16. 6. 2010, Bratislava, Kníhkupectvo MODUL-    Svet knihy (v centre mesta, Obchodná), Od 16:00 Hiraxova čítačka k    vydanému cestopisu a básniam Nech je nebo všade, od 17:00 krst    spomínaných dvoch kníh + románu od slovenskej autorky Olivie Olivieri a    jej debutu "Cicuškine zápisky". Víno, chlebíčky a pozitívna nálada    zabezpečené. Vstup jeden úsmev :-).   Utorok, 22. 6. 2010, Zlaté Moravce, súkromná čítačka    v reedukačnom zariadení pre mladistvých. Cestou späť pôjdem cez    Piešťany. Ak viete o nejakom priestore (knižnica, čajovnička, jazz klub    atď), kde sa dá spraviť čítačka, ozvite sa mi cez mail.   Utorok, 1. 6. 2010, Čechy, Oficiálne vydanie českej    verzie románu Vteřina před zbláznením (Všechno je, jak je) pod vlajkou    pražského vydavateľstva XYZ.   Pondelok, 28. 6. 2010, Topoľčany, 17.00 hod Galéria    (program pre mladých), 18.00 reštaurácia Kaiserhof Nám.M. R. Štefánika    (čítačka Hirax).   Máj-jún 2010: Výstava Hiraxových fotografií z    Thajska v martinskej kaviarničke Kamala. Pozor, nejedná sa o    žiadnu "galériu". Bude sa jednať o 12-14 záberov, ktorými som sa  snažil   vystihnúť túto krajinu. Vstup jeden úsmev.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Ako urýchliť zámer? Obetou a pokorou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (2.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (1.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Všetci sme mágovia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Vnútorným dialógom k osobnému šťastiu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Baričák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Baričák
            
         
        baricak.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ľudia majú neuveriteľnú schopnosť pripísať všetko, čo bolo napísané autorovi, že to na vlastnej koži aj sám prežil. Prehliadajú schopnosť vnímania, pozorovania sveta a pretransformovania ho do viet s úmyslom pomôcť druhým. Ale na druhej strane sa mi dobre zaspáva s pomyslením, že ostatok sveta na mňa myslí. Cudzí ľudia o mne vedia všetko, teda aj to, čo neviem ani ja sám. Ďakujem im teda za ich všemocnú starostlivosť! Buďte všetci šťastní, prajem Vám to z celého môjho srdca. Hirax &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/anketa_0.php?id=754643" href="http://blueboard.cz/anketa_0.php?id=754643"&amp;amp;gt;Anketa&amp;amp;lt;/a&amp;amp;gt; od &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/" href="http://blueboard.cz/"&amp;amp;gt;BlueBoard.cz&amp;amp;lt;/a&amp;amp;gt; 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    558
                
                
                    Celková karma
                    
                                                7.11
                    
                
                
                    Priemerná čítanosť
                    3664
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šlabikár šťastia 1.
                        
                     
                                     
                        
                            Šlabikár šťastia 2.
                        
                     
                                     
                        
                            Vzťahy (Úvahy)
                        
                     
                                     
                        
                            Tak plače a smeje sa život
                        
                     
                                     
                        
                            Zachráňte malého Paľka!
                        
                     
                                     
                        
                            Po stopách komedianta
                        
                     
                                     
                        
                            Sárka Ráchel Baričáková
                        
                     
                                     
                        
                            Sex (Úvahy)
                        
                     
                                     
                        
                            NEVERŠOVAČKY (Básne)
                        
                     
                                     
                        
                            PRÍBEH MUŽA (Básne a piesne)
                        
                     
                                     
                        
                            SEKUNDU PRED ZBLÁZNENÍM (Román
                        
                     
                                     
                        
                            KÝM NÁS LÁSKA NEROZDELÍ (Román
                        
                     
                                     
                        
                            RAZ AJ V PEKLE VYJDE SLNKO/Rom
                        
                     
                                     
                        
                            ČESKÁ REPUBLIKA (CESTOPIS)
                        
                     
                                     
                        
                            EGYPT (Cestopis)
                        
                     
                                     
                        
                            Ekvádor (Cestopis)
                        
                     
                                     
                        
                            ETIÓPIA (Cestopis)
                        
                     
                                     
                        
                            FRANCÚZSKO (Cestopis)
                        
                     
                                     
                        
                            INDIA (Cestopis)
                        
                     
                                     
                        
                            JORDÁNSKO (CESTOPIS)
                        
                     
                                     
                        
                            KOSTARIKA (Cestopis)
                        
                     
                                     
                        
                            Mexiko (Cestopis)
                        
                     
                                     
                        
                            Nový Zéland (Cestopis)
                        
                     
                                     
                        
                            PANAMA (Cestopis)
                        
                     
                                     
                        
                            POĽSKO (Cestopis)
                        
                     
                                     
                        
                            SLOVENSKO (Cestopis)
                        
                     
                                     
                        
                            THAJSKO (Cestopis)
                        
                     
                                     
                        
                            USA (Cestopis)
                        
                     
                                     
                        
                            VIETNAM (CESTOPIS)
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Niečo o zrkadlení a učiteľoch
                                     
                                                                             
                                            Vlastné šťastie ide cez prítomnosť
                                     
                                                                             
                                            Áno, dá sa naraz milovať dvoch ľudí
                                     
                                                                             
                                            Chcete zmeniť vzťah? Zmeňte najprv seba.
                                     
                                                                             
                                            Chcete byť šťastnými? Prijmite sa.
                                     
                                                                             
                                            Ohovárajú vás? Zraňujú vás klebety? Staňte sa silnými!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Liedloffová Jean - Koncept kontinua
                                     
                                                                             
                                            Alessandro Baricco - Barbari
                                     
                                                                             
                                            Christopher McDougall - Zrození k běhu (Born to run)
                                     
                                                                             
                                            Ingrid Bauerová - Bez plenky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            The Killers
                                     
                                                                             
                                            Brandom Flowers
                                     
                                                                             
                                            Sunrise Avenue
                                     
                                                                             
                                            Nothing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Martin Basila
                                     
                                                                             
                                            Spomíname:
                                     
                                                                             
                                            Moskaľ  Peter
                                     
                                                                             
                                            Miška Oli Procklová Olivieri
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HIRAX Shop
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Skutočná Anna Kareninová bola Puškinovou dcérou
                     
                                                         
                       Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                     
                                                         
                       Surinam - zabudnutá juhoamerická exotika
                     
                                                         
                       Hodvábna cesta - 56 dní z Pekingu do Teheránu 2.časť ( Uzbekistan, Turkménsko, Irán)
                     
                                                         
                       Detskí doktori, buďte detskí
                     
                                                         
                       Nemohli nič viac
                     
                                                         
                       Metalový Blog - Folk metal crusade
                     
                                                         
                       Veľká noc vzišla z pohanských sviatkov
                     
                                                         
                       Stačilo p. premiér, oblečte sa prosím
                     
                                                         
                       Jiřina Prekopová: Podmienečná láska je bezcenná
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




