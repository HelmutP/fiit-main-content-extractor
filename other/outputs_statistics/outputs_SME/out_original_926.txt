
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            pavel ondera
                                        &gt;
                Recepty
                     
                 Zaváranie húb je rozprávka (fotorecept) 

        
            
                                    10.10.2008
            o
            9:11
                        |
            Karma článku:
                10.74
            |
            Prečítané 
            22782-krát
                    
         
     
         
             

                 
                    Tak na toto ročné obdobie sa nesmierne teším. Zbieranie húb v našom kraji patrí k neskoro jesennej, ale dlho očakávanej udalosti. Nažhavení hubári denne sledovali správy o výskyte húb,  kontrolovali trhoviská, najistejšie barometre rastu toľko očakávanej pochúťky. Kúpiť si zavarené či nasušené hríby dokáže každý. Bez urážky, čaro spočíva v zbieraní a samotnej úprave. Dnes sa venujem zaváraniu sucháčov z hubárskej obilnice, Záhoria.
                 

                 Prvý výlet nepriniesol také očakávané výsledky aké sa podľa počierných matrón, ledva viditeľných za kopami hríbov na trhoviskách dali predpokladať. Nuž, nemám tak početnú rodinu, ktorá sa vyrojí do lesa ako menšia rota a v rojnici doslova prešmejdí centimeter po centimetri. Chvála Bohu, že nemám. Živiť pätnásť krkov deň čo deň, to chce nielen podporu od štátu, ale hlavne nekonečnú drzosť a šikovnosť s ktorou sa títo naši spoluobčania už musia narodiť, pretože to, sa jednoducho nedá naučiť.      
   
Nuž povedzte, nestojí takýto pohľad za tých pár kilometrov v nohách? Takto by to nenaaranžoval ani Saudek. Pardón, ten je skôr na baby.  Na obrázku  Suchohríb hnedý (Xerocomus badius).            
   
Huby sa čistia priamo na mieste kde ich nájdeme. Jednak nám to ušetrí neskôr kopu času a na druhej strane, považujem za akúsi povinnosť nechať na mieste nálezu aspoň korene, podhubie, ktoré ako asi mylne predpokladám, je zárukou nového vyrašenia hríbu na mieste nálezu. Nuž čo, každý máme nejaké predstavy.  Takto to vyzerá, keď sa držíme tohoto pravidla, hríbiky už stačí ozaj iba minimálne a detailne očistiť. Úlovok rozdelíme na tie, ktoré budeme sušiť a tie ktoré chceme mraziť alebo zavárať. Do pohárov vyberáme menšie hríbiky, zdravé, tvrdé, bez náznaku plesňe.  Drobnejšie necháme celé i s hlúbikom, väčšie krájame na polovičky a štvrťky.            
   
Sprcha je blahodárny vynález. Osvieži a dokonca i čistí. To mi pripomína ten vtip, ako malý cigán vrieska, že už chce von spod sprchy. -Už som osprchovaný mama, už mi vidno sveter!  Hríbiky dôkladne prepláchneme sprchou, mydlo ani shower gel neodporúčam používať. Veľmi by to neskôr bublinkovalo, keď príde na rad prevarenie. No posuňme sa ďalej, vlastne o pár hodín späť.            
   
Cestou z hubačky sme "úplnou náhodou" míňali predavačov tiež jesennej lahôdky, burčiaku. To auto zastavilo celkom samé. Nie a nie sa pohnúť.            
   
Zázrak rodenia sa červeného vína. Sladký, až sa jazyk lepí. Toto keby poznali bohovia na Olympe, nikdy by neoslavovali "ambróziu". Tak si trošku dáme do nosa, nech nám ide práca od ruky.            
   
Do vrelej vody vsypeme dve kopcové čajové lyžičky soli a jednu kyseliny citrónovej. Kyselinu?  Že prečo?    Voľakedy som hríbiky preváral len v osolenej vode. Po naplnení pohárov a zaliatím nálevom mi doslova vyliezali von. Neskôr sa všetky namačkali na jednu hromadu, ako keď rozheganý Ikarus v bratislavskej MHD začne prudko brzdiť. Zavarené hríbiky proste plávali pod víčkom a dole bol trojcentimetrový usoplený priestor. Keď som sa snažil po otvorení jeden, dva nabrať vidličkou, zväčša som vytiahol celú zlepenú guču, v lepšom prípade som odtrhol kúsok hríbika. Kyselina dokázala zázrak. Huby sa uložili pekne rovnomerne, dajú sa po jednom vyberať. Kto by sa obavál prílišnej kyslosti, nemusí mať strach. Kyselina, neviem ako, spôsobí len zmenu konzistencie lepidla jednotlivých kúskov pri zachovaní adekvátnej chrumkavosti, bez zmeny iných chuťových vlastností.    Varíme len päť minút. Na hladine sa utvorí pena, ktorú treba odoberať sitkom alebo lyžičkou. Inak by nám obsah hrnca vykypel. Do piatich minút sa huby prestanú tlačiť k hladine, ako vidieť na obrázku zo začiatku varu.            
   
Prevarené, už "vyškolené" hríby scedíme a prepláchneme studenou vodou. Obsah sitka pred prevarením a po ňom je jasne rozdielny.            
   
Na dno pohárov (mimochodom menších, od horčice), dáme cibuľu. Používam červenú kombinovanú s bielou. Na spodok dva mesiačiky, iné môžeme dať do stredu alebo na vrch. Na jeden "moderný horčičák" (pohárik), potrebujeme minimálne 200g surových hríbikov, na to nezabudnite! Zalejeme nálevom. Ja používam normálny sladkokyslý, pripravený podľa návodu na obale, tak ako pri uhorkách. Akurát kôpor vynechávam. Nuž, ale ak by niekto chcel, môže si nielen nálev, ale i obsah ochutiť chrenom, cesnakom, chilli papričkami, videl som hríbiky s mrkvou a šparglou či olivami, fantázii sa na kulinárskom poli medze nekladú.            
   
A tu už vidieť, ako spokojne si naše sústa hovia v pohári. Nikto sa nikam netlačí, nikto nechce byť skôr prehnaný tráviacim traktom. Berú svoj údel taký ako je. Keď prídu na radu ...            
   
Mňam. Škoda že necítite tu almáziu na jazyku. No, vyzerá to, že prázdny pohár znamená sucho v studni. Idem sa pozrieť či tam ešte niečo neostalo.            
   
Ešte aspoň pár kvapiek hovorím si. Potom mi došlo, že som doma sám.            
   
To je ono. Dary slnka, prírody a Boha pretavené do opojného lahodného moku.            
   
A tu máme výsledok. Po zatvorení som hríby sterilizoval, teda skôr varil v hrnci ďalších päť minút. Neodporúčam variť dlhšie. Raz som bol na návšteve, kde sterilizovali hríby DVE! hodiny. Mal som neskôr to "šťastie" musieť takto upravený produkt okúsiť. Fuj. Čisté blato s hlienom pretavené do jednej kaše.   Ale nechcem Vám na záver kaziť chuť. Ako vidíte, hríbiky sú uložené na starej posteľnej povliečke, ktorá bude suplovať fusak. Dôvod je jednoduchý. Idú spinkať.  Pekne v teplúčku, aby postupne chladli a proces zavárania bol dotiahnutý do konca. Tam pôjdem aj ja. Pod perinku, kde proces premeny burčiaku na neškodné telesné tekutiny bude pokračovať.    Pri zbere takýchto hríbov nebezpečie otravy nehrozí. Čo sa Vám však môže stať, že sa do košíka primotá  Hríb horký (Boletus radicans). Ako už názov naznačuje, je to horká potvora. Pred rokmi sa nedopatrením dostal i k nám a žiaľ, nevšimli sme si ho. Čo horšie, usušili sme ho spolu s ostatnými a putoval do štvorlitrového pohára, neskôr do vianočnej kapustnice. Celá skončila u organizácie Vodárne a kanalizácie a žena narýchlo dovárala náhradnú kapustovú polievku. Ak sa Vám nejaký hríbik nepozdáva, treba ho nakrojiť a obliznúť. Keď sa Vám zmraští tvár, naďabili ste na záškodníka. Nuž a ak by Vám ho našli v košíku, hrozí pokuta až 1.000,-Sk za každý kus. V extrémnom prípade a pri veľkej smole tak môžete zaplatiť aj sto tisíc. Stačí nazbierať sto absolútne nepoužiteľných hríbikov.    Prajem krásnu jesennú hubačku.           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        pavel ondera 
                                        
                                            Vlak do Betlehema odchádza
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        pavel ondera 
                                        
                                            Sila Dunaja
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        pavel ondera 
                                        
                                            Ostrokyslá jednoducho a po našom - fotorecept
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        pavel ondera 
                                        
                                            Nie je súťaž ako súťaž
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        pavel ondera 
                                        
                                            Veľmi rád by som žil v štáte ...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: pavel ondera
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                pavel ondera
            
         
        ondera.blog.sme.sk (rss)
         
                        VIP
                             
     
        Egoista, ale už s tým bojujem, optimista, ale s realistickým pohľadom, snílek s nohami na zemi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    254
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3121
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Taxikárske story
                        
                     
                                     
                        
                            Realita života
                        
                     
                                     
                        
                            Kresťanstvo včera a dnes
                        
                     
                                     
                        
                            Cez objektív
                        
                     
                                     
                        
                            Dutá vŕba
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Recepty
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Bratislavské kostoly
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            www.pohrebnictvo.sk
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            A. Uhlíř; Komentáre k Biblii
                                     
                                                                             
                                            Biblia
                                     
                                                                             
                                            K. Ham; Lež evoluce
                                     
                                                                             
                                            L. Schneider; Jeruzalém ohnisko dění
                                     
                                                                             
                                            M. Gilbert; Dejiny Izraela
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio 7
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Laco Šebák
                                     
                                                                             
                                            Jozef Javurek
                                     
                                                                             
                                            Nataša Holinová
                                     
                                                                             
                                            Tibor Javor
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotogalérie
                                     
                                                                             
                                            Slovenské pohrebníctvo
                                     
                                                                             
                                            HCJB
                                     
                                                                             
                                            KZ Rača
                                     
                                                                             
                                            NKZ Bratislava
                                     
                                                                             
                                            Dejiny kresťanstva
                                     
                                                                             
                                            Kresťan
                                     
                                                                             
                                            Kresťanské fórum
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Obľúbená odpoveď politickej čvargy: „Zákon nebol porušený“
                     
                                                         
                       Vlak do Betlehema odchádza
                     
                                                         
                       Sila Dunaja
                     
                                                         
                       Pokuta pre Zelenú hliadku
                     
                                                         
                       Dojmy a pojmy fotoamatéra (1.časť)
                     
                                                         
                       Pohľadnica z Peenemünde
                     
                                                         
                       Odkaz pápežovi...nie sme charita
                     
                                                         
                       Lietajúce farby
                     
                                                         
                       Zoči-voči
                     
                                                         
                       Ostrokyslá jednoducho a po našom - fotorecept
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




