
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Turanský
                                        &gt;
                Brazília
                     
                 Brazílske školstvo 

        
            
                                    13.3.2010
            o
            21:25
                        (upravené
                13.3.2010
                o
                22:19)
                        |
            Karma článku:
                9.65
            |
            Prečítané 
            1912-krát
                    
         
     
         
             

                 
                    Odchodil som si tu v Brazílii päť školských mesiacov a ešte približne dva si odmakám. Stihol som za tú dobu vidieť takmer všetky typické školské aktivity od klasických hodín po písomky, hodiny telesnej výchovy ale aj bitky, vytopenie záchodov, „stužkovú“ a mnoho iného...
                 

                     Brazílske školstvo rozdeľujeme na dva primárne typy škôl, a to súkromné a štátne. Tie sa ďalej rozdeľujú na školy cirkevné a necirkevné. Osobne navštevujem školu cirkevnú – súkromnú. Zatiaľ som sa tu ešte s nenábožensky orientovanou školou nestretol, aj keď v mnohých prípadoch tu platí, že slovo cirkevná je len pojem, pretože celé náboženstvo sa v niektorých školách začína a zároveň končí v názve alebo svätým obrázkov nad vchodom.   Súkromné školy tu majú veľmi veľkú tradíciu a po portugalsky sa im povie „colégio“. Takisto ako aj na Slovensku, ani tu si nemôže pre svoje dieťa dovoliť každá rodina a ceny sú tu mnohokrát vyššie. Neviem, ako je to s cenou súkromných škôl na Slovensku ale tu sa platí priemerne 1500 realov za mesiac, čo je v prepočte cca. 600 eur. Veru, zadarmo to nie je. Myslím si však, že tomu dokonale zodpovedajú služby. Školy sú tu na slovenský pomer veľmi veľké. Moja má cez 4000 žiakov. Prečo tak veľa ? Vysvetlenie je jednoduché. Je to výchovná-vzdelávacia inštitúcia, ktorá sa venuje deťom od päť do osemnásť rokov. Naučia Vás jesť príborom, neskôr abecedu a násobilku a nakoniec Vás pripravia na prijímacie skúšky na univerzitu. To všetko za cenu „iba“ 84 000 eur. Na obranu však musím dodať, že v pomere výkon: cena je to prijateľné. Každá škola mám zamestnaný manažment na plný úväzok v priemere 200 pracovníkov, nerátame profesorov. Tu su zaradení iba riaditelia oddelení (majú ich tu skutočne mnoho, na menovanie by som potreboval ďalší článok), pracovníci oddelení, ekonómia, sekretárky, knihovníci, upratovači a „domingos“ – vo voľnom preklade ostraha. Je to približne desať pánov, ktorí sú rozmiestnení po celom areáli školy, komunikujú vysielačkami a „plieskajú bičom“ nad divým osadenstvom školy. Už vyššie spomenutá bitka mala celkové trvanie asi 15 sekúnd, kým nedobehli strážcovia školského poriadku. Vinníkom odviedli do miestnosti, ktorá má jediný účel. Vychovávať. Áno, takéto nespratné živly tam dostanú výprask. Na týranie sa síce môžu odvolávať ale každý rodič pri prijatí potomka do školy, podpíše papier, že aj s takouto výchovnou metódou súhlasí.   Hodiny prebiehajú veľmi podobne ako u nás, začína sa 07:15 končí 12:30, v triede je v priemere od 30 do 50 žiakov. Klasické tabule by ste tu hľadali márne, všetko nahradili tabule interaktívne, na ktorých profesor premietajú pripravenú prezentáciu (zväčša PowerPoint) a popri vysvetľovaní učiva dopisuje a dokresľuje do obrázkov dotykovým perom. Hodiny fungujú formou prednášky (predmety sú identické s tými na Slovensku), žiak dostane ku každej prednáške navyše vytlačený materiál, počúva, študuje učebnicu, robí si poznámky, na konci je priestor na otázky, domáce úlohy sa zadávajú výhradne formou prezentácii, priebežne sa píšu testy, okrem posledného ročníka, tam si už žiaci vyberajú predmety podľa univerzity, na ktorú by sa chceli dostať a raz za týždeň chodia prednášať profesori priamo odtiaľ. Vyrušovanie na hodinách (spať môžete, pokiaľ nechrápete) sa netoleruje a pri prvých náznakoch je povolaný „domingo“, ktorý Vám to už vysvetlí...svojsky ale vysvetlí...Telesná výchova sa dá opísať veľmi ľahko – futbal. Či ste chlapec, dievča alebo zvieratko, budete ho hrať jeden až dvakrát do týždňa. Odmietnutie sa berie takisto ako keby ste odriekli matematiku alebo portugalčinu. Majú tam na to svojich ľudí...Škola má vlastnú kuchyňu, ktorá pripravuje desiatu, formou ,,sandwichov´´ a teplé obedy. Školská knižnica by sa mohla rovnať ktorejkoľvek slovenskej mestskej. Areál školy je veľký, upravovaný (niekoľkými záhradníkmi), odísť počas vyučovania je nemožné, pokiaľ nie ste Henri Charriére.   O stužkových slávnostiach, štátnych školách a iných zaujímavých zážitkoch nabudúce, nerád by som Vás unavil ;-).     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Nie všetko "značkové" je naozaj "značkové" alebo o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Posledný večer
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Amazónia - Tajomná a očarujúca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Komunisti ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Z blata do kaluže
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Turanský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Turanský
            
         
        tomasturansky.blog.sme.sk (rss)
         
                                     
     
        Som študent, bývalý výmenný, ktorý si absolvoval rok v Brazílii a teraz si zvyká znovu na realitu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1528
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Brazília
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Andrzej Sapkowski - Boží Bojovníci
                                     
                                                                             
                                            Roald Dahl - Neuveriteľné Príbehy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Led Zeppelin
                                     
                                                                             
                                            Sex Pistols
                                     
                                                                             
                                            Ramones
                                     
                                                                             
                                            Rádio Expres
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




