
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Buno
                                        &gt;
                Nezaradené
                     
                 Buď si sviňa alebo nula, nič medzi tým! 

        
            
                                    1.3.2010
            o
            11:59
                        |
            Karma článku:
                10.86
            |
            Prečítané 
            3866-krát
                    
         
     
         
             

                 
                    Ak chcete na Slovensku urobiť pre ľudí aspoň niečo, musíte byť skorumpovaný. Poctivému nikto nič neodklepne – štátne granty, eurofondy, nič. Proste, jednou rukou vám niekto čosi odklepne, druhú už naťahuje za províziou... Tiež z princípu neverte najmä tým, ktorí nahlas hovoria o morálke. Bývajú to najväčší hajzli... A nezabudnite, že každý prejav vďaky, uznania, priateľstva alebo rešpektu môže byť podvodom v podvode; ťahom v ešte väčšom ťahu klamlivej a nebezpečnej hry, akú ľudia neustále hrajú. Lesť a úskok, tie sa rátajú!
                 

                 Nemohol som začať inak, ako parafrázovaním niektorých viet z novinky V tieni mafie od Jozefa Kariku. Je len pár dní na našich pultoch a autor sám tvrdí – „Úspech bude, keď prežijem!“ Po prečítaní knihy chápem, prečo to hovorí. Nemilosrdne a tvrdo totiž nastavuje zrkadlo našej spoločnosti. Áno, mafia je v skutočnosti všade a ovláda obchod, drogy, prevádzačstvo, prostitúciu, eurofondy, štátne zákazky, machinácie s nehnuteľnosťami...  Kniha nie je iba ďalším príbehom o mafiánoch, ale skutočným románom, aký na slovenskom trhu chýbal. Žiadny priemerný príbeh, ale poctivý, dobre čitateľný, ktorý zamrazí, šokuje, no knihu nechcete odložiť, kým ju nedočítate. Vyčítať sa jej dá možno nanajvýš mierne zdĺhavý úvod, v ktorom opisuje detstvo troch hlavných hrdinov – intelektuála Michal, typického bitkára Vojtecha a sexi Denisu, ktorá vyvolá iskrenie v tomto trojuholníku. Chápem však autora - na opise chcel pravdepodobne ukázať, že mafiánom sa človek nerodí a ako výrazne nás formujú zlomové momenty života (veď napríklad Michal pomáhal deťom z decáku, snažil sa byť pri skúškach na škole férový a poctivý...no zopár naozaj tvrdých životných úderov nahlodá každé presvedčenie).   Týmito pár desiatkami strán sa však oplatí prelúskať, pretože potom sa rozkrúti kolotoč, z ktorého nejednému slabšiemu žalúdku príde zle. Najmä pri opisoch nakrúcania porna či brutálnom dobití mafiána svojimi kolegami a priateľmi... Sledujeme ako Denisa odchádza do Londýna, kde ju po čase vtiahne nemilosrdný stroj pornografického priemyslu a doslova ju rozdrví. Čítame o Michalovi, ambicióznom a vzdelanom mladíkovi, ktorý sa postupne – pod tlakom okolia – mení na prefíkaného stratéga a v skorumpovanej spoločnosti sa zrazu pohybuje ako ryba vo vode. Karika drsné scénky opisuje mimoriadne plasticky, pútavo a vy hltáte jeden riadok za druhým. Dlho sa mi nestalo pri slovenskom autorovi, aby som tak žral príbeh! Autor skvele zachytil celú škálu pocitov: bezradnosť, odpor, nechuť, nenávisť, poníženie, šok...  Autor používa množstvo vulgarizmov, nadávok, no tie nepôsobia nadbytočne – takýto jazyk výborne vystihuje atmosféru románu. Cítiť z neho zrelého autora (napísal už zopár kníh a teraz ľutujem, že toto je prvá, ktorú som od neho prečítal) a človeka, ktorý rád vŕta a provokuje. Držím palce, aby nezostalo iba pri úspechu „prežitia“, ale aby sa po knihe zaprášilo, pretože si to určite zaslúži!  Prečítajte si ukážky z knihy „V tieni mafie“.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Ako šli Sovieti kradnúť nápady do Ameriky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Marián Gáborík. Inšpirujúci príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Buno
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Buno
            
         
        buno.blog.sme.sk (rss)
         
                        VIP
                             
     
         Robím to, čo milujem...po dlhých rokoch v médiách som sa vrhol na knihy a vo vydavateľstve Ikar mám všetky možnosti a príležitosti, ako si ich vychutnať naplno. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    297
                
                
                    Celková karma
                    
                                                7.34
                    
                
                
                    Priemerná čítanosť
                    2448
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nesbov SYN sa blíži. Pozrite si obálku a čítajte 1.kapitolu
                                     
                                                                             
                                            Zažite polárnu noc (nielen) s Jo Nesbom!
                                     
                                                                             
                                            Jony Ive - Geniálny dizajnér z Apple
                                     
                                                                             
                                            Konečne prichádza Láska na vlásku so Celeste Buckingham!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Tretí hlas
                                     
                                                                             
                                            Mandľovník
                                     
                                                                             
                                            Dôvod dýchať
                                     
                                                                             
                                            Skôr než zaspím
                                     
                                                                             
                                            Tanec s nepriateľom
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            M.Oldfield: The Songs of Distant Earth
                                     
                                                                             
                                            Ludovico Einaudi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Severské krimi
                                     
                                                                             
                                            Knižný svet na Facebooku
                                     
                                                                             
                                            Titulky.com
                                     
                                                                             
                                            Jozef Banáš
                                     
                                                                             
                                            Táňa Keleová-Vasilková
                                     
                                                                             
                                            BUXcafé.sk všeličo o knihách
                                     
                                                                             
                                            Noxi.sk
                                     
                                                                             
                                            Tyinternety.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Šup, šup, najesť sa. A utekať!
                     
                                                         
                       Ako šli Sovieti kradnúť nápady do Ameriky
                     
                                                         
                       Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                     
                                                         
                       Marián Gáborík. Inšpirujúci príbeh
                     
                                                         
                       Knižný konšpiračný koktail alebo čože to matky na severe miešajú deťom do mlieka?
                     
                                                         
                       Paulo Coelho
                     
                                                         
                       Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Tánina pekáreň
                     
                                                         
                       Malala - dievča, ktoré rozhnevalo Taliban
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




