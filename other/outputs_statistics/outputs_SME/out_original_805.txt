
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Čo je nové na sme.sk
                                        &gt;
                Novinky na sme.sk
                     
                 M.SME.SK - SME vo vašom mobile 

        
            
                                    20.8.2008
            o
            0:35
                        |
            Karma článku:
                10.26
            |
            Prečítané 
            33712-krát
                    
         
     
         
             

                 
                    Dnes sme spustili prvú testovaciu verziu špeciálnej stránky SME prispôsobenej pre displeje mobilných telefónov. 
                 

                                             Nájdete ju na adrese:     m.sme.sk     (Áno, môžete si ju prezerať aj v bežnom prehliadači v PC, aj keď nevieme presne, prečo by ste to robili :-)     Prečo špeciálny web?     Ak máte v mobilnom telefóne prehliadač, www.sme.sk ste doň mohli naťukať aj doteraz a stránka sa vám spravidla bez problémov zobrazila.     Nová špeciálna verzia má oproti zobrazeniu bežného webu v mobile dve zásadné výhody:      1. Pri jej využití prenesiete oveľa menej dát. Aktuálna titulná strana má vyše 60 kilobajtov v bežnej verzii, ale len 8 kilobajtov vo verzii pre mobily. Znamená to, že mobilný web je nielen výrazne rýchlejší, ale ak platíte za dátové prenosy, tak pre majiteľa telefónu aj osemkrát lacnejší.      2. Mobilná verzia je prispôsobená pre úzke displeje, takže nemusíte blúdiť po stránke doprava-doľava, aby ste si prečítali všetko podstatné - stačí sa posúvať smerom nadol. ,     Mobilnú verziu má už aj väčšina sekcií, napríklad články z počítače.sme.sk nájdete na http://m.sme.sk/?sek=pocit, články o mobiloch na http://m.sme.sk/?sek=mobil a z pekinskej olympiády na http://m.sme.sk/?sek=oh2008.      Čo zatiaľ chýba      V najbližších dňoch ešte budeme stránku ďalej vylepšovať a dopĺňať - pribudne napríklad pressfoto či komentáre a najmä diskusie k článkom. Ďalší obsah pridáme aj na základe vašich pripomienok - zaujíma nás najmä:       Aký obsah vám v mobilnej verzii chýba, čo by ste si v mobile chceli prezerať?     Ak už web v mobile používate, aké stránky si najmä prezeráte, ako často a v akých situáciách? (napr. v autobuse, na WC, atď :-)     Zdá sa vám v mobile stránka m.sme.sk prehľadná, nájdete rýchlo to, čo potrebujete?        Píšte nám na m@sme.sk alebo do diskusie. Ďakujeme.          Doplnené 20.8. 21:21: Pod každým článkom na m.sme.sk už nájdete aj odkaz na mobilnú verziu diskusie aj s možnosťou prispievať. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (43)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            SME hľadá redaktorov, online editorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Prečo pod niektorými článkami nie je možnosť diskutovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Sme.sk hľadá HTML/CSS kodéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Vitajte na úplne nových blogoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Nová titulná stránka SME.sk: Viac správ, viac čítania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Čo je nové na sme.sk
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Čo je nové na sme.sk
            
         
        novinky.blog.sme.sk (rss)
         
                                     
     
         Novinky, zlepšenia a vôbec všetko, s čím sa chceme podeliť. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    153
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6917
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Novinky na sme.sk
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




