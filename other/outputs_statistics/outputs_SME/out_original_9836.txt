
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Barlog
                                        &gt;
                Čierna kronika
                     
                 Ždiarska dolina 

        
            
                                    29.6.2010
            o
            14:33
                        (upravené
                29.6.2010
                o
                13:20)
                        |
            Karma článku:
                16.69
            |
            Prečítané 
            3907-krát
                    
         
     
         
             

                 
                    Dlhých 14 hodín a 1 200 metrov prevýšenia na 40 kilometrov dlhej túre Liptovská Teplička - Kráľova hoľa - Andrejcová - Liptovská Teplička. Celú cestu som si nadával - čo sa už ja, starý pako, snažím dokázať medzi mladými ľuďmi plnými síl a odhodlania? Napokon, ako mnohokrát v živote, spomienky na námahu, pot a otlaky odvial čas a ostala len spomienka na krásny deň v nádhernej prírode. No na jednu nepríjemnú časť dňa sa zabudnúť nedá...
                 

                 Už z hrebeňa medzi Kráľovou hoľou a Ždiarskym sedlom som občas zazrel známy obraz.         Cesty kvôli ťažbe kalamity boli miestami vyryté až tesne pod hornú hranicu lesa.      Pred Andrejcovou vyliezla holina až priamo na hlavný nízkotatranský hrebeň.      My sme tu z neho schádzali do Ždiarskej doliny.      A boli to pohľady opäť nepríjemné...                     Cesty vyryté buldozérmi vyliezali spomedzi stromov na hlavnú cestu na mnohých miestach...      ...prechádzali potokom Ždiar všade, kde sa to lesníkom hodilo.         Avízovaný front doľahol na celé Nízke Tatry a začalo vytrvalo pršať. Cesta dole rozbahnenou dolinou bola neznesiteľná.         Stále sa otvárali rovnaké obrazy.            Desať kilometrov šliapania dolinou, ktorá je bojovou líniou medzi človekom a prírodou.      Otočka nad Staníkovom. Veľkorysosť pri využití územia tu rozhodne nechýba.      Niva Ždiaru s najcennejšími biotopmi.         Takto si lesníci predstavujú praktickú ochranu prírody - podľa potreby zasypú územie nivy pri ceste so vzácnymi biotopmi...      ...aby mali dosť miesta pre vyťažené drevo. Ospravedlňujem sa za kvalitu fotiek - stále vytrvalo pršalo a voda zalievala objektív napriek snahám o jeho ochranu.      Staníkovo. Tu som definitívne zabalil fotoaparát do brašne pod pomaly premokajúcou pláštenkou. Čakalo ma ešte 8 kilometrov šliapania v hustom daždi. A - čo bolo horšie - ešte vyše 4 kilometre na hranicu - národného parku. Boli sme totiž stále na území Národného parku Nízke Tatry. Opäť som mal možnosť vidieť, ako si lesníci predstavujú ochranu prírody v praxi. Od prvého júla je vďaka nezmyselnému zlúčeniu ministerstva životného prostredia a ministerstva pôdohospodárstva práve toto etalón ochrany prírody na Slovensku.      Foto M. Barlog     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (42)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Barlog 
                                        
                                            Štefanské kvety
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Barlog 
                                        
                                            ESO
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Barlog 
                                        
                                            Nová tradícia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Barlog 
                                        
                                            Babky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Barlog 
                                        
                                            Novostavba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Barlog
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Barlog
            
         
        barlog.blog.sme.sk (rss)
         
                        VIP
                             
     
        Starnúci muž s očami dieťaťa a dušou pubertiaka, ekológ.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    307
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2581
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z prírody
                        
                     
                                     
                        
                            Zápisník strážcu
                        
                     
                                     
                        
                            Krajina
                        
                     
                                     
                        
                            Alternatívy
                        
                     
                                     
                        
                            Úlety
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Čierna kronika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vodopády v tvare vodopádov
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vojen Ložek: Zrcadlo minulosti. Česká a slovenská krajina v kvar
                                     
                                                                             
                                            Brian Greene: Elegantní vesmír
                                     
                                                                             
                                            Begon/Harper/Towsend: Ekologie jedinci, populace a společenstva
                                     
                                                                             
                                            Fritjof Capra: Bod obratu
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Pink Floyd
                                     
                                                                             
                                            Nazareth
                                     
                                                                             
                                            Beatles
                                     
                                                                             
                                            Led Zeppelin
                                     
                                                                             
                                            Deep Purple
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Juraj Smatana
                                     
                                                                             
                                            Róbert Oružinský
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Milan Hraško
                                     
                                                                             
                                            Spišiaci
                                     
                                                                             
                                            Spišiaci
                                     
                                                                             
                                            Spišiaci
                                     
                                                                             
                                            Spišiaci
                                     
                                                                             
                                            Nekriticky obdivovaný
                                     
                                                                             
                                            Alexander Ač
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Rastislav Jakuš
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Vlci - diskusné fórum pre milovníkov prírody
                                     
                                                                             
                                            ideme nahuby.sk
                                     
                                                                             
                                            Metropola Spiša inými očami
                                     
                                                                             
                                            FotoNet
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Škodná?
                     
                                                         
                       Demagógia a bludy na Chelemendikovej stránke
                     
                                                         
                       Viete, čo majú spoločné Igor Rattaj (J&amp;T) a Miroslav Trnka (Eset)?
                     
                                                         
                       Nížinné lesy Slovenska - stromy vs disturbancie
                     
                                                         
                       Akáže Banská, Šopingová Bystrica.
                     
                                                         
                       Židia si za antisemitizmus môžu sami
                     
                                                         
                       Humbug zvaný Zachráňme slovenskú pôdu.
                     
                                                         
                       Romy alebo celé zle
                     
                                                         
                       Internetové dezinformácie
                     
                                                         
                       Potočná nálada
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




