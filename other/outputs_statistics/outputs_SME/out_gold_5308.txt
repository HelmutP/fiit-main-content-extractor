

 
 
 Dnešný obed som trávil s Filipom a rozprávali sme sa o včerajšku. Bolo to strašne na dlho, stihol mi povedať o "čisle" na toaletách, že mu chvíľu trvalo, kým sa mu postavil, čo uňho za bežných okolností nie je problém, ale že inak je Nela bohyňa a noc bola ešte krajšia. 
 "V noci sme si to rozdali 2x a ráno ešte raz na rozlúčku. Ale neviem či sa jej ešte niekedy ozvem." hovorí zasnene a v jeho výraze je vidieť, že si v hlave premieta celú včerajšiu noc. 
 Akurát som mu začal rozprávať o Dare a o našej noc, keď sa k nám pridali Nina so Sisou. "Máte tu voľné, chlapci?" spýtala sa Nina a nečakala ani na odpoveď a už sedela za našim stolom. 
 Nina a Sisa boli rovnako staré sesternice, ktoré spolu bývali a zároveň si boli najlepšími kamarátkami. Sisa bola 28 ročná ekonómka, ktorá pracovala ako manažérka ekonomického oddelenia na ministerstve vnútra, bol to ten druh ženy, ktorý nás mužov moc nepriťahuje: slobodná, arogatná, prefeminizovaná žena, ktorá vie čo chce a sakra dobre vyzerá. Spoznali sme sa na jednej afterpárty asi pred šiestimi rokmi. Na Sisu musí mať človek proste náladu aby s ňou vydržal viac ako 3 minuty. Nina bola z tejto dvojice tá o niečo menej krajšia žena, ale za to, mala srdce na mieste a keď má človek chuť ísť na kávičku, je to presne ona, koho by na tú kávu zavolal. Vyštudovala pôvodne sociálnu prácu, ale vďaka svojmu bývalemu priteľovi-pseudomafiánovi si otvorila butik s drahými handrami. 
 "Vieš čo Filip, večer než pôjdeme s Tonym a Martinom na pifko, zastav sa u mňa, dorozprávam ti to a pôjdem za nimi." navrhol som mu a nevšímal som si votrelkyne pri našom stole. 
 "Adam, pál do piče! Ja sa kvôli tvojej nočnej avantúre nebudem jebať hodinu k tebe a ďalšiu hodinu od teba do Budíka, ani za toho Boha." zostra mi vyplul do tváre Filip. 
 "Akože Adamko, my môžme odísť, ale dobre vieš, že pred nami sa nemusíš hanbiť." pridala sa k Filipovi Sisa. 
 "Sisa daj si pohov! Za to, že sme spolu raz spali a to v dosť podnapitom stave, nemusím pred tebou rozoberať s kým a čo som robil včera v noci." odvrkol som im, pretože ma fakt, že opúšťala moja dobrá nálada a na Sisu som dnes náladu nemal. To určite viem. 
 "Dajte mu pokoj" zastala sa ma Nina. 
 "Ďakujem Ninka, aspoň že ty ma máš rada." 
 Ešteže sme s Filipom pažraví, alebo ako my hovoríme - gourmeti s chuťou do jedla, a že sme už dojedli našu čínu a mohli odísť z obeda. Pri odchode som ešte Nine stihol povedať, že by sme mohli zbehnúť na kávu nejak v nedeľu pred obedom. Povedala, že sa mi v priebehu soboty ozve lebo zajtra ide na nákupy do Viedne a príde až v sobotu okolo obeda. 
 Na parkovisku sme sa s Filipom rozdelili, on šiel ešte do práce a ja už som pre dnešok skončil, ešteže mám flexibilnú pracovnú dobu. 
 Na križovatke na "Račku" som si našiel správu od Dary. písalo sa v nej: Dnes končím v práci o 17:00 a o 18:30 by sme mohli dať fitko a potom zmrzlinu pri telke u mňa doma…hm, čo ty na to? 
 Neviem ako jej vysvetlím, že dnes večer určite nemôžem, pretože idem na pivo s kamarátmi, z čoho vyplýva, že uprednostním kamarátov pred ňou a že to čo sa stalo včera v noci je pre mňa momentálne uzavretá kapitola. Odložil som mobil na sedadlo spolujazdca a esemeska mi pomaly vyfučala z hlavy. 
 Išiel som za Björnom, pretože moja finančná situáacia je zlá a potrebujem rozmnožiť mojich zvyšných jurášikov. 
 Björn bol finančný poradca, ktorý mi už neraz pomohol z najhoršieho,netvrdím, že vždy to bolo čisté, ale topiaci sa aj slamky chytá. Stravil som v jeho kancelárii približne 2 hodiny a výsledkom bolo, že do pondelka by som mal mať na účte asi o 2000 eur viac. Keďže som cítil veľkú vďačnosť, zavolal som ho večer s nami na pivo, aj keď viem, že sa s Martinom nemajú moc v láske, pretože asi pred rokom Björn prebral Martinovi jeho veľkú lásku ako on sám tvrdil. Ale dlho spolu neboli, pretože aj Björnovi ušla asi po 4 mesiacoch s nejakým Talianom a od vtedy ju nikto nevidel. 
 Popoludní som sa mal stretnúť s Gabi, bola to jedna zo žien môjho života, nie ako partnersky, ale priateľsky. Ako je u mňa zvykom, meškal som a to som ešte nebol ani na ceste. Vytočil som ju a hneď po zdvihnutí som jej nedal šancu na pozdrav a hneď som na ňu spustil: "Gabi, prepáč, budem meškať tak 15 až 20minút, ani za toho Boha som nemohol nájsť čisté ponožky a ešte aj myčka sa mi pokazila a vytiekla. Kurva, keď sa darí, tak sa darí." a všetko som to stihol na jeden nádych a s veľkým dôrazom na slovo - kurva. 
 "V pohodičke, Adamko, vieš, že ťa už poznám a kebže nemeškáš, tak si to neni ani ty." ironicky mi odpovedala Gabi. 
 Nemám rád keď mi niekto povie Adamko s takým tónom ako to vie iba Gabi. 
 Sedeli sme v lacnej študáckej kaviarni obkolopení rôznorodou zmesou ľudí. 
 Gabi potrebovala radu, či si má booknuť letenku to Paríža a ísť za Matiasom, s ktorým chodila asi pred 3 rokmi, ale až teraz si uvedomila, že ho chce a že je schopná opustiť všetko čo má a čo si vybudovala. Matias sa medzičasom odsťahoval do Paríža kde pracuje ako v inštitúte architekta mesta Paríž. 
 Nevedel som je dať konkrétnu odpoveď, pretože neviem čo by som robil ja na jej mieste. 
 Pri rozlúčke mi sľúbila, že keď bude definitívne rozhodnutá, že mi dá vedieť. 
 Keď som čakal na taxik, čo ma mal odviesť za chalanmi a za zaslúženou odmenou po celom dni - oroseným pivkom, zazvonil mi telefón. Už podľa melódie som vedel, že je to Filip. 
 "Čo je ty vajcožrút, čo chceš?" spýtal som sa ho. 
 "Daj pokoj. Ja len že dnes nejdem, nestíham a mám niečo dôležitejšie, idem za jednou holkou." kontroval mi Filip. 
 "Do piče Filip, neser ma. Netrhaj partu kvôli dákej kurve! Alebo vieš čo? Rob čo chceš, ale chalanom to daj vedieť sám." nasrane som vyprskol. 
 "Adam pochop ma, ty by si urobil to isté pri prvej príležitosti, vieme to obaja!" kričal do telefónu Filip. 
 "Okej, ale zajtra mi porozprávaš čo a ako! Tak si to uži, vyťaž z toho čo to dá! Čau." 
 "Serko a pozdrav chlapov, že im to vynahradím inokedy!" 
 Taxik medzitým dorazil a odviezol ma do Budíku a hneď som sa s taxikárom dohodol, že nech o pol 12 príde po mňa a na displeji môjho mobilného aparátu blikala číslo 17. Dobre som vedel čo to znamená a kto sa mi 17x pokúšal dovolať. Bola to Dara. 

