
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                jedlo, bylinky
                     
                 Máš chuť majoránky, lásko má... 

        
            
                                    26.5.2010
            o
            8:58
                        (upravené
                26.5.2010
                o
                9:53)
                        |
            Karma článku:
                8.16
            |
            Prečítané 
            2560-krát
                    
         
     
         
             

                 
                    spieval nám dlhé roky nebohý Karel Zich... Vždy, keď som tú pieseň počúvala, tak som majoránku naozaj cítila, pretože vyvolať si v predstavách  jej vôňu, či výraznú chuť, naozaj nie je pre toho, kto ju pozná, žiadny problém... Poďme sa na tú ospevovanú majoránku (Majorana hortensis) pozrieť trochu bližšie.
                 

                 
  
   Ak varíte, čo i len "rekreačne", tak predpokladám, že medzi koreninami, by som u vás našla aj majorán. Ide o tradičnú koreninu, ktorá sa v slovenskej kuchyni používa hojne a už celé roky.   Viete si predstaviť jaterničku, zemiakové placky, zakáľačkovú polievku, guláš, držkovú alebo strukovinové polievky bez majoránu? Ja nie. Táto bylinka obsahuje mimoriadne silnú silicu, chutí jemne sladkasto. Jedlu dodá výraznú chuť a tak si myslím, že majorán možno milovať alebo nenávidieť. Niečo medzi tým  vari neexistuje. Pri varení by sme mali dodržiavať jednu zásadu.   Majorán sa nemá variť, treba ho pridávať do hrnca,  až do hotového jedla, alebo pár sekúnd pred vypnutím tepelného zdroja. Ináč nám hrozí, že majorán zhorkne a jedlu skôr ublíži ako osoží.   Majorán je zároveň aj liečivka, ktorá obsahuje triesloviny, horčinu a vitamín C. Má silný vplyv na nervovú sústavu a tak sa odpradávna používa pri nespavosti, migréne, pri pocite slabosti či na zmiernenie stresu. Majorán však pôsobí aj proti kŕčom, hnačke, nadúvaniu (preto sa pridáva ku strukovinovým jedlám). Mamičky vari vedia, že má priaznivý vplyv na tvorbu mlieka. Čaj z byliny osladený medom je hotovým balzamom pre hlasivky. Pomáha aj v boji proti kašľu, zvonku lieči ťažko hojace sa rany či zápaly. Keď bola dcéra malá často som varievala tento voňavý čajík. Chutí výborne a tak som nemala problém s tým, žeby ho nechcela piť. Jeho utišujúce účinky na kašeľ mám rokmi overené. V lekárňach je možné zakúpiť aj majoránovú masť. Voňavá bylinka našla svoje uplatnenie aj v kozmetickom priemysle, pridáva sa do mydiel, či kúpeľových zmesí.   Majorán si môžeme vypestovať aj doma v rámci hoci aj "parapetnej" záhradky. Semienka sadíme opatrne, prikryjeme ich len tenkou vrstvou zeminy. Pozor na hrudky, lebo aj tie môžu zabrániť rastu. Substrát by mal byť dobre prekyprený. Sadí sa v máji a tak to ešte stihnete!   Ak vás neoslovuje pestovanie  vlastných byliniek (čo je škoda), tak majorán zakúpite aj v sušenej forme. Čoraz častejšie sa zjavuje aj predaj v črepníku. Bez námahy si domov prinesiete krásnu zelenú rastlinku, okamžite vhodnú k použitiu.   Majorán je nepostrádateľným korením do rôznych polievok, do paradajkových jedál, je súčasťou údenárskych výrobkov, je vhodným doplnením tučného mäsa, jahňaciny, bravčoviny, hovädziny aj hydiny. Ide o silné korenie, ktoré postačí v jedle ako jediné dominantné, doplnené trebárs mletým čienym či bielym korením. V opatrnej dávke sa znesie s tymiánom, šalviou či rozmarínom. Ale pozor, niekedy menej je viac!   Pridám návod na prípravu záparu (čaju):   jednu čajovú lyžičku na pohár horúcej vody, nechať cca 5 minút lúhovať a piť 2x denne. Ak chceme použiť na vonkajšie účely, pripravíme si silnejší koncentrát. Vhodným doplnením lahodného nápoja je med.   Málokto vie, že majorán je vhodný aj k zemiakom. Poskytnem vám recept na zemiakovú kašu s krúpkami a majoránom:   do klasicky pripraveného pyré pridáme vopred uvarené, scedené krúpky, rozotretý cesnak, čierne korenie a majorán. Podávame posypané s opraženou cibuľkou (prípadne aj slaninkou) a ako prílohu odporúčam zeleninový šalát. Táto kaša chutí výborne aj ku údenému mäsku.   Ak pri jedle voňajúcom touto čarovnou bylinkou, zanôtite svojej láske, že  tiež vonia , či chutí majoránkou, myslím, že sa len poteší. Prípadne preskočte na ľudovú nôtu a zaspievajte si Majorán, majorán, zelený majorán... a svet bude hneď o čosi veselší!       Foto: zdroj internet 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (209)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




