
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Gočová
                                        &gt;
                Festivaly
                     
                 Showdance v znamení filmových hitov 

        
            
                                    7.4.2010
            o
            16:45
                        |
            Karma článku:
                4.25
            |
            Prečítané 
            1416-krát
                    
         
     
         
             

                 
                    Potom, čo sa Slováci dosýtosti nabažili  speváckych show, sa dostalo na rad aj z iného súdka. Keď prestali  spievať aj tí, ktorí nikdy nemuseli, začalo sa tancovať. Preteky  o najlepšieho tanečníka sa začali medzi dvoma najfrekventovanejšími  televíziami. Zatiaľ čo jedna ponúka všetko, čo k spoločnekým tancom  patrí, tá druhá oblečie interpretov do teplákov a nechá ich váľať sa po  pódiu vo víre profesionálov... Človek by si myslel, že v tom seriálovom  boome nebudú herci vedieť, kde im hlava stojí, nie ešte v tanečných  show.
                 

                 Keď je z herečky moderátorka..    Prirodzená improvizácia by mala patriť do balíčka talentu každého herca či herečky. Viac by sa však očkávalo od sympatickej Zuzany Fialovej. Celý jej moderátorsky výkon vyznieva neisto, umelo a lacne. Neviem, či celá show má vyzerať a´la „len aby niečo bolo“, ale jej vystupovanie k tomu pridáva na efekte. Ako pri žrebovaní tomboly na obecných pretekoch v behu do vrchu oblečená vo veciach z čínskeho upadajúceho trhoviska. Ten, kto má na starosti kosýmy by mohol konečne otvoriť oči. Pre takú krásnu ženu ako je Zuzana by mohlo byť urážkou to, do čoho ju navliekajú. Minulý rok to boli róby, keď na moderátorskom poste vystriedala prirodzene ironickú Adelu Banášovú. V jej novej práci sú to šaty, ktoré by si veru ani Lady Gaga neobliekla..       Jej oporným pilierom je Matej „Sajfa“ Cifra. Z jeho poznámok by si človek mohol vysveliť, že má v zuboch konkurenčnú televíziu. Predsa len označiť za televízneho škriatka, kaziaceho prenos „markizáčik“ bolo neprofesionálne a hodné desať ročného chlapca závidiaceho lízanku Banášovej.   Hviezdna porota   Veselá kopa Gabika Dzuríkova, s materinským dôrazom Hana Gregorová, miestny kat Libor Vaculík... a ťažký frajer Tony Moisés. Prvé dve menované sú herečky v domovskej telvízii a aj keď sa na tanec radšej pozerajú, ich hodnotenie je prijateľné, milé a povzbudivé. Libor je jojkársky „pravdomluvec“, a keďže sa v dvoch tretinách svojho života venoval tancu má výhradné právo kritizovať, lebo svojimi vyjadreniami podáva objektívnu kritiku.   Prečo ale Tony Moisés? Nemáme vari v našej krajine niekoho.. kto sa aspoň viac podobá na tanečne znalého? S výrazom boha akoby na tróne tanečného olympu sediaci zhadzuje tanečníkov a sám by si pritom mohol uvedomiť, že od roku 1989 prebehlo more času a on už dávno nie je tým, čím možno niekedy bol. Pri jeho ironických poznámkach by som ho chcela vidieť tancovať s tým klobúkom na hlave a bielymi kuravičkami. Hádzať sa o zem ako pred dvadsiatimi rokmi. Podobalo by sa to asi na chrliacu sa lávu zo sopky Vezuv.   Filmové kolo   Kto by neobľuboval hity z Hriešneho tanca, Hair či Pomády?  Druhé kolo sa nieslo v duchu filmových muzikálov a týmto výberom sa nedá nič pokaziť. V porovnaní s Let´s Dance je to viac odviazané, živé a nechýba tomu dávka energickej energie. Za obeť padol herec Braňo Bystriansky, ktorý si v nasledujúcom kole už nezatancuje. „Poplašený“ barman z Paneláku sa po svojom ťarbavom výkone už v show naukáže a bude mať aspoň viac času na na nakrúcanie.   Do erotického ovzdušia nás nasiakla Veronika Paulovičová. Tanec v extravagantom a odvážnom kostýme nadchol nejedného muža v publiku. A aj to jej dosť pomohlo z posledného miesta v rebríčku.       Na situácie by sme sa nemali dívať len čierne alebo bielo. Výkon Zuzany Mauréty však patril k tým jednofarebým, jednotvárnym a nudným. Celkovo jej vystupovnie pôsobí utrápene a prudérne. Tejto muzikálovej herečke akoby miestami chýbal život. Niekomu sa do vienku neušlo. Kto vie..možno prekvapí. Kontrastným spôsobom k nej však vystúpila charizmatická Diana Mórová. „Baby“ nesedela v kúte a prítomných očarila zdvíhačkou z legendárneho Hriešneho tanca. Ako povedala Gabika Dzuríková, keby o nej v tom čase vedeli, Jenifer by sa v tomto filme neukázala.       Prvé miesto obsadil Peter Batthyány. Banán z Mafstory uviedol ukážku z muzikálu West Side Story. Diváci však asi hlasujú za jeho herecké výkony, nie tanečné. Na pódiu nebol žiadny z „tryskáčov“. Skôr tatko jedného z nich..       Je otázkou, ako tieto showky  prispievajú k slovenskej kultúre. Herci strácajú na kvalite nedostatkom času. Ich výkony v divadle neovplyvňuje len spomínaný filmový boom, ale aj súčasné tanečné súťaže. Na ľudí to však môže pôsobiť pozitívne, privedú svoje ratolesti hýbať sa a neskysnú pri počítačových hrách...   Text: Katarína Gočová   Foto: Zdenko Hanout 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            NOVAROCK 2010: Festivalová sezóna otvorená
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Undergroundová žúrka na BUM 2010 v čiernobielých fotografiách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Eurovision Song Contest 2010 – A víťazom sa stáva!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Eurovision Song Contest 2010 - Neprekvapivé druhé semifinále
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Ivan Janko: Ľudia na Haiti pomoc potrebujú teraz
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Gočová
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Gočová
            
         
        gocova.blog.sme.sk (rss)
         
                                     
     
         ... osoba, ktorú zaujíma dianie na kultúrnej scéne a celkovo život, spájajúci ľudí a kultúru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Festivaly
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Koncerty
                        
                     
                                     
                        
                            Reportáže
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Blog - Roberta Dolinská
                                     
                                                                             
                                            Blog - Zdenko Hanout
                                     
                                                                             
                                            Blog - Jana Šlinská
                                     
                                                                             
                                            Hudobný portál MUSIC4U.sk
                                     
                                                                             
                                            Reality Prešov
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




