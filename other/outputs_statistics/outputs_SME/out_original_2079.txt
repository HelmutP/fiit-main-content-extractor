
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Dostál
                                        &gt;
                Politika
                     
                 1300 dní s Ficom, Slotom a Mečiarom 

        
            
                                    19.1.2010
            o
            16:29
                        (upravené
                19.1.2010
                o
                17:18)
                        |
            Karma článku:
                19.60
            |
            Prečítané 
            8965-krát
                    
         
     
         
             

                 
                    V sobotu to bude 1300 dní, čo Slovensku vládnu Fico, Slota a Mečiar. Nešťastných 13 x 100 dní, v ktorých sa korupcia snúbi s boľševickým myslením a nekompetentným babráctvom. A aby sme na to náhodou nezabudli, už v prvých dňoch roku nám to koaličníci pripomenuli spŕškou neprehliadnuteľných káuz.
                 

                 
www.17november.sk 
   Snáď najlepším stelesnením neduhov najhoršej možnej zostavy bolo zavádzanie diaľničného mýta. Na počiatku stál výber najdrahšej ponuky. Čo, ak nie korupcia a klientelizmus, vás presvedčí rozhodnúť sa pre drahšiu ponuku, keď máte k dispozícii lacnejšiu? Žeby kvalita a technická dokonalosť systému? Práve "kvalita" a "technická dokonalosť" mýtneho systému sa ukázali v plnej nahote v prvých dňoch jeho prevádzky. Protesty dopravcov proti zbabranému mýtu prinútili Fica zaradiť spiatočku. Aj jeho čiastočné ústupky však sprevádzali silácke rečí a boľševické táranie o ohrození demokracie za trápneho asistovania najvyšších ústavných činiteľov.   Slovensko sa pod Ficovým vedením konečne dočkalo aj pozornosti celosvetových rozmerov. Vďaka spackanej policajnej akcii s výbušninou na linke Poprad - Dublin. A opäť tu máme ako vo výklade: Amatérske babráctvo. Bagatelizovanie problémov. Zatĺkanie do poslednej chvíle. Neschopnosť priznať si chybu. Neschopnosť vidieť problém v zneužívaní civilistov na cvičenie policajných psov. Medzinárodná hanba.   A do tretice Tomanová a jej slávne sociálne podniky. Na problémy okolo nich upozornila minulý rok v relácii STV Reportéri redaktorka Martina Kubániová. Najprv bolo na priamy zásah riaditeľa STV Štefana Nižňanského stopnuté odvysielanie Kubániovej reportáže. Potom síce pod verejným tlakom bola odvysielaná, ale onedlho bola stopnutá celá relácia. Minulý týždeň sa prevalilo, že vážne nedostatky v Tomanovej sociálnych podnikoch našla aj Európska komisia. Jeden musel byť dokonca zrušený. Na druhý deň po tom, ako sa verejnosť dozvedela tieto informácie, sa Martina Kubániová dozvedela, že STV s ňou nepredĺži zmluvu. Lepšiu symbolickú bodku ako vyrazenie redaktorky, ktorá na celý problém upozornila, si snáď ani nebolo možné vymyslieť.   Na novoročných kauzách je pekné ešte niečo - odohrávajú sa v rezortoch Ficovho Smeru. Nie pod kuratelou Mečiara a Slotu, ktorí svoje "schopnosti" naplno predviedli už v 90. rokoch v trojici s Jánom Ľuptákom. Vážny, Kaliňák aj Tomanová sú Ficovi ľudia. Predstáva, že problémom Fica sú len zlí koaliční partneri, je falošná. Táto partia patrí dohromady. Vládne nám už 1300 dní. A nivočí túto krajinu.   My, čo túto krajinu máme naozaj radi, nemôžeme mať radi túto vládu. A dáme jej to najavo presne 1300 dní od jej nástupu. V sobotu 23. januára 2010 o 15:00 na Námestí SNP v Bratislave. Za účasti predsedov šiestich opozičných strán. Šanca na zmenu existuje. O výsledku parlamentných volieb ešte nie je rozhodnuté. 1300 dní s Ficom, Slotom a Mečiarom naozaj stačilo.   Pozvánka: Máme radi túto krajinu, nemáme radi túto vládu 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (129)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Konečne sa dozvieme mená všetkých členov Smeru
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Niekoľko viet o Táni Kratochvílovej
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Smeráčina á la Brixi: Len si tak niečo vybaviť
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Fico nám berie peniaze a dáva ich svojim kamarátom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Dostál
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Dostál
            
         
        dostal.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som predsedom OKS. V novembrových voľbách kandidujem v bratislavskom Starom Meste do miestneho i mestského zastupiteľstva. Ako jeden z členov
Staromestskej päťky. Viac na www.sm5.sk. 

  

 Ondrej Dostál on Facebook 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    202
                
                
                    Celková karma
                    
                                                15.07
                    
                
                
                    Priemerná čítanosť
                    5758
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Smrteľne vážne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                             
                                            Ivan Kuhn
                                     
                                                                             
                                            Juraj Petrovič
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Michal Novota
                                     
                                                                             
                                            Ondrej Schutz
                                     
                                                                             
                                            Tomáš Krištofóry
                                     
                                                                             
                                            Peter Spáč
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            INLAND
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Konzervatívny inštitút M.R.Štefánika
                                     
                                                                             
                                            Peter Gonda
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 robí Ficovi reklamu
                     
                                                         
                       V TA3 Fica hrýzkajú slabúčko
                     
                                                         
                       O tom, čo platí "štát"
                     
                                                         
                       Konečne sa dozvieme mená všetkých členov Smeru
                     
                                                         
                       My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                     
                                                         
                       Smeráčina á la Brixi: Len si tak niečo vybaviť
                     
                                                         
                       Potemkinov most v Bratislave
                     
                                                         
                       Fico nám berie peniaze a dáva ich svojim kamarátom
                     
                                                         
                       Výsmech občanom v priamom prenose alebo .... Mišíková v akcii ...
                     
                                                         
                       Ficov podrazík na voličoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




