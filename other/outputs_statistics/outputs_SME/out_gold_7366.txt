

 Juniperus communis L. je ker, prípadne stromovitá drevina v rodnom jazyku borievkou obyčajnou zvaná. Jej plodmi sú borievky, krásne guľôčky modročiernej farby . 
 V stredoveku, v období častých epidémií sa borievky  používali ako silný dezinfekčný prostriedok. Kto denne žuval niekoľko bobulí borievok, bol uchránený pred fatálnymi následkami opakujúcich sa epidémií. Aj dnes môže byť borievka nápomocná v prevencii, trebárs v čase, keď vystrája chrípka. Stačí ráno nalačno rozžuvať 5-10 bobulí. Chlapi sa už asi  usmievajú, že majú teraz dôvod, prečo si ráno dať hlt borovičky bez nutnosi žuvania, cha. 
 Borievka je silne močopudná, vyvoláva potenie, odstraňuje tráviace problémy, odstráni nadúvanie, dezinfikuje tráviaci trakt, pomáha v boji s kašľom, s kožnými chorobami. Silný odvar je použiteľný aj ako dezinfekčná ústna voda. Vyrába sa z nej éterický olej a ten je silným utišujúcim prostriedkom pri reumatických bolestiach, pri bolestiach svalov. Jeho stopu nájdeme v krémoch a géloch, určených proti bolestiam pohybového aparátu. Kúpeľ s niekoľkými kvapkami borievkového kúpeľového oleja urobí divy. Éterický olej môžeme  použiť  aj v arompaterapii, pár kvapiek do aromaterapeutickej lampy a môžete si vychutnať jeho úžasnú vôňu a liečivé účinky. Borievka je  aj súčasťou močopudných čajových zmesí. 
 Keď to zhrniem, tak lekárnictvo a likérnictvo sú hlavné oblasti, kde sa tieto bobuľky hojne využívajú. 
   
 Borievka obsahuje silice, horký juniperín, živice, flavonoidy, živice... 
 Ide o dekoratívnu rastlinu, ktorá môže upravená ako bonsaj skrášliť nejeden príbytok. 
 Borievka sa odpradávna používa ako korenina, či už ako celé bobule alebo v mletom stave. Ochucuje prívarky, šaláty, kapustu, mäsové jedlá  - predovšetkým divinu, je súčasťou marinád a rôznych koreninových zmesí. 
 Blíži sa sezóna čoraz populánejších grilovačiek, odporúčam použiť túto voňavú koreninu. Dodá mäsu jemnú aromatickú chuť a pošteklí chuťové poháriky jedákov. 
 Divinu je vhodné pred tepelnou úpravou marinovať. Chutnú a jednoduchú  marinádu pripravíme z oleja, korenia na divinu, borievky a červeného vína. Aj do marinád iných druhov mäsa môžeme borievku použiť. Ja robievam utopence a zvyknem do nálevu pridať teiž pár bobuliek jalovca. Keď varím údené mäsko, tiež ho pridávam do vody. 
 Najjednoduchším použitím borievky ako liečiva je zápar z celých, prípadne rozdrvených suchých plodov. Zápar lieči reumatizmus, dnu a tráviace problémy. 
 Som presvedčená, že borievka nie je bežnou súčasťou domácich zásob korenia. Uznávam, že aj bez nej môže kuchyňa v pohode existovať. Čo však nie je , môže byť... Stačí zájsť do potravín, pristaviť sa pri koreninách a siahnuť po borievke. Za lacný peniaz si obohatíte svoju zbierku korenín a zaiste nájdete spôsob ako ju zužitkovať, ako obohatiť novou chuťou klasické jedlo, prípadne ako si pomôcť pri neduhoch, od ktorých nás môžu tieto aromatické guľôčky oslobodiť. 
 Na borievku mám jednu milú spomienku z môjho detstva. Moja stará mama zvykla len tak hodiť na pec pár guľôčok jalovca. Doteraz cítim tú krásnu vôňu, ktorá sa rozliala po celom dome. Teraz už viem, že okrem príjemného prírodného osviežovača vzduchu možno aj dezinfikovala prostredie... Vedomosti našich babičiek, starých mám ma často prekvapujú. 
 ...hm, presvedčila som vás? Alebo radšej zostanete verní jej tekutej podobe? Ak je to s mierou, nebudem okolo toho "moralizovať" a doložím: "Na zdravie, priatelia!" 
   
 Foto: zdroj internet 
 PS: Ako vždy, teším sa na diskusiu. Predpokladám, že bude zdrojom nových inforámcií, rád a návodov ako sa dajú tieto cenné guľôčky zužitkovať. Vopred ďakujem, že si nájdete čas a ste ochotní podeliť sa aj s ostatnými. 

