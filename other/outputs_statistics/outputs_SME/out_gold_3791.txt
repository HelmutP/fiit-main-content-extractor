

 Tento rozhovor mi pripomenul podobnú situáciu dámy menom Nathalie. Prečítajte si jej krátky príbeh a dozviete sa, že keď zahodíte strach, môžete začať podnikať kedykoľvek. Nápad určite príde a keď príde, proste spravte prvý krok! 
 - Čo budem teraz robiť? Kde začať? Päťdesiatka na krku... 
 Hlavou jej vírili takéto myšlienky už niekoľko dní. V práci dostala padáka. Cítila sa mizerne. 
 V tom čase bol jej muž na služobke v Maďarsku. Cestou domov sa zastavil na pumpe, aby sa osviežil. Potreboval dobiť baterky, ale mal problémy so žalúdkom, nemohol si dať hocičo. Oči mu padli na červenú okrúhlu plechovku.  - Na pumpe predávajú box na topánky? Taká bola jeho prvá myšlienka. 
 - SCHO-KA-KOLA. Energetická čokoláda... káva... kola orechy... Hmmm... 
 - Len si ju kúpte, je skvelá! Radil mu pumpár.  - Kamionisti sa idú za ňou potrhať. A aj ja si ju kupujem, vždy keď ideme na cesty. 
 - Nathalie, pozri, čo som objavil? Cestou domov som sa zastavil na pumpe, bol som vyčerpaný, chcel som si kúpiť niečo na povzbudenie.  - Čo to je? Box na topánky? 
 Zasmial sa tomu.  - Je to energetická čokoláda. A tá krabička má svoje čaro, dá sa otvárať jednou rukou. Ochutnaj! ... 
 No a takto sa začal jej príbeh podnikateľky. Dostali podnikateľský nápad. Objavili skvelý produkt, ktorý naozaj funguje. Na slovensku sa táto energetická čkoláda nedala kúpiť. Zistili, že SCHO-KA-KOLA má dlhoročnú tradíciu. Kedysi o nej hovorili aj ako o starej vojenskej čokoláde. Rozhodli sa, že ju začnú sami distribuovať. Poste to skúsia. Potom už išli veci rýchlo. Založili firmu, kúpili prvé zásoby tovaru a hor sa do práce. 
 Ja sama som si už SCHO-KA-KOLU kúpila na pumpe aj v lekárni. 
 Dámy a páni, prosím, ak ste prišli o prácu, nesmúťte príliš dlho. Ak si neviete nájsť nové miesto, skúste sami podnikať. Začnite v malom. Čo môžete stratiť? Ak si s tým neviete poradiť, určite sa vo vašom okolí nájdu ľudia a inštitúcie, ktoré vám rady pomôžu. 
 Že ste to ešte nikdy nezažili? No a? Život je predsa zmena. 
 Že to môže dopadnúť hrozne? Áno, môže, ale radšej si vyberte tú variantu, že to môže dopadnúť skvele. 
 Prikladám vám ešte zopár linkov, kde nájdete zaujímavé informácie: 
 http://nathalie.sk/schokakola - stránka energetickej čokolády 
 http://www.nadsme.sk/ - stránka Národnej agentúry pre rozvoj malého a stredného podnikania 
 http://www.employment.gov.sk/ - Ministerstvo práce, sociálnych vecí a rodiny 
 http://mp.msponline.sk/ - mesačník Podnikanie 
 http://www.zivnostnik.sk/ - portál pre živnostníkov 
 http://www.szco.sk/ - portál pre samostatne zárobkovo činné osoby 
 http://www.podnikam.sk/ - informácie pre podnikateľov 
 http://www.uspesne-podnikanie.sk/ 
 http://www.porada.sk/ - diskusný portál aj o mnohých oblastiach podnikania 
 http://www.strukturalnefondy.sk/ - grantové programy 
   
 Veľa šťastia! 
   
   

