

 Vonku je dážď, zima a vietor, meravému nebu chýba priestor a tak si radšej rýchlo kýchlo. Svet trápi deravý pršiplášť, dáždniky chodia každý zvlášť, ich skackavé prítmie v rytme skrýva schúlené príbehy ľudí, v ktorých dážď znova zobudil sychravosť. Dážď, no tak už dosť! 
   
   
 ............v          m         a          s          d          i           s          v          s 
 ............o          e                      v          á          ch        k                      y 
 ............n          r          t           e          ž                      r          k          ch 
 ............k          a          a          t           d          s          ý          t           r 
 ............u          v          k                      n          k          v          o          a 
 .....................é                      t           i           a          a          r          v 
 ............j           m         s          r          k          c                      ý          o 
 ............e          u          i           á          y          k          s          ch        s 
 ........................................p                      a          ch                    ť 
 ............d          n          r          i           ch        v          ú          d.........!  .  
 ............á          e          a                      o          é          l           á 
 ............ž          b          d          d          d                      e          ž          D 
 ............ď         u          š          e          i           p          n          ď         á 
 ...............................e          r          a          r          é                      ž 
 ............z           ch       j           a                      í                       z          ď 
 ............i           ý                      v          k          t           p          n 
 ............m         b          r          ý          a          m         r          o          t 
 ............a          a          ý                      ž          i           í           v         a 
 ..............................ch .... .. p        d          e          b          a          k 
 ............a          p          l           r          ý                      e 
 .....................r          o          š                      v          h          z          u 
 ............v          i                       i           z                      y          o         ž 
 ............i           e          k          p          v          r                      b 
 ............e          s           ý         l           l           y          ľ  ....  .  u          d 
 ............t           t          ch        á          á          t           u          d          o 
 ............o          o          l           š          š          m         d          i           s 
 ............r          r          o          ť          ť          e          í           l           ť 
   
 ............!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
   
   
   

