
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Lužinský
                                        &gt;
                Viera
                     
                 ´Oni mali peniaze´ 

        
            
                                    3.4.2010
            o
            5:34
                        (upravené
                3.4.2010
                o
                9:48)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1764-krát
                    
         
     
         
             

                 
                    Stvoriteľ vesmíru a všetkého života takto varoval Izrael, svoj ´vyvolený národ´, čo sa s nimi stane, keď porušia zmluvu, ktorú sa zaprisahali dodržiavať:
                 

                 "Istotne ma opustia a porušia moju zmluvu, ktorú som s nimi  uzavrel. Preto skutočne v ten deň vzplanie môj hnev proti nim a  istotne ich opustím a skryjem pred nimi svoju tvár a stanú sa niečím,  čo má byť strávené; a prídu na nich mnohé nešťastia a tiesne."  - 5. Mojžišova 31:16-17.   História dokazuje, že tak sa stalo mnohokrát nielen pr.n.l., ale tiež od toho ´dňa´, keď izraelský národ odmietol a nakoniec zabil Božieho Syna, Ježiša Krista, ktorého Všemohúci Stvoriteľ, Jehova Boh poslal ako ich Záchrancu a Vykúpiteľa. Už v roku 70., n.l. rímske vojsko zničilo Jeruzalém s jeho chrámom a približne milión obyvateľov a ďalších  asi stotíc zajatcov, ktorých popravovali na ceste do Ríma ako výstrahu a varovanie pre budúcich vzbúrencov proti ríši. Lenže ´nešťastia a tiesne´ pre ´vyvolený národ´ pokračovali, až vyvrcholili v Holokauste, kde ich údajne asi 6 miliónov stratilo život. A zaujímavý detail spomenula i vo  svojom článku blogerka na sme.sk ohľadne dôvodu, že čo ľuďom na nich prekážalo. Odpoveď: ´Oni mali peniaze´. Fascinujúci, ale aj šokujúci detail! Žeby len tak málo stačilo, aby ľudia boli vôbec schopní tolerovať genocídu? Ale smutná skutočnosť je, že v tomto svete sa história neustále opakuje. Kristus povedal svojim učeníkom:   "Ja som cesta a pravda a život...ak mňa prenasledovali,  budú prenasledovať aj vás." - Ján 14:6; 15:20.   Tak, ako ľudstvo bolo schopné tolerovať genocídu Izraela, lebo ´oni mali peniaze´,  tak prenasledujú duchovný Izrael dnes. A dôvod? Lebo ´oni majú pravdu´! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (85)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Vlády ako divé zvery
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Prenasledovanie za pravdu je príjemné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Pravá božska láska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Kto pozná Božie meno?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Smrťou sa končí všetko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Lužinský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Lužinský
            
         
        luzinsky.blog.sme.sk (rss)
         
                                     
     
         Autor sa bude snažiť budovať, povzbudzovať a utešovať ľudí (vrátane samého seba) svojou prózou i poéziou.    
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1303
                
                
                    Celková karma
                    
                                                1.18
                    
                
                
                    Priemerná čítanosť
                    481
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Teológia
                        
                     
                                     
                        
                            Historia
                        
                     
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Viera
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




