
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Polák
                                        &gt;
                Politika
                     
                 Mikloš je nahý! (rozzúrený rev) 

        
            
                                    2.6.2009
            o
            12:00
                        |
            Karma článku:
                10.29
            |
            Prečítané 
            2379-krát
                    
         
     
         
             

                 
                    Ani po dvadsiatich rokoch slovenskí tzv. ekonomickí odborníci neprispôsobili svoje znalosti svetovému štandardu, naďalej veselo rozprávajú hlúposti a deficit schopnosti ekonomicky uvažovať u verejnosti rastie. Ľudia typu Ivana Mikloša radi ohurujú v televíznych debatách nabiflenými štatistikami, ktorých správnosť, ale predovšetkým relevantnosť si normálny človek nemá ako overiť. Už pomenej sa stáva, aby niektorý z nich zrozumiteľne vysvetlil nejaký zložitejší ekonomický jav, ktorého podstata sa líši od skúsenosti bežného človeka. "Experti" oveľa častejšie používajú primitívne analógie, ktoré nielenže nič nevysvetľujú, ale v skutočnosti správnemu chápaniu bránia.
                 

                 "Ani po piatich mesiacoch vláda neprispôsobila rozpočet kríze, naďalej veselo míňa, deficit rozpočtu rastie a úmerne s tým pribúda aj káuz, ktoré berú peniaze z vreciek daňových poplatníkov." Takto sa začína nedávno zverejnený článok denníka Sme o tom, ako "sekera v štátnom rozpočte bujnie".   Iba tak mimochodom, už ste niekedy videli, ako nejaká sekera bujnie? Taký pohľad by ma celkom zaujímal. George Orwell kedysi brojil proti mechanickému používaniu rôznych ošúchaných obratov či zmiešavaniu metafor, pretože to podľa neho znamenalo, že autor v skutočnosti neuvažuje, iba hromadí za sebou rôzne klišé. Po prečítaní zvyšku textu treba povedať, že niečo na tom asi bude.   Článok totiž v ponurých tónoch vykresľuje, ako sa zväčšuje "diera" v štátnom rozpočte - "aj preto, že dane ho vôbec nestíhajú napĺňať." Podľa autora "[o]pozícia aj ekonómovia vládu kritizujú, že nič nerobí, len sa spolieha na to, že kríza skoro pominie a dane znovu začnú do rozpočtu tiecť." A kto sú tieto znepokojené osobnosti? Napríklad poslanec SDKÚ Ivan Mikloš: "To, že príjmy budú nižšie, je jasné už dlhšie, a napriek tomu vláda nekoná a tvári sa, akoby sa nič nedialo". Podpredseda strany Sloboda a Solidarita Jozef Mihál zase "kritizuje štedré sociálne výdavky" - dôchodcom sa podľa neho nemajú sľubovať vianočné dôchodky, majú si uťahovať opasky, podľa príkladu mladšej a strednej generácie. Ekonóma inštitútu Ineko Petra Goliaša zase článok cituje v zmysle, že "ak sa kríza rýchlo neskončí", neschopnosť vlády šetriť vyústi do prudkého rastu dlhu.   Panebože. A takíto ľudia sú na Slovensku vydávaní za ekonomických odborníkov? Takíto diletanti sa snažia miešať do ekonomickej politiky? Človek by sa už asi nemal ničomu čudovať, ale predsa len, nad takouto neznalosťou skutočne zostáva rozum stáť. Obdobou štátu predsa preboha nie je súkromná domácnosť, ktorá naozaj hromadením dlhov nič nevyrieši! Ak už použiť prirovnanie, tak je štát skôr čímsi podobným súkromnej banke, ktorá je pri systéme zlomkových rezerv tiež v istom zmysle neustále v deficite (konkrétne v deficite likvidity). Nijako jej to však samozrejme nebráni nielenže existovať, ale dokonca aj produkovať zisky.   Schodok je stupienkom k obnove   Takisto v prípade štátu nie je deficit v čase recesie problémom, ale naopak prínosom. Recesia neznamená nič iného, než že ekonomická aktivita súkromného sektoru je v dlhodobom poklese oproti predchádzajúcemu obdobiu (a tým pádom aj oproti svojej potenciálnej úrovni). Keďže celkový dopyt klesá, firmy znižujú svoju produkciu, či prinajmenšom neinvestujú do jej rozširovania - a tým pádom zdroje, ktoré sa predtým využívali, ležia ladom. Najvypuklejším príkladom je zvýšená nezamestnanosť - čo neznamená nič iné, než to, že predtým časť dovtedy využívanej pracovnej sily teraz využitá nie je.   Preto v každej rozumnej krajine zakročuje štát, ktorý tým, že vytvára rozpočtový deficit, zároveň zvyšuje celkový dopyt nad dovtedajšiu úroveň. Tým sa snaží dosiahnuť presne to, aby sa nevyužité zdroje znova zapojili do ekonomického procesu - zvýšené objednávky zo strany štátu (a v tejto súvislosti je ozaj jedno, či klientelistické alebo nie) znamenajú stimul pre súkromnú produkciu, ktorá zase znamená mzdy zamestnancom, ktorí by ich inak nedostali, čo zase znamená zvýšenú spotrebu, ktorá vedie k zvýšenej produkcii zase ďalších firiem atď. (tomuto sa hovorí multiplikačný efekt). Celkový efekt deficitu je ekonomický stimul smerujúci ekonomiku späť k potenciálnej úrovni aktivity. Vtedy, teda v čase konjunktúry, je čas snažiť sa rozpočet opäť vyrovnať a dlh splácať. Volať po znižovaní deficitu počas recesie má asi takú logiku, ako kričať "Stoj, lebo sa zastrelím!" (s vďakou Kriste B.)   Práve preto, že objem daní v čase recesie klesá, čiže inými slovami sám od seba smeruje k vzniku deficitu a v čase konjunktúry naopak stúpa, čiže opäť sám od seba smeruje k vzniku prebytku, sa dane v tejto súvislosti považujú za tzv. automatické stabilizátory. Inými slovami, celkom samé od seba pôsobia proticyklicky, teda proti výkyvom ekonomiky - aj keby vláda už nerobila nič iného.   Je skutočne trápne musieť čosi takéhoto písať. A nič sa na tom nezmení, ani keby som bol ozaj idiotom, boľševikom, červeným, Ficoľubom a ako všelijako som si to už na svoju adresu vypočul. Ja som si to totiž, prosím, nevymyslel. To je, prosím, jednoducho učivo asi tak druhého ročníka makroekonómie na škole, ktorú som mal tú česť vyštudovať. A pravdaže aj na každej inej, ktorá si dovolí tvrdiť, že vzdeláva ekonómov. Isteže, mnohí konzervatívne založení ekonómovia desaťročia piľne pracovali na tom, aby tieto zásady podkopali... a keď prišlo na lámanie chleba - t. j. dnes - okamžite sa takisto obrátili na pravú vieru - viď Obamov ekonomický stimul. Pochopiteľne - inak by ich každá vláda s pudom sebazáchovy pohnala pekne späť vegetovať do thinktankov a zachovala sa tak, ako jej velí zdravý rozum. Že deficit v recesii je liekom a nie jedom vie dokonca ešte aj tá neučesaná Wikipedia.   Muži v ofsajde   A "odborníci" typu Ivana Mikloša, Jozefa Mihála či Petra Goliaša to nevedia??? Nikdy o tom nepočuli??? Fakt si myslia, že dvadsať rokov po novembri 1989 nie sú na Slovensku už rádovo stovky ľudí, ktorým je jasná nehoráznosť takýchto rečí? Alebo sa jednoducho spoliehajú na tichú dohodu, že totiž za inteligenta je považovaný každý, kto vie pekne farbisto nadávať na Fica a Smer, aj keby inak nevedel dať dokopy dve a dve?   Sú teda skutočne diletantmi, ktorí síce vedia šermovať s čísielkami, ale nemajú v skutočnosti v tele kúštik schopnosti ekonomicky uvažovať? Alebo - a táto otázka sa nenatíska prvýkrát - naopak základy svojej oblasti síce poznajú, ale bez jediného záchvevu svedomia nonšalantne verejnosť ťahajú za nos, len aby si nahnali politické bodíky? Zavádzajú azda vedome, len aby si zase raz mohli udrieť do "Ficovlády"? Aj za cenu toho, že pritakávajú primitívnym analógiám, ktoré so skutočnou ekonómiou nemajú nič spoločné?   Úprimne neviem, čo je horšie. V každom prípade, zaslúžili by si...   Ó hrozný Jehovah !  Ty bez milosrdenstva, ja pravicu ti trestajúcu vzývam, na vlastné plamä - na ekonómov - Tvoju prosím pomstu...           (S vďakou Štefanovi Mészárošovi za neúmyselnú inšpiráciu a s ospravedlnením Ivanovi Kraskovi za svojvoľnú úpravu jeho veršov)   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Polák 
                                        
                                            Obama vyhrá prinajmenšom o osem percent
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Polák 
                                        
                                            Detektív Šípoš opäť zasahuje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Polák 
                                        
                                            Ján K. Myšľanov: hasta siempre, náš dômyselný rytier...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Polák 
                                        
                                            Topoliáda alebo O týždenníku Slovo a Gabrielových otrávených šípoch
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Polák
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Polák
            
         
        michalpolak.blog.sme.sk (rss)
         
                                     
     
        Polovicu života žije s prestávkami v Británii. Približne desať rokov študoval v rôznych obmenách ekonómiu a filozofiu na London School of Economics a odniesol si za to všetky relevantné tituly. Už počas štúdia pracoval na čiastočný úväzok v Slovenskej redakcii BBC World Service a neskôr do nej nastúpil aj ako riadny redaktor. Vždy mu bolo proti srsti spomínať svoje skúsenosti, keďže jedinou podstatnou vecou by mala byť sila argumentu. Po dlhoročnej skúsenosti však rezignovane skonštatoval, že na Slovensku sú stále oveľa dôležitejšie podružnosti typu titul, vek, či postavenie. Takmer dva roky bol šéfredaktorom spoločenského týždenníka Slovo a keby si myslel, že niekto je schopný uveriť tomu, v akých partizánskych podmienkach sa tento týždenník tvorí, napísal by o tom niekedy možno viac. Vie variť anglo-indické karí, základnú taliansku omáčku na cestoviny a bryndzové halušky.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2123
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Slovenské médiá
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Dean Baker
                                     
                                                                             
                                            Brad DeLong
                                     
                                                                             
                                            Milan Jaroň
                                     
                                                                             
                                            Eduard Chmelár
                                     
                                                                             
                                            Ján K. Myšľanov
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Slovo - spoločensko-politický týždenník
                                     
                                                                             
                                            The Guardian
                                     
                                                                             
                                            BBC News
                                     
                                                                             
                                            Britské listy
                                     
                                                                             
                                            Právo (komentáre)
                                     
                                                                             
                                            A2 - kulturní týdeník
                                     
                                                                             
                                            Literární noviny
                                     
                                                                             
                                            The Huffington Post
                                     
                                                                             
                                            Slate Magazine
                                     
                                                                             
                                            ZNet
                                     
                                                                             
                                            Counterpunch
                                     
                                                                             
                                            Indymedia
                                     
                                                                             
                                            Venezuelanalysis
                                     
                                                                             
                                            Writings of Subcomandante Marcos
                                     
                                                                             
                                            Red Pepper
                                     
                                                                             
                                            New Statesman
                                     
                                                                             
                                            Spectrezine
                                     
                                                                             
                                            UK Watch
                                     
                                                                             
                                            Priatelia Zeme - CEPA
                                     
                                                                             
                                            Changenet
                                     
                                                                             
                                            Vsieti
                                     
                                                                             
                                            Mladí sociálni demokrati
                                     
                                                                             
                                            JeToTak
                                     
                                                                             
                                            Center for Economic and Policy Research
                                     
                                                                             
                                            Left Business Observer
                                     
                                                                             
                                            Dollars &amp; Sense
                                     
                                                                             
                                            The History of Economic Thought Website
                                     
                                                                             
                                            Review of Radical Political Economics
                                     
                                                                             
                                            Economics and Philosophy
                                     
                                                                             
                                            Socialistický kruh
                                     
                                                                             
                                            Marxists Internet Archive
                                     
                                                                             
                                            New Left Review
                                     
                                                                             
                                            BBC Radio4 Comedy
                                     
                                                                             
                                            BBC Radio7 Listen Again
                                     
                                                                             
                                            The Daily Show
                                     
                                                                             
                                            Multiorganizácia KRIAK
                                     
                                                                             
                                            Echo
                                     
                                                                             
                                            Joomla!
                                     
                                                                             
                                            Used Book Search
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




