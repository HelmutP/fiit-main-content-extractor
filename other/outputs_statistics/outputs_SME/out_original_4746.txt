
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrej Brník
                                        &gt;
                Prečo?
                     
                 Cesta k demokracii 

        
            
                                    17.4.2010
            o
            16:53
                        |
            Karma článku:
                2.25
            |
            Prečítané 
            212-krát
                    
         
     
         
             

                 
                    Od počiatku dôb, keď sa ľudia stali ľuďmi, sa vytváral sociálny systém spolužitia ľudí. Išlo o akési pravidlá, ktoré samozrejme vtedy neboli písané, ale jedinci sa snažili o dodržiavanie týchto pravidiel, aby bola zabezpečená rovnováha. Isté pravidlá sa zrodili prirodzene.
                 

                 Môžeme sem zahrnúť to, že matky sa starajú o deti, a muži sa starajú o matky s deťmi. Všetky pravidlá viedli k tomu, že najsilnejší jedinec stál na vrchole všetkých. O svoju úlohu musel však bojovať. Tak isto sa starým ľudom pripisovala múdrosť už odjakživa. Išlo o múdrosť, ktorá tkvela v ich skúsenostiach.   Organizovaný systém jedincov sa premenil na kmene, tie na kráľovstvá. Prišla však antika a s ňou aj revolučné myšlienky, ktoré nechceli vládu jedného, ale chceli aby sa na rozhodnutiach o spoločenstve podieľali aj iní. A tak už v antike sa rodia prvé zárodky demokracie - vlády ľudu. Neskôr sa tento model preniesol aj do Ríma. Mohli by sme predpokladať, že demokracia je úzko zviazaná s inteligenciu, vzdelaním. Hodnoťme históriu kriticky a všímajme si to, že keď bolo zanedbávané vzdelanie, ľudia sa riadili pudmi. Tak aj Rím, keď bol vyplienený Vandalmi, ľudstvo zabudlo na vzdelanie a vrhlo sa do tmy a vlády jedného.   Kráľovstvá sa odrazu po páde Ríma vynárali po celej Európe ako huby po daždi. Svoju úlohu v tom čase začala ukazovať aj Cirkev, ktorá sa si robila výlučné právo na vzdelanie. A tak ľudia, ktorý boli prostí, nechali sa ovládať panovníkmi a Cirkvou.   Najdôležitejšiu úlohu v ceste k demokracii zohral najväčší objav všetkých čias. Nepochybne sa ním stala kníhtlač. Tá v 15. str. spôsobila revolúciu, na ktorú nebol pripravený nikto. Knihy pre všetkých. S knihami prišli noviny. Odrazu utláčaná spoločnosť čítala. Dozvedala sa veci ktoré nepoznala. Dozvedela sa o pradávnej minulosti, kde boli ľudia slobodní. Knihy a vzdelanie formovali prvé názory. Tie sa časom pretavili do skutkov a v dobe osvietenectva sa ľudstvo opäť posunulo o krok dopredu k demokracii. Ľudia chceli vládnuť ľuďom. Chceli sami rozhodovať o svojich veciach. Všetko nasvedčovalo tomu, že autoritárske režimy sú minulosťou. Vznikali revolúcie, heslá v duchu SLOBODA, ROVNOSŤ, BRATSTVO. Ľudia sa oslobodzovali. A tak sa v 19. a začiatkom 20. str. rodia prvé demokratické štáty, na ktorých čele stáli ľudia vybraní ľudom.   Po prvej svetovej vojne, ktorá bola ťažkou skúškou pre demokraciu sa skoro celá Európa tešila. Prišli však mnohé diktátorské režimy. Ľudia sužovaní hladom, a inými problémami chceli znova niekoho, kto im zabezpečí pohodlie a jedlo. Diktátori ktorým sa to podarilo naplniť boli na výslní a vysmievali sa demokracii. Druhá svetová vojna ukázala najväčšieho z nich. Hitler chcel zničiť svet a vytvoriť na ňom iný, „lepší". Problémom diktátorov, ktorí boli vybraní ľudom aby im pomohli a z ktorých sa neskôr stali diktátori bolo to, že po splnení cieľa, za ktorým boli ľuďmi zvolení sa nechceli vzdať svojej moci. V dejinách poznáme málo takých ktorí odolali a vrátili moc späť do rúk ľudu. Ale späť k demokratickému procesu.   Po vojne sa objavil socializmus, ktorý chcel ešte väčšiu rovnosť ako demokracia. Myšlienka to bola dobrá ale ľudstvo pokazené chamtivosťou nebolo pripravené na tento systém. A tak socializmus priniesol len to, že zašľapal demokraciu v celej východnej Európe pod zem a priniesol len útlak a strach. Zatiaľ čo v západnej Európe mali ľudia možnosť žiť v demokracii, tí na východe také šťastie nemali. Prišli však roky 90-te a s nimi aj koniec socializmu a nástup demokracie.   Dá sa demokracia naučiť? Môžeme ihneď hovoriť o tom, že máme demokraciu? A čo je vlastne demokracia?   Demokracia je model správania sa, ktorý berie v ohľad okolitý svet, a snaží sa, aby netrpel tým, že sa ženieme za vlastnými ideálmi. Demokracia je systém, v ktorom sa musí každý snažiť, aby sám sebe a svojmu okoliu vytvoril podmienky vhodné pre dobrý a spokojný život.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Brník 
                                        
                                            Biznis nadovšetko!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Brník 
                                        
                                            Tí prví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Brník 
                                        
                                            Kamarát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Brník 
                                        
                                            Koleno
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Brník 
                                        
                                            Bola si
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrej Brník
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrej Brník
            
         
        brnik.blog.sme.sk (rss)
         
                                     
     
        Som študent ale hlavne zoon politicon
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    788
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Prečo?
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




