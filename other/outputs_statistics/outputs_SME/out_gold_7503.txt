

 Brzdy vozidla zapíšťali na Rajskej. Hurá, zaplesal som a cez plece si prehodil športovú tašku, ktorá mi v práci hnila už od stredy, čo som sa kvôli silnej opici nemohol dostaviť na tréning. Plán bol jasný – v sporiteľni zaplatiť faktúru za antivirák, ktorá je už niekoľko dní po splatnosti, poslať Shootymu nejaké éčka na billboardy a nakúpiť potraviny na hríbové rizoto. 
   
 Bolo asi pol šiestej, keď som svoj rozkysnutý zadok vytrepal na Obchodnú. Sporiteľňu zatvorili o piatej. Finančné transakcie sa nekonali. Vybral som sa teda na nákup. V Bille som z tašky vytiahol zdrap, na ktorý som si načmáral potrebné ingrediencie, keď mi ich Bára diktovala cez telefón. Zeler, špeciálnu ryžu na rizoto, sušené hríby, BIELE VÍÍÍNO a bio bujón. 
   
 V oddelení zeleniny chaosila skupina dôchodcov pri váhe. Staršia, bohorovne tváriaca sa žena objemnejších rozmerov ťukala po dotykovom displeji, hľadala citróny. Ani jej nenapadlo, že na tej polici má číslo veľké ako grécky dlh. Nedala si povedať. Bude to v pohode chlapec, vydrž, hovoril som si, tetuška tie citróny o chvíľku nájde. Nenašla, vrátila ich späť. A odrazu bola škaredá, ako ten zeler, čo som mal v košíku. 
   
 Regál s ryžou som našiel rýchlo. Dlhozrnná, krátkozrnná, výberová, guľatá, odrodová, neviem aká, ale na ryzoto ani jedna! Tak som zobral tú, ktorá sa tvárila najkvalitnejšie. Bola drahá ako nástenkový tender. O sušených hríboch v tej predajni ešte nepočuli, tak som zobral v slanom náleve. Pri hľadaní bujónov mi z oka takmer vytislo slzu. Zúfalo som prechádzal pomedzi regále, skôr by som tie magy kocky vysral ako ich tam našiel. 
   
 Vyšiel som z obchodu rovno na zastávku trolejbusu. Mal ísť o 10 minút. Zapálim si, o chíľu je tu, na druhú stranu námestia sa mi nechce. Vynechal spoj, trolejbus neprišiel. Stokilová taška sa mi zarezala do pleca. Ak nemôžem ísť touto cestou trolejbusom, môžem ísť druhou autobusom. 
 „Which bus goes to the bus station?“ opýtal sa ma odrazu mladý černoch v obleku, iba som sa rozbiehal, že odídem. 
 „Two hundred six,“ nestačil som sa diviť, ako mi tie čísla napadli. 
 „From Rača?“ reagoval. 
 „From Rača what?“ už som bol mimo. 
 „Bus to the bus station from this Rača?“ pokračoval. 
 Čo má, kurva, autobusová stanica a trolejbus číslo 206 s Račou? Čo som dnes komu spravil? 
 „Íst tu alebo íst druhá strana cesta?“ zadelil. 
 „Trolley to the bus station goes from this stop,“ hurá pochopili sme sa! 
   
 Tieklo zo mňa ako z pokazenej práčky. Na druhej strane námestia postávalo pár ľudí. Tašku som zložil na zem v okamihu, ako som uvidel prichádzať známu herečku, zaslúžilú umelkyňu. Bola, tak ako vždy, elegantná, jej charizmu bolo cítiť na metre. Krásna žena! Doteraz si fičí na doskách, ktoré znamenajú svet. Pristavila sa iba pár metrov odo mňa, tak sme na ten autobus z Petržalky čakali spolu. 
   
 Poloprázdna kraksňa zastavila poslednými vrátami nám rovno pred nosom. Pustil som staršiu pani, nech ide prvá. Išiel som len zastávku, a tak som si ani nesadol. V druhej polovici harmonikového tátoša som bol ja, ona a dvaja nejakým fetom potužení mladíci. 
 „Som ju trtkal celú noc,“ hovorí jeden druhému. 
 „Ale to nemôžeš, ona je s Ferom,“ odpovedal druhý. 
 „Nech idú do piče,“ zareval. 
 Chvíľka ticha netrvala ani minútu, keď mladý zadelil: 
 „Vy nie ste žena nejakého herca? Ja vás poznám z telky!“ 
 Herečka sa iba usmiala. 
 „Tak to ja si vás zoberiem za ženu a budete sa o mňa starať. Budete mi variť, prať.“ 
 „Pre vás som už stará, mladý muž,“ odpovedala priateľsky na návrh. 
 „No jasné, koľko máte rokov?“ zahučal. 
 „Už 83. A vy nemáte hádam ani 25,“ povedala príjemne. 
 „No jasné, 83!? Ale v posteli musíte byť iná jeba, nejdete so mnou na kávu?“ 
 Neveril som vlastným ušiam, v tej chvíli som neveril absolútne ničomu. Túžil som po tom, aby sa mi ten okamih iba zdal. 
 „To nepôjde, už som stará,“ nenechala sa vyviesť z rovnováhy. 
 Mal som pocit, že som svedkom natáčania nejakého absurdného seriálu. 
 „Pôjdeme na kávu, uvidíte, že to pôjde,“ nedal sa odbiť fagan. 
 Stará pani si zachovala chladnú hlavu. Úkratkom sa usmiala a z jej pohľadu by aj sprostý pochopil, čo chce povedať. Nevydala zo seba jedinú hlásku. Boli sme pár metrov pred zastávkou, no stopla nás červená. Mal som pocit, že trvá večne. 
 „Tak keď so mnou nechcete trtkať, dajte mi 15 eur,“ zarvalo chlapčisko. 
 Uši mi takmer odvalilo na podlahu. A nech mi nikto nehovorí, že sa neblíži koniec sveta! 
 „Žiaľ, nemám pri sebe peniaze, chlapci,“ odpovedala opäť neskutočne pokojným tónom. 
 „Ale no tak! Aspoň nejaké drobné, pár šušňov...“ nedali si povedať. 
   
 Kraksňa sa pohla a už aj brzdila na zastávke. Mladí zmrdi zostali vo vozidle, herečka vystúpila. Vybehol som spotený ako divá sviňa. Bol som mimo, ako už dlho nie. Ani som nevládal ísť domov. Sadol som si na múrik pri kvetinovom záhone a jednu si zapálil. Tú situáciu som nevedel predýchať.  
  
 Chlapčiská sa stratili v odchádzajúcom autobuse, herečka v priľahlých uličkách.  
  
 A ja som sa stratil vo svete, o ktorom som si doteraz myslel, že ho poznám... 

