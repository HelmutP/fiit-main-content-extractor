

 Čím bližšie sú parlamentné voľby, tým viac sa niektoré politické strany a ich lídri prikláňajú k aktivitám ako sú osočovanie, podozrievanie, obviňovanie, očierňovanie atď. Tam kde chýba kreativita nastupuje agresivita. Niektoré strany moc kreatívne nie sú. Dnes nás o tom zase raz presvedčil súdruh Fico v diskusii s Radičovou. 
 Po billboardoch, ktoré upozorňujú pred možnou kolaboráciou s SMK dnes Fico vytiahol ďalší level primitívnych argumentov. 
 To, že sa Smer nikdy, v žiadnej volebnej kampani nezmohol na propagáciu vlastných myšlienok a vždy len profitoval z antagonistických myšlienok, ktoré vnáša do myslí nevnímavých ľudí sme si už zvykli. Takisto sme si zvykli aj na jedinú kreačnú aktivitu, ktorou sú rôzne vykonštruované škandály, ktoré navyše ani nemajú logické argumentačné pozadie. 
 Ale s čím prišiel Fico dnes už považujem za akt zúfalstva  a konanie človeka, ktorý už naozaj nevie priniesť reálnu tému. 
 "Pamätám si, keď ste kandidovali na Prezidentku SR, tak ste išli na snem SMK, kde bol aj pán Orbán. A všetci si pamätáme, ako ste prijali podporu od pána Orbána na prezidentské voľby na Slovensku. Orbán okrem iného povedal, že je výborné, že je taká kandidátka na Slovensku, ako je pani Radičová..."  
 - toto všetko je pravda, ale čo to dokazuje??... Mal snáď Orbán pred voľbami vyjadriť sympatie Gašparovičovi, kandidátovi, ktorí vlastne odfajčil maďarskú menšinu. Pritom vlastne zabudol, že tá mu zabezpečila jeho prvé víťazstvo.  
 Je snáď zásluha Radičovej, že Orbán prišiel s takýmto zákonom o občianstve?  Nevyjadrila predsa aj samotná Radičová odmietavý postoj k tomuto zákonu? Nepovedala predsa Radičová, že SMK sa svojím kladným postojom k tomu zákonu odpísala v prípade povolebnej spolupráce?... 
 Ja hlboko obdivujem Ficove dedukčné schopnosti. Je to človek, ktorý dokáže tak neuveriteľne prekrúcať fakty a z faktov vytvárať podozrenia, z podozrení robiť jasné závery.... a to všetko bez argumentácie. Jediným argumentom je, že to povie on. To jeho voličom stačí... (je to dokola stále tá istá pesnička) 
   
 Čo ma však zaráža teraz najviac sú témy, ktoré Smer používa. Je to vlastne len jedna téma a je ňou stará známa maďarská karta. 
 Smer bol pri skladaní vlády kritizovaní za prijatie SNS. Tak prečo teraz mlčia tí európski socialisti? Veď Smer sa správa rovnako ako SNS. Smer vôbec nepropaguje sociálne myšlienky. Ak aj áno sú druhoradé, niekde úplne zabudnuté. Keby nemá Smer v názve to SD, tak nikto ani nevie, že je to strana sociálnodemokratická (?!?).  
 Keby teraz nejaký cudzí človek počúva diskusie a vyhlásenia Fica, keby si všimne niektoré billboardy a plagáty, takl si isto pomyslí, že Smer je národnou/nacionalistickou stranou. 
 Tak sa teda pýtam, či náhodou nenastal čas na ďalšiu zmenu názvu strany. Bola tu kedysi Tretia cesta, teraz SD...čo tak napríklad Smer-národný socializmus...?? 

