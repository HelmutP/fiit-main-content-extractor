
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Evka Hruscova
                                        &gt;
                Nezaradené
                     
                 Feministky alebo ženy, ktoré sa uvedomili? 

        
            
                                    17.6.2010
            o
            17:12
                        (upravené
                17.6.2010
                o
                18:01)
                        |
            Karma článku:
                6.68
            |
            Prečítané 
            1528-krát
                    
         
     
         
             

                 
                    Sú mladé, krásne, mimoriadne úspešné a čo si budeme nahovárať – aj s vysokým IQ. Vždy sú IN, oblečené podľa najnovšej módy, ladnými krivkami tela pútajú pohľady. Nielen chlípnych, túžiacich mužov, ale aj závistlivých, žiarlenia chtivých žien. Každý normálny človek, zaliaty problémami dnešnej doby by si pomyslel, že život pre nich musí byť príjemnou jarnou prechádzkou. Majú dostatok peňazí a všetci muži im ležia pri nohách... No je to skutočne tak?
                 

                 Moja najlepšia kamarátka mi nedávno poslala správu na Facebooku: „ Nechápem to!!! Konečne som ho stretla. Bol sympatický, úspešný s dobre rozbehnutou kariérou a k tomu na moje počudovanie bol „single“. Párkrát  sme sa stretli, vyzeralo to nádejne. Dokonca priznal, že som ho zaujala. V spoločnosti zakaždým pošepol aký je na mňa hrdý. Podporoval ma v rozbehnutej kariére. V každej situácií sme sa vedeli dohodnúť a nájsť kompromis.    No potom sa to stalo. Pri jednej zvlášť búrlivej hádke mi vyšplechol do tváre, že som feministka, nikdy zo mňa nebude dobrá matka jeho detí, že sa pri mne necíti ako chlap, že sa vyvyšujem... Vtedy som len skromne sklopila oči, nahmatala kabelku a vybehla z bytu. Na druhý deň sa v práci objavil kuriér s tuctom ruží a malou kartičkou nesúcou ľútostivé ospravedlnenie. Prehltla som to, potlačila  emancipáciu a vrátila sa naspäť k nemu.“    Idilka, že? No príbeh mojej kamarátky sa tu nekončí.    Tento nazvime to konflikt sa opakoval čo raz častejšie a s čoraz väčšou intenzitou. A tak raz za sebou  zabuchla dvere bytu -  tentoraz navždy. Samozrejme nebolo to jednoduché. Nielen jej zranená identita, ale aj narážky od tzv. priateľov (okrem pár výnimiek), že je feministka a nevie ustúpiť, znižovali jej  sebahodnotu ešte viac. Postupne  nabrala nové a nové sily a opäť sa stala tou emancipovanou, dobre vyzerajúcou mladou ženou, ktorá však tentoraz je oveľa opatrnejšia – voči láske, voči mužom.   A dnes? Stále je „single“, ale spokojná so svojím životom, šťastná vo svojej koži. A na poznámky typu: „ Ty si ale feministka, takto si nikdy chlapa nenájdeš“, reaguje len ležérnym mávnutím ruky a hrdou odpoveďou: „Radšej byť single, ale uvedomelá, ako zatvárať oči nad chlapovou hlúposťou.“    Tak milé dámy, či chceme alebo nie muža vo svojom živote potrebujeme. Veď odpradávna patríme k sebe ako soľnička a korenička. Ak teda je cítiť vo vzduchu striktný feminizmus, je lepšie hodiť spiatočku, no ani tváriť sa ako hlúpa hus nie je asi to pravé orechové.   Len rovnováha na oboch stranách je zárukou úspechu. Ak muž rešpektuje ženu ako mysliacu a hlavne rovnocennú bytosť a žena považuje svojho muža za zdroj istoty, potom ich vzťah má dobrý predpoklad pre úspech. Feminizmu sa vyhnúť, no uvedomiť sa!  - poslanie dnešnej modernej doby?   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (76)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Evka Hruscova 
                                        
                                            Vyčerpaný nasadením všetkých síl, prekročiť rímsu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Evka Hruscova 
                                        
                                            Malí – veľkí ľudia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Evka Hruscova 
                                        
                                            Mexická vlna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Evka Hruscova 
                                        
                                            Mame kazdy den sancu zacat znovu..?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Evka Hruscova
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Evka Hruscova
            
         
        hruscova.blog.sme.sk (rss)
         
                                     
     
        Zivot je ako cokoladova bonboniera, nikdy nevies, co ochutnas.Obcas je to kusok horkej cokolady s orieskami, ktore tak nenavidim,no obcas styri kocky neobycajne lahodneho belgickeho kremu, ktory tak milujem.Znie to banalne,no kazde rano, aj v to najsychravejsie ci najdusnejsie, je tu sanca zacat znovu,tak preco ju nevyuzit?
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1003
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




