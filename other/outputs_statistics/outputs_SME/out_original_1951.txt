
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Dérer
                                        &gt;
                Nezaradené
                     
                 Zaostalé Slovensko a Sulíkova ponuka 

        
            
                                    14.12.2009
            o
            22:10
                        (upravené
                14.12.2009
                o
                22:17)
                        |
            Karma článku:
                12.40
            |
            Prečítané 
            3343-krát
                    
         
     
         
             

                 
                    Čo si budeme navrávať: sme zaostalá krajinka. Nie síce Papua-Nová Guinea, ale v európskom merítku zaostalá – a krajiny, čo po páde komunizmu začínali z rovnakej štartovacej línie, nás už nechali kus za sebou. Možno poviete, nech netrepem, veď máme Euro (to ani Česi či Maďari nemajú), v Maďarsku zúri horšia kríza než u nás, a ceny bytov, prenájmov či hotelových nocľahov sú v Bratislave už vyššie, než vo Viedni – takže Bratislava je zrejme pupok sveta. Na Slovensko chodia turisti z celého sveta; po zážitku s úrovňou reštaurácii a služieb sa síce nemusia vrátiť, ale veď prídu iní. Dokážeme úspešne exportovať športovcov, operných spevákov i prostitútky. A už aj našinci z hladových dolín či regiónov pochopili, že práca za nimi nepríde, musia oni za ňou – takže každý pondelok sa pol Slovenska valí do Bratislavy na týždňovky. Kde je teda problém?
                 

                 Nuž, vyspelosť či zaostalosť je otázka kritérií. Vôbec nejde len o to, či dostanem za svoj denný plat v obchode väčší krajec, než dostane Čech, Maďar alebo Poliak u nich doma. Je to o myslení ľudí. O tom, čo si ceníme, aké máme priority, hodnoty, akú etiku.   Ceníme si napríklad vzdelanie? Tituly sú predsa na predaj, a vzdelanie je v krajine montážnych hál a logistických skladov zbytočná záťaž. Vadí, že naša veda a výskum je na úrovni Rwanda-Burundi? Asi nevadí, veda je hra darmožráčskych intelektuálov, všakže (a intelektuál je u nás nadávka). Prečo by nás malo trápiť, že nik zo Slovenska ešte nedostal Nobelovu cenu, že naše univerzity zúfalo zaostali i za maďarskými, českými i poľskými? Osadenstvu krčmy v Hornom Výplachu je toto predsa ukradnuté. Preberáme technológie vymyslené inde, veď čo budeme sami vymýšľať, nedajbože určovať trendy. Akoby sme nemali vyššie ambície, než makať pri páse či jazdiť vysokozdvižným vozíkom (to nám rovno stačí kosák a kladivo).   Ceníme si kultúru, dobrú muziku, alebo postačí Senzus? Oceníme gastronómiu na úrovni, alebo nám tak ľudovo a plebejsky stačí vyprážaný syr a treska s rožkom?    A ceníme si vlastné občianske práva? Ako napísal Milan Lasica, na Slovensku sme si autority nikdy nevážili, ale vždy sme ich poslúchali. Nikde v Európe si občania nenechajú tak skákať po hlave od štátu, vlády, inštitúcií a byrokratov. Večne nadávame na pomery, ale vždy skloníme hlavu. Nebúrime sa. Ako by to bola prírodná vyššia moc, na ktorú nemáme vplyv. Politika je predsa panské huncútstvo, teda pre pánov, my potomci poddaných sa do nej nemiešame. (Sakra, ako nám tá vlastná šľachta chýba!)   Dalo by sa tu obšírne hromžiť na biedu nášho školstva, zdravotníctva, súdnictva, takmer hocičoho, nuž ale nadávať predsa vieme. Čo nevieme (a zišlo by sa nám!) je vziať veci do svojich rúk. Ale ako?       Pri všetkej nemohúcnosti opozície, zavše sa objaví pokus o alternatívu. (Načase, do parlamentných volieb zostáva pol roka!) Z tých nových a neokukaných ksichtov je Richard Sulík so svojou SaS asi najnápadnejší, napokon, tie bilbordy pred župnými voľbami na nás číhali úplne všade. Ako politicky neangažovanému voličovi vyznávajúcemu pravicový liberalizmus by mi mala byť vec jasná: voliť Sulíka! Na fungovanie ekonomiky má predsa zdravé názory. Aj keby do iných oblastí chodu spoločnosti ani nezadrel (zatiaľ moc nezadiera, zdá sa mi), v rozumne poskladanej vládnej koalícii by vo svojom odbore mohol byť užitočný. Že do strany nechce žiadneho bývalého komunistu a vylučuje povolebnú spoluprácu so Smerom, je pozitívum. Zopár maličkostí navyše, ktoré Sulík propaguje (napríklad odluka cirkvi od štátu) sú vlastne už bonusovým prídavkom.       Zo Sulíkovej bilbordovej kampane mám ale zmiešané pocity. Ktorýsi hollywoodsky režisér raz povedal, že inteligenciu divákov nemožno dostatočne podceniť. Núka sa zaujímavá otázka, či možno to isté predpokladať o myslení voličov. A na tomto predpoklade postaviť volebné heslá a témy. Aby bolo jasné: ilúzie si o voličoch robiť netreba. Zavše človeku rozum stojí. Kedykoľvek vyjde najavo nejaké vládne rozkrádanie či korupcia, volebné preferencie vládnych strán len stúpnu. Stačí, že Fico vymenil nejakého pešiaka, a už ho chvália, ako narobil poriadky.    Ľuďom, čo nepremýšľajú o súvislostiach, asi naozaj treba podkladať jednoduché námety. Chápem, že Sulík by sa chcel dostať do parlamentu, a bez populistického útoku na myslenie prostejších ľudí to bude ťažké. Napokon, aj Dzurinda vyhral až vtedy, keď začal sľubovať dvojnásobné platy. (Koľkí na Slovensku chápu, že dobrý štát je ten, ktorý nemá na výšku našich príjmov takmer žiadny vplyv?)    Aj tak ma Sulíkove plagátové témy omínajú. Obmedziť poslaneckú imunitu je chvályhodný zámer, ale samotný počet poslancov (ktorý chce Sulík znížiť) či prehnané ceny vládnych limuzín nie sú najväčším, ba ani stredne veľkým problémom tejto krajiny. Dokonca ani koncesionárske poplatky, hoci ma štvú rovnako ako väčšinu spoluobčanov. To je určite jasné aj Sulíkovi. Zrejme rozmýšľa takto (aj keď nahlas sa také čosi nehovorí): keď už raz má volebné právo každý, čo má na krku hlavu a v rici díru, treba trkvasom zalepiť oči plagátmi, nech sa k slovu a k riadeniu štátu môžu dostať tí múdrejší – inak nám zasa bude vládnuť luza. Závisť je slovenská národná vlastnosť, takže prostého voliča skôr zaujmú pridrahé vládne autíčka, než technické stránky odvodového bonusu, ktorým nik nerozumie. Čo mi teda na plagátoch vadí?       Stručne povedané, trochu mi chýba štátnický rozmer. Politik má mať nejakú (konzistentnú) sústavu názorov, zásad a predstáv o spravovaní vecí verejných. Všetkých, nielen ekonomických. A musí byť jasné, že tieto predstavy má, vízie ďaleko presahujúce horizont jedného volebného obdobia. Jeho zásady sa nemajú odvíjať od hlasu ulice, ale od politikovho najvnútornejšieho presvedčenia. Politik má byť morálnou autoritou, trochu i filozofom, určovať trendy; ak treba, aj proti momentálnemu názoru väčšiny. Bežne sa predsa stáva, že sa väčšina mýli. Až čas ukáže, kto mal pravdu. (Kto bol väčší formát - Chamberlain, ktorý vyhovel momentálnej túžbe verejnosti po mieri, alebo Churchill, ktorého najprv ľudia nebrali vážne resp. odsudzovali?)    Viem, skutočných osobností je u nás zúfalý nedostatok. Nemali sme a nemáme ľudí kalibru Havla, Klausa, ani Walesu. Je tiež jasné, že Sulík asi nesmeruje do premiérskeho kresla. Ak však bude viesť aj budúcu kampaň populistickým nadbiehaním ľudovej vrstve, môže vyvolať pochybnosti u tých, ktorí by ho radi volili na základe rozumnej úvahy (a nie emócií). Môžu zapochybovať o jeho budúcom štátnickom rozmere. Písať úderné blogy, akokoľvek dobré, je jedna vec, reálpolitika druhá. No, do volieb je ešte trochu času. Nič neočakávam, ale rád sa nechám presvedčiť. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (109)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Dojmy a pojmy fotoamatéra (1.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Čo majú podľa mňa urobiť učitelia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Novembrové impresie po litri vína
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (3.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (2.časť)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Dérer
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Dérer
            
         
        tomasderer.blog.sme.sk (rss)
         
                                     
     
        Chemik, euroskeptik, klimaskeptik, slobodomyseľný bezočivec a kacír, spochybňovač autorít, podozrivý zo sympatií k libertariánom. Súkromník slúžiaci zahraničnému kapitálu. Sodoma-Gomora? Čo už, nič lepšie o sebe mi nenapadá. To najlepšie na mne sú moja žena a moje deti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2117
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




