

 Kráča. Nikto ho nevidí. Nevidí, lebo odrazu akoby sa každý stratil. Brloh je dnes málo zimný... Mrazivo je sťaby v ňom, nie von. Ale iba cez obed. Neďaleko sa objavil jarný potok, ktorý potom zas každý rok ustúpi. Pozná cesty hromov. Prídu a sú, ale v zime nie sú. 
   
 Nad svahom je les, všade je les. Je ešte skoro. Malo by sa loviť. Možno v ďalekej hore práve beží divoko srna, lebo vietor zaniesol pach vlka. Nepobeží, kríva. Zima bola ťažká a namáhavá. 
   
  
 Ale oči svieta ako svietili včera. Vravia a vidia. Zdá sa temer krotký, nepriblížim sa. Zdá sa akoby už nebol vlkom vo vlčom rozmere. Leží. Nevošiel ani do brholu. Leží. Iba občas pokrúti hlavou, stále vetrí. Možno o mne vie. Ale to je jedno, zrejme sa nepohne. Odrazu otvorí papuľu a cerí zuby. Je na nich krv. Aspoň sa zdá. Áno... nepochybne... Je to krv... 
   
 Kam mieria vlky, kam mierili pred rokmi... 
   
 Aký je krehký, ten obávaný. Aký je skromný. Akú má zvláštnu srsť. Skoro by som si ho privlastnil. Privlastnil krásu, dobro. Prchavosť nádhery, ktorá je taká bohatá pre moju malú dušu. Aké sú krásne stromy v jeho pozadí. Je ako sólista hôr, ako ten, čo sprevádza karavánu nocou, keď ho nikto nevidí. 
   
 Každý tvor má svoj príbytok. Lebo hlava sa skladá, zloží do príbytku. Známe steny, posvätné steny. Aj líšky. Pravdaže. Vošiel, zaliezol. Brloh, sväté steny. Bože, koľko námahy si vložil do vlka. 
   
 Chrám lesa, tajomno, nepoznanie. Kaluž ako labyrint jazera. Každý výmoľ, každý pohodený konár. Tíško dnes ležia. Kráča pomedzi ne. Pozná hraničné body, zájde ku nim, postojí, obnoví, vráti sa. Jeho oči horia, mysticky zlostné. Je ukrytý. Je skrytý v horách, a v tých očiach. 
   
 Keď sa vracal, zastavil sa opäť pri tej istej kaluži. Alebo inej. Pozeral ponad ňu. Napadlo ma, že tie jeho oči sú ako kaluže. Sivý kožuch ako krása pravosti. 
   
  
   
 Keď sa vrátim, znovu by som ho tu chcel nájsť. Ale či tu bude... Znovu by som rád kráčal po tých istých miestach pod klenbou stromov. Ukrytý pred jeho očami a on predsa o mne vie. 
 Vlk šiel svojou stranou. Chcel som opýtať, a to je všetko.... nič. To už nič. Nič sa nedozviem... Nič neokúsim, ani strach, ani nič. Ja neviem. Tak stojím a pozerám. Bol tu. 
   
 Oči sa obracajú k tebe. Oči všetkých sa upierajú k tebe. Vieš to. Oči nemusia byť viditeľné. Dôležité je, aby oči mali čo vidieť. Keď umiera čosi v nás, vždy bežím za Stvoriteľom, za Spasiteľom, lebo  v tej jednej osobe by sa dal nájsť život. Cítim vlčie oči na sebe. Nedajú pokoj. Myslel som, že ho pozorujem, ale on ide za mnou. Sleduje ma. On sleduje mňa. Nijaké zamknuté dvere. Ja už ani nepoznám pojem dvere. Okolo tretej v noci zreteľne počujem jeho skučanie. Počul som, ako sa hýbe. Zišiel som z postele a šiel. Nezastavil som. 
   
 Preto som šiel za ním. Preto za ním blúdim po lese. Blížim sa k nemu a  ustupujem. Vrčí na mňa a pripája sa navždy k môjmu bytiu. Inokedy uniká do hĺbok, kde ľudská noha nie je nevyhnutná. Že sedí na slnku a nevýrazne výrazne ma preniká. To všetko ma fascinuje. A že sa to stalo, že sa to stáva, že príbeh chodí hore a potom sa spustí zasa dolu, to nebudem hovoriť. To je len tu. Tu. V mojom srdci. Na to nikto nárok nemá. To si vezmem so sebou. 
   
   
   
   
  
   
   
   
   
 + 

