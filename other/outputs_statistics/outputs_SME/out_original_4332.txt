
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                Tvorivosť
                     
                 Ako som sa s horúcou žehličkou pozabával 

        
            
                                    11.4.2010
            o
            9:27
                        (upravené
                11.4.2010
                o
                15:28)
                        |
            Karma článku:
                13.10
            |
            Prečítané 
            6749-krát
                    
         
     
         
             

                 
                    Doma máme často takého tvorivého ducha. S manželkou nie sme žiadni umelci, ani na to nemáme školy, ale radi si tvoríme tak  pre radosť. Moja drahá polovička dostala opäť ten záchvat tvorivosti a hľadala inšpirácie. Na internete objavila niečo, čo sa volá enkaustika a stručne ma s ňou oboznámila. Priznám sa, do včera som netušil, že niečo také existuje.
                 

                 Preto prejavujem  záujem a zisťujem, že ide o starú techniku ešte z čias starovekého Egypta, ktorá na maľby využívala horúci včelí vosk zmiešaný s farebnými pigmentmi. Roztavený vosk sa nanášal na pripravený drevený podklad. Dnes sa používajú syntetické farebné vosky, ktoré sa nanášajú na papier, drevo, plátno a iné podklady. Nástrojom je hlavne enkaustické pero s elektrickou vyhrievacou špirálou a s meniteľnými nástavcami, alebo práca priamo na vyhrievanej kovovej doske teplovzdušná pištoľ, alebo špeciálna enkaustická žehlička.   Manželka mi ešte preklikala pár videií a obrázkov na internete, ktoré prezentovali túto techniku a zároveň mi zasadila tvorivého chrobáka do hlavy. Chcel som to skúsiť, ale doma sme nemali nič zo špeciálnych nástrojov a materiálov spomínaných v opise tejto maliarskej techniky a úprimne povedané, ani som nechcel investovať do nie celkom lacného vybavenia, kým si to nevyskúšam v nejakom lacnejšom prevedení.   Tak som trocha porozmýšľal a preskúmal som sobotné podvečerné, domáce možnosti. Po rekognoskácii domácich zdrojov, som mohol ohlásiť pripravenosť na zahájenie tvorivého večera zameraného na enkaustiku. Špeciálne vosky s pigmentáciou nahradia obyčajné voskové pastelky (voskovky), ktorých máme pri dvoch, tiež tvorivých dcérkach doma za vedro, špeciálnu malú enkaustickú žehličku zasupluje klasická nenaparovacia žehlička s hladkým povrchom a papiera je v našej domácnosti, už zo spomínaných dôvodov habadej. Žehličku máme len jednu, preto sa budeme musieť striedať. Keďže výtvarnú techniku objavila manželka, prenechávam jej právo začať.   Ona priloží farebnú voskovku k horúcemu povrchu žehličky a potom roztopený farebný vosk rozotrie jemným pohybom po papieri. Po zdvihnutí žehličky sme obaja  fascinovaní, a naša fascinácia ešte narastie po pridaní ďalších farieb.             Potom preberám štafetu a žehličku odmietam pustiť z rúk niekoľko hodín. Je zaujímavé, že ako málo stačí, aby si človek urobil radosť a ako ho potešia aj jednoduché farebné ilúzie, ktoré vytvorí farebný vosk a teplo. Takto som sa ja včera večer so žehličkou pozabával a toto z toho vzniklo:                                                                   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4080
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




