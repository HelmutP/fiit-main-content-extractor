

    
 Snažil som sa dať nejako dokopy moje vlastné priority,, preto prosím o toleranciu čitateľov, ktorých rebríček sa od toho môjho odlišuje,, veď každý sme iný, a každý má právo na vlastný názor,, pre každého je dôležité niečo iné,,  
 pred časom som považoval za prioritu zastaviť Fica,, uvedomil som si však, že to ale nie je to najdôležitejšie,, tento článok ale nie je o tom, prečo nepovažujem za najdôležitejšie zastaviť Fica,, je práve o tom, čo považujem za najdôležitejšie,, 
   
 No a teraz ten môj rebríček: 
 1. nezadlžovať ďalej krajinu,, viesť štyri roky štátny rozpočet do mierneho zisku, alebo na nule,, ale hlavne bez pôžičiek,,  
 To je pre mňa kritérium číslo jedna,, do ďalšieho výberu sa dostanú len strany, ktoré sa k tomuto vo svojich predvolebných programoch zaviažu,, ak sa k tomu nezaviaže ani jedna, tak žiaľ, nebude koho voliť,, aspoň nie pre mňa,, 
  2. zachovanie suverenity Slovenskej Republiky, hlavne vo vzťahu k Európskej Únií,,  
 Je v poriadku, že Slovensko spolupracuje z inými krajinami sveta,, a najmä z tými najbližšími, teda zo štátmi Európy,, avšak druhým mojím kritériom na voľbu je práve zachovanie suverenity,, chcem, aby o zásadných veciach rozhodoval slovenský parlament, a nie Európsky,, prečo?,, v Európskom parlamente je vyše 700 poslancov,, z toho len 13 Slovákov,, a to je dosť málo na to, aby sa tam prijímali zásadne rozhodnutia ohľadne Slovenskej Republiky,,  
 Aby som to celé nejako priviedol k záveru, tak strana musí garantovať, že ak bude mať svojho zástupcu v NRSR, tak bude rozhodovať na základe potrieb Slovákov, a nie na základe pokynov z Únie,, a tiež, že v kľúčových veciach, nebude rešpektovať nie len názor Únie, ale ani jej rozhodnutie,, 
  3. zavedenie nedotknuteľnosti národných parkov na Slovensku,, 
 V našej krásnej krajine máme 9 národných parkov,, 9 oblastí, kde by človek nemal zasahovať do prírody,, avšak najmä kvôli turistickému ruchu a kvôli ťažbe dreva tomu tak nie je,, rozloha týchto národných parkov je spolu 3 178, 48 km2 (http://sk.wikipedia.org/wiki/Zoznam_n%C3%A1rodn%C3%BDch_parkov_na_Slovensku),, celková rozloha Slovenskej republiky je 49 035 km² (http://sk.wikipedia.org/wiki/Slovensko),, čiže mojím tretím kritériom je, aby sa kandidujúca strana zaviazala zaviesť nedotknuteľnosť týchto 3 178,48 km2 našej prírody,, 
   
 No a teraz by som sa rád vrátil k názvu tohto článku,, prosím v ňom o radu,, o radu, koho voliť,, neprosím o radu, aké kritéria si pre výber kandidáta vybrať,, moje kritéria sú zhrnuté v týchto troch jednoduchých bodoch a ostatné je pre mňa menej podstatné,, a ako som už spomínal, nemusia sa zhodovať s kritériami iných,, ale myslím si, že je viac ľudí, pre ktorých sú kľúčové práve tie body, ktoré som tu spomenul,,  
 Teda,, ktoré politické strany majú okrem iného aj toto vo svojom programe?,, 
 Štefan Glova 
   

