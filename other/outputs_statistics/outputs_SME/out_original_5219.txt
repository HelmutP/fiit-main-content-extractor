
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Gogola
                                        &gt;
                Ľudia a lesy
                     
                 ...pocit márnosti? 

        
            
                                    25.4.2010
            o
            13:03
                        (upravené
                26.4.2010
                o
                9:39)
                        |
            Karma článku:
                11.37
            |
            Prečítané 
            1423-krát
                    
         
     
         
             

                 
                    Stretol som sa s priateľom. Navštívil som ho, aby som mu zablahoželal k meninám.  Sedeli sme spolu uňho doma v obývačke, spolu s našimi manželkami, obklopení jeho džavotajúcimi deťmi (má ich tri). Pred desiatimi mesiacmi sme obaja patrili k skupinke, ktorá rebelovala proti donebavolajúcemu šafáreniu v štátnych lesoch. Udalosti uplynulých mesiacov rozdelili naše cesty. Ja som od januára nezamestnaný (pre "nadbytočnosť"), on sa rozhodol odísť z manažérskeho postu na generálnom riaditeľstve na pozíciu pestovateľa na lesnú správu v Bohom zabudnutej doline, kde líšky dávajú dobrú noc (alebo česť práci?).
                 

                 
Keď sa veci začínali hýbať...Foto: Edo Genserek
   "Tak ako žiješ? Dá sa?", opáčil som, kým jeho manželka ukladala deti spať.   "Čo chceš počuť? Vidím tú mizériu na vlastné oči. Zalesňujeme vďaka podpore z eurofondov. Keď skončí, nebude za čo zalesňovať. Smrečiny v tom čase už budú pod zámienkou ich chradnutia zrúbané, či v nich šarapatí podkôrnik, alebo nie."   "Súčasné vedenie sa zrejme odkazom Dekreta Matejovie príliš neinšpiruje..."    "No, ten sa musí chudák v hrobe obracať!". Obaja sme sa zasmiali.   "Myslíš, že to, čo sme urobili, malo nejaký zmysel? Otvorený list premiérovi, trestné oznámenie, petícia, celá tá rebélia? Odvolali generálneho riaditeľa, potom ministra, vymenili vedenie a výsledok - psy štekajú a karavána ide ďalej. Vlastne nie, psy rozohnali.", nastolil som "filozofickú" otázku.   "Určite áno, malo to zmysel. Pre nás, čo sme sa na tom podieľali. Zachovali sme si sami pred sebou svoju česť a dôstojnosť. Ale inak to neprinieslo asi nič. Nás lesníkov to nepozdvihlo. Ostali sme prikrčení, so sklonenou hlavou.", povedal a zjavne posmutnel.   "Vieš, mám taký pocit, že to nie je len problém lesníkov. Celá spoločnosť je apatická, znechutená, otrávená každodennými informáciami o tunelovaní, rozkrádaní, podvodnom financovaní, korupcii a podobne. Obyčajní ľudkovia nechcú celý život protestovať, manifestovať, rebelovať. Chcú normálne žiť."   Odchádzal som z návštevy u priateľa s hlavou plnou myšlienok. Oňho sa nebojím, je to silný, čestný a pracovitý človek. Nestratí sa. Ale to, čo povedal o "prikrčení a sklonení hlavy", to vystihol bohužiaľ presne. Sme stádo. Politici to vedia a kalkulujú s tým. Teraz nás oblbnú predvolebnou kampaňou a po voľbách sa ten kolotoč rozkrúti opäť. V štátnych lesoch i všade tam, kde má vplyv politika. A keď o pár rokov (alebo desaťročí?) budeme riešiť problém zdevastovaných lesov, nedostatku pitnej vody, atď., asi ma nenaplní pocit zadosťučinenia, "...však sme im to hovorili!"  Skôr naopak. Naplní ma pocit márnosti.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Referendum–dum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Bubeníkove osudy III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Stopárov sprievodca Banskou Bystricou (1. časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Včera som stretol dôchodcu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Otvorený list budúcemu ministrovi pôdohospodárstva
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Gogola
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Gogola
            
         
        petergogola.blog.sme.sk (rss)
         
                                     
     
        Komárňan, domestifikovaný v Banskej Bystrici. Milovník prírody a dobrého vína. Fanúšik rockovej hudby, bubnov, bežiek a Monthyho Pythona.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Banská Bystrica
                        
                     
                                     
                        
                            Ľudia a lesy
                        
                     
                                     
                        
                            Zbrane
                        
                     
                                     
                        
                            Bubny a bubeníci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




