
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslava Trungelová
                                        &gt;
                Príliš moje...
                     
                 Prežívať posledné stredoškolské dni II 

        
            
                                    13.5.2010
            o
            16:45
                        (upravené
                15.5.2010
                o
                9:38)
                        |
            Karma článku:
                4.07
            |
            Prečítané 
            414-krát
                    
         
     
         
             

                 
                    Po ceste do školy prejsť všetky uličky... Kúpiť si tradične 2 hot-dogy, netradične nezabalené.
                 

                 
  
   Spať na sedačke, ktorá je oranžová.   Na literatúre...nepočúvať   Mať z maturitného slohu 100%.   Vyprázdniť skrinky a cítiť prázdno.   Odovzdať kľúčik, tisíckrát stratený.   Naplniť si igelitku spomienkami.   Behať po škole... naposledy.   Robiť si srandu z triednej... z profesorom "Bé".   Odpovedať pred tabuľou a prešľapovať.   Buchnúť si hlavu o stĺp v triede.   Dražiť výzdobu... zadarmo.   Sadnúť si vedľa profesorky nemčiny a mať v očiach strach.   Na poslednej nemčine mať Nichts auf dem Herz a tešiť sa auf dem Freitag.   Zarobiť si cez leto a ísť stanovať do Belgicka, bez stanu... Dám ti vedieť.   Tešiť sa na poslednú telesnú, ktorú pokazili.   Zistiť, že žraloky majú nekonečno zubov.   Nájsť saponát na podlahu, chcieť si zobrať do flaštičky a mať aspoň jednu voňavú spomienku na prázdnu rannú rozospatú triedu.   Skonštatovať, že v škole nemáme hasiaci prístroj na snehovú vojnu.   Odchádzajúc z vyučovania venovať profesorom smutné úsmevy, ktoré bolia.   Zjesť cudziu čokoládu, lebo je dobrá.   Sedieť v škole, aj keď mám 4 hodiny voľné.   Počúvať hovorcu organizácie „Pite s rozumom“.   Nie, nemyslíme si, že Vilko je debil.   Robiť zbytočné veci... nevadí.   Pred tabuľou sa treba ukľudniť.   Zistiť, že matematiku treba riešiť komplexne...   ..a že priamka nie je úsečka...   ...že vieme celkom spievať, ak bude Miro trochu tichšie.   Poslednýkrát zažiť naštvanú triednu.   Tešiť sa z tabla a lepiť fotky.   Trénovať si podpis a byť nervózni.   Byť trochu narcistickí.... priznať si, že lepší ako my na tú školu už nezavítajú.   Odísť o niečo skôr, ako prídu slzy...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Trungelová 
                                        
                                            Najkrajší deň môjho života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Trungelová 
                                        
                                            Srdce na dlani
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Trungelová 
                                        
                                            Kam sa podel môj starý život?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Trungelová 
                                        
                                            Dlho som sa neozvala... však?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Trungelová 
                                        
                                            Tesco Stores a.s.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslava Trungelová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslava Trungelová
            
         
        miroslavatrungelova.blog.sme.sk (rss)
         
                                     
     
         MVC. 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    753
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Postrehy z ulice
                        
                     
                                     
                        
                            Tvorba
                        
                     
                                     
                        
                            Na cestách
                        
                     
                                     
                        
                            Básne
                        
                     
                                     
                        
                            Knihy, knižky, knižôčky
                        
                     
                                     
                        
                            "Veterinárske" postrehy
                        
                     
                                     
                        
                            Príliš moje...
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Láska nie je klietka
                                     
                                                                             
                                            Život je len dočasný
                                     
                                                                             
                                            My dvadsaťroční
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Evitapress
                                     
                                                                             
                                            Stephenie Meyerová - Zatmenie
                                     
                                                                             
                                            Stephenie Meyerová- Nov
                                     
                                                                             
                                            Stephenie Meyerová - Súmrak
                                     
                                                                             
                                            J.D.Salinger-Kto chytá v žite
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Alena Šlezingerová
                                     
                                                                             
                                            Domi Zuborová
                                     
                                                                             
                                            Venuša Dičérová
                                     
                                                                             
                                            Maslen
                                     
                                                                             
                                            Juraj Kumičák
                                     
                                                                             
                                            Romana Štefunková
                                     
                                                                             
                                            Ján Gergel
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Fantasy svet
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




