

 V predchádzajúcom článku (Trampoty so stavebnou firmou GREVA Investment) som sa Vám "posťažoval s problémami, ktoré máme s prevzatím/odovzdaním domu do užívania. 
 Zároveň som Vám sľúbil, že Vám a budúcim zadávateľom objednávky na stavbu domu ukážem, ktoré všetky odkazy na nete sa týkajú predmetnej spoločnosti GREVA Investment, s.r.o. 
 Po kliknutí na obrázok sa vám otvorí originálne umiestnenie. 
 Domovská stránka spoločnosti vyzerá takto:  
 V inzerátoch sa stretnete aj s týmto domom, ktorý je z danej lokality Harmónia:  
 Aj toto je ponuka z lokality Harmónia, ktorú realizuje spoločnosť GREVA Investment, s.r.o.:  
 Aj takúto prezentáciu predmetnej lokality nájdete:  
 Toto je dom, ktorý už bol odovzdaný a užívaný a je na predaj. Prečo?  
 Takto sa prezentuje pekná lokalita (čo je pravda) a kvalita (dovolím si pochybovať).  
 V inzerátoch to vyzerá super:  
   
 Prečo tento článok? 
 Aby sme pripravili ľudí na možné problémy v prípade objednania si stavby rodinného domu od neseriózneho stavebníka.  Dúfam, že ľudí, ktorí sa rozhodli stavať dom a stali sa rukojemníkom stavebníka bude čo najmenej. 
   
 PS: Ešte sa tejto téme budem venovať, nakoľko sa nám dostalo reakcií na predchádzajúci článok a začínajú sa rysovať ďalšie problémy, na ktoré sme doposiaľ neprišli, ale odborníci (stavbári), ktorí sa nám ozvali ich odhalili po preštudovaní priložených povolení, správ a rozhodnutí vydaných počas stavebného konania a výstavby. 
 PS II: Jeden z diskutérov nás upozornil aj na nutnosť izolovať železobetónový veniec 6 cm STYRODUR-om, ako máme uvedené v projekte. Realizáciu vidíte na obrázku.  
 Predpokladám, že daný diskutér je buď z danej spoločnosti GREVA Investment (alebo pre nich v minulosti robil), alebo niekto, kto vie o ktorý dom sa jedná, pretože pri minulom článku som fotky stavby nemal zverejnené a on nás aj napriek tomu na túto závadu upozornil a keď som sa pozrel na fotky, tak má pravdu. 

