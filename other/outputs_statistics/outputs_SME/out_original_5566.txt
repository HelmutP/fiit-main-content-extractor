
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Greškovič
                                        &gt;
                Nezaradené
                     
                 Pekné dievčatá sú ako tiene 

        
            
                                    30.4.2010
            o
            16:53
                        (upravené
                30.4.2010
                o
                17:28)
                        |
            Karma článku:
                7.13
            |
            Prečítané 
            1897-krát
                    
         
     
         
             

                 
                    Nedávno som narazil na jednom internetovom fóre na zaujímavé diskusné vlákno. Volalo sa "Beautiful girls are solar powered." Jadrom diskusie bolo očividné pozorovanie, že s príchodom prvých teplých jarných dní sa na uliciach náhle objaví nevídané množstvo pekných dievčat. To je niečo, čo určite potvrdí každý muž, ktorý sa o také veci trochu zaujíma. S týmto však vyvstáva jedna zaujímavá otázka: kam sa všetky tie pekné dievčatá podejú v zime?  
                 

                 Objavilo sa množstvo teórií, ktoré sa tento fenomén snažili vysvetliť. Niektorí tvrdia, že pekné dievčatá v zime hibernujú. Tak ako medvede, aj ony sa poberú na zimný spánok. Iní sú zasa presvedčení, že sa na zimu zakuklia, aby mohli v jari vyletieť znova von, tak ako motýle... prípadne sa premieňajú na niečo, čo zďaleka nie je také príjemné na pohľad. A možno, že (aspoň niektoré) odlietajú na zimu do teplých krajín, tak ako vtáky.  Ja si však myslím, že je to celé oveľa jednoduhšie. Tvrdím, že pekné dievčatá jednoducho prestanú existovať krátko po tom, ako sa pokazí počasie. Akokoľvek absurdne to na prvý pohľad zneje, asi to je to najjednoduhšie vysvetlenie. Nie je ťažké nájsť príklady iných vecí, ktoré sa správajú podobne. Hneď naporúdzi sú, ako aj titulok napovedá, tiene. Kým svieti slnko, sú jasné, vyhľadávané a zreteľne viditeľné pre každého, kto sa pozerá. No len čo sa slnko čo i len skryje za mrak, tiene už nie sú. Nikam neodišli, na nič sa nepremenili, jednoducho -- zmizli.  Možno je to len tým, že si ich inokedy menej všímame. Možno je to len kratším (či priesvitnejším) oblečením, lepšou náladou alebo niečím iným. No nech už to je spôsobené čímkoľvek, isté je, že nás to o pekné dievčatá ľahko pripraví. A s nimi aj o všetky tie pohľady, myšlienky, príjemne pocity, ktoré sú s nimi spojené. A to nám dáva ešte o jeden dôvod viac ceniť si a obdivovať tieto krásne a také krehké stvorenia, ktoré okolo seba vidíme. Užime si ich, kým môžme!       Pozn.: Tento blog bol pôvodne napísaný v angličtine. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Greškovič 
                                        
                                            Veľký tresk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Greškovič 
                                        
                                            Praktické problémy moderného človeka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Greškovič 
                                        
                                            Zdravotné poistenie v EU
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Greškovič 
                                        
                                            Top 5 stopárských zážitkov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Greškovič 
                                        
                                            Návrat do luxusu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Greškovič
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Greškovič
            
         
        greskovic.blog.sme.sk (rss)
         
                                     
     
        Zvedavý človek, astronóm, programátor, (rádoby zasa) študent a mnoho ďalšieho...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Veľký tresk
                     
                                                         
                       Letíme nízkonákladovkou - EÚ vs. Ázia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




