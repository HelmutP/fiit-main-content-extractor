
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriela Oremová
                                        &gt;
                Nezaradené
                     
                 Na to najdôležitejšie som zabudla 

        
            
                                    29.5.2010
            o
            22:35
                        (upravené
                8.6.2010
                o
                21:21)
                        |
            Karma článku:
                7.36
            |
            Prečítané 
            1041-krát
                    
         
     
         
             

                 
                    Mala som štrnásť. Moja kamarátka bola o rok staršia. Oslavovala narodeniny aj to, že ju prijali na vysnívanú strednú školu. Tiež už mala svoj novučičký občiansky preukaz. Rodičia jej pripravili oslavu na ktorú som bola pozvaná.
                 

                 S darčekom a kvietkom som vstúpila cez predzahrádku do dvora. Všade bolo niečo porozťahované. Papuče, hračky, papieriky sa váľali po tráve aj poletovali vo vetre. Opatrne som otvorila dvere  do predsiene, kde mi už vybehla v ústrety oslávenkyňa.   Vošli sme spolu do jedálne. Sedelo tam niekoľko dospelých v dobrej nálade. Deti pobehovali snáď všade a veselo šantili. Stôl bol plný vtedy populárnych obložených chlebíčkov, zákuskov a nechýbala žltá malinovka v sklenených fľaškách a čapovaná kofola.   Hneď mi priniesli stoličku a tanierik a priateľka mi poukazovala darčeky. Po chvíľke priniesla pani domáca ašte horúcu štrúdľu, ktorá rozvoniavala po celom dome. Každý dostal jeden kúsok s uistením, že ďaľší plech sa pečie.   Pred odchodom na oslavu ma babka s mamou inštruovali o slušnom správaní sa na návšteve a tiež o tom, že je čas prísť aj čas odísť. Radšej som ani nečakala, kým prinesú  ďaľšiu porciu mojej obľúbenej pochúťky a rozlúčila som sa s tým, že večer sa na ulici pod lampou stretneme.   Dobehla som domov a začala som rozprávať o všetkom, čo sa tam odohrávalo. Ako tam pobehovali deti a robili krik a neporiadok a ako sa cez chodbu za kopu vecí nedalo ani prejsť a o všetkom, čo bolo rozhádzané po dvore.....Babka sedela a počúvala a stále viac sa na mňa mračila. Nechala ma rapotať hodnú chvíľu a potom sa ma spýtala, či to je už všetko, či mi tam ani nič neponúkli.   Zahanbila som sa a povedala som, že okrem iného teta pripravila aj vynikajúcu štrúdľu. Vtedy sa babička zamračila ešte viac a urobila mi školenie na ktoré som nikdy nezabudla. Zdôraznila, že všetok neporiadok na dvore aj v dome sa ľahšie uprace, ako sa dá do poriadku zlé svedomie, ktoré má človek, keď ohovára a nevšíma si podstatné veci.   Vždy na konci mája mi príde na um tento príbeh, lebo dnes nemá narodeniny len moja bývalá kamarátka, ale aj moja dcéra a moja mama, ktorá už nežije, by slávila meniny. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Skúsim
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Miesto pre život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Vôbec nechápem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Nečakaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Zázračná hruška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriela Oremová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriela Oremová
            
         
        oremova.blog.sme.sk (rss)
         
                                     
     
        Som presvedčená ,že ak človek do svojho zámeru vloží silu svojej duše a teplo svojho srdca, celé nebo sa spojí, aby mu pomohlo!

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




