

 Pražská továreň odovzdala už 28. apríla 1900 prvú lokomotívu 197.82 ako továrenské výrobné číslo 1. Bola slávnostne vyzdobená a vpredu mala pripevnený ozdobný štítok s číslom 1 a nápisom Na Zdar! Preto sa im neskôr zvyklo hovoriť aj „Nazdárek“. Lokomotíva 197.82 bola v prevádzke na miestnej železnici Šilperk – Štíty (dnes Dolní Lipka – Štíty). V roku 1904 bola prečíslovaná na 97.182 a v roku 1914 bola v prevádzke v Černovicích na Bukovině (Halíč). Pražský závod vyrobil v rokoch 1900 až 1913 celkovo 39 lokomotív radu 97, v roku 1918 prešlo k ČSD 31 z nich. U ČSD ostali i ďalšie stroje tohto radu vyrobené v Rakúsku. V roku 1924 ČSD preznačili celkovo 133 strojov na radu 310.0. 
   
  
  
  
  
  
   
 Lokomotívy radu 310.0 z Českomoravskej továrne boli typu C m2t s vylepšenou konštrukciou oproti strojom tohto radu, vyrobeným v iných podnikoch v rokoch 1883 – 1899. Ich kotol zabezpečoval mokrú paru. Objemný komín obsahoval ružicu, ktorá odrážala iskry strhnuté prúdom vyfukovanej pary. Ich úlet komínom sa takto zmenšil a už nepredstavovali také riziko pre svoje okolie. Kúrenisko kotla bolo medené. K napájaniu kotla vodou sa pôvodne používali sacie injektory pod búdkou, po jednom na ľavej a pravej strane. Takto bolo možné čerpať vodu zo studní k tomu prispôsobených (v niektorých staniciach miestnych železníc) a tak doplňovať zásoby vody do vodojemu. Rám lokomotívy bol plechový a ložiská klzné. Iba niekoľko lokomotív malo lúčovité kolesá. Zásoby vody boli umiestnené po stranách kotla, zásobník uhlia bol na ľavej strane pred búdkou. K výbave patrila jednoduchá sacia brzda a ručná brzda.  
   
  
   
 Stroje určené pre dopravu vlakov mali i prestavovaciu automatickú saciu brzdu. Brzdovým vysávačom (ejektorom) sa vytváral podtlak v brzdovom potrubí a brzdovom valci. Atmosferický tlak na jednej strane brzdového piestu spôsobil jeho pohyb do priestoru podtlaku v brzdovom valci. Piestna tyč prenášala pohyb na brzdové tyče, ktoré pritlačili brzdové obloženie na obruče kolies. Stroje boli neskôr vyzbrojené tlakovou brzdou, vodojem sa týmto skrátil a pribudol kompresor. 
  
   
 Hlavné technické údaje: pretlak pary v kotle 1,1 MPa, plocha roštu 1,04 m2, výhrevná plocha kotla 59,1 m2, zdvih piestov 480 mm, počet a priemer valcov 2 x 345 mm, priemer spriahnutých kolies 930 mm, adhézna a celková hmotnosť 30 t, max. výkon 250 k (188 kW) až 300 k (221 kW), max. rýchlosť 40 km/h, zásoby vody 3 až 4 m3, zásoby uhlia 1 – 1,5 m3, najmenší možný polomer prechádzaného oblúka 90 m. V priebehu prevádzky došlo k rôznym zmenám a úpravám na ďalších zariadeniach. Dvojdielne dymnicové dvierka nahradili jednodielne okrúhle dvierka, dosadil sa valcový komín a hlavne došlo k podstatnému zjednoteniu parametrov kotla a jeho vybavenia.  
   
  
   
 Lokomotívy radu 310.0 sa na lokálnych železniciach veľmi osvedčili, a to i na kopcovitých tratiach. Vzhľadom na to, že hmotnosť vlakov neskôr stúpla, museli ustúpiť lokomotívam radu 423.0, ktoré sa tiež vyrábali v Prahe. Dosluhovali na posune až do času, kým ich aj odtiaľ nevytlačili motorové lokomotívy. Posledný stroj tejto série – 310.097 – skončil na posune v lokomotívnom depe v Bratislave hl. s. a z prevádzky bol vyradený v roku 1968. V roku 1970 sa do Národného technického múzea v Prahe dostala zachovaná lokomotíva 310.0118.  
   
  
   
 Takmer 70 rokov prevádzky dosvedčuje výnimočnú kvalitu lokomotív, ktoré otvorili cestu dlhoročnej výrobe parných lokomotív v podniku ČKD Praha. Konštruktérska genialita a mimoriadna obľúbenosť medzi ľuďmi sa odzrkadlili aj na množstve modelov i hračiek vyrobených v rôznych mierkach a prevedeniach, od papierových až po celokovové. 
   
  
  
  
  
   
   

