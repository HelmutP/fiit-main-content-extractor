
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Dočítal som Stratený symbol 

        
            
                                    9.6.2010
            o
            11:11
                        (upravené
                9.6.2010
                o
                14:49)
                        |
            Karma článku:
                4.40
            |
            Prečítané 
            1194-krát
                    
         
     
         
             

                 
                    Teda tak, naozaj až teraz, hoci knihu autora Dana Browna Stratený symbol som po prvý raz držal v ruke ešte v predvianočnom ruchu v kníhkupectve nášho MAX-u, ako familiárne môj pomaly dospievajúci tínejdžr nazýva stredisko s mnohými službami a ešte širšou predajnou ponukou. Vedel som celkom presne, ako si túto knihu túži nájsť pod stromčekom a naozaj úprimne ma trápilo že si to neželá nástojčivo, ale skôr opatrne, lebo dobre vie, ako jej exkluzivita „kradmou rukou siaha na moje možnosti voľnej dispozície s dostupnou menou.“ To viete, darčeky v takej konštelácii faktov, keď máte hlboko do vrecka bývajú skôr praktické, lebo len čo je treba, to sa kúpiť musí, no napokon som neodolal a knihu som si dal zabaliť. Niekoľko dní ležala doma ako keby nepovšimnutá a priznávam, že naozaj nástojčivo som sa chcel pustiť do jej čítania. Celkom iste by som to do Vianoc stihol, aj ju znova darčekovo zabaliť, ale odolal som! Obdarovaný má nárok dostať ju nepoškvrnenú, bez zásahu rušivých vplyvov a s odstupom času presne viem, že jej mystický obsah sa prejavil práve takto.
                 

                     A pochopiteľne, viem celkom spoľahlivo aj to, že väčšina národa na blogu už Stratený symbol prečítala, ale hoci som tu dva roky fyzicky vôbec nebol, naozaj som nenašiel ani len slovíčko o tom, že sa voľakto priznal či vyznal, čo ten dej, odkaz a štýl diela s ním porobil, ako sa podpísal na jeho myslení, čo urobil s hodnotami v srdci či v hlave a či sa mu vôbec žiadalo niekomu o tom hovoriť!? Lebo naozaj je to tak, že kniha je svojim účinkom na človeka mocnou energiou, často nesmierne komplikovanou a tých necelých 600 strán textu ma neustále nútilo sa v plynulom toku rozprávania zabrzdiť, popremýšľať, ba veru sa aj vrátiť, lebo túžba správne sa orientovať a pochopiť všetky súvislosti, to naozaj predstavuje komplexnú filozofiu niekoľkých svetonázorov.   Prečítal som samozrejme všetky Brownove bestselery, ak sa nemýlim, tak tento je v poradí piaty a pravdupovediac, je svojim obsahom a odkazom najkomplikovanejší, dejovo najrýchlejší a podľa mňa najtajomnejší, hoci nám rozpráva udalosti „žeravej“ prítomnosti. Neuveriteľne veľakrát som sa zabrzdil pri jednoduchej dedukcii zrodu voľačoho svetového naozaj z ničoho, iba z dobre známej pravdy. Takej, čo poznáme všetci a z ničoho nič bola k dispozícii myšlienka fantastickej hodnoty a významu a ak sa čitateľovi podarí si ju osvojiť, je to niečo na spôsob osvietenia ducha. Vopred všetkým čo ju nečítali pripomínam, že kniha je napísaná dynamicky, väčšina kapitol má charakter krátkeho a kompaktného textu, nie je v nej veľa postáv, ale aj tak, potreba všetko pochopiť bezozvyšku si vyžaduje postáť!   Rozprávať príbeh je zbytočné, sú to dobrodružstvá prežité pri pátraní po tajomstvách slobodomurárov, čo je tajné spoločenstvo geniálnych ľudí v histórii ľudstva, ktorí nám ako odkaz zanechali kamennú pyramídu so zašifrovaným textom a tá má byť mapou na ceste za najväčším bohatstvom ľudstva. Podľa náznakov a dejových odbočiek dlho tušíte, že to bude niečo na spôsob záhad premeny myšlienky na niečo hmatateľné a zároveň zázračné a tento vedný obor budúcnosti v knihe predstavuje Katherine, sestra jednej z hlavných postáv románu Petra Solomona, správcu a najvyššieho strážcu slobodomurárskych tajomstiev. Tak ako v Da Vinciho kóde je ústredným hrdinom príbehu Robert Langdon, univerzitný profesor so špecializáciou symbológ, ktorý okrem dobrodružstiev na ceste za odhalením tajomstiev prežije naozaj všetko, čo si dokážete v tej najväčšej fantázii predstaviť. Aj smrť, vzkriesenie, poznanie súvislosti obsiahnutých v tej úžasnej novej vede s menom noetika a veru sa treba poriadne posošiť, aby ste sa udržali vždy v dokonalom obraze. To slovíčko pomenúvajúce nový vedný odbor – noetika – je niečo až mystické, ako ostatne takmer všetko s čím sa v románe stretnete, ale myšlienka, že myslenie jedinca, alebo kolektívu ľudí môže ovplyvniť materiálny svet, zdravie, vnútrobunkový svet, alebo čokoľvek iné reálne je fascinujúca. Spojenie sveta biblických textov, bájnej histórie a súčasnosti – možno až v pôsobení noetických dôsledkov – to je v závere románu najúžasnejším odkazom myšlienok všetkého napísaného. Dobrodružstvo bolo neuveriteľné, dramatické, naozaj ničomu doteraz prežitému podobné, ale jednoduchosť odkazov a vyslovené poučky sú napriek svojej chronickej známosti geniálnymi odkazmi.   A ešte niečo, v texte knihy sa čitateľ dozvie naozaj veľa o mentalite amerického národa, o jeho histórii a architektúre, o slávnych v dejinách a celkom iste objaví výnimočnosť hlavného mesta Washingtonu, v ktorom je nielen sláva a tradície, ale veru aj neuveriteľné poklady. Budete ich objavovať so zatajeným dychom, ak sa do čítania knihy pustíte. A nakoniec sa priznám, hoci s odstupom času, že som bol úprimne rád, že som synovi ten vianočný darček vinou zvedavosti nepoškvrnil v jeho neprečítanej panenskosti, v tej rýchlosti časovej tiesne by som väčšinu odkazov nepočul, nepochopil, obišiel a celkom iste zle prečítal, lebo tento román musíte čítať nielen očami, ale predovšetkým hlavou a bez výnimky aj srdcom. Verte mi, to je nevyhnutná podmienka jeho pochopenia a akceptácie. Sám pre seba som si definoval, že som dostal najlepšiu filozofickú školu od čias povinného štúdia základov ľudského myslenia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




