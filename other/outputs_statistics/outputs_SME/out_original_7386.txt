
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Kumičák
                                        &gt;
                Nezaradené
                     
                 Srandista Orbán a červená kravatla 

        
            
                                    27.5.2010
            o
            9:57
                        (upravené
                27.5.2010
                o
                9:51)
                        |
            Karma článku:
                15.90
            |
            Prečítané 
            1780-krát
                    
         
     
         
             

                 
                    ...dnes trocha ostrejšie...
                 

                 Nemyslím si, že dvojité občianstvo Maďarov je rozumný nápad. Skôr ide o populizmus a zbytočné politikárčenie. A asi aj provokovanie - hlavne prostoduchých slovenských hlupáčikov, ktorí na takúto provokáciu skočia. Ale tiež si nemyslím, že dvojité občianstvo je pre SR problém. Nie je, lebo nemá sa ako stať problémom. Dokonca si nemyslím, že by problémom pre SR bolo  trojité, štvorité ... občianstvo našich občanov. Lebo pokiaľ občan SR žije v rámci zákonov a platí dane, kľudne nech má aj 26 občianstiev a farebné brká narvaté v drdole, pre túto krajinu to žiaden problém byť nemôže. Môže to byť problém pre Maďarsko, že si chce do svojho volebného priestoru vpustiť pár sto tisíc inostrancov, ktorí môžu razantne preskupiť politiku tejto krajiny. Ale to je problém Maďarska, a nie normálnych a mysliacich ľudí v tejto krajine.   Tak prečo potom toľko kriku? No..., sú tu dva dôvody.   Prvý dôvod : Orbán sa bez servítky x krát vyjadril, že hlavnou úlohou jeho vládnutia bude vyčistiť krajinu od sociálnodemokratického sajrajtu. A predovšetkým táto snaha je pre Fica tým hlavným bezpečnostným rizikom. Lebo čo ak to Orbánovi vyjde? Čo ak sa krajina, ktorá je dnes kdesi hlboko v ľavičiarskom bahne, začne dvíhať? A začne sa zrazu približovať svojmu susedovi, ktorý vďaka nášmu sociálnodemokratickému sajrajtu začal SMERovať do nášho, zvrchovaného a národného, vlastného bahna? A čo keď to uvidia naši občania? Napríklad tí na juhu Slovenska? A začnú porovnávať? A z toho porovnávania vyjde, že hlboko sociálne cítiaci slovenský sajrajt je horším riešením ako ten zlý zlý zlý Orbán? Toto je bezpečnostné riziko tak veľké, že sa oplatí zvolávať bezpečnostnú radu, parlament, šamanov a matku Pôstkovú, by na poplach sa bilo zdivoka...   Druhý dôvod je ale ešte vážnejší : Orbán niekoľko krát úplne nezakryte dal najavo, že nejakého Fica má v päte. A to sa Fico strojil stretnúť sa kvôli dvojakému občianstvu s Orbánom, by sa naň vybliakal, o strašne strašne tvrdom postupe mu povedal, a novú červenú kravatlu ukázal, nech všetci vidia, že smelým je a parádnym. A Orbán? Má Fica v päte a počká si na toho, kto vylezie z nových volieb. No ako má teraz Fico pospolitému svojmu ľudu ukázať, že len on, on jediný, tým najväčším a najhlasnejším tu je, keď ho má ten Maďar v päte? Ako toto samoľúby a jediný spravodlivý môže prežrieť, a to sú voľby za dverami, prúserov za dva kýble, a retardov, čo by ho volili, čoraz menej? A čo to ten Orbán povedal? Že prvá návšteva jeho do Poľska bude smerovať? To ako k susedovi svojmu? To akože naša krajina pod vedením Fica už pre Maďara ani neexistuje? To ju Orbán už ani neakceptuje? Po toľkých pozitívnych Ficových výsledkoch (lacnejšie lieky, trinásty dôchodok, bezplatné školstvo, chránení občania...)? Nemyslí to ten maďarský srandista nedajbože tak, že krajinu by možno aj akceptoval, ale toho niktoša s červenou kravatlou v čele nie?   ...tu kdesi končí sranda. Neviem, či si Orbán robí prdel vedome, alebo to len tak vypálilo. Neviem, či si nejakého Fica váži. Každopádne sa správa tak, ako by si ho nevážil. Nuž - ak Fico sedel iba 30 cm od Slotu v momente, keď Slota splietal čo to o maďarskom šašovi na koni, a mlčal (zopakujem - ... Fico v tom momente mlčal, neohradil sa, nezaprotestoval, len mu po rozume kráľ, náčelník, predseda a generálny tajomník zväzu starých Slovákov súdruh Svätopluk v sedle Hatátitlu chodil), tak sa nedivím. Nedivím sa, že nejaký maďarský srandista neuznáva Fica (a žiaľ, možno s ním i celú našu krajinu). Lebo okrem maďarského srandistu si Fica nevážim ani ja, a dôvodov na hrdosť na túto krajinu za ostatné štyri roky mám čoraz menej. Ono hlavnou úlohou slovenskej vlády je vytvárať také Slovensko, na ktoré budú hrdí i Maďari tu žijúci. Tak hrdí, že budú mať nejaké iné občianstvo v päte. Lenže budovať krajinu sa akosi nedarí. A tak prichádza na rad tlupa idiotov. Najskôr si myslí, že donúti Slovákov používať spisovnú reč hrozením pokutami. Teraz si myslí, že naučí Slovákov loajalite hrozbou straty občianstva, a dokonca aj práce.   Idiot s červenou kravatlou vyhlásil, že dianie v Maďarsku je pre Slovensko životne nebezpečné. Nie! Životne nebezpečné je pre Slovensko vládnutie idiotov, ktorí si myslia, že metódami gulagu, hrozbami a trestami sa dá riadiť štát. Nedá. Nedokázal to Stalin, a nedokáže to ani ten jeho ideologický klon z Topoľčian.   A to nie som Maďar. Fakt... Nie som...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (124)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Kumičák 
                                        
                                            Chorý štát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Kumičák 
                                        
                                            Prečo toľko breše?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Kumičák 
                                        
                                            Róbertkovo divadlo...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Kumičák 
                                        
                                            O učiteľskom štrajku veselšie...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Kumičák 
                                        
                                            Žilinská nemocnica z iného uhla
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Kumičák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Kumičák
            
         
        jurajkumicak.blog.sme.sk (rss)
         
                                     
     
        Názorovo som väčšinou na strane menšiny. Názor väčšiny väčšinou nemám rád.
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2354
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Vojnové príbehy
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Povolebný návrat Smeru k duchovnému bratstvu s Harabinom
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Strach vo Ficových očiach
                     
                                                         
                       Chudák kandidát.
                     
                                                         
                       Pomôže Ficov „útek“ štátnemu rozpočtu?
                     
                                                         
                       Ako Mika ohýba Ficovi chrbát a ako prestávam platiť koncesionárske
                     
                                                         
                       Procházka, Kňažko, dnes - je to divné
                     
                                                         
                       Morálny víťaz volieb
                     
                                                         
                       Kto je úžerník a kto hlupák
                     
                                                         
                       Fico vs. Kiska a pán Bielik...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




