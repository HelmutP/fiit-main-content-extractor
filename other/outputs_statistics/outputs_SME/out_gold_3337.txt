

    
 Všetko čo chceš, všetko čo nepoznáš, všetko čo máš, aj to čo nemáš. Každú vec, aj každú nehmotu. Seba samého, svoju povahu a svoj život. Deň, ktorý príde, aj ten, ktorý už bol. Farbu svojich vlasov a svojich priateľov. Koniec príbehu a pohľad za zastretým oknom. Zvuk harfy a ľudský hlas. Chuť suši a kúpeľňu vášho budúceho domu. Klip k pesničke, ktorú počujete prvý, druhý a tretí krát. Tanečného partnera (:)) a kroky vlastného tanca. Životný štýl, počet schodov na štvrté poschodie, dokonalú maturitnú tému. 
 To všetko si môžeš vysnívať. 
 Ale to, čo nemôžeš, je snívať sám. 
   
   
 „Sen, ktorý snívaš sám je iba sen. Sen, ktorý snívame spolu je realita.“ 
  John Lennon 
   
   

