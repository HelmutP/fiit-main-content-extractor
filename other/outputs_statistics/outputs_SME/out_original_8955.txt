
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Veselý
                                        &gt;
                Životný štýl
                     
                 Mozog - výstup sa skladá z vstupov 

        
            
                                    18.6.2010
            o
            7:37
                        (upravené
                13.8.2010
                o
                10:53)
                        |
            Karma článku:
                8.57
            |
            Prečítané 
            1214-krát
                    
         
     
         
             

                 
                    Ako často zamestnávate svoj mozog? Čím ho zamestnávate? Nechávate ho odpočívať pri ľahkých témach? Alebo si ho cibríte a posilňujete? Viem, sú to ťažké otázky.
                 

                 Pre niektorých ľudí je pohodlnejšie ísť do priemernej práce a neskúmať, či by sa nedalo svoj čas a um investovať lepšie. Po práci je pohodlné prísť domov a pustiť si televízor. Prečítať si noviny a „vypnúť mozog“, ako mnohí povedia. Oplatí sa vám však častejšie „vypínať“ mozog?    To, čo v našom tele nepoužívame, slabne.    Ak do seba roky pumpujete povrchné informácie z televízie, rádia a novín, či klebety, aj vaše myslenie sa začne stávať povrchným. Pozor na to!   Žiaľ, masmédiá tlačia do národov celého sveta negatívne správy, pohromy, tragédie, zločiny. Naliehavé správy, ktoré už zajtra budú beznádejne zastarané a zbytočné. Milióny ľudí na celom svete si týmito informáciami poslušne plnia svoje hlavy s pocitom, že „treba byť v obraze“. Ale sú tieto „obrazy“ pre vás dobré? Potrebujete ich? Nevpúšťate do svojej hlavy aj zbytočné negatívne obrazy, ktoré môžu byť neskôr vaším podvedomím použité práve proti vám?   Negatívne správy z novín a televízie sa časom môžu stať spolutvorcom vášho ja, lebo ovplyvňujú vaše postoje, pocity, často následne aj sebavedomie a tým aj vašu budúcnosť.    Ak sa v mysli často venujete negatívnym správam, obrazom a postojom, tak v budúcnosti dostanete len viac negatívneho. Viac pesimizmu, viac depresie, viac pocitov márnosti a bezmocnosti. Potom si vytvoríte predstavu sveta, v ktorom je všetko nanič, nič nemá zmysel; veď v tejto fraške neexistujú už žiadni čestní ľudia, už sa nikomu nedá veriť.   Začnete sa prispôsobovať obrazu sveta, ktorý vám vykreslili vami vybraté médiá. Pritom to nie je pravdivý obraz. ale ak nemáte iné informačné vstupy, uveríte mu. Začnete akceptovať, že realitou sú len tragédie, pohromy, podlosť, podrazy, nenávisť, kriminalita.    Budete už vopred očakávať, že v živote sa aj tak nemôže bez protekcie nič vydariť, a že je zbytočné o čokoľvek sa snažiť. Ak tomu uveríte, potom si takú realitu naozaj spoluvytvoríte.   Preto je dobré byť opatrný na to, aké informácie vpúšťate do svojej hlavy. Ak je na vstupe veľa negatív, je veľmi pravdepodobné, že na výstupe, teda v budúcnosti, dostanete tiež veľa negatív. Prečo? Lebo platí priama úmera: Čomu venujeme viac pozornosti, toho dostaneme viac.   Dostanete viac toho, čomu pravidelne venujete pozornosť.   Tento výrok si dobre zapamätajte. Ak ho využijete, zmení vám život k lepšiemu.     Ak budete venovať pozornosť negatívnym veciam, budete ich do svojho života priťahovať ako magnet. Prečo priťahovať? Pretože ich oceňujete svojou pozornosťou.   Ak budete venovať pozornosť pozitívnym veciam, budete ich do svojho života priťahovať ako magnet. Ak si budete všímať veci príjemné, priaznivé, odrazu zistíte, že aj váš život je celkom fajn. Oceňte u ľudí to, čo sa vám na nich páči, všimnite si to. Dostanete od nich viac príjemných a pekných prejavov. „Len“ preto, že ich oceňujete svojou pozornosťou.     Nabudúce viac o tom, ako výberom toho, čomu venujete pozornosti ovplyvníte svoj život. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Test: Okuliare na ochranu krčnej chrbtice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako nepribrať cez sviatky?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Kniha o chudnutí zadarmo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako sa pečie domáci chlieb - video
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Chudnutie a chôdza - video
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Veselý
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Veselý
            
         
        vesely.blog.sme.sk (rss)
         
                        VIP
                             
     
         Píšem na témy chudnutie, práca, opatrovanie a občas posielam fotky z výletov. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    412
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5070
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Videoseriál chudnutie
                        
                     
                                     
                        
                            O chudnutí krátko, stručne
                        
                     
                                     
                        
                            Chudnutie v sebaobrane
                        
                     
                                     
                        
                            Recepty na varenie
                        
                     
                                     
                        
                            Odpočinkové články
                        
                     
                                     
                        
                            Firefox
                        
                     
                                     
                        
                            Recenzie kníh
                        
                     
                                     
                        
                            Prikázania pre úspešný web
                        
                     
                                     
                        
                            O písaní blogov
                        
                     
                                     
                        
                            The Bat! - Program na e-maily
                        
                     
                                     
                        
                            Iné omrvinky
                        
                     
                                     
                        
                            Hudobná skrinka
                        
                     
                                     
                        
                            Životný štýl
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Dukanova diéta
                                     
                                                                             
                                            Turistika
                                     
                                                                             
                                            Energetická hodnota potravín
                                     
                                                                             
                                            Reality
                                     
                                                                             
                                            Kalkulačka BMI a obezity
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Web o opatrovaní a pomoci
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Glykemický index
                                     
                                                                             
                                            Kam na výlet
                                     
                                                                             
                                            Recepty na varenie
                                     
                                                                             
                                            Najlepšie probiotiká
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




