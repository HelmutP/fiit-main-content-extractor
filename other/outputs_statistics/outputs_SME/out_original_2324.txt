
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                História
                     
                 25 rokov od poslednej spartakiády 

        
            
                                    20.2.2010
            o
            14:21
                        |
            Karma článku:
                10.94
            |
            Prečítané 
            11275-krát
                    
         
     
         
             

                 
                    Spartakiáda bolo hromadné, verejné, telovýchovné vystúpenie telovýchovných jednôt, konkurujúce Sokolským zletom, ktoré  mali tradíciu ešte v prvorepublikovom Československu. Názov "Spartakiáda" bol odvodený od mena známeho rímskeho gladiátora, ktorý viedol veľkú vzburu otrokov v rokoch 73-71 pred Kr. Prvá celoštátna spartakiáda sa uskutočnila v roku 1955, bola vyvrcholením osláv 10. výročia oslobodenia Československa Červenou armádou.
                 

                 Potom sa zaviedla päťročná cyklickosť a preto sa ďalšie celoštátne spartakiádne vystúpenia konali v rokoch 1960, 1965, 1975, 1980, 1985.  Spartakiádu v roku 1970 režim radšej zrušil z obáv, aby sa nestala vyvrcholením revolučných pohybov z konca 60. rokov 20. storočia. Politické a revolučné zmeny nakoniec pochovali aj Československú spartakiádu 1990, ktorá sa síce uskutočnila, ale v oveľa menšom meradle, necvičilo sa vo všetkých kategóriách a absentovali aj slovenskí cvičenci.      Takže posledná skutočná spartakiáda sa uskutočnila pred 25 rokmi, a práve v takomto čase v roku 1985 už boli prípravy v plnom prúde, naplno sa cvičilo na školách, v obciach, v podnikoch, v telovýchovných jednotách a vo vojenských útvaroch. Uskutočňovali sa miestne, okrskové a okresné kolá spartakiády, pričom sa realizovali výbery cvičencov v jednotlivých kategóriách, ktorí potom mali istý postup na celoštátnu spartakiádu v Prahe, ktorá sa konala na Strahovskom štadióne. Podľa niektorých informácií bol tento štadión svojou rozlohou najväčším športoviskom na svete. Jeho kapacita bola 240 000 miest z toho 50 000 na sedenie, rozloha cvičiska bola 63 tisíc m². Svoju súčasnú podobu získal v rokoch 1948 až 1975. Jeho sláva je však už dávno za zenitom.      Pre úspech Spartakiády 1985 boli rozhodujúce republikové a okresné cvičiteľské zrazy, na kvalite nácvikov bola zainteresovaná aj Československá televízia, ktorá vyrobila inštruktážny seriál „ Na značky!", vysielaný v celorepublikovom meradle a tak sa aj do domácností prinášali jednotlivé zostavy v jednotlivých kategóriách. Vybratí cvičenci potom smerovali do Prahy, kde  ich čakala prenesene aj doslovne ich značka, ktorých bolo na pieskovej ploche štadióna 13 824, a tvorili tak sieť z 96 radov a 144 stĺpcov.      Program hromadných telovýchovných vystúpení bol organizovaný  nasledovne:   I.Programové popoludnie (27.6. a 29.6. 1985)             Počet cvičencov       Zahájenie     648       Ženy I     13824       Rodičia a deti     4672       Starší žiaci     10368       Mladšie žiačky     10752       Armáda     13824       Dorastenky     13824       Vysoké školy     5184       Dorastenci     6912       Učňovská mládež     13824              II. Programové populudnie (28.6. a 30.6. 1985)             Počet cvičencov       Zväzarm     6144       Najmladší žiaci     5760       Mladší žiaci     10752       Staršie žiačky     13824       Ženy II     13824       Muži     8640       Dorastenky  a ženy     6912       Armáda     13824       Záver     16000          Slovami mnohých komunistických potentátov mala byť Československá spartakiáda 1985 oslavou 40. výročia  oslobodenia československej vlasti Sovietskou armádou a zároveň mala prispieť k rozvoju telesnej a brannej výchovy. Ďalej mala byť manifestáciou politickej, ekonomickej  a kultúrnej vyspelosti socialistického Československa, zároveň mala byť preukázaním úcty pracujúceho ľudu komunistickej strane a prejavom hrdosti na socialistickú vlasť. Či sa to straníckym štruktúram KSČ naozaj podarilo naplniť je určite otázne. Ja sám som sa tejto akcie nezúčastnil ani ako žiak v roku 1985, a organizovanému cvičeniu som sa vyhol aj ako študent gymnázia v školskom roku 1989/1990. Keďže spartakiáda v roku 1990 už nemala taký veľký rozmer, v pamäti mám hlavne šialenstvo okolo spartakiády z roku 1985. Pamätám si na celodenné vysielanie spartakiádnych zostáv z Prahy a vyhľadávanie známych tvárí v mase cvičiacich. Z môjho pohľadu to bolo na tom celom asi najzaujímavejšie a ináč to bola obyčajná niekoľkohodinová nuda. Televízia však nič iné nevysielala, takže možnosť prepnutia bola vopred vylúčená. Dnes s odstupom času, takéto akcie považujem až za obdivuhodné, nie preto, že by mi takéto podujatia chýbali, skôr je v centre môjho obdivu akýsi psychologický moment, čoho boli ľudia v socialistickom Československu schopní, ako boli ochotní obetovať svoj voľný čas v prospech režimom organizovaného cvičenia. S dnešným vnímaním slobody jednotlivca, ktoré je nám v podstate vlastné iba 20 rokov, je to ťažké čo i len predstaviť si. Aj keď niekedy mám pocit, že niektorým politickým stranám na Slovensku a ich sympatizantom a voličom, by oživenie tradície oslavných masových cvičení neprekážalo.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (43)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4081
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




