
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Foto Potočný
                                        &gt;
                Zleužitá štatistika
                     
                 AVVM - Agentúra pre vytváranie verejnej mienky, s.r.o. 

        
            
                                    13.5.2010
            o
            14:14
                        (upravené
                24.5.2010
                o
                17:33)
                        |
            Karma článku:
                5.61
            |
            Prečítané 
            2017-krát
                    
         
     
         
             

                 
                    Svojim prvým odhadom politických preferencii sa nedávno na trh uviedla nová agentúra AVVM s.r.o. Zdôrazňujem slovo odhad, lebo títo páni inžinieri majú k serióznemu prieskumu ešte ďalekú cestu. Oni preferencie podľa všetkého neskúmajú a aktuálny stav ich nezaujíma. Miesto toho zrejme pracujú na ich vývoji. To je, zdá sa, takto pred voľbami lepšie platené.
                 

                 
  
   V podobnom období merali politické nálady obyvateľstva aj tri na trhu známejšie agentúry Focus, MVK a Polis.  V štvrtom stĺpci tabuľky prikladám priemer výsledkov oných troch renomovaných agentúr, pre lepšiu orientáciu. Minule som spomínal, že matematicky korektné je, ak sú výsledky uvedené formou tzv. 95-percentného intervalu spoľahlivosti. Tieto sa žial v správach pre média neuvádzajú. Tak napríklad agentúra Focus na základe prieskumu vykonaného v druhý aprílový týždeň tvrdí, že stranu Smer by v apríli volilo viac ako 33,3 a menej ako 40,3 percent obyvateľstva. Týmto tvrdením si je agentúra Focus istá na 95 percent.   Všetky tri menované renomované agentúry sa tak zhodujú na tom, že šanca Smeru získať v apríli menej ako 31,5 percenta bola veľmi  malá. Menšia ako napríklad šanca, že pokrový hráč naslepo uhádne, akú kartu má  na ruke jeho súper. V prospech AVVM hovorí, že robil prieskum ako posledný a Smer má už od januára klesajúci  trend preferencii. Aj vďaka bujnejúcej kauze "pôžička Grékom" sa teoreticky už v  májových prieskumoch môžeme dočkať toho, že oné tri agentúry takýto  volebný výsledok Smeru tiež pripustia.       agentúra 1) Focus
 2) MVK
 3) Polis
 4)Priemer 5) AVVM
     v dňoch 7.-13. 4. 16.-23. 4. 24.-26.   4. 3 renom. 
 27.4.-6.5.     účasť/vzorka
 70,6%/1040
   75,2%/1022   62,8%/924
  
79,2%/1061
 respondentov   Smer-SD 36,8 35,1 36,2 36,0 31,4 (v  %)   SDKÚ-DS 13,4 11,7 13,8 13,0 16,1 (v %)   KDH 8,6 11,4 13,2 11,1 12,1 (v %)   SaS 11,5 11,6 9,2 10,8 9,4 (v %)   SNS 8,6 6,2 5,3 6,7 5,5 (v %)   ĽS-HZDS 5,4 5,2 4,0 4,9 4,7 (v %)   Most-Híd 5,1 5,1 6,2 5,5 6,2 (v %)   SMK 5,1 6,0 5,8 5,6 3,6 (v %)   SDĽ 1,2 1,8 1,3 1,4 2,4 (v %)   AZEN 0,2 0,4 0,5 0,4 3,1 (v  %)     Podozrivá je ale bezprecedentne vysoká volebná účasť, nameraná v AVVM. To samo o sebe signalizuje, že s ich záhadnou metodikou nebude všetko v poriadku.  Ak nahuckajú bezmála 80 percent ľudí, aby v ankete "imaginárne volili" nie veľmi to  musí kopírovať odpovede povedzme 65 percent tých opýtaných, čo pôjdu  voliť aj v skutočnosti.   Azda s tým môže súvisieť aj na prvý pohľad neuveriteľný 3-percentný zisk strany AZEN. Nové strany ako SDĽ a Nová Demokracia by mohli rozprávať, koľko to stojí  práce a peňazí, prehupnúť sa čo i len nad jedno percento. Záhradník s Martinákovou by zase vedeli rozprávať o tom, že oné peniaze ešte vôbec žiaden výsledok nezaručujú. A tu ľaľa našli zrazu anketári AVVM z tisícky náhodne oslovených až 25 respondentov, ktorí by volili stranu AZEN. Stranu, o kotrej pred zverejnením tohoto prieskumu toľko smrteľníkov možno ani nepočulo.   Skúste si to sami. Vžite sa do role anketára a zožente 25 kladných odpovedí na otázku: "Volili by ste stranu Aliancia Za Európu Národov, kde lídrom je toho času poslanec NR SR pán Urbáni?" Čo ako sugestívne je táto otázka postavená (čo v originálnom prieskume AVVM snáď nebola), kladnú odpoveď dostanete vari len ako recesiu. Pokiaľ by ste ovšem nezavítali do bardejovského domu dôchodcov, kde mala strana nedávno predvolebný míting. Tam by ste našli hádam aj päť potenciálnych voličov. Vo Vranove nad Topľou možno ďalších päť. No a babka k babke, bude mať agentúra správne výsledky "výskumu" pre sponzora. No a ten hybaj s nimi do médii a na svoju webstránku.   Nuž tak...   Buďme ostražití a odlišujme agentúry zaoberajúce sa skúmaním verejnej mienky od tých, čo sa ju snažia vyvíjať, formovať či deformovať.       Áno ale čo nízke preferencie KDH od Focusu? (doplnené o 19:19)   Jeden zo vzácnych čitateľov napísal veľmi dobrú pripomienku: Výskum AVVM je podivný, ale nie je korektné, že ste nespomenuli ďalšie podozrivé výsledky - KDH: podľa Polisu má 13,2 zato podľa Focusu 8,6. To je rozdiel  4,6 percenta a takýto rozdiel je oproti rozdielu pri strane Smer o to  neobvyklejší, že sa jedná o malú nameranú hodnotu. 
   Absolútne súhlasím a pôvodne som aj mal v článku odstavec, ktorý sa tejto anomálii venoval. Neskôr som ho však vymazal, v záujme väčšej stručnosti a kompaktnosti článku. Môj laický pohľad je nasledovný. Preferencie KDH+SDKÚ+SaS treba vnímať ako prepojené nádoby (podobne ako SMK+Most). V tomto kontexte je dôležité si všimnúť, že Focus, MVK a Polis im v súčte pripisujú 33,5, 34,7 a 36,2 percent. To už sú rozdiely pohybujúce sa pod hranou štastickej významnosti. Preto som náchylný uveriť, že ich reprezentatívnosť vzorky a metodika je dostatočná na relatívne presné kvantifikovanie pravicového voliča v prvom priblížení. U niektorej z týchto agentúr však už nie dosť presná na to, aby zistila v akom pomere sa pravicový volič do oných troch nádob rozleje. Ktorá z agentúr odhaduje tento pomer najlepšie sa dozvieme asi až v deň volieb. Agentúra AVVM odhaduje tento súčet na 37,6 percent, čo je opäť v rozpore s agentúrami Focus a MVK, vzhľadom na to, že metodiku by mali mať s nimi podobnú. Respondent si vyberá z ponuky kandidujúcich strán. Jedine agentúra Polis má otvorenú otázku.       Ten istý a stále rovnako vzácny čitateľ pripísal aj tento postreh: Inak osobne si myslím, že  daná chyba môže byť spôsobená aj neúmyselne, napríklad takáto divoká  teória: AZEN je prvá v abecede - a ak rozdávali lístky so stranami v  abecednom poradí, časť respondentov si mohla vybrať v náhlosti (alebo z  recesie) prvý v poradí. Toto chovanie môže byť vo volebnách  miestnostiach úplne iné, či už kvôli inému poradiu (podľa volebného  čísla) ale aj kvôli inému myšlienkovému rozpoloženiu voliča vo volebnej  miestnosti oproti prieskumu.
   
Osobne s tým súhlasím, len trošičku inak. Viem si predstaviť, že istá časť respondentov by vďaka neprofesionálnemu prístupu anketára zvolila AZEN, SDKÚ či inú stranu namiesto odpovede "neviem"  resp. "nechcem voliť". Rovnako dobre si viem predstaviť, že anketári naozaj len náhodou narazili na hlúčik ľudí vracajúcich sa z mítingu AZEN. Či úmyselne alebo nechtiac, agentúra AVVM nastavila v konečnom dôsledku našej spoločnosti krivé zrkadlo. Teraz už je len na pánoch inžinieroch, ako sa s tým budú vedieť vysporiadať. A ešte na pánoch novinároch, čo to celé sprostredkovali.
   Nuž tak...  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Keď slovák Človeku je vlkom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            HC Slovan - skrátená Bratislava
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Ako si politici z novinárov vlastných kot-kódov spravili
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Zlodejova Republika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Kauza hala nezaháľa, a výsledky?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Foto Potočný
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Foto Potočný
            
         
        martinpotocny.blog.sme.sk (rss)
         
                                     
     
        Šport môžem vo všetkých jeho prevedeniach a vo všetkých jeho podobách, aj literatúru a dobrú hudbu. Rád diskutujem pri dobrom pive o komplexnosti histórie, o zakrivení priestoru a o tom, že jazyky a dialekty tvoria spojité zobrazenie definované na povrchu geoidu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Črevo slepô zapálenô
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Mýty bez legendy
                        
                     
                                     
                        
                            I taká je politika
                        
                     
                                     
                        
                            Sever proti Juhu
                        
                     
                                     
                        
                            Zleužitá štatistika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Problém hokeja je naozaj široký... (.týždeň - máj 2008)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jana Kamencayová (žvásty plné geniá..)
                                     
                                                                             
                                            Jozef Kuric (ruka čo nezoslabne)
                                     
                                                                             
                                            Mika Polohová (o nenápadných ľuďoch)
                                     
                                                                             
                                            Helena Sedlárová (myšlienkou fascino..)
                                     
                                                                             
                                            Michal Kravčík (lúska kvapky vody)
                                     
                                                                             
                                            Samuel Marec (o ničom a predsa niečom)
                                     
                                                                             
                                            Petra Jamrichová (tuctový človek)
                                     
                                                                             
                                            Viera Mikulská (prevažne nevážne)
                                     
                                                                             
                                            Andrea Fecková (usporiadaná pestrosť)
                                     
                                                                             
                                            Ján Babarík (aj s Berkym Jariabkom)
                                     
                                                                             
                                            Dominika Handzušová (hokejoumelec)
                                     
                                                                             
                                            Ľubo Randjak (nebojí sa pravdy)
                                     
                                                                             
                                            Lukáš Cabala (vnímavý týpek z Bošáce)
                                     
                                                                             
                                            Ivana Pajanková (hospic - z prvej ruky)
                                     
                                                                             
                                            Drogy (Vlado Schwandtner)
                                     
                                                                             
                                            Melisa (začína žiť odznova)
                                     
                                                                             
                                            Boris Kačáni (boľševikom na večné časy)
                                     
                                                                             
                                            Janette csC. (celkom slušná Cigánka)
                                     
                                                                             
                                            Martin Vlachynský (tuzemský filozofix)
                                     
                                                                             
                                            Jan K. Myšľanov (Zlé správy počkali...)
                                     
                                                                             
                                            Viliam Búr (Programátor, aktivista)
                                     
                                                                             
                                            Chen Lidka Liang (Čínske dievča)
                                     
                                                                             
                                            lenka jíleková (veci, ktoré žije)
                                     
                                                                             
                                            Richard Sulík (Fachman, Občan)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




