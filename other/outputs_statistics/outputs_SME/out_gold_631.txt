
 Bolesť.

Takto napísané to vyzerá skoro banálne a neškodne. Pár písmeniek. Viac nič. Keď príde zmení všetko. Niekomu možno na pár hodín, niekomu na pár dní, týždňov, mesiacov. Niekomu bude spoločníčkou na celý život.
Kým je malá, dá sa pomôcť. Lieky ju odoženú a my na ňu rýchlo zabúdame.
Väčšia, už pýta podstatne viac. Viac liekov, utrpenia, sĺz , prebdetých nocí. Dotýka sa nielen tela, ale naše myšlienky pomaly smerujú iba k nej. Hľadáme možné aj nemožné spôsoby ako sa jej zbaviť.
 
Ľudia od nepamäti trpeli. Nemali také možnosti ako my teraz a ani lieky, ktoré dnes účinkujú za pár minút.
Pokrok nám dal lieky, utišujúce injekcie a terapie ktoré pomáhajú. 
Ale bolesť tu stále je. Taká istá ako kedysi. Nemenná. Keby jej nebolo, tiež  by to nebolo dobré. Nevšimli by sme si, že sa v našom tele deje niečo nedobré.
Viem, že je zbytočné hľadať vinníka, prečo musia trpieť ľudia, čo si to najmenej zaslúžia.
Prečo musia trpieť malé deti a následne ich rodičia, lebo im nedokážu pomôcť.
Bolesť tu bola a aj ostane. Na tie telesné sa snáď podarí vymyslieť ešte lepšie lieky.
Na bolesť srdca vraj nevymyslí nikto nič, aj keď asi na tú jedinú by bol liek, ktorý má v sebe snáď každý.
Chorý človek, človek ktorý trpí potrebuje nielen analgetiká, ale hlavne ten pocit, že na to nie je sám. Že sú pri ňom ľudia. 
Nechcú súcit, ten nepomáha. Pomáha obyčajný záujem.

Dnešná doba je zvláštna. Chceme byť úspešní, uznávaní, zdraví. Mohlo by sa zdať, že máme všetko. Niekedy stačí chvíľa, okamih a všetko sa môže zmeniť. Môžeme si peniazmi priplatiť lepšiu starostlivosť, lieky, ale ak v bolesti ostaneme sami, vtedy asi zažijeme tú najväčšiu.
Možno nám naše bolesti vravia aj to, že hlavne nemáme zabúdať. Nie len na svoje telo, ale na trápenia tých druhých. 
Byť v správnu chvíľu na správnom mieste. Človek pri a pre človeka.


 
