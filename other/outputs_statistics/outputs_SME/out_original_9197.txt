
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Olga Pietruchová
                                        &gt;
                Cirkev a náboženstvo
                     
                 Môže sa cirkev vyjadrovať k politike? 

        
            
                                    19.6.2010
            o
            14:52
                        (upravené
                19.6.2010
                o
                15:38)
                        |
            Karma článku:
                10.33
            |
            Prečítané 
            2448-krát
                    
         
     
         
             

                 
                    Sme prinielso dnes článok o vyjadrovaní sa a zasahovaní cirkvi do politiky, kde sú i moje skrátené vyjadrenia. V  nasledujúcom texte ich prinášam v plnom znení.
                 

                      Mala by sa cirkev vyjadrovať k politike? Ak áno, prečo a akou formou? Kde je hranica, za ktorú by už nemala ísť?   Myslím, že cirkev má právo ako ktokoľvek v liberálnej demokracii zasahovať do verejnej diskusie a vyjadrovať sa aj k politike. Deje sa to aj v iných tradične katolíckych krajinách ako Španielsko, Poľsko, Litva a pod. Na druhej strane treba povedať, že vo vyspelých a skutočne sekulárnych demokraciách je prílišné angažovanie sa cirkvi považované za prinajmenšom neštandardné. Demokratická spoločnosť by mala byť schopná udržať verejnú diskusiu v hraniciach rešpektujúcich slobodu slova ale i vierovyznania všetkých ľudí.   Za neprípustné považujem, keď cirkevná hierarchia vytvára nátlak na vládu a mimo štandardných legislatívnych procesov ovplyvňuje prijímanie zákonov a materiálov. Stalo sa to v prípade Národného programu reprodukčného zdravia, ktorý prešiel štandardným legislatívnym procesom a vláda o ňom začala rokovať. Spočiatku ho jasne podporoval aj premiér, ale nakoniec sa na spoločnom obede s biskupmi dohodol na stiahnutí programu. Podobne iné návrhy z dielne ministerstva zdravotníctva ako Národný program starostlivosti o ženu, bezpečné materstvo a reprodukčné zdravie alebo predložená novela Zákona o zdravotnej starostlivosti upravujúca schválený poslanecký návrh k interrupciám boli predložené na pripomienkovanie a následne na tlak cirkvi stiahnuté.       Cirkev tým, že znemožní prijatie spomínaných opatrení, zasahuje do života a slobody tej časti obyvateľstva, ktorá sa nehlási ku katolíckej cirkvi alebo je bez vyznania - a to je na Slovensku druhá najväčšia skupina ľudí. Argumentácia o zastupovaní 80% obyvateľstva, ktorú KBS rada používa, je nezmysel;  väčšina veriacich sa s rigoróznym učením v oblasti sexuality a ľudských práv nestotožňuje.   Spomínané ovplyvňovanie politiky prekračuje nielen hranice transparentných demokratických procesov, ale aj ústavy, podľa ktorej sa Slovensko neviaže na žiadnu ideológiu ani náboženstvo. Samozrejme,  je to aj chyba politikov, ktorí cirkvi na túto hru naskočia.  Nie náhodou ostala požiadavka novembra o odluke cirkvi a štátu nenaplnená. Katolíckej cirkvi táto situácia veľmi dobre vyhovuje a akákoľvek diskusia na tému je umlčaná hneď v začiatkoch.       Cirkev sa relatívne otvorene vyjadrovala v rámci prezidentskej kampani, keď kritizovala Ivetu Radičovú. Boli takto prezentované stanoviská v poriadku, alebo už boli za hranicou únosnosti?   Niektorí predstavitelia cirkvi sa v tomto prípade znížili na úroveň bulváru, keď neprimeraným spôsobom zaťahovali jej súkromný život do verejnej diskusie. Podotýkam, že tak nekonali v žiadnom inom prípade a nevyjadrovali sa napr. k ikstému manželstvu Jána Slotu, ktorý sa tak rád oháňa Bohom a národom, ani k iným politikom, ktorí sústavne  porušujú nejedno prikázanie Desatora. V prípade Radičovej šlo nielen o obsah, ale i formu; označenie za konkubínu je pejoratívne a všeobecné chápané za označenie ženy, priživujúcej sa na úspešnom mužovi. V súčasnosti sa možno hodí na bulvárové celebrity po bohu slovenských novozbohatlíkov, ale určite nie na sebavedomú nezávislú ženu a úspešnú političku.   Rovnako vyjadrenie biskupa Baláža opierajúce sa o  paralelu s Hitlerom bolo vzhľadom na nedávnu minulosť a klérofašistický Slovenský štát prinajmenšom nemiestne.   Žijeme v liberálnej demokracii. Sme súčasťou Európskej únie, ktorá sa veľmi dobre poučila z minulosti a preto kladie na rešpektovanie univerzálnych ľudských práv veľký dôraz. Katolícka cirkev u nás však akoby zaspala a vývoj a nepochopila, že nemá univerzálny monopol na morálku a etiku. Navyše, nespočetné škandály so sexuálnym zneužívaním mladistvých v radoch cirkvi a politika zahmlievania spôsobili, že cirkev stratila v mnohých krajinách morálne právo kázať o mravnosti a čistote. Je len typické, že u nás sa namiesto otvorenej diskusie zo strany cirkvi táto vec bagatelizuje a namiesto povestného brvna vo vlastnom oku vyťahujú smietky u iných.       Môžu postoje cirkvi k voľbám ovplyvniť samotných voličov? Majú na nich podľa teba pastierske listy, články kňazov na blogoch, resp. vyjadrenia duchovných na sociálnych sieťach typu facebook a pod. reálny vplyv?   Zrejme sa v autoritárnej inštitúcii ako je cirkev očakáva, že „pastier povedie svoje ovečky".  Reálny vplyv tieto vyjadrenia určite nemajú a ide skôr o presviedčanie presvedčených.  O tom najlepšie vypovedá fakt, že hoci sa v poslednom sčítaní ľudu 69% populácie prihlásilo ku katolíckemu vierovyznaniu, KDH má dlhodobo volebné výsledky okolo 8% a ešte horšie dopadol preferovaný prezidentský kandidát František Mikloško. Samozrejme, inou motiváciou môže byť budovanie si kariéry samotných duchovných.   Progresívni veriaci sa riadia inými prioritami. Na tom nezmenia nič ani výzvy a pastierske listy, najmä keď sa v nich neustále objavujú len interrupcie a homosexualita. Skôr si myslím, že takéto spolitizovanie viery vyvoláva u ľudí, ktorí považujú vieru za intímnu spirituálnu záležitosť, negatívny dojem. Slovensko je napriek všetkým snahám modernou krajinou a ľudia sú väčšinou oveľa tolerantnejší a liberálnejší, ako si politici myslia a ako sa im to cirkev snaží nahovoriť.           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (349)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová ideológia? Nie, iba spravodlivosť a fair-play pre naše dcéry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová rovnosť ako kultúra života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Odpoveď na otvorený list Gabriele Kuby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Ženskosť a mužskosť alebo komu slúžia stereotypy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Od Harryho Pottera ku gender ideológii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Olga Pietruchová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Olga Pietruchová
            
         
        pietruchova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Viac mojich článkov nájdete na www.olga.sk


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    180
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7252
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Práva žien a rodová ne/rovnosť
                        
                     
                                     
                        
                            Ateizmus a veda
                        
                     
                                     
                        
                            Politika a spoločnosť
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Fejtóny:-)
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Sexualita, reprodukcia a práva
                        
                     
                                     
                        
                            The God Delusion
                        
                     
                                     
                        
                            Zelené témy
                        
                     
                                     
                        
                            Cirkev a náboženstvo
                        
                     
                                     
                        
                            O čom sa ne/hovorí...
                        
                     
                                     
                        
                            Jej príbeh
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ženy v politike, politika pre ženy
                                     
                                                                             
                                            Môj rozhovor v Sme Víkend
                                     
                                                                             
                                            Na výsluchu: Hierarchia katolíckej cirkvi mi leží v žalúdku
                                     
                                                                             
                                            Môj rozhovor na www.sme.sk
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Steven Pinker: The Blank Slate
                                     
                                                                             
                                            Jared Diamond: Collapse
                                     
                                                                             
                                            Martin Uhlír: Jak sme se stali lidmi
                                     
                                                                             
                                            Lynn Abrams: Zrození moderní ženy
                                     
                                                                             
                                            Daniel Dennett: Breaking the Spell
                                     
                                                                             
                                            Richard Dawkins: God Delusion
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moja osobná stránka
                                     
                                                                             
                                            Zošity humanistov
                                     
                                                                             
                                            Spoločnosť pre plánované rodičovstvo
                                     
                                                                             
                                            Edge -  The Third Culture
                                     
                                                                             
                                            JeToTak
                                     
                                                                             
                                            Changenet
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




