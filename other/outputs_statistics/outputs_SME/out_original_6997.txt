
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            kristína čahojová
                                        &gt;
                Články
                     
                 Couchsurfing....o umení cestovať 

        
            
                                    31.5.2010
            o
            18:12
                        (upravené
                31.5.2010
                o
                21:15)
                        |
            Karma článku:
                10.82
            |
            Prečítané 
            2071-krát
                    
         
     
         
             

                 
                    Je Paríž neprekonateľne ďaleko? Nedôverujete moslimom v Istanbule? Po Japonsky rozprávate? Hnusia sa vám turistické hotely?  Ja žiadnu prekážku nemám. Ja som totiž couchsurfer:) Vítajte u mňa! Ja mám pre vás voľný gauč a ponúkam lepší servis ako hostel. Cestovať je umenie a má patriť všetkým....preto couchsurfing.
                 

                 
  
   Pred skoro dvoma rokmi ma inšpiroval článok s nadpisom: "Spím u neznámych ľudí". Len tak z haluze som sa zaregistrovala na portál www.couchsurfing.org a musím priznať celkom značne to zmenilo môj pohľad na svet.   Od tej doby sa u mňa premlelo asi tak na 20 národností a ľudia z polky sveta. Minule u nás spal jeden Japonec (strašne fajn chalan Keith, ktorý vymýšľa veľmi známe počítačové hry psst:). Bol prvý krát v Európe a tak sa čudoval skoro všetkému. Jeho skúsenosť s couchsurfingom bola veľmi krásna. Mi vravel:   "Som si povedal, že navštívim môjho priateľa v Berlíne. Je tam už skoro rok. Robí maliara a pouličného umelca. Prvý deň, čo v Berlíne na ulici vystupoval a nevedel reč ani nemal ubytovanie, sa stretol s iným umelcom. Bol pozvaný k nemu domov na pohárik. Lenže v tú noc, keď k nemu prišiel, tak jeho nového priateľa tam prišla zatknúť polícia. Odvtedy mu môj kamoš stráži byt a robí couchsurfing. Tak teraz snáď budem správny hosť." dopovedal Keith.)   Je to síce trocha extrémny úvod, ale v podstate príklad.   Couchsurfing je niečo ako internetová komunita ľudí, ktorí radi cestujú. Jeho filozofia je úplne jednoduchá: každý má doma voľný gauč. A kto by radšej nezažíval atmosféru domáceho prostredia s domácimi ľuďmi?       Ja som odvtedy zažila s cestovaním omnoho viacej vecí. Spala som u guvernéra Granady, v olympijskej dedine v Barcelone, v Dubline u chalana, ktorý ma ani nevidel, len mi poslal jeho kľúče a na mnoho a mnoho iných miestach. Spoločné tomu bolo vždy len kopec srandy a zážitky, ktoré sa nedajú kúpiť v žiadnej cestovke. Couchsurfer ťa totiž zatiahne do miestneho života, vezme ťa medzi svojich priateľov, na miesta kam turisti nechodia. Navyše aký cvok toto môže robiť zadarmo? Preto sa netreba báť. Skoro vždy ide o "prvotriednu ľudskú kvalitu", alebo aspoň zážitok.      Mýty o couchsurfingu   Je to nebezpečné   Nie aj áno. Každý má na stránke svoj profil s fotkami, informáciami, miestami kde s kým cestoval a hodnotenie od svojich návštevníkov (čiže odporúčania). Ja teda neubytovávam chlapov, ktorí nemajú od nikoho žiadnu referenciu a posielajú hromadné správy. Ale to je vec každého vkusu. Keď niekto raz systém zneužije už nemá šancu proste participovať ďalej.   Cestujú len ľudia, ktorí nemajú na hotel   To je tiež úplná blbosť. Cestujú a hostia práve ľudia, ktorých to obohacuje. Navyše couchsurfing nie je podmienený prespaním. Ja sa iba tak stretávam s cestovateľmi, ktorí ma pozvú na "kávu". Je to sloboda, nikto nie je ničím viazaný.   Takto sa nedá cestovať v skupine   Áno, je to pravda je to ťažšie. Ale ja som spala s 5 ostatnými dievčatami 2 týždne stále u niekoho iného, takže výnimky sa nájdu:) Najlepšia je asi dvojica. To je aj väčšia zábava.   Potom stále musím niekoho ubytovávať   Nie. Keď mám chuť, tak si nastavím profil, že mám voľný gauč. Vtedy ti možno niekto napíše a keď sa mi nechce nikoho ubytovávať tak si to jednoducho na profile zruším.   Ja nemám čas sa celý deň niekomu venovať   Tak to vôbec nie. Na to ani väčšinou nie je čas. Proste cestujete a spoznávate, máte vlastný program. Nedá sa od niekoho očakávať, že vám teraz niečo vymyslí.   Pokiaľ nemôžem nikoho ubytovať, nemôžem cestovať   Nie couchsurferovi stačí venovať aj voľný čas. Dohodnúť sa s ním, že mu niečo v svojom mieste ukážeš, vezmeš na party. Nie každý býva sám:)   Je to proste paráda! Couchsurferi sa nachádzajú v 69 000 miestach, rozprávajú 302 jazykmi a od roku 2004 sa uskutočnilo 1,7 milióna "návštev" a utvorilo rovnako priateľstiev. Tak trocha droga nie?.) Všade ťa niekto čaká, necestuješ anonymne. Netreba na to skoro žiadnu väčšiu snahu.  Ja couchsurfingu vďačím za najnevšednejšie zážitky môjho života. Veď svet sme my ľudia, my tvoríme kultúru a atmosféru, preto do spoznávania patríme!    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        kristína čahojová 
                                        
                                            &amp;#161;Ďakujem, že neplatíte!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        kristína čahojová 
                                        
                                            Mladá podstatná skupina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        kristína čahojová 
                                        
                                            O Vietnamskom pohrebe, peniazoch a veselom básnikovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        kristína čahojová 
                                        
                                            O stratených 1000 EUR, dobrých ľuďoch a Taiwancoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        kristína čahojová 
                                        
                                            Rozprávka o tisíc a jednej noci
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: kristína čahojová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                kristína čahojová
            
         
        cahojova.blog.sme.sk (rss)
         
                                     
     
        nechcem žiť normálne a zatiaľ mi to našťastie nehrozí....,)

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3314
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            poviedky
                        
                     
                                     
                        
                            ako prežiť chaos
                        
                     
                                     
                        
                            odkazy
                        
                     
                                     
                        
                            Články
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Myšlienky, ktoré by mali znieť Slovenskom
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Raz si kúpim motorku a pôjdem vlastnú Vuelta del America Sur
                                     
                                                                             
                                            Umenie cestovať, je meniť svet k lepšiemu
                                     
                                                                             
                                            Zaručená zábava namiesto stresu
                                     
                                                                             
                                            Naša Bratislava, pre zmenu k lepšiemu v mojom rodnom meste
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ohlásenie vstupu do prvej línie
                     
                                                         
                       Ako použiť peniaze na volebnú kampaň zmysluplne
                     
                                                         
                       Odporúčam ignorovať zákaz jazdy po nábreží v Bratislave
                     
                                                         
                       Viete, čo majú spoločné Igor Rattaj (J&amp;T) a Miroslav Trnka (Eset)?
                     
                                                         
                       Keď televízia tvrdí, že 2+3 je 7
                     
                                                         
                       Úsvit a kultúrne zákulisia vedy o sexe. Prípad masturbácie.
                     
                                                         
                       Prečo primátor masakruje bratislavské lesy?
                     
                                                         
                       Prečo šváby nemajú kolesá
                     
                                                         
                       Manželstvo ako šťastie
                     
                                                         
                       Na Slovensku žiť, radšej sám sa utopiť!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




