
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Veci súkromné
                     
                 Popradská železničná stanica, nedeľa, 14:43 

        
            
                                    6.6.2010
            o
            20:23
                        (upravené
                6.6.2010
                o
                20:38)
                        |
            Karma článku:
                15.41
            |
            Prečítané 
            4240-krát
                    
         
     
         
             

                 
                    V nedeľu vedú všetky cesty do Bratislavy. Cesty na Slovensku vedú vôbec do Bratislavy, okrem tých, ktoré vedú do zahraničia.
                 

                 Tak všetci sedíme na druhom poschodí popradskej železničnej stanice, každý so svojou batožinou, pripravený vydať sa na tú svoju cestu do Bratislavy, lenže cesta sa ešte ani nezačala a vlak už 30 minút mešká. Tridsať minút nie je nič oproti pol metru vody v dome, beriem to tak, idem na záchod a cigaretu.   Železniční policajti postávajú v skupinke na prvom poschodí, nič sa im nechce a ani nič nerobia, je predsa nedeľa. Je navyše aj horúco a ani mne sa nič nechce. Vraciam sa naspäť na druhé poschodie, opäť som členom anonymného davu Bratislava. Spoznáte nás podľa toho, že máme kúpené veľké fľaše minerálky a ešte podľa toho, že si jeden druhého v tom dave nevšímame. Sme každý sám za seba. Dav Bratislava ako každú nedeľu.   Po stanici sa prechádzajú skupinky mladých. Neviem prečo to robia, možno nevedia, čo iné by v nedeľu robili, tak robia práve toto. Ako každú nedeľu. Prejdú sa po nástupišti, potom stoja pri schodoch a sledujú prichádzajúcich ľudí. Presne rovnaké skupinky som vídaval, keď som ešte bol na strednej. Spoznali ste ich podľa toho, že hodiny pozerali na eskalátory. Eskalátory boli vždy štyri, vtedy fungovali aspoň dva a dnes žiaden. Zato je stanica skoro úplne zrekonštruovaná.   Hlásia nové meškanie. Štyridsať. Pán vedľa mňa niekomu volá, že to lietadlo asi nestíha. Rozmýšľam, čo mám robiť ja. Ostatní nerobia nič. To je tá skvelá vec na slúchadlách. Keď ich máte, nemusíte robiť nič a môžete si žiť vo vlastnom svete. Veď vravím, spoznáte nás podľa toho, že si jeden druhého nevšímame. Je nedeľa a vážne horúco, tak nakoniec ani ja nerobím nič.   Z lavičky vedľa mňa vstáva Cigán. Mladý, špinavý. Keď som bol na tej strednej, stanica ich bola plná. Niektoré veci sa proste nemenia. Stojí, kolíše sa, pozerá niekam, smerom do nikam. Pri tvári má priloženú handričku. Potom sa pomaly, veľmi pomaly poberie niekam preč.   Je nedeľa, je horúco, všetci ideme do Bratislavy a vlak mešká. V dave menom Bratislava každý sám za seba plus jeden fetujúci Cigán, lebo niektoré veci sa nemenia. A policajti na prvom poschodí.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




