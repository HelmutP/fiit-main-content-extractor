
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Balogova
                                        &gt;
                Pocity
                     
                 Horšie časy 

        
            
                                    27.5.2010
            o
            20:33
                        (upravené
                27.5.2010
                o
                20:39)
                        |
            Karma článku:
                2.69
            |
            Prečítané 
            403-krát
                    
         
     
         
             

                 
                    Už sú tu?
                 

                     Pod vankúšom som našla sladký cukrík odložený na horšie časy. Časy, keď ja sa trasiem, cítim studený pot na čele, krku, hrudi..a všetci ostatní prítomní v mojej izbe, vo vedľajšej izbe, v celej štvor-poschodovej bytovke spokojne a nerušene spia ďalej. Zjedla som ho, nepozerajúc na horšie časy, možno už ich nestihnem, alebo oni nestihnú mňa. Pri ubežaných dňoch posledného mesiaca by som sa ani nečudovala. Uniká mi veľa vecí pomedzi prsty.   Ako dieťa vyplazím jazyk na S. a ten sa zhrozí, aj keď ja sa veselo usmievam ďalej. Cukrík, ktorý som potajomky zjedla, mi sfarbil jazyk na modro, a S. vyhlásil, že mi odumiera. Očíme na seba oči a cítime tam radosť. Radosť po posledných dňoch, ktoré nám tiež utiekli dolu vodou so záplavami, kvôli učeniu sa o nás, kvôli práci pre nás.   V špajzičke objavím zabudnutú detskú výživu, zatvorenú tak silno, aby prežila aj nekonečnú cestu z východu na západ, alebo jednoducho, aj horšie časy. S. sa ju snaží otvoriť, a musím sa smiať, že môj otec je predsa len silnejší, na čo S. reaguje, že každý chlap je silnejší ako on. Keď cítim pravú dužinu jabĺk, predstavím si, ako sme ich oberali, keď otec bol na strome a my s mamou pod ním a čakali, že tie najkrajšie nám zhodí do dlaní. Odložili sme ich poctivo do fliaš, s imaginárnym listom, že boli to dobré časy. Občas mi chýbajú, ako o rok tak budem hovoriť novým spolubývajúcim o tých starých, že nám bolo dobre, a že mi chýbajú. Chýbajú mi už teraz, ešte len pri myšlienke na to.   Nedočkavo pri tom hľadím na mail a čakám ten správny, so správnym písmenkom. No chodí len ponuka na bazénovú párty v podobe výpredajových grilov z minulého roka a bazénov pre rôzny počet členov. Možno ten istý mail dostal aj pán profesor a ponuka ho zaujala, výsledok sa dozviem inokedy. Pre istotu nechávam medzi prijatou poštou, jeden nikdy nevie kedy ho napadne grilovať.   Dnes sme so Z. usúdili, že prišla veľmi divná doba, lebo: človek si za jeden plat nekúpi nič, študenti namiesto radosti riešia svoje existenčné problémy, ženy sú  náročnejšie na mužov priamoúmerne s ich vekom (len správnych mužov ubúda). Asi naozaj odzvonilo dobrým časom, a na rad prišli tie horšie. Schovávam ďalší cukrík pod vankúš.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Balogova 
                                        
                                            Pohoda, jahoda a čerešne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Balogova 
                                        
                                            Napísať blog
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Balogova 
                                        
                                            Posledné dva týždne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Balogova 
                                        
                                            Sila trojuholníkov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Balogova 
                                        
                                            Čo potrebujem(e)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Balogova
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Balogova
            
         
        martinabalogova.blog.sme.sk (rss)
         
                                     
     
        Ľudí nudí byť deťmi a tak sa ponáhľajú, aby dospeli, a keď sú dospelí, zas túžia byť deťmi. Strácajú zdravie, aby zarobili peniaze, a potom utrácajú peniaze za to, aby si dali do poriadku svoje zdravie. Natoľko sa strachujú o svoju budúcnosť, že zabúdajú na prítomnosť, a tak vlastne nežijú ani pre prítomnosť, ani pre budúcnosť. Žijú, akoby nikdy nemali umrieť, a umierajú, akoby nikdy nežili...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    65
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    748
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Pocity
                        
                     
                                     
                        
                            Spomienka
                        
                     
                                     
                        
                            Otázky a odpovede
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Poezia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Alenka
                                     
                                                                             
                                            Sona
                                     
                                                                             
                                            ujo doktor..
                                     
                                                                             
                                            drtic..
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




