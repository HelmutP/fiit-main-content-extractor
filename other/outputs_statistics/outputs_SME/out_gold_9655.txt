

 Being shocked by the realization that they have been actually FLIRTING caused that I missed the following couple of short sentences they exchanged. But the shock was only going to come after I revived and realized where the dialogue is leading to! 
 He teased her: "So? Are you coming?" 
 She giggled: "Oh, I could, but it would be DEVASTATING  for you!" 
 They both laughed with sparks in their eyes and moved out of the door finally so I could continue on my way to the seminar although I felt the way out to the fresh air could do me a better favor. 
 Since then I feel I really missed out some of the lessons of life........ or is it all just an experience that comes with years? Well, then I cant wait till my retirement. 
 Ammerdown, UK, 20.6.2010 
 (note: the average age of Cloud of Unknowing members seemed to be around 75) 

