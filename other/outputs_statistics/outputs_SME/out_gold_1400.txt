

 BRATISLAVA - Slováka, ktorý slúžil vo francúzskej cudzineckej légii, obvinili, pretože na to nemal povolenie od prezidenta. Polícia to zistila, keď vyšetrovala trestné činy 25-ročného muža z Galanty, ktorý je vo väzbe v Leopoldove za inú trestnú činnosť. 
 Vo francúzskej cudzineckej légii slúžil od marca do decembra minulého roku. O povolenie mal žiadať prezidenta cez územnú vojenskú správu a ministerstvo obrany. Nárok na to má každý, na koho sa vzťahuje branná povinnosť. 
 "O udelenie takéhoto povolenia nepožiadal," potvrdil hovorca prezidenta Marek Trubač. Nevedel povedať, či už niekedy takúto žiadosť prezident dostal. 
 Evidenciu o slovenských občanoch v cudzích ozbrojených silách si nevedie ani ministerstvo obrany. Ak by išlo o profesionálneho vojaka alebo vojaka základnej vojenskej služby, prípad by riešila vojenská polícia. "Zatiaľ taký prípad nemáme," povedal hovorca ministerstva obrany Zenon Mikle. 
 Francúzska cudzinecká légia je známa tým, že v jej radoch slúži množstvo ľudí z bývalého východného bloku, hovorí sa aj o Slovákoch. Na Slovensku za to nebol v poslednom čase stíhaný ani jeden. "Veľvyslanectvo Francúzskej republiky nie je informované o počte Slovákov, ktorí chcú vstúpiť do cudzineckej légie," napísala v stanovisku Natália Fošnárová z tlačového oddelenia francúzskeho veľvyslanectva. Tvrdí, že slovenskí občania, ktorí by chceli vstúpiť do légie, sú informovaní, že naša legislatíva to zakazuje. (rp) 
 Légia je veľkou pýchou francúzskej armády 
 Prvé jednotky cudzineckej légie Francúzsko začalo budovať v roku 1831 na zabezpečenie svojich kolónií. Dnes má cudzinecká légia asi 8000 mužov a je považovaná za jednu z najlepšie vycvičených armád sveta. 
 Základným predpokladom na uchádzačov, ktorí prichádzajú z celého sveta, je vek medzi 18 až 40 a výborný zdravotný stav. Prípustné nie sú ani viditeľné jazvy po operáciách. Dôvodom na vyradenie sú alergie, viditeľné kazy zubov či kožné choroby. Toleruje sa iba drobná očná chyba. 
 Náborové strediská sú otvorené 24 hodín denne, 7 dní v týždni. V minulosti sa veľmi nehľadelo na minulosť hlásiacich sa ľudí, légia sa tak stala útočiskom pre niektorých ľudí, čo utekali pred spravodlivosťou. 
 Prvé dni pozostávajú zo zdravotných, psychologických, inteligenčných a fyzických testov. Nasledujú pohovory nazývané gestapo. Po absolvovaní testov sa do záverečného výberu dostáva 30 zo 100 uchádzačov. Z nich sa definitívne vyberie polovica. Po podpísaní zmluvy už nie je možné z légie odísť skôr, ako kontrakt vyprší. Jediným riešením je dezercia, ktorá sa v prípade dolapenia prísne trestá. Veľký dôraz sa kladie na vzťahy v jednotkách. Fyzické riešenie sporov je neprípustné, trestá sa odchodom do civilu. V légii sa preto údajne šikanovanie takmer vôbec nevyskytuje. 
 Po troch rokoch služby má legionár právo požiadať o francúzske občianstvo. Žiadosti však krajina väčšinou vyhovie až po šiestich rokoch. (r) 
 Spracované podľa www.specnaz.cz 
 Čo hovoria zákony o službe v cudzom vojsku 
 * slovenský občan, ktorý bez povolenia slúži v cudzom vojsku, sa potrestá odňatím slobody na dva až osem rokov, 
 * väzením na päť až desať rokov sa potresce ten, kto sa skutku dopustí počas brannej pohotovosti štátu, 
 * občan môže slúžiť v ozbrojených silách iného štátu len so súhlasom prezidenta, 
 * žiadosť podáva občan prostredníctvom územnej vojenskej správy ministerstvu obrany. To ju po dohode s ministerstvami vnútra a zahraničných vecí predkladá prezidentovi, 
 * proti rozhodnutiu prezidenta sa nemožno odvolať. 

