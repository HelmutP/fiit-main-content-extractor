

 "Veď u teba je zdroj života." - Žalm 36:9a. 
 Vedci zaoberajúci sa vznikom života neprestávajú formulovať nové, viac prijateľné scenáre pre ´drámu´ o prvom vzniku života na Zemi. Ale ich ´scenáre´ nepôsobia presvedčivo. Ani pred niekoľkými rokmi v roku 1996 na Medzinárodnej konferencii o vzniku života neboli predložené žiadne riešenia. Práve naopak! Ako uvádza informácia časopisu Science, okolo 300 svetových vedcov ´zápasilo s hádankou, ako na začiatku vznikli molekuly DNA a RNA a ako sa vyvinuli na bunky schopné reprodukovať samy seba´. Nemecký biochemik Klaus Dose* z Inštitútu Mainz, konštatoval: ´Teraz sa všetky diskusie o hlavných myšlienkach aj o experimentoch v tejto oblasti končia buď patom či úprimným priznaním neznalosti´. Teraz malo porozmýšľajme: Na to, aby bolo možné študovať a dokonca začať vysvetľovať to, čo sa deje aj na molekulárnej úrovni v našich bunkách, bola potrebná inteligencia i pokročilé vzdelanie. Bolo by rozumné veriť, že tieto komplikované kroky sa odohrali bez riadenia, samovoľne a náhodou v nejakej ´prebiotickej polievke´? Na druhej strane verš od kráľa končí: 
 "Svetlom od teba (Jehovu) môžeme vidieť svetlo." - Žalm 36:9b. 
 Svetlo alebo zjavená pravda od nášho Stvoriteľa, Jehovu Boha, nám pomôže vidieť svetlo alebo pravdu o tejto veľkej ´hádanke života´! 
 *http://afterall.net/quotes/388 

