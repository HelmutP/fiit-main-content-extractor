
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniel Horecký
                                        &gt;
                Nezaradené
                     
                 Keď je biznis viac ako Fair-play 

        
            
                                    11.4.2010
            o
            10:24
                        |
            Karma článku:
                7.31
            |
            Prečítané 
            1416-krát
                    
         
     
         
             

                 
                    Akosi sme si už zvykli, že niekedy sa veci okolo nás nedejú s kostolným poriadkom. Na chyby poukazujeme, no tým naša snaha o nápravu končí .Je to tak v každej oblasti života, prečo by sa to teda nemalo diať aj v športe. Preto je o to zarážajúcejší fakt, že si niekto dovolí kritizovať niekoho za to, že si chybu sám priznal a urobil nápravu.
                 

                             Prebiehala 79. minúta futbalového zápasu České Budejovice - Příbram, keď domáci postupovali do útoku pozdĺž postrannej čiary. V snahe o zrýchlenie hry poslal domáci obranca dlhú prihrávku dopredu, ktorú zastavil hosťujúci Hušbauer evidentným tečom do autu. Rozhodcovia však teč nepostrehli, a tak dali vhadzovať hosťom. Hošbauer sa však zachoval čestne, upozornil rozhodcu že po jeho teči by mali vhadzovať domáci, a tak sa aj stalo. Nasledoval však aut, z ktorého sa vyvynula akcia znamenajúca gól domácich a ich výhru 2-1. Povedali by ste si, hráč urobil správnu vec, tešme sa že aj takí sa nájdu. ČMFS mu za čin udelil cenu Fair-Play. No nie všeko je tak idylické, ako sa zdá.               Tréner Karol Marko označil čin svojho hráča za naivný a pre tlač sa vyjadril, že po jeho rozhovore s hráčom už Hušbauer podobnú vec nikdy neurobí. Po vyprchaní zápasových emócií sa spoluhráči, fanúšikovia a aj vedenie klubu vyjadrovalo pochvalne k činu hráča, no tréner Marko akoby bol stále vo svojom svete plnom peňazí a zákernosti.               Pán Marko, ak povyšujete biznis a emócie nad ducha Fair-Play, pre mňa nie ste čestný človek, a nemáte čo hľadať na pozícii, ktorú práve zastávate. Priznať si totiž pravdu nemôže byť predsa hamba za žiadnych okolností, nech už ide o akýkoľvek biznis či situáciu. Je to šport, a v ňom má duch Fair-Play ešte silnejšie poslanie. Chcel by som počuť vaše vyjadrenie po tom, čo by sa podobná vec stala v prospech Vášho mužstva, alebo by z následnej akcie súpera nepadol gól. Myslím že odpoveď si každý domyslí aj sám... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Horecký 
                                        
                                            Volebný pat v ČR
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Horecký 
                                        
                                            Nie som ovca, nevystúpim
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Horecký 
                                        
                                            Keď ani vyše 100 rokov nie je tradícia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Horecký 
                                        
                                            Posilnenie verejnej dopravy na Moste SNP
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Horecký 
                                        
                                            Nastal čas na SSD? II. - výber
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniel Horecký
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniel Horecký
            
         
        horecky.blog.sme.sk (rss)
         
                                     
     
        Jedno osobné stretnutie povie o človeku tisíckrát viac ako tisíc ním napísaných slov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1012
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ori Brafman - Houpačka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď ani vyše 100 rokov nie je tradícia
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




