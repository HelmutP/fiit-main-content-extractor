

 Druhá najsilnejšia komerčná televízia TV Joj je vlastnená finančnou skupinou J&amp;T, čo z času na čas vytvára konflikty medzi tým, ako chce na verejnosti pôsobiť J&amp;T a tým, ako by o udalostiach chceli byť informovaní diváci Jojky. Pri občasnom pozeraní Joj sa mi zdalo, že televízia sa vyrovnáva so záujmami majiteľov vcelku dobre (napr. Počiatkova návšteva na jachte ľudí J&amp;T).   Čo mi najviac vadilo bolo, že redaktori tento svoj konflikt záujmov zamlčiavali a divákom férovo nepripomínali pri konkrétnych príspevkoch, že informujú o svojich majiteľoch či niektorých ich firmách. Takto to bolo aj pri kvázireklamnom príspevku o štarte sesterskej českej stanice Z1 v roku 2008. A Joj divákom podobne podľa archívu doteraz zamlčiava, že konzorcium Skytoll, operátor mýtneho systému u nás, je investičnou sestrou televízie.   Ešte horšie je, že toto prepojenie má zrejme vplyv na vysielanie televízie. Zo všetkých štyroch televízií bola Joj voči chybám zavedenia mýtneho systému jasne najmäkšia.  Joj je síce bulvár, čiže až tak ľahko sa so serióznejšími stanicami neporovnáva, ale protest dopravcov býva aj pre tento typ médií atraktívna téma, ako to ukázala aj naša bulvárna tlač (z protestov sú dobré obrázky, ide o relatívne menej vzdelaných ľudí - podobní sú aj konzumenti bulváru, ale téma bola aj o dopade na ceny základných tovarov).  Joj sa ale k téme postavila netradične rezervovane. Diváci sa po celý čas od štartu mýta nedozvedeli závažné zistenia nefunkčnosti: ako prudko niektorým prepravcom stúpli náklady po zavedení mýta, že to môže mať dopad na ceny bežných komodít, že niektoré autobusové linky už kvôli tomu zdvihli cestovné. Joj tiež nespomenula, že systém započítaval za rovnaké trasy dopravcom rôzne sumy, podobne ako nefungovala online kalkulačka výpočtu mýtneho. Naopak nadpriemerne Joj opakovala údaje operátora, koľko peňazí pre štát sa už na mýte vybralo, a vyzdvihovala problémy, ktoré blokovanie dopravy spôsobuje krajine.  Už po prvých 4 dňoch od zavedenia mýta hovorila TA3 o očividne nedobre fungujúcom systéme, Markíza zase o kamiónovej revolúcii či kritickej situácii na niektorých hraničných prechodoch, obe o zmätku v MHD v Košiciach. Joj v rovnakej dobe síce priniesla pár informácií o čakaní na hraniciach, ale na avizovaný protest dopravcov sa pozerala dosť ironicky: 
 Zdá sa, že my Slováci na rozdiel od iných národov štrajkovať nevieme. Dopravcovia síce v súvislosti so zavedením elektronického mýta avizovali ostrý štrajk a blokovanie čerpacích staníc, no skončilo to len pri zbieraní podpisov. ... Situácia neskôr zachraňovali študenti. Hromadný protest tak dostal nový názov. Petično - informačná akcia. Zber petičných podpisov v Bratislave prebiehal v celku nenápadne. Miestami bolo treba aktivistov hľadať.   dobrovoľní aktivisti -------------------- Sme dostali ponuku, tak ako pôjdeme aj tak sú prázdniny nemáme čo robiť. Nie sme platení, určite. 
 Problémy pokračovali aj v piaty deň, aspoň podľa niektorých televízií. Tu sú titulky ich príspevkov: 
 Hranice sú upchané čoraz viac (Markíza) Platia aj za neprejazdené úseky (Markíza) Problémy s mýtom: Kolóny, chaos a nervozita (TA3) Dopravcovia dali vláde ultimátum (TA3) Dopravcovia hrozia tvrdými protestami (STV) 650 tisíc za prvé hodiny (Joj): "Elektronické mýto funguje. Tvrdí prevádzkovateľ systému SkyToll. Do dnešnej 15-tej hodiny za prejazdené kilometre vraj vybrali 350 tisíc eur. Aj keď sa vodiči sťažovali na výpadky systému, išlo len o lokálne problémy..." 
 Kým ostatné televízie informovali viac o podpore protestu aj od skupiniek vodičov osobných áut, či na internetovom Facebooku, Joj ako jediná urobila anketu, kde traja zo štyroch vodičov formu protestu kamionistov kritizovali! (8.1). 
 anketa - vodiči -------------------- Lezie mi to totálne na nervy, pretože ja v jednom kuse som na ceste a obmedzuje ma to.   anketa - vodiči -------------------- Nie, no tak naozaj musia sa starať o svoj job. Musia sa starať o svoje peniaze. Musia sa starať o to, aby prežili.   anketa - vodiči -------------------- Je to prijate možno viac ako rok ten zákon a myslím, že dopravcovia mohli skorej robiť nejaké akcie.  anketa - vodič -------------------- Mali si zvoliť inú formu toho protestu. Nie takto skolabovať dopravu.  
 Joj si ako jediná minulý piatok vôbec nevšimla prvé ústupky štátu v mýtnom systéme, no opäť si podala protestujúcich:   
 Keďže kamionistov polícia nevpustila do centra mesta na úrad vlády klopali tak trochu netradične. Premiérovi poslali e-mail. 
 A ako ma upozornil čitateľ, ani včera v správach sa Joj nevyhla negatívnemu zaujatiu: 
 Nekonečným protestom sa dala označiť demonštrácia skupiny slovenských autodopravcov. ... V tom čase sa pred úradom vlády zbehla hŕstka kamionistov, ktorých tam bolo menej ako novinárov (veď šli na stretnutie, nie protestovať, tak aký má význam že ich bolo málo???-g.š.). Ich zástupcovia sa chceli stretnúť s premiérom. Keďže neprestali s blokádou, neprijal ich. A tu vidíte názor protestujúcich na to, že by mali ukončiť blokádu (pohľad kamery na kopu hnoja pred Úradom vlády - g.š.). ... Podľa premiéra totiž mýto funguje a bude aj naďalej. Posledné informácie hovoria o tom, že sa za 11 dní jeho prevádzky vyjazdilo do štátnej kasy dovedna viac ako milión 600 tisíc eur. 
 Zdá sa, že veľká zákazka ako mýto ukázala pre Joj hranice, kde už služba vlastným divákom nie je v spravodajstve rozhodujúca. 
  STV nechcela robiť paniku: Pri porovnaní hlavných večerných správ televízií podľa časového priestoru venovaného vtipnej policajnej akcii na letisku v Poprade vyplýva, že STV téma zaujala najmenej. Dvakrát menej ako TA3 a Markízu, a dokonca tesne menej ako bulvárnu Joj. STV napríklad vôbec neuviedla citáty zahraničných médií na vykreslenie dosahov, aké slovenská akcia mala na reputáciu krajiny v zahraničí. 
 
 



TA3
13:47


Markíza
12:48


Joj
07:28


STV
06:55



Zdroj: Televízie, 5-7.1, hlavné večerné spravodajstvo
  Predčasný výrok roka: tiež padol o situácii pri zavedení mýta:  STV, 1.1: „Prvý deň funguje všetko ako švajčiarske hodinky.“ (Simona Simanová)  Mimochodom, v spravodajstve STV za ostatných 10 rokov nepadlo porovnanie fungovania niečoho so švajčiarskymi hodinkami ani raz. Až teraz – keď sme zaviedli mýto. Výrobcovia značiek Omega, Longines či Rolex sa určite potešili. 

