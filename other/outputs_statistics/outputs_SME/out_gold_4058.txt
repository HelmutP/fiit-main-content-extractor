

   
 Sedeli sme na tráve 
 Dvaja a nie jeden 
 Hrali sme karty so životom 
 Spievali pili kolu 
 teraz sme stratení  
 a dávame si zbohom. 
   
   
 Prešli sa roky po horách 
 A zamávali z diaľky  
 Zostal mi v rukách silný  
 Prúd  
 Spomienok všelijakých. 
   
   
 Karty som venoval okoloidúcim  
 Hodinky tykajú a neúprosne  
 A miesto pokoja a rozumu  
 Si pieseň v tele nosím. 
   
   
 Ty si preč a je ti fajn  
 A život nehrá prvé husle  
 A ja si plávam naprieč tmou  
 A zaspávam v nezmysle. 
   
   
 Teraz je jedno  
 Ako skončí príbeh dvoch  
 Napol bláznov 
 Už nie sme rovnakí  
 A v tom mám jasno 
 A nevadí mi ostrý dym 
 A viem že svetlo zhaslo.  
   

