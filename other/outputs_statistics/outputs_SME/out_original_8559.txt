
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomír Galko
                                        &gt;
                Nezaradené
                     
                 Smeti patria do koša. Aj táto vládna koalícia 

        
            
                                    11.6.2010
            o
            2:45
                        (upravené
                16.6.2010
                o
                17:27)
                        |
            Karma článku:
                14.81
            |
            Prečítané 
            4246-krát
                    
         
     
         
             

                 
                    Určite mnoho občanov Slovenska by ma podporilo v myšlienke, že viacerí predstavitelia súčasnej vládnej garnitúry patria niekde úplne inde a nie do koša. Za to, čo našej krajine počas uplynulých štyroch rokov urobili - kam ju doviedli. Bolo o tom napísaných už množstvo článkov, blogov - rozpisovanie všetkých zlodejín a škandálov už nie je naozaj nutné. Avšak - sú chvíle, keď aj symbolika poteší - obzvlášť symbolika predstavujúca nádej zmeny. Príďte teda 12.06.2010 symbolicky hodiť do koša súčasnú vládnu koalíciu.
                 

                 Do otvorenia volebných miestností zostáva už naozaj krátky čas. Budú to voľby, ktoré rozhodnú o tom, či sa nám podarí zvrátiť krkolomnú cestu do priepasti, naordinovanú súčasnými dobrodruhmi, ktorí vedú tento štát alebo v tej deštrukcii títo dobrodruhovia budú môcť pokračovať ďalej.   Kampaň pomaly končí. Súťaživosť aj medzi pravicovými stranami, ktorá však podľa môjho názoru neprekročila etické hranice, bola viditeľná. Je to samozrejmé, veď voliči hlavne týchto strán sa navzájom často prelínajú.   Teraz však ide už do tuhého a nepodstatné veci musia ísť bokom. Ide totiž už o všetko, ide o spoločnú vec.   Chcem poprosiť všetkých ale doslova všetkých priaznivcov SDKÚ-DS, KDH a Most - Híd aby išli voliť svoju stranu. Všetkých - do jedného. A pre priaznivcov SaS platí tá istá prosba.   Ak niekto "balansuje" medzi niektorými z vyššie spomenutých strán, nech si vyberie jednu, preňho tú najviac priechodnú ale hlavne nech ide voliť tiež.   Aj posledný prieskum od Focusu ukázal, že spoločne máme na to.   HZDS - strana s čiernou minulosťou a verím, že aj čiernou budúcnosťou, sa potáca na hranici 5%. A reálne  môže zostať mimo parlament. Všetko je to len o volebnej účasti. Ako dokážeme zmobilizovať svojich voličov.   Veľmi si želám, aby sa nám to podarilo spolu dosiahnúť. Verím, že Most-Híd sa dostane do parlamentu so skvelým výsledkom a SDKÚ-DS aj KDH prajem maximálnu disciplinovanosť ich elektorátu pri ich ceste do volebných miestností. Aby ani jeden hlas nevyšiel nazmar.   Vrátim sa k úvodu článku. V sobotu - 12.06.2010 budú až do 22,00 hod na Námestí SNP k dispozícii tri smetné koše. Tri smetné koše - tri kontajnery na separovaný zber volebných lístkov troch strán, zodpovedných za našu zdevastovanú krajinu - Smeru, HZDS a SNS.   Odovzdajte zmene politických pomerov Váš hlas a príďte symbolicky hodiť do koša túto vládnu koalíciu. Príďte si s nami pripiť nealkoholickými nápojmi na novú šancu. Som presvedčený, že v neskorý večer alebo na druhý deň ráno si budete môcť s Vašimi priateľmi a rodinami pripiť aj šampanským.   Že to proste vyjde.   Akcia začína o 10,00 hod. Za SaS aj s Janou Kiššovou budeme tam. Rovnako ako Lucia Žitňanská a Ivan Mikloš za SDKÚ-DS, Jana Žitňanská a Daniel Lipšic za KDH a László Sólymos a Ondrej Dostál za Most-Híd - OKS.   Ten údajný hovor údajného manipulátora z údajnej nahrávky, ktorá sa dnes objavila, je už len taká čerešnička na torte. Žiadalo by sa povedať - taká posledná kvapka v pohári. Avšak ten pohár je už dávno, dávno pretečený.   Tak zaplníme spoločne tie koše v sobotu rekordne rýchlym spôsobom?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (31)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Alternatíva voči Ficovi existuje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Píše redaktorka Pittnerová za špinavé peniaze Smeru?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            "Kali" prenajíma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Bejby sú naďalej medzi nami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Ten Sulík sa zase vyfarbil!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomír Galko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomír Galko
            
         
        lubomirgalko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Exminister obrany a poslanec NR SR Podpredseda strany Sloboda a Solidarita Zakladajúci člen a člen Republikovej rady.  ---------------------------------------------------------- Mojim tvrdým bojom proti korupcii som sa stal vážnym problémom a nepriateľom pre mnoho nečestných ľudí, včítane politikov, naprieč celého politického spektra, ktorí doteraz radostne parazitovali na systéme                ------------------------------------------------------- Nikdy mi nešlo o koryto, ale pokiaľ mam možnosť, budem stále ťať do živého, odkrývať zlodejiny a upozorňovať na tú rozbujnelú chobotnicu aj na jej chápadlá. -----------------------------------------------------------  Pretože si myslím, že to stále má zmysel. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    164
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6774
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




