
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Bajus
                                        &gt;
                Nezaradené
                     
                 Abruzzo som si zamiloval - Taliansko 2010, časť III. 

        
            
                                    15.5.2010
            o
            18:26
                        (upravené
                16.5.2010
                o
                11:26)
                        |
            Karma článku:
                7.39
            |
            Prečítané 
            1368-krát
                    
         
     
         
             

                 
                    Otváram oči a budím sa... Z malého okienka na mna vykúka abruzzské ranné slnko. Viem, že to nie je sen, ale krásne prebudenie do nového dňa. Čaká ma náročný deň plný turistiky a večerný presun do hotela v mestečku Chieti (neďaleko Pescary), kde zotrvám ďalšie dve noci.
                 

                 
Castel del MonteBranislav Bajus
   Ako už viete, nachádzam sa v historickej stredovekej obci Santo Stefano di Sessanio. Je to vari najočarujúcejšia dedinka akú som kedy videl. Včera večer som sa ešte stihol poprechádzať vonku po tajomných uličkách a na to ticho nezabudnem. Nočná hmla sadla takmer všade, miestni obyvatelia už spali a ja som si naplno vychutnával romantiku Santa Stefana.   Santo Stefano di Sessanio sa nachádza len 27 km od L´Aquily a približne 150 km od Ríma. Apeninské pohorie, v ktorom leží, je zasnežené ešte aj na konci apríla. Tunajšie počasie sa dá prirovnávať asi ako k podtatranskej obci. Je tu avšak o čosi väčšia vlhkosť, keďže celý región je situovaný bližšie k moru. K jadranskému pobrežiu sa dostanete veľmi rýchlo a to je takisto značná výhoda pre potenciálnych návštevníkov.   Otváram okno a s veľkou zvedavosťou sa nachýlim von. Predo mnou sa týčia vzdialené pahorky Národného parku Gran Sasso. Idem si urobiť rýchlu kávu a kochám sa naďalej prekrásnym výhľadom na okolie. Cítim sa ako kráľ, ktorý má všetko pod kontrolou a v „mojej“ dedinke o mne všetci vedia.:) Vari som to aj vystihol. Veď v Santo Stefano žije len 120 obyvateľov a každý o každom vie. Dokonca na včerajšej večeri v miestnej útulnej reštaurácii som postretával domácich, ktorí si prišli posedieť k vínku. Medzi turistami Santa Stefana som bol jediný Slovák, ale nebol som sám. V našej skupinke boli aj traja Holanďania a jedna Češka. Nasledujúci program sme mali rovnaký a aspoň som sa čo to dozvedel aj o inej kultúre.                      Vrátim sa naspäť k stručnému opisu lokality, kde som strávil príjemné chvíle. Santo Stefano je v Taliansku známe ako jedna z najmalebnejších dediniek a právom nesie neoficiálne pomenovanie „I Borghi Piu belli D´Italia“. Každoročne sa tu prvý septembrový víkend koná festival jedla tzv. „Sagra delle Lenticchie“. Ten som síce nemohol uvidieť, ale toto miesto na mňa zanechalo silný dojem a spomienky. Príjemní ľudia, bohatá história a skvelá atmosféra mi dali veľa. Naučil som sa pochopiť tunajší spôsob života a rád by som sa s domácimi vymenil aspoň na 1 mesiac.:)                       Keď sme včera podvečer prišli do Santo Stefano, auto sme zaparkovali na začiatku a ďalej sme si museli s batožinou vystúpať pár desiatok krokov až k apartmánu. Autom by sme sa tam nedostali, keďže je to kamenné historické miesto s úzkymi a strmými uličkami. Vodič nechal auto aj s kľúčmi otvorené, nezamknuté. Dokonca nechal otvorené aj dvere na aute... Hneď som ho na to upozornil, že asi zabudol. Lenže on sa šalamúnsky pousmial a odpovedal: „Tu sa nekradne! Je to v poriadku.:)“ Vtedy som mal pocit, že to nemôže byť pravda, ale skutočne sme nechali auto úplne otvorené a vzďaľovali sme sa do centra dedinky.               Oficiálnym patrónom obce je „Sessanio“ alebo „Sextantia“. Obidva názvy vznikli v románskej dobe už v 11. storočí n. l. Santo Stefano malo ešte pred rokom krásnu vežu „Torre Medicea“, ktorú bolo vidieť už z diaľky. Bol to architektonický skvost, ktorý bohužiaľ zničilo zemetrasenie a veža sa zrútila. Dnes je možné vidieť len jej konštrukciu, ktorá dočasne nahrádza a pripomína pôvodný tvar. Veža sa má vraj znovu vybudovať a všetci dúfajú, že to bude čoskoro. Človek si aspoň takto vie predstaviť aké silné bolo minuloročné zemetrasenie. Hoci hlavné epicentrum bolo v L´Aquile, otrasom sa nevyhli ani okolité obce. Santo Stefano si príroda vybrala a historická veža padla. Miestny sprievodca (starší pán) nám poukazoval všetky uličky a povodil nás aj do niekoľkých apartmánov, ktoré sú pripravené na ubytovanie ďalších hostí.           Po vyše hodinovej zaujímavej prehliadke sme sa rozlúčili a pokračovali ďalej. Calascio bol ďalším bodom nášho celodenného programu. Je to dedinka vzdialená od Santo Stefano len 5 km. Má podobný vzhľad, kde dominuje elegantný chrám so sakristiou zo 16. storočia „Chiesa Santa Maria delle Grazie“.               Nad Calasciom sa týči jedinečná pevnosť „Castello di Rocca Calascio“, postavená z kameňa a bola využívaná výhradne pre prípadné vojenské použitie a ochranu pred útočníkmi. K hradnej pevnosti sme použili auto a úzkymi cestami sme sa vyštverali až úplne hore. Rocca di Calascio má veľmi výhodnú polohu z obranného hľadiska. Pevnosť (cca 1500 metrov n. m.) je vybudovaná v tvare štvorca s vežami okolo centrálneho námestia a patrí medzi najvyššie opevnenia v Taliansku. Podľa slov miestneho sprievodcu tu tradične prebieha aj náboženská cesta a natáčali sa tu filmy „Lady Hawke“ (Michelle Pfeiffer, Rutger Hauer), či „Meno ruže“ (Sean Connery) alebo „Padre Pio“.                                                                       Po pár kilometroch ďalej na východ sme sa zastavili v ďalšej stredovekej obci Castel del Monte a mali sme možnosť prezrieť si múzeá historických obydlí, ale aj spôsobu života v nich.                               Po prehliadke naša púť pokračovala prejazdom cez plošinu Campo Imperatore Plateau, odkiaľ je nádherný výhľad na najvyššie zasnežené vrcholy. V motoreste sme si dali typický neskorý abruzzský obed chutného opekaného mäsa s červeným vínom.                   Na konci celodenného programu som absolvoval zaujímavý pobyt vo vlčej rezervácii v mestečku Popoli. Bolo to veľmi zaujímavé a zahliadol som vlkov v oplotenom a uzavretom priestore uprostred parku.   Podvečer som sa ubytoval v hoteli Best Western Parco Paglia v Chieti a na ďalší deň som sa stretol so známym talianskym priateľom. Spoločne sme si pozreli prístavné mesto Pescara s novovybudovaným mostom „Il Ponte del Mare“ a priľahlé letovisko Francavilla al Mare.                                                       Aj pobrežné časti sú veľmi pekné a cítil som tam pokoj a celkovú spokojnosť miestnych obyvateľov. Letná sezóna sa pomaly začne a v Pescare to začne ožívať...   Môj pobyt v Abruzzo si veľmi cením a chcel by som sa tam raz opäť vrátiť. Odporúčam navštíviť všetky miesta, ktoré som videl a tie, ktoré som nestihol, verím, že uvidím niekedy v budúcnosti.                         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Bajus 
                                        
                                            Narýchlo vo Viedni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Bajus 
                                        
                                            Rakúsko je lyžiarskym rajom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Bajus 
                                        
                                            Kostolík svätého Michala Archanjela v Dražovciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Bajus 
                                        
                                            Krátke potulky Wadowicami a kráľovským Krakovom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Bajus 
                                        
                                            Trenčiansky hrad
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Bajus
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Bajus
            
         
        branislavbajus.blog.sme.sk (rss)
         
                                     
     
         Príležitostný cestovateľ, ktorý hľadá skutočný život obyčajných ľudí. Albert Camus: "Každá minúta života má v sebe vlastnú hodnotu zázraku." 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1243
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nový začiatok , alebo keď sa Slovák sťahuje do Venezuely
                     
                                                         
                       Azerbajdžan - krajina večného ohňa.
                     
                                                         
                       Rakúsko je lyžiarskym rajom
                     
                                                         
                       Narýchlo vo Viedni
                     
                                                         
                       Kostolík svätého Michala Archanjela v Dražovciach
                     
                                                         
                       Stretnutie v Ortaköy, Istanbul
                     
                                                         
                       Krátke potulky Wadowicami a kráľovským Krakovom
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




