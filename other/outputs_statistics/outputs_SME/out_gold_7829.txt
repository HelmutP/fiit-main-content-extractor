

  
 To, že som v dobách centrálne plánovaného (rozumej socialistického) hospodárstva často chodil do Prahy na služobné cesty kvôli pečiatkam a podpisom, aby som mohol vybaviť to, čo teraz si dokážem ako živnostník vybaviť od pracovného stola doma, malo aj krásnu stránku. 
  
 Do pražských ulíc som vybehol skoro ráno, lebo pracovná doba sa začínala v mnohých podnikoch už pred siedmou, ako pozostatok z dôb pracovitého cisára pána, ktorý nám vládol nekonečných šesťdesiatosem rokov. Tento starý pán vraj nemal dobrý spánok a tak zavčasu vstával, zobudil svojich ministrov, aby mu referovali o plnení úloh, tí museli mať po ruke svojich štátnych tajomníkov, tí zase vedúcich oddelení a referátov, až sa postupne  zobudila do skorého začiatku pracovného dňa celá ríša a to nám zostalo aj dlhé desaťročia potom, ako sa ríša cisára pána rozpadla. Zato už okolo tretej popoludní som musel končiť, lebo otravovanie služobných kontaktov pred ich odchodom z práce, sa mi mohlo vrátiť ako bumerang v nevybavených a ťažko vybaviteľných  pracovných záležitostiach. 
   
 A tak neskoré popoludnie, podvečer a večer som venoval spoznávaniu Prahy, nepochybne jedného z najúchvatnejších hlavných miest Európy i sveta. Plnil som pritom odznak Zlatá Praha tak, že som si do špeciálneho zošitka zbieral pečiatky z múzeí, galérií a rôznych pamätihodností a na záver, po niekoľkých takýchto návštevách Prahy, som bol odmenený vkusným odznakom. Podobne pri plnení odznaku Turista ČSR (Českej socialistickej republiky). Napriek škaredému úradnému názvu samotná myšlienka a s ňou spojená KPČ, teda kultúrno-poznávacia činnosť, stála za to. Nuž a večer bol na spoznávanie repertoáru pražských divadiel. 
   
 Všetko to malo iba jedinú chybičku krásy a síce že sa v Prahe ťažko hľadalo ubytovanie (nočný život Praha určite mala aj v časoch socialistického puritánstva, len ja som oň akosi nikdy nestál). A tak som musel večer často sadnúť na vlak a vybrať sa na noc niekam mimo Prahy alebo jednoducho sa vrátiť domov nočným rýchlikom. Dnes už ubytovacie zariadenia sú v Prahe na každom kroku, ale i turistov je tam stále viac a viac a tak neviem, či by som zase nemusel ďaleko mimo Prahy alebo len domov. 
   
 Teraz ma do Prahy pracovné povinnosti už nenútia, lebo doba sa zmenila a tie pečiatky pražských organizácií už netreba. Nie pre život náš každodenný pracovný, možno áno pre plnenie turistických odznakov. Mám veľmi pekné skúsenosti s takýmto pečiatkovaním. 
  
   
   

