

 Časy bratislavského propelera sú už dávno zaviate prachom, no pre malých lodníkov sa dá nájsť zodpovedná náhrada. Tých najmenších zoberte na vyhliadkovú plavbu (pozor, len od stredy do nedele), s tými väčšími skúste výlet z bratislavského nábrežia na Devín. V námorníckych tričkách si môžete s ratolesťami urobiť predprípravu na letnú dovolenku pri mori. 
  Pohľad z lode na vynovenú dominantu 
  Spod mosta všetko vyzerá trochu inak 
  Botel, parlament a hrad 
 Lístky na plavbu si môžete zarezervovať vopred cez internetovú stánku www.lod.sk, ale dajú sa kúpiť bez problémov aj pred plavbou v budove prístavu na Fajnorovom nábreží (pri budove Slovenského národného múzea). A potom stačí s dobrou náladou a malým občerstvením (ale to môžete kúpiť aj priamo na lodi) vykročiť za novými zážitkami. 
 Plavba smerom na Devín trvá približne 90 minút, takže budete mať dosť času na kochanie sa oboma dunjskými brehmi - pohľad na historickú časť nábrežia s vynoveným hradom vystriedajú novostavby Riverparku, ktorý akoby bol postavený len pre pohľad smerom od Dunaja. Kúsok romantiky na ľavý breh Dunaja vracia pekná lodenica, pravobrežnou zaujímavosťou sú rakúske rybárske chatky, kde rybári chytajú ryby do sietí. Až postupne sa pred vami objaví silueta zrúcanín devínskeho hradu. Pre deti je najzábavnejšie, keď okolo prepláva loď, ktorá narobí poriadne vlny. 
   Pohľad na hradby hradu Devín z Dunaja 
  Panenská veža na Devíne, pod ňou Slovanské nábrežie 
 Dve hodiny, kým sa loď vráti do centra Bratislavy, postačia na prechádzku Slovanským nábrežím, výhľad na sútok Moravy a Dunaja, prehliadku hradu či malé občerstvenie. Ak si zo sebou zoberiete aj bicykel, môžete pokračovať po cyklistickom chodníku pozdĺž Moravy alebo sa do mesta vrátiť vlastným pohonom. 
  Bratislavké UFO 
  Výletná loď, v pozadí budova prístavu a budova SNM 
 Na cestu späť po prúde stačí kapitánovi už len polhodinka. S vetrom vo vlasoch budú vaše ratolesti (a možno i vy) príjemne unavení a pripravení na ďalší výlet po Dunaji. Napríklad do Gabčíkova preplaviť plavebné komory vodného diela či len tak, na vyhliadku po bratislavskom nábreží. 
 Praktické informácie: 
 - cena lístku Bratislava - Devín - Bratislava:  dospelí 5,50 €, deti 2 - 15 rokov 3,50 €, bicykel 2,00 € 
 - cena jednosmerného lístku: dospelí 3,50 €, deti 2 - 15 rokov 3,00 € 
 Loď premáva denne okrem pondelka, odchod z Bratislavy 10:00 a 14:30. 
 - cena plavby "Bratislavský okruh": dopelí 4,00 €, deti 2 - 15 rokov 2,50 € 
 Lode premávajú od stredy do nedele s odchodmi o 11:00, 13:30 a 15:30. Viac informácií nájdete na www.lod.sk 
   
  
   
 Poznámka: Túto cestu hradil manžel :-) 
   
   

