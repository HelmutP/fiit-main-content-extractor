

 Nová pravica si už paralelne delí kreslá vo vláde. Všetko sa zdá byť jasné. Budeme mať „slušnú" premiérku, hoci aj s nejakým tým „neslušným" šrámom. Najsilnejšia pravicová strana sa zdá byť pevná a jednotná. Nevedno však, či tak tomu je skutočne, či v nej nejestvujú frakcie, čo by radšej „vládli" aj v ľavo-pravej vláde. „Mačky vo vreci", teda OKS a Obyčajní ľudia, sú možno nielen jediní, neobyčajní účastníci parlamentu. 
 Inzerát Smeru a SNS by mohol teda na najbližšie obdobie uhladene, do predčasných volieb 2011, znieť reálnejšie: „Hľadáme 4 poslancov. zn. Dohoda istá." 
   

