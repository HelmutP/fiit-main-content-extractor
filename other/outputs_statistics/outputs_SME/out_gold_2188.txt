

 Odvtedy už ubehlo pár týždňov, teraz sediac pri šálke čaju sa mi myšlienky vracajú späť k tejto téme. Snažím sa spomenúť si na svetonázory - podľa mňa múdrych ľudí, ktorí sa vo svojich dielach venovali tejto téme. Uznávam, že materiál je pomerne skromný. Napadá mi Yalom a jeho uchopenie vedomia, ktoré zanikne. Jung a jeho návrat k stavu pokoja. Freud a jeho racionalizácia a popieranie spirituálnej roviny. Hesse a Harukami a ich vnímanie smrti ako možné riešenie pre nezvládnuteľné problémy. 
 Nedávno  v správach hovorili o atentátničke, ktorá zabila okrem seba ešte 13 ľudí a o ďaľšich dvoch v metre. Predpokladám, že v nádeji, že potom sa dostane do raja. Fanatizmus, bezpochyby. Ale vráťme sa späť k téme smrti, vlastne po smrti. Náboženstvá moderného sveta operujú so strachom ako peklo, zníženie karmy a nedosiahnutie nirvány. 
  Ale nejde mi o náboženstvá, skôr o vnútorne presvedčenie človeka. Ako veľmi ovplyvňuje moje správanie to, že verím v peklo alebo nebo? Je toto presvedčenie dostatočne silné nato, aby vo mne evokovalo niečo ku zmene svojho správania voči spoločnosti v ktorej žijem? Jednoducho povedané som lepší človek, ak nepokladám smrť za definitívny koniec svojej duše? Ako na mňa vplýva strach, že sa nedostanem do neba? 
  Som si istá, že drvivá väčšina z vás by mi odpovedala, že to vôbec nesúvisí. Že moje vnútorné presvedčenie alebo viera zo mňa nečinia lepšieho, či horšieho človeka. Ale také jednoduché to predsa len nie je. Čoho dôkazom je detailné vypracovanie tejto tematiky v každom náboženstve. Zdá sa, že ľudský mozog napriek (alebo práve kvôli) svojej vyspelosti nedokáže spracovať definitívny koniec, neexistenciu, zánik. Z určitého dôvodu potrebuje predstavu, že potom ešte niečo bude. Iná podoba, určité kontiníum. 
 V gréckej mytológii bohom smrti je Thanatos dvojička Hypnos bohu spánku. Dnes už vieme, že v spánok je aktívny a nie pasívny ako sa dávno predpokladalo. Teda smrť a spánok sa veľmi líšia. Ako si smrť definujete vy? Čo si myslíte o nebi? Ako si predstavujete peklo? 
 Ak by ste mali chuť sa zamyslieť nad týmito otázkami: 
 1 . Ako by sa zmenilo moje správanie, ak by som mal 100% garanciu, že po smrti sa dostanem do neba a bude to niečo veľmi príjemné. 
 2. Ako by sa zmenilo moje správanie, ak by som mal 100% garanciu, že po smrti už nič neexistuje,že moje vedomie zanikne. 
 3.  Ako by sa zmenilo moje správanie, ak by som mal 100% garanciu, že po smrti sa musím vrátiť- reinkarnovať na nižšiu úroveň. 
 Ak máte chuť  podeliť sa o ten myšlienkový experiment potešíte. 

