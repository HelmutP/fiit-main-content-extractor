
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Referendum žije! 

        
            
                                    8.2.2009
            o
            22:36
                        |
            Karma článku:
                13.95
            |
            Prečítané 
            25141-krát
                    
         
     
         
             

                 
                    Vadí Vám, keď naši poslanci jazdia opití? Tiež si myslíte, že šéf polície nemusí mať pretekárske Audi s motorom Lamborghini? Nechcete platiť 1680 Sk ročne za zhnitú STV? Podporte Vaším podpisom Referendum2009
                 

                  Možno si spomínate na január minulého roka, kedy sme vyhlásili petíciu za zrušenie koncesionárskych poplatkov. Práve vtedy pripravovala naša vláda nový zákon o koncesionárskych poplatkoch, podľa ktorého mal za "službu poskytovanú verejnosti " platiť každý - bez ohľadu na to, či má televízny prijímač alebo či vôbec má televízny signál. Rovnako scestný bol návrh, aby platili zamestnávatelia za svojich zamestnancov, napriek tomu, že zamestnanci už raz ako súkromné osoby poplatky platia a napriek tomu, že v práci (možno okrem STV) sa predsa televízia nepozerá. Nakoniec aj tak naši ctení poslanci tento nezmyselný zákon schválili.     Prvé mesiace sme boli s našou petíciou za vyhlásenie referenda celkom úspešní a vyzbierali sme vyše 80 tisíc podpisov, potom sme dočasne túto aktivitu utlmili. Dôvodom boli nevyjasnené iné aktivity a to politické.     Pri zbieraní podpisov za zrušenie koncesionárskych poplatkov sme sa stretli s týmito výhradami:      Odhadovaných 200 mil. Sk (6,6 mil. Eur) nákladov na referendum je príliš drahý prieskum verejnej mienky   Aj tak nepríde požadovaných 50% oprávnených voličov k samotnému referendu a referendum bude neplatné   Ide nám len o zviditeľnenie, lebo plánujeme založiť politickú stranu a o nič iné      Uznávam, že je na nich kus pravdy a preto sme sa rozhodli k pôvodnej otázke pridať ďalších päť otázok. Potrebných 350 tisíc podpisov plánujeme zbierať celý tento rok a v januári 2010 ich chceme prezidentovi alebo prezidentke odovzdať spolu so žiadosťou o vyhlásenie referenda. Viedli nás k tomu tieto úvahy:      Uskutočniť referendum so šiestimi otázkami naraz je finančne efektívnejšie   Referendum so šiestimi otázkami je atraktívnejšie a pritiahne viac ľudí   Na zbieranie podpisov máme takmer rok času   Keď odovzdáme podpisy v januári 2010, jediný rozumný termín na konanie referenda je deň parlamentných volieb (prezident je povinný vyhlásiť referendum do 30 dní, ktoré sa musí uskutočniť do ďalších 90 dní, ale zároveň nesmie byť 90 dní pred voľbami, smie byť ale v deň volieb)   Referendum v deň parlamentných volieb výrazne zníži náklady naň, lebo sa bude konať spolu s voľbami   Referendum v deň parlamentných volieb výrazne zvýši šance, že príde k nemu dostatočný počet ľudí, a bude platné      Členovia petičného výboru sú zároveň členmi prípravného výboru strany SAS a referendom sledujeme dva ciele. Prvý cieľ je zviditeľniť novovznikajúcu stranu, ktorá chce byť 2010 v parlamente a 2014 vo vláde a druhý cieľ je presadiť zopár rozumných vecí, ktoré naši politici nikdy nepresadia. Práve preto väčšina zvolených otázok obmedzuje privilégiá politikov.     Aby sme sa vyhli možným problémom so znením otázok, požiadali sme ústavného právnika Dr. Radoslava Procházku o ich naformulovanie. Terajšiu petíciu sme nazvali Referendum2009 a vybrali sme tieto otázky:      1. ZRUŠENIE KONCESIONÁRSKYCH POPLATKOV
      Samozrejme, náš klasik nesmie chýbať. Zákon je dnes rovnako nezmyselný ako pred rokom, len marazmus v STV je ešte väčší, nový šéf ešte neschopnejší a STV má ešte nižšiu sledovanosť. Určite chceme zrušiť koncesionárske poplatky, ale medzičasom by neuškodilo zrušiť celú STV.      2. OBMEDZENIE POSLANECKEJ IMUNITY
      Ani vy nemáte ten dojem, že poslanec môže  jazdiť opitý, aby mohol lepšie rozhodovať? Pôvodný zmysel imunity bol ten, aby poslanci mohli rozhodovať podľa svojho najlepšieho svedomia a vedomia. Ale to je dávno preč, teraz slúži imunita len na to, aby mohli jazdiť opití a rýchlo. Preto chceme zrušiť priestupkovú imunitu.      3. ZNÍŽENIE POČTU POSLANCOV ZO 150 NA 100
      Veľakrát sú z nich len hlasovacie figúrky a aspoň ušetríme na platoch, asistentoch, kanceláriách, notebookoch. Nemajte obavu, že možno budú musieť o niečo viac robiť. Minulý rok mali len 90 pracovných dní a tento rok si naplánovali iba 78.      4. STANOVENIE MAXIMÁLNEJ CENY VLÁDNYCH LIMUZÍN NA 40 TISÍC EUR
      Neviem ako vás, ale mňa naozaj vytáča, keď si naši mocipáni kupujú z našich daní každú chvíľu najnovšie a najdrahšie autá a navyše jazdia na nich bezohľadne a arogantne. Nepochopím, prečo musí mať náš policajný prezident Audi RS6 s motorom Lamborghini. Taká Škoda Superb alebo Peugeot 607 sú pohodlné a kvalitné autá a nikomu nepadne koruna z hlavy, keď sa bude voziť na nich.      5. VOĽBY CEZ INTERNET
      Je to rýchlejšie, lacnejšie a efektívnejšie. Možnosť voliť aj cez internet (ako doplnok ku klasickému spôsobu) by pritiahla k voľbám viac ľudí, čo je pre demokraciu prospešné. Pokiaľ je to možné v Estónsku, musí to byť možné aj u nás.      6. ZMENA V TLAČOVOM ZÁKONE
      Konkrétne ide o zrušenie práva na odpoveď pre našich politikov. Nemajte obavu, ešte stále im ostáva právo na opravu a právo na dodatočné oznámenie     Podpísať sa pod našu petíciu má význam keď súhlasíte aspoň s jednou otázkou. V tejto fáze ide len o vyhlásenie referenda a až v samotnom referende sa bude hlasovať o každej otázke samostatne.     Prosím prepošlite tento mail vašim známym.     Podpisový hárok a všetky náležitosti nájdete na Referende2009  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (277)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




