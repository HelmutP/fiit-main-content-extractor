

 U nás, v Harmónii, sme ku príkladu dnes spojazdnili bazén. Vlastne nie jeden, ale rovno tri. Trištvrte ročné písomky sú za nami a tak zostalo trošku času na blbnutie (ehm, chcel som povedať - na voľnejšie opakovanie :o) 
 Decká sa rozdelili na tri skupiny a každá z nich dostala za úlohu napustiť jeden "bazén" spoluhláskami. 
   
   
 Baby, ako jemnejšie pohlavie, si prirodzene zvolili mäkké spoluhlásky a na ich znázornenie použili balóny :O) 
  
   
 Chalani, ako páni tvorstva a rytieri, si potom vzali na mušku obojaké spoluhlásky (našli si akési plastové loptičky :O)  
  
 a tvrdé spoluhlásky (tu zas padli za obeť drevené kocky z herne - pssst, neprezraďte nás pani riaditeľke :O)))  
  
   
 A potom už len kúpanie a samé životné pozitíva a istoty :o))) 
  
   
 Či sa súkromné školy skutočne topia v peniazoch, to neviem. No celkom s istotou si dovolím tvrdiť, že blbnúť na hodinách ...ehm, chcel som povedať - učiť deti zaujímavejšie a ponúkať im viac, sa dá kdekoľvek...  
 Nejde teda o financie...Jedinou otázkou je, či sme ochotní vstať spoza katedry a pred kúpaním sa trošku zapotiť... 

