

 Už v dávnej minulosti sa rebarbora dlanitá (Rheum palmatum) používala nielen ako potravina, ale aj ako prírodné liečivo. Liečiteľstvo využívalo predovšetkým koreň. Rebarbora pozitívne ovplyvňuje tvorbu krvi, posilňuje svaly, kosti aj nervy, prečisťuje organizmus. Používa sa aj na výrobu vlasového tonika, využitie našla v aromaterapii a v olejkárstve. Pôsobí ako mierne preháňadlo a poradí si s nechutenstvom. Listy sa používajú na liečbu popálenín a rôznych poranení. 
 Nakoľko rebarbora obsahuje veľké množstvo kyseliny šťavelovej, mala by sa konzumovať striedmo a ľudia s chorým žlčníkom,  oslabenými obličkami, trpiaci na obličkové kamene by sa jej konzumácii mali vyvarovať. 
 Zdravým jedincom  rebarbora poskytne vysoký obsah vitamínu A a B, paradoxne, hoci chutí kyslo, s obsahom vitamínu C sa moc hrdiť nemôže. Obsahuje však iné cenné látky - železo, niacín, jód, draslík, fosfor, horčík. Cenná je pre vysoký obsah vápnika, ktorý môže byť alternatívou pre tých, ktorý sú alergickí na mliečnu laktózu. 
 Pranostiky hovoria, že rebarboru by sme mali konzumovať do Jána, lebo potom vraj už tak dobre nechutí. Podľa kulinárskej mágie sa rebarbora považuje za rastlinu lásky. Za najdokonalejší pokrm lásky si vraj treba predstaviť rebarborový koláč s jahodami. 
 Už som spomenula, že ide o druh zeleniny, ale ľudstvo sa k nej správa ako k ovociu. Je vhodnou surovinou na prípravu kompótov, želé, štiav, krémov, marmelád. Možno z nej vyrobiť aj víno. Snáď najtypickejším rebarborovým koláčom je koláč drobenkový. Môžeme ju vysmážať v cestíčku...  možností  použitia je nepreberné množstvo. Neukrývate náhodou aj vy  úžasný rebarborový recept po babičke? 
 Pre tých, čo rebarboru poznajú len z pultov predajní  uvediem, že ide o  rastlinu mohutného vzrastu, s veľkými tmavozelnými dlaňovitými listami, s dlhými dužinatými rapíkmi s krásnym červeným žihovaním. Práve rapíky po olúpaní sú tou časťou rastliny, ktorá je určená na konzumáciu.  Rebarboru môžeme mraziť, surovú alebo už tepelne opracovanú. Neskladujme ju dlhšie v chladničke, rýchlo vädne. 
 K rebarbore sa hodí predovšetkým cukor, či med, ale aj  škorica, vanilka, klinček... Chutí aj v jednoduchej úprave, upečená spolu s jabĺčkom, pomarančom, cukrom, škoricou, klinčekom - všetko na kocky a zapiecť asi 20 minút pri 180 stupňoch. Rýchly, zdravý dezert. Môžme postrašiť šľahačkou. 
 Rebarbora  je pre svoju zaujímavú chuť vhodnou prílohou aj k mäsám, napríklad ku grilovaným. Chutí aj k rybe. Môžeme ňou  taktiež nahradiť citrón. 
   
 Tip na blížiace sa obdobie grilovačiek, rebarborové chutney: 
 ošúpané rebarborové rapíky pokrájame na kocky, pridáme pomarančovú šťavu, cukor, trocha pomarančovej kôry, malú čili papričku, šalotku - zmes privedieme k varu. Pri miernej teplote povaríme cca 10 minút, kým hmota nezhustne. Podotýkam, že možností ukuchtiť si domáce chutney je veľmi veľa. Aj prechádzka internetom vám napovie. Rebarbora tejto prílohe dodá tú správnu sviežu chuť. 
 Musím sa priznať, že ako dieťa som skôr obdivovala krásu rebarbory na záhrade, ako si na nej pochutnávala. Moja stará mama - mamina často z nej piekla koláčik, ale mne prekážala  výrazne kyslá chuť. Mala som pocit, že mi sťahuje ústa. Cukor akoby si s jej kyslosťou nevedel poradiť. Preto kvôli mne zvykla na časť koláčika poklásť iné ovocie, aby som si mohla z neho s chuťou uštipnúť aj ja. 
 Prešli roky a ja som sa znovu dostala k rebarborovému koláču. Vychutnala som  si každé sústo. Aj chute sa menia... 
 V Česku volajú rebarboru reveň, Nemci zase der Rhabarber. Ktovie či odtiaľ nevzišla tá naša rebarbora. 
 Pri tepelnej úprave rebarbory by sa nemal používať hliníkový riad! 
   
 *** 
 Rebarbora svojou krásou vyniká 
 z jej kyslosti zbytočná je panika 
 pridaj cukor, zdravší med 
 sviežejšej maškrty niet! 
 *** 
   
   
 Foto: zdroj internet, ktorý vám poskytne aj celú radu receptov, ako premeniť  rebarboru na chutný pokrm či mls. 
 PS: chutney (čítaj čatni) je pôvodné pomenovanie ostrej indickej zmesi z ovocia, byliniek, korenín... 
 PS2: ďakujem za včerajšiu podporu v diskusii, dodala mi chuť a odvahu pokračovať vo "flora-témach" ďalej, verím, že nesklamem. 
   

