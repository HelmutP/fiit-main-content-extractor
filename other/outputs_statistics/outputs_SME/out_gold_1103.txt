

 Čo sa vlastne stalo? Zmenil sa text v zákone kde je definované, čo znamená dať prednosť v jazde. 

 Zákon 315/1996 Z.z. o premávke na pozemných komunikáciách 
§ 2 Vymedzenie pojmov, z 20. septembra 1996, 
d) dať prednosť v jazde povinnosť vodiča počínať si tak, 
aby vodič, ktorý má prednosť v jazde, nemusel náhle zmeniť smer alebo rýchlosť 
jazdy,  


 Zákon (číslo zatiaľ nepridelené, ale je už schválený NRSR a podpísaný prezidentom SR ) Z.z. o cestnej premávke a o zmene a doplnení 
niektorých zákonov, z 3. decembra 2008  
§ 2 Vymedzenie základných pojmov 
(2) Na účely tohto zákona sa ďalej rozumie 
b) dať prednosť v jazde povinnosť účastníka cestnej 
premávky počínať si tak, aby ten, kto má prednosť v jazde, nemusel zmeniť smer 
alebo rýchlosť jazdy, 


 Všimnite si rozdiely. Nový zákon rozširuje osobu z vodiča na akéhokoľvek účastníka cestnej premávky, čo je podľa mňa prospešne. Druhá zmena, podľa mňa, už prospešná nie je. Vypadlo slovo náhle. Odhadujem, že to prinesie komplikácie, nielen preto, že to slovo náhle je súčasťou dohovoru o cestnej premávke podpísaného vo Viedni, ale aj z praktickej najmä právnej aplikácie takéhoto predpisu. 


 Jeden príklad za všetky: 
 Vodič má zaparkované vozidlo kolmo na cestu na vyhradenom 
parkovisku, potrebuje vojsť na cestu. Vodič stojí a dáva prednosť všetkým 
vozidlám idúcim po ceste. Až keď na dohľad vpravo ani vľavo nie je na ceste iné 
vozidlo začne vchádzať na cestu. Lenže v strede vchádzania príde rýchle vozidlo idúce priamo, ktoré má prednosť v jazde pred vozidlom vychádzajúcim z parkoviska. 

 Podľa aktuálnej právnej úpravy, hoci prichádzajúci vodič má prednosť je donútený znížiť rýchlosť ale nie náhle. Vodič minimálne na 
vzdialenosť na ktorú dokáže zastaviť, zbadá vozidlo ako vchádza na cestu, zníži 
rýchlosť, čím umožní druhému vozidlu vchádzanie na cestu dokončiť. 

 V prípade novej právnej úpravy bude môcť prichádzajúci vodič do vozidla vchádzajúceho na cestu v súlade s predpismi nabúrať. Jednoducho 
podľa novej právnej úpravy, ten kto má prednosť dať, je povinný konať tak, aby 
ten kto prednosť má, nemusel meniť rýchlosť alebo smer jazdy, bez ohľadu na to 
akou rýchlosťou ide a či bol vidieť. 

 Jediné ale významné slovo vypadlo, následkom čoho sa stávame právnym rajom pre cestných pirátov. 

