
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alfonz Halenár
                                        &gt;
                Samospráva
                     
                 Ke: S Kositom na večné časy? 

        
            
                                    5.5.2010
            o
            21:25
                        |
            Karma článku:
                3.66
            |
            Prečítané 
            1014-krát
                    
         
     
         
             

                 
                    Kosit a.s. je firma podnikajúca v zbere a zneškodnení komunálneho odpadu (KO). Mesto Košice má 34%-ný podiel v a.s. Apr27 2010 sa poslanci Košíc uzniesli, že bude mať monopol na ďalších 10 rokov v KO. Ako poslanec mesta som dal protinávrh. Aby mesto namiesto predĺženia zmluvy s Kositom oslovilo firmy pôsobiace na trhu s KO v SR a vymenoval som požiadavky, aké majú splniť. Aby komisia vybrala najmenej 2 firmy. Poslanci môj návrh odmietli. Hlasovanie: 9za, 10proti, 25zdrž.
                 

                     Návrh na uznesenie       Mestské zastupiteľstvo v Košiciach podľa § 6 ods. 2 písm. ac) Štatútu mesta Košice       ukladá riaditeľovi magistrátu mesta Košice   a)      vyzvať relevantné obchodné spoločnosti pôsobiace v SR na trhu so zberom komunálneho odpadu, aby v súvislosti s termínom platnosti zmluvy s firmou Kosit predložili svoj ekonomický, finančný a technický plán a projekty na zabezpečenie zberu a zneškodnenia, resp. využitia komunálneho odpadu v meste Košice.   b)      za relevantné obchodné spoločnosti považovať všetky, ktoré v súčasnosti zabezpečujú zber komunálneho odpadu v mestách SR s počtom obyvateľov nad 50 tisíc.   c)      aby vo výzve uviedol nasledovné:   -          projekt musí zodpovedať súčasnej, eventuálne byť pripravený na novú legislatívu v SR, EÚ a trendom v EÚ. Záujemcom oskytnúť všetky k tomu potrebné podklady   -          potrebné podklady pre popis súčasného stavu možno získať   -          želaný stav po spustení projektu znamená stav k zmluvne dohodnutým dátumom. Želaný stav projektu/ov je definovaný takto:    1. stav ihneď po      spustení.       a/ zabezpečuje zberné nádoby každého druhu podľa systému zberu pre domácnosti, pre   podnikateľov, pre záhradkárske a chatové osady, pre triedený zber,   b/ vykonáva odvoz odpadu z domácností a od podnikateľov a komunálneho odpadu   uloženého na stanovisku zbernej nádoby mimo zbernej nádoby (napr. pvc sáčky, vrecia,   kartóny, krabice) zo záhradkárskych a chatových osád, pravidelný /najmenej 2x do roka/   odvoz objemového odpadu,   c/ vykonáva pravidelnú očistu plochy stanovišťa zbernej nádoby,   d/ prevádzkuje zberné dvory pre všetky druhy komunálnych odpadov skupiny 20 Katalógu   odpadov a pre drobné stavené odpady,   e/ zabezpečuje zhodnocovanie a zneškodňovanie odpadov v súlade so zákonom a ďalšími   všeobecne záväznými právnymi predpismi,   g/ monitoruje a kontroluje vhodnosť zberných nádob a stanovísk zberných nádob a oznamuje   písomnou formou alebo elektronickou formou zistené skutočnosti objednávateľovi na   príslušné oddelenie Magistrátu mesta Košice.   h/ ďalšie činnosti podľa požiadavky objednávateľa.   i/ trvanie zmluvy s dodávateľom bude zodpovedať ekonomickému, finančnému a technickému plánu projektu.       2. priebežný stav:   a/cena jednotlivých služieb od dodávateľa bude tvoriť celok, ktorý bude podliehať posudzovaniu mestom Košice. Dohodnutá cena bude platiť jeden rok.   b/ zabezpečenie zberu jednotlivých komponentov komunálneho odpadu - biologického, papiera, skla, plastov a časti z výmien vnútorného vybavenia bytov   c/  zabezpečenie trendu zvyšujúceho sa objemu a váhy zberu separovaného odpadu spracovaného dodávateľom. To bude slúžiť ako podklad pre prípadné ďalšie predĺženie zmluvy, spolu s vyhodnotením plnenia zmluvy a jej výhodnosti pre mesto.   d/ kontrola služieb dodávateľa zabezpečovaná ako jeho pracovníkmi, tak organizáciami z oblasti starostlivosti o životné prostredie s poverením od mesta.   e/ dodávateľ umožní mestom poverenej inštitúcii pravidelné aj námatkové kontroly dohodnutých parametrov aj v tých priestoroch a zariadeniach, ktoré používa na zabezpečenie zmluvy.       Bolo rozhodnuté, že projekty vyhodnotí komisia.   Zloženie:  jeden predstaviteľ magistrátu mesta Košice, jeden člen komisie Životného prostredia, dvaja poslanci za koalíciu KDH/SDKÚ-DS/SMK, jeden za ostatných, jeden člen delegovaný primátorom mesta, jeden z tímu ktorý sa zúčastníl projektu predĺženia zmluvy s Kositom.       Poverenie: komisia z predložených projektov vyhodnotí a rozhodne o funkčnom modeli pre zber a znehodnotenie komunálneho odpadu pre mesto a to najmenej od dvoch dodávateľov. Toto vyhodnotenie predloží verejnosti aj poslancom na ďalšie posúdenie riadnym postupom, ako pri schvaľovaní bodov rokovania MZ.       Začiatok a ukončenie práce komisie: 15.4. 2010, 15.10. 2010       Termín: 15.10.2010, zodpovedný: riaditeľ MMK.   Priebežné termíny zabezpečuje riaditeľ MMK a predseda alebo poverený člen komisie po jej vytvorení.           predložil A. Halenár                                                                                      Košice apr12 2010         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Hliadka MsP na stanovišti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Mestský zákon o informovaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: možná korupcia na MZ feb23_2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Sneh a psí trus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            SR: Prečo budem voliť Radičovú
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alfonz Halenár
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alfonz Halenár
            
         
        halenar.blog.sme.sk (rss)
         
                                     
     
        Pracovník IT, poslanec Košíc a poslanec Košického kraja.
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    114
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2932
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Samospráva
                        
                     
                                     
                        
                            Demokracia
                        
                     
                                     
                        
                            Komunisti okolo nás
                        
                     
                                     
                        
                            Politické strany
                        
                     
                                     
                        
                            Aktuálne reakcie
                        
                     
                                     
                        
                            Poslanci Košíc
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Barbara Masin: Odkaz
                                     
                                                                             
                                            Štuktúry moci na Slovensku 1948-1989
                                     
                                                                             
                                            Josef Frolík: Špion vypovídá
                                     
                                                                             
                                            Margaret Thatcher: Umění vládnout
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Black Eyed Peas: I´ve got a feeling
                                     
                                                                             
                                            Faustas blog (skvelý podcast)
                                     
                                                                             
                                            BBC podcasty
                                     
                                                                             
                                            ČRo: Lidé pera (podcast)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Barbara Masin: Gauntlet (anglicka verzia knihy)
                                     
                                                                             
                                            D-FENS (politika, spoločnosť,...)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




