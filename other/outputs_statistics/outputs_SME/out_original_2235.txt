
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tatiana Melisa Melasová
                                        &gt;
                Všeobecné
                     
                 Moje fakty a úvahy  o prostitúcii 

        
            
                                    8.2.2010
            o
            22:23
                        |
            Karma článku:
                14.57
            |
            Prečítané 
            10544-krát
                    
         
     
         
             

                 
                    Rada by som napísala, že sú to fakty, ale nejde to. Nie sú to vedecky podložené skúmania, iba moje osobné zážitky a pozorovania. Nerada ľudí súdim a škatuľkujem, možno práve preto, lebo mňa odsúdili a zaškatuľkovali nespočetne veľa krát. Tu sa tomu však nevyhnem, aby to bolo prehľadné. Je toho strašne veľa a tak to budem písať na pokračovanie, lebo do jedného článku by sa to nezmestilo. Tento prvý budem venovať práve ženám a dievčatám, ktoré túto prácu vykonávajú.
                 

                 Je až zarážajúce, ako sa ľudia stavajú k tejto profesii. Odmietajú ju akceptovať ako súčasť spoločnosti, zatvárajú pred ňou oči a tvária sa, že neexistuje. A pritom väčšina z tejto spoločnosti využíva jej produkty. Či už na strane „výrobnej alebo spotrebiteľskej“.   Ženy, ktoré vykonávajú takúto profesiu ( či už každodenne alebo len príležitostne), to poctivo zakrývajú a taja. V iných odvetviach, sa každý rád pochváli, čo dosiahol. Tu nie. Nijaká žena neprehlási:„ Dobrý deň, som prostitútka a dnes som uspokojila dvadsať chlapov.“ Uspokojiť toľko chlapov bez ujmy ( či už svojej alebo jeho), tiež považujem za nadľudský výkon. O prostitúciu ide vtedy, ak aspoň v mojej terminológií, dôjde k vzájomnej dohode zúčastnených strán. Za úplatu, poskytnem sexuálne služby. Nemusia to byť vždy peniaze (drogy, šperky,...), ale musí to byť vždy sexuálne uspokojenie. Každá strana si háji to svoje, aj keď to veľa krát vypáli celkom inak. No o tom inokedy. Teraz už k spomínaným typom žien.   Je nad slnko jasnejšie, čo motivuje ženy, aby sa venovali prostitúcií. Sú to peniaze, myslím že to nikomu netreba ozrejmovať. Dôležitým faktorom je, z akého dôvodu sa rozhodnú pre výkon tejto profesie. A práve ten nás rozdeľuje do rôznych skupín a delí nás na typy, akými sa staneme.  Obetavé mamy a dcéry  Tieto typy, ja strašne obdivujem, lebo ich k prostitúcií donútili životné okolnosti a nejdú za vidinou vlastného bohatstva. Sú to väčšinou ženy nad tridsaťpäť rokov a v krajine kde bývajú trpia chudobou, preto sa rozhodnú ísť do inej krajiny, aby uživili svoje deti. Ich situácia je bezútešná, pretože nemajú platné víza a nikto im poriadnu robotu nedá. A tak im neostáva nič iné ako prostitúcia, lebo tam teda ozaj nevyžadujú platné víza. Ale postupom času sa dá zarobiť na právnika, aj na papiere a tak si potom aj tak nájdu „slušnú“ robotu.   Ako mi veľa z nich povedalo:„ Ja som už stará, ja na toto nemám žalúdok!“ Postupne sa z nich stávajú alkoholičky, aby si aspoň trocha otupili zmysly. Drogy neberú, lebo tie stoja peniaze a peniaze sa hodia deťom. Nič si na seba nekupujú a skoro všetko posielajú domov, aby ich deti mali lepší život, ako ony sami. Deti za tie peniaze študujú a posielajú mamine fotky, ktorá ich potom pyšne ukazuje všade naokolo.  Do tejto skupiny patria aj dcéry, ktoré sa rozhodli zachrániť rodinnú situáciu, z toho istého hľadiska. Sú mnohopočetná rodina a nikto z nich nepracuje. Ako ony sami hovoria, nie preto, že by nechceli ale nemôžu si nájsť robotu. Takisto posielajú všetko, čo zarobia domov, akurát sa od tých mám líšia tým, že majú o pätnásť rokov menej. Sú dosť naivné a od klienta sa veľa krát vracajú s plačom. Sú dobrými kandidátkami stať sa  jednou z nasledujúcej kategórie, pretože postupne prídu o všetky ilúzie.   Bezcitné suky  Aj túto skupinu obdivujem, možno práve preto, že ja som sa nikdy nestala jednou z nich. Patria sem dievčatá, ktoré presne vedia za čím idú. Ich pohnútky sú zväčša psychického charakteru, ktoré by som aj vedela špecifikovať, ale to by bolo na dlho a tento článok by nemal konca-kraja. Klientov berú ako nepriateľov a snažia sa ich čím skôr zneškodniť. Ako inak než finančne.   Aj z neperspektívneho klienta, dokážu vyťažiť maximum a to takým spôsobom, že to dotyčný oplakáva najmenej mesiac. Nemusia byť najkrajšie, zato presne vedia, čo na chlapa platí. Pijú drahý nealkoholický nápoj, neberú drogy, nestrácajú čas a zachovávajú si čistú hlavu. Presne vedia koľko môžu z každého vyťažiť. Ich heslo znie:„Využi chlapa skôr, ako on využije teba!“. Sú bezcitné a idú aj cez mŕtvoly, len aby dostali to svoje.   Narkomanky  Bohužiaľ do tejto skupiny som nikdy nechcela patriť, no a práve tu som sa zaradila. Čo napísať?! Myslím, že je tu akýkoľvek komentár zbytočný. Snáď len toľko, že pre prostitúciu sme sa rozhodli preto, aby sme sa nemuseli živiť krádežami a podvodmi. No jedno nevylučuje druhé.   Väčšinou sa zdržujeme na ulici, lebo klubová prostitúcia nás obmedzuje v našej závislosti. Hodnotu peňazí v podstate nepoznáme pokiaľ si to neprerátame, koľko dávok z toho budeme mať. Väčšinou sme v zlom psychickom aj fyzickom stave, čo je na nás aj patrične vidieť.  Zvláštnu kategóriu tvoria klokanky. Sú to dievčatá, ktoré sú spráskané pod obraz boží a stoja v jazdnej dráhe vodiča, v pozícií klokana. Spia postojačky a klientovi sa môže stať, že mu baba zaspí počas orálneho aktu priamo na penise. Výhodou však ostáva, že už nemáme ilúzie, o ktoré by sme prišli. A to je strašné plus!   Príležitostné šľapky  Táto kategória je asi najhoršia, pretože ako už z názvu vyplýva, vykonávajú to iba príležitostne. Tým pádom nie sú také zničené, ako my ostatné. Väčšinou sú mladé, atraktívne a jediné, čo ich naozaj zaujíma, je čo najskôr nájsť dobrého dojaceho koreňa.   Patria sem najmä študentky, mladé mamičky a tanečnice. Veľa z nich prehlasuje, že to robí z nudy. No ja neviem, ale nechce sa mi tomu veľmi veriť. Môžu si nájsť veľa iných koníčkov a prostitúciu by som rozhodne nepovažovala za koníček z nudy. Sem patria aj maloleté prostitútky, čo utiekli z detských domovov a ústavov. Kým ich policajti pochytajú, tak sa väčšinou predávajú na ulici.  Ešte je veľa iných skupín prostitútok a aj každá z vyššie uvedených skupín má ešte podskupiny. Zo spoločenského hľadiska, by som sa zato mala hanbiť. Nehanbím sa, nikomu som neubližovala, iba ak tak sama sebe a svojej rodine. Minimálne sa nehanbím priznať si to a rozprávať o tom.   Za iné veci, ktoré som spravila pociťujem a budem  pociťovať hanbu. V nasledujúcom článku sa budem venovať klientom, ktorí tieto služby využívajú. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (484)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcii (3)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcií 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Verona
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Deti a rodičia drogy spolu nezničia (aspoň u nás)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Nikdy sa všetkým nezavďačíš
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tatiana Melisa Melasová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tatiana Melisa Melasová
            
         
        melasova.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8108
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Všeobecné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            DROGY-SOS
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




