
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maximos Dragounis
                                        &gt;
                Nezaradené
                     
                 Peloponéz, zázračná grécka krajina 

        
            
                                    20.3.2010
            o
            22:16
                        (upravené
                21.3.2010
                o
                2:56)
                        |
            Karma článku:
                7.56
            |
            Prečítané 
            2680-krát
                    
         
     
         
             

                 
                    Peloponéz (po novogrécky Peloponnisos), do 19 stor. u Grékov známy hlavne pod menom Moreas, je najjužnejšia časť gréckej pevniny, je to miesto s bohatou históriou a nádhernou kultúrou, ktorej korene sú staré tisícky rokov. Nachádzali sa tu významné starogrécke mestá ako Sparta, Mykény, Tiryntha, Korinthos,  Olympia, Megalupoli, Argos a Nemea. Peloponéz od staroveku sa delí na nasledújúce kraje: Achaia (sever), Korinthia (severovýchod), Argolida (severovýchod), Arkadia (stred), Messinia (západ) a Lakonia (juh). Povrch je hornatý, nachádzajú sa tu pohoria Taugetos, Erymanthos, Chelmos, Parnonas a Panachaiko oros. Hlavné mesto Peloponézu je Patra, na severnom pobreží Achaje. Iné mestá sú tu Tripolitsa (Tripoli), Aigio, Nauplio, Korinthos, Sparti, Krestena, Pyrgos, Mesini či Kalamata. Celý polostrov má 1 100 000 obyvateľov.
                 

                 História Peloponézu:   História tohto nádherného gréckeho kraja je veľmi bohatá a plná napínavých príhod. Územie Peloponézu je kontinuálne osídlené už od prehistorickej doby. Prví obyvatelia boli Pelasgovia, mýtmi opradený a tajomný národ, ktorý žil na území Grécka predtým, ako sem prišli grécke kmene. Peloponéz bol centrom Pelasgov a pravdepodobne sa nazýval Pelasgia. Neskôr dostal meno podľa bájneho hrdinu Pelopa. Peloponéz znamená Pelopov ostrov.   Prví Gréci, ktorí sa usadili na Peloponéze boli Achájci, tvorcovia prvej gréckej kultúry, Mykénskej kultúry. Achajci sa tu usadzovali od roku 3000 pred Kr. Pelasgovia sa udržali vo vnútrozemí polostrova. Po páde Mykénskej civilizácie (cca 1200 pred Kr.), sa na Peloponéz dostáva nový bojovný grécky kmeň, Dóri. Dóri sa usadili vo veľkej časti Peloponézu a podrobili si Achajcov, známi starovekí klasickí Sparťania boli Dóri. Mnoho Achajcov sa vtedy presídlilo do hornatej vnútrozemskej Arkádie a tak jedine tento kraj nebol osdílený Dórmi. Zanikajú aj Pelasgovia, ktorí sa s Grékmi definitívne pomiešali. V klasickom staroveku tu bola vyspelá grécka kultúra, dórska Sparta bola mocným rivalom iónskych Atén a bol to známi vojenský štát. V Olympií sa konali Olympíady, kde súťažili všetky grécke štáty. V Epidaure sa nachádzalo veľké divadlo s výbornou akustikou, ktorú môžeme obdivovať dodnes. V 3 stor. pred Kr. Peloponéz ovládol silný severogrécky národ- Macedónci pod vedením Filipa Jednookého a jeho syna Alexandra Veľkého, ktorí využili situáciu na zjednotenie Grécka, pretože najmocnejšie štáty Atény a Sparta boli vyčerpané vzájomnými bojmi v Peloponézskej vojne.   V 2 stor. si Peloponéz podmanili Rimania, keď porazili spojenecké vojská, Sparta si však uchovala určitý stupeň nezávislosti. V rímskej dobe pokračovala doba plná prospechu a vzdelanosti, Achajci a Dóri sa miešali aj v samotnej Sparte, hornatá Arkádia si stále uchovala povesť pokojného pastierskeho miesta, kde pastieri žili priam až filozofický život. Tak sa pre Rimanov pojem Arkadia stalo synonimom pre miesto pokoja (Et in Arcadia ego-Vergilius). Peloponézčania postupne prestupovali na kresťanstvo, síce hornatá Arakdia či polostrov Mani na juhu si stále uchovávali pohanské kulty. Medzi 3-5 stor. Peloponéz vyplienilo mnoho barbarských kmeňov-Góti, Herulovia a Huni. Od roku 395 patrí do Východorímskej (gréckej Byzantskej) ríše.   Obdobie pokoja a Vergiliovskej idilky nám definitívne končí v 6 stor. Byzantskú dunajskú hranicu totiž prelomili Slovania, ktorí prenikli na celý Balkán a aj do Grécka, predovšetkým však sa usadzovali na Peloponéze. Slovania sa usadili na západnej strane Peloponézu, východ ovládala Byzancia. Slovania si podmanili grécke obyvateľstvo. Mnohí Peloponézania zo zúfalstva odchádzali do byzantských provincí na Sicíliu a do južnej Itálie, kde v tej dobe žili Gréci. Sparťania sa uchýlili na juh Peloponézu, na polostrov Mani a na juhovýchod (Tsakonia), kde založili mesto Monemvasia. Až keď sa byzantským Grékom podarilo ukludniť východnú-Perzskú hranicu, mohli sa venovať situáciou na Balkáne. Generál Staurakios a armáda cisárovnej Ireny viackrát zaútočila na gréckych Slovanov, podrobila si ich a odvážala ich na východnú anatólsku hranicu. Nespokojnosť Slovanov s týmto riešením vyústilo začiatkom 9 stor. do obrovskej vzbury na Peloponéze. Slovania vylúpili mnohé grécke dediny a zaútočili na dobre opevnené grécke mesto Patra. Tu však Byzantský cisár Nikiforos Slovanov úplne zničil. Na rozkaz cisára bolo takmer celé slovanské obyvateľstvo Peloponézu presídlené do východnej Anatólie, kde muži slúžili vo vojsku. Ostal tu iba malý slovanský kmeň, ktorý sa utiahol do vysokých pohorý. Nikiforos prikázal, aby na Peloponéz prišli grécke rodiny zo Sicílie, Itálie a Anatólie a aby tak posilnili peloponézske obyvateľstvo, ktoré po odchode Slovanov ostalo zdecimované. Znovu tu boli vytvorené cirkevné územia a definitívne bol pokresťančený aj zbytok gréckeho obyvateľstva Arkádie a Mani.   Potom už nastáva obdobie pokoja  a prosperity. Bolo tu postavené nádherné mesto Mystras, kde stoja mnohé byzantské kostoly.  V roku 1204 padla Byzantská ríša, keď ju dobyli križiaci a o rok neskôr vtrhli na Peloponéz, kde pri Kundurských olivových hájoch zničili armádu peloponézskych Grékov. Peloponéz tak ovládli Európania-Benátčania. Taliani sa snažili zaviesť feudálny spôsob vlády, ktorý však Gréci nepoznali. V tejto dobe bol pogréčtený aj ten zbytok Slovanov, ktorých Taliani odvliekli z hôr na roviny, kde sa s väčšinovými Grékmi pomiešali. Naopak, mnohi Gréci ušli pred feudalizmom do hôr a prevýšili tu malý počet Slovanaov, ktorí zmyzli aj tu. Taliani priviedli na Peloponéz aj Albáncov, usadili ich hlavne v Argolide ale tiež v malých oblastiach Achaie, Messinie a Lakonie. Albánci-Arvaniti sa však pokladali za Grékov, kedže boli ortodoxní kresťania. Arvaniti žijú v týchto územiach dodnes, udržali si svoj albánsky jazyk, no necítia sa Albáncami, sú to Gréci. Arvaniti tak od tejto doby tvoria približne 8% peloponézskeho obyvateľstva. Peloponéz nakoniec opäť ovládla znovuvzniknutá Byzancia. V tejto dobe tu znova prekvitala kultúra, v meste Mystra pôsobil snáď najznámejší grécky filozof byzanstkej doby-Jeorjios Jeimstos Plithon.   V 15. stor. však kraj ovládli osmanskí Turci. Niekoľko Peloponézanov následne prestúpilo na islam a tak sa z nic stali Turci. Táto turecká komunita sa na Peloponéze udržala až do 19. stor. Peloponézski Gréci odmietali tureckú nadvládu, hlavne bojovní Manioti (potomkovia Sparťanov). V 17. stor. tu vniklo povstanie, ktoré však Turci potlačili. Grécka vojna za nezávislosť začala v roku 1821 práve na Peloponéze, v kláštore Ajia Laura (Sv. Laura),  kde biskup Jermanos vztýčil grécku vlaku. Peloponézskych Grékov viedli generáli Theodoros Kolokotronis, Petros Petrobeis Mauromichalis, Nikitaras, Papaflessas, Kunduriotis a Ypsilandis. Kolokotronisova armáda dobyla hlavné turecké sídlo, mesto Tripolitsa, neskôr Kolokotronis porazil veľkú tureckú armáda Dramaliho Pašu v bitke pri Dervenakií. Keď sme začali revolúciu nemali sme ani zbrane a jeden sa nás spýtal "aide, kam to idete bojovať bez zbraní?" Ale my sme išli a zvíťazili sme! To sú slová generála Kolokotronisa z jeho pamätí.  Keď sme dobyli Tripolitsu na námestí som uvidel platan (symbol Grékov), pozrel som sa na neho a hovorím: "aide more, koľko ľudí z mojej krvy, koľko Grékov bolo obesených na tomto platane", povedal som a prikázal som ho vyrúbať. To sú ďalšie slová vekého generála Kolokotronisa. Mnohí moslimovia boli vyvraždení a zbytok z Peloponézu ušiel. Turci museli poprosiť o pomoc Ibrahima Pašu, vládcu Egypta, ktorý na Peloponéz vyslal egyptskú armádu. Generál Papaflessas s 1000 Grékmi sa v bitke pri Maniaki postavil proti 6000 Egypťanom, no bol tu porazený, žiaden Grék však nezutekal, a do jedného aj s Papaflessasom hrdinsky padli. Až zákrok mocností znamenal prevrat a Grécko získalo nezávislosť. Od roku 1831 je tak toto nádherné územie Hellady opäť slobodné. Počas 20. stor. sa mnoho Peloponézanov presťahovalo do Atén, takže 1 100 000 obyvateľov neznamená skutočný počet Grékov peloponézskeho pôvodu.   KULTÚRA-ľudové tradície, zvyky, hudba a informácie o Peloponézanoch   Stin Rumeli ke ston Moria oli, more, oli chorevun tin itia, toto sú úvodné slová gréckej ľudovej piesne Itia (Vŕba), ktoré v preklade znamenajú: V Rumélií (Stredné Grécko) aj v Morei (Peloponéz), všetci tancujú vŕbu (tanec). Táto pieseň nám hovorí o tradičnej kultúre Peloponézanov (gr. Moraites), ktorí sú známi svojimi tancami a množstvom ľudových piesní. Najviac známi svojimi piesňami sú obyvatelia stredného Peloponézu, Arkádie. Bohatý folkór Arkádčanov bol známi už pred 2500 rokmi, keď miestnu kultúru prezentoval tancujúci boh Pan, napol koza, napol človek. Jeho divoké melódie a rýchle tance na Peloponéze nájdeme aj dnes. Peloponézska ľudová hudba a tance majú pôvod už v antickom Grécku. Melódia je v orientálnom štýle, ale nie je orient ako orient. Peloponézske orientálne melódie nemôžeme porovnať tým anatólskym, alebo arabským. Piesne spievajú o dievčatách, víne alebo platane. Najznámejšie peloponézske ľudové piesne sú Stis Arkadias to platano (Pod arkádskym platanom), Mes´t´ambeli (Vo vinici), Ti na se kano galani, na jinis mauromata (Čo mám s tebou spraviť zelenooká, aby si mala čierne oči?), Kokkina chili filisa (Červené pery som pobozkal) alebo Saranda varkes sto jialo (40 ľodiek v prístave).  Peloponézske tance, rovnako ako aj ostatné grécke tance sa tancujú do kruhu. Najznámejšie miestne tance sú Syrtos, Kalamatianos (od mesta Kalamata), Tsamiko a Tsakonikos.  Tance majú tiež staroveký pôvod. Veľmi populárne je práve Tsamiko. Tento tanec sa tancuje v celom pevninskom Grécku. Je to akrobatický tanec, ktorý tancuje jeden muž, ktorého pridřža druhý pomocou šatky.  Tanec Tsakonikos sa vyvinul zo starovekého spartského bojového tanca a tancujú ho Tsakónski Gréci. Peloponézania slávia mnohé sviatky, na ktorých neustále tancujú, spievajú a hodujú. Tieto tradície  pochádzajú tiež z predkresťanských čias, keď sa oslavovali rôzni bohovia. Dnes, naopak, sú tieto sviatky venované rozličným svätým.   No a aké sú peloponézske tradičné ľudové nástroje? Predovšetkým je to klarina (klarinet), a v pozadí hrá brnkacia udi (orientálna mandolína), ktorá dodáva melódií špecifický podtón. Klarinet do Grécka prišiel až v 19 stor. a nahradil do vtedy používaný nástroj tzuras, ktorý bol podobný klarinetu a vychádzal zo starogréckeho nástroja aulos.   Neodmysliteľným symbolom Grékov je už pár tisícročí strom platan (po gr. Platano). Už staroveký spisovateľ Thukidides v 5 stor. pred Kr. spomína uctievanie platana ako starý grécky zvyk. Platan je dnes uctievaný hlavne na Peloponéze a snáď neexistuje jedna dedina, v ktorej by v strede námestia tento strom chýbal. To je jedna z mnohých pohanských tradícií, ktorých sa Gréci odmietli vzdať. Iná tradícia je oslavovanie Veľkej noci (gr. Pascha). V Peloponézskom meste Tripolitsa (Tripoli) sa koná najznámejšia  grécka veľkonočná slávnosť v ktorej pretrvávajú veľmi silné pohanské zvyky. No a ako vyzerá taký tradičný Peloponézan? Dnes celkom rovnako ako hociktorý iný Európan, no ešte do polovice 20. stor. to bolo inak. Tradičný mužský kroj pozostával z bielej suknice Fustanella, košely so širokými rukávmi (Pukamiso)  a  tradičnej čapice s názvom fesi (teda fez). Ženský kroj pozostáva z dlhej sukne, košele a trblietavých ozdôb.   Eleni, to je tradičné ženské peloponézske meno  a po slovensky je to Helena. Pre Peloponézanov je meno Eleni stelesnením krásnej trósjkej Heleny (ktorá bola tiež Peloponézanka). Peloponézania si toto meno nesmierne ctia, čo si môžeme vypočuť v jednej piesni, Ach Eleni, ise zachari, ise meli (Ach Helena, si cukor, si med).   Poďme si povedať niečo o pôvode dnešných Peloponézanov. Dnešní Peloponézania sú výsledkom miešaní pôvodných tajomných Pelasgov, starogréckych kmeňov: Dórov, Achajcov a možno aj Iónov, Grékov, ktorí na Peloponéz prišli v 9. stor. po Kr.  z južného Talianska, Sicílie a Anatólie, aby osídlili po Slovanoch vyľudnené oblasti. Peloponézania vstrebali aj zbytok slovanského obyvateľstva a tiež talianskych kolonistov, ktorí sem prišli v 13. stor. Nesmieme zabúdať ani na albánskych Arvanitov, ktorí sem tiež prišli v tejto dobe a dodnes si uchovali svoj jazyk, no sú to hrdí Gréci. Arvaniti tiež asimilovali pôvodné, grécke obyvateľstvo na územiach, kde sa usadili. Nemôžeme vynechať bojovných Maniotov, priamých potomkov antických Sparťanov, ktorí však od slovanskej invázie žijú na polostrove Mani (južne od Sparty). Manioti sú veľmi divokí a aj preto Turci nikdy nedokázali ovládnuť Mani. Tradičné maniótske dediny nemajú obyčajné domy, ale obyvatelia žijú vo vežiach a to preto, lebo v minulosti tu boli tradičné boje medzi najsilnejšími rodmi-Vendetta. Hovorí sa, že mnohí Turci vstúpili na Mani, no nevrátil sa odtiaľ ani jeden.  Nikdy Maniota neurazte, lebo potom zabudne na tradičnú filoxeniu (pohostinnosť) a z dediny by vás vyhnal. Inak sú to veľmi priateľský ľudia a prec cudzinca spravia hocičo.  Ďalšími potomkami starovekých Sparťanov sú Tsakončania, Gréci, ktorí žijú v arkádskom prímorskom kraji Tsakonia. Tsakóni sem tiež prišli počas slovanskej okupácie a dokonca dodnes hovoria gréckym jazykom, ktorý sa vyvynul zo starogréckej dórčiny, ostatné moderné grécke jazyky sa vyvynuli z iónčiny. No ak ak niekoho zaujíma ako Peloponézania fyzicky vyzerajú, tak sú podobní ako ostatní Gréci, často majú tmavšie vlasy a oči.   Nedá mi, nezmieniť sa ešte raz o Slovanoch, ktorí na Peloponéze žili viac ako 200 rokov a síce boli skoro všetci presídlení, dodnes tu ostali mnohé slovanské názvy. Mnohé dediny majú slovanský názov, alebo aspoň mali do 20 stor. kedy im boli vrátené pôvodné, starogrécke mená. Dediny so slovanskými menami sú Stemnitsa, Strezova, Zatuna, Mazeika, Verzova, Polovitsa, Kamenitsa, Arachova, Levetsova, Topolova či Lechana. Aj pohorie Chelmos má svoj názov zo slovanského jazyka a dokonca aj mesto Aigio sa do 19. stor. volalo Vostitsa a bolo spätne premenované antickým menom.    Na Peloponéze sa dorába aj najkvalitnešie grécke červené víno. Dorába sa na vysokopoložených viniciach v meste Nema v Argolide, kde mýtický Herakles zabil nemejského leva. Toto víno sa nazýva Ajiorjitiko (Svätojurajské), lebo Nema sa až do 19 stor. volala Ajios Jeorjios (Sv. Juraj) a bola premenovaná na svoje staroveké meno. Kvalitné víno sa dorába aj v kraji Mandinia v Arkadii a tiež v Messinii.   No čo treba vidieť keď prídete na Peloponéz? Rozhodne staré Mykény, divadlo Epidauros, ruiny Sparty, opustené byzantské mesto Mystras plné kostolov, prímorské mesto Monemvasia a tradičnú vidiecku zábavu-Panijiri. Tolko k mojej krajine, dúfam že sa vám páčila a že ju niektorí z vás aj navštívia.              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (37)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok Gréka XI-Grécke voľby-bezvládie, pat a čoskoro ďalšie voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Rembetiko, hudobný štýl gréckeho podsvetia a gréckych gangstrov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka X: Prešiel úsporný program, rozpad parlamentu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného GrékaIX-Nová pôžička, rozpad koalície a grécke voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka VIII.-Historická genéza gréckej krízy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maximos Dragounis
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maximos Dragounis
            
         
        dragounis.blog.sme.sk (rss)
         
                                     
     
        Volám sa Maximos, pochádzam z Grécka. 
Facebook:
http://www.facebook.com/pages/Maximos-Dragounis-Gr%C3%A9cko-bez-propagandy-%CE%97-%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1-%CF%87%CF%89%CF%81%CE%AF%CF%82-%CF%80%CF%81%CE%BF%CF%80%CE%B1%CE%B3%CE%AC%CE%BD%CE%B4%CE%B1/103882279704191?sk=wall
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1935
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




