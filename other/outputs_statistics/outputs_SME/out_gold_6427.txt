

 Ak by sme ich výrazne zredukovali, čo by sa stalo? Skoro nič. Až na to, že by sa zistilo, že veľa úradníkov a úradov je úplne zbytočných. Preto sú byrokrati všetkých krajín takí usilovní v obhajobe vlastnej existencie.  Vo vyrábaní predpisov a noriem, ktorých dodržiavanie potom kontrolujú. Sú tvorcami práce pre seba.  Oni potrebujú tie mraky predpisov, nie občania. Potrebujú to preto, aby sme ich mohli z našich daní platiť a oni nás za to s obľubou, v mene štátu šikanujú. 
 Aby sa nikto neurazil. Nie je to vec osobná, nie je to úchylka, či povahová vada. Je to chyba systému alebo jeho prirodzený sklon kumulovať moc do rúk byrokracie. A aj takto, ticho, nenápadne a plazivo, oberať  občana o slobodu. 
 Nemusí to tak byť. Keď si ten problém uvedomíme, môžeme proti nemu bojovať. Pokiaľ máme politikov, ktorí sú o tom presvedčení.  Nemusíme tlieskať každému predpisu, ktorý si úradníci v mene krajších zajtrajškov vymyslia. A je pritom úplne jedno, či sú to naši domáci byrokrati alebo európski. Dnes asi 80% legislatívy je vyvolanej úpravami a zosúlaďovaním s Bruselom. Je to vysoké číslo. Netvrdím, že všetko je nezdravé. Ale mnohé z toho je diskutabilné a často našimi domácimi predkladateľmi  snaživo sprísnené. Veď prečo nebyť pápežskejší od pápeža? 
 Podnikateľské prostredie, ale nielen ono,  sa tak zaplienilo normami s množstvom povinností všetkého druhu. Hygienické, požiarné, zdravotné, archivačné, stavebné,... .  A všetky rastú, púchnu, zmnožujú sa. Koľko povinností a povolení z nich vyplývajúcich sú zbytočné, obmedzujúce a komplikujúce život ? Množstvo. Koľko potvrdení musí krížom krážom človek predkladať, aby dostal potrebne pečiatky? More. A koľko duplicitne? Každý úrad chce svoje. Akoby sme nemali internet. Akoby sme nežili v informačnom veku. 
 Potrebujeme preto zákon proti šikanovaniu občanov štátom. Zákon, ktorý zavedie základnú povinnosť pre úrady: 
 Ak raz predložím potvrdenie o čomkoľvek jednému úradu, nemusím tak na ďalšom úrade, ale ten si to sám zistí a zabezpečí,  pretože bude každý úrad prepojený  s iným navzájom. Žiadosti bude postačujúce podávať internetom, a tak budú aj vybavované, v krátkych a presne určených lehotách. A bez úplatkov. 
 Potrebujeme tiež zoškrtať aspoň 50% predpisov, noriem a vyhlášok upravujúce všetky oblasti  ekonomických činností. Ako rozsahom, tak aj množstvom povinnosti. Nech tam zostane len to, čo je naozaj nevyhnutné a minimálne. Všetko ostatné preč. 
 Bude sa nám lepšie dýchať, zlepší sa podnikateľské prostredie, budú sa ľahšie tvoriť pracovné miesta, zníži sa administratíva, zníži sa korupcia a úradníci budú milší a príjemnejší. Nie je to ľahké ani jednoduché, ale dá sa to.  S ľavicou sa to však  ešte nikde  nepodarilo.  Naopak. Keďže ľavica rada riadi životy občanov, posilňuje byrokraciu a jej moc , oberá občanov o slobodu a ťahá nám z vrecka peniaze. Nedovoľme jej to. 
 Viaceré aktuálne témy preberáme na mojom Facebook profile www.facebook.com/ludokanik. 
 Pre tých, čo radi sledujú krátke správy som aj na Twitteri - www.twitter.com/ludokanik 
 Všetky informácie o mne sa sústreďujú na mojej webstránke -  www.kanik.sk 
   
 Ľudo Kaník 
 Kandidujem do NR za SDKU-DS pod č. 30 
   

