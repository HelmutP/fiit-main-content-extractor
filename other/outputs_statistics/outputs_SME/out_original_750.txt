
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Basila
                                        &gt;
                Momentky
                     
                 Ako som sa takmer stal majstrom ČR v triatlone 

        
            
                                    16.7.2008
            o
            16:39
                        |
            Karma článku:
                17.96
            |
            Prečítané 
            13349-krát
                    
         
     
         
             

                 
                    V druhej triede na základnej škole som ukončil plavecký kurz prevzatím korytnačky. Slabšie hodnotenie už bohužiaľ neexistovalo, čo veľmi škrelo našu pani učiteľku. Za moje strnulé prerúčkovanie predpísanej polovičky bazénu s  výrazom čerstvo vyliahnutého mamuta by mi pani učiteľka najradšej udelila diplom v tvare nákovy.
                 

                Zato beh mi šiel naozaj famózne. Všetky disciplíny som vyhrával s veľkým náskokom a pokiaľ ma pamäť neklame spomínam si, že som raz zabehol dvanásťminútovku pod desať minút.   Aby som potvrdil moje kvality cyklistu spomeniem len krátky a poučný príbeh o tom, ako som vyrazil na sestrinom horáku z Trenčína do Tatier. Do uší som si vložil slúchadla zo superprenosného rádia, rádio som pichol do karimatky, ktorú som mal omotanú okolo rúry bicykla a v príjemných melódiách som si to šliapal do pedálov. Pri Ružomberku ma náhle zastihla jemná bolesť v chrbte. Natiahol som sa teda, povystieral ubolené cyklistické telo a nevšimol si, ako sa rádio po kúskoch vyťahuje z karimatky a jemným oblúkom letí do predného kolesa.   Boli to milisekundy, v ktorých sa človek naozaj nestíha čudovať, prečo ho niekto chytil z vnútra za uši a celou silou mu treskol hlavou o riadidlá bicykla. Neverili by ste, ale kábliky od slúchadiel vydržia nenormálne veľa. Ležal som v jarku vedľa cesty medzi tými jablkami, kukal som na tú oblohu modrú a otrasený mával kamionistom, ktorí si naplno užívali toto malé spestrenie nudného kamionistického života.     Netrvalo dlho a dospel som. Postupne som si uvedomoval svoje športové danosti a po krátkej diskusii sme s kamarátmi dospeli k názoru, že sme sa narodili pre triatlon. Pustili sme sa teda do trénovania (..ja ani moc nie..prišlo mi to únavné).   Netrvalo dlho a založili sme si viacúčelový klub „Hrom do piecky!“, pod hlavičkou ktorého sme sa začali zúčastňovať amatérskych triatlonov.  Podávali sme a stále podávame krásne výkony, plné radosti, endorfínu, športového zápolenia, nezlomnej vôle, kŕčov, nutkania tresknúť bicykel do najhlbšej priepasti, obeseniu sa na šnúrkach od tenisiek a celkovej nechuti žiť...     Minulý víkend nás jeden z kamarátov prihlásil na triatlon v českom Tábore. Nikdy sme nezápolili na území českých bratov takže nám to prišlo ako dobrý nápad.   Je nedeľa ráno a my vchádzame do mesta. Je to tu divné. Všade sú poliši, všade sú zábrany,  je tu kopa nafúknutých reklamných oblúkov, jedným slovom to tu vyzerá moc moc nóbl. Potichu pregĺgame.     Vchádzame do miestnosti na registráciu.   Vidíme dlhý rad pretekárov.   Vidíme dlhý rad profesionálnych pretekárov.   Vidíme dlhý rad pretekárov, ktorým chodíme tlieskať na majstrovstvá republík a pohárové závody a ticho ich pritom obdivujeme.   Veľmi hlasno pregĺgame.     Nad dlhým radom pretekárov vidíme názov závodu „Majstrovstvá Českej republiky  a finále českého pohára“. Zo stodvadsaťdva pretekárov je tu 117 profesionálov, dvaja vojaci a mi traja. Nedýchame. Je nám nevoľno. Vzájomne sa podopierame a registrujeme sa u zneistenej tety.   Dušan ticho porovnáva svoj trekový bicykel za pätnásť tisíc so stotisícovými cestnými strelami. Ja porovnávam svojich 106kg s telami gréckych polobohov, ktorí vážia o niečo viac ako vrece cementu. Janko s telom gréckeho poloboha si objednáva palacinky.       Stretáme Paľa Šimka, ktorý ide o pár týždňov reprezentovať SR na olympiádu.   „Čaute prišli ste povzbudzovať? Super!“ zvolá nadšene.  "Nie. Prišli sme ti natrhnúť prdel...“ Odpovedáme a ja odchádzam so stiahnutým žalúdkom na toaletu.      Stretáme Andreja Orlického, reprezentanta SR.   „Čaute prišli ste povzbudzovať? Super!“ zvolá nadšene.  „Nie. Prišli sme ti natrhnúť prdel...“ Odpovedáme a ja opäť odchádzam na toaletu.     Opäť stretám Andreja Orlického.  „Keby som vedel, aké je to tu hovädsky kopcovité, nikdy by som sem nešiel“ vraví nešťastný po zhliadnutí mapy.  „Ku..a“ zašepkám a odkráčam na toaletu. Je obsadené. Sedí tam Dušan a Janko čaká v rade.     Štart prebehol pomocou výstrelu z dela. Keby som nebol desaťkrát na toalete, tak sa určite z tej rany poseriem. Na brehu jazera je obrovské množstvo divákov, ktorý čakajú na výbeh z vody do bicyklového depa.   O pár sekúnd som vo vode sám. Všetci odfrčali rýchlosťou motorových člnov v ústrety bójkam. Prvých dvesto metrov sa pokúšam o kraul. Zvyšok udýchaný doprsíčkujem.  Na brehu nie je okrem mojej drahej nikto. Z ľútostivým úsmevom ma milo povzbudzuje. Po dvesto metroch behu do kopca vbieham na pokraji infarktu do prázdneho depa. Obúvam sa a sadám na jediný bicykel čo tam zostal. Vyrážam do ulíc historického Tábora. Veľká chyba.      V propozíciách písali, že trať prechádza cez kopec so 17% stúpaním. Nazvem si ho pracovne Sviniar. Sviniar prichádza, sústredím sa na neho, dupem, na vrchu sa točím a so šťastným úsmevom víťaza zjazdujem. Ešte dvakrát ho musím dať a budem koniec.    Pozor pozor pozor!!! Niečo nie je v poriadku! Žlté šípky vravia zaboč HORE. Zaboč hore do brutálneho kopca. Meraviem. Tvrdnem ako betón. Kopec predtým nebol Sviniar.   Sviniar sa týči do výšky v sklone, kde som si istý že sa čoskoro prevrátim na chrbát a budem metať nôžkami ako nešťastný chrobák. V prvej tretine kopca zosadám a za obrovského povzbudzovania tlačím bicykel. Táto scéna sa opakuje ešte dvakrát. Cítim, že som miláčikom publika. Občas okolo mňa prejde fučiaci profík a mrmle niečo o zavadzaní. Trikrát mám chuť to vzdať a trikrát mám chuť sa od vysilenia povracať. Bojujem s telom i psychikou. Dávam si tie najhoršie mená a preklínam vrásnenie. Chcem žiť v Maďarsku na rovine! Chcem jesť halászle a hľadieť pri tom do diaľky, kde mi nezavadzia žiaden vyvrásnený KOPEC!   Hmlisto si pamätám, že som sa v depe prezul do tenisiek a tri krát v mrákotách a tridsať stupňovej teplote obehol mestský park.   Všetci policajti ma povzbudzujú. Som aj ich miláčik. Hneď ako prechádzam, s úľavou balia zábrany a odchádzajú domov. Vchádzam do cieľa, počujem rapkáč, oslavné výkriky, padám. Jem melón. Jem veľa veľa melónu. Všade okolo mňa je melón. Ja sám som melón.          Na výsledkovej listine je pri mojom mene DNF (Did Not Finish).    „Ako to, že som neukončil, keď sa tu na vás živý zdravý kukám?“ pýtam sa prekvapene.  „Nooo...my sme s tým už nejak nepočítali a potrebovali sme to už vyhodnotiť. Viete, ľudia by už radi išli domov...ale na internete to už bude v poriadku...“ upokojujú ma.     Na druhý deň s prekvapením zisťujem, že som nebol posledný! Jeden nedokončil a jedného diskvalifikovali za nejakú blbosť.   Oblieva ma úžasný pocit. Verím, že raz ich bude za nešportové správanie diskvalifikovaných viac a ostatných postihnú defekty (fakt netuším ako sa mi mohli vysypať z dresu moje pripináčiky pre šťastie) a ja povstanem na stupienku majstra Českej republiky.   Už pri písaní týchto slov mi neznáma sila ťahá ruky nad hlavu a na tvári sa mi rozlieva úsmev víťaza. Preberám pohár z českého kryštálu a krásny veľký melón...Som plný zadosťučinenia...Treba len trpezlivosť a pripináčky pre šťastie...Pomaly zakusujem do šťavnatého melónu...Som šťastný...Triatlon je krásny šport....           Cieľové foto (Maťko, Duško, Janko)         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (134)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Basila 
                                        
                                            Snívajte. Spolu.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Basila 
                                        
                                            Chodec, Murár a Svine
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Basila 
                                        
                                            Pár krokov vpred
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Basila 
                                        
                                            Dolina číslo 36 (Maťkovi Kopálovi)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Basila 
                                        
                                            Môj otec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Basila
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Basila
            
         
        basila.blog.sme.sk (rss)
         
                        VIP
                             
     
        ..kronikár vedeckej nadácie Za Šťastné Tváre Našich Žien - ZŠTNŽ...keby neovplýval citom pre ďaľeký hod granátom a prekvapivo rýchlym šplhom na tyči, nikdy by si v triede nenašiel kamarátov...[gramatický analfabet]..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    69
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5850
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Život
                        
                     
                                     
                        
                            Príručky
                        
                     
                                     
                        
                            Svetom
                        
                     
                                     
                        
                            Momentky
                        
                     
                                     
                        
                            Všeličo
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            moskaľova žena (the best)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            David Gray &amp; Annie Lenox
                                     
                                                                             
                                            Adriana Kučerová &amp; Oskar Róža
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Lubo Némeš - Did Riddick
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




