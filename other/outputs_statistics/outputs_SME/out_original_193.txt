
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lucia Kramaričová
                                        &gt;
                Cultus atque humanitas
                     
                 Indoeurópska jazyková rodina a otázka jej pravlasti 

        
            
                                    27.9.2007
            o
            10:10
                        |
            Karma článku:
                11.81
            |
            Prečítané 
            5682-krát
                    
         
     
         
             

                 
                     
  Indoeurópska jazyková rodina zahŕňa najrozšírenejšiu skupinu jazykov - takmer všetky európske jazyky a väčšiu časť strednej Ázie - jazyky Iránu, Indie a priľahlých oblastí. V súvislosti s objavením indických jazykov a sanskrtu koncom 18. storočia sa začalo štúdium historickej a porovnávacej jazykovedy a hľadal sa pôvodný prajazyk. Určiť oblasť, odkiaľ vyšli všetky indoeurópske jazyky je s odstupom niekoľkých tisícročí dosť náročné. Pravlasť Indoeurópanov sa kládla do oblasti strednej Ázie, odkiaľ mali príslušníci tejto jazykovej rodiny migrovať v niekoľkých vlnách do Európy.  
                

                  Názor vznikol z nadšenia pri objave sanskrtu a stotožnením jeho starovekých používateľov s pôvodnými Indoeurópanmi, čo sa však ukázalo ako chybné. Keďže neexistujú dôkazy ani zmienky o tom, že by predkovia Grékov, Keltov, Germánov a ostatných európskych kmeňov boli v tejto oblasti, pravlasť Indoeurópanov sa hľadala v Európe. Podporu má táto teória vo fakte, že práve  v Európe je najviac indoeurópskych jazykov, doložených už od dávnych čias. Navyše sa tieto jazyky dosť odlišujú, čo vylučuje príchod jednoliatej veľkej skupiny, ktorá by sa mala postupne rozpadať. Tento názor bol mierne spochybnený objavom tocharských dialektov na území Číny, avšak bližším skúmaním fonetických zmien ovplyvnených stykom s inými, neindoeurópskymi jazykmi, prišli vedci k názoru, že tocharčina sa na Ďaleký východ dostala dlhou cestou - z pravlasti Indoeurópanov.     Čo sa týka postupu Indoeurópanov, je jasné, že dnešné Grécko kolonizovali helénske kmene zo severu, takisto aj Itália bola osídľovaná zo severu. Západná časť Európy - Pyrenejský polostrov bol obývaný neindoeurópskymi kmeňmi až do príchodu Rimanov. Indoeurópska skupina na východe hraničila s ugrofínskou, ktorej pravlasť ležala pravdepodobne medzi Volgou a Uralom. Z toho vychádza, že pravlasť Indoeurópanov bola medzi Rýnom a oblasťou dnešného stredného a južného Ruska. V skúmaní pravlasti a prajazyka sa objavujú dva názory - monogenéza: indoeurópske jazyky sa vyvíjali z jedného pôvodného centra, z ktorého neskôr skupiny migrovali. Polygenéza: vychádza z porovnania indoeurópskych a neindoeurópskych jazykov, teória splývania dvoch jazykových oblastí s veľmi obtiažnym nájdením pôvodného domova.     Rekonštruovať indoeurópsky prajazyk je veľmi náročná úloha. V ostatnej dobe sa hovorí o tom, že nebol jednotný prajazyk, ale išlo o skupinu veľmi blízkych nárečí. T. Burrow v publikácii The Sanskrit Language (1973 3. vydanie) v úvode píše: „Na vývin indoeurópčiny nemožno nahliadať ako na vznik románskych jazykov z latinčiny. Vtedy sa rôzne jazyky odvodzovali z jedného a jednotného jazyka. Ale v prípade indoeurópčiny je isté, že to nebol takýto jednotný jazyk, ktorý možno vyskúmať pomocou porovnávania. Bolo by jednoduché vytvoriť viac-menej donekonečna rad tvarov (čo sa z čoho vyvinulo) pre sanskrtské nābhi-, a grécke οmfalós (pupok), ktoré napriek tomu, že sú priamo zdedené z pôvodného indoeurópskeho obdobia a sú spríbuznené, nedajú sa redukovať na jediný pôvodný výraz. V skutočnosti detailné porovnanie jasne ukazuje, že indoeurópčina, ktorú takto môžme dosiahnuť, bola už dávno rozčlenená na rad rôznych dialektov." J. Vacek v skripte Úvod do studia indických jazyků II, Indoárijské jazyky v kontextu indoevropské jazykovědy (1975): „Snahy o rekonstrukci prajazyka byly důležitou součástí indoevropské jazykovědy v jejích počátcích, avšak postupně se ukazovalo, že pojetí indoevropského prajazyka jako dialektově jednotného jazyka je neudržitelné." Problémom je datovanie - v niektorých jazykovedných prácach sa uvádza, že okolo roku 2500 pr. n.l. sa z jednotnej pravlasti začali migračné vlny, ktoré spôsobili vznik jednotlivých jazykov. Avšak v čase sťahovania z pravlasti už museli nadobúdať dialekty svoje charakteristické črty, pretože je jasné, že keď sa oddelila indo-iránska vetva (cca 2000 pr. n.l.), jej členovia hovorili nie pôvodným spoločným indoeurópskym jazykom, ktorý sa postupne zmenil na indoiránčinu, ale práve indoiránskym jazykom, ktorý sa formoval už na území Európy pred odchodom. Údaj o počiatkoch migrácie treba posunúť viac do minulosti. T. Burrow: „Ak niekedy existovala jednotná indoeurópčina, ktorá sa šírila z ohraničenej oblasti, muselo to byť dávno pred akýmkoľvek raným obdobím, ktoré môžme dosiahnuť porovnaním. ´Primitívna indoeurópčina´ sa musí brať ako kontinuum príbuzných dialektov, ktoré boli na rozsiahlom území v Európe, dialektov, ktoré už pred časom veľkých sťahovaní začali nadobúdať charakter rozdielnych jazykov." J. Vacek: „Rekonstruovaná indoevropština je v podstatě souborem prvků, které příslušejí různým, často obtížně určitelným obdobím a oblastem. ... Pizani /Pizani: K indojevropejskoj probleme, 1966/ připouští pouze možnost rekonstruovat ´prajevy´, nikoli ´prajazyk´. ... V novějších pracech se celkově objevuje důležitá změna v nazírání na povahu onoho vztahu mezi jazyky. Zatímco starší práce chtějí rekonstruovat a hledat společný zdroj, uvažuje se v moderních pracech právě o otázce možného vzájemného vlivu."         Indoeurópska jazyková skupina sa skladá z jazykov:   Albánsky jazyk: dva dialekty - severný gegický a južný toskický. Pokladá sa za jazyk vyvinutý z illýrčiny alebo z thráčtiny. Albánčina je silno ovplyvnená latinčinou, taliančinou a turečtinou. Najstaršie pamiatky 16. - 17. storočie.   Árijské (indo-iránske) jazyky: tri skupiny - iránske, dardské (káfirské) a indické jazyky. Stará iránčina dochovaná v klinopisných záznamoch 6. - 4. stor. pr. n.l. a avesta - posvätné texty zoroastriánov. K iránskym jazykom patrili aj jazyky Skýthov, Sarmatov a Médov. Stará indičtina tradovaná ústne - védy od cca 1500 pr. n.l., podobne ako staroiránčina je písomne zaznamenaná oveľa neskôr.   Arménsky jazyk: podobne ako gréčtina používaný už od staroveku. Preklad Biblie v 5. storočí.   Baltské jazyky: litovčina, lotyština a vymretá stará pruština. Litovčina si zachovala archaické črty. Najstaršie pramene z 15. - 16. storočia.   Slovanské jazyky: západné - lužická srbčina, čeština, slovenčina, poľština; južné - slovinčina, chorvátčina, srbčina, macedónčina, bulharčina; východné - ukrajinčina, bieloruština, ruština. Najstarší prameň preklad Biblie z 9. storočia. Baltské a slovanské jazyky majú viacero spoločných čŕt, ale aj rozdiely, pre ktoré nemožno hovoriť o spoločnom pôvodnom baltoslovanskom jazyku.   Grécky jazyk: zložený z dialektov (aiolský, iónsky, attický, dórsky...). Cez spoločný jazyk koiné a byzantskú gréčtinu až po novogréčtinu. Najstaršie texty sú v mykénčine, cca 1500 pr. n.l.   Italické a románske jazyky: italické: osko-umbrijské - oskičtina, umbrijčina, sabelčina...; latino-faliské - latinčina, faličtina. Nápisy od 6. storočia pr. n.l.   Latinčina je základom románskych jazykov, západné - francúzština, provensálčina, portugalčina, španielčina, katalánčina, rétorománčina, sardínčina; východné - moldavčina, rumunčina, taliančina, dalmátčina (vymretá od 1898).   Keltské jazyky: kontinentálne keltské jazyky boli používané v staroveku v západnej Európe, na Balkáne a v Malej Ázii (Gallovia, Kelti, Galati), dnes vymreté. Ostrovné keltské jazyky - gaelčina, kymérčina, kornčina, bretónčina, používané v Írsku, Walese, Škótsku a ostatných ostrovoch patriacich k Veľkej Británii. Najstaršie pamiatky z 8. storočia.   Germánske jazyky: západné - angličtina, nemčina, frízčina, holandčina; severné - škandinávske (švédčina, nórčina, dánčina, islandčina); východné - vymretá gótčina. Najstaršia pamiatka je Ulfilov gótsky preklad Biblie, 4. storočie; škandinávske runové nápisy z 8. storočia.   Tocharský jazyk: dialekt A a B, oblasť dnešnej Číny - Turkestan, najvýchodnejší indoeurópsky jazyk. Pamiatky sú buddhistické rukopisy, preklady zo sanskrtu, 6. - 10. storočie.   Chetitský jazyk: v Anatólii, najstarší doložený indoeurópsky jazyk, dialekty: klinopisná chetitčina používaná v úradných záznamoch, luvijčina a palajčina. Pamiatky sú klinopisné tabuľky z obdobia 1700 - 1200 pr. n.l.   Ostatné vymreté indoeurópske jazyky: lykijčina, lýdčina a frýgičtina v Malej Ázii. Z nich je zachovaných pár nápisov v písme odvodenom z gréckeho, 7. - 4. storočie pr. n.l. Na Balkáne a v Itálii bola rozšírená illýrčina, thráčtina, dáčtina, staroveká macedónčina, všetky sú doložené niekoľkými nápismi alebo glosami u gréckych a latinských autorov.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (47)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Dvor pri usadlosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            PF
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            V starom dome - retro kúpeľňa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Jar a začiatok leta v starej záhrade
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Od zimy do leta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lucia Kramaričová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lucia Kramaričová
            
         
        kramaricova.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.milosch.sk/LuckaMilos 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    165
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3234
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Aliquid de antiquitate
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            De universitate
                        
                     
                                     
                        
                            Cultus atque humanitas
                        
                     
                                     
                        
                            Animalia
                        
                     
                                     
                        
                            Železník
                        
                     
                                     
                        
                            Indológia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Zatepľovanie - ničenie historických fasád
                                     
                                                                             
                                            Bezhlavé zatepľovanie
                                     
                                                                             
                                            Ručná výroba thonetiek
                                     
                                                                             
                                            Statok bez modernizácie
                                     
                                                                             
                                            Panelákové bývanie
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Titus Popovici - Cizinec
                                     
                                                                             
                                            Učebnice sanskrtu - zas a znova
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            andy
                                     
                                                                             
                                            vlastnou rukou, mokopa
                                     
                                                                             
                                            výtvarník
                                     
                                                                             
                                            certikoviny
                                     
                                                                             
                                            denník z korfu
                                     
                                                                             
                                            brankárska stránka
                                     
                                                                             
                                            indo-európsky blog
                                     
                                                                             
                                            pamiatky
                                     
                                                                             
                                            anca din românia
                                     
                                                                             
                                            čosi z orientu
                                     
                                                                             
                                            archeológ martin
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




