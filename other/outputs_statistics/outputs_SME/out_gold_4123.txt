

 Deti, nech už je ako chce, taká dobrá tarhoňa (to som ale vyhrabal šupu starú...) či ryža, keď sa dobre pripravia aj samotné ako hlavné jedlo, nie len ako príloha, sú lahôdky na pohľadanie. A navyše ryža je pre dietárov ako stvorená (samozrejme bez toho bravčového guláša a nie pre chorých dnou) a môže sa pripraviť na bez počet spôsobov. Tu sú dve inšpirácie: 
 Balkánska ryža  
 Potrebujeme: 
 0,5 kg ryže, 15dkg balkánskeho syra, 2 cukety, 1 veľký pór, 5 opečených a ošúpaných paprík (lenivci nahradia paprikami upečenými v rúre, či čerstvou paprikou), 2-3 mrkvy, kus čínskej kapusty, čierne mleté korenie, petržlenovú vňať (sušenú či čerstvú), saturejku, alebo čubricu (inú bylinku, napríklad oregano), olivový olej a soľ. 
  
 Aj ryžu si treba zaslúžiť... 
 Opečenú či upečenú papriku (pečieme v rúre na 160 stupňov vcelku v olejom vymastenom pekáčiku dohneda) nakrájame na menšie kúsky, čínsku kapustu nakrájame na rezančeky, ryžu dobre premyjeme, pór pokrájame na pol krúžky, cuketu podobne, len väčšie a mrkvu očistíme a nastrúhame na hrubom strúhadle. 
  
 Vo vhodnej hlbšej najlepšie teflonovej nádobe s pokrievkou najprv urestujeme mrkvu, pridáme pór, bylinky a čierne korenie. 
  
 Posolíme a prehrejeme. Na rad prichádza čínska kapusta, 
  
 potom cuketa, všetko trochu podlejeme, prikryjeme a 5 minút dusíme, len aby nám zmes trochu povolila. 
  
 Pridáme ryžu, pečenú papriku, dobre podlejeme a občas premiešajúc dusíme do mäkka. 
 Zo začiatku bude ryža trochu lepiť. Trpezlivo pomaly miešame a postupne počas varu prilievame ubúdajúcu vodu tak, ako ju bude ryža pohlcovať. 
  
 Výsledok je veľmi príjemný. Ryža je úplne mäkká, nasala všetky chute  zeleniny a zároveň sa krásne oddeľuje zrniečko od zrniečka. 
  
 Podávame posypané balkánskym syrom a obložené čerstvou zeleninou, alebo  aj nie, veď zeleniny  v našom rizote dosť. 
  
 Dobrú chuť. 
  
   
   
 Zapekaná špenátová ryža 
 Potrebujeme: 
 0,5 kg ryže, viazaničku mladej cibuľky, 2 balenia mrazeného listového špenátu, 1 balenie údeného tofu syra, 0,5 l smotany na varenie, 3 strúčiky cesnaku, 3 vajíčka, muškátový oriešok, čierne mleté korenie, mletú rascu, soľ a olivový olej. 
  
 Na oleji najprv urestujeme nakrájanú mladú cibuľku, pridáme nakrájaný cesnak a mrazený špenát. Zmes okoreníme, osolíme a pridáme aj trochu mletej rasce. Rasca a cesnak sú dobré na prekrytie prípadnej horkosti mrazeného špenátu. 
  
 Hrniec prikryjeme a necháme špenát rozpustiť. Občas premiešame. Na rad prichádza premytá ryža. Ako v prvom prípade dobre podlejeme a dusíme ryžu do mäkka prilievajúc vodu koľko ryža pojme. V prípade potreby ešte dosolíme. 
  
 Tak a tu máme našu ryžovo - špenátovú smrť. 
  
 Nasypeme ju do vymasteného pekáča, urovnáme a 
  
 vymiešame si zmes vajíčok so smotanou na varenie a postrúhaným údeným tofu syrom. Ochutíme ju čerstvým muškátovým orieškom (1/3 kávovej lyžičky), posolíme ju 
  
 a šup s ňou na špenátovú ryžu. 
  
 Po zhruba 30 minútach pečenia na 180 stupňoch vytiahneme túto nádheru. 
  
 Dobrú chuť 
  
 a príjemný zvyšok nedele. 
  
   
 tenjeho 
   
   

