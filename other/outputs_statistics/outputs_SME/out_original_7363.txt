
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mirka Vávrová
                                        &gt;
                Alica v krajine zazrakov
                     
                 Sociálny pedagóg alebo Kdepak ty ptáčku hnízdo máš? 

        
            
                                    26.5.2010
            o
            22:47
                        |
            Karma článku:
                8.09
            |
            Prečítané 
            1788-krát
                    
         
     
         
             

                 
                    Minule som takmer vyhrala argumentačný súboj. No i strieborné miesto pohladkalo moje ego. Dostala som niekoľko pozitívnych spätných väzieb. Blahosklonne som ich prijímala a po celý čas som sa v duchu usmievala a hovorila si: Moji milí, ak by ste aj vy boli sociálnymi pedagógmi, vedeli by ste argumentovať snáď i lepšie. S otázkou, čo je to vlastne sociálna pedagogika, sa totiž stretnem priemerne raz za sedem dní. Ak mám šťastný týždeň, aj dva či trikrát. Nuž a vysvetľuj ľudu, keď ani sám celkom presne nevieš... Moje zmiešané pocity pramenia predovšetkým z obsahovej náplne štúdia. Posúďte sami.
                 

                 V rámci svojho štúdia som už stihla aj presedlať z jednej populárnej univerzity na druhú. Informácie mám teda z prvej ruky, dvoch zdrojov a s minimálne troma svedkami pri všetkom, čo sa práve chystám napísať. Hoci popudov na napísanie podobného článku som počas svojej (zatiaľ) štvorročnej oddysey mala už niekoľko, najsilnejšia motivácia mala len prísť. A prišla. Presne včera o ôsmej večer. Mali sme skúšku z informatiky. Ani vám sa v hlave sociálna pedagogika nespája s týmto výpočtovým čímsi? Vitajte v mojom svete.   Dovoľte mi predstaviť vám stručný výber z predmetov, ich obsahov a prevedení, po absolvovaní ktorých hrdo tasím argument, že sociálny pedagóg je flexibilná odborná (i odolná) sila.   1. Informatika   Už som čo to načrtla. Bola skúška a nebola ľahká. Tak už to chodí, viem. O informatickej online skúške sa však nedá povedať ani to, že bola ťažká. Pretože bola šialená. Hneď prvé tri vygenerované otázky sa ma spýtali, či viem, o koľko percent väčšie sú riadky v programe Excell 2007 oproti jeho predchodcovi z roku 2003. Boli zvedavé aj na to, či viem, ktorú špeciálnu funkciu priniesol nový Power Point. Nebyť psychickej opory po mojom boku, skočím z okna. Neskočila som. Ale ani nepochopila, prečo boli pre sociálneho pedagóga tieto informácie tak podstatné.   2. Psychohygiena   Na začiatku bola asi krásna myšlienka. Psychohygiena je predsa dôležitá, o tom niet najmenších pochýb. Hlavne v pomáhajúcej profesií človek musí vedieť, kedy vypnúť, aby sa mohol vôbec znova zapnúť... Zarážajúci bol spôsob prevedenia. Mladá žena, ktorá sa asi z princípu neusmieva ale zazerá, prichádza a odchádza ako fúria a o psychohygiene nikdy nič nepočula, nieto testovala, nám za desať minút porozprávala desať nesúvislých viet a odišla. Nahnevali sme ju. Dodnes nechápeme čím.   3. Angličtina - úroveň B(l)ééé   Jazyk je nevyhnutnosť! To vie každý. Po absolvovaní tohto predmetu mám však pocit, že sa po anglicky dohovorím aj s niekým, kto po anglicky v živote necekol ani slovo. Tak dobre už angličtinu ovládam. Že bola skúška sito s veľkými okami asi nemusím ani písať. Smutné, že kvôli jazyku prišiel svet o nejedného kvalitného pedagóga...   4. Základy špeciálnej pedagogiky   Tak toto bola strata času de lux. Opäť (snáď) dobre mienené. Skutek však, ako to už býva, utek. Odborné termíny pre všetky diagnózy sveta, hostilná nálada k ľuďom, ktorí sú predmetom pedagogického pôsobenia tohto odboru, beznádej a mínus desať euro, ktoré putovali do profesorovej kasičky (kvôli skriptám, ktoré sa rozpadli po troch intenzívnejších pohľadoch do knihy a len umocnili atmosféru "aj tak sú to kripli") - to je všetko, čo mi po semestri ostalo.   5. Sociálna geografia   Zaujímavé, tešila som sa, hľadiac do rozvrhu. Ani prax nebola taká zlá, ako mohla byť. Ale povedzte mi, na čo je potrebné vedieť, do ktorej sociálno-ekonomickej časti sveta patrí Irak? Keď klienti sú z Malaciek či Pezinka? Pedagóg môjho rangu (ako hrdo to znie:) mal s týmto predmetom spoločné len to prídavné meno na úvod...   Na záver by som vypichla ešte jednu špecialitku. Názvy predmetov zneli dobre. Aj súvis so študovaným odborom bol zrejmý na prvý pohľad. Jediný problém bol, že nedorazil prednášajúci. Ani raz...   Nuž, zrekapitulujme si to. Bolo málo predmetov, ktorých obsah, personálne zabezpečenie, či význam pre prax, som zaznamenala a dobrého dojmu sa neviem ani nechcem zbaviť, i keď uplynuli mesiace aj roky. Jedným z nich bol predmet Metódy rozvoja kritického myslenia. Snáď preto som sa prestala báť veci hodnotiť. Netreba vždy len prikyvovať a bez rozmyslu brať, čo nám ponúkajú.   Ak sa nepoznáme, asi mi neuveríte, že napriek istým nedokonalostiam ma štúdium sociálnej pedagogiky vždy bavilo i baví. Cítim, že je to pre mňa správna cesta. Aj preto sa snažím zorientovať v otázke, kde sú hranice pôsobnosti takéhoto odborníka. Nuž, ako to vyzerá, zatiaľ nikde. Takže zajtra sa možno stretneme aj u vás v práci...:) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Letné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pani učiteľke s láskou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pohľad späť (cez spätné zrkadlo)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Malé špongie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Návraty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mirka Vávrová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mirka Vávrová
            
         
        mirkavavrova.blog.sme.sk (rss)
         
                                     
     
        Milujem a neúnavne študujem život.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1418
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Alica v krajine zazrakov
                        
                     
                                     
                        
                            la vita é bella..niekedy
                        
                     
                                     
                        
                            Svet detí (a ja v ňom)
                        
                     
                                     
                        
                            Lásky jednej rusovlásky
                        
                     
                                     
                        
                            Išla Mirka na vandrovku
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dobrá škola (časopis)
                                     
                                                                             
                                            English is easy, Csaba is dead
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                             
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            30 seconds to Mars
                                     
                                                                             
                                            Passenger
                                     
                                                                             
                                            Emeli Sande
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mary - lebo píše ľahučko a krásne
                                     
                                                                             
                                            Sjuzi - lebo :)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Letné prázdniny
                     
                                                         
                       Pani učiteľke s láskou
                     
                                                         
                       Pohľad späť (cez spätné zrkadlo)
                     
                                                         
                       Malé špongie
                     
                                                         
                       Návraty
                     
                                                         
                       Klub anonymných strachoprdov
                     
                                                         
                       Jedna hodina, tri predsudky
                     
                                                         
                       50 centov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




