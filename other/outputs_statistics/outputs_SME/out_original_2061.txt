
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Lorenzová
                                        &gt;
                Nezaradené
                     
                 Nevinné dieťa za mrežami postielky Detského domova koho vinnou? 

        
            
                                    15.1.2010
            o
            12:30
                        |
            Karma článku:
                15.38
            |
            Prečítané 
            6427-krát
                    
         
     
         
             

                 
                    Dnes mi došla žiadosť od známeho:
                 

                 
Ilustračné foto 
   Žiadosť o pomoc pre tých, ktorí chcú pomôcť maloletému ročnému dieťaťu       V zastúpení maloletých nevinných detí dvoch súrodencov a maloletého ročného dieťaťa, ktorí boli umiestnení v našej profesionálnej rodine z nemenovaného Detského domova v SR, obraciam sa na Vás so žiadosťou o pomoc týmto maloletým nevinným deťom, nakoľko počas jedného roka som bol svedkom poškodzovania nielen práv a záujmov maloletých detí, ale aj zákonov SR. Stručne sa predstavím: som ženatý, s manželkou sme žili 19 rokov v šťastnom harmonickom manželstve, spoločne vychovávame tri dospievajúce deti vo veľkom rodinnom dome a po príprave na profesionálne rodičovstvo sme prijali do našej rodiny tri maloleté deti z DeD. Dvaja boli súrodenci a najmladšie deväťtýždňové bábätko. Splnili sme všetky podmienky - príprava, veľký rodinný dom, odpisy z registra, môj súhlas, zamestnali manželku a veril som, že môžem pomôcť, preto som sa aktívne od samého začiatku zapájal a pomáhal našej novej profesionálnej rodine. Nepíšem konkrétne mená, domov, adresy, lebo poznám zákon na ochranu osobných údajov, ale hlavne prosím iba kompetentých, ktorí naozaj môžu pomôcť, o kontakt, ktorý uvádzam v mailovej a mojej adrese. Je možný aj osobný kontakt. Som iba človek a v našom štáte by sme v prvom rade mali chrániť ľudí. Uvediem všetko, čo spôsobil iba jeden DeD a príslušné úrady. Viem však z pobytov náhradných rodín, ktorých sa zúčasťňujem, že podobne to funguje aj inde.         Dôvodom mojej žiadosti je  zistenie viacerých skutočností, ktoré vyvolávajú dôvodnú obavu, že postup príslušných štátnych zamestnancov, ako aj psychológa DeD, je v rozpore s právnymi predpismi SR na jednej strane, resp. s Etickým kódexom Slovenskej psychologickej spoločnosti na strane druhej. Dlho som sa všetkému len prizeral, ale nastal čas prehovoriť. Študujem sociálnu prácu a tohto roku mám štátnice, pýtam sa ako laik kompetentných na ich názor a prosím a žiadam o pomoc. Ak nenájdem pomoc v Slovenskej republike v tejto veci, budem hľadať ďalej, lebo chcem dať odpoveď mojim vlastným deťom, ale aj maloletým nevinným deťom, ktoré nám boli zverené a nevedia sa brániť.    Nezhody, ktoré sú v rozpore s právnymi predpismi, zákonmi SR a Deklaráciou práv dieťaťa a Chartou práv dieťaťa, sa pokúsim uviesť chronologicky ako svedok, šofér. Manželke som pred 19 rokmi prisahal, že ju neopustím, preto som jej oporou, ale najmä svedkom týchto skutočností, ktoré sa diali v našej rodine:    · vo februári 2009 som prevzal tri maloleté deti - dvaja boli súrodenci a najmladšie deväťtýždňové bábätko, 3 autosedačky, 2 postieľky, 1 paplón, 1 vankúš. Veci, ktoré mali oblečené v čase presunu sme museli vrátiť späť do DeD;   · súrodenci boli v DeD 13 mesiacov a nemali žiadne osobné veci, kontakt od biologickej rodiny bol iba telefonický. Po 9 mesiacoch prežitých u nás, nakoľko išlo o rómske deti, som požiadal o zapísanie do zoznamu žiadateľov a chcel som ich po príprave vziať do NRS – pestúnstva a pracovníčka príslušného úradu zisťovala: Sú súrodenci v zoznamoch? Neboli! O mesiac sa zrazu ozvala biologická príbuzná súrodencov, ktorá bývala o tri ulice ďalej od DeD, kde boli 13 mesiacov. Osobne mi povedala: ,,Vyzvali ma z úradu, že ich zoberú cudzí ľudia“. Pýtam sa, prečo ju vyzvali po 22 mesiacoch? Zo strany DeD nariadili manželke dovolenku a odovzdanie maloletých detí do DeD na intenzívny kontakt. Je povinnosťou zamestnanca citovať zákonník práce riaditeľovi? V našom prípade to pomohlo, lebo dovolenku zrušil. Intenzívnym kontaktom, na ktorý sme boli pozvaní, boli, podľa DeD, v decembri „až“ dve interakcie a súrodenci šli na hostiteľský pobyt. Vôbec ju nepoznali, ale pripravili sme ich na bezbolestný prechod, lebo šli z rodiny do rodiny. Budú zverené do náhradnej osobnej starostlivosti. Kde odišli? Do jednoizbového bytu, kde býva radšej neuvediem koľko ľudí, príslušným úradom posúdeným ako vyhovujúci.       V profesionálnej rodine ostalo len maloleté bábätko, ktoré medzitým oslávilo rok života:       · deväťtýždňové maloleté bábätko bolo v našej profesionálnej rodine od februára 2009;   · v apríli 2009 požiadali žiadatelia o zverenie maloletej do NRS;   · v júli 2009 príslušný úrad  maloletú zaradil do prehľadu ponúkaných detí;   · v auguste 2009 žiadateľov vyzvali na kontakt a ponúkli k maloletej ešte dve nevlastné sestry, ktoré boli pol roka v DeD bez maloletej. Zákon hovorí o práve dieťaťa na udržiavanie a rozvíjanie súrodeneckých väzieb a do tohto obdobia sme boli v DeD minimálne raz do mesiaca a psychológ ani sociálny pracovník nám žiadne sestry nikdy neukázal, ani maloletú s nimi nekontaktoval;   · súrodenecké väzby boli konštatované „na základe vzájomných interakcií“, ktoré prebiehali v DeD napriek tomu, že maloletá v tom čase bola 80 km vzdialená v profesionálnej rodine, nebola pozvaná, nie sú o týchto interakciách žiadne písomné záznamy, ani ich spoločné fotografie. Psychológ, ktorý pozoruje tri sestry a zároveň posudzuje ich súrodenecké väzby z 80 km vzdialenosti, nie je asi ani v EÚ! Keby sa to dozvedeli boli by sme asi prvým štátom, ktorý má takúto osobnosť. Dôkaz je na ÚPSVaR v Bratislave. Prvé spoločné fotografie sestier nafotil sociálny pracovník až v novembri;   · v auguste 2009 nás opäť nikto nepozýva na rozvíjanie súrodeneckých väzieb;   · v septembri 2009, keď bola maloletá tri dni po operácii, ktorú mala naplánovanú DeD,  a v šetriacom režime, dal príkaz riaditeľ na jej okamžitý presun do DeD. Preto píšem aj teraz žiadosť o pomoc, lebo vtedy mi niekto pomohol. Vďaka im som pochopil, že ešte existuje spravodlivosť. Ako šetria štátni zamestnanci? Psychológ, sociálny pracovník, šofér, ich plat, k tomu auto a cestovný príkaz na prejdenie 160 km, aby maloletú bez akéhoľvek dokladu vzali. Na podnet opäť prejdú 160 km a po 2 hodinách ju vrátia späť do profesionálnej rodiny;   · v októbri 2009 konečne prebehlo prvé spoločné stretnutie dvoch sestier ponúkaných do NRS. Paradoxom je, že bolo za mojej účasti v miestnosti, ktorá sa podobá nemocničnej čakárni bez asistencie psychológa a sociálneho pracovníka. Dokonca ma tam nechali samého aj s biologickým otcom maloletej. Psychológ a sociálny pracovník, namiesto sledovania konečne prvého stretnutia, sedeli u pána riaditeľa. Keď som požiadal o prítomnosť na tomto stretnutí, pán riaditeľ ma nazval „Nikto“ a odmietol so mnou komunikovať. Spoločných stretnutí od februára 2009 do januára 2010 dvoch sestier ponúkaných do NRS bolo päť. Priebeh interakcií bol nasledovný: 1. – 14.10. v mojej prítomnosti bez asistencie psychológa, 2.- 18.11. u nás doma, kedy sa venoval psychológ starším súrodencom, 3. – 3.12. bola spojená aj s interakciou súrodencov s tetou, podobne ako aj 4. - 9.12. Piata interakcia 17.12. bola opäť v mojej prítomnosti bez asistencie psychológa, sociálneho pracovníka, ktorí, v čase stretnutia dvoch sestier, sedeli opäť u riaditeľa. Na základe takýchto interakcií budú odborne posudzované súrodenecké väzby?    · stretávanie sestier v našej rodine som viackrát navrhol, nepadlo to na úrodnú pôdu;   · podľa zákona o SPODaSK - biologickí rodičia maloletej ju videli prvýkrát v júli a druhýkrát v októbri 2009 prišiel na stretnutie len otec, ktorý vyjadril spokojnosť, že maloletá napreduje, je šikovná, pretože rastie v našej profesionálnej rodine a zároveň mi za to poďakoval. Keď som mu povedal, že prvýkrát vidím sestru maloletej, vyjadril sa, že tá nie je jeho dcéra, ale manželkina;    · MPSVaR odporučilo v septembri žiadateľom, aby si podali žiadosť o zverenie maloletej do pestúnstva na súd, termín bol vytýčený na 20.11.2009. Konanie bolo odročené, nakoľko sa biologickým rodičom nepodarilo doručiť predvolanie napriek tomu, že obaja boli v ten istý deň na plánovanej návšteve v DeD;    · nakoľko manželka mala nastúpiť na nariadenú dovolenku od 1.12. do 18.12.2009, a maloleté deti sa mali vrátiť do DeD, žiadatelia maloletej podali žiadosť na súd o predbežné opatrenie, ktoré bolo zmarené. Riaditeľ prisľúbil manželke nástup na dovolenku hneď nasledujúci deň po odovzdaní maloletých detí do NRS a opäť sa tak nestalo, pretože je na nútenej dovolenke od dnešného dňa;   · nový termín súdneho konania bol stanovený pred Vianocami 2009 na 15. februára 2010;   · dňa 8.1.2010 sme boli pozvaní na 6. interakciu (počas 11 mesiacov!) dvoch sestier, kde nás psychológ a sociálny pracovník zoznámil s novou žiadateľkou o NRS. Je to moja spolužiačka z vysokej školy, ktorú poznám tri roky. Upozornil som novú žiadateľku, že maloletá má vytýčený termín na súde. Interakcia, za asistencie psychológa, prebiehala v duchu ustavičného nabádania staršej sestry, ako sa má správať k mladšej. Asi preto som bol vyzvaný na opustenie miestnosti;   · dňa 11.1.2010, pre zhoršujúci sa zdravotný stav maloletej, lekárka LSPP doporučila hospitalizáciu na detskom oddelení. Manželka zostala s ňou a telefonicky dala na vedomie túto informáciu DeD. Z rodinných dôvodov manželka z nemocnice 12.1.2010 večer odišla;   · celé to vyvrcholilo 13.1.2010, keď psychológ, vedúca oddelenia SPOD a SK príslušného úradu a zdravotná sestra DeD vzali z detského oddelenia maloleté choré ročné dieťa do DeD, ktoré ide nadväzovať intenzívny kontakt s „budúcou pestúnkou“. Viem, že právo nazývať sa pestúnom, patrí žiadateľom o NRS až po právoplatnom rozhodnutí súdu. Pýtam sa, ako je možné, že nám bol oznámený presun tohto dieťaťa až vtedy, keď už v nemocnici nebolo? Opäť ten istý scénar – výjazd 4 osôb, k tomu auto a cestovný príkaz...podobne ako v septembri 2009, tentokrát aj s dokladom o prevzatí.    · ako je možné, že riaditeľ a psychológ nazvú žiadateľa o NRS pestúnom po jednej hodinovej interakcii? Ak zamestnáva riaditeľ DeD psychológa, ktorý posudzuje súrodenecké väzby na vzdialenosť 80 km, dokáže aj po hodinovej interakcii označiť žiadateľa o NRS za pestúna!    · primár detského oddelenia sa nám ospravedlnil, že konal na príkaz riaditeľa DeD a ešte sa s takýmto jednaním nestretol, podobne aj ošetrujúca lekárka. Neobliekli ju ani do jej osobných vecí, ktoré tam ostali, spolu s jej hračkami, vecami osobnej potreby.        Maloleté dieťa u nás rástlo v profesionálnej rodine 11 mesiacov a choré ho previezli do DeD na interakcie s „budúcou pestúnkou“.    Je to v súlade s Deklaráciou práv dieťaťa, zákonmi a predpismi SR?    Ako budú prebiehať tieto interakcie, ak je nasilu vytrhnuté zo známeho prostredia a k tomu ešte choré?    Kto jej pomôže?    Kto sa jej zastane?    Ovláda základné slová na tento vek – mama, tata, papa, mimi, dado, ťapi... myslíte, že jej budú rozumieť?    Že je smutná, opustená a k tomu ešte chorá?    Nemôže mať tento nehumánny a neetický skutok v budúcnosti dosah na jej psychomotorický vývin, keď opustená plače zatvorená v postieľke detského domova?    Je to vhodný pestún, ktorý trvá na interakciách a intenzívnom kontakte s dieťaťom aj napriek tomu?        Ide tu niekomu vôbec o to dieťa, o ktoré žiadatelia požiadali už v apríli 2009? Považujem za potrebné uviesť, že žiadatelia o maloletú, ktorých tu stále spomínam, od februára 2009 do januára 2010 mali s maloletou, v rámci rodinných návštev - oslavy menín, narodenín, vianoc a podobných príležitostí - nespočetne veľa vzájomných stretnutí. Ich záujem bol nielen o maloleté dieťa u nás zverené, ale aj ich adoptívny syn si vytvoril hlboký citový vzťah k súrodencom nám zvereným. Môžem o tom poskytnúť rozsiahlu fotodokumentáciu a videozáznam.   Môj názor ako laika na tento celý problém je, že maloleté 13-mesačné dieťa trpí za to, že DeD na čele s riaditeľom, psychológom, sociálnym pracovníkom a príslušnými úradmi rok nekonali a nešlo im nikdy o najlepší záujem dieťaťa. Podľa vyjadrenia zodpovednej pracovníčky jedného nemenovaného úradu, ide iba o rómske dieťa, na ktorom mne ale záleží a hájim jeho práva už len preto, lebo sa nevie hájiť samé. A pýtam sa, dokedy budú takýto odborníci ešte sedieť na svojich miestach?   Žiadam o vystavenie odborného znaleckého posudku na posúdenie vzájomných súrodeneckých väzieb nezávislým psychológom, lebo pri šiestich interakciách, z toho som bol pri dvoch úplne sám so sestrami, som ich pozoroval. Jeden rozpor som ešte zabudol uviesť. Ako je možné, že osemročná sestra maloletej nebola ponúkaná žiadateľom do NRS a čakalo sa sedem rokov, kým sa jej narodí nevlastná sestra?   Pri príprave na profesionálne rodičovstvo mojim mottom bolo: „Rozdávať úsmev aj s bolesťou v srdci.“ Dnes zomrel môj otec...napriek tomu sa nevzdávam...manželka preto nachvíľu opustila maloletú 12.1.2010 v nemocnici...        Vie si niekto z vás predstaviť, čo bude prežívať toto maloleté 13 mesačné dieťa počas pobytu  v DeD, keď 11 mesiacov rástlo v rodine? Ak existuje človek, ktorý dokáže dieťa vrátiť z DeD späť do našej profesionálnej rodiny, do právoplatného rozhodnutia súdu o zverení dieťaťa do NRS, tak ho veľmi prosím, aby niečo pre to urobil.       S úctou otec profesionálnej rodiny     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (123)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Barborka vôbec nemusela byť v detskom domove.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            "Lucinka bude dobrá mama!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Mohol tu byť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Slovenský triler
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Prečo je u nás lepšie byť opusteným psom,
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Lorenzová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Lorenzová
            
         
        evalorenzova.blog.sme.sk (rss)
         
                                     
     
        Som mamou biologických aj prijatých detí, pracujem v OZ DOMOV V RODINE, ktoré združuje profesionálne rodiny a pomáha a podporuje všetky formy náhradného rodičovstva.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2580
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pozrimeže, kto sa to objavil v pozadí „veľkej daňovej lúpeže“
                     
                                                         
                       Som plačúci Bratislavčan
                     
                                                         
                       Myslí to bratislavský Magistrát s čiernymi skládkami vážne? časť 4.
                     
                                                         
                       Pre tehotné deti nezdvihli úradníčky zadok ani telefón
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




