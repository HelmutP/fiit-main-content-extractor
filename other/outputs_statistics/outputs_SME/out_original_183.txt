
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lukáš Svoboda
                                        &gt;
                Moje názory
                     
                 Imatrikulácie 

        
            
                                    23.9.2007
            o
            18:16
                        |
            Karma článku:
                10.68
            |
            Prečítané 
            5256-krát
                    
         
     
         
             

                 
                    Vzhľadom k tomu, že chodím do 1. ročníka na vysokej škole, neminuli ma známe imatrikulácie, ktorých som  sa pred nedávnom zúčastnil.
                 

                  Hoci som mal, aké-také informácie o vysokoškolských imatrikulačkách ako prebiehajú, stále som nechcel uveriť  faktu, že prebehnú bez ponižovania a iných idiotských manierov vyšších študentov. Už len skutočnosť, že som  bol oblečený v obleku ma ako-tak ukľudňovala, že by to nemuselo byť nič hrozné. A ani nebolo. V podstate išlo  len o slávnostné privítanie nových študentov generalitou školy. Dostali sme imatrikulačný list a bolo po všetkom.  A čo na to povedať?? Len toľko, že je to úplne odlišné oproti strednej škole. Osobne som zažil stupídne  imatrikulácie na strednej škole, plné ponižovania, kriku a podobne. Nuž mal som smolu na štvrtákov, ktorí boli  naozaj najhorší za posledných 10 rokov na strednej škole. Jesť cesnak a iné kraviny, po ktorých vám je zle  tak ako ďakujem, neprosím. Lenže za štyri roky sa situácia opakuje. Hoci sa im imatrikulačky pred 4 rokmi nepáčili, spravia to isté novým prvákom.  Povedia si, prečo by sme im to nemohli trocha "osoliť" veď nás tiež nešetrili. A je to tu. Akási pomsta na  ľuďoch, ktorí za nič ani nemôžu. Minuloročnej imatrikulácie som sa ani nezúčastnil. Som zástanca pokojnej   imatrikulácie, bez nejakých hlúpich úloh. Lenže problém je v tom, že imatrikulácie na strednej škole robia   študenti a nie vedenie školy ako je to na vysokej škole. Nuž ale hoci by už štvrtáci mali byť dospelí, nevyzerá  to tak. Trocha rozumu by niektorým ani nezaškodilo, veď imatrikulačky sa dajú robiť aj iným spôsobom. Lenže  ak aj niekto má nejaký nápad, aj tak z toho nič nie je, pretože nik by ho nepočúval.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Svoboda 
                                        
                                            Ryanair, Stansted a moje skúsenosti s nimi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Svoboda 
                                        
                                            Prečo študenti chlastaju a nie len oni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Svoboda 
                                        
                                            Ľudia sú bezcharakterné svine
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Svoboda 
                                        
                                            5 najlepších ženských odpisov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lukáš Svoboda 
                                        
                                            Afrika a ako som to tam videl ja
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lukáš Svoboda
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lukáš Svoboda
            
         
        svoboda.blog.sme.sk (rss)
         
                                     
     
        autor studuje na UTB v Zline. Rad si pocuvne dobru hudbu a precita dobru knihu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2172
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Moje názory
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Adamová - Na lásku sa nezomiera...
                                     
                                                                             
                                            Učebnice jazyka C
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Kabát - Corida
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            KST Stará Turá
                                     
                                                                             
                                            eInvestor
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




