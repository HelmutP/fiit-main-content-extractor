

  
 Model si priblížime najprv niekoľkými fotografiami zo stavby 
  
 Detaily pripravené na "konečnú montáž" 
  
  
 Predloha modelu tvorí už takmer tri desaťročia najrozšírenejší typ železničného vozidla s vlastným pohonom vyrábaný v Československu - do prevádzky bolo nasadených 680 kusov, výrobcom bola vagónka v Studénke. 
  
 Súčasnosť týchto motorákov na Slovensku nebude mať dlhé trvanie. Väčšina strojov sa podrobila alebo ešte podrobí modernizácií a zvyšné vozidlá plánuje Železničná spoločnosť Slovensko vyradiť do konca roku 2011. Najpočetnejšia flotila 19 motorákov dosluhuje na východnom Slovensku v okolí Humenného. 
  
  
 Tri typické československé motoráky pózujú aj s preskleneným interiérom 
  
  
 Štyri modely série Železnice HO mám hotové. Posledný, piaty model bude nasledovať čoskoro... 
  
   

