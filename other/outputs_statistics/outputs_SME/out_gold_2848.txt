

      Predovšetkým mám dostatočne dobrú pamäť a Slovensko som neopustil, takže som kontinuálne sledoval dianie od tzv. revolučných dní až po dnes... 
 1. KDH je v preklade „Kresťansko demokratické hnutie" › za ostatných 20 rokov som nezažil, že by KDH bolo plne v súlade s pojmom „kresťanské" › zaujímalo by ma, či by predsa len nebolo lepšie, ak by sa KDH samo rozpustilo (už sme tu mali samoúnos, samocenzúru, samotrýznenie a podobné SAMO) 
 2. KDH môže za to, že vzniklo SDKÚ › keby nebolo iných dôvodov, tak tento jediný by stál za rozhodnutie nevoliť KDH.      Zaujímalo by ma, či by sa predsa len KDH akokoľvek nedalo rozpustiť... 
 3. KDH môže za to, že vzniklo KDS na čele s ex-poslancom za KDH, bývalým ministrom vnútra.      Zaujímalo by ma, či by súčasní členovia KDH nemohli prejsť na vyšší level tým, že by zrušili KDH ako celok. 
 4.Ak sa nemýlim, tak je práve aktuálna kauza s niekdajším členom KDH pánom Pavlom Bielikom › ako asi súvisí vnútorná demokracia KDH s tým, že P.B. bol vyzvaný Pavlom Hrušovským na odchod zo strany, ten sa však odvolal na prezumpciu neviny › ale akoby zázrakom sa stratilo okolo 45 nahrávok z policajných akcií, z čoho 7 sa týkalo kauzy Bielik...      Zaujímalo by ma, či by predsa len nebolo lepšie, aby sa KDH zrušilo? 
 5.KDH hlasovalo proti vzniku Slovenskej republiky.      Zaujímalo by ma, ako dlho bude ešte trvať, kým sa KDH nejako zruší... 
 6.KDH hlasovalo proti Ústave Slovenskej republiky.      Zaujímalo by ma, či je nejaká nádej na zrušenie KDH. 
 7.KDH v osobe Jána Čarnogurského › na konferencii „Európa a my" v Prahe vystúpil s prednáškou „Ako prekonať dynamickú silu národného princípu na Slovensku."      Zaujímalo by ma, či je v krátkodobom horizonte šanca na zrušenie KDH? 
 8.Niekdajší minister spravodlivosti Daniel Lipšic prišiel s ideou 3-krát a dosť (o ostatnom si nájdite na webe).      Nie som tak starý › hádam sa dožijem dňa, keď bude v televíznych novinách INFO o zrušení KDH... 
 9.KDH nepodporilo v našom parlamente snaženie EÚ o zmeny › kde šlo najmä o zjednodušenie rozhodovania EÚ, ktorá má už 27 členov.      Zaujímalo by ma, či niekto presvedčí členov KDH, že pre ich blaho a aj naše › by bolo viac ako prínosné › zrušiť KDH. 
 10.Jeden z mnohých citátov z Katechizmu › 1887: Zámena prostriedkov a cieľov - pri ktorej sa hodnota posledného cieľa dáva tomu, čo je iba prostriedok na jeho dosiahnutie, alebo sa osoby pokladajú za číre prostriedky vzhľadom na cieľ(909) - plodí nespravodlivé štruktúry, ktoré „sťažujú alebo prakticky znemožňujú kresťanské správanie, zhodujúce sa s prikázaniami najvyššieho Zákonodarcu". 
      Zrušenie KDH nie je až tak ťažké!, treba urobiť len prvý krok, tie ostatné sa pridajú... 
   
   

