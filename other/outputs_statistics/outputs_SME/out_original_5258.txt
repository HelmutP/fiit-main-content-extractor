
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alžbeta Iľašová
                                        &gt;
                Nezaradené
                     
                 Medzi kaňonmi 

        
            
                                    26.4.2010
            o
            1:53
                        (upravené
                26.4.2010
                o
                2:41)
                        |
            Karma článku:
                5.59
            |
            Prečítané 
            1166-krát
                    
         
     
         
             

                 
                    Z Ammoudary vyrážame v nedeľu o pol jedenástej, zostava posádky je rovnaká, a to Manos, Martina a ja. Dnešný cieľ cesty - Preveli – pláž a kláštor v tomto čarovnom kraji spadajúcom pod správnu oblasť Rethimno. Prvýkrát sa vydávame na juh ostrova, divoký, drsný a v tomto čase turistami ešte neobliehaný.
                 

                 
pláž Preveli 
   Cesta sa od Heraklionu tiahne smerom na západ, popri pobreží ideme asi 75 kilometrov. Zastavujeme nad dedinkou Bali s pekným výhľadom na maličký záliv.                       No hneď pokračujeme ďalej, máme toho pred sebou ešte dosť. Okolo Rethimna len prechádzame, nestojíme, iba vykukneme z auta. Typické prístavne mestečko s benátskou pevnosťou. Odbočujeme na juh a vchádzame do vnútrozemia. Ráz krajiny sa začína meniť. Kopce sa mi zdajú viac zelené a plné poľných kvetov, hlavne žltých podobných naším púpavám, ako drobné slniečka. Kde sa pozriem, všade sú malé kríky, ako ježkovia vykúkajúci z trávy, jeden učupený vedľa druhého. Schádzame z hlavnej cesty a po chvíli vchádzame do kamenného kaňonu – Kourtaliotiko Farangi (Kourtaliotikov kaňon). Na dne rokliny tečie malá riečka, či lepšie povedané potôčik ústiaci do mora. Zastavujeme uprostred kaňonu, na dne vidíme zhrdzavené auto, ako pripomienka, že tu nie človek, ale príroda má navrch.       Kľukatou cestou sa blížime k južnému pobrežiu, už cítime prímorský vzduch. No ešte sa musíme posilniť kofeínom, a tak zastavujeme v malej taverne vedľa starého benátskeho kamenného mosta, pod ktorým preteká už spomínaná riečka.      Cesta nás ďalej zavedie k starému kláštoru Kato Moni Preveli. Dnes sa už týčia len holé kamenné steny, niekde aj so strechami a zvláštnymi komínmi. Miesto je opustené, ale so špecifickou atmosférou. Ľahko sa dá presunúť do 16. storočia a predstaviť si kláštorný pokojný život uprostred tejto drsnej prírody, pohltený v tichu hôr a tieni olivovníkov. No hlavný kláštor Piso Preveli je ešte len pred nami. Prichádzame k nemu, tam na koniec sveta, kde konči cesta. Pod ňou je len ostrý spád a tyrkysové more. Rozhodli sme sa ale vychutnať si kláštor až neskôr, teraz chceme najskôr zísť k moru na slávnu pláž Preveli. Tak otáčame auto a asi po kilometri sa ostro bočíme doprava. Ďalej sa nedá, nechávame teda auto na parkovisku a schádzame po vlastných dole do neznáma. Cesta je upravená, skáčeme po spevnených schodoch a hltáme výhľad.       Je pod mrakom, no dusno, nohavice aj tričká sa nám lepia na telo, no nevadí nám to. Zostup trvá asi 20 minút a my sme s Martinou super vybavené "pohodlnými" žabkami. V duchu sa modlím, aby som si nevykĺbila členok a myslím na turistov v Tatrách, z ktorých sa stále smejem keď ich stretávam podobne obutých ako sme my dnes.. Už z polcesty vidíme pláž Preveli. Je výnimočná tým, že sa tam stretáva sladká voda so slanou. Riečka z kaňonu tam končí svoju púť. Schádzame až k pláži, ktorú napoly rozdeľuje koryto rieky. Prebrodíme sa cez neho, z výšky sa to možno ani nezdá, ale rieka má tam svoju silu. Piesok nie je zlatý ako sme si zvykli na severe, ale skôr čierny, a hrubší. Naľavo míňame palmy a kvitnúce oleandre a usádzame sa na konci pláže pod vysokou skalou. More je divoké, vlny vyššie a silné, dno príkro klesá. Kúpe sa tam len pár odvážlivcov a Manos, neskôr tam vlezie aj Martina, len ja zbabelo vzdorujem. V mori rovno pred nami je veľká skala v tvare srdca, ten úzky spodok jej zrejme vytvarovalo more počas mnohých rokov. Tam za srdcovou skalou v diaľke ležia brehy Afriky. Myslím, že ešte nikdy som jej nebola tak blízko.       S Manosom sa vydávame na obhliadku okolia. Stúpame do skál a zhora dovidíme na ďalšie pláže južného pobrežia, prázdne, bez ľudí. Pod nami ostré zrázy skál budia rešpekt, vrážajú do nich vlny, more mení farby od slabomodrej, cez tyrkysovú až po zelenú a hlbokú modrú.       Vraciame sa späť a už spoločne s Martinou vyrážame popri rieke do vnútra kaňonu. Strácame sa medzi vysokými palmami, miestni tam udržujú cestičku, a osekávajú vyčnievajúce palmové listy, ktoré sa nám už suché váľajú pod nohami. Ostré steblá tráv nás režú do nôh, cítim sa ako v pralese. Napravo stúpajú vysoké tehlovočervené skaliská, vľavo sa pokojne vinie riečka, pod nohami piesok.       Na brehu rieky rastú zvláštne krásne kvety. Vysoké s členitými listami a veľkými bordovočiernymi zamatovými kalichmi  (môj Tomáš mi zistil, že ten kvet sa volá Drakunkulus vulgaris, Drakovec obyčajný). Každým krokom žasneme nad dokonalosťou prírody.       Brodíme sa na druhú stranu rieky, Manos a Martina zmyjú zo seba soľ v sladkej vode, rieka tam tečie pomalšie a vytvára jazierko. Po druhom brehu sa vraciame späť k pláži a hneď stúpame po kamenných schodoch k parkovisku. Výhľad na pláž Preveli sa postupne vytráca a my smerujeme opäť ku kláštoru. Už pri vchode nás upozorňuje tabuľa, že fotiť je zakázané a tak smutná nechávam foťák v aute. Prechádzame najskôr múzeom, kde sú vystavené vzácne omšové rúcha, liturgické predmety aj bohato zdobené biskupské koruny. Celý kláštorný komplex je starý viac ako tisíc rokov, ale jednotlivé stavby majú rôznu dobu vzniku. Je zasadený do rázovitej krajiny, obklopený rozsiahlymi pozemkami s olivovníkmi a pasúcimi sa kozami, pod ním je more.       Kedysi patril k najvýznamnejším kláštorom Kréty, bol zapojený do mnohých protitureckých povstaní a počas druhej svetovej vojny sa stal centrom odporu proti nemeckým okupantom. V hornej časti komplexu sa nachádzajú mníšske cely so zvláštnymi špicatými komínmi, dole je malé nádvorie so studňou. Vchádzame do kláštorného kostola, ktorý je zasvätený apoštolovi Jánovi. Kostolík je malý, dvojloďový, plný starých a vzácnych ikon. Vnútri sa modlia traja mnísi, kývnu, že môžme vstúpiť. Sadám si do prítmia v zadnej časti a sledujem ich spevavé modlitby. Dvaja stoja po bokoch pred oltárom a pred nimi sú nádherne vyrezávané drevené stojany na knihy. Tretí sedí sám vzadu a v rukách drží dlhú šnúru s drevenými guličkami, ktoré postupne posúva, podobné ako náš ruženec, ale oveľa dlhšie. Len jeden z nich je v čiernom habite a na hlave má štvorcovú asi 20cm vysokú čiapku s dlhým závojom spusteným dozadu. Sedím, cítim pokoj tohto posvätného miesta a zvonku počuť spev pávov. Kostolom sa nesie zvláštna vôňa, horí len pár sviečok, dokázala by som tu ostať večnosť. Pomaly opúšťame kláštor a vraciame sa úplne vyhladovaní do našej taverny v príjemnom tieni ihličnanov vedľa kamenného mosta. Tzatziki, grécky šalát, králik a kuriatko v paradajkovej omáčke okamžite zmizne z našich tanierov. Na záver si ešte pripíjame ako je to tu zvykom s majiteľom reštaurácie raki. Neštrngáme si ale len buchneme pohárikmi o stôl. Keď pán videl, že nemám problém vypiť pohárik raki do dna, zakričal „bravó“.  A pri ďalšej runde už zisťuje odkiaľ som a či tam všetci tak pijú. Ta vychodňarka jak še patri. :-) Manos nám vysvetľuje, že preto si s nami domáci neštrngol, lebo mu asi niekto umrel v rodine. Tradícia hovorí, že sa smútok drží 40 dní a vtedy si rodina nesmie ani štrngnúť. Významný je tretí, deviaty a najdôležitejší je štyridsiaty deň, kedy sa zídu príbuzní a priatelia zosnulého.  Spokojní odchádzame domov. Po chvíli sme na severnom pobreží, cestu lemujú ružové oleandre a zapadajúce slnko sfarbuje nebo tiež do ružova. More je pokojné a vidieť už aj biely mesiac na oblohe. Ani jeden z nás nemá odvahu rušiť tieto krásne chvíle slovami. Pomaly sa pred nami ukazujú svetlá Heraklionu, sme doma. Juh ostrova v nás zanechal úžasné dojmy. Je nespútanejší a viac prekvapujúci ako obývanejší sever. Nedá sa to opísať, túto krajinu musíte milovať.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Hľadanie Dia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Grécka cesta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Veľká noc na Kréte
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alžbeta Iľašová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alžbeta Iľašová
            
         
        ilasova.blog.sme.sk (rss)
         
                                     
     
        Študentka šiesteho ročníka 1.lekárskej fakulty Univerzity Karlovej v Prahe, telom aj dušou hrdá východniarka - spišiačka, momentálne sa nachádzajúca v Heraklione, v hlavnom meste Kréty.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    969
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Jozef Klucho
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




