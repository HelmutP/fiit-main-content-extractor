
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jano Oravec
                                        &gt;
                Nezaradené
                     
                 Slovák som a Maďar budem. 

        
            
                                    1.6.2010
            o
            9:50
                        (upravené
                1.6.2010
                o
                9:55)
                        |
            Karma článku:
                24.95
            |
            Prečítané 
            8440-krát
                    
         
     
         
             

                 
                    Som Slovák, moji rodičia boli Slováci, aj moji starí rodičia boli Slováci. Žijem striedavo na Slovensku a v Kanade. V blízskej budúcnosti splním v Kanade zákonné podmienky na získanie kanadského občianstva a keďže v Kanade podnikám a kanadské občianstvo potrebujem, požiadam o občianstvo. Kanadské občianstvo budem verejne používať a preto ho priznám a stratím slovenské. Presnejšie povedané Róbert Fico mi svojim zákonom slovenské občianstvo ukradne.
                 

                 Keďže mám podnikateľské aktivity nielen v Kanade, ale aj v Európe, z mnohých dôvodov nevyhnutne potrebujem aj občianstvo Európskej únii. Využijem teda možnosť a požiadam o maďarské občianstvo. Ako som už spomenul, mám len slovenských predkov, ale som presvedčený, že podmienky spĺňam. Moji predkovia pochádzajú z Uhorska. Genetický výskum ukázal, že DNA Maďarov a Slovákov je veľmi podobná. Ba dokonca genetický výskum dokázal šokujúci fakt, keďže ázijské ugrofínske kmene po príchode do podunajskej nížiny mali viac žien a teda aj viac potomkov na hornom Uhorsku ako na dolnom Uhorsku, dnešní Maďari sú menší nositelia ázijsko-uralských (ugrofínskych) génov ako Slováci. Tento prekvapivý poznatok je pravdepodobne dôsledok toho, že vtedajšie horné Uhorsko bolo viac zaostalé a tak príslušníci ázijského kmeňa tu mali väčšiu moc a mohli si dovoliť mať aj 10 žien a potomkov s nimi. V každom prípade výsledok je vedecky dokázaný. A teda je pravdepodobné, že moja DNA (ale napríklad aj Slotova, Mečiarova, Ficova či Dzurindova) obsahuje väčšie percento ugrofínskych génov ako DNA Viktora Orbána. Som teda presvedčený, že aj z hľadiska pôvodu spĺňam podmienky na získanie maďarského občianstva. Po maďarsky neviem ani slovo, ale práve som si kúpil učebnicu maďarčiny a začal som sa učiť. Verím, že 12.júna 2010 urobíme súdruhovi Ficovi, ktorý zavinil moju situáciu škrt cez rozpočet, v opačnom prípade nebudem mať na výber, Slovák som a Maďar budem. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (94)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jano Oravec
            
         
        janooravec.blog.sme.sk (rss)
         
                                     
     
        Zijem striedavo v Kanade a na Slovensku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8440
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




