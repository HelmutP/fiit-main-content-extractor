
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Spoločnosť
                     
                 Čarnogurského džin, Slotov duch... 

        
            
                                    17.6.2010
            o
            7:00
                        (upravené
                17.6.2010
                o
                9:51)
                        |
            Karma článku:
                14.63
            |
            Prečítané 
            3458-krát
                    
         
     
         
             

                 
                    V prvom dokumente, v ktorom sa budúce koaličné strany hlásia k spolupráci sa hovorí, že po 20 rokoch sa etika musí znova zlúčiť s politikou. SDKÚ-DS, SaS, KDH a Most-Híd  reflektujú politickú realitu po parlamentných voľbách a chcú pod vedením Ivety Radičovej zostaviť vládu.
                 

                 
Džin. Kresťanský?archiv
   "Prerieknutie" sa predsedu KDH o Vatikánskej zmluve či o tom, ako sa z programových téz KDH dostane táto pasáž aj do programového vyhlásenia vlády znepokojili na Slovensku tých, ktorým sa po sobote uľavilo. Eufóriu síce pozorovať ako ojedinelý jav, lebo ako celok budí víťazný blok strán len opatrný optimizmus. 35% strany SMER vzbudzuje rešpekt a nabáda k pokore. A to je možno aj dobre, ale...    Z útrob tzv. Centra pre bioetickú reformu sa prostredníctvom zverejnených kontaktov na predsedu KDH má začať psychologický tlak a morálny lynč kvôli tomu, že "KDH má možnosť zakázať potraty a prijať Vatikánsku zmluvu s výhradou vo svedomí, ako i presadiť ďalšie bioetické princípy, ale KDH nemá záujem ani len vyjednávať!" Rozumej so súdruhom Ficom ako garantom verejnej mravnosti a morálnej integrity priemerného slovenského protipotratového jedinca. Poplašná správa pani Tutkovej pokračuje: "Smer dal však KDH neoficiálne vedieť, že príjme etické požiadavky KDH, vrátane zákazu potratov podľa poľského modelu. KDH však dalo ľahkovážny prísľub (podobne ako ten pred rokom Radičovej) bez akýchkoľvek podmienok, že so Smerom ani len nebude vyjednávať."    Ak sa vám to zdá málo, úvahy No-Choice aktivistky pokračujú tým smerom, že ak by KDH dostalo garancie od Smeru, má jedinečnú pozíciu, že Smer bude závisieť od KDH, a nie ako pred 4 rokmi, keď Smer nepotreboval KDH. Dnes sú okolnosti iné. Jeho voliči nie sú podľa pani Tutkovej primárne vyhranení voči Ficovi, ako (podľa nej) sú zásadoví v hodnotových veciach.   V závere svojho textu vypúšťa smerom na Jána Figeľa kresťanského ducha, o ktorom sa zmienil Ján Slota: "Zodpovednosť za životy a duše desiatok tisícov sú na pleciach Jána Figeľa. Povzbuďte ho, aby sa nebál zrušiť planú dohodu, do ktorej KDH aj tento rok vmanérvroval Daniel Lipšic.  Píšte, telefonujte, či SMSkujte Jánovi Figeľovi, aby okamžite predložil svojim liberálnym partnerom podmienku zakázať potraty a ak ju nepríjmu, aby šiel rokovať s Ficom. Je príliš málo utešovať sa dohodou, že nebudú uzákonené homosexuálne partnerstvá, lebo tie nám tu každú chvíľu aj tak natlačí Európska únia skrz Lisabonskú zmluvu."    Jánovi Figeľovi je po tieto dni potrebné zaželať pevné nervy a snáď aj nové telefóny s utajenými číslami... Kto verí v silu modlitby, môže sa za neho aj pomodliť. Uškodiť by to nemalo. Tá časť elektorátu novej koalície, ktorá sa nemodlí, nech vyvinie inú intelektuálno-duchovnú aktivitu.    Nuž takéto obsahy šíri slovenský kresťanský duch. Je dobré zaregistrovať toto mentálne partnerstvo pani Tutkovej, Jána Slotu, Jána Čarnogurského, Roberta Fica a jeho vlastnej hlavy. Záujmy, ktoré ich spájajú sú nejasné. Je jasné, aké misie a emisie spájajú po tom, čo sa stratil otec vlasti, zvyšky koaličného zlepenca. Aká misia spája pani Tutkovú a nevinného posla dobrých správ Jána Čarnogurského? Je to asi bezduchý tuzemský džin. Kresťanský? Mohla by to byť spravodajská hra na úrovni ŠtB. Rusofilstvo niektorých jej protagonistov však môže hovoriť aj o rovnako neetickej hre o nejaký ten rád vyššej. A možno o nič nejde...    Pani Tutkovej a pánovi Čarnogurskému je potrebné povedať v zmysle tuzemského ducha kresťanského, že ich SMERom cesta nevedie, lebo klamať, kradnúť, podplácať a klaňať sa iným idolom je hriech. Pán posol tento odkaz môže odniesť aj súdruhovi Ficovi a bratovi Slotovi. Odpovedi zo soboty títo páni nechcú stále rozumieť. A dáma nerozumie asi vôbec ničomu.    Aj preto je dobre, že v prvom dokumente, v ktorom sa budúce koaličné strany zatiaľ hlásia k budúcej vládnej spolupráci sa hovorí, že po 20 rokoch sa etika musí znova zlúčiť s politikou. Otázkou je už len ako na to. Zaregistrované partnerstvo duchov a džinov dáva tušiť všeličo. Aj netušené.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




