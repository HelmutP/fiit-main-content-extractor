
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Vyhláška 9/2009, ktorou sa vykonáva zákon o cestnej premávke 

        
            
                                    7.2.2009
            o
            20:39
                        |
            Karma článku:
                10.21
            |
            Prečítané 
            84289-krát
                    
         
     
         
             

                 
                    Text vyhlášky, ktorou sa vykonáva zákon o cestnej premávke nie je na počet paragrafov rozsiahly, má ich len 43, ale na druhej strane vyhláška má až 21 samostatných príloh.  Z tohto dôvodu som sa spravil analýzu vyhlášky, doplnil ju o funkciu hypertextových odkazov a každú prílohu dal samostatne tak, aby sa dala prezerať súčasne so základným textom vyhlášky.
                 

                 
  
   Najdôležitejšia časť vyhlášky je obsiahnutá v paragrafovom znení a v prvej prílohe, ktorá sa skladá z dvoch dielov. V prvom dieli je grafické vyobrazenie dopravných značiek a v druhom dieli je popísaný ich význam. Ostatné prílohy sú viac-menej administratívneho charakteru.   Vyhláška Ministerstva vnútra Slovenskej republiky 9/2009 Z. z., ktorou sa vykonáva zákon o cestnej premávke    Jednotlivé paragrafy vyhlášky:  § 1 Označenie spojnice pri vlečení motorových vozidiel a jej dĺžka (k § 34 ods. 6 zákona)  § 2 Osobitné označenie vozidiel(k § 44 ods. 9 zákona)  § 3 Označovanie prečnievajúceho nákladu (k § 51 ods. 5 zákona)  § 4 Označovanie osoby vykonávajúcej práce na ceste(k § 58 ods. 2 zákona)  § 5 Zvislé dopravné značky  § 6 Vodorovné dopravné značky  § 7 Dopravné zariadenia  § 8 (Dopravné značky a dopravné zariadenia odkaz na vyobrazenie)  § 9 Riadenie cestnej premávky svetelnými signálmi a pokynmi(k § 62 ods. 3 zákona)   § 10 --//--  § 11 Svetelné signály pre chodcov a cyklistov  § 12 Riadenie cestnej premávky pokynmi  § 13 Zastavovanie vozidiel (k § 63 ods. 3 zákona)  § 14 --//--  § 15 Organizácia vykonávania skúšok z odbornej spôsobilosti  § 16 --//--  § 17 --//--  § 18 Skúška z predpisov o cestnej premávke  § 19 Skúška z náuky o motorových vozidlách a ich údržbe  § 20 Skúška z vedenia motorových vozidiel  § 21 --//--  § 22 --//--  § 23 Rozsah odbornej prípravy skúšobných komisárov(k § 85 zákona)  § 24 Skúška na vydanie preukazu skúšobného komisára(k § 85 zákona)  § 25 --//--  § 26 --//--  § 27 Preukaz skúšobného komisára  § 28 Zdravotná a psychická spôsobilosť (k § 86 ods. 6 zákona)  § 29 Lekárska prehliadka (k § 87 ods. 6 zákona)  § 30 Psychologické vyšetrenie(k § 88 ods. 7 zákona)  § 31 Doklad o zdravotnej spôsobilosti a doklad o psychickej spôsobilosti(k § 89 ods. 2 zákona)  § 32 Vodičský preukaz (k § 94 ods. 4 zákona)  § 33 Medzinárodný vodičský preukaz(k § 101 ods. 8 zákona)  § 34 --//-- (k § 101 ods. 8 zákona)  § 35 Evidenčné číslo (k § 123 ods. 18 zákona)   § 36 --//--  § 37 --//-- (k § 125 ods. 4 zákona)  § 38 --//-- (k § 128 ods. 3 zákona)   § 39 Poznávacia značka Slovenskej republiky (k § 133 ods. 3 zákona)  § 40 --//-- (k § 140 ods. 8 zákona)  § 41 Prechodné ustanovenia  § 42 (EU)  § 43 Účinnosť   Text vyhlášky sa zobrazuje v novom okne po kliknutím na príslušný paragraf alebo  sem.  Ak chcete vo svojich článkoch, diskusiách odkazovať na konkrétny paragraf zákona, môžete to spraviť linkom, kde za znakom mreža uvediete požadované číslo paragrafu. Napríklad:   http://blog.sme.sk/blog/115/182028/2009-9-vyhlaska-k-zakonu-o-cestnej-premavke.html#43   Prílohy k vyhláške č. 9/2009 Z. z.   
  
Príloha č. 1 
1.diel - Vyobrazenie a vzory dopravných značiek, dopravných zariadení, ich čísla, rozmery, farby a presné vyhotovenie dopravných značiek, dopravných zariadení (1,5 MB)
  
Príloha č. 1 
2.diel - Význam dopravných značiek, dopravných zariadení a osobitných označení (302 KB)
   
Príloha č. 2 Žiadosť o udelenie vodičského oprávnenia (236 KB)
  
Príloha č. 3
Vzor protokolu o skúške z odbornej spôsobilosti
  
Príloha č. 4
Vzor preukazu skúšobného komisára
  
Príloha č. 5 Minimálne požiadavky na úroveň telesnej schopnosti a duševnej schopnosti viesť motorové vozidlo a spôsob ich posudzovania
  
Príloha č. 6 Čestné vyhlásenie posudzovanej osoby vo vzťahu k jej zdravotnej spôsobilosti na vedenie motorových vozidiel
  
Príloha č. 7 Doklad o zdravotnej spôsobilosti žiadateľa o udelenie vodičského oprávnenia/ vodiča, ktorý sa podrobuje preskúmaniu zdravotnej spôsobilosti/ vodiča, ktorému bolo odobraté vodičské oprávnenie
 
Príloha č. 8 Doklad o zdravotnej spôsobilosti vodiča
  
Príloha č. 9 Ďalšie podmienky pre vodiča alebo úpravy motorového vozidla
  
Príloha č. 10 Kritériá na posudzovanie psychickej spôsobilosti
  
Príloha č. 11 Čestné vyhlásenie posudzovanej osoby vo vzťahu k jej psychickej spôsobilosti na vedenie motorových vozidiel 
  
Príloha č. 12 Doklad o psychickej spôsobilosti žiadateľa o udelenie vodičského oprávnenia/ vodiča, ktorý sa podrobuje preskúmaniu psychickej spôsobilosti/ vodiča, ktorému bolo odobraté vodičské oprávnenie
  
Príloha č. 13
Doklad o psychickej spôsobilosti vodiča
  
Príloha č. 14
Vzor vodičského preukazu (112 KB)
  
Príloha č. 15
Vzor čistopisu medzinárodného vodičského preukazu podľa Viedenského dohovoru (774 KB)
  
Príloha č. 16
Vzor čistopisu medzinárodného vodičského preukazu podľa Ženevského dohovoru (1,8 MB)
  
Príloha č. 17
Vzory tabuliek s evidenčným číslom (780 KB)
  
Príloha č. 18
Vzory tabuliek s evidenčným číslom vozidiel cudzích zastupiteľských úradov na území Slovenskej republiky, vozidiel administratívneho a technického personálu cudzích zastupiteľských úradov so sídlom na území Slovenskej republiky a obchodných zastupiteľstiev zriadených diplomatickou misiou a značiek s písmenami CD a CC (238 KB)
  
Príloha č. 19
Vzor tlačiva na vedenie prehľadu o manipulácii s tabuľkami so zvláštnym evidenčným číslom a o ich prideľovaní, tlačiva o pridelení zvláštneho evidenčného čísla a osvedčenia o pridelení zvláštneho evidenčného čísla
  
Príloha č. 20
Vzor poznávacej značky Slovenskej republiky
  
Príloha č. 21
Zoznam preberaných právnych aktov Európskych spoločenstiev a Európskej únie
    Jednotlivé prílohy sa zobrazujú v novom okne po kliknutím na príslušné číslo prílohy alebo dielu prílohy.   Prajem veľa trpezlivosti pri štúdiu a nezabudnite ani na zákon 8/2009 Z. z. o cestnej premávke. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




