
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Iliaš
                                        &gt;
                Energia
                     
                 Energia: slnko verzus vietor 

        
            
                                    17.6.2010
            o
            14:25
                        |
            Karma článku:
                5.69
            |
            Prečítané 
            1344-krát
                    
         
     
         
             

                 
                    Fanúšikovia obnoviteľných energetických zdrojov pozor! Do ringu sa derie svojimi lúčmi slnko. Vyzývateľ vietor si už mädlí vrtule. Kto zvíťazí?
                 

                 Aj medzi jednotlivými druhmi obnoviteľných energetických zdrojov existuje konkurencia - oplatí sa bojovať o balík dotácií, či už priamych, alebo nepriamych. Priame dotácie (eurofondy a iné fondy, čiže v podstate peniaze daňových poplatníkov) poznáme, nepriame sú skryté vo forme niekoľko násobne vyššej štátom či nami spotrebiteľmi dotovanej ceny vyrobenej elektriny v porovnaní s cenou, za ktorú sa elektrina nám smrteľníkom predáva.        Sám som bol zvedavý, kto by zvíťazil vo virtuálnom súboji, ak by sme porovnávali veternú elektráreň s fotovoltickou v slovenských klimatických podmienkach s rovnakým záberom pôdy. Výsledok som síce očakával, ale až taký veľký rozdiel prekvapil. V tomto článku som údaje čerpal z reálnych zámerov výstavby zariadení zverejnených v informačnom systéme [3] hodnotenia vplyvov na životné prostredie (tzv. EIA).        Zatiaľ čo panický strach z veterných turbín na Slovensku, ktoré sú vraj hlučné a špatia krajinu, zabíjajú vtáky, či spôsobujú epilepsiu a iné smrtonosné dôsledky pretrváva dodnes, voči fotovoltickým elektrárňam odpor verejnosti prakticky neexistuje. Mimochodom, okrem estetického hľadiska sa takmer žiadne z negatív využívania veternej energie v praxi nepreukázali. Väčšina navrhovaných veterných elektrární získala kladné odporúčacie stanovisko v procese hodnotenia vplyvov na životné prostredie.        V tomto porovnaní sa pozrieme bližšie na možnosti výroby elektriny z vetra a slnka čisto z technického hľadiska: koľko môžeme očakávať elektriny pri rovnakom zábere pôdy?       Na postavenie jednej turbíny veternej elektrárne potrebujete pozemok s dostatočnou veternosťou. Aby som predišiel obvineniam z tendenčnosti, vo virtuálnom súboji vetra so slnkom použijem príklad jedinej dlhodobo fungujúcej veternej elektrárne na Slovensku. V Cerovej sú inštalované 4 veterné turbíny, každá s výkonom 0,66 kW, spolu 2,64 MW. Keďže veterný park je v prevádzke už niekoľko rokov, údaj o predpokladanom množstve vyrobenej elektriny je ťažko spochybniteľný. Predpokladané množstvo elektriny dodanej do distribučnej sústavy teda činí 5 000 MWh za rok [1]. Na jednu turbínu to je 1 667 MWh dodanej elektriny za rok.        Z pripravovaných zámerov výstavby veterných elektrární na Slovensku si vieme spraviť aký-taký obraz o zábere pôdy. V tomto údaji sú už zahrnuté aj prístupové cesty.       Tab.: Predpokladaný záber pôdy navrhovanými veternými turbínami na Slovensku [3] (výber)       Lokalita     Počet   turbín   [ks]     Inštalovaný   výkon   [MW]     Predpoklad výroby   elektriny   [MWh/r]     Záber pôdy   [m2]     Priemer vyrobenej   elektriny   [MWh/m2.rok]             Nitra, Čab (variant 2)     1     2     4 500     1 200     3,750         Veľké Zálužie (variant 3)     3     6     15 000     5 908     2,539         Cerová II – Kopánky (variant 1)     1     2     4 000     7 455     0,537         Myjava, Vesný Vrch     4     8     18 000     14 643     1,229         Horná Kráľová (variant C)     8     22     44 640     12 800     3,488         Orechová Potôň (variant 3)     15     30     n/a     31 069     n/a                                 PRIEMER:     2,308             Na jednu turbínu, ktorá vyrobí cca 4 700 MWh elektriny teda potrebujeme okolo 3 000 m2 plochy (predstavte si obdĺžnik so stranami 30 m x 100 m, čo je asi polovica futbalového ihriska). Pre porovnanie, spotreba priemernej slovenskej domácnosti sa pohybuje okolo 2,3 MWh elektriny za rok. Takáto priemerná turbína by teda mohla zásobovať cca 2 000 domácností. Koľko by sme na rovnakej ploche vedeli vyrobiť elektriny zo slnka vo fotovoltických paneloch?           Tab.: Predpokladaný záber plochy navrhovanými fotovoltickými elektrárňami na Slovensku [3] (výber)       Lokalita     Inštalovaný výkon    [MWp]     Predpoklad výroby elektriny    [MWh/r]     Záber plochy    [m2]     Priemer vyrobenej elektriny [MWh/m2.rok]             Hurbanovo I, II, III     11,10     11 801     232 709     0,051         Moldava nad Bodvou     7,94     8 559     187 000     0,046         Tornaľa I, II, III     11,97     10 773     257 000     0,042         Gamboš I     1,00     999     33 000     0,030         Gamboš II     1,00     999     33 000     0,030                           PRIEMER:     0,040             Ako môžeme vyčítať z tabuliek vyššie, z jedného m2 plochy vieme získať niekoľko násobne viac elektriny z vetra ako zo slnka. Je to logické, nakoľko slnečná energia dopadá na zemský povrch vo forme slnečného žiarenia značne „zriedená“, zatiaľ čo veterná energia už predstavuje „zhustenú“ slnečnú energiu, ktorá sa prejavuje prúdením vzduchu vzniknutým nerovnomerným ohrevom na oveľa väčších plochách Zeme. Veterné turbíny vyrábajú elektrinu aj v noci, v zime je to dokonca viac ako v lete. Fotovoltické panely musia byť umiestnené v rozstupoch, aby si navzájom netienili, čo tiež zvyšuje záber plôch.        Ak si porovnáme pevne stanovené ceny elektriny z obnoviteľných energetických zdrojov, ktoré musí (je na to zákon) zaplatiť distribútor výrobcovi elektriny, zo súboja vyjde opäť víťazne vietor. Cena elektriny [4] je stanovená pre veterné elektrárne 80,91 Eur/MWh; pre slnečné elektrárne (s výkonom nad 100 kW) je cena 425,12 Eur/MWh. Pre porovnanie, cena elektriny, ktorou doma svietite, sa pohybuje okolo 134,81 Eur/MWh. To znamená, že elektrina z vetra sa už dnes dá vyrobiť lacnejšie ako je jej konečná cena pre domácnosti.        Z porovnania vychádza, že veterné elektrárne sú v rámci obnoviteľných zdrojov pomerne lacný zdroj elektriny. Veterné parky nevyžadujú veľký záber pôdy a z hľadiska využitia územia sa ukazujú ako najúspornejšie s malými nákladmi na prevádzku. Otázna ostáva regulácia výroby elektriny – ani slnku ani vetru sa (našťastie) nedá rozkázať.                Zdroje informácií:   [1]        Úrad pre reguláciu sieťových odvetví: zoznam potvrdení o výrobe elektriny z obnoviteľných zdrojov: http://www.urso.gov.sk/sk/regulacia/elektroenergetika/e-oze   [2]        D. Kudelas, R. Rybár: Wind Energy in Slovakia. Život. Prostr., Vol. 40, No. 3, p. 133 – 136, 2006.   [3]        Enviroportál – Informačný systém hodnotenia vplyvov na životné prostredie (EIA): http://eia.enviroportal.sk/zoznam    [4]        Výnos ÚRSO 7/2009         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Prečo sa cirkev pletie do politiky, alebo o dvoch ríšach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Cirkev a peniaze
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Zachránia nás elektromobily ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Indiáni, EÚ, inžinier a kedy sa minie ropa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Nie je to až také zlé
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Iliaš
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Iliaš
            
         
        igorilias.blog.sme.sk (rss)
         
                                     
     
        Kresťan, inžinier, otec... pracujem v oblasti tepelnej energetiky.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4822
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Energia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Obamov omyl s elektromobilmi
                                     
                                                                             
                                            CO2 a teploty: Popletená příčina a důsledek
                                     
                                                                             
                                            Sloboda prejavu - Je každý antiputinovec hrdina ?
                                     
                                                                             
                                            Stredovek - doba temna?
                                     
                                                                             
                                            Nie Euroválovu
                                     
                                                                             
                                            Národná tragédia
                                     
                                                                             
                                            Pán prezident, nezavadzajte!
                                     
                                                                             
                                            Rakúsko je dosť blízko
                                     
                                                                             
                                            Aby ste v Londýne niekoho neurazili!
                                     
                                                                             
                                            Svätí arizátori, orodujte za nás
                                     
                                                                             
                                            Konečne diskusia o odluke cirkví
                                     
                                                                             
                                            Ale čo ak sa naozaj neotepľuje?
                                     
                                                                             
                                            Podľa prezidenta som hrozba pre mier
                                     
                                                                             
                                            Pozeral som Modré z neba. Dvadsať sekúnd
                                     
                                                                             
                                            Keď pomoc nepomáha ale škodí
                                     
                                                                             
                                            Modly, ktorým sa klaniame
                                     
                                                                             
                                            Na Figeľa tlačia fanatickí kresťania
                                     
                                                                             
                                            Robotný Cigáň
                                     
                                                                             
                                            Čo mi vadí na súčasnej homo eufórii?
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio 7
                                     
                                                                             
                                            Experimental_FM
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pavel Kábrt
                                     
                                                                             
                                            Iný môj blog na Evanjelik.sk
                                     
                                                                             
                                            Miroslav Lettrich
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Evanjelik
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




