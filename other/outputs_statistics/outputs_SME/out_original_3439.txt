
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radovan Potočár
                                        &gt;
                Školy
                     
                 Dva roky školskej reformy 

        
            
                                    27.3.2010
            o
            17:24
                        (upravené
                17.2.2011
                o
                2:52)
                        |
            Karma článku:
                15.85
            |
            Prečítané 
            3611-krát
                    
         
     
         
             

                 
                    „Mikoľajená reforma“ či „deforma školstva“ – i takéto označenia prischli na školách reforme, ktorá v praxi funguje už takmer dva roky. Ako sa za ten čas zmenil život na základných a stredných školách?
                 

                 
Učebnice slovenského jazyka a literatúry pre 2. ročník gymnáziiP. Šturc
   Bolestivý pôrod   Už začiatok príbehu o tom, ako sa dostať z blata rovno do kaluže, neveštil slovenským školám nič dobré. Zmeny pretláčané zhora nadol sa totiž zvyčajne minú žiadanému účinku. Zatvorené dvere ministerstva, chaotické bľabotanie pána ministra a zmetení učitelia sa ihneď stali epitafom celej reformy.   Dá sa povedať, že informácie o reforme boli až do posledných chvíľ prísne utajované, pravdepodobne oveľa dôslednejšie než zoznamy príslušníkov SIS. Jediné, o čo sa mohli učitelia dlhú dobu opierať boli nezmyselné klábosy a rôzne stratégie povešané po stránkach ministerstva či školských úradov a ak mali šťastie, možno čo - to zachytili z médii.   Letné prázdniny v roku 2008 sa už plne niesli v znamení reformy, lepšie povedané - očakávania reformy. Učitelia trávili cez prázdniny nemalé množstvo času vypisovaním školských vzdelávacích programov, bleskurýchlym prepisovaním učebných plánov, ale najviac času im asi zabralo krútenie hlavou nad tým, čo sa to na naše školy rúti. Myslím si, že každá škola by mala mať vytýčené svoje ciele, cieľavedomý program a stanovené priority, čím by rodičia dostali ďalšiu pomoc pri rozhodovaní sa, na ktorú školu svoje dieťa zapíšu. Školské vzdelávacie programy sa však v réžii pána Mikolaja zmenili na frašku.    Keď žiaci prvého a piateho ročníka základných škôl a prvého ročníka stredných škôl v septembri 2008 vbehli do škôl, zdalo sa, akoby niekoho vyrušili. Pán Mikolaj očividne nepočítal s tým, že učitelia by popri všetkých programoch, formulároch, plánoch a školeniach radi aj občas niečo odučili. Namiesto toho, aby reforma priniesla učiteľom viac slobody         a kreatívneho prístupu k práci, spravila z nich otrokov administratívy.       Reforma na plné obrátky        Prvé týždne školského roka 2008/2009 sme zvedavo gúľali očami po škole a hľadali známky reformy, o ktorej čosi písali v novinách, ale bohužiaľ sme nič nové nenašli. Telocvičňa s omietkou, ktorá nám padá na hlavu, triedy s nábytkom, ktorý by vyhodili aj z väznice, chemické laboratórium s vybavením, ktoré si aj moji rodičia pamätajú zo študentských čias ako zastarané. Jedinými známkami reformy, ktoré sme našli, boli zmätení učitelia.   Tí sa už po prvých týždňoch fungovania reformy začali deliť na dve skupiny. Prvá, tá väčšia, sa k reforme radšej ani nevyjadrovala a mlčky plnila pokyny zhora. Druhá, trochu menej početná, reformu v podstate otvorene kritizovala a do nových osnov sa snažila vtesnať aspoň čo-to zo starých koncepcii a vniesť do neporiadku aspoň závan logiky a zdravého rozumu. Zakrátko sa ukázalo, že reforma zo školského systému nespravila hnací motor, ale brzdu vzdelávania na slovenských školách.   Jedným z najväčších sklamaní, ktoré reforma priniesla pre mňa, bol slovenský jazyk a literatúra - „po novom". Do popredia vystúpila gramatika, literatúra sa odsunula celkom na okraj. Usporiadaniu tematických celkov v literatúre na gymnáziách pritom zrejme nerozumie ani pán minister. Tento školský rok sme napríklad začali E. M. Remarqueom, povedali si niečo o expresionizme a hneď nato sme sa učili o rozdelení epitet, o básnických symboloch a Ivanovi Kraskovi. Potom sme úplne plynulo (čiarou v zošite) prešli na drámu a divadlo, aby sme hneď vzápätí skočili na časomerný veršový systém. Po časomiere sme hovorili o Jankovi Jesenskom, rozdelení lyriky a na poslednej hodiny sme sa naučili niečo o dekadencii. Keď sme sa pani učiteľky spýtali, či v tom vidí nejaký systém, pokrútila hlavou a povzdychla si. Nechcel by som byť na jej mieste.   Podobným spôsobom sa zreorganizovali aj iné predmety, pričom reforma zväčša priniesla zmenu poradia z 1,2,3,4,5,6,7,8,9,10 na premyslený systém 4,7,1,5,3,10,8,2,6,9. Ak je situácia na základných školách rovnaká ako na gymnáziách, vôbec by ma neprekvapilo, ak by sa prváčikovia naučili písať E, T, R, S a učiteľka od nich mala podľa reformy vyžadovať, aby napísali slovo MAMA.   Osobitnou kapitolou celej reformy sú učebnice. Chýbajú nám takmer všetky a dá sa povedať, že istí si môžeme byť len tými, ktoré nám rodičia musia kúpiť. Učitelia biológie, dejepisu, geografie či chémie sa tak veľakrát môžu opierať len o to, čo poskytujú staré učebnice používané pred reformou. Na slovenský jazyk a literatúru používame napríklad tento rok dovedna osem učebníc. Áno, osem! Časť preberaného učebnica sa totiž nachádza v jednej, časť v druhej, tretia sa nedá nájsť nikde (viď. Obr.). Popri tom sa ešte vydalo niekoľko nových učebníc... A namiesto školských tašiek idú na dračku školské vrecia.       Šiesta iks       Naša trieda sa nedávno musela premenovať zo sexty na VI.X. Pán minister totiž vymyslel, že názvom ako prima, sekunda, tercia... nikto nerozumie a latinské názvy treba nahradiť. Ja mám však skôr pocit, že väčšina ľudí nerozumie, odkiaľ sa berú miliónové majetky pána Slotu, tryskáče a drahé autá. Ak má pán minister chuť ozrejmovať, nemusí meniť zaužívané názvy tried. Stačí vysvetliť, ako sa dá prísť spoločensky unavený do parlamentu, rabovať z verejného majetku aj priklincované či ako sa dá podpisovať do prezenčnej listiny večného absentéra a na druhej strane hlásať vznešené heslá o láske k národu a vlasti.       Školská reforma však rozhodne priniesla aj pozitíva. Napríklad hodiny biológie nám pán minister umožnil využívať z deväťdesiatich percent času trénovaním písania poznámok, keďže už sedem mesiacov nieto učebníc a učiť sa nám treba. Ďalším prínosom je, že žiaci slovenských škôl majú jedinečnú možnosť pochopiť význam slov Marka Twaina: „Nikdy som nedovolil, aby škola stála v ceste môjmu vzdelaniu." A taktiež nás pán minister naučil, že nič nie je také zlé, aby nemohlo byť ešte horšie.   Ak sa má naše školstvo raz pohnúť k lepšiemu, sú potrebné veľké zmeny. Pokrytectvo a aroganciu na najvyšších pozíciách v rezorte musí vystriedať pokora, slušnosť a pripravenosť na dialóg. Pokým k takýmto zmenám nedôjde, učitelia zostanú úradníkmi, ktorí musia sem-tam odbehnúť učiť a vzdelanie degradované na diplom a titul. Kým brány školy raz prekročia moje deti, budem robiť všetko preto, aby sa obrat o 180 stupňov mohol podariť.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Yankees go home! (desať minút s národniarom)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Bod nehybnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            O dvanástich kartičkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Darjeeling - Goa - Bombaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Vyzlečte sa, zvyknite si. Couchsurfing
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radovan Potočár
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radovan Potočár
            
         
        potocar.blog.sme.sk (rss)
         
                        VIP
                             
     
         Boli Sme. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    96
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3328
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vážne veci
                        
                     
                                     
                        
                            Vlažne vážne
                        
                     
                                     
                        
                            Trochu scesty
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Školy
                        
                     
                                     
                        
                            Esej?
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Yankees go home! (desať minút s národniarom)
                     
                                                         
                       Prechádzky čínskymi rozprávkami
                     
                                                         
                       Ako Fico priviedol Slovensko k bankrotu
                     
                                                         
                       Ľudia, odpojte sa!
                     
                                                         
                       Byt, kola, pivnica, vínko, Boh a Výboh ....
                     
                                                         
                       Prvýkrát
                     
                                                         
                       Cukornička
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Trnka vracia úder ....lebo iba " trtoší"  ?
                     
                                                         
                       David Luiz do PSG? Zbytočne vyhodené peniaze
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




