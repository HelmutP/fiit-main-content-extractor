

 Vtedy, ešte v minulom roku, keď som na tom bol nebolo rozšírené až tak toto dielo, jednoducho ľudia sa báli, či to za tú cenu (7 éčok!!! bez zľavy!!!) nebude ďalšia sci-fi kravina... Veľmi som tomuto filmu prial, aby si na seba aspoň zarobil, lebo Cameron od Titanicu nevyprodukoval nič zarobkuhodné a ak by sa Avatar nevydaril, tak by sa pravdepodobne pochoval... 
 Teraz už je zjavné, že ľudia modrú farbu proste milujú. Kiná sú po 20tich dňoch obehu Avatara stále plné. Všetko funguje ďalej a zatiaľ nevznikla žiadna kritika filmu Avatar, ktorá by ho dokázala nejako skompromitovať. Teda okrem Pocahontas. 
 A Môj osobný názor na tento film? Z predchádzajúceho textu ste ho už určite pochopili. 
 Podľa mňa by sa mohol považovať za jeden znajprevratnejších filmov tohto desaťročia. Filmové efekty boli skutočne super. Na tej modrej "oblude" sa hýbali aj tie najmenšie vlásočky, mimika fungovala dokonale, jediné čo bolo zavádzajúce bola vraj matka všetkých bitiek, ktorá vôbec nebola najväčšia bitka, ale iba menšia zábava počítačových grafikov. Film bol strhujúci, príbeh priemerný - nie je možné pri dnešnom produkovaní filmov vytvoriť film s naozajstným dostávajúcim príbehom, to musí byť fakt len podľa skutočnej udalosti. 
 Na záver len, že určite v najbližšej dobe bežte na Avatara, je to najlepšie vyhodených 7 euro za film, aspoň nateraz, kým nepríde Avatar 2. 
  

