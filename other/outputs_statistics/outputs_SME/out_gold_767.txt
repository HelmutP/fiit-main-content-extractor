

 
Drevorubač ju ignoruje a reže ďalej. 
Konár povolí a chlapík spadne na zem. 
Drevorubač sa pozviecha, rozbehne sa za starenkou a kričí: "Babička, vyveštite mi ešte niečo!"
 
 
Juraj Lukáč z Lesoochranárskeho zoskupenia VLK už roky opakuje: Keď budete pokračovať v rúbaní lesov na východnom Slovensku, letné búrky budú znamenať tragické povodne. Termín určí na začiatok júla, a miesto vyberie podľa toho, kde vznikol holorub v "lievikovitom" teréne.  
 
 
A ono sa to stane, Lukáčova predpoveď  sa splní.    
 
 
Mágia? Nie, je to kombinácia troch poznatkov zo stredoškolského zemepisu. 
 
 
1. zdravý les má vysokú vodozádržnú funkciu, vyrúbaná holina má retenčnú schopnosť oveľa menšiu 
 
 
2. prevažujúce geologické podložie na východnom Slovensku je flyš - hornina zložená z vrstiev pieskovcov a ílovcov, pričom ílovce sú nepriepustné
 
 
3. v lete sa u nás pravidelne opakuje tzv. "Medardova kvapka", obdobie zvýšených zrážok a ochladenia, nazývané aj "európsky monzún"
 
 
Potiaľto je všetko jasné.  Ťažšie sa však chápe, prečo lesníci stále pokračujú v odlesňovaní chrbátov východoslovenských pohorí. 
 
 
Ako hovoria kriminalisti - ak nevieš, aký je motív, sú za tým peniaze. Ak za tým nenájdeš peniaze, sú za tým strašne veľké peniaze.
 
 
Peniaze z ťažby dreva končia na súkromných účtoch, veď príjem z ťažby dreva do štátneho rozpočtu je mizivý. Na pomoc obyvateľom vytopených oblastí však nejdú peniaze z týchto účtov, ale zo spoločného. Sústrasť pozostalým tiež nepríde vyjadriť riaditeľ ťažiarskej firmy, ale predseda vlády.  
 
 
Dokiaľ bude náš štát - teda my všetci - poisťovňou, ktorá bez problémov prepláca úrazy a rozbité motorové píly, dovtedy nám tu budú zo stromov padať drevorubači. A Juro Lukáč bude "veštiť" povodne s vyššou úspešnosťou ako Nostradamus.
 

