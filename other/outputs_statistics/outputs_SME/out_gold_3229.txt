
 Mária je maturantka, má osem súrodencov. Napriek tomu, že rodičia pracujú zo všetkých síl, nemôžu sa dostať zo stavu hmotnej núdze. Mária sa učí skoro na samé jednotky, podobne aj jej súrodenci patria k zodpovedným žiakom, takto ich vychovávajú ich rodičia, síce v skromnosti, ale k tomu, aby si na chlieb zarábali svojou pracovitosťou. Rodina Jany sa ocitla v hmotnej núdzi po tom, ako ich otec nastúpil na invalidný dôchodok po tom, ako začal mať vážne zdravotné problémy. Jana patrí medzi najlepšie žiačky v triede, do školy chodieva rada a má predpoklady, že by to mohla dotiahnuť aj ďalej

Chcel by som touto cestou poďakovať všetkým, ktorí spolu so mnou pomáhajú prežiť radostnejšie detstvo a mladosť aj Štefanovi, Márii a Jane, budúcnosť viacerých chudobných detí sa môže výrazne zmeniť vďaka pomerne malým mesačným štipendiám. Ak viete o niekom, koho by sme mohli zaradiť do tohto programu, dajte nám prosím vedieť. Pomôžeme toľkým, koľkým budeme vládať.

Budem tu uverejňovať príbehy všetkých štipendistov, aby ste vedeli komu Vaše peniažky konkrétne pomáhajú. Ak by sa chcel niekto zapojiť do tohto projektu, môže poslať svoj príspevok na číslo účtu: 261 895 2811/1100 Tatrabanka.
Ak by niekto chcel navrhnúť nejakých nových štipendistov, nech mi pošle konkrétny návrh na moju adresu: cerven7777@gmail.com 
