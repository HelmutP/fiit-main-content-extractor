

 Človek cez nekonečné bolesti 
 veľakrát vypestuje nádhernú záhradu. 
 Ale i tak musí zasvietiť slnko, 
 aby rozkvitla. 
   
 Už sa nepýtam, 
 prečo som sa ťa stretol. 
 Už sa len teším 
 zo záhady záhrady. 
   
 A každým krokom 
 cez mlčanie a úsmev 
 rozdávam návody. 
   
   
   
   
 Buďte šťastní, Hirax 
   
 Aktivity Hiraxa: 
 Pondelok, 18. 5. 2010, Svet, Oficiálne vydanie Hiraxovej druhej básnickej zbierky Nech je nebo všade, ktorá bude pokračovaním priaznivo prijatého debutu More srdca. Knižočka bude obsahovať ďalších, približne 90 básni, aforizmov, bájok, zamyslení a tak ako pri prvých básniach bude ilustrovaná skvelými obrázkami od Janky Thomkovej. Druhý titul bude panamsko-kostarický cestopis Úcta k prírode a úsmev ako zmysel života. Väčšia časť knihy sa bude venovať pobytu v pralese Darién u Indiánov z kmeňa Emberá. Cestopis bude obsahovať približne 130 plnofarebných fotiek a okrem Hiraxovho autentického popisu písaného priamo zo srdca, bude titul niesť aj teoretické časti o živote a zvykov spomínaných Indiánov. Obidve knihy vyjdú u knižného vydavateľstva HladoHlas. 
 Streda, 19. 5. 2010 o 17:32, Žilina, kaviareň Kaťuša (Mariánske námestie - medzi Tatra bankou a reštauráciou Voyage). Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a Kostarikou s hlavným dôrazom na pobyt v panamskej džungli Darién u Indiánov Emberá. V tomto čase by mala byť vonka už aj knižná podoba tohto panamského cestopisu (a troška aj Kostarického :-)) s názvom "Úcta k prírode a úsmev ako zmysel života", ktorý vyjde začiatkom mája 2010. Vstup jeden úsmev. 
 Štvrtok, 20. 5. 2010 o 11:11, Sučany, gymnázium - súkromná recitácia. Čítačka Pavla "Hiraxa" Baričáka, kde porozpráva o svojich románoch a prečíta nejaké básne. Vstup jeden úsmev. 
 Štvrtok, 20. 5. 2010 o 18:32, Martin, kaviareň Kamala. Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a Kostarikou s hlavným dôrazom na pobyt v panamskej džungli Darién u Indiánov Emberá. V tomto čase by mala byť vonka už aj knižná podoba tohto panamského cestopisu (a troška aj Kostarického :-)) s názvom "Úcta k prírode a úsmev ako zmysel života", ktorý vyjde začiatkom mája 2010. Vstup jeden úsmev. 
 Utorok, 25. 5. 2010, Žilina, "Bez hraníc". Slovensko-poľský literárny festival. Moje čítačky budú upresnené. 
 Streda, 26. 5. 2010, Banská Bystrica, verejné čítanie na námestí v BB prebiehajúce od doobedňajších hodín do večera. Predbežný čas mojej "čítačky-rozhovoru-dialógu" je od 16:30 do 17:30. 
 Štvrtok, 27. 5. 2010, Žilina, "Bez hraníc". Slovensko-poľský literárny festival. Moje čítačky budú upresnené. 
 Streda, 16. 6. 2010, Bratislava, Kníhkupectvo MODUL- Svet knihy (v centre mesta, Obchodná), Od 16:00 Hiraxova čítačka k vydanému cestopisu a básniam Nech je nebo všade, od 17:00 krst spomínaných dvoch kníh + románu od slovenskej autorky Olivie Olivieri a jej debutu "Cicuškine zápisky". Víno, chlebíčky a pozitívna nálada zabezpečené. Vstup jeden úsmev :-). 
 Utorok, 22. 6. 2010, Zlaté Moravce, súkromná čítačka v reedukačnom zariadení pre mladistvých. Cestou späť pôjdem cez Piešťany. Ak viete o nejakom priestore (knižnica, čajovnička, jazz klub atď), kde sa dá spraviť čítačka, ozvite sa mi cez mail. 
 Utorok, 1. 6. 2010, Čechy, Oficiálne vydanie českej verzie románu Vteřina před zbláznením (Všecho je, jak je) pod vlajkou pražského vydavateľstva XYZ. 
 Pondelok, 28. 6. 2010, Topoľčany, 17.00 hod Galéria (program pre mladých), 18.00 reštaurácia Kaiserhof Nám.M. R. Štefánika (čítačka Hirax). 
 Máj-jún 2010: Výstava Hiraxových fotografií z Thajska v martinskej kaviarničke Kamala. Pozor, nejedná sa o žiadnu "galériu". Bude sa jednať o 12-14 záberov, ktorými som sa snažil vystihnúť túto krajinu. Vstup jeden úsmev. 
   

