
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marie Stracenská
                                        &gt;
                o kadečom
                     
                 Ohrozené druhy zo skrine 

        
            
                                    14.5.2010
            o
            13:48
                        |
            Karma článku:
                9.13
            |
            Prečítané 
            1453-krát
                    
         
     
         
             

                 
                    Spodnička. S čipkami. Šaty s vypchávkami pod pazuchami - potnými košíčkami. Kabátik s vypchatými ramenami. Nylonové pančušky na gumku. Látková vreckovka s vyšitým monogramom. Klobúčik.
                 

                 To všetko zvyklo byť prehodené cez čelo veľkej postele, keď sa moja babička chystala v nedeľu na prechádzku.   Aj ja sa chodievam prechádzať. A nič z toho nepoužívam. Ani by som nemohla. Nič z toho nevlastním. Nie som dáma? Som. Ale, hold, všetky tieto kusy oblečenia sa tak dlho dostávali na zoznam ohrozených druhov, až celkom vymreli. To, čo bývalo pre moju babičku a jej babičku a jej babičku... samozrejmé, už pre mňa dnes príliš nie je.   Celotelové spodničky bývali úhľadne zavesené na krásnom oháčkovanom ramienku, pekne vyžehlené, v rôznych odtieňoch smotanovej, kávovej a sivej a čiernej. Ku každej sukni a šatám iné, samozrejme. Vypchávky, ktoré mali zachytávať pot, boli všité v každých lepších šatách. Babička ich pravidelne právala - odpárala a potom zas prišila. Dnes mi pripadajú trochu absurdné, ale v časoch neholenia si podpazušia a dobe, keď automatická práčka mala podobu dvoch čudných strojov, to asi malo logiku. Pamätám si ako jeden z nich (miešačka) smiešne poskakoval počas činnosti po kuchyni a ja som ho niekedy naháňala s lavórom, aby vyžmýkaná voda nezmočila podlahu. Látkové vreckovky mávala babička úhľadne zložené v minišuflíčkoch v komode: v prvom dámske biele, v druhom kvietkované, v treťom pánske, vo štvrtom s rôznymi vzormi a v piatom hodvábne. Nádherne tie šuflíky voňali kúskami mydla vloženými medzi kapesníčkami... Pančušky super držali a netrhali sa príliš. Klobúčiky bývali každý vo svojej škatuli v šatni, v susedstve pripínacích kožušinových golierov, jemnučkých šatiek s pestrými vzormi a látkových kvetových aplikácií. Všetky gombíkové dierky mávala babička vždy vyšité ručne, lemy prišité úhľadným stehom, vo vzorne vyleštených topánkach drevené napínače.   Keby dnes babička otvorila moju skriňu, asi by bola prekvapená. Džínsy by si nikdy neobliekla. Nemala ich rada asi tak, ako ja neznášam spodničky. Látkové vreckovky mám presne tri. Dobre odložené. Používam papierové. Kožušinu žiadnu, klobúčik zapadnutý neviem kde. Zato by našla kopu tričiek, svetríkov a nohavíc rôznej dĺžky. Ona vlastnila dvoje a nenosila ich, kým nebolo naozaj veľmi nutné. To isté sa týkalo teplákov. Tie naozaj nemusela. Namiesto nich si obliekala domáce šaty. Jéj. Práve som prišla na možno posledný ohrozený skriňový druh, ktorý u mňa zatiaľ nevymrel. A ja ich nosím, domáce šaty dlhé takmer po zem, voľné a pohodlné. Pripadám si v nich aj doma žensky a pekne. Aspoň niečo podedené mi v skrini pretrváva. Možno si pôjdem kúpiť ďalšie :) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Vianoce vo štvrtok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Nepoznám žiadne dieťa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Ranná káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Cez hrádzu zrýchlene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Poschodová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marie Stracenská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marie Stracenská
            
         
        stracenska.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som žena. Novinárka, lektorka a konzultantka. Mama nádherných a šikovných dvojčiat. Manželka, dcéra, sestra, priateľka. Neľahostajná. Mám rada svet. Rovnako rýchlo sa viem nadchnúť, potešiť a rozosmiať ako pobúriť a nahlas rozhnevať. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    701
                
                
                    Celková karma
                    
                                                12.18
                    
                
                
                    Priemerná čítanosť
                    1779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o deťoch
                        
                     
                                     
                        
                            o vzťahoch
                        
                     
                                     
                        
                            o kadečom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Moja rozprávka
                     
                                                         
                       Zaťovia
                     
                                                         
                       Nápadník
                     
                                                         
                       Univerzitná nemocnica vyriešila problém Richterových prezervatívov
                     
                                                         
                       Obrovská stará dáma
                     
                                                         
                       Tutoľa máme kopčok
                     
                                                         
                       Bojovníčka
                     
                                                         
                       Vifon
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




