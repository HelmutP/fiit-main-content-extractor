
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Helena Jakubčíková
                                        &gt;
                Malé pohladenia
                     
                 Lastovičky na drôte 

        
            
                                    16.5.2010
            o
            18:14
                        |
            Karma článku:
                3.43
            |
            Prečítané 
            677-krát
                    
         
     
         
             

                 
                    Vonku sa ženia všetci čerti a k nám do logie prišla návšteva. Pekne spôsobne sa usadila na drôt, ktorý bol kedysi natiahnutý kvôli sušeniu prádla. Vietor s nimi kymáca a oni si čistia zmoknuté pierka.
                 

                 Oceľové lanká, ktoré už dlho neslúžili svojmu účelu, som túto jar dala preč. Ostal jeden kvôli popínavke, ktorá si ešte hovie v pohodlí zimoviska.   Pred pád dňami začali k nám nalietavať lastovičky - teda belorítky. Na veľkú "radosť" mojich mačiek to nebola ojedinelá návšteva. Prebiehala inšpekcia niekoľkými vtáčimi kontrolórmi, či náš balkón je vhodným miestom na hniezdenie. Raz sa jeden zavadil o drôt a spadol na podlahu balkóna priamo pred dvere. Kým sa pozviechal, nastala pri balkónových dverách vzrušená tlačenica. Moje mačky vtedy zabudli na vzájomné spory. Nuž, na mieste lastovičiek by som si poriadne rozmyslela stavať hniezdo práve tu.   Týmito návštevami zavítalo do relatívne nudného života mojich šelmičiek vzrušenie. Upretý pohľad, číhajúci postoj, cvakanie zúbkov, ktorým sa mačka odreagováva, keď vie, že korisť je nedosiahnuteľná. Taký je na nich pohľad, keď jednotlivé vtáčiky, ale aj celý kŕdeľ nalietavajú na logiu.   Sedím za stolom a dívam sa do okna na párik. Ale nie, je tam ešte jedna, dve - ale nie pristáli hneď ďalšie tri páry a ďalšie. Jéééj! Odlietavajú a prilietavajú a užívajú si relatívny pokoj v závetrí na juh obrátenej logie. V rohu balkóna, vysoko z dosahu mačiek, je rozostavané hniezdo. Neviem, či ho dokončia, či im nebude vadiť vyrušovanie človekom, teda mnou, alebo mojimi mačkami, z ktorých srší lovecká vášeň. Pravdu povediac, tie držím radšej vnútri aj kvôli ich vlastnej bezpečnosti. Pri vychádzaní na balkón musím daváť dvojnásobný pozor, lebo mrštná Gabrielle je rýchla ako blesk. Mucko zas driape dvere a prosebným mňaukaním sa dožaduje zábavy. Nuž, nemám chuť zbierať zabité vtáčatá alebo zabité mačky.   Našuchorené pierka z fúkanej od kaderníka vetra z nich robia krehké guľky. Vidličkové chvostové pierka zabezpečujú rovnováhu na pohupujúcom sa drôte. Vyzerajú spokojné. Sem tam si niečo pípnu. Možno sa na nás bavia tak, ako sa na nich bavíme my. Ktovie? Mačky sú usadené na parapete a pozorne ich sledujú.   Po dĺlĺĺĺhej chvíli to niektoré vzdávajú a idú sa radšej venovať zmysluplnejšej činnosti. Slastne sa natiahnuť a skrútiť do klbka snívať mačací sen. Mucko a Gabrielle sú však vytrvalí. Párik na parapete a páriky na drôte vytvárajú zaujímavú kompozíciu.       ---   P.S. Škoda, že nemôžem dokumentovať fotkami. Je šero a naviac sú okná zarosené. Moja mašinka by vyprodukovala len sivé machule. Ak by vôbec stihla ten neposedný živel zachytiť. Takže možno nabudúce.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Emočné konto
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Idiot s Kitty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Pomenuj svoj problém
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Preto lebo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Kruhový objazd na ceste
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Helena Jakubčíková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Helena Jakubčíková
            
         
        jakubcikova.blog.sme.sk (rss)
         
                                     
     
        Som to, čo zo mňa spravili otec, mama a ja. No a samozrejme mnoho ďalších ľudí:-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    164
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1048
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Čudnosti prečudesné
                        
                     
                                     
                        
                            Tretia alternatíva
                        
                     
                                     
                        
                            Snové arabesky
                        
                     
                                     
                        
                            Malé pohladenia
                        
                     
                                     
                        
                            Murko a tí ostatní
                        
                     
                                     
                        
                            Zápisky z ciest
                        
                     
                                     
                        
                            Mačacím pazúrikom
                        
                     
                                     
                        
                            Moje zlatíčka
                        
                     
                                     
                        
                            Závoz alebo životná križovatka
                        
                     
                                     
                        
                            Len tak
                        
                     
                                     
                        
                            Profesionálna deformácia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            všeličo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mňamky
                                     
                                                                             
                                            Snílek
                                     
                                                                             
                                            Pavel "Hirax" Baričák
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Šťastný deň
                                     
                                                                             
                                            Ister Felis
                                     
                                                                             
                                            MačkySOS
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Susedské spolunažívanie.
                     
                                                         
                       Ako sa zo sestry stal triedny nepriateľ
                     
                                                         
                       Víťazstvo tolerancie, alebo showbiz marketingu?
                     
                                                         
                       Prírodné repelenty
                     
                                                         
                       Mnoho systému, žiadna človečina
                     
                                                         
                       Objatie
                     
                                                         
                       Snáď ho raz zabijeme!
                     
                                                         
                       Zápisky z veteriny – dni, keď nemôžem hovoriť
                     
                                                         
                       Návrat nehy medzi muža a ženu 1.časť
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




