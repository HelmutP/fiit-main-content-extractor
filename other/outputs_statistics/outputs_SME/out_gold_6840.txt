

  
 Začala som si predstavovať akí teda asi sú tí zo Shanghaja. Možno netrúbia veľa a zbytočne. Možno nejazdia v protismere, nezaparkujú na nemožných miestach a vôbec, vedia šoférovať. Možno sa nebudú pokúšať ma zraziť, keď idem na zelenú. Možno nesedia piati na jednom mopede. Možno nepľujú, negrgajú a neprdia všade na verejnosti. Možno to nerobia aspoň ženy. Možno nejedia na chodníku a iných nepochopiteľných miestach. Možno nezablokujú chodník, aby mohli predávať ponožky, melóny či gumičky do vlasov. Možno ich deti nosia plienky a nemajú zámerne dieru v nohaviciach. Možno ich deti tiež necikajú a nekakajú na verejných miestach, záhradách, parkoch a chodníkoch. Možno používajú na ulici odpadkové koše na smeti. Možno nepoužívajú pyžamo ako spoločenský odev. Možno sa iba nesmejú, keď niečo nevedia. Možno vám aj prezradia, keď niečo nepochopili a spýtajú sa, aby neurobili hlúposť. Možno stratiť tvár nie je pre nich taká tragédia. Možno vám aj povedia úprimne ako sa veci majú. Možno ich konanie povedie k riešeniu situácie. Možno sú zvyknutí na cudzincov a nebudú vám okukovať ako nové zvieratko v klietke. 
 Na druhej strane, možno sú agresívni a hádaví. A možno nie sú úprímní a milí. Možno nie sú ani zhovorčiví í a srdeční. 
 Neviem, budem sa tam musieť vybrať na návštevu. Rozhodne to chcem zažiť na vlastnej koži. Tak ako tú „pravú“ Čínu. 
   

   

