
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ingrid Sukovska-Bussell
                                        &gt;
                Cheeky monkey
                     
                 Co mi o tehotnych zienkach zabudli povedat… 

        
            
                                    8.3.2010
            o
            14:29
                        (upravené
                8.3.2010
                o
                14:55)
                        |
            Karma článku:
                14.17
            |
            Prečítané 
            2605-krát
                    
         
     
         
             

                 
                    Dve ciarky sa takmer symbolicky objavili na Den Otcov. Kukali sme s mojim drahym striedavo na ne a na seba. Plni prvotneho zmatku a zvedavosti sme cakali, kedy sa dostavia tie euforicke slzy stastia, ktore vzdy vidno na konci romantickych filmov tesne pred titulkami. Slzy nakoniec nedosli, hoci nam dvom postupne doslo, ze budeme traja. A tych devat uplynulych mesiacov bola nakoniec skola sama o sebe, lebo ani vsetka literatura na svete ma nemohla pripravit na to, ze...  
                 

                 ...ze nie kazde babo sa ohlasuje budickom s hlavou v zachodovej mise - s troskou stastia moze tehotenstvo prebehnut akoby nic (teda okrem tej srandovej pasovej linie, co tak aspon na pol roka pripomina Skippy stepnu kenguru).    Tehotenstvo 'akoby nic' je na jednej strane super, lebo take velke spravy o takom malilinkatom novom zivote predsa netreba hned vesat kadekomu na nos. Zatlkat, zatlkat a zatlkat prve mesiace je pritom isto jedoduchsie, ked pri rozhovore s ostrazitou svokrou neodpadnem, nedemostrujem zrazu divnu naklonnost ku kyslym uhorkam a neodskakujem si priebezne vracat ako po veceri v tretotriednom curry-house.     Na druhej strane je to strasne surealny pocit vediet, ze 'som v tom' a pritom sa nic nedeje. Ako prvo-mama som cumela na brusko v zrkadle od toho dvojciarkoveho momentu a cakala, ze sa zrazu krasne zagulati a ze ja potom budem hrdo chodit po svete  a hladkat si ho ako v tych reklamach na Johnson&amp;Johnson a...a nic. Este dlho dlho nic.     ...ze brusko tehotnej zeny = res publica   Ze ked to brusko zacne konecne rast, tak takmer kazdy pribuzny, znamy a okoloiduci sa ho zrazu bude chciet dotknut a oskusat si, ci drobec kope. Ze takmer kazda kolegina a susedka bude vediet presne okomentovat, ci brusko rastie prilis rychlo alebo prilis pomaly (idealne v uzkom casovom slede jedna za druhou a so zarucene opacnymi nazormi na tuto fascinujucu temu). Ze nahodni spolusediaci v kantine, na svadbe ci v kaviarni sa mozu zmenit na nutricnych expertov, ktori presne vedia co a kolko moze (alebo nesmie!) jedna tehulka zjest ci vypit...   Metoda pokusov a omylov preukazala, ze na tie rucky-smatralky, co zrazu maju neodolatelny pocit potlapkat moje vzduvajuce sa brusko a okomentovat jeho stav, zabera len poriadna davka uprimnosti  ('Sorry moja, to bola fazulova polievka, nie kopanec od babatka...'Nie zlatko, TOTO tu je podpasovy tuk, babo je az TUTO)....    ...ze tehotna zenska = hotova zoologicka zahrada    To malicke este nemusi byt vobec na svete a pritom sa normalne jemna zienka moze s kludom a bez varovania metamorfozovat do podoby zurivej medvedice v jarnom vybehu - staci prepchaty vlak s pubertakmi, ktori sa ohanaju plecniakmi hlava-nehlava, ci plny autobus lietajuci v zatackach, v ktorom sa nikto neponahla ponuknut svoje teple vysedene miestocko...    Stala som tak raz v horucom preplnenom vozni mojho pravidelneho spoju Londyn-Kent. Vdaka snehu a katastrofickemu manazementu sme stali odstaveni uprostred zamrznutych poli a cakali na kolko sa nasa normalne hodinova cesta pretiahne. Sedemmesacne brusko ma tahalo k zemi, tak som sa otocila k prvym sedackam, ktore boli po ruke a slusne poprosila, ci by som sa mohla posadit. Dve vyfintene bosorky v strednom veku a cincilovych kozuchoch si to zjavne len tak za svoj dizajnersky klobucik nedali - vraj 'A vsak je tu  kopec chlapov co sedia, co sa neopytam ich? Preco by prave ONY, ZENY, mali uvolnit miesto nejakej tehulke?'...Ludia otacali hlavy a hladali povodcu vzruchu. Starsi gentleman o dva rady dalej sa postavil a ponukol mi svoje miesto. Pretlacila som sa cez dav, zlozila sa na ponuknute miesto a zasla, kde sa zrazu podela obycajna zenska solidarita? A vobec, nechceli sme my zeny nahodou emancipaciu a rovnopravnost s muzmi??    Okrem ineho sa vzdy najde zopar osvietenych kolegov, pre ktorych sa tehotna zenska z nejakeho dovodu zrazu zmeni na slepicu a potrebuju razne vysvetlit, ze vytvaranie noveho zivota pod srdcom este neznamena nahlu stratu sedej hmoty mozgovej, vsetkych profesionalnych vedomosti a pracovnych zrucnosti...a uz vobec nie to, ze zvysok zivota by sme planovali stravit postupnym plodenim, rodenim, varenim a kavickami s kamoskami. To aj napriek tomu, ze sem-tam pamat vypne do modu zlatej rybicky (interval sustredenia sa tak tri sekundy) a ze vdaka velkosti velryby sa na par mesiacov nezmestime do svojej kolekcie NEXT-ovych ciernych kostymcekov.    ...ze do postele zrazu mozem chodit ako do kina, lebo produkciu TAKYCH zivych snov by mi zavidel aj Milos Forman (aj s hviezdickou)    ...ze vyberanie mena pre babatko vyzaduje diplomaticke zrucnosti na urovni vybavovania mieru na Strednom Vychode - je fascinujuce, kolko ludi si zoberie vyber mena za svoju osobnu zodpovedost. Vratane vzdialenej pratety, sekretarky a pokladnicky v miestnom Tescu.    Kedysi to bolo jednoduche - clovek vytiahol kalendar, pichol prstom do prostriedku a presto-presto, dalsi Martinko bol na svete. Dnes je to sama Ella, Pricencess-Tailor a Hugo. Klasicke slovenske meno v nasom pripade nebolo mozne, kedze dietatko bude vyrastat v bilingualnom anglicko-slovenskom prostredi s francuzskym priezviskom (a tiez by to nebolo fair, keby raz ako prvacik musel miesto 'Joe' hned tak z hurta hlaskovat 'Rastislav').     Drobec teda dostal 'pracovne meno' a devat mesiacov ho nazyvame Egbert - povedali sme si, ze cim vacsie cudo vymyslime, tym menej budu vsetci naokolo filozovat, ked sa raz narodi a do rodneho listu zapiseme jedno uplne obycajne meno z kalendara. Pruser je, ze na toho Egberta si akosi za ten cas uz vsetci zvykli...    ...ze si zrazu nedosiahnem na snurky na topankach a po 30tich rokoch mi ich bude na chvilu opat zavazovat niekto iny    ...a ze ked mi bude nieco klopkat na brusko z vnutornej strany, vobec to nemusi byt strasidelny pocit akoby vytrhnuty z filmu Votrelec. Miesto toho mi kazdy kopanec pod rebro tou mini nozkou pripomenie, ake je uzasne stvorit novy zivot a ze cochvila bude na svete opat o jedneho cloviecika viac...    Tak vela stastia drobec na ceste za nami ;) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Sukovska-Bussell 
                                        
                                            Ad Juvenilná Justícia: Krátko z inej strany (Anglicko)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Sukovska-Bussell 
                                        
                                            Krabička na čestnosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Sukovska-Bussell 
                                        
                                            Straty a nálezy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Sukovska-Bussell 
                                        
                                            Na konci duhy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ingrid Sukovska-Bussell 
                                        
                                            Nadupanemu hotentotovi  v ciernom aute so zadymenymi sklami…
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ingrid Sukovska-Bussell
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ingrid Sukovska-Bussell
            
         
        sukovska.blog.sme.sk (rss)
         
                                     
     
        zatulanec na opacnej strane Kanala; milujuca tatranske kopce, ceske filmy, cambridgske lodky, svajciarsku cokoladu a musle na vine. ukecana, ulietana, usmiata. ja.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4336
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Láska cez Internet
                        
                     
                                     
                        
                            Girl in the City
                        
                     
                                     
                        
                            Rodinne tajomstva
                        
                     
                                     
                        
                            Multi-kulti
                        
                     
                                     
                        
                            Cheeky monkey
                        
                     
                                     
                        
                            kuk objektivom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            wicked
                                     
                                                                             
                                            arty world
                                     
                                                                             
                                            common sense
                                     
                                                                             
                                            small is beatiful
                                     
                                                                             
                                            simple life
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mucha
                     
                                                         
                       Ruky
                     
                                                         
                       Kamarátstvo je aj o vytieraní zadku
                     
                                                         
                       Krabička na čestnosť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




