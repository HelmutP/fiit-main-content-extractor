
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Huran
                                        &gt;
                Nezaradené
                     
                 Tabuľka najčastejších životných situácii - Go eGov 

        
            
                                    8.4.2010
            o
            21:10
                        (upravené
                8.4.2010
                o
                22:10)
                        |
            Karma článku:
                3.07
            |
            Prečítané 
            1098-krát
                    
         
     
         
             

                 
                    Pri koncepcii informačných systémov vo VS treba vychádzať z procesov a poznatkov, tak aby sme vedeli čo najprecíznejšie identifikovať  najčastejšie životné skutočnosti. Preto som sa rozhodol spísať  tabuľku životných situácii s ktorými sa počas života občan, podnikateľ, či samotná verejná správa najčastejšie stretáva.
                 

                 Táto tabuľka je ilustratívna a neobsahuje všetky životné procesy.  Cieľom je priblížiť najčastejšie životné situácie "od narodenia až po smrť". Má edukatívne napomôcť k pochopeniu viacerých funkcii verejnej administrácie a môže všeobecne vymedziť podstatné časti budúceho eGovernmentu     
     Skutočnosť     Služby   verejnej správy     Služby   vo verejnom záujme       Narodenie     Rodný list   Pas   Pobyt osoby     Životné poistky       Vzdelávanie     Potvrdenie o návšteve školy   Ukončenie štúdia   Dosiahnuté štúdium     Informácie o štúdiu   Zápis na školu   Štipendiá   E-learning       Zdravie     Informácie o zdravotníctve   Podnety na úrad pre dohľad nad zdravotnou   starostlivosťou     Zdravotné poistenie   Zdravotná karta   Návšteva lekára   Výber zdravotnej poisťovne       Doprava     Vodičský preukaz   Register motorových vozidiel   Lustrácia osôb     Zjazdnosť ciest   Rezervácie dopravy   Diaľničné poplatky   Mýto       Zamestnanie     Evidencia nezamestnaných   Živnosť   Obchodný register   Podpora nezamestnanosti     Hľadanie zamestnania       Bývanie   Kultúra     Kataster nehnuteľností   Stavebné povolenie   Územný plán - prístup do GIS systémov     Plná informatizácia   Kultúrne poukazy/informácie       Dane   Poplatky   Odvody     Daňové informácie a priznania   Odvody a Clá   Dôchodkové poistenie   Rodinné prídavky   Miestne dane a poplatky     Výber DSS   Zmluvné poistenie   Koncesionárske poplatky           Demokracia     Voľby   Referendá   Participácia na verejných veciach   Notári     Petície   Združovanie sa   Politická agitácia   Lobbing       Bezpečnosť     Trestné oznámenie   Oznámenie priestupku   Trestné registre (spisy)   Súdy     Komunikácia občanov s OČTK       Exitus     Výmaz/deaktivácia záznamov     Pohreb      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Huran 
                                        
                                            Zhorená vlajka EU. Trestné oznámenie (vzor) za zásah.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Huran 
                                        
                                            99% je čistá. Spišiak zneužil políciu na politiku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Huran 
                                        
                                            Maďarič neposlúchol Fica.  Má ho za „pandrláka“?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Huran 
                                        
                                            Pracujúce matky musia mať pracovný čas v súlade s rodinou.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Huran 
                                        
                                            Pokuty mobilných operátorov a zákonný spotrebiteľský „zlepšovák"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Huran
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Huran
            
         
        huran.blog.sme.sk (rss)
         
                                     
     
        Informatika, právo, verejná správa sú moje hobby a práca.   

Osobná web stránka www.huran.sk  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4109
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubo Kužela
                                     
                                                                             
                                            Moje eTREND blogy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            WikiDemokracia
                                     
                                                                             
                                            Aliancia nezávislých
                                     
                                                                             
                                            STOP BYROKRACII
                                     
                                                                             
                                            kLEKAROVI.sk - rezervačný systém
                                     
                                                                             
                                            LEGUS - Právny/procesný IS
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




