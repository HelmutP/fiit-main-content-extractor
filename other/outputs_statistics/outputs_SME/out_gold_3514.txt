
 Nemoralizujem, len konštatujem. Akýkoľvek pocit viny považujem za zbytočnú emóciu. Chce veľkú odvahu a silu zmieriť sa sám so sebou. Úprimnú snahu nezopakovať už nezvrátiteľné z minulosti. 

     Dnes som mala na trucovni rozhovor s mojou staršou sestrou, hlboko ma ranilo vedomie, že tvrdohlavo odmieta akúkoľvek pomoc. Mala dosť komplikované obdobie života, ktoré stále neprekonala a vedome sa týra v mylnej domnienke, že si to zaslúži. Fakt je, že manžel jej to neuľahčuje, skôr naopak.

     Tu si uvedomujem rozdiel pováh a pomer sily zvládnuť a spracovať problém, nielen v najbližšej rodine, ale aj v mojom okolí priateľov, či známych. Čo jeden zvládne ľavou zadnou, iný v tichosti roky spracúva a trpí a niekto sa nikdy nevyrovná s tým, čo mu život postavil do cesty.

     Problémom sa nikto nevyhne, no spôsob ako sa k nim postavíme, ovplyvňuje náš ďalší život. Pozerať sa na problémy ako na nezvládnuteľné prekážky, nie je cesta ako dosiahnuť cieľ pokoja duše. Byť večne nespokojný, izolovať  a ľutovať sa nemá zmysel a má na okolie skôr opačný účinok, hoci sa tým očakáva práve prejav záujmu.
 
     Zatvárala som oči pred trápením druhých, lebo som si myslela, že to moje je práve to najväčšie. Odpusť mi to, spolu to zvládneme.


     Tu je moja ruka, som tu pre teba, stačí ju prijať Mirka....
 
