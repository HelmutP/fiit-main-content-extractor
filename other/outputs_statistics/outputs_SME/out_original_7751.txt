
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Kaštier
                                        &gt;
                názory
                     
                 Nadšenie je nákazlivé 

        
            
                                    1.6.2010
            o
            13:09
                        (upravené
                1.6.2010
                o
                16:29)
                        |
            Karma článku:
                6.38
            |
            Prečítané 
            987-krát
                    
         
     
         
             

                 
                    Jednou z najprirodzenejších ľudských aktivitít je rozprávanie sa. Obojsmerný tok myšlienok. V ľudskej reči.
                 

                 Myslím, že toto je ten pravý dôvod, prečo sa mi TEDxBratislava (aj ako spolu-organizátorovi) tak páčil - pretože nebol primárne o prednáškach,   - bulletpointoch  - v  - prezentáciách  alebo hrubej čiare medzi prednášajúcim a  -------------------------------- počúvajúcim.   Bol o jednoduchom rozprávaní sa, nadšení a odovzdávaní myšlienok (Things).   (Presne tak ako keď sa starovekí cestovatelia vrátili domov a na domácom trhovisku ich obklopil hlúčik ľudí a jedna strana rozprávala o tom, čo je nové na ďalekom východe a druhá strana o tom, čo je nové doma. Analogicky mi pripadal sobotný rozhovor kvantového fyzika s lesoochranárom:)   Všetci v hľadisku vedeli, že hneď po skončení bloku budú môcť vybehnúť s konkrétnymi otázkami za rečníkom a všetci rečníci vedeli, že cez prestávku si neoddýchnu a že ich prednáškou sa to zďaleka nekončí. Domnievam sa, že práve očakávanie bezprostredného ľudského rozhovoru vytvorilo silné puto a prispelo k tej jedinečnej energii.   S týmto súvisí moje vnútorné ponaučenie - zahodiť formality a neoddeľovať informácie od emócii. Nadšenie je nákazlivé.   TEDxBA mal v mojich očiach aj v očiach Charmian Wylde dobré Chi (alebo auru). Podľa mňa preto, že bol výsekom dobrých vecí z tohto sveta. Otvorenosť v rozhovoroch, optimizmus, rešpekt, zdieľanie, nápady, nadšenie a ix ďalších keywords. Na 24 hodín som zabudol na všetky muchy tohto sveta a mal som pocit, že je možné všetko. Stačí len chcieť.   Hmatateľným dôkazom bola cesta Martina Beťka okolo sveta na embéčke, 3 knižky 11 ročnej Stely Brix, Dolníkovej Divé Maky (a omnoho viac) a to, že 4 mesiace telefonovania, emailovania, stretávania nášho teamu, ix google dokumentov, podpory partnerov a pomoci dobrovoľníkov sa pretavilo do takmer 400 skvelých ľudí pod jednou strechou a množstva ľudí na webstreame.   Podobne to bolo sformulované aj na FWD: Konferencia po ktorej je možné všetko. Nemyslím však, že sme ako organizátori dali dokopy čosi nadprirodzené - proste sme vytvorili možnosť prísť a prostredníctvom TED(x) brandu sme dali vedieť, že tu sa myšlienkam a nadšeniu medze nekladú.   Tak vám týmto dávam vedieť, že toto platí aj dnes, kdekoľvek ste.   -   -   Ak máte tipy, ako čo najviac zužitkovať a pretaviť do ďalších aktivít to, čo bolo na TEDxBratislava výnimočné, istotne nám dajte vedieť. Videá pribudnú na web čoskoro. Zatiaľ ako chuťovku pridávam fotku Ghinsbergovej osnovy, čo sa nám podarilo nájsť na pódiu.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            20 kníh môjho roka 2012
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            25 kníh roka 2011
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            Ako reformovať dane a odvody - príspevok do diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            50 kníh za rok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            Wikileaks v dobe xeroxu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Kaštier
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Kaštier
            
         
        kastier.blog.sme.sk (rss)
         
                                     
     
        ...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    714
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            názory
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Boston bigpicture
                                     
                                                                             
                                            TED
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




