

 
Potrebujeme:
 
 
Čakali sme (ako inak) hostí, takže sme
potrebovali veľa vtáčkov. Na 10 statných vtáčikov potrebujeme
zhruba 1 - 1,30 kg hovädzieho stehna, prípadne roštenky, ktorá však mne
pripadá zbytočne drahá a fajnová na vtáčiky. Mäso musí byť dostatočné
vysoké a rozkrojiteľné proti vlasu, inak ho nerozklepeme. Ďalej
potrebujeme toľko plátkov dusenej šunky, koľko bude vtáčikov, 3 vajíčka, 2
cibule, 10 dkg prerastenej údenej slaniny, 10 dkg údeného syra, olej,
plnotučnú horčicu, 1 polievkovú lyžicu rajčiakového pretlaku, 3 dcl
červeného suchého vína, trochu hladkej múky, soľ a špáradlá. 
 
 
Ochutíme
čiernym mletým, novým mletým korením a tymianom. To je z korenín
všetko a to som ešte exkluzívny ako hrom s tým tymianom.
 
 

 
 
Tak a opäť práce ako na pánskom. Deduška dnes úplne vynecháme. Veď ako
by dokázal hovädzie stehno nakrájať na plátky cca 0,5 cm proti vlasu a
naklepať. Nás však nič nezastaví. Pokiaľ nemáme hovädzie dostatočne
vysoké, alebo ako napríklad ja aj dostatočne široké, postupujeme tak,
že
plátok odkrojíme, ale nedokrojíme dokonca. Necháme ho na malom kúsku po
celej šírke mäsa neodkrojený a hneď krájame ďalší plátok, ktorý
odkrojíme dokonca. Oba spojené plátky otvoríme a poriadne naklepeme.
Vytvorí sa nám jeden veľký plátok. 
 
 
Každý naklepaný plátok
posypeme čiernym mletým a novým mletým korením (ak vám vadí sladkastá 
aróma nového korenia, proste ho vynechajte), dobre posolíme a necháme
aspoň 20 minút postáť. 
 
 
V tom čase si nakrájame slaninu a aj
údený syr na toľko hranolčekov, koľko máme plátkov mäsa. Hranolčeky nesmú
byť príliš hrubé, radšej nech sú dlhšie, inak vtáčiky nezrolujeme.
Ďalej jednu polovicu cibule očistíme a odkrojíme z nej toľko tenších
mesiačikov, koľko bude vtáčikov. Všetku ostatnú cibuľu (viac ako 1 a
pol cibule) očistíme a nakrájame na kocky. Z vajíčok môžeme urobiť
praženicu, ako som to urobil ja, ale môžeme ich aj uvariť natvrdo a
nakrájať na mesiačiky, alebo na krúžky.  
 
 

 
 
Tak a ideme rolovať. Každý posolený, pokorenený a naklepaný
plátok natrieme horčicou. Položíme naň plátok šunky, potrieme lyžičkou
praženice, položíme hranolček slaniny, údeného syra a mesiačik cibule.
Zrolujeme. Môžeme si pomôcť aj tým, že obsah vtáčika prstami postláčame
na jednu stranu a okolo neho potom zrolujme mäso so šunkou. 
 
 

 
 
Každého vtáčika prepichneme skrz dvomi špáradlami na oboch stranách. Myslím, že používanie nite je zbytočné a prácne.  
 
 

 
 
Deti, svojho času som spomínal, že na rolované veci určite nepatria k
mojej doméne. Jednoducho som pri nich nešikovný. Nižšie
vidíte, že vtáčikom trčali kúsky šunky či iných častí náplne. Takže vás
môžem ubezpečiť, že keď napriek nevalnému rolovaniu, vtáčiky dopadli
veľmi dobre mne, tak vyjdú aj vám. Žiadne strachy.
 
 

 
 
A ešte jedna poznámka. Klasický Španielsky vtáčik sa plní zvyčajne kúskom špekáčky, vajíčka, prípadne slaniny a kyslej uhorky. Netvrdím, že je to zlá kombinácia, ale ja som sa predsa len rozhodol pre originálnejší spôsob. Takisto sa nedáva červené víno. 
 
 
Tak a pokračujeme ďalej. Na panvici zohrejeme olej a vtáčiky z každej strany prudko osmažíme.
Už vtedy sa nám pekne stiahnu a my budeme cítiť, že sú oveľa
kompaktnejšie. 
 
 

 
 
Navyše šunka nám obsah vtáčikov pekne drží vo vnútri, takže takmer nič nám
ani počas dusenia nevypadne. To je aj dôvod prečo je dobré použiť
plátok šunky namiesto hranolčeku. 
 
 

 
 
Keď dusíme mäso s dlhšou varnou úpravou, môžeme vždy postupovať
dvomi spôsobmi. Buď dáme mäso prikryté dusiť na šporák, alebo ho
prikryté dusíme v rúre. Pre mňa je absolútne jednoznačne vhodnejšia
rúra. Mäso nie je potrebné každú chvíľu kontrolovať, neprichytí sa a
výsledok je jemný, krehký a drží pritom perfektne pokope. Kto ste
neskúsili dusenie v rúre, vrelo odporúčam. 
 
 
Tak a keďže sme nedávno dostali nádhernú jenskú misku (Borisko, veď ty vieš...:)) je na čase ju poriadne vyskúšať. 
 
 
Na
dno misy dáme výpek zo smaženia. Poukladáme vtáčiky, ktoré zasypeme na
kocky nakrájanou cibuľou, a zalejeme zmesou asi 3/4 l vody, 3 dcl
červeného vína a kopcovitou lyžicou rajčinového pretlaku. Všetko posypeme tymianom. Misu
prikryjeme, vložíme do rúry a dusíme na strednom plameni asi 3/4 hodinu. Vytiahneme
a skontrolujeme, či sa nám vtáčiky neprilepili k dnu nádoby, prípadne
ich uvoľníme, ak je potrebné ešte prilejeme vody tak, aby vtáčiky boli
aspoň do 2/3 ponorené a zakryté dusíme ďalšiu hodinu až 1 a 1/4 hodiny.  
 
 

 
 
Po dvoch hodinách dusenia na strednom plameni je každé hovädo
mäkké.:) 'Fajnšmekri' môžu dusiť ešte pomalšie na najslabšom plameni 3-4
hodiny. Výsledok je potom impozantný, ale to by bolo už aj na mňa
priveľa. To si nechám na niečo mimoriadne fajnové.
 
 

 
 
Vtáčiky povyberáme a odložíme na teplé miesto. Omáčku nalejeme do
väčšej rajnice a rozmixujeme ponorným mixérom. Keď ju ochutnáme,
zistíme, že je nesmierne silná.
 
 

 
 
Do rozmixovanej omáčky nalejeme vodu (ja som použil dobrých 3/4 l
vzhľadom k sile omáčky), necháme zovrieť, vložíme do nej všetky vtáčiky
a chvíľu povaríme.
 
 

 
 
V troche vody si rozmiešame 2 lyžice hladkej múky, zátrepku vmiešame
do omáčky a aspoň 10 minút varíme. Omáčku podľa potreby dochutíme, ale
myslím si, že to nebude potrebne.
 
 

 
 
Tak teda dobrú chuť a príjemnú nedeľu. 
 
 

 
 
 
 
 

 
 
 
 
 
tenjeho 
 
 
 
 

