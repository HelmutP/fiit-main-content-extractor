
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Maceják
                                        &gt;
                Nezaradené
                     
                 Chceme vidieť riešenia a nie špinu v predvolebnej kampani 

        
            
                                    6.4.2010
            o
            19:09
                        (upravené
                6.4.2010
                o
                20:47)
                        |
            Karma článku:
                16.74
            |
            Prečítané 
            2171-krát
                    
         
     
         
             

                 
                    Politické strany koalície zahájili tvrdý a neľútostný boj o každé percento. Nečudujem sa im, keďže dve z nich oscilujú niekde okolo piatich percent a vytúžené teplé miestečko v parlamente nie je až také isté. Začali pomaly, ale iste bojovať o každého voliča, ale podľa mňa, tým nesprávnym spôsobom.
                 

                 Došli argumenty, že predchádzajúca vláda tu nič neurobila a pochopili, že za štyri roky vlastne neurobili oni vôbec nič, okrem vyhovárania sa na predchádzajúcu vládu a hospodársku krízu.    Takýto alibizmus po čase prejde každého normálneho človeka, ktorý si uvedomuje, že „chlieb a hry“ túto krajinu z krízy nevyvedú, a že sú tu potrebné rázne ekonomické a sociálne opatrenia, pre konštruktívne riešenia v jednotlivých oblastiach spoločenského života.   Nastal zvrat o 180 stupňov.    „Musíme nájsť vinníka“, ktorý zapríčinil to, že v tejto koalícií to nefunguje tak ako má. Tento malý cirkus sa už spustil. Najprv došlo k odvolaniu ministra SNS - niekoľkokrát, teraz HZDS obvinila SNS z brania úplatkov a tak podobne ..., nieže by nás to nezaujímalo, ale takéto tvrdenia a podozrenia by mali byť predmetom vyšetrovania orgánov činných v trestnom konaní a nie predmetom predvolebných diskusií a volebnej kampane.   Ľudia tejto krajiny potrebujú istoty.   Preto chcem vyzvať všetkých politikov, aby bola predvolebná kampaň o riešeniach, ktoré ponúkame občanom a nie o ohováraní, podozrievaní, škandáloch a podobne.    Skúsme v sebe nájsť aspoň štipku politickej kultúry a predstavme občanom programy, ktoré im dajú možnosť rozhodnúť sa medzi politickými stranami nie na základe toho,  kto viacej koho urážal z toho, že kradne a úplaca a neviem čo ešte (už to ani nepozerám), ale na základe toho, že mu strana ponúkne riešenia na jeho lepšiu budúcnosť, so stabilným zamestnaním, fungujúcim sociálnym systémom a prostredím, ktoré sa vyznačuje slušnosťou a vymožiteľnosťou práva. Tieto základné prvky modernej demokracie mi v našej spoločnosti v poslednej dobe dosť absentujú, a preto verím a dúfam, že predvolebná kampaň bude o ponúknutí riešení, ktoré budú garantovať lepšiu budúcnosť občanov Slovenskej republiky.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Zmätený Matovič
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Jadro  je energia budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            "Politologický analfabetizmus"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Zatmenie v Žiari nad Hronom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Maceják 
                                        
                                            Kde sa stala chyba?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Maceják
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Maceják
            
         
        macejak.blog.sme.sk (rss)
         
                                     
     
        Politolog, ucitel, brat, syn, .......
www.stefanmacejak.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1546
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




