
 Nechcem v tomto krátkom texte ale priamo o „tete“. Skôr o jej pohľade na Majka. Bol som naňho tiež zvedavý. Zatiaľ ho videla raz, v roku 2004, počas svojej poslednej cesty na Slovensko. Vtedy mal Majko 5 rokov, ledva rozprával, o autistických, ešte hlboko zakorenených rysoch ani nehovorím.

Tetu sme hneď na druhý deň pobytu u nás pozvali na tanečnú súťaž, v rámci ktorej cez prestávku v rámci propagácie spoločenského tanca postihnutých detí s tými „normálnymi“, vystúpil aj Majko. Keďže to bolo už jeho štvrté vystúpenie, vystúpil už pomerne sebavedome a naplno si vychutnával pozornosť a následný potlesk  asi 200 prítomných divákov. Vrátane našej tety, ktorej nadšenie bolo veľké aj preto, lebo podľa jej slov tancovanie integrovaných párov, v ktorom je jeden tanečný partner zdravý a druhý buď telesne, alebo mentálne postihnutý, ešte v živote nevidela.

Jej slová uznania na Majkovu adresu hriali pri srdiečku. A nielen preto, že sa jej páčil Marekov tanec. V synovom prípade je tanec len jedným z kamienkov, ktoré sa snažíme poskladať do čo najpevnejšej mozaiky jeho šťastného života... 

„Jožko, mne stačí vidieť tú radosť a šťastie na Marekovej tvári. Nielen po tanci sa jeho tvár smiala. Radosť a šťastie sú viditeľné na jeho tvári takmer stále. Ak by to necítil tak aj vnútri, len ťažko by ste ho prinútili ukazovať skoro neprestajne svoju radosť a šťastie vonkajšiemu svetu. Veď aký máme často my, kvázi normálni ľudia, problém prejaviť radosť, šťastie, dať najavo svoje city. Majko žiari takmer stále. Určite aj preto, lebo cíti, ako ho milujete, ako mu odovzdávate kopce lásky deň čo deň, ako ho neúnavne povzbudzujete. A čo je najdôležitejšie, žiari ako slniečko aj preto, lebo cíti, že ho beriete ako rovnocenného,“ vyslovila na jeden šup s uznaním, ktoré potešilo.

Priznám sa, dobre nám s polovičkou tie jej slová padli. Majo si ich určite zaslúži...
 
