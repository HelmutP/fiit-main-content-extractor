
 Nebolo to o umení alebo šťastí, ani o výsledku. Proste dresy zostali suché a tráva pod nohami našich hráčov neutrpela žiadnu ujmu. To sme videli my „neodborníci“ . Videli to športoví novinári,celý svet, taký bol pocit a celkový dojem z výkonu našich hráčov. Bez ohľadu na výsledok ( i keď vyznávam skôr filozofiu, že nie je dôležitý výsledok, keď vyhráme), chýbala základná vôľa, bojovnosť, srdce....  A to sa na takomto fóre a v takejto situácií neodpúšťa. 
Tu bojujú aj futbaloví trpaslíci do úmoru všetkými (aj nekorektnými) spôsobmi o čo najlepší výsledok, aj  keď prehrajú. 
Preto je tlak médií, kladenie nepríjemných otázok  a rozbor každého detailu hry absolútne opodstatnený, ba priam žiaduci. To vedia ako hráči, tak aj tréner a celý realizačný tým, pretože najmä na MS nevystupujú len ako jednotlivci alebo tým, tu vystupujú ako Slovensko. Tu nejde len o futbal, tu sa buduje imidž krajiny na mnoho rokov dopredu. Tomu sa nevyrovná žiadna iná reklama alebo celosvetová kampaň za drahé peniaze. A tak nás budú vnímať v budúcnosti nie svetové elity, ale pospolité ľudstvo mnohonárodného sveta. Čo sme im teda ponúkli ? V domácom  prekáraní sa nám uniká podstata. Čo tak závažné sa zmenilo v mužstve, ktoré v prekvapujúco zvláštnej zostave neprepotilo dresy v najdôležitejšom zápase kariéry všetkých jeho aktérov ?  Prečo ne-výkon mužstva bol tak badateľný na ihrisku aj na lavičke ?  
Mám podozrenie, že keď sa u nás mimo iné povie, že hráči sa môžu na takom fóre ako MS dobre predať, myslí sa to doslovne. Ak pripustíme hypotézu, že už postup na MS bol naše maximum, potom stojí otázka tak, že čo chceme na samotnom podujatí robiť . A dianie na ihrisku aj mimo neho nasvedčuje, že rôzni ľudia majú o tom diametrálne rozdielne predstavy.
Takže si prejdime základnú zostavu, striedania a porovnanie s predchádzajúcim zápasom podrobnejšie aj so zreteľom na zmluvnú budúcnosť našich hráčov.

Zábavník nemá zmluvu v Mainzi, je však predpoklad, že mu ju predĺžia alebo bude oňho záujem po MS, lebo je to spoľahlivý a poctivý obranca. To potvrdil aj v zápase s N.Zélandom, aspoň jedno vystúpenie na MS mu k tomu určite pomôže.

Škrteľ ako zmluvný hráč Liverpoolu je za vodou, avšak pre našu obranu je nepostrádateľný a preto musí hrať.

Ďurica je bojovník na pohľadanie, občas zmätkuje a proti N.Zélandu spolu s Hamšíkom spoluzavinil vyrovnávajúci gól. Napriek tomu dostal príležitosť aj proti Paraguaju, hoci sa musel presunúť na okraj obrany. Nemá totižto dobrú zmluvu, chce z Ruska odísť a v zostupujúcom Hannoveri sa príliš nepresadil. Zdá sa že podľa tohto kľúča musí hrať v základnej zostave.

Čech v kvalifikácií až toľko nenahral, ale vo West Bromwich Albion sa v 2.lige chytil a pomohol mu k návratu do Premier League. Nemá však nič isté a tak sa musí aspoň raz ukázať na trávniku, aj keď podľa mňa patrí skôr k tým diskutabilným obrancom pre nevyrovnanosť výkonov. Rozhodne nepatrí do základnej jedenástky SR.

Pekarík hrával v kvalifikácií dobre, je to stále nádejný a perspektívny obranca, aj keď sa mu v momentálne priemernom Wolfsburgu  nedarí. Má zmluvu ale rád by zmenil pôsobisko, takže z tohto hľadiska musí na MS hrávať. Popravde však lepšieho krajného obrancu aj tak nemáme.
Saláta je talentovaný hráč Slovana, ktorý si vďaka čo len jednému štartu na MS určite polepší a Slovan ho pravdepodobne neudrží. Či má na to skúsenosti, aby mohol hrať popri Škrteľovi stopéra nech posúdia odborníci. Osobne by som si ho vedel predstaviť skôr na poste defenzívneho stredného záložníka namiesto Štrbu.

Štrba má ešte ročnú zmluvu v podpriemernej Škoda Xanti, či je tu spokojný ťažko povedať. Vzhľadom na svoj vek začína byť pomerne citlivý na rýchlosť súperových mladších hráčov, hrať v základe v jeho prípade je síce možné, v druhom polčase by však mal byť striedaný, čo sa zatiaľ nestalo. Zo zmluvného hľadiska aj vzhľadom na svoj vek už pravdepodobne aj tak nemá šancu si výrazne polepšiť.

Hamšík je tak ako Škrteľ za vodou, dobrými výkonmi sa však môže prepracovať vyššie, ako je Neapol. Z tohto dôvodu takticky nepodpísal predĺženie zmluvy, doterajšie výkony mu však asi nepolepšia. V základe je však pre SR nepostrádateľný.

Wies je najväčším prekvapením reprezentácie, znesie tie najvyššie kritéria a už za dva zápasy si vydobyl z hľadiska zmluvnej budúcnosti výbornú pozíciu. Prekvapením potvrdzujúcim moju teóriu by bolo, keby v treťom zápase nenastúpil alebo striedal.

Šesták je už dlhšie z formy, za normálnych okolností by nemal byť v základe alebo by mal prípadne striedať. S Bochumom má síce zmluvu, ale po vypadnutí z Bundesligy len s druholigovou perspektívou, preto neprekvapuje jeho tvrdošijné nasadzovanie trénerom Weisom do základu.

Jendrišek má práve čerstvú a výbornú zmluvu so Schalke 04, bol „ukázaný“ hneď v prvom zápase a to už zrejme stačilo. Prednosť a núdzu majú iní, preto sa nedostal a nedostane už ani k lepšiemu striedaniu, ako 5 minút pred koncom.

Hološko je na tom zmluvne podobne, ako Jendrišek, v Besiktese má dobrú pozíciu .Tak jemu ako aj reprezentačnému výberu striedanie v 70 a 81 minúte z herného hľadiska nič neprinieslo, ale tureckí pozorovatelia aj fanúšikovia môžu byť spokojní.  

Kucka vystriedaný „takticky“ v 90 minúte zápasu proti N. Zélandu určite nemal na ihrisko čo priniesť, odniesol si však účasť na tráve pri vyrovnávajúcom góle, čo mu určite psychicky do budúcnosti „pomôže“. Dať do hry v takom čase a vývoji zápasu, keď už stačí len odkopávať loptu prípadne súpera, tvorivého hráča hladného podržať a pohrať sa s loptou znamená, že tretím spoluvinníkom popri Hamšíkovi a Ďuricovi za inkasovaný gól sa stáva aj tréner. Ešte že má chlapec zmluvu v suchu a herne sa v klube uchytil. A v Sparte si môžu zas gratulovať, že majú v kádri reprezentanta SR a účastníka MS.

Vittek je zmluvne konečne za vodou, pre reprezentáciu má však jeho herný štýl a nasadenie nepostrádateľnú cenu, takže hrávať má a musí.

Prečo nehrá ani nestrieda Sapara, nebol nominovaný Švento , Stoch striedava na 5 minút napriek tomu, že má veľký podiel na postupe SR na MS, nevedno. Paradoxne títo hráči majú pevné zmluvy, hrávajú pravidelne vo svojich kluboch ako opory a len tak mimochodom, sú to majstri krajín , v ktorých pôsobia. Takže sú automaticky naučený aj vyhrávať......  


A na záver Kozák, ako príklad nepochopiteľnosti nominácie do základu proti Paraguaju.  Napriek školáckej chybe v 1.polčase, keď nahral na takmer gól súpera nebol v polčase striedaný.  Napriek pokračujúcemu katastrofálnemu pohybu, pomalosti aj celkovému výkonu nebol striedaný ani do konca zápasu . Je to kamarát – taky rád ? Zovšadiaľ okrem Petržalky a reprezentácie SR sa mu v najbližšom možnom termíne porúčali. Nemá zmluvu, ale v jeho prípade mu ani štart na MS asi nič neprinesie. Alebo sa mýlim ? Ak nastúpi proti Talianom, tak je moja úvaha správna, sme svedkami totálneho výpredaja a zazmluvňovania  hráčov nehoráznym spôsobom na úkor mena našej krajiny. Tiež by nebolo od veci, keby mali hráči z dôvodu transparentnosti povinnosť zverejniť meno svojho agenta . Verte či nie, ale sú to u nás tí istý  traja páni, cez ktorých ruky idú už potenciálny reprezentanti od mládežníckych kategórií. To prosím nie je úvaha, to je povinný krok pred nomináciou. Ale to už nech rieši niekto iný. 
 
