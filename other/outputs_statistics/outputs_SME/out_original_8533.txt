
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Fotoreportáže
                     
                 San Maríno 

        
            
                                    11.6.2010
            o
            7:02
                        (upravené
                8.8.2013
                o
                13:17)
                        |
            Karma článku:
                8.71
            |
            Prečítané 
            2304-krát
                    
         
     
         
             

                 
                    Po výdatných raňajkách sme opustili Mestre a po diaľnici sa presunuli cez Bolognu do Rimini pri Jadranskom mori. Bol štvrtok, cesta bola celkom v pohode, až medzi Bolognou a Rimini sme sa chvíľu zdržali pre zápchu, bol odstavený jeden jazdný pruh kvôli havárii. 20-30 kilometrov pred Rimini sme mali pred očami zvláštny úkaz, sanmarínska skala Monte Titano nám z diaľnice pripomínala tatranský Kriváň. S ubiehajúcou cestou sa pod inými uhlami pohľadu obraz "Kriváňa" menil, až napokon vyzeral úplne ináč. Po zídení z diaľnice sme sa zaradili na prístupovú cestu do tretieho najmenšieho štátu Európy San Marína. Cesta stúpala na sanmarínský kopec, ani sme poriadne nepostrehli štátne hranice. Nikto nás nezastavoval, nekontroloval, policajti namiesto toho usmerňujú vodičov na voľné parkovacie miesta.
                 

                 San Maríno sa administratívne člení na 9 obcí, tie v smere od Rimini sú takmer pospájané. V štvrtej obci Borgo Maggiore je prvé záchytné parkovisko, z ktorého vedie 1,5 kilometrová lanovka k pevnosti La Rocca. V meste San Maríno je viacero parkovísk, za každou zátačkou je parkovisko hneď vedľa cesty. Išli sme stále vyššie až na vrchol stúpania, priamo pod hradby historickej časti mesta. Keďže tam nebolo miesto, pokračovali sme po mierne klesajúcej ceste ešte pár desiatok metrov a tam sme šťastne zaparkovali. Je tam parkovací automat a verejné WC zdarma. Aj toto je súčasť hradieb nad "našim" parkoviskom.            Rozhliadli sme sa po meste, pršalo, bola hmla. Pri dobrej viditeľnosti dovidieť na more pri Rimini, my sme nedovideli ani na samotné Rimini. Aká - taká výhliadka bola na opačnú stranu, medzi oblakmi presvecovali pekné zelené kopce východných svahov Apenín.                              Dlho sme sa v tomto skalnom meste a štáte nezdržali, po celý čas pršalo, len intenzita dažda sa menila. Nachádzali sme sa v centre mesta, už si ho len prezrieť a vyliezť na pevnosť. Ja som mal schopný skladací dáždnik, sestre Magde štrajkoval, tak sa rozhodla pre kúpu štyroch veľkých.      Takto vystrojení sme vystupovali po schodoch a strmých cestičkách až k pevnosti La Rocca, vytesanej priamo do skaly Monte Titano, keď sa nám z bezprostrednej blízkosti ozvala ohromujúca delová rana, až nám zaľahlo v ušiach. Ohlušujúce delové výstrely sa niekoľkokrát zopakovali, bolo pravé poludnie, zrejme to používajú namiesto zvonov. O pár minút pochodovala oproti nám z pevnosti delostrelecká jednotka, ktorá to mala na svedomí.               Pred vstupom do pevnosti bolo za sklenými dverami ešte mokré a teplé delo, trochu sa z neho parilo. Dážď neustával, tak sme len nazreli do nádvoria pevnosti, do múzea sa nám nechcelo.      Mali sme pred sebou dlhú cestu do Toskánska a po ubytovaní sme si ešte chceli pozrieť Pisu. Pri spiatočnej ceste z hradu na parkovisko prestalo pršať, tak sme ešte nazreli do pár obchodíkov. Ja som objavil, že Fico má v San Maríne reštauráciu a bratia si pokúpili sady sanmarinskych euromincí po 7 eur (sanmarinsku mincu vám nikde nevydajú).         San Maríno sme opúšťali s pocitom, že to nebolo to pravé orechové, že pri peknom počasí by bol zážitok oveľa krajší. Rozhodli sme sa pri ceste do Toskánska opustiť diaľnicu v mestečku Forli a prejsť po ceste 2. triedy cez Apeniny do Florencie. Toto svoje rozhodnutie sme veľmi rýchlo oľutovali už na zamotaných obchádzkach v samotnom Forli. Keď sme sa po polhodine konečne dostali na výpadovku na Florenciu, rozhodli sme sa pokračovať a nevracať sa na diaľnicu. Podľa mapy naša cesta vyzerala na skratku oproti diaľnici, ktorá išla širokým oblúkom cez Bolognu. Cesta bola pomerne úzka, nie najlepšej kvality a plná zatáčok, stúpaní a klesaní. Keď sme sa konečne dostali na západnú stranu Apenín, bolo nám jasné, že sme si cestu predlžili oproti diaľnici o 2 hodiny, išli sme totiž väčšinou 40-50 km za hodinu. Pri výstupe do sedla sme nikde nezastavovali, tak so skúšal urobiť nejaké foto za jazdy, veľmi sa mi však nedarilo. Výhľad z cesty do okolia väčšinou zacláňali stromy, cítil som sa ako na hociktorom prechode slovenských kopcov.            Na vrchole hrebeňa sme si urobili jedinú malú zastávku, o pár minút nás po ceste predbehla skupina peších turistov, ktorú sme pred chvíľou predbehli my. Nechápali sme, odkiaľ kam asi tak vykračujú, keď široko - ďaleko nebola žiadna dedina.      Napokon sme chvíľu blúdili v blízkosti Florencie, chceli sme obísť mesto a dostať sa na diaľnicu smerom na Pisa-nord, v blízkosti ktorej sme mali zabezpečené ubytovanie. Cesty sú pomerne zle značené, nie sú zavčasu výstrahy pre odbočenie, smerové tabule sú osadené až v samotnej križovatke. Napokon sa nám to podarilo a po menšom zaváhaní sme našli aj farmu Tenute di Badia pri mestečku Altopascio, kde sme sa na 3 dni ubytovali. Mali sme objednaný 5-miestny apartmán na prízemí peknej starej kamenno-tehlovej budovy s 2 spálńami, kúpeľňou, a priestrannou halou s kuchynskou linkou, na dvore sme mali pripravený bazén. Majiteľ farmy nás osobne privítal a uviedol do apartmánu.                  Po ubytovaní sme sa rozhodli ešte v podvečer navštíviť neďalekú Pisu, ale o tom už nabudúce. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




