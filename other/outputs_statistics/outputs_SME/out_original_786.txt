
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Urda
                                        &gt;
                Cestovanie
                     
                 Dôchodcov vlaňajší letný mesiac - 1 

        
            
                                    17.3.2010
            o
            10:02
                        (upravené
                5.5.2010
                o
                23:11)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            3458-krát
                    
         
     
         
             

                 
                    Dôchodca vylihuje na posteli a je mu dobre. Na posteli v špitáli čaká na zajtrajšok. Presne na deň po 21 mesiacoch ho zbavia klina a skrutiek v ľavej nohe. Keď tento článok uzrie svetlo sveta, on bude akurát na operačnom stole možno celý v narkóze, možno len umŕtvená noha.  Pred sebou týždeň starý netbook, do ktorého už z notebooku preniesol všetko svoje bohatstvo, a pomaly do dalšieho článku kladá  písmenko za písmenom a spestruje to fotografiami. Spomienky na jeden mesiac vlaňajšieho leta, keď prechodil Slovensko krížom-krážom od Bardejova po Myjavu a od Starej Ľubovne po Fiľakovo. Krásy prírody, mestá, múzeá... Pre neho však najkrajšie hory, naše krásne slovenské hory.... Niektoré objekty navštívil prvý raz, ale tých je málo. V niektoré dni počasie až nepríjemne krásne, horúce, slnečné, v iné pochmúrne, ba až daždivé. Okrem dvoch trojdňových pobytov pod Vysokými Tatrami turistickou základňou všetkých jednodňových výletov bolo jeho mesto. No a pomaly si už richtuje zoznam, čo by mal pozrieť tohto leta, lebo v poslednom období takmer každé leto venuje jeden mesiac takýmto vylomeninám.
                 

                 *  Jeden z mnohých pohľadov na Vysoké Tatry * A teraz jednotlivé dni, ako po sebe nasledovali:   ***DEŇ PRVÝ***  BRATISLAVA - krásavica na Dunaji   Cesta ta a späť vlakom   Cestou z hlavnej stanice do mesta som šiel popri úrade vlády. Tam vedľa v parku bol postavený tento stan. Vyšla z neho strapatá teta, podstatne mladšia odo mňa, a oborila sa na mňa, prečo fotím, tak som sa radšej rýchlo pratal preč   Ceduľa na vnútorných dverách istého bratislavského kostola upozorňovala veriacich na číhajúce nebezpečenstvo   Pohľad na Dunaj y bratislavského kopca   Dom pod kopcom   Živý a mŕtvy vedľa seba  ***DEŇ DRUHÝ***  MANÍNSKA A KOSTELECKÁ TIESŇAVA A SÚĽOVSKÉ SKALY  Vlakom do Považskej Bystrice, autobusom do Považskej Teplej, peši Manínskou tiesňavou, Kosteleckou tiesňavou, cez Záskalie a Vrchteplú po ceste, odtiaľ krížom cez hory do Súľova a do Jablonového, odtiaľ autobusom do Bytče a vlakom domov    Manínska tiesňava - cesta a potôčik sa nezmestia vedľa seba medzi bralá, cesta je nad potôčikom v podobe mosta celou tiesňavou. Po ceste-moste premávajú aj autobusy SAD   Tento maličký potôčik si prerazil cestu pomedzi tie vysoké bralá    Kostelecká tiesňava       Krásne Súľovské skaly   ***DEŇ TRETÍ*** LUČENEC - srdce Novohradu - rodný môj kraj Cesta ta a späť vlakom   Známa lučenská Zlatá ulička   Ošarpaná a spustošená židovská synagóga   Najkrajší kostol v meste a pravdepodobne aj v celom Novohrade - kalvínsky - s kohútom na túrni  ***DEŇ ŠTVRTÝ***   VEĽKÝ FATRANSKÝ KRIVÁŇ Medzinárodný výstup   Vlakom do Šútova a späť, hore cez Šútovo, Zajacovú, Snilovské sedlo, dole popod Chlebom a Hromovým, popri Mojžišových prameňoch, Šútovskom vodopáde a Šútovskou dolinou    Hrdý Veľký Fatranský Kriváň, najvyšší vrch Malej Fatry Prevýšenie voči ž. st. Šútovo väčšie,  ako je prevýšenie tatranského Kriváňa voči Štrbskému plesu     Pod Hromovým priamo zo skaly vyvierajú pramene vody - Mojžišove pramene, ktoré napájajú Šútovský vodopád   Výborná nálada bola pri zostupe pri chate Tatranka s ľudovými muzikantmi.  Celý deň tam prebiehala súťaž heligonkárov   ***DEŇ PIATY***  BYSTRIČKA Bystrička je len na dohodenie kameňom od Martina, tak cesta ta a späť bicyklom   ***DEŇ ŠIESTY***  PIEŠŤANY - svetoznáme kúpeľné mesto, perla Považia Cesta ta a späť vlakom   Pešia zóna   Centrum mesta   Známy kolonádový most na kúpeľný ostrov s kŕdľom labutí pod ním   Kúpeľný dom Thermia Palace na kúpeľnom ostrove   Kúpeľné jazierko s vodnými kvetmi   Sochy piešťanských parkov  Pri prechádzke mestom sa spytujem jednej panej na hotel Lipa, v ktorom som absolvoval niekoľko programátorských školení, keď táto oblasť bola na Slovensku ešte v plienkach.  No a pani si myslela, že si z nej robím "dobrýdeň", pretože sme stáli práve oproti nemu.  Očakával som, že už bude konečne v plnej kráse, a on furt celý zafačovaný ako pri mojej predchádzajúcej návšteve Piešťan. Aj odfotiť som ho zabudol.  ***DEŇ SIEDMY*** BANSKÁ ŠTIAVNICA - jedno z najkrajších miest na Slovensku, mesto s bohatou históriou,  v ktorom sídlila prvá technická vysoká škola na svete, mesto zapísané v r. 1993 do Zoznamu kultúrneho a prírodného bohatstva UNESCO.     Pohľady na slávne banícke mesto   V krivolakých uliciach mesta   Kalvária nad mestom: niektoré časti sú zrekonštruované, iné sa rekonštruujú a niektoré ešte len čakajú na sponzorov   Nový zámok, ktorý bol vybudovaný ako strážna veža v časoch tureckých vpádov. Dnes je tam Múzeum protitureckých bojov.  ***DEŇ ÔSMY***  KOŠICE A LIPTOVSKÝ MIKULÁŠ Cesta ta a späť vlakom  KOŠICE - metropola východu   Krásny zámoček, ktorý bol aj dočasným sídlom prezidenta Beneša  po jeho návrate z Londýna do Československa   Štátne divadlo   Dóm   V centre za dažďa  LIPTOVSKÝ MIKULÁŠ - historické mesto s krásnym prírodným okolím   Historické námestie   Pred Liptovskou galériou P. M. Bohúňa   Okolie L. Mikuláša - Liptovská Mara s pohľadom na Choč   Pokračovanie 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (66)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Z Detvy do Hriňovej...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Znova z Handlovej do Prievidze
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Králická tiesňava a vodopád
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Zákopčianskymi kopanicami
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Hmly a spomienky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Urda
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Urda
            
         
        urda.blog.sme.sk (rss)
         
                        VIP
                             
     
         jednoducho dôchodca, jednou rukou na blogu, druhou nohou v hrobe so životnou zásadou podľa Diderota: 
 "Lepšie je opotrebovať sa, ako zhrdzavieť" 

                                    * * * 
Krásy Slovenska som propagoval i tu: 
http://fotki.yandex.ru/users/jan-urda/ stále živá stránka - http://fotki.yandex.ru/users/jujuju40/ 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    517
                
                
                    Celková karma
                    
                                                7.56
                    
                
                
                    Priemerná čítanosť
                    2324
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Detstvo - spomienky
                        
                     
                                     
                        
                            Škola - spomienky
                        
                     
                                     
                        
                            Práca - spomienky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Z Turca
                        
                     
                                     
                        
                            Staroba - spomienky
                        
                     
                                     
                        
                            Filatelia - svet poznania
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Nezamestnanosť - najzávažnejší problém na Slovensku
                     
                                                         
                       Môj 4% ný kamarát.
                     
                                                         
                       Straka
                     
                                                         
                       Zadarmo do Žiliny
                     
                                                         
                       Oneskorene k 17. Novembru ....
                     
                                                         
                       Komunálne voľby prerušené na 38 minút.
                     
                                                         
                       Inzerát
                     
                                                         
                       Koľko je reálne hodné euro?
                     
                                                         
                       Gruzínsko - cesta do kraja bez ciest
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




