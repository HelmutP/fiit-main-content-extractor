

 Bolo tam nádherne, či už na ľadovej ploche alebo i mimo ňu. Ohromne veľa zážitkov, skúseností a stretnutí je za nami. Veľa videného i počutého by mohlo byť zárukou, že niečo so sebou, okrem maskotov a suvenírov, domov i donesieme a obohatíme tak svoje najbližšie, ba možno i vzdialenejšie okolie. 
   
  
   
  
 Na ľade sme sa snažili, no rozdiely v kvalite sme nedokázali, zatiaľ, prekonať. Akokoľvek sme boli v pár zápasoch blízko k dobrému, dokonca výbornému výsledku, predsa len nám záver unikol a výhry sme sa nedočkali. No výsledky, ktoré máme za sebou, či už s domácou Kanadou (18:0), alebo so Švédkami (6:2), potom so Švajčiarkami (5:2), s Ruskami (5:2) a nakoniec i s Čínou (3:1) nie sú tak hrozivo priepastné, vzhľadom na naše slovenské pomery v ženskom hokeji. Pravdaže, Kanada nám odskočila ďaleko, ale tá je ďaleko pred všetkými. Pre nás prínosné i potešujúce zároveň bolo, že sme dokázali odolávať  tlaku a hrať "pomerne" vyrovnane s ostatnými tímami top divízie, a že ten odskok rusiek či severaniek alebo i aziatiek nie je neprekonateľný. Mravčou, vytrvalou a obetavou prácou s hokejistkami, mladšími či staršími, je možné v budúcnosti nejaký ten zápas azda i vyhrať... Len - žiada si to pomoc i širšieho okolia, zainteresovanosť kompetentných, lebo snaha nás hokejistiek nestačí zvládať všetku záťaž... 
 Hoci vieme, v akých pomeroch sa zmieta slovenský hokej, sme jeho súčasťou a žiadame svoj podiel pozornosti, ktorý si nesporne minimálne svojimi poslednými výsledkami zaslúžime. A i napriek tomu, že sme dokázali aj bez tejto pozornosti dosiahnuť úspech v podobe kvalifikovania sa na OH a postupu do A skupiny MS, aj napriek tomu sa cítime byť súčastou slovenského hokeja... 
 Záujem hrať hokej zo strany dievčat je, len otázka, "kde a kedy môžem začať" často stroskotá na klubových podmienkach toho ktorého mesta. I keď, vďaka, že existujú výnimky! 
 Nech už akokoľvek, boli sme tam a zúčastnili sa na historických olympijských hrách nielen pre nás, účastníčky, ale aj pre ženský hokej na Slovensku vôbec a to je významný medzník, ktorý nemožno a nesmie sa opomínať!  Pravda je, že máme na čom stavať! Kedy, keď nie teraz! :-) 
  
   
 Účasť na OH má pre nás význam nielen športový, ale veľkoleposťou týchto hier naberá aj spoločenský, kultúrny, sociálny a ľudský rozmer. Mali sme možnosť hrať na ľade kvalitných štadiónov, vidieť športové areály mesta, navštíviť prírodné parky Kanady, stretli sme priateľských a ochotných ľudí. Videli sme svet! 
 Už sme nejakú tú dobu doma, a účasť na olympijských hrách sme prijali takmer ako každodennú resp. „každo-4-ročnú" aktivitu. Prečo nie opäť v Soči? Odpoveďou je, prečo nie Vancouver 2010! Stali sme sa príkladom, že všetko je možné, a ako hokejistky z maličkej krajinky uprostred Európy sme na kanadskom území hájili naše farby. Ambasádorky športového ducha, predvoj úspechu slovenských hokejistiek a športu - bude niekomu vzorom? Potiahneme našu mládež? Kiež by tomu tak bolo.... Kiež by sa vylepšili naše športové vzťahy na rôznych etážach spoločnosti laickej i odbornej. 
   
 ....pre česť a slávu športu. Tak sľubujeme. 
   
   
   

