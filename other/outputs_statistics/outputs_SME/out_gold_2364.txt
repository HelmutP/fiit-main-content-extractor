

 zakážu vravieť pravdu 
 pribitú na kríž - 
 veriť zvonom, 
 v ktorých slnko 
 zhasne na rukách. 
 Čo viac ti ostane, 
 ako zraziť tmu na kolená. 
   
 Bez snov si 
 ako Jeruzalem 
 bez svojho Syna - 
 oltár bez modlitby, 
 v ktorej zomiera tvoja Atlantída, 
 tá večná zem bez zvonov 
   

