

      Už v podvečer sa davy fanúšikov zhromaždili pred MŠH, aby sa dostali čo najbližšie pod ruky hlavnej hviezdy večera. Pri vchode každého fanúšika čakalo malé prekvapenie v podobe croissantu. Aj keď predkapela, česko - slovenská kapela Popcorn Drama, na pódiu predvádzali svoj repertoár, zhruba polovica fanúšikov stála čakajúc pri vchode. No keď Ivan Tásler a spol. vystúpili na pódium, plná hala burácala a krikom privítala nadaného speváka. 
   
      Dokonca prekvapilo Ivana natoľko, že po skončení koncertu povedal: ,,Prešov je najvďačnejšie publikum na Slovensku a aj kvôli tomu sa tu rád vraciam." Nielen rozpálený dav dokázal prekvapiť. Zahanbiť sa nedali ani členovia skupiny, ktorí predviedli fantastickú šou plnú nových hitov z najnovšieho albumu, ale aj starších hitov ako Veselá pesnička, Exotika a Opri sa o mňa. Jednu z týchto najznámejších pesničiek skupiny venoval Vladovi Krauzsovi, ktorý koncert sledoval priamo z publika.Najväčším prekvapením bol saxafónista , ktorý vyšiel počas koncertu z davu sediacich a po schodoch zišiel hrajúc na unikátnom nástroji dole, kde už naňho Ivan čakal. 
   
      Dá sa povedať, že Táslerovci opäť nesklamali a s určitosťou môžeme povedať, že odohrali najlepší koncert so šokujúcimi svetelnými a zvukovými efektmi z dielne odborníkov, aký Prešovská športová hala kedy zažila. 
      Skupina I . M. T. Smile funguje od roku 1992 kedy ju súrodenci Miro a Ivan Tásler založili. V súčasnosti má skupina 5 členov a na konte 8 albumov. Práve vyšiel 9. s názvom Odysea 2, s ktorým sa spája aj toto vydarené turné... 

