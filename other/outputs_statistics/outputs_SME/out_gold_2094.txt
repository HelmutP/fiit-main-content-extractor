

 Kto mal to šťastie a prešiel si tieto diaľavy, vie, že zastaviť sa je možné len v strašidelných moteloch s neuveriteľnými názvami napr.: „Ďaľeko od ženy", kde žijú a aj vás obslúžia  „merzotnye bľadi" ktoré vás pozývajú v búdky-jebudky s rozmerom niekde okolo 2x2 m. 
 Skrátka, Tarantino so svojim  Od súmraku do usvitu(From Dusk Till Dawn) je jednoducho „čmo" - on nevidel naozajstný život. 
 Vo všetkom tom „gavne" sa veľmi komfortne cíti iba jediná skupina ľudí. „Daľnobojščiky", po našom - v zjednodušenom preklade Tiráci. 
 No a miesto pre príjem stravy v takýchto moteloch sa skladá zo strašného domčeka Nif-nifa - „vagónčik" zlepená škatuľa  na hodine domácich prác, alebo „sarajčik" (kôlňa) a „v pontových" (na prestížnych) miestach naozajstný vagón, kde sa akýmsi zázrakom vojde personál, kuchyňa a hlavne „otkušivajuščie" (čiže pojedajúci). 
 O týchto miestach som už veľa písal. Pri cestách po šírej matke Rusi sa im nevyhneš. Aj tentoraz. 
 Omská oblasť - "strašná žopa!". 
 Nejako sme vyhladli, cesta do civilizácie ešte ďaleká a zásoby na nule. V takýchto momentoch sme vždy zastali pri nejakom „mangale" (plechová škatuľa 20x50 cm na nôžkach s žeravými uhlíkmi) a keď tam bol kaukazec a pobehovali psici miestnej „paródy" tak sme si dali šašlik. Kaukazec by nikdy neurazil šašlik psím mäsom. Miestny Blonďák bez problémov. 
 No tu dávno nič sme nestretli, museli sme sa zastaviť „pažrať" pri takom to motele. 
 Pred „vagónčikom" ako je tu zaužívané „štuk 6 fur s vodilami" (6 ks nákladiakov so šoférmi) - čas obeda. 
 Vo vnútri ako sme predpokladali - šesť obrovských „mužíkov", päste mali ako moja hlava. 
 Jedia všetci to isté jedlo - „peľmeni" - špecifickú vôňu týchto malých pirôžkov, naplnených mäsom, plávajúcich vo vlastnom vývare, poliatych hustou smotanou je ťažko vôbec opísať. Kto nejedol, nepochopí. 
 Vedľa každého taniera čaj - tu zvaný „čefír". „Závarka" (lístky čaju) je do polovice pohára a nepripravený človek po hlte tohto nápoja nie je schopný zopár minút pohnúť čeľusťou. Tak stŕpne. 
 Na všetkých tvárach - sústredenie bez emócii, všetci napäto, rytmicky hrmocú lyžičkami, všetci do jedného pozerajú na jedno a to isté miesto niekam pod plafón.  Je hrobové ticho. 
 Nikto s nikým nehovorí, všetci počúvajú. 
 Pozriem na miesto, ktoré ich takto hypnotizuje a vidím, televízor na stene, kde práve ide nejaká brazílska mydlová  tele novela. 
 Podľa všetkého ide o moment vyvrcholenia nejakého konfliktu. 
 Nejaká stará latiská „mraz" (slušne povedané potvora) ženského rodu vysvetľuje inej mladej sympatickej babe niečo v tom zmysle, že ona vie, s akou inou latinskou hnusobou jej muž zahýba. 
 Vyvrcholenie emócii je až také, že títo obrovskí mužíci zamierajú,  niektorí dokonca aj proces žuvania spomaľujú, vidím na tvári - svaly napätia... zjavne negatívny vzťah k žalobabe. 
 A v najemotívnejší moment, v ten najdôležitejší, keď po nekonečnom dialógu, keď stará baba už už má povedať meno „razlučnice" (tej čo muža kradne) - cítim vo vagóniku vnútorné napätie (aj kuchár vybehol) no a v tento dôležitý moment  začína reklama. 
 Napätie ihneď padá, jeden „zdarovyj" obrovský daľnobojščik ne vydrží, vstáva od stola, pozerá priamo do televízora, nazúrené hádže vidličku o stôl (v Rusku sa málo používa pri druhom jedle aj nôž), tá odlieta do rohu a nahlas hovorí:  „VOT SVIŇA!"*** 
  Zberá veci a v tichu vychádza. Nikto sa v miestnosti ani nepohol. 
 Po chvíli všetci začínajú „žrať" ďalej. 
 Opona padá. Paka 
 PS:*** Asi chápeš, že náš rozcítený hrdina použil tu nepoužiteľné slovo. 

