

 Vstala som, a šla som si posťahovať moje obľúbené ruské a francúzske pesničky z internetu, napálila som si ich na CD a už som sa tešila, aké bude dnes ranné umývanie. Pripravila som si aj cviky, ktoré som videla s fyzioterapeutkami, reku si zacvičíme po raňajkách a obed, ten ukuchtím presne tak rýchlo, ako moja domáca. 
 A vstávanie veru bolo úžasné. 
 Ako vždy - objatie, pohladenie s mojou formulkou: „Dnes vám dávam veľa energie plnú spevu vtáčkov, ktorú som si vzala z neďalekých stromov. Vezmite si ju prosím, bude sa vám lepšie vstávať. Celý deň budete vládať všetko a nebudete unavený (všetky frázy nacvičené z prekladového slovníka :-) ) 
 Je úžasné robiť rannú toaletu pri tejto hudbe dedkovi. Dedkovi dávam nielen ja energiu, ktorej mám veľmi veľa, ale dnes ešte aj tá hudba. Striedala sa lahodná, zamýšľajúca, plávajúca s energickejšou, a tak som mu občas zamávala veselo rukami ako pri tancovaní. Dedko sa dnes viac sám umýval, utieral. A hlavne viac sa usmieval. Nemal pokrčené čelo. Jeho obočie nesmerovalo dolu, ako pri bolesti. Bolo zdvihnuté a hnedé očká mu žiarili. 
 Keď vstával, dotkol sa len stoličky, mojich pliec nie. Zas úspech. Pochvala - „ausgezeichnet" výborne. 
 Dedko dlho raňajkoval, potom som ho nechala v tichu oddýchnuť a šla som si pochystať niečo na varenie.  Vari po 15 minútach som vzala pripravené cviky  a s úsmevom som vošla ku do izby. Keď videl, že ideme cvičiť, dal sa do pozície pri ktorej vstáva a odkráčal bez paličky na stoličku. Posadila som sa oproti nemu. Všetky cviky som mu predvádzala úplne v tichosti za doprovodu melódií z CD -Dido a on po mne krásne opakoval a niekedy aj naviac. Pri niektorom cviku viac vládal ako ja :-). Úsmevné však? Nuž ja s tým mojím bruchom, nemám na dedka. 
 Takmer sme dokončili, keď sa dedko potreboval dostať na toaletu. Potom som ho už netrápila ďalej a povedala som, že na dnes dosť, aby nebol ustatý. Ja reku idem variť obed. Pustila som mu jeho obľúbeného Tuchoľského a šla som po robote. Neviem, či prešlo 10 minút a zazvonil zvonec. 
 Skoro som spadla z nôh, keď som počula, že ide fyzioterapeutka. Vôbec som nevedela, že má prísť a tak som sa začala smiať, že čo ide dedko robiť. Aký bude zas zničený?! 
 Nechali otvorené dvere, nuž som občas nakukla, ako sa dedko snaží. Po 30 minútach cvičenia a chôdze aj po schodoch fyzioterapeutka posadila dedka na stoličku a známym spevavým „tschús" odišla. Prišla som do dedkovej izby smejúc sa, že sme ho 2 ženy poriadne zničili, a dedko sa smial tiež, ale vôbec nie zničene. Ozaj! Predstavte si, že má všetky svoje zuby, ktoré si denne pekne 2x očistí celkom sám (má 72 rokov). A to som myslela, že mu budem ešte vyberať aj chrup a čistiť ho pekne ručne a vkladať do pohára. Kdeže! Dedko má úžasný chrup. Poviem vám, že hociktorý 50 -nik mu môže závidieť. Aj ja. 
 A tak sme sa zasmiali a ja som mu oznámila čas obedu. Započúval sa opäť do Tuchoľského, číta ho pán s úžasným hlasom. Aj mne sa dobre počúva. 
 Dokončila som obed - opäť ryby, ale na iný spôsob so zemiakovou kašou a mrkvovo-papájovým šalátom, polievku som urobila šalátovú a šla som ho zavolať. 
 Jedli sme pri veľmi krásnych ruských gitarových skladbách a keď dedko dojedol, otočil sa ku mne a po prvý krát mi povedal: - Karola, vieš výborne variť, dávaš tam veľa svojej dobrej energie. Veľmi veľa. 
 Usmiala som sa, poďakovala za prejavený kompliment a šla som ho uložiť. 
 Do postele sa ukladal takmer sám. Len jemným dotykom som ho nasmerovávala, nakoniec sa posunul doprostred postele a ja som žasla, ako to dokáže. 
 Dedka jednoducho lieči aj ruská a francúzska hudba.  A on si zamilováva všetko, čo mu prinesiem. To som ani len netušila, že niekedy môžem Nemca poblázniť mojou hudbou a piesňami zo 70. rokov. 
   

