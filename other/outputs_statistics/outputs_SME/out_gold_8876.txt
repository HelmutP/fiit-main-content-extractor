

 Zazvonil mi telefón. Odložila som rozbehnuté veci a zdvihla. 
  ,,Poď rýchlo ku mne, opálila som si ruku žeravým olejom. 
 Moje mozgové závity totálnej blondínky mi začali fungovať na plné obrátky. 
 ,, Urýchlene si daj ruku pod tečúcu vodu a maj ju tam,  
 hneď som u teba.“  
 Schmatla som kľúče od auta a letela tie dva km. 
   
   
 Keď som prišla a počula tiecť vodu, uľavilo sa mi aj kamarátka bola v poriadku a menej cítila bolesť.  
 ,,Ideme na pohotovosť,“  povedala som jej. Vodu zastavila a ruka zasa začala páliť a červenieť. 
 Usúdila som, že bude lepšie, ak si vezmeme so sebou vedro s vodou. 
   
   
 Prichádzali sme na pohotovosť. Ja som na rukách držala vedro s rukou vo vode. Zdravotný personál musel vidieť náš príchod, lebo všetci vyleteli von s krikom, vrátane čakajúcich pacientov, či nesieme vo vedre odrezanú ruku.  
  ,,Iba opálenú,“  vravíme . 
   
  Sadli sme si teda do čakárne a čakali. 
 Pán, čo tam sedel a videl ten masaker ,,odrezanej ruky,“ vletel do ordinácie a zabuchol dvere.  
   
 O chvíľku ho sestrička vyviedla do čakárne, s vedrom, že ak by mu bolo zle, aby ho v prípade nutnosti použil. 
   
 A tak sme tam už sedeli a čakali s dvomi vedrami.  
   
 Kamarátka bola statočná. Studená, tečúca voda jej zachránila pokožku a ušetrila ju od bolesti. Dostala špeciálny obväz a všetko dobre dopadlo. 
   
 Vždy, ak dôjde k podobnej popálenine, je dobré ochladzovať postihnuté miesto pod tečúcou vodou, dá sa tak vyhnúť vzniku väčších pľuzgierov.  Nedávať na to hneď oleje a mastičky. Človek sa ušetri pred bolesťou a zničením tkaniva. 
   
 Moja rada:  
 Opekajme len vtedy, ak sme prítomní aj duchom. 
   
   
   
   

