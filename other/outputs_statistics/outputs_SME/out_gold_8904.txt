

  
 eeeeeeeeeeeeeeeeeeeeeeeeee 
 eeeeeeeeeeeeeeeeeeeeeeeee 
 e 
  
  
  
 [Správa z webu: sinsky@wircom.sk, IP odosielateľa: 217.119.117.213] Pán JÁRAY, potrebujem vypočítať celkovú hybnosť Zeme. Dokážete to? Ďakujem vopred! 
  
 Vážený adresát! 
 Najprv naznačím ideový výklad vašej otázky. Pod pojmom hybnosť chápeme súčin rýchlosti a hmotnosti telesa. Keďže každý pohyb je v zmysle súčasnej fyziky iba relatívny, iba očná vidina, tak aj hybnosť telies môže byť iba očná vidina. Z čoho potom plynie, že jedno teleso môže mať nekonečne veľa relatívnych hybnosti, ale aj nulovú hybnosť a to navyše aj naraz. Takže moja odpoveď na vami položenú otázku znie nasledovne:  
 Zem má nekonečne veľa (očami videných) relatívnych hybnosti! 
 Keby ste chceli vedieť akú absolútnu hybnosť má Zem vo vesmíre, v konečnom dôsledku keby ste chceli vedieť akou absolútnou rýchlosťou sa pohybuje Zem vesmírom, v tom prípade museli by ste mi opísať aký je podľa vás rozdiel medzi relatívnym a absolútnym pohybom Zeme, lebo tade vedie cesta k rozuzleniu gordického uzla vašej otázky.  
  
 (Pokračovanie po doručení doplňujúcej odpovedi.) 
  
 Správa z webu: sinsky@wircom.sk, IP odosielateľa: 217.119.117.213] O.K. Ja nie som fyzik, ani matematik, som amatér. Toľko viem, že sú veci relatívne aj absolútne. Chcem zistiť, koľko má pohybovej energie zo svojej hmotnosti a rýchlosti, aj zo svojej rotácie, planéta Zem. Môžeme sa dohodnúť, že to vypočítate na splynutie Zeme s povrchom Slnka. 
  
 Píšete: "Toľko viem, že sú veci relatívne aj absolútne." Uvezte že ktoré veci sú relatívne a ktoré absolútne. Hybnosť Zeme vzhľadom k jej pohybu okolo slnka, nie je skutočná hybnosť, ale iba jedna z hybnosti Zeme. 
 
   
 
 
  (Pokračovanie po doručení doplňujúcej odpovedi.) 
 
  
 
 
 [Správa z webu: sinsky@wircom.sk, IP odosielateľa: 217.119.117.213] Viete, o relativitu mi ani nejde, je zaužívané, že to patrí k všeobecnému vzdelaniu, tak to rešpektujem. To si pamätám ešte zo školy, že ak napr. ja teraz sedím - som teda voči predmetom v bezprostrednom okolí v pokoji, ale ten pokoj je iba relatívny, pretože Zem sa hýbe okolo Slnka a zároveň Slnko sa otáča okolo Galaxie atď., je dosť možné, že vesmír sa točí a zrejme v tomto pozorovanom vesmíre aj rozpína. Ja cez klasiku niečo viem vypočítať; odpíšem si niekde hmotnosť Zeme, vieme z astr. pozorovaní rýchlosť Zeme, vieme aj uhlovú rýchlosť pri otáčaní Zeme. Jednoducho povedané, že tak z brucha to viem vypočítať, koľko by sme mohli odčerpať energie, až kým by Zem pristála na Slnku. Lenže môže byť hmotnosť Zeme iná. A, tak som predpokladal, že by to mohlo korešpondovať s rozpínaním vesmíru a by ste to do tej celkovej energie, ktorú by bolo možné odčerpať, použili. 

  
 
  A o to tu ide. Keby ste chceli vypočítať hybnosť Zeme, tak na to stačí zistiť obežnú rýchlosť Zeme okolo slnka a tá činí 30 km/sec a vynásobiť ju hmotnosťou Zeme, ktorá je približne  5,98×1024 kg a vec je vybavená. Kvôli tomu by ste sa neobrátili na mňa.  
 Zápis nerovnosti  Fa &gt; Fr, hovorí o tom, že sila akcie je vždy väčšia ako sila reakcie (čo protirečí všetkým zákonom klasickej  i relativistickej fyziky) znamená iba toľko, že ak akceptujeme princíp kauzality, že na impulzu sily akcie nemôže, okamžite, v jednom čase vzniknúť sila reakcie (lebo potom by bol problém učiť ktorá sila je akčná a ktorá reakčná) potom nemôžeme akceptovať Newtonov Zákon akcie a reakcie, lebo on tvrdí, že na impulz sily akcie, naraz  v jednom čase vzniká sila reakcie, čo sa medzi rečou nepovšimne, ale zápisom Fa = Fr, sa to zdôrazňuje.  
 To by ale znamenalo, že existuje aj nekonečne veľká rýchlosť šírenia impulzu sily. Potom táto rovnosť sa dá zapísať dvomi ekvivalentnými rovnosťami a to Fa = Fa;  Fr = Fr. Z tejto dvojice rovnosti neskôr vznikol vo fyzike princíp ekvivalencie akčnej a reakčnej silovej interakcie.  
 Inými slovami povedané ak galaktická rýchlosť  slnka činí 217 km/sec a obežná rýchlosť slnka okolo slnka je 30 km/sec, tak hybnosť Zeme sa mení v rozmedzí (217 + 30) km/sec a  (217 - 30) km/sec . A ešte k tomu ešte je potrebné vziať do úvahy zmenu hmotnosti Zeme ktorá vzniká následkom zmeny jej (čiastočnej) absolútnej rýchlosti pohybu okolo slnka pohybujúceho sa čiastočnou, galaktickou absolútnou rýchlosťou okolo stredu galaxie.  
 Problém hybnosti Zeme nie je matematický problém, ale ani fyzikálny, je to skôr problém filozofický, ktorý stojí a padá na definícii pojmu pohyb hmoty. Ak je pohyb relatívny, tak všetko okolo hybnosti Zeme je jasné. Ak je pohyb absolútny, tak okolo hybnosti Zeme nič nie je jasné (v zmysle zákonov relativistickej fyziky).  
 A je len prirodzené, že bez nerovnosti Fa &gt; Fr, sa o rozpínaní vesmíru nedá povedať nič rozumného. Predstava že sa vesmír rozpína mimo silovej interakcie Fa &gt; Fr, len tak s pasie, z rozmaru, je fantazmagória teoretických fyzikov relativistickej definície pohybu (boh vie čoho).  
 V tom momente kedy sa exaktným spôsobom zadefinuje absolútny pohyb hmoty, v tom momente sa skončí sláva relativistickej fyziky i rovnosti Fa = Fr. (Tejto definícii sa relativistický fyzici boja viac ako čert svätenej vody.) Preto v chápaní rozdielu medzi absolútnym a relatívnym pohybom hmoty, v stave chemických prvkov, je ukrytý problém dynamiky materiálnej (pozorovateľnej) časti vesmíru.  
 Kto nepozná rozdiel medzi absolútnym pohybom a relatívnym pohybom, musí byť otrokom výmyslov relativistickej fyziky. Ja som slobodným človekom, bez okov relativistickej fyziky. Ak sa neurazíte, vy nie ste slobodným človekom, teda aspoň zatiaľ nie. 
 
  
 (Pokračovanie po doručení doplňujúcej odpovedi.) 

