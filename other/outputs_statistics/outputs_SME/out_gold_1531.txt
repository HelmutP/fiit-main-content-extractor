

 Cieľom tohto príbehu nie je nám podať nejakú informáciu v zmysle -  kde, kto, čo a prečo. Jeho cieľom je upozorniť na nejakú skutočnosť,  ktorá nemusí byť zrejmá na prvý pohľad. Ukázať na nejakú skutočnosť, ale  novým spôsobom, z iného uhla pohľadu. 
 Pokiaľ by autor napísal len, že ľudia vo svete nemajú dôveru ku kresťanom, sú im na smiech, ich posolstvo vnímajú, ako niečo vysoko neaktuálne - tak to nemalo takú silu, ako keď použijete príbeh o nejakom klaunovi, ktorý príde niečo hovoriť do nejakej dediny. Nevieme, prečo sa klaun tak rozhodol, čo ho motivovalo. Nevieme, či ľudia v danej dedine mohli niečo vedieť o možnom ohni. Máme len informácie, ktoré nám autor poskytol. 
 Tento príbeh chce, aby sme sa zamysleli nad tým, kde sa ocitlo kresťanstvo po dlhých rokov uplatňovania kresťanskej zvesti v praxi. Akú predstavu o ňom majú ľudia, ktorí netvoria "tvrdé jadro priaznivcov". Treba sa nad ním zamyslieť a vyvodiť nejaké dôsledky pre svoj život. Uvedomiť si, že pre niektorých ľudí bude hovorenie o kresťanstve predstavovať rovnaké bláznovstvo. 

