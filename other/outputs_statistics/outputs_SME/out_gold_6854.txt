

 A tak sa zakrádam ticho a pomaly 
 Ako keď smrť sa plíži po krajine 
 Keby ste ma blázni naozaj poznali 
 Nesprávali by ste sa naivne 
   
 Voláte ma vaším priateľom i radcom 
 Tá pretvárka ma verte mi, že bolí 
 Dnes dostane sa pocty všetkým zradcom 
 Skončia nabodnutí na drevené koly 
   
 Nemám silu preto nehrám čisto 
 Nehrám, ani nebudem hrať fér 
 Oproti mne Gróf Monte Christo 
 Vyzerať bude ako amatér 
   
 Moju ruku vedie  moja pomsta 
 A nech prekliaty na večné časy 
 Je rod váš, čo i do potomstva 
 Ja dýkou dnes vašu dušu spasím 
   
 Počul som, že pomsta býva sladká 
 No ja v ústach cítim horkú chuť 
 Keď nový život mi ani nová matka 
 Nemôže viacej ponúknuť 
   
 A tak sa zakrádam pomaly po krajine 
 Tak ako smrtka v čiernom kabáte 
 Už je mi jedno kto vlastne bol na vine 
 Nehrajte s pomstou, aj tak prehráte 
   
   

