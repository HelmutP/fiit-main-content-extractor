

 Dovolím si tvrdiť, že bude víťaziť „jednoduchosť". Technológie v IT išli dopredu tak rýchlo, že sa často zabúdalo na to najpodstatnejšie: na užívateľa. Žijeme v prostredí, ktoré v poslednom období prinieslo neuveriteľné množstvo nových technológií. Tieto nové hračky lákajú nás vývojárov na hru,  v zápale ktorej  často zabúdame na účelovosť. Tak vznikajú komplikované zariadenia a programy, ktoré sa ťažko používajú, a čo je horšie, v bežnom živote sú nanič. Často sa  vyvinú produkty a až potom sa hladá ich praktické uplatnenie. 
 A tak namiesto "pretláčania" čo najväčšieho počtu technologických noviniek a ohurovania zákazníka, je dnes dôležité niečo iné. Musíme si položiť dve otázky: 
 
 Čo užívateľ od nás očakáva, a čo naozaj potrebuje v živote? (či už pracovnom alebo súkromnom) 
 Ocení užívateľ viac jednoduchosť zariadenia, alebo množstvo funkcií? (ktoré v konečnom dôsledku zariadenie komplikuje) 
 
 Softvér neslúži na to, aby ohúril množstvom tlačidiel a funkcií. Slúži hlavne na to, aby sa s ním dalo pracovať. A pokiaľ možno jednoducho. Túto víziu "jednoduchosti" priniesol napríklad Steve Jobs do Applu. Nové produkty vo vývoji neposudzoval ako technik, ale ako používateľ. A bol to hlavne on, ktorý sa snažil odstrániť z produktov čo najviac možno nepotrebných funkcií a parametrov. A tak osekal funkcie zariadení tak, že sú dnes inšpiráciou rôznych vtipov: 
   
  
 Obr. 1: Porovnanie iPad-u s melónom :)  
 Čím je zariadenie komplikovanejšie a čím viac možností ponúka, tým je náchyľnejšie na poruchy. Toto je vo všeobecnosti technickým pravidlom. Reťaz je taká silná, aký silný je jej najslabší článok. A čím menej modulov má systém, a čím sú jednoduchšie, tým jednoduchšie je ovládanie systému. A tým je menej náchylný na poruchy. 
 Prehrávač hudby má predovšetkým slúžiť na prehrávanie hudby. Na to stačí pár tlačidiel. Darmo je používatelovi kopec tlačidiel a funkcií, keď ho nahratie skladby do prehrávača stojí kopec času a nervov. 
 Steve Jobs chápal produkty ako vec, ktorá má spôsobiť u používateľa zážitok. Nie sú dôležité parametre, ale skúsenosť používateľa (a následne aj predajnosť). Preto Apple nepredáva „len počítač", ale napríklad aj „domáce hudobné štúdio". Raz som strávil pol hodinu v predajni Applu, kde boli na počítač pripojené girtara a klávesy. A hral som sa. Vôbec nebolo treba zisťovať ako to funguje, všetko bolo intuitívne, a bolo to zážitkom. 
 Najnovšie som v stĺpčeku Marka Zuckerberga  čítal jeho reakciu na žiadosť používateľov na väčšiu, ale jednoduchú kontrolu súkromia na Facebooku. Tieto dve požiadavky sa v systémoch dosť bijú, s čím okrem Facebooku zápasí napríklad aj Google. 
 Ako hovorí Mark, zakladateľ Facebooku: „Najdôležitejsia správa, ktorú sme počuli v poslednej dobe je, že ľudia chcú jednoduchšiu kontrolu nad svojimi informáciami. Jednoducho povedané, mnohí z vás si myslia, že naše ovládanie bolo príliš zložité. Naším zámerom bolo, aby sme vám dali veľa možností kontroly, ale to nemusí byť to, čo mnohí z vás chceli. Iba sme sa minuli cieľu. [1] 
   
  
 Obr. 2: Prečo sú aplikácie Apple a Google úspešné, a vaše nie :) 
 Užívateľ chce jednoduché ovládanie. Aby práca s počítačom nebola vedou, ale zážitkom. Preto „víťazia" jednoduché aplikácie. A toto zjednodušovanie bude pokračovať. Práve preto Microsoft začína zaostávať. Používateľ nemá chuť neustále nastavovať komplikovaný systém, a pravidelne inštalovať aktualizácie. 
 Možno nastáva nová éra, na čele ktorej  stojí Google. Počítačový priemysel sa otriasa v základoch, ukazuje sa jeho neefektívnosť a stáva sa príliš nákladným, rovnako ako na konci éry strediskových počítačov (kde kraľovalo IBM). Firmy ako Google, Salesforce a Yahoo, sa snažia pomocou internetu urobiť prácu s počítačom efektivnejšiu, produktívnejšiu a lacnejšiu. A nie je to práve to, čo vlastne chcú užívatelia od svojho počítača? 
 Programy a dáta už nie sú sústredené iba na PC, ale sú dostupné online. Ludia s týmito aplikáciami pracujú prostredníctvom internetu. A internet sa stáva počítačom. Tento trend je známy ako „software ako služba" (SaaS) alebo „cloud computing". Softvér sa nepredáva ani neinštaluje na jednotlivé počítače, ale beží online v „internetovom oblaku" a užívatelia si ho prenajímajú alebo im je voľne k dispozícii. [2] 
 Toto je pravým opakom trendu ktorý začal Microsoft. A práve preto Microsoft môže byť nervózny. 
 A aké výhody ponúka užívateľovi "cloud computing", napríklad v podaní Googlu? Softvér beží na serveroch, takže najnovší hardware nieje potrebný. Dáta sú uložené tiež na serveroch, takže nerastie požiadavka na pamäťovú kapacitu. Aktualizácia softvéru, odstraňovanie chýb a vylepšovanie robí Google na diaľku. Za takýchto podmienok môžu byť PC lacnejšie a jednoduchšie. A tým sa dostávame od zjednodušovania softvéru k hardvéru. Zastarávanie počítačov sa tak spomalí. 
 Ďalej je to možnosť zdieľania. Obsah vzniká tam, kde sa ukladá. To znamená e-maily, tabulky, dokumenty, fotky a ďaľšie výsledky osobnej tvorivosti. Už teraz napríklad existuje množstvo online programov na úpravu a následné zdieľanie fotografií. Bez nutnosti kupovania a inštalácie programu, a často kliknutím na pár tlačidiel. Navyše je tu možnosť spájania informácií (napríklad odkazy na Google maps v blogoch). 
 Samozrejme sa vynárajú aj nevýhody, ktoré sú podobné ako pri sociálnych sieťach: zabezpečenie ochrany súkromia. Google aj Facebook garantujú, že vaše údaje sú dobre zabezpečené, a nebudú poskytnuté iným osobám. Stále však platí, že k nim majú prístup. 
 V blízkej budúcnosti budú pravdepodobne kombinované obidva prístupy: používanie online aplikácií, ako aj samostatne fungujúce PC programy. Oba prístupy majú prednosti aj nevýhody. Ten reprezentovaný Microsoftom (offline) je pre používateľa nákladnejší, zatiaľ čo prístup Google (online) je vo väčšej miere ohrozovaný zrútením siete a odstrihnutia užívateľa od programu.  [2] 
   
 V najbližšej dobe budú podľa mňa vládnuť informatike dva smery: zjednodušovanie softvéru a zariadení, a presúvanie dát a programov z osobných počítačov na internet. 
   
 Odkazy: 
 [1] Môj preklad stĺpčeka Marka Zuckerberga: Facebook odpovedal na otázky súkromia 
 [2] Kniha: Jak myslí Larry Page a Sergej Brin 
   
 Tiež odporúčam knihu: Jak myslí Steve Jobs 
   
   

