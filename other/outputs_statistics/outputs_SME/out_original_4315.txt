
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marian Letko
                                        &gt;
                stava sa
                     
                 Mutácia. 

        
            
                                    13.4.2010
            o
            17:00
                        |
            Karma článku:
                5.93
            |
            Prečítané 
            1274-krát
                    
         
     
         
             

                 
                    Tak som sa po 35 piatich rokoch dozvedel, že obsahujem zmutovaný gén. Už som za vodou, už ma to netrápi, ale čo keby som nebol?....
                 

                 Predstavte si človeka, žiaka, študenta, na internáte pijúceho alkohol. Nie, nepredstavujte si mňa, ja som bol na intráku stelesnená nevinnosť. Imaginárny študent si dá za pohárik vína, či fľaškové pivo a v momente očervenie. Problém. Pred vychovávateľom svoje pitie, hoc i úplne neškodné, neutají. Hrozí vyhadzov a problémy v škole. Budúcnosť žiaka je neistá.   Alebo si predstavte nesmelého panica na dedinskej zábave snažiaceho sa zbaliť potenciálnu matku svojich detí. Nie, ani tentokrát si nepredstavujte mňa. Niežeby som niekedy nebol nesmelý panic na dedinskej zábave, len sme s partiou nikdy nijako extra neriešili matky detí. Kto by predsa v 20 rokoch života riešil spoločnú budúcnosť s takými čudnými bytosťami ako sú ženy, keď máte futbal.   Náš imaginárny panic však matku svojich detí rieši, len vďaka svojej hanblivosti si netrúfa osloviť ju. Tak si teda chce zvýšiť sebavedomie panákom tvrdého. Ale chyba. Len čo ho vypije, jeho tvár sa zmení na čerstvo dozretú paradajku. Celý očervenie a sebavedomie miesto stúpania klesá. Kým sa farebne znormalizuje, objekt jeho túžby je už 2× štastne rozvedený a v trenčianskej pôrodnici rodí svoje tretie dieťa....Náš nesmelý je stále nesmelý a stále hľadá..... A v čom je teda problém?   Zmutovaný gén   Vypitý alkohol sa v pečeni rozkladá na acetaldehyd, ktorý sa pôsobením ďalšieho enzýmu zmení na kyselinu octovú. Genetici z Yalskej univerzity zistili, že skoro 50 % Aziatov a 5 % Európanov trpí mutáciou, ktorá síce spôsobuje urýchlený rozklad alkoholu, ale vzniknutý acetaldehyd hromadí v tele a ten následne rozširuje krvné kapiláry a dochádza k červenaniu. Zdroj tejto mutácie objavili na miestach, kde sa pred 10 000 rokmi začala pestovať ryža. Mutácia sa šírila veľmi rýchlo vďaka pitiu alkoholu z ryže. Zmeny metabolizmu mali zamedziť konzumentovi vypiť veľké množstvo ryžového vína. ( zdroj:magazín Koktejl , tlačená verzia)   Keď teda niekedy niekde nad krígľom uvidíte niečo červené, nepohoršujte sa. Nemusí to byť hneď opilec. Pokojne to môže byť jeden z tých 5 percentných.   Neviem, či pred 10 000 rokmi táto mutácia fungovala tak, ako to opisujú univerzitní genetici, ale môžem 100 % vyhlásiť, že u mňa tak nefungovala.Na mňa to pôsobilo málinko opačne. Po prvej kvapke alkoholu som bol kompletne červený a keď som sa chcel priblížiť k svojej pôvodnej a prirodzenej farbe, alkoholu som musel vypiť viac,čiže žiadna mutácia mi v pití nebránila. Doteraz som to pripisoval alergii, alebo mojej neskúsenosti v oblasti konzumácie alkoholických nápojov. (Veď ako si organizmus môže zvyknúť na alkohol, keď mu raz za týždeň doprajete pol litra fľaškového piva počas sledovania televíznych novín na našej nasledovanejšiej televízii?  Odpoveď: Nijako, a preto blbne:-)).   Alkoholu sa tak radšej vyhýbam a som pokojný, viem, že to nie je ďalšia z mojich alergií, aj keď som si nie úplne istý či mutácia je lepšia. A ešte k tomu taká, ktorá nefunguje tak, ako má. V tejto súvislosti ma tak napadá...      Žeby tá mutácia časom ešte zmutovala?    A nie som teraz náhodou tak trocha Číňan?   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marian Letko 
                                        
                                            Pravdivý príbeh o tom, ako Maroško Husáka zvesil a znova ho musel zavesiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Letko 
                                        
                                            Ego
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marian Letko 
                                        
                                            Hotel Pieris
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Letko 
                                        
                                            Nórsko – Oslo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marian Letko
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marian Letko
            
         
        marianletko.blog.sme.sk (rss)
         
                                     
     
         Život je nekompromisný, splní nám obavy i sny. Tak pozor na to čo chceme, lebo tu kašu si zjeme. (HzM) Ya Basta 
 
Vytvorte si svoju vizitku 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    174
                
                
                    Celková karma
                    
                                                7.99
                    
                
                
                    Priemerná čítanosť
                    1301
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fikcia
                        
                     
                                     
                        
                            šport
                        
                     
                                     
                        
                            Foto
                        
                     
                                     
                        
                            cestou necestou
                        
                     
                                     
                        
                            stava sa
                        
                     
                                     
                        
                            hlboke uvahy
                        
                     
                                     
                        
                            Maroško
                        
                     
                                     
                        
                            politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            O čom čítam o tom píšem (nekorigované)
                                     
                                                                             
                                            Maďarský pomaranč
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            hudba z marsu
                                     
                                                                             
                                            Ska-p
                                     
                                                                             
                                            plostin punk
                                     
                                                                             
                                            mnaga a zdorp
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            janka
                                     
                                                                             
                                            jarka
                                     
                                                                             
                                            ivka
                                     
                                                                             
                                            evka
                                     
                                                                             
                                            didi
                                     
                                                                             
                                            dominika
                                     
                                                                             
                                            samo
                                     
                                                                             
                                            brano
                                     
                                                                             
                                            zuzka
                                     
                                                                             
                                            listie lesa
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            korektúra blogu
                                     
                                                                             
                                            Nepálčatá
                                     
                                                                             
                                            Korektúry textov
                                     
                                                                             
                                            Moja osobna
                                     
                                                                             
                                            Koktejl
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tesco "Value"
                     
                                                         
                       (ne)Amazonia 7
                     
                                                         
                       Z hradu šup-čľup do čokolády!
                     
                                                         
                       Zaujímajú nás podmienky pracovníkov v textilnom priemysle?
                     
                                                         
                       Moje spomienky na život v ČSSR
                     
                                                         
                       Neklikajte, ak hľadáte utajovanú pravdu o rakovine
                     
                                                         
                       Nórsko – Oslo
                     
                                                         
                       Penty sa nebojím
                     
                                                         
                       Ako Maroško suseda o úrodu zrejme pripravil
                     
                                                         
                       Moji čínski partneri
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




