

 Každú chvíľočku dobehli s prosíkom o niečo, o nejaké vysvetlenie čohosi, o čom sa im dosiaľ ani nesnívalo tam, tam bez mamy. Ona im vysvetľovala, poúčala ich a radila im, čo a ako. 
 Pozorovala ich, ako sa zaujato hrajú a vymýšľajú pestvá. Hovela si spokojná, že sa deje to, čo sa deje, že ich má, že sú jej a ona ich. 
 Vtom sa z diaľky ozvalo škrípanie kolieskových korčúľ na rozteplenom betóne. Znenazdajky sa pri nich pristavila dievčinka. Zastala a pozerala zvedavo na dve malé dievčatká, ktoré sa bavili jedna radosť. Pozerala, pozerala. Aj ona, tiež začala pokukovať na zvedavú korčuliarku. Čo vidí na tých dvoch šintroch? Čo ju tak zaujímajú, že prestala naháňať diaľku cesty?... Dievčinka si  k nej váhavo prisadla. Chvíľku tíško sedela a len očila. Naraz z nej vzrušene vyhŕklo: 
 „Teta, nie je to Danka a Janka?... Musia to byť...“ 
 Čosi jej stislo hrdlo, zamrazilo ju v duši, zakabonila sa, zaváhala s odpoveďou, ale potom sa z nej náhle vysypalo: 
 „Áno, sú... a prečo sa pýtaš?“... čakala na odpoveď celkom zmeravená. 
 „Viete, teta, ja ich poznám. Z Kvetinky, z domova...“ rozrečnila sa  rozšafne slečinka. 
 „Ja som ich neraz uspávala. Mala som ich rada...“ dodávala s istou dávkou pýchy. 
 „Áno?... A ty si  tiež z Kvetinky?...“ rozväzoval sa jej pomaly jazyk. Celkom tak, ako neznámej, vyvedenej z miery, vzrušenej dievčinky. 
 „Áno, teta... som...“ 
 „A to ťa takto samotnú púšťajú až sem, do takej diaľky... z domova?...“ 
 „Áno, môžeme si ísť zakorčuľovať... ale už mám len chvíľočku čas... Musím sa už vrátiť... 
 Budete aj inokedy sem chodiť s Dankou a Jankou?“ vyzvedala s tajnou túžbou a očká  pozabudla na tvári mladej ženy pred sebou. 
 Zaváhala s odpoveďou. Tentoraz zaklame. Predsa musí byť anonymná… i jej deti. Jej deti... 
 „Asi nebudeme... Teraz sme tu len na chvíľočku, vieš... Neviem, či sa ešte stretneme...“ vypovedala váhavo, keď zbadala na tváričke dievčatka tieň, akoby sklamania a zármutku. Mrzela ju lož, ale pocítila ju ako nevyhnutnosť, povinnosť, voči svojim novým ratolestiam. 
 „Ahá... škoda, občas by som sa prišla s nimi pohrať... Tetuška, je fajn, že už majú mamu... Ja 
 ešte nemám...“ smutne dokončievala rozhovor dievčinka. Vstala, hodila závistlivý pohľad na rozvýskané, rozjašené deti. 
 „Tetuška, do videnia!“ slušne pozdravila a vyrazila na korčuliach domov. Domov?... 
 Pozerala dlho za korčuliarkou, ktorá uháňala ostošesť do svojho údelu - bez mamičky. Stislo jej opäť srdce. Nemá mamu... A chcela by... Chcela... Túži po nej... S nehou sa zadívala na drobce pred sebou. Práve obrátili na ňu svoje nádherné nezabúdkové očká. Kontrolovali, či je pri nich: ich mama.... 
   

