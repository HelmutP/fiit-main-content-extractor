
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Soňa Cummings
                                        &gt;
                Také ľudské
                     
                 Stúpol mi tlak 

        
            
                                    3.3.2010
            o
            16:57
                        (upravené
                10.5.2010
                o
                10:09)
                        |
            Karma článku:
                25.24
            |
            Prečítané 
            11286-krát
                    
         
     
         
             

                 
                    Tak ale teraz som sa už skutočne nasrala. Čo to má do pekla znamenať?! Viete, pred pár mesiacmi som sa naozaj pobavila na pokuse o uzákonenie vyspevovania hymny. A teraz otvorím noviny a čo nevidím? Ono to naozaj prešlo! (a neupozorňujte ma prosím na to, že ide "len" o počúvanie) Niekto to naozaj zobral vážne! Čo tým ľuďom už naozaj hrabe?!
                 

                 "Ja si robím, čo chcem.", poviem zvyčajne, keď sa ma niekto pokúša dotlačiť k nejakej reakcii (alebo obecne k niečomu vôbec). Neporušujem zákony, nefajčím a nepijem, dodržujem sľuby, som zdvorilá, milá a poctivá, som skrátka taký ten človek, akých sa dnes už málo nájde - inými slovami naivný hlupák a idealista - a to všetko dobrovoľne!   Ale práve som sa rozhodla, že stačilo. Rozhodla som sa zmeniť sa. Preto oznamujem všetkým šialencom v parlamente a najmä tomu, ktorý si prepil mozog a v normálnej krajine by ho už dávno odvliekli v zvieracej kazajke, že na ten váš zákon zvysoka seriem (tak ako pán oný šťal svojho času z balkóna). Vôbec ma nezaujíma.   V pondelok ráno (a hlavne od apríla) sa akurát tak prevalím v posteli, pustím si A-ha alebo Depeche mode a pôjdem si urobiť na raňajky chleba s kvasnicovou pomazánkou. Zákon vchádza do platnosti prvého apríla, čo som sa rozhodla považovať za nepodarený pokus o vtip a preto ho neberiem vážne.   Týmto zároveň vyzývam všetkých normálnych ľudí k tomu, čo sa nazýva občianska neposlušnosť. A týka sa to najmä tých, ktorí by nám mali púšťať hymnu. Pustite radšej do školského rozhlasu nejakú dobrú muziku, ktorá decká naladí na nový (prekliaty) týždeň v škole aspoň trocha pozitívne. Kto chce počúvať hymnu, nech si ju pustí na Ipode na základe vlastného rozhodnutia. Dajte najavo tým parchantom, že nás do ničoho nútiť nebudú! A že miesto sprostostí by sa radšej mali postarať o to, aby sa v školstve aj hajzlový papier nemusel používať z oboch strán.   Kto je za?       (pozn.aut.: Ospravedlňujem sa za vulgarizmy použité v článku pod vplyvom zvýšeného krvného tlaku.)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (108)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Soňa Cummings 
                                        
                                            Moje pozadie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Soňa Cummings 
                                        
                                            neFrancúzska neMusaka alebo zapekané cestoviny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Soňa Cummings 
                                        
                                            Obrátený jablkový koláč s orechovým korpusom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Soňa Cummings 
                                        
                                            Svet podľa Sone
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Soňa Cummings 
                                        
                                            Zverina
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Soňa Cummings
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Soňa Cummings
            
         
        cummings.blog.sme.sk (rss)
         
                                     
     
        Astronóm amatér, opatrný spisovateľ, pozorovateľ ľudských charakterov a egocentrik. 
A neustále sa čudujem ako Pratchettov Smrť: Čo len tí ľudia nevymyslia?
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1874
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Bože, vyzerám strašne!
                        
                     
                                     
                        
                            Epi
                        
                     
                                     
                        
                            Fotománia
                        
                     
                                     
                        
                            Také ľudské
                        
                     
                                     
                        
                            Mňamky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




