
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Miloslav Smrek
                                        &gt;
                Nezaradené
                     
                 Pán  premiér SR, môžete dobre spávať, 

        
            
                                    3.6.2010
            o
            0:05
                        |
            Karma článku:
                13.06
            |
            Prečítané 
            2080-krát
                    
         
     
         
             

                 
                    ako najvyšší predstaviteľ výkonnej moci štátu, ktorý je podľa Ústavy SR výlučným vlastníkom všetkých vodných tokov na Slovensku a keď v posledných týždňoch tieto rozvodnené vodné toky spôsobujú tisíckam našich spoluobčanov strastiplné dni, bezsenné noci a nenávratné škody, zatiaľ „iba" na majetku ? Keď ste sa zoznamovali z masmédií (napr. za všetky iba jeden článok http://www.sme.sk/c/5403004/voda-vyhnala-ludi-z-domov-ostali-ruiny.html ), alebo keď ste videli všetku tú hrúzu na vlastné oči pri návšteve v obci Hul a ako pomoc postihnutým ste habkavo spomínali akési materiálne prostriedky zo štátnych hmotných rezerv slovenskej armády, namiesto finančných prostriedkov, ktoré ste tak veľkoryso rozdávali na každom výjazdovom rokovaní vlády na Slovenskom vlády a robili si tak nepokryte predvolebnú kampaň ?
                 

                 Iste, máme na Slovensku v poslednom období do činenia so zvýšeným stavom letných povodní, vyvolaných frontálnymi, dlhotrvajúcimi dažďami, zasahujúcimi veľké časti povodia vodných tokov. Súčasne však vieme, ako tieto veci predvídať a predpovedať, ako im predchádzať, resp. za určitých situácií aspoň, ako minimalizovať ich škodlivé prejavy. Vieme, avšak iba pri správnom riadení odborných činností súvisiacich s uvedenými javmi (búrky, povodne). Teda predovšetkým pri systematickom, dlhodobom sledovaní a vyhodnocovaní stavu vôd (najmä ich kvality, prietokov, výšky hladín) a ovzdušia, pri správe vodných tokov, teda o.i. pri zodpovednom plánovaní a včasnej realizácií finančne náročnejších stavieb protipovodňovej ochrany, pri systematickej kontrole stavu vodných tokov, ich plánovanej a pravidelnej údržbe a čistení ich korýt od dnových nánosov, rovnako ako aj pri včasnej realizácii agrotechnických a lesotechnických opatrení proti veternej a vodnej erózií a pri pravidelnej údržbe a prevádzke hydromelioračných zariadení, teda pri čistení zavodňovacích a odvodňovacích kanálov a pri prevádzke a údržbe čerpacích staníc na nich.   O odbornom riadení uvedených činností možno v celom rade prípadov, po nastúpení novej vlády v roku 2006 odôvodnene pochybovať. Určite tiež aj v dôsledku toho sa v súčasnosti sťažuje Slovenský hydrometeorologický ústav, že nemôže pre nedostatok finančných prostriedkov pokračovať v systematickom sledovaní hydrometeorologických javov, že správcovia vodných tokov sú trvale podfinancovaní na výkon svojich povinností pri správe vodných tokov a hydromelioračné kanály sú väčšinou takmer úplne zarastené a zariadenia hydromelioračných čerpacích staníc končia v zberniach starého železa.   Nemožno sa teda diviť, že v dôsledku uvedených skutočností sa prietočná kapacita riek a potokov i hydromelioračných kanálov výrazne zmenšila a pri dlhotrvajúcich dažďoch voda z nich vybrežuje do okolitého územia, zastavaného, či nezastavaného, so všetkými svojími ničivými a škody spôsobujúcimi účinkami. To všetko sa však dá pri odborne vykonávanej správe vecí verejných na všetkých stupňoch riadenia nielen predvídať, ale musí sa tomu aj predchádzať.   Škoda, že tomu tak v poslednej dobe na Slovensku nie je.   Závisí od oprávnených voličov tejto republiky, či sa ten stav zmení k lepšiemu.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Občania druhej kategórie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Aký prívlastok dať štátu, ktorý neprávom  zadržiava peniaze
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Farizejstvo, či úprimný záujem o týrané zvieratá väčší,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Nájde sa v slovenskom parlamente „bohatier boží,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Patrí kontroverzná osobnosť na čelo riešiteľských tímov?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Miloslav Smrek
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Miloslav Smrek
            
         
        jozefmiloslavsmrek.blog.sme.sk (rss)
         
                                     
     
        Som dôchodca, ktorý sa neprestal zaujímať o dianie v našej spoločnosti,o morálne aspekty správy vecí verejných, o osud tohto štátu a jeho obyvateľov z hľadiska možných dôsledkov z politických nominácií na všetky vedúce funkcie v štátnych orgánoch a inštitúciach, obrazne povedané "štátnymi tajomníkmi počínajúc a upratovačkami končiac". Vo voľných chvíľach, ktorých dôchodca nemá nikdy nadostač, skúšam fotografovať a pre vlastné potešenie kompilovať hudobné CD prevážne zo slovenských ľudových piesni a pravoslávnych chrámových zborov.  

moje fotky na SME.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    49
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1134
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




