
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Pavol Dinka: Slovenské masmédiá – metódy manipulácie (recenzia) 

        
            
                                    19.1.2009
            o
            15:30
                        |
            Karma článku:
                10.02
            |
            Prečítané 
            14023-krát
                    
         
     
         
             

                 
                    Naše médiá nás už až tak manipulujú, že už to ani vôbec nedokážu, píše Pavol Dinka v novej knihe
                 

                 
 stv.sk
   Šéfredaktor www.sme.sk Tomáš Bella sa cez víkend na blogu posťažoval, že mu Pavol Dinka v knihe vydanej Politologickým ústavom Matice slovenskej na jeseň minulého roku ukradol zhruba tretinu jeho seminárnej práce z roku 2001, spolu cez 700 slov textu. Ten istý Dinka, ktorý sa v nedávnej diskusnej relácii STV  zamýšľal nad nedostatkom etiky a profesionality slovenských médií. Áno, v tej relácii, kde – inak tiež plagiátor  - E.Chmelár zavádzal o prieskume  o korupcii v slovenských médiách. V tej relácii, v ktorej dominovali žurnalisti kedysi oddaní poslednej Mečiarovej vláde.  Na Dinkovu novú knihu som našiel na internete len jednu recenziu, ak sa to tak vôbec dá nazvať. Vyšla v novembri v týždenníku Záhorák pod názvom Nedajte sa zmanipulovať!  („nová kniha spisovateľa a publicistu Pavla Dinku, rodáka zo Skalice“) a napísal ju Jozef Pálenický. Citujem ho:   
 		Médiá majú u občanov veľkú dôveru, ľudia sa na ne v mnohom spoliehajú, hľadajú u nich pomoc... Nie vždy je to však tak, presviedča nás Pavol Dinka. Médiá niekedy svojou ťarbavosťou a aroganciou dosiahnu pravý opak! Dôvera by mala zaväzovať, lenže nie je všetko zlato, čo sa blyští... Najmä nie v médiách! Autorovi ide  o to, aby médiá svojou silou a mocou neprispievali s čoraz väčším umocnením k vytváraniu a zväčšovaniu skepsy a beznádeje na tomto svete. Usiluje sa o to s úmyslom predostrieť  javy a fakty čo najviac sa približujúce  pravde. 
 
 Okrem štýlu a pátosu spred sto rokov je zaujímavá zhoda takmer celej recenzie (okrem dvoch úvodných odstavcov) s textom prológu samotnej knihy. Dinka v knihe (str. 7-8) píše:  
 	Médiá majú u občanov veľkú dôveru, ľudia sa na ne v mnohom spoliehajú, hľadajú u nich pomoc... Nie vždy je to však tak. Médiá niekedy svojou ťarbavosťou a aroganciou dosiahnu pravý opak! Dôvera by mala zaväzovať, lenže nie je všetko zlato, čo sa blyští... Najmä nie v médiách! .... Mne ako autorovi ide však o to, aby médiá svojou silou a mocou neprispievali s čoraz väčším umocnením k vytváraniu a zväčšovaniu skepsy a beznádeje na tomto svete. Pokúšam sa o to s úmyslom predostrieť  javy a fakty čo najviac sa približujúce  pravde. 
 
 Takže buď napísal recenziu Pálenický tak, že okrem začiatku len skopíroval úvod z Dinkovej knihy a pozmenil priamu reč na nepriamu, alebo....čo sa mi zdá viac pravdepodobné, Pálenický je samotný Dinka. Naznačuje to aj niekoľko starších citátov Pálenického  z internetu. Sú takmer totožné s vetami, ktoré Dinka ponúka vo svojej novej knihe.  Takže knihu som sa prečítal radšej sám. O čom je? 
    
 Dinka vidí súčasné slovenské médiá veľmi podobne ako premiér Fico, hrubo materialisticky. Za Mečiara boli manipulácie, priznáva, ale odvtedy sú ešte horšie – proamerické, prodzurindovské, prokádéhácke, prozápadné, pravičiarske – a navyše motivované cudzími peniazmi. Podľa Dinku žurnalistami manipuluje najmä zahraničie. A to hlavne zahraniční majitelia slovenských médií ako aj zahraničné nadácie, ktoré cez financovanie slovenských nadácií a inštitútov manipulujú cez novinárov domácou verejnou mienkou. Manipulácie mimovládok (vrátane INEKO, kde pracujem) sú pre Dinku obzvlášť nebezpečné - venuje im celých desať strán oproti pol strane o vplyve majiteľov (str.31):  
 	Manipulovať žurnalistami však možno aj veľmi nenápadne, skryto, takmer nebadane. Majstrami v tomto smere sú rozličné združenia, nadácie....ktoré sa tvária ako slobodné a nezávislé. A môžbyť to tak aj bude! Nezávislé od Slovenska, od života slovenských občanov a závislé od peňazí, od mocnej sily neoliberalizmu v celom svete, najmä však od dolárikov plynúcich z USA a ďalších krajín...  
 
 Čo Dinka navrhuje ako obranu pred touto manipuláciou? Manipuláciu mimovládkami s lepšími hodnotami (str.41):  
 	V záujme určitej rovnováhy je nevyhnutné vytvoriť nadácie a združenia v opačnom garde, ktoré by pomáhali, asistovali pri presviedčaní, že len práca všetkých a pre všetkých prinesie spoločnosti prospech a úžitok. Mali by vzniknúť mimovládky, ktoré nebudú proti vládam iba brojiť, ale predovšetkým konštruktívne kritizovať a pri dobrých veciach ochotne podajú pomocnú ruku.  
 
 Hoci Dinka pravidelne ulieta mimo realitu, vo väčšine knihy sa opiera o popis štandardných foriem možnej manipulácie v médiách. Má pravdu, že treba poznať majiteľov médií, ktoré sledujeme, aby sme vedeli minimalizovať možné zavádzanie. Má pravdu, že používanie expertov je u nás príliš laxné a často novinári výroky expertov z mimovládok či firiem nepreverujú pre presnosť, konzistentnosť či logiku. Ale je niečo celkom iné robiť zo žurnalistov mechanické figúrky ovládané komplotom zo zahraničia. Podľa mňa majú antipatie väčšiny žurnalistov k súčasnej vláde základ v ich hodnotách a skúsenostiach, tak ako je to u väčšiny slovenských odborných elít. Prevažná väčšina chýb, ktoré médiá robia majú zdroj v povrchnosti či nedostatočnej odbornej vyspelosti novinárov. Tak to vidia aj samotní novinári v prieskume  SPW spred roka – tlaky majiteľov či editorov sú pre nich problémom až v tretej či štvrtej línii.   Dinka vo väčšine knihy aj teoreticky správne menuje druhy možných manipulácií, ako je nesúlad titulku s obsahom článku, zneužívanie ankiet v televíznych reportážach, problémy s informovaním o prieskumoch verejnej mienky či nedostatok a oneskorenosť opráv. Ale príklady, ktoré uvádza, sú väčšinou slabé. Príkladom za všetky drobné je Dinkove videnie kauzy ministra Jahnátka spred dvoch rokov. Ten pre TREND povedal, že aj slovenské štátne firmy môžu v obchode so zbraňami uplácať, len to treba vedieť „ošetriť.“ Dinka sa diví, prečo média problém nafúkli, veď Jahnátek bol aspoň úprimný (str.197):  
 	Skrátka, Jahnátek doplatil na to, že chcel, aby sa naše firmy správali rovnako ako štátne firmy všade vo svete. Úprimnosť sa však na Slovensku nenosí! 		...Zdá sa, že väčšina našich novinárov nemá šancu atakovať konkrétne činy, a tak sa orientuje s riadnou dávkou arogancie na verbálne útoky.  
 
 Ako keby slová ministrov neboli vhodné na kritiku, ako keby používanie korupcie bolo v poriadku. Veru, na mediálnu kritiku Jahnátka museli byť tí naši žurnalisti nesmierne dobre zorganizovaní!  Dinkovou knihou sa navyše vezie silný paradox. V úvode, keď zdôvodňuje význam svojej knihy, píše (str.15 a 27):   
 	...verejná mienka je...v ostatnom čase až priveľmi závislá od médií... „[médiá] sú spolu s ekonomikou jedným z rozhodujúcich ovplyvňovateľov politických smerovaní v Slovenskej republike. Vo veľkej miere rozhodujú o tom, z koho urobia hrdinu, spasiteľa Slovenska, a koho znosia pod čiernu zem. Na Slovensku je to jednoducho tak! 
 
 No v závere už vidí Slovensko inak, zabúdajúc na svoje vlastné slová. Cituje vysokú podporu Ficovej vlády v prieskumoch aj po dvoch rokoch jej vládnutia a tvrdí (str.291):  
 	O masmédiách sa všeobecne tvrdí, že výrazne ovplyvňujú verejnú mienku, s tými našimi je to zrejme inak. Obyčajný sedliacky rozum má na to jednoduché vysvetlenie – občania sa pravdepodobne už prejedli ich „jedovatej“ potravy, odhalili priehľadné pokusy o manipuláciu...Ukazuje sa, že naši občania si aj napriek masmediálnym „ohlupovačkám“ dokážu vytvoriť vlastný názor, netrpia stádovitosťou, ako sa im to usilujú nahovoriť mnohí žurnalisti. Vedia rozlíšiť realitu a klamstvo, nenaletia na prázdne slovné žonglérstvo! A za to im patrí na vysvedčení jednotka. 
 
 Tu sa navyše pekne ukazuje Dinkove zmýšľanie. Verejnosť chváli priamo za to, že napriek mediálnej kritike Fica, vykazujú vláde vysokú podporu. Autor teda jasne vie, že vláda hovorí pravdu a robí pre Slovensko dobré veci.  
 Čo je, samozrejme v poriadku, keďže Pavol Dinka bol koncom minulého roka zvolený parlamentom do licenčnej rady na kontrolu objektivity a etiky slovenských elektronických médií. Mandát mu vyprší  v úvode roka 2015.  
  PS Dinka viackrát cituje – neutrálne až pozitívne - aj prácu SPW.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




