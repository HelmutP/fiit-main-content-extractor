

 
 
 Obrovským problémom Petržalky je parkovanie. V súčasnosti na 60 000 áut pripadá len 20 000 parkovacích miest. A preto som bol neskutočne šťastný, keď sa mi po rokoch sledovania inzerátov podarilo pred ôsmimi rokmi kúpiť garáž pod panelákovou terasou kúsok od môjho domu. 
 Prešlo niekoľko rokov garážovej idyly a na oblohe môjho žiarivého parkovacieho šťastia sa objavil temný ponurý mrak. Do vchodu nad mojou garážou, ktorá je tesne pri schodoch na terasu, sa nasťahoval jeden taxikár a začal mi parkovať so zaradenou rýchlosťou pravidelne pred mojou garážovou bránou. Bolo mu zaťažko zaparkovať na 50 m vzdialenom parkovidku. Volával som na číslo dispečingu, uvedené na taxíku a nechával mu poslať odkaz, aby si auto láskavo preparkoval, že sa nemôžem dostať von alebo dovnútra, ale bez akejkoľvek odozvy. Podobne nereagoval ani na lístky s výzvou („Neparkovať pred garážou"), ktoré som mu strkal pod stierač. Keď som si ho raz úplne zúfalý pred zablokovanou garážou vyčkal a slušne ho požiadal, aby s tým už konečne prestal, takmer som dostal na búdku (meral asi dva metre a jeho vyholená hlava plavne prechádzala do hrude bez náznaku zátylku) s upozornením, že ak na aute nájde čo len škrabanec, tak si ma riadne podá a bude si naďalej parkovať, kde chce. A tak som pochopil, že ho musím na dobré susedské vzťahy a potrebu dodržiavania princípov slušného spolunažívania upozorniť spôsobom, ktorý si naozaj zapamätá. 

 Niektorí blokovaní v takých prípadoch obehnú auto s ostrým predmetom v ruke, druhí vypúšťajú pneumatiky, tretí ich prepichujú... Čím väčšie je poškodenie auta, tým bližšie je konanie blokovaných k tým arogantným blokujúcim. A ja som teda taký skutočnš byť nechcel. Vzall som teda rozum do hrsti, chvíľku popremýšľal a následne použil skutočne profesionálne a originálne riešenie s využitím lokálnych ľahko prístupných zdrojov, ktoré malo neuveriteľne dlhodobý pamäťový účinok na parkujúceho. 
 Dvoch vecí je v Petržalke naozaj dosť. Poletujúcich igelitových vreciek a polihujúcich produktov psieho metabolizmu (teda okrem tých, ktoré si ľudia na topánkach nedobrovoľne nosia domov). Na druhý deň, keď som mal garáž opäť zablokovanú, som vzal jedno igelitové vrecko bez pána, natiahol si ho na ruku a najbližšie mäkučké psie oné som mu natlačil pod kľučky taxíka. Neviete si ani len predstaviť, ako to strašne smrdelo. No byť zákazníkom, do takého auta ani len nevstúpim a zďaleko ho obídem, nie to aby som ešte taxikárovi dával nejaké sprepitné. Natlačil som to poriadne hlboko a dôkladne rozmazal do všetkých neprístupných miest, aby to umývačka na pumpe nedokázala vyčistiť a taxikár si to musel dôkladne vyškrabovať ručne prštekom do posledného páchnuceho drobčeka. A pod stierač som mu zasunul lístok s odkazom: „A nabudúce mi nezabudni pred garážou zaparkovať znova". 
 
 
 Nuž - nebolo to z mojej strany práve najslušnejšie upozornenie, ale s niektorými povahami inak nepohneš. Nikdy viac mi už pred garážou nezaparkoval. 

 P.S.    Keďže som bol vyradený z titulky SME, k mojim článkom sa tak dostanú iba tí, čo ma majú v RSS čítačke alebo ktorým na mňa pošlú link ich známi. Takže ak sa Vám moje príbehy páčia, neváhajte  dať link na ne aj Vašim priateľom. 
 
 
   

   
   

