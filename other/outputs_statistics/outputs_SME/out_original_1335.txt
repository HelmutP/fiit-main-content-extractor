
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Šuraba
                                        &gt;
                Nezaradené
                     
                 Veľkonočné tradície 

        
            
                                    10.4.2009
            o
            11:54
                        |
            Karma článku:
                3.36
            |
            Prečítané 
            4602-krát
                    
         
     
         
             

                 
                    Konečne sa čierne stromy začínajú farbiť dočervena. Jar má mnoho podôb, mnoho krás. Počas jari cítim vzduch iskriť a všetko okolo mňa krásne vonia. A keď si na jar spomeniem, tak počas tohto magického obdobia mám najviac pozitívnych zážitkov. S jarou súvisia aj sviatky jari. Kto by ich nepoznal? Ja mám na Veľkej noci najradšej tú pestrosť. A pestrosť je aj tradíciami, aké nám ponúka.
                 

                 
  
       Veľká noc patrí k najväčším cirkevným sviatkom, ale takisto to nie je iba sviatok kresťanstva. Napr. Židia oslavujú vykúpenie z otroctva. A čo pohania a naši predkovia? Slovania sa tešili týmto sviatkom, pretože oslavovali príchod jari. (A dodnes sa tešia.)       Medzi najznámejšie udalosti príchodu jari patrí topenie Moreny. Keďže bohyňa bola neraz označovaná za krutú, silnú, chladnú, necitlivú, ľudia sa tešili, že jej obdobie konečne skončilo. Topenie Moreny malo zaujímavý rituál. „V některých krajích se jednalo o malou loutku, jinde se velikostí blížila lidské postavě. Dělala se obvykle tak, že se na dřevěnou hůl obalenou slámou navlékly dívčí šaty. Zdobila se pentlemi a na krk se jí věšely „korále" z bílých vyfouknutých vajec a prázdných šnečích ulit (symbolů smrti). Průvod vynesl figurínu - symbol smrti, nemoci, bídy a všeho zlého - za vesnici a hodil ji do vody nebo svrhnul ze skály. Původně se snad i kamenovala a pálila." (Červené svátky aneb jak slavili staří Slované jarní rovnodennost, 2008)       Jedným z ďalších obradov bola Živá voda. Povráva sa, že ľudia sa umývali rosou, alebo utekali umyť sa k potoku. Malo to ochrannú funkciu pred chorobami. Zrejme počas Veľkej noci mal potok liečivé, ba až hojivé účinky. Vraj najzdravšie sa mal ten, kto sa umyl ešte počas tmavej noci.       Veľmi významnou udalosťou boli vetvičky vŕby. Vraví sa, že ľudia si odtrhovali vetvičky a nechali si ich pokropiť svätenou vodou. Počas Bielej soboty sa zapichovali vetvičky do polí a tento obrad slúžil ako posvätná ochrana úrody.       Ďalším známym rituálom je šibanie a polievanie dievčat. Korene Veľkonočného pondelku by sme mohli hľadať v 14.storočí. No šibať devy snáď smeli iba tí šarvanci, ktorí si vedeli upliesť korbáč sami. Korbáč mal byť vyrobený z vŕby. Takisto sa povráva, že žena, ktorá nevedela vstať zavčasu, bola poliata vodou. Symbolom toho sviatku je to, aby dievčice boli navždy mladé a krásne. Tento zvyk poznáme dodnes, je najznámejší. Hoci po pravde, niekedy mám pocit, že je v ňom viac chlapskej retardácie ako rituálu.       Slovania si nevedeli predstaviť obrad akéhokoľvek sviatku bez ohňa. Počas sviatkov jari existoval Veľkonočný oheň. Bol to symbol ochrany pred požiarom, povodňou a všakovakými inými pohromami. Neskôr sa ohňu hovorilo „Judášov oheň"-       Teraz vám čosi napíšem o neslovanských tradíciách.       Neviem si predstaviť Veľkú noc bez veľkonočných vajec. Ide o prvú veľkonočnú tradíciu, ktorú založili podľa všetkého starí Egypťania. Veľkonočné vajíčka boli darom pre pevcov kolied. Maľovali sa 7. farbami a na stránke http://www.velikonoce.org/velikonocni-tradice/ sa dočítate, čo všetko slúžilo k výrobe kraslíc.       Známy je aj veľkonočný baranček. Baránok je známy ako symbol pastiera božieho stáda. Od obdobia stredoveku je známy ako jedlo na obradnej hostine. Keďže baranina bola veľmi vzácna, ľudia sa častejšie začali kŕmiť pečivom s tvarom barana.       A čo veľkonočný zajac? Pôvod má v Nemecku. Niektoré jazyky šepkajú, že zajac sa vyskytoval dosť často ľudských obydlí a hľadal potravu. Ďalšie vysvetlenie hovorí, že ľudia často piekli chlieb v tvare zajaca a dávali uprostred vajíčka.       Tak Vám prajem krásne sviatky jari!       Bibliografia:       &lt;http://www.velikonoce.org/velikonocni-tradice/&gt; [cit.2009-04-10]   &lt;http://www.e-stredovek.cz/view.php?nazevclanku=cervene-svatkybraneb-jak-slavili-stari-slovane-jarni-rovnodennost&amp;cisloclanku=2008030003&gt; [cit.2009-04-10]       Zdroj obrázkov:   &lt;http://www.hka.cz/_KULTURA/_tech_k/img/2005/velikonoce.jpg&gt; [cit.2009-04-09]   &lt;http://www.vysoke-myto.cz/portal/images/stories/velikonoce1.jpg&gt; [cit.2009-04-04]     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            O Košiciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Návrat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Železničná krčma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Pierre Picaud
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Veľmi, veľmi málo o rytieroch okrúhleho stola
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Šuraba
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Šuraba
            
         
        suraba.blog.sme.sk (rss)
         
                        VIP
                             
     
        


 
geovisite


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    295
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1671
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Malý princ
                        
                     
                                     
                        
                            Českí herci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sestra
                                     
                                                                             
                                            Najkrajšia milovníčka mačiek
                                     
                                                                             
                                            Moja zlatá Šárka
                                     
                                                                             
                                            Spolužiak-knihovník
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Veď kliknite:-)
                                     
                                                                             
                                            Blog nielen o bezpečnosti IT
                                     
                                                                             
                                            Kúpte si skriptá
                                     
                                                                             
                                            Jedna šikovná archivárka
                                     
                                                                             
                                            Nevinné duše troch kamarátov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sila / Slabosť (?)
                     
                                                         
                       Giethoorn - holandským mestečkom na člne
                     
                                                         
                       Základy Poa
                     
                                                         
                       Električkové zízanie
                     
                                                         
                       Víkend v Derbyshire: Chatsworth House
                     
                                                         
                       Krátko o ženách a meškaní
                     
                                                         
                       Gionatanove farby
                     
                                                         
                       Divadlo na jednotku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




