
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Petržlen či paštrnák? (najlepšie obidva) 

        
            
                                    12.5.2010
            o
            9:05
                        (upravené
                12.5.2010
                o
                9:18)
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            9669-krát
                    
         
     
         
             

                 
                    Rozoznáte ich? Dávate možnosť rozvinúť svoju lahodnú chuť a nádhernú vôňu aj paštrnáku?
                 

                 
  
   Petržlen má všestranné využitie a môžeme skonštatovať, že ide o "dva v jednom". Táto  skvelá rastlina nám  ponúka nezameniteľnú vňať a koreň. Pochádza zo Stredomoria.   Petrželenová vňať je liečivka, ktorá je v kuchyni používaná už odpradávna. Okrem svojho vysoko dekoratívneho účelu má aj nutričný význam, predovšetkým pre  vysoký obsah vitamínu C, B, vápniku, horčíku , železa. Je tradičným diuretickým prostriedkom, čo znamená, že je močopudná a tak pomáha riešiť obličkové problémy. Podporuje trávenie. Vzácna je obsahom  chlorofylu, ktorý môžeme využiť aj na svieži dych, stačí požuvať pár lístkov. Pristavím sa ešte pri dekoratívnom účele, ktorý ocenia tí, ktorí sa radi vyhrajú (ako ja) s úpravou podávaného jedla. Najdekoratívnejší je kučeravý petržlen, ktorý je ozdobou každej studenej misy...   Silica z petržlenu bola izolovaná už v 16 storočí. Je nepostrádateľná pre potravinársky priemysel. Už naše babičky, studnice vzácnych skúseností a vedomostí,  poznali silu vňate petržlenu v využívali ju aj na kozmetické účely. Ak chcete mať pleť bez pieh a pigmentových škvŕn vyskúšajte:   viazaničku vňate dáme variť do jedného litra vody, povaríme tridsať minút, vývar zostane tmavožltý. Najlepší účinok dosiahneme, ak si budeme postihnuté miesta oplachovať 3x denne. Závisí od každého jedinca, ale približne po 30 dňoch by mali byť pigmentové škvrny odstránené. Silný odvar pôsobí blahodarne aj na vlasy, dodá im lesk, posilní ich.   Použitie v ľudovom liečiteľske, v homeopatii, v farmaceutickom priemysle je nesporné. Ak chceme posilniť imunitu môžeme pristúpiť aj k odšťavovaniu. Kvalitný prístroj to zvládne.   Koreň petržlenu je nepostrádateľnou zložkou našej kuchyne, bez nej by nechutil žiadny mäsový vývar,  zeleninová polievka, omáčka na sviečkovú a podobne.   V lete som opatrovala niekoľko dní synovca a neter. Tomáško sa často a rád motal po kuchyni. Dokonca mi navrhol, aby sme si raz uvarili špagety s petrželnovou vňatou. Vlastnou rúčkou mi napísal aj recept. Verím, že sa nenahnevá, ak ho poskytnem aj vám:   Tomáškove špagety   Na kvalitnom olivovom oleji osmahneme vňaťku (krátko a nešetríme množstvom), pridáme cesnak, čili papričku a vegetu. Vmiešame do špagiet a ochutíme parmezánom. Mňam!       *******           *********         ********            *********           *******                 ********   O čosi menej známy a stále podceňovaný je rodný brat petrželnu - paštrnák. Málokto vie, že jeho výživová hodnota je vyššia ako u petržlenu. Ide taktiež o žltkastý koreň s krásnou, šťavnatou, jemne nasladlou dužinou. Je mrazuvzdorný, tak si ho môžeme zberať aj v zime, podľa potreby. Samozrejme, že nemusíte mať záhradu, aby ste sa k nemu dostali, už sa zjavuje aj na pultoch predajní.   Má vysoký obsah minerálnych látok, éterických olejov (pôsobia na trávenie a nervový systém), nezanedbateľný je obsah vitamínu C a vitamínov skupiny B. Môžeme ho konzumovať aj čerstvý, napríklad do šalátu. Je vhodné  postrúhať ho nahrubo a pridať k ostatnej zelenine. Môžeme ho smažiť, dusiť, mixovať, piecť. Ja ho rada pridávam k pečeným zemiakom,  tak že ho rozložím na plech pomedzi zemiaky.  Raz som videla taký recept v programe J. Oliviera a zaujal ma. Výborný je podusený na masle s pridaním aj iných druhov zeleniny, podľa chuti. Samotné chute sú najväčším zdrojom našej fantázie... Pustite si teda fantáziu "na špacír". Dobrú chuť!   Kedysi sa paštrnák sušil, mlel na múku a piekol sa z neho chlieb medovníkovej chuti. Vraj sa dá aj vykvasiť na víno.   Som presvedčená, že tieto "albínske mrkvy" nájdu svoje miesto aj vo vašom jedálnom lístku. Vašu pozornosť si zaslúžia.       Foto: zdroj internet         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (104)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




