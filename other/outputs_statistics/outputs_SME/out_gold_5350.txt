

 Mňa viac zaujala informácia, že slovenskú delegáciu viedla pani ministerka a že  mala na kongrese aj vystúpenie. Na tom by nebolo nič zvláštne, keďže na takomto poste by mohol byť človek, ktorý sa rozumie právu a má čo povedať. 
 Tak som bola zvedavá čo pani ministerka pred celým zhromaždením OSN predniesla. Nebolo zložité na stránkach OSN nájsť text slovenského príspevku a aj priamy link na video z príspevku pani ministerky. 
 A nakoniec, z toho bolo celkom zábavné video. Moderátor kongresu uvádza so slovami „ a teraz ďalší prednášajúci, Jej Excelencia Viera Petríková...." a miesto pani ministerky prichádza pán v obleku, ktorý sa nepredstaví, nevysvetlí čo sa stalo s pani ministerkou. So sklopenou hlavou, bez žiadneho kontaktu s poslucháčmi prečíta jednu stranu textu v angličtine s veľmi silným prízvukom. 
 Video z kongresu slovenského príspevku je tu.(odporúčam posunúť na 0:42:26) 
 Prepis textu je tu. 
 Obsahom tohto príspevku sú veľmi všeobecné prehlásenia, že Slovenská republika plní ciele stanovené na predchádzajúcom stretnutí v Bankoku a že krajina vykonala významné kroky v rámci prevencie kriminality. 
 Vystúpenie ministerky Petríkovej sa malo uskutočniť v rámci záverečnej časti kongresu, kedy ako uvádza Harabin, sa začalo stretnutie na „najvyššej úrovni". Išlo už o záverečnú časť kongresu, kedy predstavitelia jednotlivých štátov mali možnosť na 7 minútové vystúpenie. Podľa oficiálneho programu už  pravdebodobne neriešili priamo odborné veci, ale išlo skôr o spoločenské stretnutia politikov. 
 Keby tento čas pani ministerka a predseda Najvyššieho súdu využili na odbornú debatu s ostatnými účastníkmi, aby sa inšpirovali a priniesli do slovenského právneho systému niečo nové a užitočné...to by bola veľmi idealistická predstava. 
 Reálnejšie je, že znovu, tak ako tisíc krát predtým si  možno niekto spravil výlet za štátne peniaze....ale veď Salvador je pekné mesto:) 
   
   

