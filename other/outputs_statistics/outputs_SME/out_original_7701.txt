
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Šálek
                                        &gt;
                Politika
                     
                 Incident v Stredomorí 

        
            
                                    31.5.2010
            o
            21:29
                        (upravené
                31.5.2010
                o
                22:21)
                        |
            Karma článku:
                6.23
            |
            Prečítané 
            732-krát
                    
         
     
         
             

                 
                    Incident so smrteľne zranenými aktivistami medzinárodnú reputáciu Izraela a jeho vlády rozhodne nevylepší. Otázka je, kde sa vlastne stala chyba?
                 

                 Od rána sa z rôznych médii šírili správy o izraelskej razii na flotilu šiestich lodí vezúcich materiálnu pomoc do Gazy. Správy o počte obetí sa po celý deň líšili. Najprv sa spomínalo "niekoľko", potom dokonca 20. Večer BBC referovala o desiatich obetiach. Podľa tvrdenia izraelských úradov námorné sily vyzvali flotilu šiestich lodí, aby zmenili kurz a mohli skontrolovať, či neprevážajú zbrane. Izrael má totiž už skúsenosti s dovozom zbraní pre palestínske radikálne skupiny práve cez Stredozemné more. Keďže lode flotily výzvu neuposlúchli, podnikli izraelské sily výsadok. Tu treba zdôrazniť, že streľba sa odohrala len na jednej zo šiestich lodí. A zábery poskytnuté izraelskými silami a čiastočne aj zábery urobené tureckým filmovým štábom na palube dokazujú, že vojaci boli napadnutí.       Hádam nikto nemôže čakať od izraelských vojakov, že sa pri plnení úlohy nechajú mlátiť a naháňať s nožmi. Sú to normálni chalani, ale majú dôkladný výcvik a vedia, kedy sa majú brániť. Zatiaľ nie je známe, ako došlo k smrteľným zraneniam. Či v rámci chaosu alebo v otvorenom boji. Netreba tež zabúdať, že konvoje do Gazy neorganizujú iba idealistickí pacifisti, ale často sa na nich podielajú aj radikálni islamisti. Tejto plavby sa údajne zúčastnil napríklad šéf Islamského hnutia v Izraeli Raed Salah. A taktiež sa napojenie na extrémistov spomínalo aj v prípade tureckej charitatívnej organizácie IHH, ktorá flotilu organizovala.       Čo k tomu dodať? Tragédia sa už stala a nič to nezmení. Poškodia sa hlavne už tak dosť naštrbené vzťahy Izraela a Turecka, čo je na škodu pre Európu aj Ameriku. Po incidente v Dubaji prestíž Izraela a vlády premiéra Netanyahua na medzinárodnej scéne opäť raz utrpí. Dôležité ale je, aby sa organizátori humanitárnych akcii v prospech obyvateľov Gazy nepokúšali hrať na "hrdinov" a aby s Izraelom spolupracovali. V opačnom prípade sa môže stať práve niečo podobné. Niekedy totiž podobné konvoje majú skôr ako pomôcť Palestínčanom zvýšiť prestíž Hamasu. Teda hnutia, ktoré je svojou militantnou politikou a zadržiavaním Gilada Shalita zodpovedné za situáciu, ktorá v Gaze momentálne je. Špeciálne priateľské k Hamasu boli napríklad konvoje Viva Palestina. Nebolo by dobré, keby profitoval aj z tejto nepríjemnosti. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Egypt a prevrat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Arabský komunista a Pražská jar
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Jichak Šamir
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Egypt po voľbách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Situácia v Sýrii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Šálek
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Šálek
            
         
        salek.blog.sme.sk (rss)
         
                                     
     
        Politológ so záujmom o dejiny, subkultúry a iné veci, ktoré človeka ťažko uživia.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    44
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    878
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Všeobecne
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Marek Čejka- Izrael a Palestina
                                     
                                                                             
                                            Irvine Welsh- Lepidlo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zdenek Muller
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




