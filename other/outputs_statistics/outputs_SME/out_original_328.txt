
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lubomir Tuchscher
                                        &gt;
                Film
                     
                 Na čo (ne)ísť do kina 06/12/07 

        
            
                                    6.12.2007
            o
            17:07
                        |
            Karma článku:
                7.28
            |
            Prečítané 
            7665-krát
                    
         
     
         
             

                 
                    Môj pravidelný blog s dojmami z premiérových filmov v slovenských kinách dnes mešká. Dôvodom bola moja túžba vidieť nový slovenský film Polčas rozpadu. Na Medzinárodnom filmovom festivale Bratislava bolo jediné predstavenie pre platiacich divákov vypredané viac ako 24 hodín vopred. Film som videl až na prvom regulárnom, platenom predstavení dnes o 11.40. (V kinosále sme boli traja!) Okrem dojmov z neho si môžete prečítať, čo si myslím o ďalších dnešných premiérach - príbehu Jane Austen Vášeň a cit a animáku Pán včielka.
                 

                   Polčas rozpadu (Polčas rozpadu) - slovenské kiná roky čakajú na film, ktorý by dokázal osloviť masy. Filmoví kritici a recenzenti (medzi nich sa neradím) zasa čakajú na niečo také, čo by mohli mohli odporučiť a za čo by si spravili čiarku, že na neho "dostali divákov". Nalejme si čistého vína - rád by som sa mýlil, ale obávam sa, že tieto očakávania sa nenaplnia ani Polčasom rozpadu. Pritom v porovnaní so súčasnou českou tvorbou (napr. Účastníci zájazdu alebo Medvídek) je táto snímka serióznym partnerom. Na rozdiel od iných slovenských filmov z uplynulého obdobia som Polčasu rozpadu uveril. Obávam sa, že ľudia s príjmom menším ako priemer v národnom hospodárstve s ním budú mať problém. Venuje sa len vyššej strednej triede, respektíve horným desiatim tisícom.     Uveriť hercom je vec, ktorá mi chýba aj pri Ordinácii v Ružovej záhrade. Hlavnou devízou Polčasu rozpadu je podľa mňa scenár, dialógy sú presne také, ako keby pán Klimáček prepísal reálne rozhovory reálnych ľudí. Keby film prekonal aspoň istú magickú úroveň v návštevnosti, určite by viaceré repliky zľudoveli (tak ako to dnes už platí o niektorých hláškach z jeho hry Modelky). Na rozdiel od viac či menej absurdných postáv z divadla GUnaGU, ktorého Klimáček je dvorným autorom, však postavy v tomto filme stáli pevne pri zemi. Každá informácia vo filme malo svoje miesto a význam, tak ako to pri Klimáčkových hrách býva. Škoda len, že predstavenie postáv a vytvorenie zápletky trvalo skoro hodinu. Vyvrcholenie, záver a "pointa" však boli dávkované ako v správnom diváckom filme.     OK, Polčas rozpadu nie je dokonalý film. Pre väčšinu Slovákov môže byť absurdná výzva postavy Diany Mórovej k českej manželke Janka Krónera, aby začala pripravovať a moderovať vlastnú televíznu reláciu, alebo "catering s čašníkom a speváčkou na oslave narodenín na chate, ktorej sa zúčastňuje deväť blízkych ľudí a varia si vlastný guláš". Ale ani Hitchcock nebol dokonalý. Počas rozpadu ma oslovil. Ak sa rozhodnete Polčas rozpadu vidieť a nebudete v ňom a priori hľadať chyby, nebude to úplne stratený čas. Hodnotenie: palec mierne hore      O filme nie je spomienka na IMDB, vo filmografii Tatiany Pauhofovej na IMDB  sa neuvádza.      

            Vášeň a cit (Becoming Jane) - k tomuto filmu som pristupoval s istým odstupom. Knihy Jane Austen ma neoslovujú, nepatrím k jej cieľovej skupine. Ako platiaci divák som však videl spracovanie Austenovej Rozumu a citu, ako aj Emmu. Obe som prežil bez ujmy. Ťažšie som však znášal Pýchu a predsudok s Keirou Knightley. Pri pozeraní tohto DVD som zaspal... Na tento film si však sťažovali moje kamarátky, fanynky Colina Firtha a spracovania Pýchy a predsudku z dielne BBC, takže problém nebol asi vo mne. Odstup voči Vášni a citu (slovenský názov vybraný špeciálne pre slovenské diváčky, aby vopred vedeli, o čo ide) som však mal aj kvôli sklamaniu, ktoré mi nedávno priniesol príbeh inej britskej autorky Miss Potter.     Musím však konštatovať, že tento nový film ma oslovil. Druhýkrát by som si ho už asi nepozrel, ale neboli to zbytočne zabité dve hodiny. Dĺžka mi absolútne neprekážala, príbeh a dialógy boli veľmi príjemne vyskladané. Pozitívne hodnotím výbeh hercov a herecké výkony: dámy britského filmového neba - Julie Walters (Billy Elliot), Maggie Smith (Izba s výhliadkou) alebo páni James Cromwell (Kráľovná) s Jamesom McAvoyom (Posledný škótsky kráľ). V neposlednom rade treba spomenúť Anne Hathaway. Tejto slečne som pri pozeraní odpustil tak americký pôvod (áno, ďalšia Američanka ktorá "hrá britský akcent") aj filmy jej "hereckého detstva" ako napríklad Denník Princeznej. Hathaway som uveril a to je (ako som spomenul vyššie) asi to najdôležitejšie. Suma sumárum - odkaz všetkým mužom: ak si vaša drahá zmyslí, že chce ísť na Vášeň a cit, nemusí to byť až taký zlý večer. Pre dámy, romantičky povinné čítanie. Hodnotenie: palec hore                 Pán včielka (Bee Movie) - poviem to priamo, od tohto animáku som čakal viac. Ale asi tak to býva pri filmoch, na ktoré distribučná spoločnosť musí lákať odkazom "animák od tvorcov Shreka  a Madagascar". Otvorene hovorím - nie je to úplný prepadák, obsahuje množstvo dobrých nápadov, ale žiaľ skĺza k mnohým klišé. "Posolstvo" filmu o tom, ako je dôležitá práca každého jedného člena pracovnej skupiny, je až také hlasité, že pravdepodobne po vydaní DVD najväčší podiel medzi kupujúcimi budú tvoriť špecialisti na ľudské zdroje. Ak zasadím túto novinku medzi animáky tohto roka, nachádza sa kdesi v priemere. Na Ratatoille  alebo Divoké vlny  nemá, ale určite je lepší ako Tajomstvo Robinsonovcov  alebo Pasca na žraloka. Ak Vás teda budú deti ťahať, aby ste na tento film išli, treba povedať, že Pána včielku prežijete. Má totiž v sebe množstvo vtipov určených aj pre dospelých divákov.  Varovanie: Pán včielka prichádza do slovenských kín s českým! dabingom, čo by sa pri animovaných filmoch nemalo stávať. (Osobne si gratulujem, lebo sa mi v celom texte podarilo nespomenúť Včielku Maju). Hodnotenie: priemer so silnými ale aj slabými momentami             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubomir Tuchscher 
                                        
                                            List bratovi Michalovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubomir Tuchscher 
                                        
                                            Ústav pamäti národa: Bolo ich deväť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubomir Tuchscher 
                                        
                                            Vedeli ste, že za plyn platíme menej ako pred tromi rokmi?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubomir Tuchscher 
                                        
                                            Kto by nechcel lacnejší plyn?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubomir Tuchscher 
                                        
                                            Na čo (ne)ísť do kina 10/04/08
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lubomir Tuchscher
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lubomir Tuchscher
            
         
        tuchscher.blog.sme.sk (rss)
         
                        VIP
                             
     
        Zberateľ skúseností.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    150
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3279
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Otvorené oči
                        
                     
                                     
                        
                            O plyne
                        
                     
                                     
                        
                            Zdielanie autorských práv
                        
                     
                                     
                        
                            Film
                        
                     
                                     
                        
                            TV
                        
                     
                                     
                        
                            Médiá
                        
                     
                                     
                        
                            Všetci sme cirkev
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            LAST.FM
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            DVD za facku
                                     
                                                                             
                                            Dobri ludia - stále žijú, dajte o nich vedieť!
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List bratovi Michalovi
                     
                                                         
                       Nehaňte biskupov mojich!
                     
                                                         
                       Ako to bolo s tým portálom cestovného ruchu
                     
                                                         
                       Keď nie ste v televízii, akoby ste ani neboli
                     
                                                         
                       VÚB, a.s.
                     
                                                         
                       Ako vyliečiť eurofondy.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




