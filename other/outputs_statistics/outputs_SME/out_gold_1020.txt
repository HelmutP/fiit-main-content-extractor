

             Poľský národný vrch Giewont (1 894,5 m n.m.) síce nepatrí medzi najvyššie
vrcholky Tatier, ale napriek tomu je jedným z najvýznamnejších
a najnavštevovanejších vrcholov poľskej strany pohoria. Jeho vápencové strminy
a 600 metrov vysoká severná strana tvoria typickú siluetu možnú vidieť zo
Zakopaného. 
 
 
            Na počiatku 20. storočia sa
rozhodli veriaci zo Zakopaného podľa talianskej módy vybudovať kríž na
niektorom tatranskom vrchole. Vybrali si Giewont a 27.9.1900 tu správca
zakopanskej fary Kazimierz Kaszelewski vysvätil veľký drevený kríž. Ten však náhle
zničila víchrica. Dnes na vrchole stojí 15 metrov vysoký oceľový
kríž, ktorý zhotovila železiareň Józefa Góreckého z Krakowa. 17,5 metru vysoká konštrukcia
(2,5 m
je v skale) sa skladá z 400 častí a váží 1800 kg. Na vrchol Giewontu
ju so 400 kg
cementu a 200 panvicami vody vynieslo 3.7.1901 päťsto veriacich z Hali
Kondratowe. Dňa 19.8.1901 kríž vysvätil lvovský biskup Władysław Bandurski.
 
 
V Tatrách podľa polskej legendy spia rytieri,
ktorí prídu v núdzi pomáhať národu. Nejvätší z nich zkamenel práve v
Giewonte. Leží na chrbte s hlavou obrátenou na západ. V skrížených rukách
drží na prsiach štít. Keď sa v Polsku dejú zlé veci, rytier sa pokúša vstať a
bojovať za pravdu. Naposledy se vraj taký pokus odohral roku 1981, keď bol v
Polsku vyhlásený výnimočný stav. Skaly a balvany, ktoré vtedy padali z
Giewontu, boli dôkazom toho, že rytier sa pohnul.
 
 
            Na vrchol Giewontu vedie niekoľko
výstupových trás. Najkratšia (1,5 hodiny) vedie po modrej turistickej značke z Hali Kondratowe.
Tu, priamo pod skalnými zrázmy Giewontu, sa nachádza turistická chata s
kapacitou 28 lôžok. Na mieste dvoch jej predchodkýň z rokov 1910 a 1933 ju v rokoch
1947-48 postavilo Poľské tovaryšstvo tatranské podľa projektu B. Laszczka. Prvým
chatárom bol známy predvojnový lyžiar-olympionik Stanisław Skupień. Chata stojí v nadmorskej výške 1 333 m n. m. a v zime je
ohrožovaná mohutnými lavínami. Ani v lete nie je úplne v bezpečí. Dňa 29. mája
1953 spadla zo zrázu Giewontu mohutná kamenná lavína. Jeden balvan vážiaci približne
30 ton prerazil roh chaty a z polovice preletel do jedálne, ktorá
bola na šťastie v tom čase prázdna. Ďalší skalný blok s hmotnosťou okolo
70 ton se zastavil len niekoľko metrov pred budovou. Balvan, ktorý prerazil
budovu dnes leží tesne pri chate a pripomína túto udalosť.
 
 
            Aj keď vrchol Giewontu
nedosahuje hranice 2000 m,
výstup nepatrí k ľahkým prechádzkam. Výstup sa nedoporučuje v zime a
nebezpečný je aj v čase búrky. Ako som už spomínal, najkratšia cesta na
vrchol vedie z Hali Kondratowe, ale ja som šiel na tento kopec zo Zakopaného a výstup
mi mojim priemerným tempom trval dve a pol hodiny. Je to príjemná jednodňová
túra, ale treba počítať s 1000 metrovým prevýšením. Tesne pod vrcholom sa ide
po reťazami istenom chodníku. Vápencové skaly Červených vrchov
sú na tomto úseku dohladka vychodené, preto sú reťaze na tomto úseku nevyhnutné.
Skaly na chodníku sú nebezpečné, dosť sa na nich šmýkalo. Do niekoľkých skál
boli pred pár rokmi vybrúsené stupy, tie sa však tiež šmýkajú. Na vrchole býva
počas slnečných dní množstvo ľudí, predsa len Giewont je poľský národný vrch. Neodporúča
sa sem ísť, ak vo vzduchu visí búrka. Blesky vraj bijú do vrchu kvôli krížu a
mokré skaly na chodníku sa premenia na klzisko. Tak len toľko som dneska chcel.
A teraz už šup ho hodiť „kuk“ na fotečky. 
 
 
Buďte šťastní, Hirax
 
 
 
 
 
PS: Rozhodol som sa môj panamsko-kostarický "cestopisík" vydať aj
knižne. Dátum realizácie som si predbežne stanovil na máj 2009, ale to
vás budem ešte na mojom blogu informovať. A ešte jeden dátumík: vo
štvrtok, 19.2.2009 sa v martinskej kaviarničke Kamala uskutoční moje
rozprávanie a premietanie fotiek z potuliek po Indii. Vstup je zdarma a
všetci ste vítaní. Hirax 
 
 
 
 
 

 
 
Skokanský mostík, na ktorom sa v ten sobotňajší podvečer diali dokonca preteky.   
 
 

 
 
Ufff, začíname. Tieto tabule na začiatkoch túr sú neprajnícke... bŕŕŕ
 
 

 
 
Zjavili sa ovečky a ľudia museli ísť z cesty. Aspoň raz to bolo naopak. 
 
 
 
 
 
Príroda si začala chystať pastelky. Dostala chuť maľovať. 
 
 

 
 

 
 
Chatááá! Prestávka, voda, záchod, chleba, slnko...  
 
 
 
 
 
Hra iba pre vás: Máte päť sekúnd na to, aby ste našli na obrázku aspoň jedno zviera. Začíname! 
 
 

 
 
Ja a jedna úžasná duša - môj bracho. 
 
 

 
 

 
 
Poďte, dáme si jeden jazykolam: Mravenčekovia sa mravenčekovali v mravenčekovnisku.
 
 

 
 
"Síce mám mladé nohy, ale mne sa už proste nechce! Nejdem ďalej, to si vyhoďte z hlavy!!!" 
 
 

 
 

 
 

 
 
Dávam do pozornosti trávové ostrovčeky - milujúce to bradavice zeme. 
 
 

 
 
Ťaví chrbát - vzdúva sa, koža je napätá, stačí málo, praskne a pohltí tie malé štekliace blchy špacírujúce sa drzo po ňom! 
 
 

 
 
"Ja som skala - pyšná a hrdá! kto je viac?" 
 
 

 
 

 
 
"Ufff, krížik, konečne sa približuješ...," - dychčím ako havo.  
 
 

 
 

 
 

 
 
Už len sneh, igelit a šup sa po riti do údolia, čo? 
 
 

 
 
Konečne na vrchole.Tu som si ľahol, poprosil horu o energiu a na hodinu zaspal. Občas ma zobudil burácajúci vetrisko, ale ja som mu bol za to vďačný, pretože až po pár sekundách som si vždy uvedomil kde som a pohľad na to božské horstvo vždy stál za prebratie sa do nebeskej reality. 
 
 
 
 
 
Hore bola tlačenica ako v priornom dome dva dni pred Ježíškom. 
 
 

 
 
"No, opacha, ja by som ťa nestaval! Ale vyzeráš majestátne, to je pravda. Síce na tomto zábere by si ťa mohli niektorí ľudia zmýliť z elektrickým stĺpom stojacim na nejakej polianke medzi mestom a elektrárňou. A neurážaj sa! Proste je to tak!"
 
 

 
 
"Ale čo to trepete, aký je Hirax pribratý! Tak tam fúkalo, že som sa ledva na tej skalnej hranici udržal (tužiac byť na fotke v pozadí s obrazom). To len vietor mi vošiel pod tričko, do nohavíc a mal chuť ma opanovať, hrať sa so mnou a preveriť moje kosti, či vydržia zimu..."  
 
 

 
 

 
 
  
 
 
Cesta dolu bola trošku horšia. Veru sa tam aj jednej pani šmyklo a muselo sa čakať.
 
 
 
 
 

 
 
Vám vravím, Václavák je nič. 
 
 

 
 
Kamienky zelenkasté, peknučké... 
 
 

 
 
"Áno, áno, Gjewont - si krásny, my to vieme." 
 
 
 
 
 
Keď Boh utiera s oblakom nebo (aby na nás lepšie videl a mohol sa zabávať ). 
 
 

 
 

 
 
Niekto ide hore, niekto dole - taký je život. 
 
 

 
 
Koník koníkovský z čeľade koňovitej. Môžete mi veriť, v zoológii som doma. (To už sme v mestečku, to nebolo na vrchole, xixi).
 
 
 
 
 
Koník koníkovský z čeľade koňovitej s plynovou maskou (no čo ten človek kvôli lenivosti nevymyslí, však? Aj  vrece spĺňajúce tie najprísnejšie kritéria automatického kŕmenia vyhúta, len aby mohol oddychovať).  
 
 
 
 
 
Lúčim sa s vami s veveričkou. Vôbec sa nebála ľudí (nechápem, hmhm) a bez ostychu sa dala kŕmiť. Škoda, že nemôžete vidieť za aký krátky čas dokázala zjesť oriešok a ako obratne si ho držala vo svojich labkách. A že vy neviete jej presný názov? No veď to je veverička veveričkovitá z rodu veveričiakov. Jáj, také ľahké a vy to neviete. No nič, končím. Buďte šťastní.   
 
 
 
 
 
Hirax
 
 
 
 

