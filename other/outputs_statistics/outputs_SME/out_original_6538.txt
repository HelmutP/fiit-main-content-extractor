
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peťo Zahradník
                                        &gt;
                Zo života
                     
                 Anjel strážny 

        
            
                                    16.5.2010
            o
            9:35
                        (upravené
                23.8.2010
                o
                14:31)
                        |
            Karma článku:
                7.16
            |
            Prečítané 
            842-krát
                    
         
     
         
             

                 
                    Rodičia mi o ňom hovorili už ako malému chlapcovi. Vraj ho nemám len ja, ale každý človek. Ako dieťa som teda vedel, že mám niekoho, koho síce nevidím, ale napriek tomu ma ochraňuje, vždy pri mne stojí, podáva mi pomocnú ruku a pomáha mi v živote. Keď som však vyrástol a dospel, nejako som naň zabudol a vytesnil som ho zo svojho života.
                 

                 
  
       V roku 1997 som pôsobil v Mníchove a zúčastnil som sa otvorenej diskusie s piatimi abstinujúcimi  narkomanmi z komunity Cenacolo. Veľmi ma oslovil význam terapie prácou, ako aj to, že sa tu nepoužívajú lieky či prostriedky zmierňujúce drogu, dokonca ani pri abstinenčnej kríze. Liekom tu je ,,anjel strážny", ktorým je mladý človek s podobným osudom. Niekoľko mesiacov v komunite už žije a má teda vlastné skúsenosti. On preberá starostlivosť o nováčika a pomáha mu dvadsaťštyri hodín denne, vždy mu hovorí pravdu, aj keby bola nepríjemná. Keď pracuje, stojí pri ňom, pretože nováčik nemá vôľu ani energiu pracovať. Musí trpieť a bojovať spolu s ním proti volaniu drogy, ktoré je na začiatku veľmi silné. Nováčik často po prvý raz nachádza úprimného priateľa a niekoho, kto sa o neho stará bez toho, aby za to niečo chcel.   V diskusii som sa dozvedel aj to, že pobyt v komunite trvá tri až päť rokov. Jeho presná dĺžka nie je stanovená. Denný program je prispôsobený liečbe prácou, ktorá sa končí večer okolo šiestej. Po večeri a modlitbách sa deň končí o desiatej. Komunita je sebestačná . Mladí ľudia jedia to, čo si sami dopestujú, dochovajú, navaria. Stavajú domy, pracujú v záhrade či stolárskej dielni a peniaze získavajú z predaja tričiek, pohľadníc, kníh, vlastných výrobkov z dreva a pod. Žijú vo vzájomnej úcte a rešpekte, a tak pomaly pracujú sami na sebe a učia sa žiť zdravým životom.   Stále si občas spomeniem na ten rok 1997. Pamätám si celkom presne, ako veľa som sa na stretnutí s touto komunitou naučil a ako mi to pripomenulo moje detstvo. Ľudí v komunite ochraňoval anjel. Stál pri nich v dobrých i zlých chvíľach, podával im pomocnú ruku, keď to potrebovali. Bol presne taký,  akého som ho poznal z rozprávania mojich rodičov. Už na to asi nezabudnem.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Zahradník 
                                        
                                            Nebo je vidieť aj v mláke (TEDx Bratislava 2011)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Zahradník 
                                        
                                            Grafiky pre dobrú vec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Zahradník 
                                        
                                            Lepší svet na Radlinského
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Zahradník 
                                        
                                            Monika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peťo Zahradník 
                                        
                                            Granátové jablko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peťo Zahradník
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peťo Zahradník
            
         
        petozahradnik.blog.sme.sk (rss)
         
                                     
     
        Urobil som aj veľa hlúpostí, ale snažím sa z nich poučiť a ísť stále ďalej.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1256
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spiš a okolie
                        
                     
                                     
                        
                            Grécke ostrovy
                        
                     
                                     
                        
                            Severná Afrika
                        
                     
                                     
                        
                            Turecko
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Gary Chapman
                                     
                                                                             
                                            Irvin D. Yalom
                                     
                                                                             
                                            Dan Millman
                                     
                                                                             
                                            Michal Hvorecký
                                     
                                                                             
                                            Daniel Hevier
                                     
                                                                             
                                            Dušan Mitana
                                     
                                                                             
                                            Don Miguel Ruiz
                                     
                                                                             
                                            Maxim E. Matkin
                                     
                                                                             
                                            Haruka Murakami
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Plastic Swans: Night acoustic version
                                     
                                                                             
                                            Plastic Swans: Devils
                                     
                                                                             
                                            Coldplay: Viva La Vida
                                     
                                                                             
                                            Mattafix: Living Darfur
                                     
                                                                             
                                            Nocadeň: Na čo asi myslíš?
                                     
                                                                             
                                            Nocadeň: Voľným pádom
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            klinka.blog.sme.sk
                                     
                                                                             
                                            peterherman.blog.sme.sk
                                     
                                                                             
                                            mozolova.blog.sme.sk
                                     
                                                                             
                                            olivieri.blog.sme.sk
                                     
                                                                             
                                            herda.blog.sme.sk
                                     
                                                                             
                                            baricak.blog.sme.sk
                                     
                                                                             
                                            hanout.blog.sme.sk
                                     
                                                                             
                                            davidkralik.blog.sme.sk
                                     
                                                                             
                                            nataliablahova.blog.sme.sk
                                     
                                                                             
                                            dobrovodsky.blog.sme.sk
                                     
                                                                             
                                            PASTIER.medialne.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            facebook.com
                                     
                                                                             
                                            youtube.com
                                     
                                                                             
                                            citylife.sk
                                     
                                                                             
                                            clovekvohrozeni.sk
                                     
                                                                             
                                            sme.sk
                                     
                                                                             
                                            kaspian.sk
                                     
                                                                             
                                            majak.org
                                     
                                                                             
                                            Voices.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




