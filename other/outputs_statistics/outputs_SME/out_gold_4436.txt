

 Snáď som si už konečne našiel voľnú chvíľku a môžem sa pustiť do hodnotenia môjho víkendového výletu do Bratislavy spojeného s výstavou Notebook Expo. Akurát som zavesil nejaké fotky na Facebook a možno ich hodím aj sem, uvidím..Pravdepodobne to bude dlhšie čítanie tak sa pohodlne usadte a jedzte praclíky. ;) 
   
 Na začiatok môžem povedať, že vôbec neľutujem, že som sa rozhodol ísť do Bratislavy (posledný krát som ju navštívil asi pred 6 rokmi, čo je už dlhý čas a všeličo sa zmenilo a aj ja som vyrástol) a neľutujem, že som išiel ani na spomínanú výstavu. Nečakal som, že to bude až také vzrušujúce. Zažil som všeličo. Emócie, radosť, šťastie, dobrodružstvo, srandu atď. Samozrejme nemôžem nespomenúť moju priateľku, ktorá to všetko absolvovala so mnou (Milujem Ťa), takže budem písať v množnom čísle :) 
   
 V piatok večer sme sa v hnusom počasí dostali do stanice Kysak, kde sme čakali nejaký čas na náš rýchlik do Bratislavy. Priznám sa, že vlakom som na dlhšie trasy necestoval už dlho, a keďže mám rad vlaky, tak som sa prirodzene na cestu tešil. Vlak bol relatívne poloprázdny tak sme sa nemuseli deliť s kupé s inými cestujúcimi a mohli sme si aj trošku zdriemnuť bez obáv o našu batožinu a životy (:D žartujem). 
 Cesta zbehla celkom rýchlo a nemali sme tuším ani meškanie. Ráno sme sa už prebudili niekde pred Bratislavou, vytiahol som z tašky rožky s paštétou, jablko a naraňajkoval sa. Na hlavnej stanici v Bratislave sme vystúpili z vlaku, bol som dolámaný zo "spánku" na sedadlách a unavený z cesty tam sme sa čím skôr pobrali hľadať spolužiaka, ktorý prišiel o čosi skôr ako my a čakal nás niekde na stanici. Tak sme sa presunuli pod hrad, kde prebehlo naše ubytovanie aj raňajky a pobrali sme sa na výstavu. 
   
  
   
 Hotel Crown Plaza, v ktorom sa výstava konala bol veľmi pekný a určite aj drahý :D. Vo vnútri som si veľmi nevšímal nič, až sme došli do výstavnej miestnosti. Po pravde som to očakával o čosi väčšie a nie len zopár stánkov najväčších výrobcov notebookov, kde mi chýbal stánok Apple, na ktorý som sa tešil asi najviac (ak by tam bol), no to mi vykompenzovalo aspoň to, že som držal a hral sa s (podotýkam, ako jeden z mála na Slovensku!) iPad-om. Bol to pre mňa silný zážitok (nesmejte sa :D) a asi najkrajšia vec, čo som na tej výstave videl. Apple ma svoje čaro a dokázal ho vtesnať aj do takéhoto prístroja. iPad sa mi veľmi páčil, mám s ním niekoľko fotiek a bolo to pre mňa príjemné prekvapenie, že sa tam niekto ukázal aj s ním. Ešte predtým sme boli na prednáške o iPade a mal som takú chuť si ho chytiť no nedalo sa. Potom som si všimol jednu slečnu sediacu na gauči s iPadom v ruke a opýtal sa či sa môžem od fotiť a už to bolo! :) 
   
   
  
   
   
   
  
   
 Potom sme sa ešte zapojili do nejakej sútaže, kde sme samozrejme nič nevyhrali. Aspoň zagratulujem spolužiakovi, ktorý vyhral predplatné časopisu. Odniesol som si odtiaľ pekný zážitok, fotoaparát s niekoľkými fotkami, tašky s brožúrkami a perami. 
   
 Keďže už bol obed tak sme sa rozhodli zájsť do nejakej reštaurácie. Priateľka ma zatiahla neďaleko do reštaurácie Divný Janko. Môžem len odporučiť! Ceny boli vzhľadom na porciu veľmi dobré. Výdatne sme sa najedli chutného mäsa, čo sme nedojedli sme si nechali zabaliť a pobrali sa spať na ubytovňu. Tam sme sa tiež dozvedeli smutnú správu o páde poľského lietadla s prezidentom, ktorá nás dosť šokovala. 
   
   
   
   
   
   
  
 Zväčšiť mapu 
   
   
   
   
 Keďže je toho ešte dosť, rozhodol som sa článok rozdeliť na dve časti tak pokračovanie nabudúce. :) 
   
   

