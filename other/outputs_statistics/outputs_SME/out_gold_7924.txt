

 Ide o miesto kde sa v Bratislave pripájajú vozidlá zo Staromeskej na Nábrežie L. Svobodu smerom k PKO.Vozidlá zo Staromestskej a Nového mosta (SNP) idú v jednom jazdnom pruhu a musia dávať prednosť na základe dopravnej značky daj prednosť v jazde. 

Hlavná cesta má dva jazdné pruhy a vozidlá sú na nej púšťané v dávkach podľa činnosti semaforu z predchádzajúcej križovatky. Ak nie je hustá premávka, tak väčšina vodičov sa preraďuje do ľavého pruhu aby uľahčila pripájanie vodičom z vedľajšej cesty. 

  

 
 Obr. 1. Najprv bola nehoda vozidiel Smart a zelená Felícia.  

 Zrejme Smart na hranici križovatky zastal a Felícia za nim nie. Pred nimi stálo vozidlo polície kde riešili vzniknutú situáciu. Miesto nehody bolo označené výstražným trojuholníkom. Ďalšie vozidlá ich obchádzali, ale aj tak museli dávať 
prednosť v jazde. Preto zastalo aj vozidlo striebornej farby, ale vozidlo 
červenej farby nie. 

  
Obr. 2. Našťastie to vodiči riešili s pomerne s chladnou 
hlavou a nezopakoval sa včerajší prípad z Ostravy, kde
jeden vodič pri riešení banálnej dopravnej situácie dobil druhého vodiča päsťami a ten opätoval útok streľbou. 

 Záver: 
Nehody boli sú a aj budú, ale keby sa dodržiaval bezpečný odstup a pozeralo sa pred seba, tak by ich mohlo byť podstatne menej. Okrem iného by sa v takýchto situáciách hodilo, keby policajti nechali zapnuté majáky, predsa tým dajú 
vodičom najavo, že treba zvýšiť pozornosť.  P.S. aj z električky sa dá nafoiiť veľa vecí, ktoré sú dopravne zaujímavé ... 

