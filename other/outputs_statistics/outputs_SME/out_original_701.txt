
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír (Miro) Jurík
                                        &gt;
                Rómovia
                     
                 Rómska hymna 

        
            
                                    15.6.2008
            o
            14:14
                        |
            Karma článku:
                10.46
            |
            Prečítané 
            6063-krát
                    
         
     
         
             

                 
                    Hymna Rómov - "Opre Roma" (Hor sa, Rómovia), známa aj pod názvami "Romale, čhavale"  a  "Gelem, Gelem", (prípadne "Delem, Delem" a "Geľom, geľom") má korene v pôvodnej ľudovej piesni balkánskych Rómov. V roku  1968 režisér Alexandar Petrovič  použil jej melódiu  vo filme Nákupca peria. V roku 1971 pieseň upravil Jarko Jovanovič. Za rómsku hymnu ju vyhlásili v roku  1971 na  1. Medzinárodnom zjazde Rómov v Orpingtone vo Veľkej Británii.
                 

                  Okrem medzinárodnej hymny pokladajú Rómovia na Slovensku za svoju hymnu aj pieseň Čhajori romaňi (rómske dievčatko), ktorá vznikla v koncentračnom tábore Osvienčim. Rómsku hymnu hrávajú  na stretnutiach rómskych aktivistov alebo pri  oslavách Medzinárodného dňa  Rómov (8. apríl).           Notový zápis rómskej hymny z roku 1971. Obrázok prevzatý z http://www.romale.sk/                                     Text hymny       (Jedna z  variant):     Geľom, geľom, lungone dromenca,  Malaďiľom bachtale Romenca.  Aj, Romale, aj čhavale.  Aj, Romale, aj čhavale.     Aj, Romale, khatar tumen aven?  Le cerhenca, bachtale dromenca?     The man esas bari famiľija,  Murdarďa la e kaľi legija.     Aven manca sa lumake Roma  Kaj phuterde le Romenge droma.     Ake vrjama – ušťi Rom akana  Amen chuťas the mištes keraha.  Aj Romale, aj čhavale,  Aj Romale, aj čhavale.     Slovenský preklad:     Šiel som, šiel som, dlhými cestami  Stretol som  sa so šťastnými Rómami.     O, Rómovia, o chlapci,  O, Rómovia, o chlapci,     O, Rómovia, odkiaľ  prichádzate?  So  stanmi, po šťastných cestách?     Aj ja som mal  veľkú  rodinu.  Zabila ju  čierna légia.     Poďte so  mnou, Rómovia celého sveta.  Kde sa otvorili  cesty Rómom.     Teraz  je tá  chvíľa – povstaň človeče  Vyskočíme a bude dobre.  O, Rómovia, o chlapci,  O, Rómovia, o chlapci.     Mne osobne sa najviac páči rómska hymna v podaní  hudobnej skupiny Cigani Ivanovici. Tu si ju môžete vypočuť.     ((Rómska hymna))                                 Použitá literatúra:              HANCOCK, Ian: My Rómsky národ. Bratislava: Petrus, 2005                 Internetové zdroje:          http://www.romale.sk/      www.rnl.sk   http://www.gipsy.sk/   http://romovia.vlada.gov.sk                            

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (45)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír (Miro) Jurík 
                                        
                                            Čo som si zapamätal z litovského jazyka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír (Miro) Jurík 
                                        
                                            Beirut - Nantes
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír (Miro) Jurík 
                                        
                                            Kernave - litovska Troja (fotografie)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír (Miro) Jurík 
                                        
                                            Krizovy vrch v Litve (fotografie)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír (Miro) Jurík 
                                        
                                            Trakai -  hrad na ostrove (fotografie)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír (Miro) Jurík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír (Miro) Jurík
            
         
        vladimirjurik.blog.sme.sk (rss)
         
                                     
     
        Exštudent zvažujúci svoje možnosti. 
....................................................
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1715
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Rumunsko
                        
                     
                                     
                        
                            Rusko
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Videoblog
                        
                     
                                     
                        
                            Dvaja Slovaci v Litve
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Rusofilov komentár k jednej tragikomickej férovke
                                     
                                                                             
                                            V rómskej osade (1985)
                                     
                                                                             
                                            Na Simoninu pamiatku
                                     
                                                                             
                                            Rozhovor s Marínou Čarnogurskou (vrelo odporúčam)
                                     
                                                                             
                                            Sen Banskobystričana
                                     
                                                                             
                                            Šachová lovestory
                                     
                                                                             
                                            Novodobé dejiny Kosova
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Lao-c: Tao te ťing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Romajur znova bloguje
                                     
                                                                             
                                            Lietajúci bloger
                                     
                                                                             
                                            Ján Myšľanov
                                     
                                                                             
                                            Edo Chmelár
                                     
                                                                             
                                            Zdenka Bencúrová
                                     
                                                                             
                                            Mikser
                                     
                                                                             
                                            Rieper
                                     
                                                                             
                                            Peter
                                     
                                                                             
                                            Mika
                                     
                                                                             
                                            Veľký priateľ prírody
                                     
                                                                             
                                            Roman Bábik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Britské listy
                                     
                                                                             
                                            Je to tak
                                     
                                                                             
                                            Hefaistos z Hrochote
                                     
                                                                             
                                            Čítajme
                                     
                                                                             
                                            Zenit Peterburg
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




