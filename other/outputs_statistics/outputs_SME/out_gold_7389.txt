

   
 Nepovedia - lebo nevedia. 
 Najmenej nebezpečná kategória. Je možné, že aj naše elity ešte stále nie sú rozhodnuté. Je možné, že programy jednotlivých strán a ich personálne obsadenie, stále nie je dokonalým prienikom toho, po čom ich srdce túži a aká je ponuka na politickom trhu. Že by nešli voliť vôbec, je na vzhľadom na ich nasadenie a miesto v spoločnosti, málo pravdepodobné. 
   
 Nepovedia - lebo nechcú. 
 Zaujímavá kategória. Naše elity si zrejme myslia, že svojim jednoznačne vyjadreným názorom by devalvovali svoj mýtus apolitickosti. Ale kto tu verí na apolitickosť? Kto tomu uverí, že štátny úradník, predseda stavovskej organizácie, ale keď preženiem, aj prezident štátu, policajného zboru, hasičov, generál u vojska, sudca po nastúpení do funkcie, zatratí svoj zmysel pre politickú realitu? Prestane vnímať svoje okolie, ba dokonca, že mu prestane na politickom vývoji krajiny záležať? Ich konanie, dennodenná práca a rozhodovanie má byť apolitické. Nie ich názor na veci verejné raz za štyri roky. To nedokáže vygumovať žiadne nariadenie ani zákon z hlavy. 
   
 Nepovedia - lebo nemôžu. 
 Najnebezpečnejšia kategória. Pokiaľ spevák a zabávač dajú svoje umenie na predvolebných guľášoch do služieb tých, ktorých by nevolili a nemôžu to povedať priamo, lebo by diskvalifikovali seba a svojim chlebodarcom by radosť neurobili, je to v ich mentálnej rovine. 
 Oveľa nebezpečnejšie sú ale elity, ktorých oligo-majetky sú priamo od politických špičiek závislé. Tieto  pred každými voľbami rozhodia siete do obidvoch táborov, prípadne do všetkých politických táborov, aby si svoj biznis po voľbách poistili. Siete nesmrdia od rýb, ale peniazmi, ktoré sa tam dostali zase len takýmto pokútnym spôsobom na základe osvedčeného postupu spred štyroch rokov. Je ťažké povedať, čo je nezdravšie? Samotní aktéri, ktorí nikdy nepovedia, koho budú voliť, alebo systém, ktorý im to umožnil? 
 Zlá správa na záver: Ten systém sme si za dvadsať rokov vytvorili pracne my sami, voliči. 
 A dobrá správa na záver: Ten istý systém môžeme zmeniť zase len my sami. 
   
 A ešte za seba: Nie som elita, ale aby som nekázal vodu a pil víno, poviem priamo, že volím Slobodu a Solidaritu. 
   

