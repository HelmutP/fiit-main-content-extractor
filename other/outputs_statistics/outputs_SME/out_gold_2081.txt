
 Poriadny pes je raz ročne očkovaný proti rôznym svinstvám, ktoré ho môžu v psom živote stretnúť. Skúkam sa celý život pohyboval vo voľnej prírode, žul tam všelijaké páchnuce veci a tak besnota, parvoviróza, psinka, infekčná hepatitída, leptospiróza, koronaviróza, boli naši nepriatelia, proti ktorým sme bojovali pravidelným očkovaním.

Zo začiatku to prijímal ako osud. Musí to byť, tak to teda musí byť. Prvé jeho vážne zoznámenie s lekármi prišlo keď mal niečo vyše roka.

Mali sme kanceláriu v dedine, kde celý náš tým sedel za počítačmi a Skúkam sa voľne prechádzal po záhrade a raz za čas niečo nekalé vymyslel. Raz to boli sliepky od suseda, inokedy pomalá mačka. Neplechy prinášali síce nepríjemnosti so susedmi, ale Skúkam sa od zbojstiev odučil. Až na jedno. Tým bolo bezstarostné prechádzanie cez cestu prvej triedy na trase Bardejov – Prešov.

Raz som ho počas obeda načapal na kraji cesty, po ktorej sa celkom svižne preháňali autá. Trochu som zazmätkoval a zapískal na neho.  Problém bol ale vtom, že Skúkam bol na druhej strane cesty. Rozbehol sa ku mne a to nemal robiť. V strede cesty prvej triedy sa uprostred dediny na rovnakom mieste v rovnakom čase ocitol Skúkam a modrá skriňová Avia, uháňajúca aspoň deväťdesiatkou. Výsledok bol katastrofálny. Pre Aviu i Skúkama. No, pre Skúkama asi viac. Avia sa ledva udržala na vozovke a s pokrivenou karosériou rýchle zmizla v diaľke, bez toho, aby sa šofér zaujímal koho zabil.

Skúkam ležal nehybne odhodený na kraji cesty a ja som bol v šoku.  Zobral som bezvládnu kopu kože a kostí na ruky a odniesol som ho k susedom.  Nie mňa, ale suseda napadlo, že by sme predsa len mohli skúsiť veterinára  v Prešove. Nasadli sme do auta a nasmerovali sme to do zvieracej nemocnice. V aute, tak po desiatich minútach, Skúkam otvoril jedno oko, druhé oko a ja som zajasal, napriek tomu, že oči opäť zatvoril. Prvé zoznámenie s veterinárnou nemocnicou bolo dynamické. Keď som so psom na rukách vošiel do čakárne, zazrel som desiatky psov rôznych farieb, rás i veľkosti. Skúkam so zatvorenými očami vztýčil uši, začal ňuchať a keď zistil o čo ide, vrhol sa mi z rúk rovno na psov. 

Vôbec nevyzeral na to, že bol pred pätnástimi minútami ešte mŕtvy. Zmätok , ktorý vyvolal, nám umožnil predbehnúť všetkých čakajúcich pacientov a vbehnúť rovno k lekárovi. Tam som sa s prekvapením dozvedel, že žiaden röntgen nebude a nebude ani žiadne veľké vyšetrovanie. Lekár usúdil podľa bitky, ktorú Skúkam vyvolal, že je vlastne v poriadku. Odviezol som ho domov a do týždňa sa stal zázrak.

Skúkam týždeň preležal , v noci plakal pri obracaní z boka na bok ako malé decko, ale vydržal podľa rady lekára nič nejesť a málo piť. O mesiac na ňom už nikto nespoznal psa, ktorú zdemoloval vlastným telom nákladné auto.

Ale malý červík pochybnosti v jeho hlave začal pomaly hlodať. Kdesi vzadu ostala spomienka na nepríjemnú príhodu spojenú s ujom v bielom plášti a zvláštnou budovou s podivným zápachom.

Averzia k stavu veterinárov naplno prepukla o rok. Spali sme na Čergove. Bol taký normálny víkend. V piatok večer mi Skúkam kdesi utiekol a asi za pol hodiny sa vrátil, nič nehovoriac, so sklopenými ušami. Až v nedeľu,  keď sme kráčali na autobus, som si všimol, že z vnútornej strany stehna má rozrezanú kožu v dĺžke asi dvadsať centimetrov. Na ploche veľkej ako dlaň mal odkrytý sval a mňa pri pohľade na jeho poranenie celkom slušne napínalo. On si z toho veľa nerobil, len si ranu, ktorú mu evidentne spôsobil diviak, neustále olizoval.      

 V pondelok sme ihneď zrána išli k lekárovi a to som asi nemal robiť. Dozvedel som sa, že rana je, vďaka neustálemu olizovaniu, takmer zahojená, a tak je ju potrebné opätovne rozrezať a zašiť. Výsledkom rozhodnutia veterinára bola operácia v narkóze, niekoľkohodinové preberanie z núteného spánku a doživotne nadobudnutá averzia k zverolekárom.   

Skúkam už nikdy nepripustil dotyčného veterinára k sebe na vzdialenosť bližšiu ako tri metre. Antibiotika po operácii som mu musel injekčne podávať sám, rovnako ako som ho musel kdesi v lese zbavovať stehov s amatérsky vyrobenou pinzetou.  Ročné povinné očkovanie sa stalo pre mňa nočnou morou. Už prvé očkovanie po operácii bolo zaujímavé. Skúkama s nasadeným košíkom som pevne držal a veterinár sa nenápadne s injekciou približoval kdesi zboku. Moje pevne držanie sa skončilo tak, že ma môj vlastný dobrosrdečný pes vláčil po veterinárovej záhrade, metal so mnou kde chcel  a ja som prišiel domov ako keby som čistil kanály. Samozrejme som injekciu musel nakoniec podať priateľovi sám osobne.  

Návštevy lekárov som teda musel obmedziť len na výber príslušných náplní do injekčných striekačiek, až na prípady, keď očkovanie prebehlo ako súčasť pitia piva, pričom Skúkam ležal pod stolom a kamarát z Čiech, veterinár, mu nenápadne zhora pichol nenávidený prípravok.

Tretia a posledná návšteva lekára sa konala pred niekoľkými rokmi v Liptovskom Mikuláši, keď Skúkama, pri natáčaní s Českou televíziou, uštipla vretenica.  Opuchol a nevyzeral dobre. Odviezli sme ho k veterinárovi, ale Skúkam hneď vedel, kto býva v tom rodinnom dome a odmietol, podľa svojho dlhoročného návyku, vstúpiť dnu. Ako sa ukázalo, nebolo to ani potrebné. Lekár sa totiž pri mojej žiadosti o aplikáciu séra veľmi úprimne zasmial.   

„Viete čo, my nemáme sérum pre ľudí, nieto pre psov,“ znela jeho veselá odpoveď. 

Problém sme vyriešili podaním akýchsi ukľudňujúcich tabletiek, a Skúkam, čo už ma ani neudivilo, sa aj z tejto nehody vystrábil bez problémov.

Na všetkých svojich lekárov si Skúkam  spomenul , keď raz večer, na čergovskom hrebeni, neposlušne navštívil veľké stádo diviakov.  Pudy mu nedali, napriek môjmu varovaniu, mi odbehol, ale keď zistil, že na mieste je viac ako desať zverov, ktoré mu spôsobili pred rokmi komplikácie s operáciou, okamžite sa otočil a začal trieliť preč.  Bolo ale neskoro.

Dav diviakov sa rozbehol za ním a môj najlepší priateľ mal čo robiť, aby pred nim udržal bezpečnú vzdialenosť. Toto všetko som nevidel. To som len vytušil z rôznych zvukov, ktoré sa ozývali z tmy bukového lesa.  Čo som uvidel  ako prvé, v úzkom lesnom úvoze, bol Skúkam s vyplazeným jazykom, ktorý sa okamžite schoval za môj chrbát a pozeral sa, ako vyriešim vzniknutý problém.

Za ním sa totiž rútila masa čiernej zveri, ktorá išla nekompromisne riešiť  problém s večerným votrelcom.  Nebolo kde uhnúť a ja som sa už videl na operačnom stole v narkóze vedľa Skúkama.  Keď som ale zasvietil čelovkou, aby som si kdesi rýchle našiel nejaký obranný papek,  diviakom sa naša zostava prestala páčiť a s nazlosteným chrochtaním zmizli v októbrovej tráve.

To bolo posledný krát, keď bol so Skúkamom v lese nejaký problém. Odvtedy som problémy vyrábal už len ja. 

Ale o tom nabudúce. 
