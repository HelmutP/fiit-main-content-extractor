
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Renáta Holá
                                        &gt;
                Pocitovky
                     
                 V tie dni ... 

        
            
                                    23.4.2010
            o
            19:58
                        (upravené
                23.4.2010
                o
                20:04)
                        |
            Karma článku:
                6.93
            |
            Prečítané 
            1440-krát
                    
         
     
         
             

                 
                    Sú dni, kedy sa nemám najlepšie. Antidepresíva, liečba akoby nepomáhali.
                 

                 Životný pocit - to je to, čo mi po tie dni chýba. Prázdnota - to je to, čo ma po tie dni napĺňa. „Vymením prázdnotu za životný pocit. Rozdiel doplatím," tak by ešte pred niekoľkými dňami znel môj inzerát v miestnych novinách.   Avšak teraz by som ho už nepodala. Stačilo siahnuť do knižnice a oslovil ma citát:   „Sú to myšlienky, čo robia život človeka šťastným alebo nešťastným." (Marcus Aurelius)   ...a tak, keď sa v mojej duši zmráka a mňa zalieva prázdnota, siaham po knihách. Po knihách, ktoré hovoria o sile našich myšlienok a sile nášho podvedomia, lebo šťastie a radosť je len zvyk, zvyk pozitívne myslieť. Zvyk ceniť si každý nový deň.   Chce to však opustiť pohodlné i keď boľavé bahnenie sa v depresívnych myšlienkach, nájsť si v tie dni chvíľku pre seba, pre svoju dušu a očistiť myseľ od balastu, ktorý sa v nej hromadí. Stačí upokojiť svoju myseľ a postupne ju napĺňať myšlienkami harmónie, šťastia, radosti a lásky a človeku sa podarí vyhnať prázdnotu a nahradiť ju v tie dni tým úzkoprofilovým tovarom - životným pocitom, chuťou do života.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (31)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Sme rozdielni, tak aj naša liečba je individuálna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Aj na Slovensku cítiť potrebu reformy psychiatrie
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Pasívne čakanie nie je moja cesta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Funguje to po kvapkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Ide to aj bez antipsychotík?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Renáta Holá
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Renáta Holá
            
         
        hola.blog.sme.sk (rss)
         
                        VIP
                             
     
         blogujúca psychotička, dôverne poznajúca stavy depresie či mánie, snažiaca sa byť normálnou manželkou a matkou 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    174
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2359
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodičovstvo
                        
                     
                                     
                        
                            Psychika
                        
                     
                                     
                        
                            Z dovolenky
                        
                     
                                     
                        
                            Kde bolo,tam bolo...
                        
                     
                                     
                        
                            Drobnosti
                        
                     
                                     
                        
                            Práca
                        
                     
                                     
                        
                            Myslím si
                        
                     
                                     
                        
                            Pocitovky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




