
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Anna Zboraiová
                                        &gt;
                Nezaradené
                     
                 Májové pastorále 

        
            
                                    4.5.2010
            o
            22:46
                        |
            Karma článku:
                2.98
            |
            Prečítané 
            297-krát
                    
         
     
         
             

                 
                    Máj, to je mesiac dažďov a jemne hrejivého slnka. Mesiac majálesov a májových vatier. Máj je mesiac stavania májov a Turíc.
                 

                 Ešte donedávna sa májové zvyky dodržiavali aspoň po dedinách. Teraz je to čoraz zriedkavejšie. Naši predkovia si svoje zvyky chránili a snažili sa aby sa uchovali pre budúce generácie. Terajšia generácia však akosi zabúda na svoje korene a ľudové zvyky.   Máje- stavanie májov má korene v symbole obnovy prírody a neskôr začalo symbolizovať dvorenie a verejné potvrdenie citov mládenca k vyvolenému dievčaťu. Počas noci keď sa stavali máje, zvykli vyhrávať ľudoví muzikanti ťahavé nočné melódie a clivé serenády. Zaľúbení mladíci strážili svoj s pýchou postavení znak lásky s papekom aby ho nejaký posmešný potrimiskár neukradol a oni neostali v hanbe.   Májové vatry- majú viac symbolických znakov. Dá sa v nich nájsť starodávna mystická viera v prírodu a v božstvá ale má aj kresťanské znaky. Je spojená s ohňom, silou a snekonečnosťou prírody. Je vňom symbol znovuzrodenia a viery. Každý si môže prispôsobiť oheň týčiaci do výšky k tomu čo verí a dúfa. Oheň je nepriateľom a zároveň spojencom, je silou a zároveň slabosťou. Oheň predstavuje ľudskú podstatu.   Majálesy- sú veselé jarné slávnosti, plné hudby, spevu a smiechu. Sú to podujatia na oslavu jari. Radostné vítania tepla po zime. Úsmevy, praclíky, šišky, džbány vína, prvé stretnutia, radosť ktorá je úžasne nákazlivá. Polky, čardáše, pikantné piesne. Mierne vínkom šmrcnutý primáš zmiešava Straussovu polku s Montyho čardášom. To je majáles.   Turíce- v kresťanskom ponímaní sú to sviatky Zoslania Ducha Svätého. Turíčne zvyky sú však z úplne iného obdobia. Z obdobia starodávnej predkresťanskej éry starých Slovanov. Práskanie bičom na lúkach a turíčne vatry majú korene u starých božstiev, ktorí ochraňovali ľud pred zlými silsmi. Na Turíce sa otvárali a čistili studničky, nosili sa do príbytkov zelené ratolesti, ktoré mali za úlohu priťahovať dobré sily a ochraňovať pred zlom.   To je máj, mesiac nádherných zvykov a vetra ktorý jemne pochládza tvár. Náhle búrky a slnečné dni. Vôňa lipových kvetov a púpavy. Steblá trávy vlhké od rannej rosy. Teplý súmrak dňa. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Zboraiová 
                                        
                                            By si sa čudovala...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Zboraiová 
                                        
                                            Prudká úvaha o vlastnej dôležitosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Zboraiová 
                                        
                                            Homo periculum sapiens
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Zboraiová 
                                        
                                            Asi som z Centauri, lebo z Venuše určite nie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Zboraiová 
                                        
                                            Teta, vychovali ste hovädo!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Anna Zboraiová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Anna Zboraiová
            
         
        zboraiova.blog.sme.sk (rss)
         
                                     
     
        Fascinujú ma matky, ktoré napomínajú svoje deti potichu a oni ich poslúchajú.Mám svoje názory na veci okolo seba ale ak sa zmýlim uznám svoj omyl a nežijem v ilúzii o svojej pravde.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    111
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    489
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ómamine recepty
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Náhle traumy vedomia
                        
                     
                                     
                        
                            Annikine fejtóny
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Stieg Larsson-Dievča, ktoré sa hralo zo zápalkami
                                     
                                                                             
                                            Stieg Larsson-Vzdušný zámok, ktorý vybuchol
                                     
                                                                             
                                            Stieg Larsson-Muži, ktorí nenávidia ženy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Evanescence-Best Hard Rock Performance
                                     
                                                                             
                                            Evanescence-The Open Door
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            lesná víla-žaneta Daroková
                                     
                                                                             
                                            Ziff-Ivana Odeen
                                     
                                                                             
                                            Spriaznená duša- Janka Dratvová
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




