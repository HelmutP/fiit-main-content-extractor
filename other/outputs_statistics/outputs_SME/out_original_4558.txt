
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Feik
                                        &gt;
                Cestovanie
                     
                 160 km/h na diaľnici. Šetrenie času mizerné, riziko vysoké. 

        
            
                                    14.4.2010
            o
            18:10
                        (upravené
                14.4.2010
                o
                18:26)
                        |
            Karma článku:
                13.27
            |
            Prečítané 
            3721-krát
                    
         
     
         
             

                 
                    Zvýšenie rýchlosti na 160 km/h na niektorých úsekoch diaľnice je nezmysel. Čas neušetrí parkticky žiadny, zato však závratným spôsobom stúpa nebezpečenstvo.
                 

                 Rezort dopravy nedávno pripustil, že na vybraných úsekoch diaľnic uvažuje o zvýšení maximálnej povolenej rýchlosti na 160 km/h. Konkrétne má ísť o dva úseky, Križovatka Rakoľuby – Trenčín (14,6 km) a Bytča – Vrtižer (10 km). Hovorí sa aj o treťom úseku, pravdepodobne na diaľnici medzi Prešovom a Košicami.   Našťastie, Národná diaľničná spoločnosť zavedenie stošesťdesiatky  neodporúča. Z finančných dôvodov. K spomínanému návrhu však evidentne chýba analýza zdravého rozumu. Na desaťkilometrovom úseku vodič ušetrí presne 51 sekúnd času. Pritom sa však ženie rýchlosťou kde za jednu sekundu prejde 44 metrov a brzdná dráha sa za ideálnych okolností predlžuje na 125 metrov (za mokra na 167 a v snehu dokonca na viac ako 670 metrov). Pre porovnanie - brzdná dráha auta idúceho rýchlosťou 130 kilometrov za hodinu je na suchej vozovke 83 metrov, na mokrej 110 a na snehu dokonca 440 metrov.   Navrhované úseky intenzitou dopravy nepatria medzi najvyťaženejšie, takže dôvodom na zvýšenie rýchlosti nie je ani lepšia plynulosť premávky. Oboma úsekmi prejde v priemere 20 tisíc vozidiel denne (pre porovnanie, na trase Vajnory – Senec je to až 46 tisíc vozidiel), takže návrh postráda akýkoľvek rozumný zmysel. Teda okrem toho psychologického - pôžitku z rýchlej jazdy v rýchlom aute. Diaľnica však nie je okruh Formuly 1. Viac ako 25 tisíc dopravných nehôd a 347 mŕtvych na slovenských cestách v roku 2009 by malo byť dostatočným varovaním.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (129)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Feik 
                                        
                                            Cargo bikes: Revolúcia v zásobovaní Starého Mesta
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Feik 
                                        
                                            Cyklotrasy v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Feik 
                                        
                                            Podporuje Kňažko program primátora Milana Ftáčnika?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Feik 
                                        
                                            Pribinova ulica pri Eurovei – koniec parkovania na chodníkoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Feik 
                                        
                                            Občiansky rozpočet Bratislavy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Feik
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Feik
            
         
        feik.blog.sme.sk (rss)
         
                                     
     
         Poradca primátora Bratislavy. Koordinátor cyklistickej a pešej dopravy, člen Komisie mesta pre verejnú dopravu a člen Rady mesta pre trhy. Podporovateľ zavedenia participatívneho rozpočtu. Aktívny občan. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2241
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Cyklodoprava
                        
                     
                                     
                        
                            Životné prostredie
                        
                     
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Samospráva
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Cargo bikes: Revolúcia v zásobovaní Starého Mesta
                     
                                                         
                       Skúpia mecenáši M. Kňažka všetkých súperov?
                     
                                                         
                       S prepáčením „Transparentnosť“
                     
                                                         
                       Cyklotrasy v Bratislave
                     
                                                         
                       Timemanažment medzi milencami?
                     
                                                         
                       Bratislava je otvorenejšia a transparentnejšia
                     
                                                         
                       Pribinova ulica pri Eurovei – koniec parkovania na chodníkoch
                     
                                                         
                       Hľadá sa typický symbol pre Bratislavu
                     
                                                         
                       Mestské zastupiteľstvo alebo kto bude prvý pred kamerou
                     
                                                         
                       Ponúkam výsledky: zdravšie financie, dopravné projekty, otvorenosť
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




