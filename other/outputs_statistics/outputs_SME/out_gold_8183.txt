

 1. Ak sa zopakujú volebné čísla z roku 2006, tak to vôbec nebude dobrým vysvedčením, ale na druhej strane, ak sa na to pozrieme optimisticky, tak v tomto prípade budeme mať veľmi blízko ku Grécu :-(. 
 2. Ak padne táto možnosť, tak sa môžme trocha potešiť, ale musíme si uvedomiť, že sme v posledných rokoch žili veľmi nad pomery a to značí, že musíme počítať s určitými nutnými zmenami (utiahnutie opaskov na určitú dobu). 
 3. Ak sa niekto nájde z dnešnej opozície a dokáže kolaborovať so SMER-om, musí si byť vedomý, že je to jeho posledný politický výkrik (kolaborant je v spolku vždy iba na špinavú robotu). 
 4. Ak by sme sa dostali číslami na hranicu (50:50 opozícia - koalícia), tak je ešte jedna možnosť, ktorú predstavujú opakované voľby. 
 Každý kto chce, aby u nás nepadla možnosť číslo 4, môže pre to niečo urobiť. 
 Prekonať svoju lenivosť, preorganizovať svoje činnosti, dvihnúť zadok z kresla a navštíviť volebnú miestnosť dňa 12.6.2010. Nebudem nikomu hovoriť, koho má voliť a hlavne prečo. Som presvedčený, že každý zodpovedný občan má svojho favorita, prípadne podporí (podľa jeho názoru) menšie zlo. 
 Čo tu napíšem s plný vedomím je, že ja budem voliť SDKÚ-DS. Budem ich voliť hlavne z dôvodu, že je to "partaj", ktorá sa už osvedčila, keď nás vytiahla z bahna mečiarovského vládnutia. Som si vedomý ich chýb, pochybení, zlých rozhodnutí, ale dávam im šancu, lebo ich práca môže byť aj mojím prospechom ako občana SR. 
 PS. Roky tvrdím (nadsadené): Kto nebol voliť, nemá právo kecať o zlej/dobrej vláde. 

