

 Ako sa už vyjadrili niektorí známi na fórach - realita má ďaleko k naivným predstavám.  Určite sa na jeseň nebudú hromadne preinštalovávať všetky komerčné programy na všetkých PC v štátnej správe. Zmluvy sa uzatvárajú dlhodobo a nikto nečaká, že sa zo Slovenska stane DebianLand. Skôr pôjde o jednotlivé aplikácie ako o celý systém (napr. OpenOffice, VLC...).  Za dobrých podmienok by sa mohla zlepšiť jazyková podpora týchto aplikácií, prípadne sa podporia vývojári, ktorí vydávajú svoje programy zadarmo. Vláda by však musela popracovať aj na autorskom práve, aby sa niektoré veci stali realitou. 
 Dobrou správou však je, že sa o voľne dostupnom softvéri uvažuje. V ideálnom prípade sa proprietárne kodeky STV zmenia o otvorené, na Kataster sa dostanete aj z "iného" prehliadača a dokumetny nebudú vo formátoch .doc, alebo .docx.  Na školách sa žiaci dozvedia aj o "iných" operačných systémoch a skúsia niečo vytvoriť pomocou pygame... 
 Nakoniec, ktovie - možno tento bod bude znamenať aj to, že sa na úradoch odstránia defaultné "míny" a "karty" a začne sa hrávať Frozen Bubble, Wesnoth, alebo Urban Terror. 

