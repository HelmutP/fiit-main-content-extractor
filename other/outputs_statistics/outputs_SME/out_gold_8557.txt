

 Mnoho krát sa mi stalo, že mi na jednej strane baby povedali isté kritériá pre svojho priateľa a na druhej strane si našli niekoho úplne iného. Pýtam sa prečo to tak je? 
 Kritérií na výber je veľa a dajú sa rozdeliť na krátkodobé a dlhodobé. Medzi tie krátkodobé patrí samozrejme vzhľad (hlavne svalu musculus gluteus maximus) a spôsob, akým chlap zaujme - humor, pohľad, čin... Myslím tým niečo, čo nám stačí na súhlas s jednou schôdzkou. Dlhodobé kritériá sa už líšia u každej zvlášť. Existujú tie čo chcú verného, tie čo chcú vtipného, tie čo chcú drsného na dlhodobejší vzťah. Dalo by sa povedať, že to je tým, že ženy vlastne nevedia, čo chcú, teda u mužov. 
 Niektoré potrebujú ochrancu, iné poskoka, ďalšie zas kópiu svojho otca, iné úplny protiklad, iné potrebujú len zábavu a tak by som mohla pokračovať až do nekonečna. Stále vravíme nejaké vzorce do ktorých by mal chlap pasovať. Príde mi to až smiešne. 
 Najväčší rozdiel je v tom, čo nahovárame kamarátkam a sebe a tým, čo sa deje v realite. S vetou " Ja som len na tmavovlasých!" aj tak skončíme s blonďákom. Tak načo kategorizovať a vyberať si podľa takýchto vecí ideálneho partnera keď aj tak ten pravý môže byť akýkoľvek? 
   

