

 Prešov 2. februára (TASR) - Amputovať ľavú nohu v predkolení
museli chirurgovia prešovskej nemocnice 59-ročnej chodkyni, ktorú
dnes ráno v Prešove zrazil opitý policajt. Dopravná nehoda sa stala
krátko pred piatou hodinou ráno a spôsobil ju 23-ročný
nadstrážmajster Róbert L. z Prešova, študent denného štúdia Akadémie
PZ v Bratislave. 

 Hovorkyňa Krajského riaditeľstva PZ v Prešove Magdaléna Fečová
pre TASR spresnila, že policajt jazdil na súkromnom aute značky VW
Beetle po Kováčskej ulici, kde pred barom Alfa z doteraz nezistených
príčin zrazil Viktóriu L. z Prešova. Vozidlom ju pritlačil o betónový
múr lemujúci okraj chodníka. Žena utrpela ťažké zranenia s trvalými
následkami. Je hospitalizovaná na chirurgickom oddelení NsP v Prešove. 

 Podľa Fečovej vodičovi namerali pri dychovej skúške 1,4 promile
alkoholu v krvi a pri opakovanej skúške 1,24 promile. Škoda na
vozidle je 100 tisíc korún. Nehodu objasňuje Okresný dopravný
inšpektorát v Prešove. O prepustení policajta zo služieb Policajného
zboru rozhodne rektor Akadémie PZ. 

 "Krajské riaditeľstvo PZ v Prešove sa ospravedlňuje poškodenej
a jej príbuzným za ujmu na zdraví spôsobenú nezodpovedným konaním
policajta," dodala hovorkyňa. 

