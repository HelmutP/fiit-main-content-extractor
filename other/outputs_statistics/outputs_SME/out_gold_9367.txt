

 Ona stála pri dverách: zadných. Teplota v autobuse kulminovala, telo bolo na tele, tašky sa šúchali o šaty, pančuchy, ktoré na prepotených nohách boli špeciálne spôsobilé sa roztrhať, vytrhať a nitky z nich ťahať sa súdružsky do diaľky s pôvodcom skazy. Jednou rukou kŕčovite zvierala kovovú držadlovú tyč, druhou objímala ťažkú, preplnenú študentskú aktovku. Nedočkavo vyzerala von, či už nie je zástavka - tá jej. Kdežeby!... 
 Nové trúbenie, nový stisk, nové „pardon“, „prepáčte“, „čo ste slepý!“, a podobne, zaznievalo z metra na meter, ako sa červený rozheganý autobus dral vozovkou. 
 „Pani, poďte si sadnúť s tým malým!...“ zarapotalo odkiaľsi zozadu k práve došlej mamičke s dieťaťom, ktorá sa postavila k dievčaťu, čo objímala objemnú školskú tašku.. 
 „Ďakujem vám, ale o chvíľu vystupujeme... Aj tak, nejde to dostať sa k vám!“ mladá matka sa bezradne obzerala. Po chvíli by sa aj bola vari chcela pobrať, pretisnúť ďalej, kde sa ozval pozývajúci, ochotný hlas, no potom kapitulovala. Rezignovane sa oprela o šesťdesiatročnú spotenú, sediacu susedu. Rukou sa pridržala sedadla, druhou stískala ruku trojročného dievčatka. 
 Malinké vyvracalo hlávku. To pozeralo na jedného, to na druhého človiečika, medzi ktorými sa tiesnilo. Otváralo dokorán ústočka ani ryba, keď sa nedopatrením ocitne na piesočine. Zadúšalo sa. Keď autobus zastával, vozidlom prebehla vlna, keď sa znovu rozbiehal – opäť. Malinké sa vlnilo s dospelými. Tváričkou mu prebiehalo napätie i kŕče strachu. Rúčka pevne zvierala matkinu pevnú ruku. Druhá mu šibrinkovala vzduchom. Raz sa dotkla nohavíc akéhosi muža, inokedy sa zachytila sedadla, raz sa prichytila akejsi sukne. 
 „Mamiíí... mami...!“ slabučko vydýchlo a lapalo dych. 
 Matka sa netrpezlivo z výšky pozrela nadol a nervózne vyhŕkla: 
 „Vydrž!... Raz-dva sme doma!“ 
 Dievčatko nechápavo i bezradne pozrelo na mať, ale ju ani poriadne nedovidelo, lebo sa do jeho pohľadu vkĺzla hlava šedivého báčika. „Mamiíí...“ opätovne hleslo. 
 Ticho. 
 Vo chvíli, keď sa autobus rozbehol, osadenstvo v ňom sa rozvlnilo opäť a maličké razom zašibrinkovalo voľnou rúčkou o záchranu. Rozhodne sa zachytilo nohy vedľa: ako kliešť. 
 Dievčina s preťažkou aktovkou sa zahniezdila.  Drobná rúčka šteklivo škrtila nohu. Nepokojne ňou zatriasla. Rúčka nepovolila. Ba naopak: nežné šteklenie prestalo a nastalo silné, zúfalé gniavenie. Opäť podrmala nohou. Nič. Zohla sa, že čosi povie. Už aj otvorila ústa, ale hlásku nevydala. 
 Dievčatko ustrašene na ňu vypliešťalo očká. 
 Začervenala sa, ešte raz však zatrepala nohou. Márne. Pršteky dievčatka trhali pančuchou, stískali kožu nohy, neodpútali sa. Hlboko si dievča vzdychlo a stálo ďalej ani hradný múr, aby malej neublížilo. 
 Zástavka. Rúčka sa odtrhla od prepoteného miesta na nohe. Matka brala malú už dolu z autobusu. 
 Dievčina sa uvoľnene vystrela. Očami cez okno hľadala dievčatko, čo práve vystúpilo . Zbadala ho. Obzeralo sa za autobusom, ktorý sa znovu pohýnal. Dievčaťu sa rozjasnila spokojnosťou tvár... 
   

