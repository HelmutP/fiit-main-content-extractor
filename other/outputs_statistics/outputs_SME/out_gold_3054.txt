
 Na prvý jarný deň sa oteplilo. Spod poliatych orchideí, ktoré dlhú zimu šírili bielu nádej, vytiekla mláka. Položil som pod ne prvý papier, ktorý mi padol do ruky. Keď som ho mokrý hádzal do koša, prečítal som si z neho príbeh Imricha Benického.

   „V ôsmom týždni som sa preriekla, že budem mať dieťa. Manžel sa nahneval a rozhodol: „Potrat!“ Lekár mi poradil: „Dajte si to preč, kým to nie je živé.“ Dali ma na izbu, kde už bola iná, čo túžila mať dieťa. Desať rokov sa liečila a – nič. Keď sa dozvedela, prečo som tam, kľakla na kolená pri mojej posteli, zopla ruky a prosila: „Donoste ho a darujte, vyvážim ho zlatom!“
   Skamenela som, mojim vnútrom to zatriaslo. Plakali sme obe v objatí, bez slova som vstala, obliekla sa a odišla domov – rodiť štvrtýkrát.  
   Dnes má moje dieťa päť mesiacov. Zabáva celú rodinu, prinieslo nám šťastie, pokoj nevzalo. Manžel predtým nikdy nechodil domov tak skoro z úradu, či služobnej cesty – kvôli dieťaťu. Deti utekali ozlomkrky zo školy – pomaznať sa s malou. Ústočka mala nastavená na permanentný úsmev, ktorý podtínal kolená. V jej očkách sa trblietalo nebo, sršali z nich iskry radosti a pokoja. Ťažko vyjadriť radosť, že každý deň máme sviatok.  
  Do srdca mi niekedy vniká meč bolesti pri pomyslení, že toto dieťa bolo odsúdené na smrť. Boh mi však poslal anjela, ktorý si kľakol a prosil za neho. Odpusť, Bože, moju vinu, buď pochválený za anjela, aj za drobného anjelika.“  

   Druhý príbeh je mozaikou sprievodného textu medzi 14 prekrásnymi originálnymi piesňami, ktoré aj s textami skomponovala Mária Durcová (10) a Lucia Čerňanská (4). Po Veľkej Noci usporiadame Experiment 3G aj Bratislave.

  Ona a On idú v dave mestom. Ona sa už dlhšie necíti dobre, má akési predtuchy, je mrzutá, nič ju nebaví. Zajtra ide lekárovi, snáď sa to tam vyrieši. Vonku je zima, v kostole sa svieti, vojdú sa zohriať. Mladí spievajú, nič im to už nehovorí, na vieru detstva zabudli, dnes majú iné problémy...  

„Je to jasné milá pani, gratulujem vám, čakáte dieťa. Ste na začiatku tretieho mesiaca, všetko vyzerá v poriadku.“  Šokovaní manželia sa nezmohli ani na reakciu. Lekár urobil všetky testy, výsledky im oznámi. Otázky začali až vonku: „My čakáme dieťa? Ako sa to mohlo stať? Veď vôbec na to nemáme čas!“ 
 
Vzrušená debata a výčitky pokračujú aj doma: „To dieťa nie je naplánované, nemôžeme ho chcieť, robí nám škrt cez všetky rozpočty.“ 
  „Prečo si nedával viac pozor, veď teraz je to úplne nemožné. Toto nám nebolo treba, na decko máme dosť času.“  Obaja dospeli k radikálnemu záveru...    
 
On bol ešte v práci, Ona už doma. Zajtra majú ísť znova lekárovi, vedia prečo. Cítila sa vyhorená a unavená. Zaspala a prisnil sa jej zvláštny sen: z diaľky sa na ňu usmievalo malé dieťa, milučký blonďák, vyzeral ako Malý princ. Prosil o niečo očami a rukami, pomaly sa smutno vzďaľoval...  
 
Netrpezlivo čakala manžela: „Lekárovi nejdeme, dieťa si necháme, na dovolenku pôjdeme na budúci rok aj s ním.“ On šokovaný počúval,nechápal náhlu zmenu. Ona pokračovala: „Už o tom nehovorme a nehádajme sa. Ono nás počuje, isto mu to škodí.“ On stále nerozumel, tváril sa urazene, ale keď pochopil, že sa nejedná o krátku zmenu nálady, objal ju a ich oči sa zaliali slzami...  

  Po krátkom čase pokoja prišla pozvánka k lekárovi. Bol vážny, nesvoj, nedíval sa im do očí: „Veľmi ma to mrzí, ale testy ukázali, že dieťa nebude zdravé. Bude  odkázané na vašu pomoc a aj tak sa dospelosti asi nedožije.“ Obom rodičom sa zastavilo srdce. 
  Ona usedavo plakala, On utrápene mlčal a hrýzol si pery - ich prvorodené dieťa bude postihnuté. Nepýtali sa na detaily a lekár pokračoval: „Odporúčam rýchlu interupciu, čas letí, stačí podpísať žiadosť.“ On sa uprene díval na manželku, ktorá prerušila plač, rukou si pohladila bruško a povedala: „Nie pán doktor, bude to naše dieťa, prijmeme ho také, aké sa narodí.“

  Tehotenstvo prvorodičky nebýva ľahké. Všetci sa vypytujú, ako sa jej darí, musí sa  usmievať a tváriť, že všetko je v poriadku. Tajomstvo však gniavi telo a dušu, život sa skladá z prebdených nocí, bezútešných depresií, aj nekonečných modlitieb.
 
  Deväť mesiacov plných starostí ušlo, dieťa sa pýtalo na svet 22.mája, na sviatok sv. Rity, patrónky beznádejných vecí. Noc bola teplá, hviezdy svietili a z jednej z nich prišiel na svet Malý Princ. Lekár vykríkol, bol to úplne zdravý, blonďavý chlapec. Keď zaplakal, rodičia sa odvážili na neho pozrieť. Mama videla, že je to ten, ktorého stretla vo sne... 
 


 
 
 
