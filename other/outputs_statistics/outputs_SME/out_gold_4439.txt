
 Ale raz napchám kúsky papiera
pod zelené tlačidlá
vo všetkých električkách

nech nemôžeš vystúpiť
keď v nich budem ja
s výrazom psychopata
kresliť srdiečka na okná

zo strechy televízie
do snov ti vysielať
moje astmatické dýchanie
šepot zahlienených hlasiviek.

A potom v noci
keď sa iným obnovujú tkanivá
budem si blížiť
lámať si tebou hlavou zaživa.

Raz kvôli tebe
prepíšem učebnice psychológie
založím umelecký smer.

A odídeme žiť do hôr
ako skupinka bláznov.

Budeme hľadať 
pôvodných obyvateľov

našich zvrátených svetov.
 
