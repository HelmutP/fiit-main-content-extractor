
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Števo Šanta
                                        &gt;
                Nezaradené
                     
                 Cyankáli 

        
            
                                    23.5.2010
            o
            20:55
                        |
            Karma článku:
                10.50
            |
            Prečítané 
            2364-krát
                    
         
     
         
             

                 
                    Stratená chuť do života sa nedá vrátiť pomarančom, minerálkou ani vľúdnym slovom. Darmo sa návštevy pri nemocničnej posteli striedajú a snažia sa. Človek ju musí nájsť sám v sebe. Je ťažké ju hľadať, keď vás operácia, zachraňujúca život, zmenila na invalida a zdravotné prognózy do budúcna sú viac ako pochmúrne.
                 

                 
 www.flickr.com
   "Kde sa dá zohnať cyankáli?"spýta sa matka syna.  "Najskôr u nejakého šperkára. Používajú to na rozpúšťanie zlata."  "Koľko ho treba?"  "Dvesto miligramov je smrteľná dávka."  "Zoženieš to?"  "Zavreli by ma do basy."  "Neprídu na teba."  "Budú to vyšetrovať, vypytovať sa všetkých príbuzných, nakoniec na to prídu. Okrem toho, nechcem, aby si zomrela. Chcem aby si tu bola."  "A načo?"  Potom sa bavia o výlete do Holandska, kde je eutanázia legálna. Matku rozhovor, zdá sa, upokojí, podchvíľou sa usmieva. Díva sa na syna lesklým, dolzinovým, pohľadom.  Syn sa usmieva tiež. Drží matku za ruku. Skúšal to všelijako. Motivačné rozhovory prinášali len opačný efekt - matkinu rezignáciu, príkre dohováranie zasa jej stíchnutie. Až pri rozhovore o možných dobrovoľných smrtiach konečne žena, ktorá ho priviedla na svet, pookriala.   Na druhý deň, cestou do nemocnice, prechádza okolo zlatníctva. Spomalí, na okamih sa pohráva s obskúrnou myšlienkou. Napokon však vojde vedľa do potravín. Kúpi pomaranče a minerálku.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Števo Šanta 
                                        
                                            Octomilky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Števo Šanta 
                                        
                                            Thriller
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Števo Šanta 
                                        
                                            Krátky príbeh o dobrých policajtoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Števo Šanta 
                                        
                                            Supernova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Števo Šanta 
                                        
                                            Adresa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Števo Šanta
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Števo Šanta
            
         
        stevosanta.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    241
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2450
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ahaho
                        
                     
                                     
                        
                            recenzia
                        
                     
                                     
                        
                            glosa
                        
                     
                                     
                        
                            neobeat
                        
                     
                                     
                        
                            songy
                        
                     
                                     
                        
                            filozofia z práčovne
                        
                     
                                     
                        
                            komentár
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            video killed the radio star
                        
                     
                                     
                        
                            rozprávka
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




