

 Občiansky preukaz vydáva a  zápisy do neho vykonáva okresné riaditeľstvo Policajného zboru v obvode, v ktorom  má občan trvalý pobyt. 
 Do preukazu sa okrem mena, adresy a pod. môžu zapísať informácie o krvnej skupine a o závažných chorobách, poprípade vysokoškolský alebo akademický titul. Vždy však treba mať overený doklad, či už od lekára, alebo od vysokej  školy, ktorá titul vydala. 
 O vydanie prvého občianskeho preukazu pri dovŕšení pätnásteho roku je povinný požiadať zákonný zástupca najskôr 90 dní a najneskôr 30 dní pred jeho dovŕšením. 
 Pri žiadosti o vydanie prvého občianskeho preukazu je dôležité predložiť: 
 *   rodný list, doklad o rodnom  čísle, 
 *   potvrdenie o hlásení trvalého pobytu, 
 *   dve fotografie s rozmermi 3 x 3,5 cm. Tvár na fotografii by mala byť bez prikrývky hlavy a okuliarov s tmavými sklami, s výškou tvárovej časti hlavy minimálne 13 mm od brady po temeno. 
 Pri žiadosti o vydanie nového občianskeho preukazu je dôležité predložiť: 
 *   doterajší občiansky preukaz alebo doklad o občianskom preukaze, 
 *   2 fotografie, ktoré spĺňajú predpísané náležitosti, 
 *  zaplatiť správny poplatok. 	 (mip, vju) 
 Kde sa vydávajú OP 
 Občianske preukazy sa vydávajú v rovnakých časoch 
 Pondelok 
 7.30 - 12.00  13.00 - 15.00 
 Streda 
7.30 - 12.00  13.00 - 17.30 
 Piatok 
 7.30 - 12.00 
 Bratislava I.  - Sasinkova 23 
 Bratislava II. - Záhradnícka 93 
 Bratislava III. - Vajnorská 25 
 Bratislava IV. - Sch.-Trnavského 1 
 Bratislava V. - Nám. hraničiarov 1 
 Poplatky 
 3 Vydanie prvého občianskeho preukazu je bezplatné. 
 3 Vydanie občianskeho preukazu (a všetky prípady súvisiace so zmenou údajov v doterajšom občianskom preukaze a  krádež občianskeho preukazu)  50 Sk 
 3 Vydanie občianskeho preukazu za stratený, zničený alebo poškodený  200 Sk 
 3 Vydanie občianskeho preukazu za stratený, zničený alebo poškodený opakovane v priebehu dvoch po sebe nasledujúcich rokov 500 Sk 
   

