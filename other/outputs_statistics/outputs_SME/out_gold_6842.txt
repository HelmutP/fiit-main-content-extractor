

 už nikdy nebudem tým 
 čím som bola 
 každá bolesť 
 každá strata 
 každá zrada 
 ulomili kúsok z môjho JA 
 ako vietor, slnko a voda 
 ktoré drobia skaly na malé kúsky 
 a zvetrávajú celé útesy a pohoria 
 rozbíjajú ich 
 odplavujú 
 odnášajú 
 ničia 
 pomaly 
 pomaličky... 
   

