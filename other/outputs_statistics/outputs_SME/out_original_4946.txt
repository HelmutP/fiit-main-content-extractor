
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Hraško
                                        &gt;
                Príroda
                     
                 Rozdiel je v prístupe 

        
            
                                    22.4.2010
            o
            15:11
                        (upravené
                22.4.2010
                o
                21:12)
                        |
            Karma článku:
                13.99
            |
            Prečítané 
            3241-krát
                    
         
     
         
             

                 
                    Pred pár dňami som sa vrátil z návštevy poľských chránených území v oblasti Bieszczad, kde som navštívil Cisniansko-Wetlinski Park Krajoobrazovy a Bieszczadski Park Narodowy. A tak mi nedá aspoň čiastočne neporovnať situáciu v chránených územiach v Poľsku a na Slovensku.
                 

                      Prvé, čo nám padlo do oka, keď sme sa blížili do cieľa našej cesty, boli veľmi pekné lesy, už na prvý pohľad iné, ako obvykle vidíte u nás, pestré, zjavne dýchajúce životom. Musím povedať, že tak na nás pôsobili počas celého nášho pobytu. Nestratili svoj punc nedotknuteľnosti a krásy ani pri bližšom pohľade. A hoci nám počasie veľmi neprialo, ukázali nám aspoň časť zo svojej krásy.    Mŕtve drevo v poľskom parku slúži nielen ako významný zdroj živín.  Pri našich vychádzkach sme narážali na množstvo stôp rôznych zvierat (majestátne zubry nevynímajúc) svedčiacich o tom, že tamojšie lesy sú nielen krásne, ale i živé.                Pôvodní obyvatelia lesov v poľských parkoch majú prioritu pred ťažbou dreva.   Na každom kroku sme nachádzali u nás tak vzácne bobrie priehrady okrášlené bobrími "palácmi". Jedna ma však obzvlášť prekvapila. Bol som trošku ohúrený a tak keď som ju uvidel, hneď som siahol po fotoaparáte. Pozerajúc do hľadáčika mi na tej fotografii niečo nesedelo. A vtedy mi to došlo a začal som sa smiať. Zaujal ma ten rezbársky výtvor majstrov bobrov a ich úmorná snaha o prehryznutie kmeňov stromov za účelom zaobstarania si potravy pred tuhou zimou. Na tom by nič tak zvláštne nebolo, to predsa robí každý normálny bobor. Ale tento úplne normálny nebol, ten si nakoniec pomohol s motorovou pílou. A keď som si to uvedomil, začal som sa smiať. U nás by im tie stromy lesníci odpílili a speňažili a tam si niekto dal tú námahu, že zobral motorovú pílu a bobrom pomohol zaobstarať si potravu na zimu.    Na území poľských parkov majú bobry zelenú. Stretávali sme sa s ich veľdielami na každom kroku.    Panoramatické fotografie bobrích hrádzí si môžete prezrieť kliknutím na obrázok.    Nič však nie je ideálne. Aj poľské národné parky sa boria s problémami. Jeden taký som si uvedomil, keď sme z diaľky uvideli les, kde pomedzi zdravé stromy bolo popadané množstvo odpílených stromov. Tak to z diaľky pôsobilo. Pripomínalo to u nás známe prerezávky, kedy sa prerieďujú husto nasadené smreky. Až keď sme prišli bližšie, uvedomili sme si, že o žiadnu prerezávku nejde. Tie popadané stromy boli prevažne borovice, ktoré nevydržali váhu ťažkého snehu a zlomili sa ako zápalky. Mnohé boli vplyvom snehu a vetra vyvrátené aj s koreňmi. Čo ma ale prekvapilo najviac, bolo to, že hoci celá kalamita ležala hneď vedľa cesty, nikto s ňou nič nerobil. A keď som sa na to pýtal domáceho lesníka, tak mi povedal, že s tým ani nič robiť neplánujú, že to všetko zostane na prírodu. A vtedy som si uvedomil ten rozdiel v prístupe.    Snehová a vetrová kalamita zostanú v poľských parkoch nespracované. Vytvorí sa tým zásoba "mŕtveho dreva" slúžiaca ako úkryty pre živočíchy, ale aj ako významný zdroj živín nielen pre živočíchy žijúce v lese, ale aj pre samotný les. Vyťažením kalamity by okrem ochudobnenia lesa o živiny a úkryty z popadaných stromov došlo aj k poškodeniu mladých stromčekov - semenáčikov v podraste.      Nedalo mi to a po návrate domov som si sadol za počítač a dal som si zobraziť satelitné snímky (Google Maps) národných parkov, ktoré som mal možnosť navštíviť a porovnal som ich so satelitnými zábermi našich parkov. Rozdiely v prístupe sú evidentné už na prvý pohľad.   V turistickom sprievodcovi som sa dočítal, že Cisniansko-Wetlinski Park Krajoobrazovy zodpovedá úrovňou ochrany našim chráneným krajinným oblastiam, čiže zhruba stupňu ochrany 2 u nás. Druhý stupeň je druhý najnižší z našich piatich stupňov ochrany. Do druhého stupňa ochrany prináležia aj tzv. ochranné pásma našich národných parkov (TANAP, NAPANT a pod.).   Poďme si to teda porovnať. Ako prvé som si náhodne vybral kúsok územia parku Cisniansko-Wetlinski Park Krajoobrazovy, teda úroveň ochrany zodpovedajúcu cca stupňu 2 u nás.    Prihraničné poľské parky sú z pohľadu satelitu len málo poškodené ľudskou činnosťou a ťažbou dreva.   Už na prvý pohľad je jasné, že na mape nevidíte prakticky žiadne holoruby a ani hustú sieť lesných ciest, či takzvaných protipožiarnych pásov.   Pre porovnanie si môžme zobrať satelitné snímky z CHKO Poľana:    Niektoré plochy v CHKO Poľana pripomínajú mesačnú krajinu.   Alebo z ochranného pásma Národného parku Nízke Tatry:  Národný park Nízke Tatry nie je na tom o nič lepšie (skôr horšie).   Tatranský Národný Park (TANAP) postihla drevorubačská kalamita rovnako ako iné parky u nás:      
   Rozdiel v prístupe k ochrane území u nás a v Poľsku vám bude jasný už v hraničnom pásme:     Hoci ide o ten istý horský masív, v pravej časti vidíte takmer nedotknuté lesy na poľskej strane a stopy po holoruboch a cestách na slovenskej strane - v ľavej časti. Hrebeňom vedie štátna hranica.       Napriek tomu, že ochrana chránených území je u nás v žalostnom stave, pripravená zonácia tatranského národného parku z dielne Ministerstva životného prostredia má tento stav ešte výrazne zhoršiť.   Preto chcem týmto poprosiť každého z Vás, ktorému na stave našej prírody záleží, podporte aj Vy svojou účasťou protestnú stanovačku proti navrhovanej zonácii Vysokých Tatier na Námestí SNP v Bratislave v piatok a sobotu, 23 a 24 apríla 2010.   Tí z Vás, ktorí sa nemôžete protestnej akcie zúčastniť osobne, môžete zabrániť zničeniu najstaršieho slovenského národného parku podpisom medzinárodnej petície adresovanej vláde Slovenskej republiky.    Bieszczadska veverička bezradne čakajúca na príchod lepších správ, pre prírodu, zo Slovenska.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Hraško 
                                        
                                            Som z toho jeleň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Hraško 
                                        
                                            Sčítanie obyvateľov 2011 v podozrení.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Hraško 
                                        
                                            Ako sa mi vysávať nechcelo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Hraško 
                                        
                                            Chcete mať domáceho medveďa? Vychovajte si ho!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Hraško 
                                        
                                            Ako sme bobry "lovili"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Hraško
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Hraško
            
         
        milanhrasko.blog.sme.sk (rss)
         
                                     
     
        Tulák túlajúci sa prírodou obťažkaný fotoaparátom.
  


&lt;a href="http://blueboard.cz/anketa_0.php?id=716682"&gt;Anketa&lt;/a&gt; od &lt;a href="http://blueboard.cz/"&gt;BlueBoard.cz&lt;/a&gt;


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    69
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2680
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Príroda
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Ochrana prírody
                        
                     
                                     
                        
                            Fretky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Technika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Karol Kaliský
                                     
                                                                             
                                            Michal Čorný
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                             
                                            Rastislav Jakuš
                                     
                                                                             
                                            Martin Marušic
                                     
                                                                             
                                            Erik Baláž
                                     
                                                                             
                                            Róbert Oružinský
                                     
                                                                             
                                            Ivka Vrablanská
                                     
                                                                             
                                            Juraj Smatana
                                     
                                                                             
                                            Katarína Grichová
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Milan Barlog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            VLK - Lesoochranárske zoskupenie
                                     
                                                                             
                                            Čergov (www.cergov.sk)
                                     
                                                                             
                                            Ticha wilderness
                                     
                                                                             
                                            www.wildlife.sk
                                     
                                                                             
                                            www.vlci.sk - diskusné fórum o prírode
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




