
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kolísek
                                        &gt;
                Súkromné
                     
                 Zázračné byliny – IV. 

        
            
                                    12.4.2010
            o
            20:13
                        (upravené
                13.4.2010
                o
                17:55)
                        |
            Karma článku:
                1.93
            |
            Prečítané 
            329-krát
                    
         
     
         
             

                 
                    Pokračování v popisu účinků byliny NONI - Morinda citrifolia. Poklepete-li 2x na moje foto otevře se můj soukromý blog a zde jsou všechny články které jsem zatím vyprodukoval.
                 

                 
  
       Plody rostliny Morinda citrifolia "NONI" jsou tropickým kulturám známy již několik tisíc let. V poslední době byly představeny široké veřejnosti jako zdravý doplněk výživy. Skutečné působení této rostliny předčilo očekávání a bylo až neuvěřitelné jaké účinky se prokázaly, avšak o teoretické stránce toho, jak tato neobyčejná rostlina vytváří takto pozitivní efekt, se ví pouze minimum. Co přesně se v Noni skrývá, co je zodpovědné za takové pozitivní výsledky a také kterak Noni "funguje", je stále předmětem výzkumu. Začněme s popisem, co se stane s upraveným plodem Noni, když ho člověk pozře. Primární, nejdůležitější složkou v Noni je velká molekula nazývaná proxeronin. Proxeronin se přes váš zažívací trakt dostane do tlustého střeva, odkud pak do jater. Játra slouží jako vlastní zásobárna pro celou řadu esenciálních výživných látek našeho těla. Každé dvě hodiny vypustí játra do krevního oběhu určité množství tohoto proxeroninu, který absorbuje do různých tkání našeho těla. Po té, co se toto stane, se musíme odebrat na molekulární úroveň, abychom pochopili co zde probíhá. Při detailním pozorování můžeme rozpoznat, že proxeronin je velká molekula s atomovou váhou 17.000 atomových jednotek (pro lepší porozumění jaká je to velikost: molekula vody má molekulární váhu pouhých 17 jednotek.) Je to dlouhý řetězec s dvěmi zesílenými konci. Aby se proxeronin mohl přeměnit v xeronin potřebuje pomoc určitého enzymu, nazývající se proxeroninase, kterým tělo disponuje v nadbytečné míře. Proces, který přeměňuje proxeronin na xeronin je velmi komplexní, avšak jednoduše vysvětleno probíhá asi následující: proxeronin se ovine kolem proxeroninase , a proxeroninase propojí oba zakryté konce proxeroninu, přičemž odpojí nyní přebytečný řetězec. Obě propojené, zakryté části vytváří ve spojení se serotoninem xeronin. Xeronin vytvořený z proxeroninu se spojuje s proteiny v našem těle. Jejich pojmenování pochází z řeckého proteios, což znamená "primární" nebo "podstatný" , a podává nám klíč k porozumění jejich významu. Proteiny se skládají z dlouhých řetězců aminokyselin. Existuje 22 aminokyselin, které tělo potřebuje, aby mohl vytvářet různé proteiny. Tyto aminokyseliny se spojuji do specifických řetězců, tak jak je to dáno příslušným DNA. Je to rozdílné pořadí aminokyselin v proteinech , co určuje jejich strukturu. Řetěz se ovíjí kolem sebe sama, seskupuje se a vytváří tak bud'to velmi velký shluk aminokyselin nebo jeden protein. Proteiny mají v těle různé, velmi důležité funkce.   1. Proteiny dávají našim vlasům, pokožce a kostem jejich strukturu. V podstatě to jsou proteiny, které dávají našemu tělu jeho strukturu, až do poslední buňky.  2. Umožňují dopravu chemikálií v rámci i mimo rámec buněk. Proteiny se dostávají skrze buněčné membrány do buněk, přitahují důležité výživné látky a umožňují tak jejich proniknutí do buněk.  3. Proteiny se v těle chovají také jako hormony. Touto svou vlastností mohou v těle koordinovat všechny tělesné procesy.  4. Fungují jako protilátky a podporují tak imunitní systém. Tyto protilátky se vážou na cizorodé vetřelce (jako jsou např. viry) a oslabují je, takže tyto pak mohou být odpuzeny. Vaše tělo neustále vytváří a redukuje chemické sloučeniny, přičemž jsou to právě proteiny, které to umožňují.   Další článek bude pokračování v popisu účinků NONI: Zázračné byliny - V.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Neobvyklá příležitost.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Povinné svícení automobilů a ekologie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Na kolik si ceníte svoje zdraví ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Vytvoření vlastní podprahové audio nahrávky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Zázračné byliny – VII.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kolísek
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kolísek
            
         
        kolisek.blog.sme.sk (rss)
         
                                     
     
        Můj zájem je především zdraví a to jak fyzické tak i psychické. Sestavil jsem unikátní regenerační zařízení a i z vlastní zkušenosti tvrdím, že prokazatelně uzdravuje lidi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    451
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




