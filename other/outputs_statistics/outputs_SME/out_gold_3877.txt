

 Prvé podrobnejšie informácie o výstavbe komplexu Eurovea v Bratislave som zachytil v roku 2008 na konferencii Betonárske dni, ktorú poriada Katedra Betónových Konštrukcií a Mostov (svf - STU) každé dva roky. Práve zo zborníka tejto konferencie som čerpal aj väčšinu informácií. 
 Základová jama rozmerov cca 100 x 400 metrov siahala až do hĺbky 12.5 metra pod okolitý terén, čo predstavovalo vzhľadom na blízkosť Dunaja hĺbku založenia cca 10 metrov pod hladinou podzemnej vody, ktorá spôsobuje množstvo komplikácií pri výstavbe. Napr. zo statického hladiska môže dôjsť k nadvihnutiu budovanej konštrukcie v čase keď ešte nieje vybudovaná dostatočná protiváha voči vztlakovej sile vo forme nadzemných podlaží . 
  
 Riešiť sa to dá čerpaním vody alebo zakotvením základov počas výstavby. 
 Ďalšou nemalou komplikáciou je aj potreba zabezpečenia vodotesnosti stien vystavených tlakovej vode počas celej životnosti konštrukcie. 
 Vodotesnosť podzemných priestorov sa v prípade Eurovei zabezpečila podzemnými stenami hrúbky 0,8 m spojenými so základovou doskou hrubou 1,2 metra bez použitia dodatočnej izolácie. Takáto technológia sa odborne nazýva "biela vaňa" a je veľmi náročná najmä na samotný projekt, kvalitu betónu ako aj na samotnú betonáž. Takto vode vzdoruje len samotný betón, ktorý nesmie byť prestúpený žiadnymi trhlinami širšími ako cca 0.3 mm (ak vzniknú väčšie trhliny je potrebné ich dodatočne utesniť) . 
 Aby sa obmedzil prítok vody do základovej jamy počas výstavby, podzemné steny museli byť zapustené až do nepriepustných ílových zemín a siahajú priemerne do hĺbky 20 až 40 metrov. Časť týchto stien slúži aj ako integrovaná časť protipovodňovej ochrany Bratislavy. 
  
 Po vyťažení cca 8 metrového pásu budúcej podzemnej steny sa do tejto drážky vloží oceľová výstuž po ktorej nasleduje betonáž. Aby nedošlo k zavaleniu vykopanej drážky používa sa tvz. bentonitová suspenzia, ktorá pôsobí voči tlaku zeminy hydrostatickým tlakom. 
   
 Nakoľko debnenie steny tvorí samotná zemina, povrch betónu "prevezme" jej štruktúru a teda po odkopaní takejto steny je potrebné jej povrch zabrúsiť a odstrániť prípadné nedokonalosti. Finálnu úpravu, ktorá sa už veľmi nelíši od hrubého opracovania môžete vidieť v podzemných parkoviskách. 
  
 Jednou z komplikácií výstavby bola aj podmienka zo strany mesta zabezpečiť možnosť budovania budúcej trasy metra, ktorého plánovaná trasa prechádza aj popod jednu časť objektu. 
 Aby sa pri budovaní metra predišlo sťaženému prestupu cez podzemnú železobetónovú stenu je táto stena v týchto miestach namiesto oceľovej výstuže vyarmovaná sklolaminátovou výstužou, ktorej prebúranie je menej náročné. Samozrejme táto výstuž je niekoľkonásobne drahšia ako bežne požívaná oceľová výstuž. 
  
 Video z vkladania tejto výstuže do vykopanej drážky v zemine si môžete pozrieť tu. 
 Aby nedošlo k zavaleniu stavebnej jamy museli byť podzemné steny počas výkopu základovej jamy rozoprené železobetónovým rozperným rámom, ktorý je dnes už súčasťou stropu nad 2. podzemným podlažím. Najskôr sa teda odkopali podzemné steny po úroveň budúceho stropu 2. podzemného podlažia, vybudoval sa rozperný rám a až potom sa mohlo pokračovať vo výkope pod touto úrovňou. Aby sa ale zabezpečilo podopretie tohoto rámu (teda budúceho stropu) musel byť dočasne podopretý oceľovými stĺpmi vyplnenými betónom. Zaujímavé riešenie je aj uloženie týchto dočasných stĺpov na vopred vybudovaných dočasných základoch, ktoré sa budovali tak, že sa dopredu určila ich poloha a zhotovili sa v predstihu ešte z úrovne terénu  v požadovanej hĺbke (podobne ako sa budujú podzemné steny). 
  
  
  
 Spojenie základovej dosky s vopred budovanou obvodovou podzemnou stenou je len v drážke vysekanej v krytí výstuže steny a kotevnou výstužou uloženou do navŕtaných dier. Spojenie základu a stien je tesnené napučiavacími pásmi a injekčnými hadičkami. 
  
 Po dobetónovaní stropnej dosky 2. podzemného podlažia, ktorej súčasťou je aj rozperný rám, bolo potrebné v miestach, kde výťahové šachty a schodiská prechádzajú cez stropnú dosku vyrezať dodatočne otvory , ktoré tam nemohli byť vynechané dopredu, lebo by znížili tuhosť rozperného rámu. Otvory sa po navŕtaní diery rezali pomocou tvz. nekonečného oceľového lana. 
  
 Postup výstavby nadzemných častí sa už veľmi nelíši od bežných stavbárskych praktík aj keď architektonický návrh stropných dosiek, kde sa pravý uhol vyskytoval len výnimočne a hrany väčšinou tvorili všemožné krivky, poriadne potrápili projektantov a aj realizátorov. Nemalou úlohou bolo aj riešenie presklenej zakrivenej 3D strechy, či presklenej vežičky na rohu objektu (podrobnejšie informácie  o ich výstavbe sa mi nepodarilo získať). 
  
  
 Každá stavba má svoje špecifické problémy a riešenia a keby som chcel všetky opísať, pravdepodobne len ich vymenovanie by zabralo zo desať strán textu. Ten kto na stavbe niekedy pracoval vie, že pojem ako pohodová stavba sa vyskytuje len v rozprávkach. 
 Žial keď už je takýto komplex hotový a ľudia sa prechádzajú popred vysvietené výklady obchodov len máloktorí z nich pomyslí na náročnú výstavbu a len málokto vie čo to dalo roboty aby sa návštevníci mohli spokojne premávať popred superznačkové predajne plné superskvelých akciových handier. 
 Zrýchlenú verziu takmer celej výstavby si môžete pozrieť tu. 
   
 Literatúra: 
 Anton Vyskoč: Poznatky z realizácie železobetónových konštrukcií komplexu Eurovea Trade Center Bratislava, in: Zborník konferencie Betonárske dni 2008, str. 71 - 76 

