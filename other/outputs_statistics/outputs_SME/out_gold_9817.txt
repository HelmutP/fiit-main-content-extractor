

 K blogu sme.sk som sa dostala skutočne len vďaka náhode. Celé roky som mala denník sme predplatený, až mi jedného dňa minulý mesiac ráno noviny do schránk neprišli. Myslela som si, že ich možno niekto zo schránky vzal, pretože "nepokradneš" už v dnešnej dobe nemá absolútne nijaký zmysel. 
 Noviny však neprišili ani na ďalší deň, ani celý týždeň. Tak som začala pátrať, čo sa mohlo stať. Zistila som, že mi prišiel ústrižok na platbu, ak mám ďalej záujem noviny odoberať, ale platbu som nezrealizovala, tak mi nové číslo neprišlo. Dcéra mi povedala, že si mám na internete nájsť dáky kontakt do redakcie a zavolať tam, ako treba postupovať. 
 Keď už som sa síce s problémami, ale predsa dostala na stránku sme.sk, tak som sama ani neviem ako, ale dostala som sa na tento blog. Začala som si čítať jednotlivé články. Článok za článkom. Skutočne boli nesmierne zaujímavé, a niektoré aj poučné. Mnohé boli z bežného života, iné písali o politike a samozrejme o povodňach, ktoré zastihli naše Slovensko. 
 Vtedy som si povedala, toto musím skúsiť aj ja. A prešlo síce niekoľko dní, ale som tu, a je podstané. Konečne po dlhej dobe čo som si sľúbila, to som aj splnila. 
 Teraz len dúfam, že môj zájem o písanie bude dlhodobý, a že o články bude záujem. 

