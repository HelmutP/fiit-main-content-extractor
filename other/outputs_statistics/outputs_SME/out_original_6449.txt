
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Javurek
                                        &gt;
                Fotografie
                     
                 Le Chat Noir 

        
            
                                    13.10.2005
            o
            22:20
                        |
            Karma článku:
                10.53
            |
            Prečítané 
            9873-krát
                    
         
     
         
             

                 
                    Titulok som si požičal z francúzštiny, ale znamená to len toľko,  že som dnes mal šťastie na zaujímavé stretnutie.  
                 

                Poverčiví ľudia by možno stretnutie s čiernou mačkou pokladali za nešťastie, ja som sa potešil. Čierna mačka sa dosť ťažko fotografuje, ale vyberať som si nemohol. Nastupuje na scénu.      
          Zdalo sa, že je prítulná, aj keď sme boli v extraviláne obce, chovala sa ako mačka domáca a ešte k tomu dobre vychovaná.       
          Namiesto pozdravu zažmurkala a ukázala svoj biely podbradník. Proste, krásavica...        
          Neviem, ako sa volá, tak ju budem volať Micka. Micka chvíľu postála, zapózovala s otvorenými kukadlami.        
          O chvíľu jej pozornosť čosi zaujalo. Nevyzerá, že by hladovala, ale taká malá myška by iste dobre padla pred domácou večerou. Mal som pravdu, dravec sa nezaprie, odchádza na lov.       
          Naozaj odchádza, kýva mi labkou a už ukazuje len svoje čierne pozadie.        
      Tento príbeh sa skutočne stal na prechádzke prírodou.       Príroda môže mať rôzne podoby, môže to byť aj rovina ako dlaň, pokosené polia, oráčina, či zasiate oziminy. Aj na rovine sa nájde občas inšpirácia, vhodné objekty pre umenie. Keby sa tým poľom prechádzal Vincent, možno by namaľoval ďalší slávny obraz s názvom: Zátišie po bitke s tekvicami...     
          Nemôžem za to, že tento kvietok vyzerá ako zvädnutý čínsky dáždnik, priznám sa, že neviem ako sa volá, ale tak sa odlišoval od svojho okolia, že som neodolal.        
          Deň sa končí, slnko zapadá, pre ďalší nádherný deň na planéte Zem.         
     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Vieme prví, len či pravdu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            November
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Moja prvá fantastika
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Papierové mestá
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Spisovatelia v Smoleniciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Javurek
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Javurek
            
         
        jozefjavurek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Neoznačené fotografie na blogu výlučne z vlastnej tvorby (c). Inak je všetko vo hviezdach...
 
"štandardný už tradičný bloger, s istou mierou poctivosti" (jeden čitateľ)
 
 


Click for "Your New Digest 1".
Powered by RSS Feed Informer

počet návštev:







 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    650
                
                
                    Celková karma
                    
                                                4.59
                    
                
                
                    Priemerná čítanosť
                    2950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Český raj
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Myšlienky, názory
                        
                     
                                     
                        
                            Blogovanie
                        
                     
                                     
                        
                            Technika a technológia
                        
                     
                                     
                        
                            Domácnosť
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            EÚ
                        
                     
                                     
                        
                            Hory, lesy
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




