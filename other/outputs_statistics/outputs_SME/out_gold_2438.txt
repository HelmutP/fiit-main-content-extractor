

 Preambula. 
  S úctou pred nepoznaným, nepochopeným a odlišným, s odvahou odkrývať svoje možnosti a rozvíjať svoje danosti,  s ambíciou napĺňať zmysel života, v duchu zásad liberalizmu a slobody, hľadajúc skutočné hodnoty a hodnotné ciele,  v dobrej viere a z vlastnej vôle uznášam sa na tejto osobnej ústave. 
  Hlava I.  Základné ustanovenia. 
  Článok 1 
 Najvyššou hodnotou, ktorú som povinný aktívne chrániť a na základe ktorej konám a z ktorej vychádzam pri rozhodovaní je sloboda; osobitne sloboda jednotlivca. 
  Článok 2 
 Ideálom mojej cesty životom je harmónia alebo napĺňanie harmónie. Obsahom harmónie je súbežné dosiahnutie stability, bezpečnosti, prosperity a zmyslu života. 
 ... 
 Hlava II. Sloboda. 
  Článok 6 
 Cestou k slobode je získanie kontroly nad svojim telom a vôľou. Človek, ktorý neovláda svoje telo alebo vôľu nie je slobodný. 
  Článok 7 
 Slobodný človek má v rukách všetky prostriedky, aby naplnil zmysel svojho života a je si tejto skutočnosti vedomý, verí jej a na základe toho aj koná. 
  Článok 8 
 Neoddeliteľnou zložkou slobody je zodpovednosť za svoje konanie a nekonanie. Zodpovednosť sa prejavuje najmä  schopnosťou plniť svoje sľuby a záväzky a v prípade ich neplnenia niesť následky. 
  Článok 9 
 Vonkajším prejavom slobody je rešpekt k názorom a konaniu iných, pokiaľ tieto nepredstavujú ohrozenie vlastnej slobody alebo slobody iného. 
  Článok 10 
 Sloboda sa prejavuje v rozhodnutiach. Ten, kto nerozhoduje alebo sa rozhoduje nedobrovoľne alebo sa nedobrovoľne podriaďuje rozhodnutiam iných, alebo pri rozhodovaní nemá možnosť výberu nekoná slobodne. 
  Článok 11 
 Nesiem svoj podiel zodpovednosti za celospoločenské presadzovanie slobody a odstraňovanie neslobody. 
  Článok 12 
 Nevyhnutnou súčasťou obsahu slobody je ochrana života, zdravia a nedotknuteľnosti vlastníctva. 
  Článok 13 
 Pracovnou činnosťou, ktorá neodporuje zásadám slobody je súkromné podnikanie, slobodné povolanie alebo verejná funkcia získaná voľbou. Zamestnanecký pomer možno prijať len dočasne a len vtedy, ak je to nevyhnutné z titulu získavania kvalifikácie, alebo z existenčných dôvodov prípadne iných vážnych dôvodov. Zamestnanecký pomer so štátom, a to aj dočasný, sa až na osobitné prípady vylučuje. 
  Hlava III. Harmónia. ... 
 Časť 3d  Zmysel života. 
  Článok 24 
 Kľúčom k žitiu zmyslu života je objavenie rozmernosti lásky a splynutie s ňou. ... 

