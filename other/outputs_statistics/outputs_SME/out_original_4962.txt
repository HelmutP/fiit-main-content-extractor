
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Šipikal
                                        &gt;
                Nezaradené
                     
                 Možno vieme, prečo papyrus, ani kameň, nič nehovoria o Mojžišovi. 

        
            
                                    21.4.2010
            o
            8:03
                        (upravené
                23.4.2010
                o
                11:34)
                        |
            Karma článku:
                6.57
            |
            Prečítané 
            1540-krát
                    
         
     
         
             

                 
                    Mojžiš je jedna z najznámejších postáv Starého zákona. Podstatná časť jeho života, ako to poznáme z Biblie, je úzko spätá so starovekým Egyptom. Egyptom faraónov.
                 

                     Niekoľko tisícročí žiariaca sláva starovekého Egypta nezvratne zhasína smrťou Kleopatry roku 30. pred n.l., kedy stráca nezávislosť. Náboženská reforma nariadená Theodosiusom I. roku 383 n.l. je už len pomysleným posledným klincom do rakvy. Piesok a čas pochovali na celé stáročia nielen mnohé udivujúce umelecké, staviteľské či písomné diela, ale aj diela veľkoleposti ducha, tradícií a náboženstva tejto jedinečnej a pozoruhodnej civilizácie. Až o celé stáročia neskôr nastupujúca "veda lopaty" archeológia, opäť krôčik po krôčiku oživovala stratenú kultúru a prebudila nebývalý záujem svetovej verejnosti. Bádanie sa podstatne uľahčilo vďaka zakladateľovi egyptológie, slávnemu J.F.Champolionovi, ktorý v roku 1822 dešifroval hieroglyfy. Do toho času, najmä počas stredoveku, bola jediným relevantným zdrojom informácií o Egypte faraónov - Biblia.  No paradoxne,  v priebehu 18. a 19. storočia n.l., stáva sa Biblia terčom útokov a spochybňovania, hlavne zo strany tzv. "Vyššej kritiky".   Neunikol jej ani Mojžiš.       Podľa Biblie sa Mojžiš narodil počas útlaku Izraelitov v Egypte, vyrastal a bol vzdelávaný v domácnosti faraóna. V dospelosti za dramatických okolností utiekol do krajiny Midián. Neskôr sa mu v horiacom kre zjavil Boh a poveril ho vyslobodením izraelského národa z otroctva. Stáva sa sprostredkovateľom Zákona, zmluvy medzi Bohom a Izraelom. Sú však tieto biblické záznamy dôveryhodné, historicky presné? Kritici ešte aj dnes najviac poukazujú, že od faraónov niet žiadnych písomných záznamov o Mojžišovi. Ale táto námietka neobstojí. Prečo? Pravdu faraóni nemali vždy radi, najmä, ak ich stavala do nepriaznivého svetla. Vojtech Zamarovský v knihe "Ich veličenstvá pyramídy" píše : "Egyptskí králi sa totiž považovali za bohov, a nemohli sa preto nikdy a v ničom stretnúť s neúspechom. V všetkých vojnách teda nevyhnutne zvíťazili, v každej diplomatickej akcii zožali úspech, boli úplne neomylní, vynikali nad všetkými mocou, hrdinstvom, bohatstvom atď. Ich dvorní historici vedeli, čo a ako majú písať. Porážky menili na víťazstvá alebo aspoň zamlčovali ..." História nám odkryla konkrétne príklady. Je známe, že Ramzes II. vynikal vystatovačnosťou. Niekoľkokrát prehral vojnu s Chetitmi, ale jeho historici zapísali pravý opak. S faktami manipuloval aj Thutmose III. Predošlú kráľovnú, jej podobizeň a nápisy dal jednoducho vysekať zo stien. Rany, ktoré dopadli na Egypt prostredníctvom Mojžiša, mali pre jeho spoločenskú, hospodársku, mocenskú no najmä náboženskú oblasť, katastrofálne následky. Neporovnateľné s ničím v dejinách staroegyptskej ríše. V kontexte týchto faktov, zdá sa ako rozumné uvažovať, že aj v tomto prípade mocenské a ideologické štruktúry "zariadili", aby pravda o Mojžišovi a udalostiach tých dní zanikla kdesi v prúde času ... A predsa!  Exodus zanechal takú silnú stopu, že o viac než tisíc rokov neskôr sa mohol egyptský historik a kňaz Manéthó opierať o určité skomolené tradície, keď písal o národe pastierov, ktorí boli vyhnaní z Egypta, vyšli zo svojími rodinami, stádami a založili si v Judei mesto Jeruzalem. Podľa prác židovského historika J. Flávia, Manéthó stotožňuje Mojžiša s egyptským kňazom Osarsifom a ukazuje, že predkovia Židov v Egypte boli, a Mojžiš bol ich vodcom. Flávius spomína aj iného egyptského pisateľa, Chaeremona. Ten napísal, že Mojžiš a Jozef boli vyhnaní z Egypta v rovnakej dobe. Nie je to pozoruhodné? Nie sú to možno ozveny umlčanej pravdy z dôb dávnych vekov?        Na prelome 70. a 80. rokov minulého storočia spôsobil vo vedeckých kruhoch rozruch archeológ-amatér, cestovateľ a dobrodruh Ronald Wyatt. Zverejnil výsledky svojich výskumov v Červenom mori a Saudskej Arábii. Dospel k záveru, že objavil pravú horu Sinaj a našiel presné miesto prechodu izraelitov cez more, čo doložil veľkým počtom fotografií a videozáznamov. Aj keď je Wyatt považovaný za kontroverznú osobnosť, jeho prínos v biblickej archeológii a čiastočne aj egyptológii je nespochybniteľný. Keď už nič iné, jeho namáhavé cesty,  bádanie, spopularizovali Bibliu ešte viac. Boli silným podnetom na potrebu vedenia dialógu medzi neveriacimi a veriacimi bádateľmi.       Verme, že nám "veda lopaty" prinesie ešte veľa prekvapení z dôb Egypta faraónov. Aj tých biblických. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Carnarvon: "Vidíte niečo?". Carter : "Ano. Nádherné veci!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Tajnostkári starí egypťania.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Lišiacky človek, skvelý dejepisec. Josephus Flávius.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Šipikal
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Šipikal
            
         
        sipikal.blog.sme.sk (rss)
         
                                     
     
        Niečo o mne? Rád si vypočujem Shirley Bassey, Sarah Brightman, Robbieho Williamsa, či Barbru Streisand. Mám rád fitness v teórii i praxi, zaujímam sa o všetko, čo súvisí so starovekým Egyptom a biblickou archeológiou. Milujem atmosféru 20.-30. rokov 20.storočia so všetkým tým rozbiehajúcim sa pokrokom a nefalšovanou zdvorilosťou. Relaxom je pre mňa príroda, rodina, šport a spoločnosť priateľov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1321
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




