

 
Na Orave bolo dobre. Podnetne. Samé dobré pocity, lebo to bola inventúra pre starosti, behanie po lúke, keď sa mieša deň 
 
 
 
 
 
s večerom, 
 
 
 
 
 
behanie po vode, 
 
 
 
 

 
mlynské kolá a stojky na hlave, jeho mama, čo hovorila, že sa stále len smejeme, že ak chcem robiť tú psychoterapiu, mala by som ešte dozrieť, lebo vyzerám na 17 a ja na to, že som to už dlho nepočula, a potom telefonovala, že som zlatá, bolo dobre a akoby som bola ich. Volala ma Bilinka, aj sestra ma tak volala, bude mať 13 a vždy hovorila to je trápne, ty si trápny. V izbe, kde som spala, mala ICQ a pýtala sa, či ešte môže ostať. Mohla a ja som sa pýtala, ako to je s tým jej opäť-sa-s-ňou-dajúcim-dokopy frajerom. 
 
 
Potom sa mi chcelo samej sebe zahlásiť: ako dobre, že mi má kto poniesť ten trápne ťažký kufor. 
 
 
Na lúke boli kvety, ktorým som nevedela prísť na meno. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Cestou do tej lúky som hovorila, že nám je dobre tak, ako nám je, len si to treba uvedomiť, no dnes si tým už nie som taká istá. 
 
 
Slávime Turíce a ja chcem toho, čo sa tak rád zosiela, aby prišiel, očakávala som ho, bola som plná očakávania a túžila, aby prišiel, potom som sledovala ten marast vo mne bez toho, aby som chcela zasiahnúť, prestaň toľko bojovať a nechaj sa obdarovať. Nechtiac som si vypočula niečo, čo som počuť nemala, to preto, že hovorili nahlas a ja som bola blízko. Niečo som sa dozvedela. O tom dievčati. Niekoľkokrát som ju videla s tým, ktorého celkom dobre poznám, chcela som mu povedať niečo ako: dávaj pozor, neublíž jej. Ale čo by som mu povedala? Že prečo? Lebo som niečo počula? Lebo som niečo videla, no musím byť ticho? 
 
 
Videla som takého človeka, čo sa úpenlivo pozeral iným do očí, ako vtedy, ak sa vrátim na tú lúku, do ktorej sme kráčali a ja som hovorila o takom druhu pohľadu, keď sa človeku zadívam do očí a uvedomím si, že pozerám na človeka, v tej chvíli sa konfrontujem s jeho človečenstvom a jeho očami, pozerám mu do nich a on to v tej chvíli vie, vie, že ja viem a je to ako uvedomenie si uvedomenia. 
 
 
Rozmýšľam, kto je v tejto chvíli najšťastnejší človek na svete. 
 
 
Sedíme a (len mne sa zdá?), že k sebe hľadáme stopy? Rozpadnú sa? Akoby v tej chvíli boli naše samoty nespojiteľné, akoby sa samoty nikdy nedali spojiť a z ničoho naozaj nemohlo vzniknúť niečo, akoby sme najprv museli obaja, no každý za seba-byť, aby sme mohli opäť byť spolu. V jeden večer som si hovorila tak, ako som raz čítala v knihe: nebola so mnou reč ani mlčanie. To je taký pocit, keď veľmi chcem, aby so mnou niekto bol, no neznesiem žiadnu jeho potechu. Ako pms, no vtedy to malo k nemu poriadne ďaleko. Odchádzal a povedal niečo také vážne až mi to bolo vtipné, možno som sa smiala a smiať sa nemala, a keď odišiel, napadlo mi, že ešte vznešenejšiu cestu vám ukážem, toto ma k tomu napadlo, keď hovoril o pravde, keď sme sedeli na Hviezdoslavovom námestí a bolo cítiť rôzne vône, keď som tam spomínala na rôzne veci, jahodovú zmrzlinu a potom prišli oni a bolo to napäté, prišli tí, ktorí mi už dlhú dobu chýbali a ja som na nich často myslela, že boli a už veľmi nie sú, boli sme tam štyria, no v takej zostave to nebolo dobre.
 
 
  Mala som také predstavy, polosny, polopredstavy, keď som poobede vysilená zaspávala, že sa hojdám na našom dvore. Hojdám sa a hojdám, až do neba, hojdačka visí z oblakov a potom zaspím. Ako snová terapia pre vystresované telo. Učíme sa na štátnice, máme z toho depky a potom už ani nie. Sedíme na balkóne, je nám dobre a myslíme na zmrzliny, kaviarne, ja na ľahké šaty a očarujúci úsmev. Niekto by ma chytil za ruku a vážil by si ma. Sedela by som v mierne teplom júli v prímorskom francúzskom mestečku na terase kaviarne, na rannej káve a dezerte Crème brûllée, bol by pri mne niekto a staral by sa o mňa, bol by veľký a ja maličká, chcel by ma na celý život a ja by som tomu verila. A ja by som tomu verila. Nebola by samota, len taká, po ktorej by som túžila, a nikto by ma nevysmial z toľkej patetickosti, lebo by bola moja a úprimná. 
 
 
Ale vrátim sa do reality a myslíme na iné veci. Že po štátniciach sa tam možno vrátim, na lúky a Roháče, čo sa zatiaľ roztopia, bude sa plávať, spať a vstávať a pri chutnaní brusnicového kompótu za stolom možno poviem nie, ja nie som nezrelá, to len tak vyzerám, všetko je len klam. A bude tam moja duálna duša a ja. 
 
 
Dnes mi je celkom dobre a pokojne. S občasnými nie-až-do-stredu-duše nahlodavajúcimi strachmi. Myslím na pojem dokonalý motív. Čo to je? Som ako v konzerve, sterilizovaná, sústrediaca sa na učenie a školu, iné pocity sú v úzadí, aby sa dalo s nimi žiť. 
 
 
                                                                              
 


