
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Prehral aj Budapešť 

        
            
                                    13.6.2010
            o
            13:44
                        |
            Karma článku:
                7.93
            |
            Prečítané 
            1435-krát
                    
         
     
         
             

                 
                      Naozaj to nie je tak dávno, čo som svoju pozornosť venoval pôsobeniu politika s celkom zvláštnym potenciálom možností, ktorému som venoval prívlastok pyšný Maďar - Pálovi Csákymu. Predsedovi politickej strany SMK, na čelo ktorej sa postavil po vnútornom súperení po jej dlhoročnom protagonistovi Bélovi Bugárovi a svoj vtedy eskalujúci tlak premenil na niečo permanentné, čo dnes charakterizujeme ako dokonané. Bugár odišiel spolu s niekoľkými najbližšími a rovnako zmýšľajúcimi spolupracovníkmi, jeho charizma spôsobila, že mu spontánne vyjadrila podporu široká verejnosť nielen v „maďarskom životnom priestore menšinových záujmov“, ale aj vôbec sa nepresadzujúca slovenská inteligencia z OKS, ktorá to v parlamentarizme jednoducho nevie a nepresadila sa. Vtedy vznikol MOST – HID, niečo na spôsob hybridu, niečoho doteraz nedefinovaného, o čom Csáky s pohŕdaním hovoril ako o strane nemaďarskej a rovnaký názor získali aj v Budapešti, predovšetkým vláda s presnou adresou Fidesz!
                 

                     A to veru nemali robiť! MOST – HID je v parlamente a Csáky s SMK zostal pred jeho bránami so zármutkom v duši, sklamaním v srdci a s výčitkami v myslení – prečo?   Už som počul naozaj veľa zdôvodnení, hľadali sa príčiny naozaj vo všetkých peripetiách života maďarskej menšiny na Slovensku, nezabúdalo sa ani na zástoj Maďarska, ako tútora mnohého, s čím „naši Maďari“, ako týchto príslušníkov menšiny z juhu Slovenska voláme, prichádzajú ako so svojou agendou a tak trošku veci doplním o zástoj kvality osobností, čo tieto politické združenia vedú.   Béla Bugár, človek ústretový, premýšľajúci a skromný, v každom prípade však názory iných priebežne analyzuje a hodnotí, ba aj akceptuje, hoci to pre politika menšiny s jej internými problémami naozaj nebýva iba ľahké, naviac, prednostne ho láka stále rastúci komfort života na Slovensku, aj pre menšinu, než pofidérne tradície „starej vlasti“, kde sa opakovane objavujú ambície vzkriesiť sny o starej a zašlej sláve Uhorska s Maďarmi na čele!   Tým druhým je Pál Csáky, človek z druhého sledu, svojim spôsobom reprezentant tých, ktorých myslenie a túžby sa nedajú interpretovať nahlas, lebo sú v rozpore s našim právnym poriadkom a so stabilitou štátu v celom rozmere jeho jestvovania. To si však málokto z nich pripúšťa verejne, kdeže, celý svet to však vie a verejnosť z územia obývaného obyvateľstvom s maďarskou národnosťou s tým bojuje vlastne nepretržite. Viete, vždy je to tak, kde sa obyčajní ľudia k sebe správajú prirodzene, ako susedia, ako priatelia, tam si ten rozdiel v niečom tak málo rozhodujúcom, ako je jazyková výbava, prízvuk, či dedko v Maďarsku a babka zasa v Žiline ani neuvedomujú, oceňujú ľudské kvality. To sa však členom jedinej maďarskej strany na Slovensku, SMK, do výbavy nehodí, lebo oni sú predsa len echt pravou hodnotou a naviac, vláda v Maďarsku je na ich strane! Sú jej predĺženou rukou na tomto území a práve teraz je tu chvíľka, keď sa môžu stať jazýčkom na tej iluzórnej váhe parity názorov. No stalo sa, pýcha predišla pád a SMK, Fidesz a budapeštianska vláda dostali po nose. Parlamentná brána sa zabuchla, za ňu vkročili iba Bugárovci, ak môžem použiť familiárnu skratku a my ostatní budeme čakať, čo bude nasledovať.   Voľačo už viem, iné si poďme domyslieť vo vlastnej réžii. Budapešť to všetko akceptuje celkom spomalene, neverí, ani s opakovaným potvrdením výsledkov a podľa spravodajcu od nás, pána Papuczeka viem, že to asi nebude jednoduchá dilema – v žiadnom smere! Venovali strane SMK dôveru, náklonnosť, podporili ju v každom ohľade a viac ako naznačovali, že je to všetko spoločné dielo na oboch stranách Dunaja. A naopak, tým druhým sa ušlo iba odmietanie a pohana, znevažovanie a naraz je MOST – HID v slovenskom parlamente, má solídny štatút možnej koaličnej strany a celkom iste toľko hrdosti a skúseností, že sa v ničom nestane vazalom mocných v Maďarsku.   Pád HZDS som predvídal, písal som o tom viackrát, no odchod SMK z relevantného politického života Slovenska ma potešil. Tým ľuďom pechoriacim sa v mene neuskutočniteľných snov o „veľkých časoch ich predkov“, síce skrytých a utajovaných, som neveril, nepáčili sa mi, neveril som im, bridila sa mi ich servilnosť a bezbrehý obdiv k sníčkom strany Fidesz v Maďarsku a nevedel som pochopiť ani to, ako sa správajú k svojim vlastným, ak nezdieľajú rovnaké postoje, Teraz však už vedia, poznajú odpoveď na otázku prečo? Maďarskí voliči u nás im dali jasnú odpoveď – my sme vám to spočítali, je to účet vašej pýchy, nereálnosti a je v tom samozrejme aj naša túžba žiť presne tak, ako celý tento národ – sme Slováci s maďarským pôvodom, s maďarskou národnosťou a žiadna piata kolóna kohokoľvek, čo chce oprášiť dávno zabudnuté maniere vecí, ktoré nás vôbec nectia. Sme obyčajný slovenský národ, my všetci spolu!   A teraz finále! Už dnes sa Pál Csáky a jeho generalita rozhodli bojové pole vypratať podľa všetkých pravidiel korektného rytierskeho bontónu – prehral si, odíď a ako jeden muž abdikovali. Otázka, čo sa stane s touto stranou v budúcnosti, aké pokračovanie bude nasledovať v osudoch jednotlivcov i celej komunity sa dá iba hádať. Keďže Bugárov MOST – HID nie je podľa nich maďarskou stranou, slovenská politická scéna sa celkom ľahko môže stať priestorom pre dobrovoľné siroty, čo v tomto ponímaní naozaj nie je dobrá perspektíva, no nebudem strašiť! Som si istý, že Bugár tých  slušných celkom rád a prirodzene adoptuje, presne podľa zásady, že dobrých sa všade zmestí veľa. Celkom nakoniec, návrh posledného slova pre Pála Csákyho.   Prehrali sme, odchádzame, Paroubek prehral, odišiel a prehral aj Fico, no ešte stále sa tu pechorí! To naozaj nevie, ako sa má zachovať gentleman, to je naozaj iba rytierom smutnej postavy. Všetko vyzerá naozaj tak, že už mu treba len tie veterné mlyny, lebo bez nich už hrdinské skutky nebudú! A aby som nezabudol – ešte je tu Vladimír Mečiar a jeho prehra. Iba tipujem – má síce svoju hrdosť, naozaj nevie prehrávať, ale predsa len, hlupákom nikdy nebol! Rytier smutnej postavy nám na Slovensku stačí iba jeden, fandím mu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




