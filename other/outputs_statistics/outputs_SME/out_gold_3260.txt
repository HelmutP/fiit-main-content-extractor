

 
 
 Koaliční predstavitelia si ukrajujú podstatne väčší časový priestor, vďaka svojej razantnejšej rétorike. Divák má v určitých momentoch pocit, že ich oponenti si išli posedieť medzi divákov. Odstopoval som časový priestor, ktorý si dokázali pre seba ukrojiť jednotliví protagonisti v celkovo šiestich televíznych dueloch v troch rôznych televíziách za posledné dva mesiace. Sústredil som sa na lídrov politických strán a popredných predstaviteľov Smeru. Výsledky sú nasledujúce : 
   
 Na telo 21.3.2010 Radičová (9 min, 51 sek) x Tomanová (13 min, 55 sek) 41% x 59% 
 Na telo 14.3.2010 Figeľ (6 min, 9 sek) x Paška (18 min 25 sek) 25% x 75% 
 De facto 12.3.2010 Radičová (11 min) x Mečiar (17 min 25 sek) 39% x 61% 
 De facto 5.3.2010 Sulík (9 min 40 sek) x Belousová (13min, 31 sek) 42% x 58% 
 O 5 minút 12 28.2.2010 Radičová (18 min, 24 sek) x Kaliňák (22 min, 40 sek) 45% x 55% 
 O 5 minút 12 7.2.2010 Figeľ (36 min) x Fico (44min, 45 sek) 45% x 55% 
   
 Celkovo pravica x koalícia 39,5% x 60,5% 
   
 Ako tak pravica obstála iba v dueloch na STV, ktoré však trvajú podstatne dlhšie. Pritom pravicoví lídri nevedia diskutovať nielen so Smerom, ale ako ukázala Radičová v De facto, ani s Mečiarom, ktorého vystupovanie je čím ďalej, tým viac, smiešno - trápne. Sulík si svoju premiéru proti Belousovej tiež určite predstavoval inak. Ďaleko najhorší výkon podal Figeľ v Na telo, kde ho jeho súper Paška prakticky ani nenechal povedať tri súvislé vety za sebou a ukrojil si tri štvrtiny priestoru. Figeľ sa zmohol len na sťažnosti u moderátora. Ten to rozhodne nemal jednoduché. Predstavitelia koalície sú oveľa agresívnejší, razantnejší, dobre artikulujú a často si berú slovo mimo poradia, nereagujú na moderátora, nerešpektujú nepísané pravidlá korektnej diskusie, často si témy určujú sami. Netrápia sa pravdivosťou svojej argumentácie, nie je pre nich dôležitá kvalita, ale kvantita argumentov. Navyše moderátor väčšinou nie je schopný postrehnúť nepravdivý argument. 
 
 
 Už teraz je jasné, že ak sa má vymeniť po voľbách vládna garnitúra, pravicové strany budú musieť osloviť do volieb aj časť nevoličov. A najlepším prostriedkom sú televízne duely, ktoré budú v nasledujúcich týždňoch lámať svoje rekordy sledovanosti. Zatiaľ to však vyzerá, že v nich bude mať navrch súčasná koalícia.  
 
 

