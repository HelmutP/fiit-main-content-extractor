

Tento môj hore uvedený predpoklad začínal mať trhliny krátko po začatí rekonštrukcie domu, pokračoval výsledkami volieb a žiaľ naďalej sa upevňuje, keďže rekonštrukcia naďalej trvá, čím trhlina sa zmenila na faktické konštatovanie, že ak xeš stretnúť veľa debilov začni rekonštruovať dom, z čoho vyúsťujú aj ďalšie konzekvencie. Fakt som si totiž nemyslel, že na Slovensku je toľko debilov. Z titulu môjho zamestnania som prichádzal do styku s istou skupinou ľudí a nesprávne som si vyhodnotil, že oni predstavujú relatívne objektívna vzorku spoločnosti. Žiaľ, dnes som okolnosťami nútený konštatovať, že som sa hlboko mýlil. Hoc generalizovanie často krát vedie k mylným záverom, dovolím si na základe výsledkov volieb roku 2006, kontinuálne stvrdzovaných výsledkami prieskumov verejnej mienky a mojimi skromnými skúsenosťami pri rekonštrukcii domu skonštatovať, že možno až 40 – 50 % Slovákov, ktorí volia, tzn. v absolútnom čísle až cca. 20 % populácie, teda 1 milión Slovákov sú debili, zároveň sú však aj voličmi.    Ľudia, ktorí sú leniví, neschopní, najradšej by sa motali, nerobili, vyhovárali sa na kapitalistov, sem tam aj velebili socializmus a volili Fica. Veľmi skromná priemerka ľudí na mojom dome. Priemerne 10 robotníkov, z ktorých je 8 kokotov, ale fakt kokotov. Napr. ráno sa dohodneme: páni, takže zásuvky budú v rovnakej výške od podlahy ako tá prvá v kuchyni. Jasné? Blahosklonný úsmev pánov robotníkov, že to je „kua úplne samozrejme, no ne.“ Večer prídete skontrolovať čo majstri robotníci urobili a oni vysmiati, že hotovo. Už pri zbežnom pohľade zistíte, že niekoľko zásuvok sa od východzej výškovo odlišuje o minimálne 10 cm. Na dotaz majstrovi pánovi robotníkovi, že sme predsa dohodli niečo iné, na čo on ráno prikývol, že „nema problema“, tento robotník – v podstate pán na Vašej stavbe, kývne plecami a povie, že „šak še prerobí“. To, že „robí“ o deň dlhšie a poškodí pri tom parkety považuje za úplne normálne. Obkladač kachličiek sa poháda s architektkou, že on ma 30ročnú prax a vie ako sa to robí a nech mu do toho panička nehovorí a že „to co chce ona še nedá tak urobic“ je takisto normálne.  Keď mu tie kachličky architektka vyhádže a poukladá ako to mal urobiť pán majster robotník, dokáže tohto voliča Fica rozhádzať panička do tej miery, že na druhý deň proste nepríde do práce a dokončiť to nepríde ani na tretí deň a nakoniec nepríde už vôbec, lebo „on môže jebať takých premúdrelých“. V ďalšom pokračovaní príkladov ktoré sa udiali pri rekonštrukcii domu nateraz by som si radšej nepokračoval, keďže počet vulgarizmov by sa len zvyšoval, čo by mohlo vyústiť, že článok je nezverejniteľný. Takže ako to teda je s voličmi pána Fica? S výnimkou ľudí, ktorí ho volili z čisto pragmatických a často pre seba racionálnych dôvodov, väčšinou však zištných sú jeho voličmi „úplne obyčajní ľudia“.  Týchto ľudí stretnete najmä na stavbe, v obchode, na úrade ale aj inde. Spoločnými znakmi týchto „úplne obyčajných ľudí“ sú najmä, nie však iba: lenivosť, neschopnosť, výhovorky ako sa niečo nedá, aké je to komplikované, ako by mali zarábať viac a nezarábajú, nechuť k poctivej, korektnej práci a dodržiavaniu termínov, veľké reči a malé skutky.   Takýto ľudia potrebujú niekoho ako je pán Fico. Tento v svojej podstate veľmi schopný a chytrý chlapík dokázal zdanlivo jednoduché (čo však neznamená, že nie geniálne), dokázal presvedčiť týchto úplne obyčajných a jednoduchých ľudí, že za to, že nemajú dosť peňazí alebo nemajú prácu alebo sa im rozpadá manželstvo, či majú depresie môžu všetci iní (napr. kapitalisti, privatizéri, židia, Česi, Dzurinda a pod.) len nie oni sami. Jeden z mojich obľúbených robotníkov dokonca tvrdí, že bol za vlády Dzurindu v takej depresii, že nemá dobrú a primerane ohodnotenú prácu, že sa mu niekedy ani nepostavil. Nie že by som netvrdil, že za pár vecí Dzurinda nie je zodpovedný, resp. že pár veci kvalitne nedojebal, to určite nie, ale za to že sa robotníkovi nepostavil si ja myslím, že náš bývalý premiér fakt nemôže a skôr si za to zodpovedá ten robotník sám, že napriek strádaniu v svojom živote o ktorom stále hovorí keďže veľa žere a pije. A keby len veľa žral a pil, ale on ešte aj chodí voliť. A volí jeho. Najprv volil otca vlasti (pozn. pre mladšie ročníky pána JUDr. Vladimíra Mečiara, známeho aj pod nickom: Doktor), teraz volí Fica (pozn. pre staršie ročníky: otec vlasti ho onehdá nazval Róbertkom). Dzurindu samozrejme nevolil, veď zodpovedá za to, že sa mu sem tam nepostaví.Aby som však nebol úplne negativistický a alibistický človek, ktorý len kritizuje a neponúka riešenia ako tá svorka novinárskych hyen navrhujem: Poďte vy ktorí si hovoríte, že na čo voliť a nevolíte a radšej tvrdo robíte či už na Slovensku alebo mimo neho (potom, čo sa snáď vrátite späť so skúsenosťami, sebavedomím a peniazmi) k volebnej urne. Doktorovi, Róbertkovi ani vyhorenému palivu (Dzurinda) hlas asi nedáte, ale možno sa do roku 2010 zjavia nejakí relatívne schopní ľudia, ktorí by snáď stali za to. Ak by sa náhodou nezjavovali, tak sa postavte na čelo a skúste to sami, veď po 18 rokoch od revolúcie už snáď prichádza čas, aby veci verejné začali ovplyvňovať ľudia, ktorí nekradli, nekecali, neprivatizovali, korektne a slušne robili a chcú aby sa oni a aj ich deti, či rodičia mali fajn. Mohlo by nás byť nakoniec už aj viac, keďže:  
 
	 
	 
	Doktor sa prirodzenou formou mení už na starého (ch)uja, nových voličov viac nezíska a starí umierajú 
	 
	 
	 
	 
	Róbertko je jediná pomoc hore uvedeného robotníka, ktorému sa stále nestavá, ale jeho ženu Róbertko nakoniec prestane baviť lebo z rečí neni ani chlieb ani orgazmus.  
	 
	 
	 
	 
	Náš bývalý premiér sa zmenil na vyhorené palivo, ktoré síce zažiarilo a urobilo veľké svetlo pre Slovensko, ale čo už s ním, keď nedokázal odísť ako Tony 
	 
	 
	 
	 
	Ostatných úplne obyčajných ľudí, ktorí začnú o Ficovi pochybovať obalamútime dobre marketingovo, aby zírali, tak jak my budeme zírať keď nakoniec vyhráme 
	 
	 
	 
	 
	 a čo sa týka Janovích alkáčov, čo do piči viem aj ja poslať tých vyjebaných Maďarov, takže hlas môžu dať kua aj mne. 
	 
	 
 
 
 Zostávam s pozdravom, šak voľby 2010 sú už za rohom jak EURo, thakže thak a btw. pamätajte volám sa Vladimír Gürtler.          
 


