
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Marušic
                                        &gt;
                krížom-krážom
                     
                 Trnavská stovka – ako zvládnuť Veľkú Vápennú 

        
            
                                    9.6.2010
            o
            11:20
                        (upravené
                9.6.2010
                o
                8:33)
                        |
            Karma článku:
                13.08
            |
            Prečítané 
            3939-krát
                    
         
     
         
             

                 
                    Trnavská stovka je tradičné podujatie, ktoré sa koná prvú júnovú sobotu už 37 rokov. Je to pochod z bratislavskej hlavnej stanice hrebeňom Malých Karpát až do Brezovej pod Bradlom, oficiálne dlhý 91,486 m, reálne dlhý podľa toho koľkokrát zablúdite a koľko skratiek poznáte. Ako majú každé cyklistické preteky svoju kráľovskú etapu, tak aj tento pochod má svoj highlight - výstup na Veľkú Vápennú (754 m n.m.).
                 

                 Ide sa na ňu zo Sološnickej doliny z výšky 270 m a to až po zhruba 45 kilometroch pochodu. Psychologicky je to veľký predel a hovorí sa, že kto prejde Vápennú je jednou nohou v Brezovej. Ako som s ňou na ostatnom ročníku bojoval ja, to sa budem snažiť popísať, aby ste sa prípadne vedeli poučiť.   Zo Sološnickej doliny vychádzam plný entuziazmu, ktorý sa ale so stúpajúcim chodníkom a klesajúcou hladinou síl, vytráca.   Po chvíli, keď to už poriadne nejde, dostanem nápad zaspievať si pochodovú pesničku. Po krátkom uvažovaní zistím, že poznám iba dve, pričom jednej nepoznám slová, len melódiu, tak nemám na výber.   Přes spáleniště, přes krvavé řeky, Jdou mstíci pluky, neochvějně dál,   Bohatstvo ten kopec je nekonečný. Zo začiatku pieseň funguje a dodáva nové sily snažiace sa udržať krok s rytmom.   Na naší stráne, srdce právo věky, Jdem vpřed jak čas, jak pomstyhrozný val.   Potom ale hora zaútočí na psychiku. Udychčaný na hrebeni, musím trochu zostúpiť, lebo tak ide značka. Nikto asi nemá rád stratu vypotenej výšky, keď má ísť ešte ďalej hore.   S potomky slávných ruských bohatýrů, Vnuk husitů jde bok po boku vpřed.   A po chvíľke zostupu, je tu jedno z najstrmších stúpaní na trase. Volám ho vracajúci padák, aj kvôli chutiam to otočiť, zísť do Sološnice do krčmy a vykašľať sa na stovku, aj kvôli tomu, že mi tam vždy príde nevoľno.   Jsme stráž i hráz, rodíciho se míru, Jsme nových dnů, první uderní sled.   Potom ale kalvária pokračuje. Vrch zaútočí dlhým tiahlym stúpaním po lesnej ceste, ktoré nemá konca kraja. Na rozum prichádzajú spomienky, ako som po ceste často nadával. Kvôli dažďom, že to nie je pochod horami ale bažinou a niektoré úseky bolo nefalšované splavovanie. Ale proti Vápennej úplná malina.   Směr Praha S velikou armádou, z cesty smetem vrahy, S armádů sovětů, dojdeme do Prahy.   Směr Praha už neznie s nadšením, ale iba ako mrmlanie medzi zuby, kroky sa skracujú, aby sa udržal rytmus a samozdôvodňovanie tohoto stavu začína pripomínať Róberta Fica. Krízu majú všetci ostatní, mňa sa netýka, len som skrátil krok.   Paže vedle paži, stát budem na stráži, Společně v boj, půjdeme vpřed, anebo spolu padneme.   Populistické sebauspokojovanie pokračuje. Aha, tak krízu pripustím už aj ja, ale narozdiel od ostatných ju zvládam. Ako reakciu mením refrén na inú verziu.   S velikou armádou, rvát se jdem se smrtí, Nás s Rudou armádou, nikto nerozdrtí. My svorní a silní, pěsti své zvedáme...    A zovretá pravá päsť letí hore, dodáva asi na dve sekundy trochu energie a okoloidúcim podozrenie, že som sa pomiatol. Pôsobí to ako šrotovné. Na začiatku to vyzerá dobre, ale v konečnom dôsledku je to plytvanie silami na pohyb ruky. Už nie je snahou urdžať krok s rytmom piesne, namiesto toho spomaľujem pieseň, aby sedela s frekvenciou krokov, až to nakoniec znie ako keď vo walkmane dochádzajú baterky.   Áno, ďalšie štádium boja s krízou. Priznanie, že sa vymyká spod kontroly, ale stále lepšie ako keď by bol na mojom mieste niekto iný. Nový pokus, ktorý ma má z krízy spasiť, po vzore sociálnych podnikov, zmena pesničky.   Smelo tavárišči v nógu, lá lalalá lalalá...   Vápenná vyhráva na celej čiare, viac ako prvé štyri verše neviem odlalalákať. Hora tak podkuruje, že peklo by mi v tú chvíľu pripadalo ako mraziareň. Zúfalé pokusy chvíľu sa vydýchať, napiť sa, upokojiť sa, voľáko nepomáhajú, hneď po opätovnom vykročení je to zase to isté. Dokonca sa pristihnem povedať vetu: „Keby nebolo Vápennej, ani ten banán sa mi v ruksaku nerozpučí." A to už som v poslednom rétorickom štádiu hospodárskej krízy na Slovensku - môže za úplne všetko.   Pozerám do GPS, koľko je vrchol vzdušnou čiarou. Vzápätí zisťujem, že to ani nechcem vedieť. Snažím sa prísť na iné myšlienky. Predsa bežci na lyžiach aj cyklisti, keď stúpajú, po krajoch majú plno povzbudzujúcich fanúšikov a tréneri im hádžu fľaše s vodou. Je debil. Mohol som dať vedieť Veronike zo Sološnice, mohla tu bežať vedľa mňa s chladenou kofolou, tlieskať, hneď by sa mi lepšie išlo. Pekná somarina, už mi asi mäkne mozog. Radšej sa znova pozriem, koľko je to ešte vzdušnou čiarou. Neviem, či ten kopec je skutočne taký dlhý, alebo je to len moja úchylka všetko považovať za oveľa dlhšie než je v skutočnosti. Ženy by vedeli o tom rozprávať...   Treba zlepšiť náladu. Začínam si hovoriť vtipy. Chuck Norris by do Vápennej udrel päsťou a bolo by z nej vápno. Keď by urobil kop z otočky, bolo by to rovno hasené vápno. Začnem sa rozpomínať na básničky zo škôlky. Potom prišiel Karel Gott, chcelo sa mu na záchod. A záchod bol zacpatý, musel spievať posratý. Potom básničky zo základnej školy. Brezička, breza riekni mi, prečo tak chlasceš noci, dni. A nakoniec zo strednej. Znám křišťálovou studánku, kde nejhlubší je les, tam prohání se lesní víly a jsou nahoře bez. Na chvíľu to vylúdi úsmev potom dojdú básničky a Vápenná zase vedie. Nekonečný kopec. A na koniec nekonečna prišiel zatiaľ iba Chuck Norris. Dvakrát. Raz odpredu a raz odzadu.    Porozhliadnem sa po okolí, či tam nie sú nejaké dámy. Nerád by som prezradil prísne strážené tajomstvo, že som nemal francúzsku guvernantku. Tú čo ma predbehla už nevidím, za mnou žiadna a lesom sa ozve:   „Pojebaný kopec!"   Zrazu sa nohy rozbehnú do nevídaného tempa, že sa nestíham čudovať a za pár minút som hore. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Zoči-voči policajnému zboru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Antigorilí volebný systém
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Vorderes Sonnwendjoch a Sagzahn
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            O zemetraseniach bez mýtov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Ževraj ajťák je najlepší pre ženu? Ťažko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Marušic
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Marušic
            
         
        marusic.blog.sme.sk (rss)
         
                        VIP
                             
     
        Profesionálny tunelár
  "Slovo tunelář vymysleli bývalí kvazikomunisté jako součást své předvolební populistické kampaně."  Viktor Kožený  &lt;a href="http://blueboard.cz/anketa_0.php?id=571219"&gt;&lt;/a&gt;  Diskusia k ankete 

&lt;a href="http://www.blueboard.cz/shoutboard.php?hid=xhu0dnibsi42x66rek6cc9ivfspapi"&gt;ShoutBoard od BlueBoard.cz&lt;/a&gt;
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    197
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4420
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Prihodilo sa mi
                        
                     
                                     
                        
                            Tunelovanie
                        
                     
                                     
                        
                            Vodné diela
                        
                     
                                     
                        
                            Planéta Zem
                        
                     
                                     
                        
                            Historia est lux veritatis
                        
                     
                                     
                        
                            Slovenská sosajety (vážne)
                        
                     
                                     
                        
                            Odpočuté
                        
                     
                                     
                        
                            krížom-krážom
                        
                     
                                     
                        
                            64 polí
                        
                     
                                     
                        
                            Slopeme s monitorom
                        
                     
                                     
                        
                            Mosty
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Winston Churchill - Druhá světová válka
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sem som to nahustil
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tu som skoro furt a ničomu nerozumiem...
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




