

 
 
 To, ze spanielcina sa vyvijala odlisne v Europe a v Juznej amerike je svata pravda, neda sa o tom ani na chvilocku pochybovat. Je niekedy az zarazajuce ako jedno slovo moze v Spanielsku znamenat jednu vec a v Juznej Amerike zas cosi uplne ine. 
 V Chile je velmi bezne, ze ak chcete niekomu povedat aby sa posunul, aby si odsadol, alebo aby uhol pouzijete slovo "correrse". Ak budete hladat v slovniku zistite, ze toto slovo ma vela vyznamov, zalezi len od pohladu na vec a od geografickeho polozenia. 
 Raz som bola s kamaratmi s Univerzity v jedalni a sadali sme si k stolu. Boli tam ludia s roznych casti Spanielska. Moj kamarat Juan Pedro tam tiez bol, on je s Andaluzie. Sadol si tak nesikovne, ze mne uz neostavalo miesto a tak som ho chcela poprosit ci by sa trosku neposunul, aby som sa zmestila aj ja. Pekne som sa na neho usmiala a nahlas som mu povedala 
 "Juan Pedro, por favor, te puedes correr un poco!" 
 Na moje prekvapenie vsetci sa zrazu na mna pozreli a zacali sa smiat. Ja som znervoznela a tak som zopakovala moju prosbu este raz. V tom sa uz cela jedalen triasla od smiechu. Nechapala som. Juan Pedro si ma pritiahol k sebe a povedal mi, 
 "Gladys, co to trepes? Ty chces aby som si vyhonil v jedalni?" 
 Ja som nechapavo na neho pozerala a skrikla 
 "Coze?!!! Ja chcem aby si sa posunul!" 
 On mi potom vysvetlil ze "correrse" v Spanielsku znamena nieco uplne ine, a odporucil mi aby som skusila pouzivat nejake ine slova ako napriklad "mover se" (posunut sa), "hacerse a un lado" (ist stranou, na bok), atd. Po tomto vysvetleni sme sa este dlho na tej historke smiali. Zial, pretoze v Chile "correrse" sa pouziva uplne bezne mam do teraz problemy, ja sa ho do teraz neviem zbavit! Raz som tak poprosila aj mojho sefa. Este ze sefino ma zenu z Argentiny. 
 A teraz z opacneho sudka. Moji rodicia ma boli pozriet v Barcelone. Pocas ich pobytu sme ju celu prechodili a precestovali. Najcastejsie sme pouzivali metro. Rano sme sa bavili aky je plan na den. Ja som im vysvetlovala ako sa budeme premiestnovat, co by bolo dobre vidiet, kde to stoji za to. 
 "Hoy iremos al Parque Güell, asi que cojeremos el metro y despues caminaremos un poco" (dnes pojdeme do parku Guell, pojdeme metrom a potom aj pesi) 
 Moj otec na mna vykrikol "Ale Gladys aka si neslusna, co to chces robit v metre?" 
 Ja som sa na neho pozrela a nechapala. Az potom mi zaplo ze slovo "cojer" znamena v Juznej Amerike "davat, trtkat, atd". 
 Vzdy ked to slovko moj ocino pocul, mi so smiechom povedal, ze som nevychovana. V Chile sa totiz autobus "toma" a nie "coje". 
   
 Tak taketo pomotane to moze niekedy byt. Niekedy mam fakt spanielsku dedinu v hlave, ale za to usmev mi nechyba. :-) 
   
 Hasta la victoria amigos! 

