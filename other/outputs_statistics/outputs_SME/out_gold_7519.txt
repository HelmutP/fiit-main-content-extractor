

 Métis 
 
Hovorí mi – nebásni! 
Básnici už vyšli z módy. 
Nemódni sú len blázni, 
proti prúdu lodivodi... 
 
 
Hovorím jej – neblázni! 
Báseň aj titánov zrodí. 
To, čo vkladám do básní, 
nie je to, čo varia z vody 
 
 
jednodňoví kuchári, 
rýchlokvasné celebrity, 
všehochuti gurmáni, 
ktorých dielo sotva cítim.  
 
 
Poet prevteľuje ríty 
do reality, moja spanilá. 
Prečo by si sa i ty 
so mnou inak bavila? 
 

