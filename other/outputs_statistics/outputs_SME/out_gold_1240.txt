

 Takmer do začiatku 17. storočia sa počas štátnych ceremónii a udalostí celoštátneho významu interpretovali pravoslávne bohoslužobné piesne. Za vlády Petra I. a určité obdobie po ňom (do 80. rokov 18. storočia) sa pri príležitosti rôznych sviatkov spievali všakovaké kantáty[1], patriotické piesne a tiež hymnus Mnogaja Lita. 
 Piesne tohto druhu ale postupne ustúpili a prenechali miesto iným, ktoré sa intepretovali počas celého 18. storočia. Najčastejšie to bola stredoveká pieseň a celoeurópsky hymnus Teba, Bože chválime (rus. Тебя, Бога, хвалим). Táto pieseň sa obyčajne zvykla spievať po víťazstve ruskej armády vo vojne, v slávnostné dni a nechýbala ani pri „cárskych" (významných) dňoch cárskej rodiny (narodeniny, výročie apod.). 
 Prvou oficiálnou[2] štátnou hymnou na území Ruského impéria sa stala Modlitba ruského národa (rus. Молитва русских). Vznikla v roku 1816 na slová Vasilija Andrejeviča Žukovského (1783 - 1852) - Boh ochraňuj cára - a melódiu anglickej hymny, ktorej autorom je nemecký hudobný skladateľ Ludwig van Beethoven (1770 - 1827). Na verejnosť bola oficiálne uvedená 19. septembra 1816 pri príležitosti výročia lýcea v dedine Carskoje selo (dnes časť Petrohradu) a spievala sa ešte s dvoma strofami, ktoré k nej dopísal Alexander Sergejevič Puškin. Prítomnému cárovi Alexandrovi I. sa pieseň tak veľmi páčila, že okmažite vydal nariadenie vždy zahrať túto pieseň počas uvítacieho ceremoniálu štátnych predstaviteľov zahraničných krajín a tiež ruského cára. 
 
 







Nič však netrvá večne, a tak prvú hymnu vystriedala druhá. Písal sa rok 1833, keď prvýkrát zazneli slová Boh chráň cára na melódiu Estónca Alexeja Fjodoroviča Ľvova[3] (5. jún 1798 - 16. december 1870) v romnomennej hymne Ruského impéria. Prvýkrát bola hymna intrepretovaná 25. decembra 1833, v deň 21. výročia sviatku vyhnania Napoleona Bonaparteho z Ruského impéria. V kurze bola celých 84 rokov - posledný raz zaznela pri dobrovoľnej detronizácii[4] Mikuláša II. dňa 2. marca 1917.
 Po februárovej revolúcii v roku 1917 sa na krátke obdobie štátnou hymnou stala Ruská Marseillaisa (oficiálny názov hymny bol Marseillaisa pracujúcich; text Piotra Lavroviča Lavrova bol publikovaný 1. júla 1875 v novinách Vpred) spoločne s Internacionálou, ktorú na základe výskytu nových podmienok a situácie (boj proti triednym nepriateľom v novom štáte) ustanovil Vladimir Iľjič Lenin. Prvý raz Internacionála zaznela na treťom zjazde Sovietov dňa 10. januára 1918 a odvtedy sa spievala a bola symbolom hymny štátu, ktorý vyhral proletársku revolúciu. 
 
 




  



Známka Sovietskeho zväzu vydaná pri príležitosti 100. výročia narodenia Alexandra Vasilieviča Alexandrova



Čas zmenu hymny nastal ku koncu druhej svetovej vojnym, a tak 1. januára 1944 znela z rádia nová celkom sovietska hymna. Jej dejiny však siahajú o niečo ďalej - do roku 1936. Vtedy ju pod názvom Hymna strany boľševikov (rus. Гимн партии большевиков) a tiež s inými slovami od ruského spisovateľa Vasilija Ivanoviča Lebedev-Kumača (1898 - 1949) prvýkrát publikovali v Sovietskom zväze. Na oficiálnu interpretáciu v podobe štátnej hymny to však nestačilo, tak si k nej sadol majster sovietskeho slova - Sergej Vladimirovič Michalkov (* 1913) spoločne s Gabrielom Arkadievičom Urekľanom[5] (1899 - 1945) - a k melódii Alexandra Vasilieviča Alexandrova (1883 - 1946) napísal nový text tak, aby vernejšie zodpovedal vtedajšiemu režimu.
 Text bol následne schválený Prezídiom Najvyššieho Sovietu ZSSR (ÚV KSSZ ho schválil 14. decembra 1943) a od 15. marca 1944 a mohol sa začať spievať pri výnimočných a štátnych udalostiach. 
 
 






Serej Vladimirovič Michalkov



Zaujímavosťou bola interpretácia tejto hymny v rokoch 1955 - 1977. Keďže sa v hymne spomínalo meno Josifa Vissarionoviča Stalina („Нас вырастил Сталин - на верность народу, на труд и на подвиги нас вдохновил!") a voči nemu mali v tom čase ešte rešpekt, jednoducho text nespievali. Vyskytlo sa však mierne fópá. Staré slová hymny totiž nikto oficiálne nezrušil, a tak sa v zahraničí naďalej veselo spievali. Priam najvhodnejšiou príležitosťou na strápnenie sovietskych občanov v tejto súvislosti boli športové podujatia. Hokejové zápasy sovietskej zbornej (známe pod názvom Super series[6]) neboli výnimkou.
 Aby sa predišlo podobným prešľapom, v roku 1977 opäť požiadali Sergeja Michalkova, aby napísal nový text sovietskej hymny. 
 Tá prežila do roku 1990, kedy ju nahradila nová hymna - RSFSR a Ruskej federácie. Tak ako sa rozpadol Sovietsky zväz a na jeho troskách vo východnej Európe vznikol nový štátny útvar, tak sa na určitý čas rozpadol sen o zotrvaní sovietskej hymny v novom Rusku a nahradila ho melódia z diela ruského skladateľa obdobia romantizmu - Michaila Ivanoviča Glinku (1804 - 1857) - pod názvom Patriotická pieseň (rus. Патриотическая песня). Pieseň vznikla v roku 1833 pod názvom vo francúzštine - Motif de chaint national. 
 
 






Neoficiálne slová hymny z rokov 1990 - 1999



Pieseň sa dostala do širšieho výberu v roku 1990, kam ju pretlačila trojčlenná vládna komisia[7], ktorá mala na starosti výber melódie novej hymny. Dokonca usporiadala aj konkurz, do ktorého sa mohli zapojiť všetci občania Ruskej federácie, ktorí chceli poslať text - buducí možný variant štátnej hymny. Slová však hymna nikdy nemala, aj keď sa o to táto vládna garnitúra pokúšala. Tesne pred schválením textu hymny - v roku 1999, - totiž nečakane Boris Nikolajevič Jeľcin z postu prezidenta Ruskej federácie odstúpil a uzákonenie novej hymny ostalo na jeho nástupcovi - Vladimirovi Vladimirovičovi Putinovi.
 Ten to vzal ako z rýchlika a veľa nerozmýšľal. Podarilo sa mu však čosi zaujímavé. Prezident si uvedomil, že bol nejaký Zväz sovietskych socialistických republík, a tak spojil jeho dejiny s dejinami Ruskej federácie. Preto dnes pri počúvaní a spievaní štátnej hymny Ruskej federácie sa všetkým Rusom vyjavuje minulosť a súčastnosť v jednom. Putinovi sa spojením minulosti a súčasnosti Ruska podaril husársky kúsok a ten dovŕšil jej textom. 
 
 







Mimochodom, hymna, tak ako tá predchádzajúca, od decembra 2000 do februára 2001 nemala slová. To sa však zmenilo na zasadnutí štátnej dumy začiatkom roka 2001, kedy sa do parlamentu dostalo 5 návrhov na text novej hymny. Autorom jednej z nich bol už spomínaný Sergej Michalkov. Po skúsenostiach s jeho textami ho ako text pre štátnu hymnu Ruskej federácie 7. marca 2001 štátna duma jedohlasne schválila. A tak sa nová hymna už mohla začať spievať.
 A v čom je to spojenie minulosti a súčasnosti? Predsa v melódii a texte Sergeja Vladimiroviča Michalkova - jediného človeka v republike, komu sa za svoj život podarilo napísať trikrát slová hymny Ruska v rôznych obdobiach. 
 Chceli by ste od neho mať autogram? Býva v Rusku, ale viac už nepoviem... 
 [1] Najstaršia ruská kantáta vznikla ku koncu obdobia vlády Petra I. a mala názov Pochod premeny Petra Veľkého (rus. Преображенский марш Петра Великого). Neskôr (koniec 19. storočia) sa táto kantáta stala často interpretovanou piesňou v Rusku. Od roku 1917 určitý čas bol dokonca jeho štátnou hymnou. [2] Neoficiálnou hymnou Ruského impéria na prelome 18. a 19. storočia bola hymna Šír sa hrom víťazstva (rus. Гром победы, раздавайся). Hymna vznikla v roku 1791 na slová Gavriila Romanoviča Deržavina (1743 - 1816) a melódiu Osipa Antonoviča Kozlovského (1757 - 1831) na motívy polonézy. Dôvodom napísania hymny bola víťazná bitka generála Alexandra Vasilieviča Suvorovova pri osmanskej pevnosti v Izmail (dnešná Ukrajina) počas druhej rusko-tureckej vojny (1787 - 1792). [3] O hymne v nadnesenom slova zmysle hovorí autor jej textu nasledovné: „Prvý pokus o vznik ruskej hymny sa datuje do roku 1833, keď knieža Mikuláš II. prikázal napísať slová a melódiu hymny skupine básnikov a hudobných skladateľov. V konečnom dôsledku sa knieža Mikuláš II. zastavil na hymne, autormi ktorej boli V. A. Žukovskij (text) a A. F. Ľvov (hudba). Text sa skladal zo šiestich riadkov: Боже, Царя храни! Сильный державный, Царствуй на славу нам, Царствуй на страх врагам, Царь православный! Боже, Царя храни! a vďaka svojej melódii v mohutnej zborovej intrepretácii, znela hymna obzvlášť pôsobivo." (Записки композитора Алексея Федоровича Львова [Текст] // Русский архив. – 1884. – № 4. – С. 225–260.) V Ruskom impériu, a nielen tam, bolo odjakživa vlastné veci maskovať. Text hymny a rok jej publikácie síce pravdivý je, ale prvenstvo melódie štátnej hymny je ľahko spochybniteľné, nakoľko pred ňou existovalo ešte niekoľko iných textov, ktoré slúžili ako štátna hymna Ruska. [4] Dokázali ho presvedčiť, že lepšie pre krajinu urobí, ak sa vzdá trónu Ruského impéria. [5] V niektorých publikáciách ho píšu ako Gabriel Aršalujsovič Ararakeľan (rus. Габриэль Аршалуйсович Аракелян) alebo Gabriel Arkadievič Eľ-Registan (rus. Габриэль Аркадьевич Ель-Регистан) prípadne Eľ-Registan. [6] Bola to séria hokejových zápasov medzi ZSSR a Kanadou a ZSSR a klubmi NHL v 70. a 80. rokoch 20. storočia. [7] Členmi komisie boli predseda Zväzu hudobných skladateľov Ruskej federácie Vladislav Igorievič Kazenin, spolupredsedovia Sergej Michalkov a minister kultúry RSFSR Jevgenij Sidorov.  

