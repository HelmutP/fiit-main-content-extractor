
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Draxler
                                        &gt;
                Politika
                     
                 Tromfujem SaS: chcem, aby NR SR mala iba 10 poslancov! 

        
            
                                    10.9.2009
            o
            22:01
                        |
            Karma článku:
                9.38
            |
            Prečítané 
            2441-krát
                    
         
     
         
             

                 
                    A voľte ma! Zaslúžim si to, na rozdiel od čelných predstaviteľov SaS nežijem z eurofondov ani zakázok ministerstiev.
                 

                     Teraz už vážne. SaS nie je strana, ktorou sa treba vážnejšie zaoberať. Majú v podstate dve priležitosti, ako svojim zakladateľom zabezpečiť vplyv v politike, keďže samostatne na parlament sotva dosiahnu. Prvá je získať dostatočnú podporu na to, aby to pred voľbami odjedalo percentá a teda vadilo SDKÚ, ktoré by ponúklo strane podobný obchod, ako kedysi DS. Zaujímavé miesto pre zopár predstaviteľov na kandidátke či nejakého dokonca vo vláde, zvyšok prebrať do svojich štruktúr. Druhá je uchytiť sa na lokálnej úrovni, v Bratislave a v bratislavskom VÚC. Ak však budú v SDKÚ chytrí, proti blogovej ministrane ľahko prijmú preventívne kroky.   Ale dnes ma zaujal ich billboard. Sľubujú znížiť počet poslancov na 100. Nuž, je to minimálne príležitosť na zamyslenie sa nad princípmi fungovania zastupiteľskej demokracie.   Prečo vlastne radikálne neznížiť počet poslancov? Napríklad na 50? Alebo na „mojich" 10? Vynárajú sa dve principiálne otázky. Po prvé, problém koncentrácie moci. Ten je mnohovrstevný.  Znižovaním počtu napríklad stúpa váha jednotlivého poslanca. Pri nekalom získavaní vplyvu (napríklad pri hlasovaniach o zákonoch) treba v menšom parlamente skorumpovať menej poslancov.  Ale tým jednotlivým poslancom bude možné ponúknuť vyššie sumy, čiže vzrastie aj ich motivácia nechať sa skorumpovať.   Platia aj iné vzťahy, napríklad viac poslancov, viac vzájomne sa kontrolujúcich hláv.   Po druhé, parlament je zastupiteľský orgán. Poslanci by mali byť, aspoň teoreticky, k dispozícii svojim voličom. A tiež straníckym štruktúram, keďže moderná demokracia je stranícka demokracia. Čím menej poslancov, tým viac ľudí, ktorých musí každý z nich pokrývať.   Mimochodom, v susednom Česku sa médiá snažia aspoň z času na čas kontrolovať, ako poslanci dodržiavajú stanovené časy, keď sú v kanceláriách k dispozícii svojim voličom. V niektorých krajinách, zvlášť s väčšinovým volebným systémom (napr. Veľká Británia), kde jeden poslanec výhradne zastupuje jeden okrsok, je úplne nemysliteľné, aby bežný poslanec nebol pravideľne so svojimi voličmi v kontakte. Nezachytil som však, žeby sa slovenské médiá nejako zaujímali o túto stránku poslancovania - koľko času poslanec strávi v styku s voličmi, s odborníkmi, alebo inak „v teréne". Oveľa viac ich zaujíma, ako poctivo poslanci dodržiavajú nezmyselný „štikačkový" systém - poslanci sa chodia nudiť do parlamentnej budovy, a čím viac času tam zabijú, tým viac sú verejnosti predstavovaní ako vzorní zastupitelia.   Takže tak. Otázka počtu poslancov je otázka nachádzania nejakého optima vzhľadom na protirečivé požiadavky (rozptýlenie moci, konkurencia, efektívne zastupovanie... versus náklady).   Iste, je možné, že v SaS si povedali, že 100 je optimu bližšie ako 150 a preto navrhujú tento počet. Ale na webstránke s argumentami o tomto bode ich navrhovaného referenda sa nič také nenachádza. Uvedené argumenty možno zhrnúť do všeobjímajúceho „poslancov treba menej, lebo sú to darmožráči."   Hm, keď mám pravdu povedať, nielen tento bod, ale celý marketingový obsah stránky je asi taký, že „populistický" Smer, z ktorého majú „sasáci" hlavného strašiaka, popri tom vyzerá ako strana váhavých a na politiku príliš filozofických intelektuálov.   Ale čoby človek neurobil pre teplé miestečko v tom nenávidenom parlamente alebo na ministerstve.   Ale namiesto SaS voľte radšej mňa! Nikto vám nedá toľko, koľko ja vám môžem sľúbiť! A to je fakt : )     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Čo sa stalo v Odese?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Vylomeniny amerických kongresmanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Niekoľko poznámok k Paškovmu obmedzovaniu novinárov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Balkánsky pop
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Ruské béčkové akčáky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Draxler
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Draxler
            
         
        draxler.blog.sme.sk (rss)
         
                        VIP
                             
     
         Momentálne žije v Prahe, kde píše, skúma a vyučuje. Po rokoch strávených v západnej Európe je to príjemná zmena, aj keď to počasie by mohlo byť aj lepšie. Bloguje aj na http://blog.etrend.sk/juraj-draxler/. (Foto: European Alternatives Cluj) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    253
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3170
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Dôchodkové veci
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Praktické rady
                        
                     
                                     
                        
                            Impresie
                        
                     
                                     
                        
                            Ekonomické zamyslenia
                        
                     
                                     
                        
                            Moje alter ego
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Daňová debata (Filko, Marušinec, Draxler, Lehuta)
                                     
                                                                             
                                            Dôchodková debata na eTrende: Draxler, Filko, Chren
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Radůza
                                     
                                                                             
                                            Szidi Tobias
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomír Némeš
                                     
                                                                             
                                            Bobby (denník z ciest po Ázii)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Uniorbis - Svet univerzít
                                     
                                                                             
                                            Denník The Financial Times
                                     
                                                                             
                                            Denník The Guardian
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Balkánsky pop
                     
                                                         
                       List priateľke
                     
                                                         
                       Sanatórium Družba: V jednej z najčudnejších budov sveta
                     
                                                         
                       Načo sú dobré univerzitné rebríčky
                     
                                                         
                       Dodrží premiér Fico svoje vlastné pravidlá?
                     
                                                         
                       Som zdravotná sestra. Personál v slovenských nemocniciach? Otrasný
                     
                                                         
                       Aj zvieratá zomierajú lepšie ako pacienti v našich nemocniciach
                     
                                                         
                       Načo sú dobré univerzitné rankingy?
                     
                                                         
                       Harvard alebo nič
                     
                                                         
                       Naozaj potrebujeme viac policajtov, pán minister?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




