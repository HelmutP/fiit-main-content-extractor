

 ... mamina, tuto vzadu a na boku mi to môžeš vystrihať. Hore si vlasy nechám narásť ... "na Hamšíka". Takto sme doma cez víkend strihali vlasy. 
 V škole dostal syn pochvalu od triednej učiteľky, aj od učiteľky v školskej družine, ako mu to veľmi pristane. 
 Predvčerom som sa zastavila u mojej starkej. Ako vždy sme sa večer rozprávali ako sa máme, čo má starká nové, o politike, o televíznych seriáloch, o lekároch, varení, práci, o deťoch a tak. 
 Vravím: "Starká pozri, aký účes má môj syn. Predtým mu to viac pristalo, toto mu nesedí. Hore na hlave má nagélované vlasy, akoby ho pokopala elektrina". 
 Na to som od mojej 79 ročnej starkej /synova prastará mama/ dostala odpoveď: "Ty si staromódna. Teraz to má na Hamšíka" ... 

