
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Blažeková
                                        &gt;
                Súkromné
                     
                 Rozprava o DOD v Kňazskom seminári v Bratislave 

        
            
                                    1.5.2010
            o
            14:00
                        (upravené
                1.5.2010
                o
                14:05)
                        |
            Karma článku:
                5.01
            |
            Prečítané 
            488-krát
                    
         
     
         
             

                 
                    Mladí muži oblečení v čiernych reverendách zbožne kráčajúci každú nedeľu do Katedrály sv. Martina v Bratislave. Pre mnohých ľudí mimozemšťania, ktorí sú viac ako divní, lebo svoj život zasvätili Bohu, zriekli sa pominuteľných hodnôt a vytrvalo kráčajú za Kristom.
                 

                 
  
        Žijú v seminári, kde sa venujú štúdiu a rozjímaniu – prakticky uzatvorení pred celým svetom. Vedia, že ľudia ich nepoznajú a vedia i to, že ich niektorí odsudzujú.      Ak bohoslovca stretnete na ulici nebojte sa ho osloviť. Spod reverendy nevytiahne chápadla, ani sa nezmení na beštiu. Aby ľudia nemali takéto skreslené predstavy, tak každú Nedeľu Dobrého Pastiera otvoria bohoslovci dvere seminára dokorán a srdečne vítajú každého zvedavého človiečika.      Bohoslovci sú vlastne študenti katolíckej teológie. Rovnako ako iní študenti VŠ aj oni majú počas dňa prednášky a cvičenia. Učia sa a píšu seminárne práce. Majú zápočty a skúšky.      Ich deň začína skoro ráno, kedy sa modlia Ranne chvály. Večer  sa modlia Vešpery. Počas dňa  čítajú so Svätého Písma, kontemplujú, učia sa, majú priestor na duchovné rozhovory a samozrejme, že sa venujú i svojim koníčkom.     Tvoria časopis, v Bratislave je to ADSUM, v ktorom rozoberajú rôzne témy a prezentujú svoj život v seminári, komentujú aktuálne spoločenské udalosti. Snažia sa priblížiť k ľuďom.    Toto všetko sa človek dozvie, keď navštívi DOD (deň otvorených dverí). A nie len toto.     Bohoslovci počas celého DOD dňa majú pripravený program. Rozhovory so zaujímavými hosťami, súťaže, prezentujú svoj hudobný talent, dávajú priestor na zvedavé otázky,... Môžete navštíviť izbu bohoslovca, priestory kde sa učia.     Zistíte, že to nie sú žiadni mimozemšťania, ale obyčajní chlapci, ktorí sa môžu stať vašimi priateľmi. Priateľmi, ktorí sa len  rozhodli kráčať za Kristom.            ak chcete vedieť viac kliknite na ich stránku www.adsum.sk     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Keď diabetes (cukrovka) "žerie" zaživa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Dobrá známka sa nerovná dobrá vedomosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Originalny pozdrav k Vianociam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Môj prvý kúpeľný pobyt a hra na hrocha
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Uhni kripel!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Blažeková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Blažeková
            
         
        martinablazekova.blog.sme.sk (rss)
         
                                     
     
        Neviem kto som, čo som, kam kráčam, čí som posol, čaká ma smrť či spása, ale aj tak usmievam sa.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    48
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    513
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            študáci :o)
                        
                     
                                     
                        
                            mňa nevymyslíš, ja som
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď diabetes (cukrovka) "žerie" zaživa
                     
                                                         
                       Dobrá známka sa nerovná dobrá vedomosť
                     
                                                         
                       Môj prvý kúpeľný pobyt a hra na hrocha
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




