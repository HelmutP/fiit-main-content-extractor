

 Pretekár, ktorý zajazdí najrýchlejší čas na zajtrajšej RAČIANSKEJ ČASOVKE, bude odmenený finančnou odmenou 100€ v hotovosti. 
  Príďte si vyskúšať svoju jarnú kondičku na 6km trati s prevýšením 340 metrov už 1.5.2013 
 Po vydarenom 1. kole Župného Pohára Na bicykli deťom sa na Vás tešíme už o  7 dní na druhom kole Župného Pohára - na Račianskej cykločasovke. 
   
 POZOR: PO dojazde do cieľa budete mať možnosť vyskúšať si bicykle FELT. Testovanie sa uskutoční 1.5.2013 hore na Bielom Kríži. Bude to deň spojený s pretekom a testovaním bikov. Tešíme sa na Vás, milí účastníci :-) 
 
 Zúčastniť sa môžete ako aj na cestných bikoch, tak aj na horských.  
 Štart pretekov je tak ako minulý rok v Rači za starým kúpaliskom (viac mapa). 
   
 
 
Usporiadateľ: MČ Bratislava – Rača Miesto štartu: Bratislava - Rača, amfiteáter v Knižkovej doline Termín: 1.5.2013, štart o 10:00 hod, intervalový štart  Prihlasovanie: cez internet (http://www.nabicyklidetom.sk/prihlaska), pomocou priloženého prihlasovacieho formulára, prípadne na mieste štartu, najneskôr však do štartu prvého pretekára  Kategórie:  muži: kadeti 15 -16 rokov juniori 17 - 18 rokov muži 19 - 39 rokov muži 40 - 49 rokov muži 50 - 59 rokov muži 60 a viac rokov  ženy: kadetky 15 -16 rokov juniorky 17 - 18 rokov ženy 19 - 39 rokov ženy 40 - 49 rokov ženy 50 a viac rokov  Trasa:  Bratislava, Rača – amfiteáter, Knižkova dolina, Dolný červený kríž, Biely kríž. Trasu môžete ísť na akomkoľvek bicykli. Či už horskom alebo cestnom.  Dĺžka trasy: 6 km/ prevýšenie 340 m Štartovné: 5 €, z toho organizátor venuje 2 € na pomoc detskej onkológii - OZ Deťom pre život. Štartovné sa platí na mieste prezentácie Občerstvenie: nápoj a guľáš v bufete Včelín na Bielom kríži Meranie času: časomieru zabezpečuje VOS-TPK Vyhodnotenie: vyhodnocujú sa prví traja pretekári v každej kategórii, ak v kategórii je min. 5 pretekárov. Odovzdávanie cien: 15:00, na Bielom kríži Ceny: poháre a vecné ceny Výsledky: budú uverejnené na stránke: http://www.vos-tpk.sk  Upozornenie:  Prilba je povinná, zloženie prilby počas pretekov bude riešené diskvalifikáciou pretekára a vylúčením zo žrebovania v tombole! Pretek sa koná za každého počasia. Zmena rozpisu vyhradená  
   
 TESTOVANIE BIKOV FELT NA BIELOM KRÍŽI PO RAČIANSKEJ ČASOVKE  Testovací deň bicyklov FELT v cieli Račianskej časovky. 
 Každý pretekár má možnosť vyskúšať bike FELT. Za vyskúšanie dostane lístok, ktorý bude môcť vymeniť za polievku. 
 Uskutoční sa 1.5.2013 na Bielom Kríži v Malých Karpatoch v čase od 9 do 18 hodiny. Ak máte chuť prísť si otestovať niektorý z našich bicyklov ste vítaní, nezabudnite si priniesť - dva doklady napr. OP a VP (dôležité pre možnosť zapožičania bicykla), tretry, pedále a prilbu.  
 Pripravili sme pre vás aj malé občerstvenie vo forme gulášu a nápojov, nebude chýbať ani hudba. 
 

