

Piaty ročník charitatívnej cyklojazdy Na bicykli deťom sa úspešne skončil. Cyklisti, medzi ktorými nechýbali trenčianskí hokejisti, herec Ivan „Tuli“ Vojtek, osobnosti televíznej obrazovky Peter Čambor, Peter Varinský či Minister hospodárstva Tomáš Malatinský prešli cca1150 km a vyzbierali viac ako 27 000 EUR, ktoré poputujú na pomoc detským onkologickým pacientom. 
















 Deti sú naše najväčšie
poklady, pre ktoré sme ochotní urobiť čokoľvek, kedykoľvek a kdekoľvek.
Obzvlášť vtedy, ak bojujú so zákernou chorobou, ktorá v nás vyvoláva strach a
beznádej. Na to, aby sme mohli vykonať veľké činy, ktoré môžu zachrániť ľudský
život, však niekedy stačí paradoxne veľmi málo. Stačí chcieť pomôcť.  „Myšlienka projektu Na bicykli deťom vzišla
práve z tejto potreby pomôcť. Preto sme 8 dní šliapali každý deň do
pedálov, i keď to častokrát vôbec nebolo ľahké. No ani tie detičky to
nemajú jednoduché, tak sme sa nemohli vzdať,“ hovori Miro Bilík, zakladateľ
občianskeho združenie Deťom pre život a držiteľ ocenenia Filantrop roka
2013. 

 Súčasťou charitatívnej
cyklojazdy bola aj verejná zbierka, počas ktorej sa vďaka dobrým ľuďom
a aj sponzorom podarilo vyzbierať viac ako 27 000 EUR. „Táto suma ešte
nie je konečná no už teraz vieme, že sme prekonali minuloročný výnos. Tieto
peniaze pôjdu na realizáciu rekondično relaxačných pobytov pre detských
onkologických pacientov, ako aj na prevádzkovanie nového projektu, ktorým je
Auto pre deti. Je to projekt individuálnej prepravy pre deti zo sociálne
slabších rodín, kotrý im umožní stráviť viac času s ich rodinou,“
uzatvára Miro Bilík.  



