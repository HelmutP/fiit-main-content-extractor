

7.ročník jedného z najobľúbenejších horských cyklomaratónov sa pomaly ale isto blíži. Čas na trénovanie máte do 16.augusta.    
     Leto, čas prázdnin, dovoleniek, oddychu...a cyklistiky! Ani toto leto totiž nebude v hlavnom meste chýbať už tradičný horský maratón po malebných zákutiach Malých Karpát. V sobotu 16.8.2014 sa všetci nadšenci cyklistiky, malí, veľkí, mladí, najmladší i tí skôr narodení postavíme na štart už 7.ročníka Kellys Greenbike Tour. Takže na chvíľu odložte nafukovačky a vytiahnite bicyke a miesto k vode si to namierne na Železnú Studničku. Vieme Vám sľúbiť skvelý športovo zábavný deň na čerstvom vzduchu. 
      Trate tohtoročného cyklomaratónu zostávajú nezmenené, takže máte možnosť si porovnať Váš dosiahnutý čas z minulého roku. Príjemné lesné chodníčky, príjemno nepríjemné stúpania a klesania, potoky či lúky na Vás čakajú na krátkej 34-kilometrovej trati a samozrejme aj na dlhej 75-kilometrovej. Tá si z roka na rok získava viac odvážlivcov. Pre najmenších pretekárov je pripravený cyklomaratónik priamo v areáli na Partizánskej lúke. 
      Čaká Vás nielen deň plný športu a skvelých výkonov, ale aj zábavy. Tú zaručuje bohatý sprievodný program plný hudby, dobrého jedla, hier, súťaží a detských atrakcií a nebude chýbať ani tombola. Tak ako po minulé roky časť zo štartovného opäť poputuje na pomoc našim najmenším, ale najstatočnejším bojovníkom - onkologicky chorým deťom. 
   
 Tešíme sa na Vašu účasť, vidíme sa 16. augusta 2014 na štarte na Partizánskej lúke!  
   
 Prihlášky a viac info - www.kellysgreenbiketour.sk  

