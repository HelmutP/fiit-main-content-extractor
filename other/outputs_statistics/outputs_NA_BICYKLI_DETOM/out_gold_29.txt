

 Prestigio City Triathlon Bratislava - 2014 ETU Sprint triathlon European Cup muži, ženy Prestigio City Triathlon Bratislava 2014 - 17.- 18. máj 2014 
   
 
Propozície: http://www.citytriathlon.sk/index.php?id=5
 
 
Prihlásenie: http://www.citytriathlon.sk/index.php?id=18
 
   
 Triatlon je aj súčasťou druhého kola novovzniknutej Bratislavskej série s názvom Bratislava Region Cup. 
  
Prestigio City Triathlon Bratislava - 2014 ETU Sprint triathlon European Cup 
muži, ženy Prestigio City Triathlon Bratislava 2014 
 
  
 
Medzinárodné majstrovstvá Slovenska vekových kategórií - muži, ženy a Slovenský 
a Český pohár v triatlone - muži a ženy, kluby, rodiny (podľa SS ČTA leto 2014) 
 
  
 
Prestigio City Triathlon Bratislava 2014 Slovenský pohár dorastenci/ky a 
juniori/ky, Český pohár dorastenci/ky a juniori/ky 
 
  
 
Prestigio City Triathlon Bratislava 2014 Slovenský a Český pohár mladší a starší 
žiaci/čky 
 
  
 
Prestigio City Triathlon Bratislava 2014 Medzinárodné majstrovstvá Slovenska mix 
klubových štafiet 3x 0,35 - 5 - 1,4 (muž, žena, muž) 
 
  
 
Prestigio City Triathlon Bratislava 2014 Slovenský a Český pohár - deti 8 - 11 
rokov 
 
Prestigio City Triathlon Bratislava 2014 Slovenský a Český pohár - deti 6 - 7 
rokov 
 
Prestigio City Triathlon Bratislava 2014 - škôlkari 4-5 rokov 
 
Prestigio City Triathlon Bratislava 2014 - škôlkari 2-3 rokov 
 
  
 
Hlavné preteky prestigio City Triathlon Bratislava 
2014 - kategória muži - sa z dôvodu vyššieho počtu pretekárov pôjde na 3 alebo 4 
semifinálové rozjazdy z ktorých sa bude postupovať do nedelňajšieho finále (75 
postupujúcich). 
 
  
 
Postupový kľúč: pokiaľ 
sa pôjdu 3 rozjazdy, pravidlo bude nasledovné: z každého semifinále postupuje 
prvých 20 mužov a 15 pretekárov s najrýchlejším časom na nepostupových miestach. 
Pokiaľ sa pôjdu 4 semifinálové rozjazdy, pravidlo bude nasledovné: z každého 
semifinále postupuje 15 pretekárov a 15 pretekárov s najrýchlejším časom na 
nepostupových miestach. 
 
  
 
  
 
MIESTO 
 
Bratislava, Kuchajda 
 
  
 
  
 
TERMÍN 
 
17.- 18. máj 2014 
 
  
 
  
 
USPORIADATEĽ 
 
Realiz sport team o.z. 
 
Ružová dolina 25 
 
821 09 Bratislava 
 
Číslo účtu: 2928866249 / 1100 
 
Tel: 0903 729 777 
 
Email: info@realizsportteam.sk 
 

www.realizsportteam.sk 
 
  
 
Realiz sport team s.r.o. 
 
Ružová dolina 25 
 
821 09 Bratislava 
 
Tel: 0905 309 576 
 
Email: info@realizsportteam.sk 
 

www.realizsportteam.sk 
 
  
 
  
 
RACE OFFICE 
 
Reštaurácia MOLO (jazero 
Kuchajda) 
 
Prezentácia pre pre všetkých účastníkov Prestigio 
City Triathlon 2014 + firemné 
alebo tímové štafety 
 
  
 
  
 
POČASIE A TEPLOTA VODY 
 
Priemerná denná teplota: 25°C 
 
Teplota vody: 20°C 
 
  
 
  
 
PREDPISY 
 
Na pretekoch Prestigio City Triathlon Bratislava 2014 sa preteká podľa smerníc 
ITU, STÚ a ČTU pre rok 2014 a oficiálneho programu pretekov. Zmeny sú vyhradené. 
 
  
 
TRATE 
 
Prestigio City Triathlon Bratislava - 2014 ETU 
Sprint triathlon European Cup muži, ženy 
 
 0,75 km plávania - 1 okruh na jazere 
Kuchajda 20 km cyklistika - 4 x 5 km okruh - okruh vedie cez ulice Tomášikova, 
Rožňavská, Magnetova a Vajnorská - do nasledujúceho okruhu sa chodí cez 
cyklistické depo 5,4 km beh - 2 x 2,7 km okruh cez Lakeside park - kruhový 
objazd a popri areáli jazera Kuchajda, na pravú ruku zabočíte do areálu Kuchajda 
a cez depo idete do druhého kola 
 
  
 
Prestigio City Triathlon Bratislava 2014 
Medzinárodné majstrovstvá Slovenska vekových kategórií - muži, ženy a Slovenský 
a Český pohár v triatlone, muži a ženy, kluby, rodiny ( podľa SS ČTA leto 2014) 
 
 0,75 km plávania - 1 okruh na jazere 
Kuchajda 20 km cyklistika - 4 x 5 km okruh - okruh vedie cez ulice Tomášikova, 
Rožňavská, Magnetova a Vajnorská - do nasledujúceho okruhu sa chodí cez 
cyklistické depo 5,4 km beh - 2 x 2,7 km okruh cez Lakeside park - kruhový 
objazd a popri areáli jazera Kuchajda, na pravú ruku zabočíte do areálu Kuchajda 
a cez depo idete do druhého kola 
 
  
 
Prestigio City Triathlon Bratislava 2014 Slovenský 
pohár dorastenci/ky a juniori/ky - Český pohár dorastenci/ky a juniori/ky 
 
  
 
0,75 km plávania - 1 okruh na jazere Kuchajda 20 km cyklistika - 4 x 5 km okruh- 
okruh vedie cez ulice Tomášikova, Rožňavská, Magnetova a Vajnorská - do 
nasledujúceho okruhu sa chodí cez cyklistické depo 5,4 km beh - 2 x 2,7 km okruh 
cez Lakeside park - kruhový objazd a popri areáli jazera Kuchajda, na pravú ruku 
zabočíte do areálu Kuchajda a cez depo idete do druhého kola 
 
  
 
Prestigio City Triathlon Bratislava 2014 Slovenský 
a Český pohár mladší a starší žiaci/čky 
 
 0,25 km plávania - 1 okruh na jazere 
Kuchajda 5 km cyklistika - okruh vedie cez ulice Tomášikova, Rožňavská, 
Magnetova a Vajnorská 2,5 km beh - okruh cez Lakeside park - kruhový objazd a 
popri areáli jazera Kuchajda, na pravú ruku zabočíte do areálu Kuchajda a do 
finish line (vedľa reštaurácie MOLO) 
 
  
 
Prestigio City Triathlon Bratislava 2014 
Medzinárodné majstrovstvá Slovenska mix klubových štafiet 3x 0,35 - 5 - 1,4 
(muž, žena, muž pričom každý z nich absolvuje celý skrátený triathlon) 
 
 0,35 km plávania - malý okruh na jazere 
Kuchajda 5 km cyklistika - okruh vedie cez ulice Tomášikova, Rožňavská, 
Magnetova a Vajnorská 1,4 km beh - v areáli jazera Kuchajda 
 
  
 
Prestigio City Triathlon Bratislava 2014 Český 
pohár - deti 8 - 11 rokov 
 
 0,1 km plávanie v jazere Kuchajda 2,8 km 
cyklistika - 2 okruhy okolo jazera Kuchajda 0,8 km beh - 2 x 400m v areáli pri 
jazere Kuchajda 
 
  
 
Prestigio City Triathlon Bratislava 2014 Český 
pohár - deti 6 - 7 rokov 
 
  
 
0,1 km plávanie v jazere Kuchajda 1,4 km cyklistika - 1 okruh okolo jazera 
Kuchajda 0,5 km beh - 2 x 250m v areáli pri jazere Kuchajda 
 
  
 
Prestigio City Triathlon Bratislava 2014 - 
škôlkari 4-5 rokov 
 
 0,3 km beh - pri jazere Kuchajda 
 
  
 
Prestigio City Triathlon Bratislava 2014 škôlkari 
2-3 rokov 
 
0,2 km beh - pri jazere Kuchajda 
 
  
 
  
 
KATEGÓRIE 
 
Prestigio City Triathlon Bratislava - 2014 ETU 
Sprint triathlon European Cup, muži a ženy 
 
  
 
Prestigio City Triathlon Bratislava 2014 
Medzinárodné majstrovstvá Slovenska vekových kategórií - muži, ženy a Slovenský 
a Český pohár v triatlone, muži a ženy 
 
(muži, ženy absolútne poradie) 
0,75-20-5,4 
 
  
 
Prestigio City Triathlon Bratislava Medzinárodne 
majstrovstvá Slovenska klubových štafiet 2014 
 
(bez rozdielu veku) 0,35-5-1,4 
 
  
 
Prestigio Bratislava City Triathlon 2014 - 
Medzinárodné majstrovstvá Slovenska vekových kategórií 
 
Muži - vekové kategórie: 20-29 
0,75-20-5,4 
 
Muži - vekové kategórie: 30-34 
0,75-20-5,4 
 
Muži - vekové kategórie: 35-39 
0,75-20-5,4 
 
Muži - vekové kategórie: 40-44 
0,75-20-5,4 
 
Muži - vekové kategórie: 45-49 
0,75-20-5,4 
 
Muži - vekové kategórie: 50-54 
0,75-20-5,4 
 
Muži - vekové kategórie: 55 + 0,75-20-5,4 
 
Ženy - vekové kategórie: 20-29 
0,75-20-5,4 
 
Ženy - vekové kategórie: 30-39 
0,75-20-5,4 
 
Ženy - vekové kategórie: 40-49 
0,75-20-5,4 
 
Ženy - vekové kategórie: 50 + 0,75-20-5,4 
 
Juniori, juniorky 1995-1996 0,75-20-5,4 
 
Dorastenci, dorastenky 1997 - 1998 
0,75-20-5,4 
 
Starší žiaci, žiačky 1999 - 2000 
0,25-5-2,7 
 
Mladší žiaci, žiačky 2001 - 2002 
0,25-5-2,7 
 
Deti (8-11 rokov) 0,1-2,8-0,8 
 
Deti (6-7 rokov) 0,1-1,4-0,5 
 
Škôlkari (4-5 rokov) 0,0-0,0-0,03 
 
Škôlkari (2-3 roky) 0,0-0,0-0,02 
 
  
 
Po odbehnutí bude pre všetkých účastníkov 
pripravené bohaté občerstvenie. 
 
  
   
 

