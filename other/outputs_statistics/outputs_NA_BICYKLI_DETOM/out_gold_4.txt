

 Pridaj
sa k nám, bicykluj za dobrú vec! Po roku sa cyklistický
pelotón opäť vydáva na potulky po Čechách a Slovensku. Chceš
bicyklovať vedľa Hochschornerovcov, Jara Bekra, Tuliho Vojteka, či
našich hokejistov z NHL? Tak neseď doma a zapoj sa do Medzinárodnej
charitatívnej cyklotour No bicykli deťom 2013! Štartujeme už
6.júna!!! 
 
   Medzinárodná
verejná charitatívna cyklotour si tento rok zapíše už svoj
4.ročník. Brázdiť cesty Slovenska a Česka začíname už 6.júna!
Cieľ medzinárodného charitatívneho  projektu Na
bicykli deťom je
tak ako minulé roky rovnaký -  podporiť a pomôcť detským
pacientom trpiacim onkologickými ochoreniami. 
 
Pomáhať týmto netradičným
spôsobom bude od 6. do 14. júna nielen stála skupina cyklistických
nadšencov, ale súčasťou projektu sa môžete stať aj VY! Stačí
sa pripojiť k pelotónu na akejkoľvek trase a absolvovať
na bicykli akúkoľvek vzdialenosť. 
 
 Pomáhajú
aj naše osobnosti 
 
 
Ak vedľa vás bude náhodou
bicyklovať hokejista Miroslav Lažo či bratia Hochschornerovci, tak
verte, že sa vám to nebude len zdať z únavy a vyčerpania,
ale bude to skutočnosť! Práve naši najúspešnejší vodní
slalomári, viacerí hokejisti z NHL, európskych či
slovenských klubov, tanečník Jaro Bekr, herec Tuli Vojtek a mnohé ďalšie osobnosti
z kultúrneho, športového či politického života sadnú na
bicykel a pomôžu. 
 
 
Trasa je rozdelená do deviatich
etáp. Štart je 6.júna v českom meste Zlín, no a po dvoch etapách
u našich susedov sa presúvame na Slovensko! 8.júna vyrážame z
najvýchodnejšej slovenskej obce Nová Sedlica. Pelotón
postupne prejde naprieč celým Slovenskom, 14.júna dorazí do
Bratislavy. Na našej ceste prejdeme takmer 1000 km a navštívime
vyše 220 miest a obcí. 
 
 
Vďaka tomuto projektu sme minulé
roky finančne podporili šesť najväčších nemocníc na Slovensku
a v Českej republike, teda tie, kde sa lieči najviac
onkologicky chorých malých pacientov. Veľký úspech mali taktiež
rekondično - regeneračné pobyty  v Tatranskej Lomnici. Ani
tento rok tomu nebude inak, vyzbierané financie opäť veľmi pomôžu
malým bojovníkom. Preto veríme, že nielen známi umelci,
športovci, či politici, ale hlavne všetci ľudia opäť dokážu,
že majú srdce na správnom mieste. Vďaka všetkým týmto ľuďom
totiž funguje projekt už štvrtý rok a má veľkú šancu
pokračovať ďalej. A čo je najdôležitejšie, vďaka
projektu  Na bicykli deťom sa onkologicky chorým deťom žije aspoň
o trošku lepšie. 
 

