

Charitatívny projekt na podporu detských onkologických
pacientov Na bicykli deťom s podporou Nadácie Markíza má za sebou prvých
viac ako 100 km. Pelotón vyrazil dnes ráno z dediny Nová Sedlica, ktorá
leží takmer pri ukrajinskej hranici.  













„Cestou nás zdravili deti, dospelí, dokonca v
Základnej škole v Stakčíne kvôli nám prerušili vyučovanie. Ľudia sa k nám
na určitých úsekoch pridávajú a je skvelé cítiť ich podporu aj v tejto o niečo
chudobnejšej časti Slovenska. A musím povedať, že práve títo ľudia,
aj keď sami veľa toho nemajú, sú veľmi štedrí,“ približuje atmosféru 1. etapy Miro Bilík, zakladateľ
občianského združenia Deťom pre život.    
 
Dnešná etapa skončila v Prešova a v priebehu zajtrajška je naplánovaná návšteva detskej onkológie v Košiciach.    
 

