

Druhý deň charitatívnej cyklojazdy Na bicykli deťom patril práve tým, kvôli ktorým celý tento projekt vznikol.  Pelotón na čele s Ivanom “Tuli“ Vojtekom zavítal na oddelenie detskej onkológie v Košiciach, kde deťom a ich rodičom odovzdal darčeky  
















 „Sú
medzi nami rodičia, ktorí si prešli tým, čo teraz zažívajú títo ľudia tu na
detskej onkológii. Preto im rozumieme a vieme, že je to pre nich veľká
pomoc vidieť, že ich niekto podporuje, a že v tom nie sú sami,“ hovorí
Miro Bilík, iniciátor podujatia Na bicykli deťom.    

  Kúsok svojej dobrej nálady deťom a aj
rodičom odovzdal herec Ivan “Tuli“ Vojtek, ktorý stále hovorí, že pre deti  a za deti a ich šťastie sa oplatí
bojovať, hoci aj na bicylkli. „Tak, ako
poznamenala jedna z mamičiek, najväčší prínos tejto akcie je v tom, že im
niekto ukázal aj pozitívny príklad, a teda, že nad onkologickým ochorením
sa dá zvíťaziť, len sa netreba vzdať,“ uzatvára Miro Bilík. 


















  Zajtra sa pelotón presúva do Tatier
a budeme radi, ak sa k nám pridáte. 

  Viac info nájdete aj na facebookovej
stránke Na bicykli deťom. 





