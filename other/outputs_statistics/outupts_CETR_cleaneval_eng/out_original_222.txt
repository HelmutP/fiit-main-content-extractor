




 
Art and Design overview















  
  
    
       
        
           
            
Post 
              16 Studies 
               

            
 
              

          
           
            
Foundation 
              Degree 
               

            
 
              

          
           
            
Art 
              Foundation 
               

            
          
           
            Evening 
              Courses  
               
              
             
          
        
      
      
          
      
    
  
   
   
    
    
       
        
Business 
          I.C.T. Vocational 
           and Education 
           

        
 
           
          

      
       
        
English 
          and Creative Arts 
           

        
 
          

      
       
        
Science 
          and Mathematics

        


      
       
        Humanities 
           
          
        
      
    
      
      
         
          
Accounting 
             

        
         
          
Business 
            Studies  
             

        
         
          
Information 
            Communication 
              Technology

        
         
          
Computing 
            

        
         
          
Advanced 
            Vocational Certificate 
              of Education (AVCE) 
            

        
         
          
AVCE 
            Information and 
              Communication Technology 
              (Double Award) 
            

        
         
          
Intermediate 
            GNVQ in Information 
              Communication technology 
            

        
         
          
AVCE 
            Travel and Tourism 
              (Single Award) 
            

        
         
          
AVCE 
            Travel and Tourism 
              (Double Award) 
            

        
         
          
AVCE 
            Leisure and Recreation 
             (Single Award) 
            

        
         
          
AVCE 
            Business (Double Award)

        
         
          
AVCE 
            Health and Social Care 
             (Double Award) 
            

        
         
          Intermediate 
            GNVQ in  
             Health and Social Care 
             
            
        
      
     
      
      
         
          
Art 
            courses  
             

        
         
           
             Design 
              and Technology 
                (Product 
              Design) 
                
          
        
         
          
Music

        
         
          
Music 
            Technology  
            

        
         
          
Performance 
            Studies  
            

        
         
          
Media 
            Studies  
            

        
         
          
English 
            Language  
            

        
         
          
English 
            Literature  
            

        
         
          English 
            Combined  
             
            
        
      
     
      
      
         
          
Biology 
             

        
         
           
             Physics 
               
          
        
         
          
Chemistry 
            

        
         
          
AVCE 
            Science 
            

        
         
          
Physical 
            Education  
            

        
         
          
Mathematics 
            

        
         
          
Further 
            Mathematics  
            

        
         
          Statistics 
             
            
        
      
     
      
      
         
          
Geography 
             

        
         
           
             History 
               
          
        
         
          
Modern 
            Languages 
             French/Spanish/German  
            

        
         
          
Psychology 
            

        
         
          
Religious 
            Studies  
            

        
         
          
Sociology 
            

        
         
          Law 
             
            
        
      
     
   
    
    
       
        
The 
          University of Liverpool 
           

      
       
        
Liverpool 
          is... 
           

      
       
        
We 
          offer you ... 
          

      
       
        
Courses 
          

      
       
        
Accommodation 
          

      
       
        
Student 
          Insights 
          

      
       
        Applications 
           
          
      
    
   
  
 
  
  
    
       
        
           
            
Learning 
              Mentors 
               
            
            
          
           
            
Group 
              Tutor Programme

            
          
          
            
Reviews

            
          
           
            
Advice 
              &amp; Guidance 
               

            
          
           
            
Learning 
              Difficulties 
               

            
          
           
            Exam Results 
               
              
            
          
        
      
      
    
  
   
   
    
    
       
        
Former 
          Students  
      
      
        
      
    
   
 
  
  
    
       
        
           
            
Enrichment 
              

            
          
           
            
Work 
              Experience  
              

            
          
          
            
Help 
              in the Community 
              

            
          
           
            
Religion 
              &amp; Spirituality 
              

            
          
           
            
Sport 
              &amp; Recreation 
            
            
          
        
      
      
    
  
   
   
    
    
       
        
Sports 
          Fixtures  
      
       
        
      
    
   
  
 
  
  
    
       
        
          
            
Application 
              Process 
               

            
          
           
            
Application 
              Form  
               

            
          
           
            Request 
              a Prospectus 
               
              
             
          
        
      
      
    
  
   
 
  
  
    
       
        
          
            
Study Centre  
               

            
          
           
            
Chapel 
               

            
          
           
            
Student 
              Recreation Area 
               

            
 
              

          
           
            
Computer 
              Suites  
               

            
 
              

          
           
            
Sports/Fitness 
              Suite 
               

            
 
              

          
           
            
Science 
              Labs  
               

            
          
           
            
Media 
              Centre  
               

            
          
           
            
Music 
              Studios  
               

            
          
           
            
Dalton Theatre 
               

            
 
              

          
           
            Art 
              Studios  
               
              
            
          
        
      
       
          
      
    
  
   
   
    
    
       
        
Price 
          Lists/Menus  
      
       
        
      
    
   
  
    
    
       
        
Fitness 
          Suite  
           

      
       
        Equipment 
          List  
           
          
      
    
   
    
    
       
        
Dates 
          &amp; Shows 
      
       
        
      
    
   
    
    
       
        
Equipment 
          List  
      
       
        
      
    
   
 
  
 

   
     
      
         
          
          
          
          
        
         
           
            
               
                
                
                
                
                
                
                
                 
                  
                    Quick Links
                    Application Process
                    Which Course?
                    Post 16 Study
                    Foundation
                    Degree
                    Art Foundation
                    Adult Education
                    Exam Results
                    How To Find Us
                    College Calendar
                    10 Reasons to
                    Choose Carmel
                    Advice &amp; Guidance
                    Former Students
                    Sport &amp; Recreation
                    Vacancies
                  
                
                
                 
              
            
          
        
      
      
         
            
            
               
                 
                  
                     
                       
                        
                           
                             
                              
                                 
                                  
                                
                              
                              
                                 
                                       POST 
                                    16 STUDIES 
                                  

                                
                              
                              
                                 
                                       FOUNDATION 
                                    DEGREES 
                                  

                                
                              
                              
                                 
                                       ART 
                                    FOUNDATION DIPLOMA  
                                  

                                
                              
                              
                            
                          
                        
                         
                        
                           
                            
                          
                        
                        
                           
                                 CAREERS GUIDANCE 
                            

                          
                        
                        
                           
                                 APPLICATION 
                              PROCESS  
                              

                          
                        
                        
                           
                                 APPLICATION 
                              FORM  
                              

                          
                        
                        
                           
                                 REQUEST 
                              A PROSPECTUS 
                              

                          
                        
                        
                           
                                 WHY CHOOSE CARMEL? 
                              

                          
                        
                        
                           
                                 EXAMINATION 
                              RESULTS 
                              

                          
                        
                      
                    
                  
                  
                     
                       
                    
                  
                
              
            
             
          
            
            
               
                
                
                
              
               
                 
                
GO BACK: 
                 
              
               
                 
                
 
                  
                     
                      
                      
                      
                    
                  
                   
                  PLEASE VIEW THE INDIVIDUAL ART SUBJECT YOU ARE INTERESTED IN FOR SPECIFIC DETAILS 
                   Why Choose Art and Design at Carmel? 
                                        In our everyday life we are surrounded by visual imagery, which shapes our experiences and thinking, ranging from advertising and film to music, architecture and fashion. By studying one or two specialist areas in the department, you can make a valuable contribution to the visual world. 
                     
                    We offer a wide variety of specialist AS and A2 levels, where it is possible to explore media and techniques in an imaginative and exciting way.  You have the opportunity to be taught by specialist and well-qualified tutors, with enviable pass rates, who will support you through your time at the college. 
                     
                    Although examinations are important, and hard work and commitment are essential to achieving your artistic aims, a great sense of satisfaction, enjoyment and fulfilment can be achieved by expressing your ideas confidently in the scale or medium you choose.  We, as tutors, are concerned with working with individual students to maximise their potential.                     
                  
                     
                      
                    
                  
                   
                   
                   
                   
                   
                   
                

                 
              
            
            

           
            
               
                
                 
                  
                     
                     
                    
                      
                        
                      
                      
                        
                      
                    
                    
                       
                         
                          
							
                        
                         
                        
                      
                    
                     
                  
                
                
              
            
             
            
               
                
              
            
            
               
                 
                  
                     
                      
 
                      
                    
                     
                       
                        
                           
                            
                            
                            
A-Z 
                              Subject List 
                               
                            
                          
                        
                        
                           
                            
                            
                            
Business, 
                              ICT and Vocational 
                               
                            
                          
                        
                        
                           
                            
                            
                            
English 
                              and Creative Arts 
                               
                            
                          
                        
                        
                           
                            
                            
                            
Science 
                              and Mathematics 
                               
                            
                          
                        
                        
                           
                            
                            
                            
Humanities 
                               
                            
                          
                        
                        

                    
                  
                  
                     
                      
                      
                      
Subjects 
                        in this area 
                         
                         
                      
                    
                  
                  
                     
                      
                      
                      
Art 
                        and Design 
                      

                    
                  
                  
                    
                       
                       
                      Fine Art 
                    
                    
                       
                       
                      Graphic Design 
                    
                    
                       
                       
                      Photography
                    
                    
                       
                       
                      Textiles/Fashion
                    
                    
                       
                       
                      
 Three-Dimensional Design  
                      
                    
                    
                       
                       
                       
                    
                     
                      
                      
                      
Design 
                        and Technology (Product Design) 
                         
                      
                    
                  
				  
                     
                      
                      
                      
Dance 
                         
                      
                    
                  
                  
                     
                      
                      
                      
English Combined 
                         
                      
                    
                  
                  
                     
                      
                      
                      
English Language 
                         
                      
                    
                  
                  
                     
                      
                      
                      
English Literature 
                         
                      
                    
                  
				  
                     
                      
                      
                      
Film 
                        Studies  
                         
                      
                    
                  
                  
                     
                      
                      
                      
Media 
                        Studies  
                         
                      
                    
                  
                  
                     
                      
                      
                      
Music 
                         
                      
                    
                  
                  
                     
                      
                      
                      
Music Technology 
                         
                      
                    
                  
                  
                     
                      
                      
                      
Performance Studies 
                         
                      
                    
                  
                
              
               
                
              
            
            
               
                
              
            
            

        
      
    
  






