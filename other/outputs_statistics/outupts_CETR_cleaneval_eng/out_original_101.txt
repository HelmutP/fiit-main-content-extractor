





   
  
 

   www.whyslopes.com 
  advice &amp; directions for students,
  parents,
  and teachers,
  approximately correct. 
    Even if you have seen a site topic, look through site coverage for 
  unique viewpoints not seen in your earlier days as a student or instructor. 
  Volume 3, Why Slopes and More Math 
   
    All results
    5 results
    10 results
    20 results
    30 results
    50 results
    100 Results
   
  [ Back ] [ Volume Entrance ] [ Next ] 
  22 Complex #'s 
   

 
 
   


 
 









  
    


    
  

 put your ad here. 
 
(V) signals RealPlayer webvideo present. 
 Area Content 
 
Foreword 1. Introduction 2. Second Preview Begins 2 Skier in Motion (V) 2 The Skier (V) 2. Position Dependent (V) 3 Slope &amp; Extrema (V) 4 Single Factor Analysis (V) 4 Two Factor (V) 4 More Factors (V) 4 With Divisors (V) 5 Maxima &amp; Minima Tests 6 Jumps &amp; Discontinuities 8 Review  (optional) 9 On Calculus Studies 11 Slope of Slope 13  Acceleration 14 Limits &amp; Error Control (V) 14 Limit of a Fn. 14. Ltd Error Control 14 Signif. Digits 14 Cauchy Limits 14 Sequence Limits 14 Decimal Arith. 15 What is Slope (V) 15 Slope Calculation (V) 15 Slope, a Limit 15 Tangent Lines 15 Linear Approx., 15 Limits via Algebra (V) 15 Recap. PS.Chain Rule for Polys PS Chain Rule- General  (V) - PS More Chain Rule (V) PS - Sign Analysis (V) 16 What is Velocity 17  What is Area 18 Integration 18 Area Calculation 18  Fn DefN, 6 Ways 19 Logs &amp; Powers 19 Natural Log. 19 Exponential Fn. 20 What's Next 21 Add Vectors 22 Complex #'s 23 Complex #'s 23 Trig Identity 23 Proofs of. 24 Complex Logs etc 

  
    Units in Calculations: 7 Velocity 7 Varying Velocity Example 7. Velocity Calculation 7 Changing Units 7 Same Velocity  Motions 10 Slopes without Units. 10 Units &amp; Slopes 10  Units in Cost vs. Quantity 10  How Units  Appear 10 Unit  Elimination 10 Partial Elimination 10 Interest &amp; Units 12 More on Units Content Guide
  


 
 
      
     




 
Chapter 22 
Complex Numbers - Basic Ideas 
 
  Here is a geometric story which describes the complex numbers, or what
  mathematicians since Gauss in the 1840's have regarded as the complex numbers.
  This geometric story provides a confirmation of the law of signs for real
  numbers. This first part of the story could be explain to someone familiar
  with arithmetic and the measurement of coordinates, rectangular and polar in
  the plane. The second part of this story is an exercise in trig. The second
  part appears in the next chapter. 
   This applet (online only)
  illustrates the ideas in this chapter. 
 
 This first half assumes you have some familiarity with the measurement of
distances and angles, with the addition of real numbers and points in the plane,
and finally with multiplication of nonnegative (that is zero and positive) real
numbers.
 
 
   
  1. The immediate motivation for this approach (in this chapter) stems
  from three successive 1976 McGill University public lectures of the late
  Richard Feynman. He simply described physics as the addition and
  multiplication of arrows in the plane. He defined their multiplication as
  follows: add their angles and multiply their lengths. In terms of the
  polar coordinate [r1,q1]
  and [r2,q2] for the
  factors, the polar coordinates of the product is [r,q]
  = [r1r2,q1+q2]
  .)
   All this was effectively presented to a general audience with no mention of
  vectors nor the Gauss-Argand representation of complex numbers. See the
  foreword.
   
 2. In Morris Kline's three-volume work Mathematical Thought from
  Ancient to Modern Times, see in volume 2, Chapter 27, the third
  section called The Geometrical Representation of Complex Numbers. This
  section briefly describes the approach of Caspar Wessel (1745-1818). Part of
  Wessel's work (translated into English) is reproduced in David Eugene Smith's
  1929 work A Source Book in Mathematics, Dover 1959 Reprint.
 
 
  
 
 Points in the Plane 
Addition
 The sum of two points with the rectangular coordinates (a,b)
and (c,d) is given by (a+c,b+d). We
therefore write
 

  
    
      
        
          (a,b)+(b,d)
            = (a+c,c+d)
        
      
    
  

For example (2,5)+(6,2) = (8,7).
 In words, the addition rule is simple add the rectangular coordinates of
the summands to get the rectangular coordinates of the sum. With this in
mind, the following question is easy: What are the rectangular coordinates of
the sum of (1,14) and (2,8)? Answer: (1,14)+(2,8) = (1+2,14+8) = (3,22). The
chapter Arrow Addition discusses the addition of points or arrows in
the plane further.
 
 Multiplication 
 Next we define using polar coordinates the product of two points in the
plane. Each point or factor is located by means of angular displacement or
rotation from the positive real axis, and also a nonnegative distance from the
origin. The product of two points is given by a third point. Its angular
displacement is the sum of the angular displacement of the factors. Its distance
to the origin is the product of the distances of the factors. This is the add
the angles and multiply the lengths rule. In polar coordinate notation, the
multiplication rule and definition is indicated by
 

  
    
      
        
          [r1,q1]?[r2,q2]
            = [r1r2,q1+q2]
        
      
    
  

(As in the previous chapter, square brackets are used to indicate polar
coordinates while round brackets indicate rectangular coordinates.)
 Example. Two arrows are to be multiplied. One has length 1.3 and angle
22.62?; the other factor has length 1.026
and angle 46.97?; and so their product
has length 1.3338 = 1.3?1.026 and angle 69.69?
= 22.62?+46.97?;
and that is it. See the following diagram.
 
  

 
 Another Example. The product of the two points [3,80?]
and [4, 60?] is [(3)(4), 80?+
60?] = [12,140?]
 
 Stop For a Summary 
 The addition of points in the plane is given by means of their rectangular
coordinates while multiplication is given in terms of polar coordinates. It is
an exercise in trig to obtain expressions for the rectangular coordinates of a
product in terms of the rectangular coordinates of the factors. See the next
chapter. (Note that while your reading of this chapter requires a mastery of the
algebraic way of writing and thinking, you could explain with diagrams and
examples only, the ideas in this chapter to someone without this mastery. It is
the on-paper communication which requires the mastery.)
 
 Introducing the Complex Numbers 
 Points in the plane with the operations of addition and multiplication just
given are called the complex numbers. The plane with these two operations on its
points is called the complex numbers plane, or more briefly the complex numbers.
 
 We will now change to a more standard notation for them. We may and often
will write the rectangular coordinates z = (a,b) as z
= a+ib, We will further call the abscissa a, the real part
of the complex number z = a+ib. We will also call the
ordinate b, the imaginary part of the complex number z = a+ib.
 
 We will say that the complex number z = a+ib is purely
imaginary when its real part a = 0. The angle of a purely imaginary
complex number z = a+ib = 0+ib = (0,b) is 90
degrees or 270 degrees (modulo
 
 
  FOOTNOTE: Two quantities x and y are equal modulo a third
  quantity c, if and only if their difference x-y
  = kc for some whole number or integer k.
 
360 degrees), depending on the sign of the imaginary part b. When b
&gt; 0, the angle is 90 degrees (modulo 360 degrees). When b &lt; 0, the
angle is 270 degrees (modulo 360 degrees).
 We will also say that z = a+ib is (purely) real when its
imaginary part b is zero. The angle of a (purely) real complex number z
= a+ib = a+i0 = (a,0) is 0 degrees or 180
degrees (modulo 360 degrees), depending on the sign of the real part a.
If a &gt; 0, this angle is 0 degrees (modulo 360 degrees) while if a
&gt; 0, this angle is 180 degrees (modulo 360 degrees).
 
 Real Numbers as Complex Numbers 
 Each complex number z = a+i0 with imaginary part zero
gives and is given by a real number a. We will write z = a
in this situation, and say that the complex number z is also a real number.
 
 With this practice, the real numbers can be regarded as a subset of the
complex numbers; and the real number line can be identified with the horizontal
axis of the plane.
 
 Confirmation of The Law of Signs 
 We identify the real number line with the horizontal axis of the plane. With
this identification, observe that positive numbers have angular displacement
zero, modulo 360 degrees. Also observe that negative numbers have angular
displacement 180 degrees, modulo 360 degrees. The magnitude of a real number is
its distance to the origin.
 
 Suppose z = a+i0 and w = c+i0. We
want to compute the product zw with the multiply the lengths, add the
angles rule. Each factor has length |a|
or |c|. Each factor
has angle 0 or 180 degrees (modulo 360 degrees). The relationships
  
   
 
 
 0? = 0?+0?
 
 180? = 0?+180?
  = 180?+0?
 
 360? = 180?+180?
  = 0? (modulo 360?) 

imply the add the angles, multiply the lengths rule for the
multiplication of complex numbers agrees with the ordinary method for
multiplying real numbers and the law of signs. The relationship in particular
imply
 
   (+1) = (+1)(+1) as 0? = 0?+0? 
 
   (-1) = (+1)(-1) = (-1)(+1)
    as 180? = 0?+180?
    = 180?+0?
 
   (-1)(-1) = (+1) as
    360? = 180?+180?
 
 
Examples and then some further comments may reinforce these ideas. For the first
example, the number 4 is now identified with the point (4,0) = [4,0?]
= [4,360?]. This number or point has
distance 4 to the origin and angle of 0?,
modulo 360 degrees, with the horizontal axis:
  
 
 For the second example, the number -2
is identified with the point (-2,0) = [2,180?].
See the figure below.
 
 
 
 Now multiplying the point [2,180?]
by itself leads to the product [2,180?]2
= [22,180?+180?]
= [4,360?] = [4,0?].
Thus the point on the horizontal axis identified with -2
when squared gives the point identified with +4 indicated above. The 360 degrees
in the diagram for the number or point 4 = (4,0) represents the doubling of the
angle 180 degrees.
 
 For an example or exercise, compute the pairwise products of 3=3+0i, 4=4+0i,
-3=-3+i0 and -4=-4+0i using the add the angles, multiply the lengths rule.
 
 Remark. The add the angles, multiply the length rule could be
used to define the product of real numbers to people/students who know (a) about
the addition of real numbers or coordinates and (b) about the multiplication of
non-negative numbers. They would not need to have any previous knowledge of the
law of signs. 
 
 
  FOOTNOTE: The add the angles, multiple the lengths rule for the
  multiplication of complex numbers thus yields a rule for the multiplication of
  real numbers once the multiplication of positive numbers with themselves or
  zero is understood/defined.
 
 More Exercises. Compute the following using the multiply the lengths,
add the angles rule:
 
 
    
   
 
A = (1.5)?(2).  
   
B = (1.5)?(-2).  
   
C = (-1.5)?(-2). 
   
D = (1.5)?(-2). 
   
E = [10,45?] ?[[1/20],15?]. 
 
Note each factor gives a point or arrow in the coordinate plane.
 Stop For A Summary. The polar coordinate definition
 

  
    
      
        
          [r1,q1]?[r2,q2]
            = [r1r2,q1+q2]
        
      
    
  

of the product of two point in the plane, involves the multiplication of lengths
(= distances to the origin) and the addition of angles. For points on the
horizontal axis, the angles of the factors are zero or 180?
(modulo 360?). Computing the angle of the
product will involve one of the following expressions:

  
    
      
        
          0?+0?

        
      
    
    
      
        
          =
        
      
    
    
      
        
          0?

        
      
    
  
  
    
      
        
          0?+180?

        
      
    
    
      
        
          =
        
      
    
    
      
        
          180?

        
      
    
  
  
    
      
        
          180?+0?

        
      
    
    
      
        
          =
        
      
    
    
      
        
          180?

        
      
    
  
  
    
      
        
          180?+180?

        
      
    
    
      
        
          =
        
      
    
    
      
        
          360?

        
      
    
  

Since the angle 180 degrees is associated with -1, and the angles 0 and 360
degrees are both associated with the number +1, the polar coordinate definition
of multiplication of points in the plane agrees with (or yields) the law of
signs for the multiplication of positive and negative numbers.
 Square Root of -1 
 The real number -1 = -1+0i
= [1,180?] has angle 180 degrees (mod 360
degrees) and length 1. The purely imaginary number (0,1) = 0+i1 = [1,90?]
has angle 90 degrees and length 1. Multiplying this point or number by itself,
that is, squaring it, gives the point with length 1 ?1 = 1 and angle 90?+90?
= 180?. So the product equals -1+0i
= -1. We call i = the principal square root of
-1.
 
 A second square root of -1 is obtained as follows. The imaginary number (0,-1)
= 0+i(-1) = [1,-90?]
has angle -90 degrees and length 1. Multiplying this
point or number by itself, that is squaring it, gives the point with length 1
times 1 =1 and angle (-90?)+(-90?)
= -180? = 180?
(mod 360?). So this product equals -1+0i
= -1 as well.
 
  
 
 This provides two square roots of -1 as both [1,+90?]2
= [1,+180?] = -1
and [1,-90?]2
= [1,-180?] = -1.
 
 Square Roots of Other Complex Numbers 
 The square root of a positive number or zero are real nonnegative numbers. I
assume in the following that you know how to compute these square roots. The
square roots of negative numbers and of other arrows or points in the coordinate
plane depend on this ability.
 
 Observe that squaring points in the plane doubles their angular displacements
and squares their magnitudes (distance to the origin). That is, the add the
angles, multiple the lengths rule gives
 

  
    
      
        
          [r?,
          1
             
            2
          
q]?[r?,
          1
             
            2
          
q] = [r
            ,q]
        
      
    
  

Therefore the arrow [r?,[1/2]q] when
squared (meaning multiplied by itself) yields [r,q]
. So it is called a square root of the arrow [r,q].
Another square root is located by the polar coordinates [r?,[1/2]q+180?]
since [r,q] = [r,q+360?]
both locate the same point in the plane. You should consider the special case of
positive numbers z = a+i0 = [a,0?]
where the angle q = 0 degrees.
 Exercises.
 
 
   Find all the square roots of 4 and -4 and plot them. 
   Find the cube roots of 27 and -27 and plot them in the plane. 
   Find the square roots of \cis(45?)
    = cos(45?)+isin(45?)
    = [1,45?]. 
 
 Complex Conjugates 
 The complex conjugate of a complex number z = a+b i
with polar coordinates r = ?[(a2+b2)]
and q is the complex number [`(z)]
= a-b i with polar coordinates r
and -q.
 
 *   Exercise. Show multiplying a complex number a+b
i by its conjugate a-b i
gives the nonnegative number r2 = a2+b2.
 
  
 
  
 
 Conjugates and Reciprocals 
 Observe that p = [(a)/(r2)]-i[(b)/(r2)]
= [1/(r2)][`(z)] has angle -q
and length [1/(r)]. Here p = [1/(r2)][r,-q]
= [[1/(r)],-q].) Multiplying number p =
[[1/(r)],-q] by z = [r,q]
gives the complex number [1,0] with length 1 and angle 0, that is, the real
number 1. And multiplication of any point (c,d) by 1 = [1,0?]
yields back the point (c,d)
 
 The reciprocal (or multiplicative inverse) of the complex number z = a+b
i with length r &gt; 0 and angle q is
the complex number p with length 1/r and angle -q.
    
 
  
 
 Observe that if r &gt; 1 then the length of the reciprocal [1/(r)]
&lt; 1 &lt; r, that is, the length of the reciprocal is less than 1 and
the length of the original number. In contrast, if 0 &lt; r &lt; 1 then
[1/(r)] &gt; 1 &gt; r. Question: Which of these two cases is
represented in the above diagram? What happens in the case r = 1?
 
 Two Algebraic Properties 
Observe 

  
    
      
        
          [r1,0]?([1,q]?[r2,q2])
            = [r1,q1]?[r2,q2]
        
      
    
  

since [r1,0]?([1,q]?[r2,q2])
= [r1,0]?[r2,q1+q2]
= [r1r2,q1+q2]
. Similarly 

  
    
      
        
          [1,q]?([r1,0]?[r2,q2])
            = [r1,q1]?[r2,q2]
        
      
    
  

 Real Multiples of Arrows 
We said earlier (in the last section of the chapter Arrow Addition) for
real numbers a, b and c that c?(a,b)
= (ca,cb) without any reference to or use of the add the
angles, multiply the lengths arrow multiplication rule. But c = c+i0
= (c,0) gives a point in the plane. So we can multiple c = c+i0
= (c,0) and (a,b) = [r,q]
using the add the angles, multiply the lengths rule. Two cases, more
precisely possibilities, will be examined.
 Case 1: c ? 0 Observe for c
&gt; 0 that c = c+i0 = [c,0] has angle 0 degrees and
length c = |c|.
Thus the add the angles, multiply the lengths multiplication rule
yields
 

  
    
      
        
          
c?(a,b) = [c,0]?[r,q]
            = [cr,0+q] = [cr,q]
            = (ca,cb)
        
      
    
  

as before.
 Case 2: c &lt; 0 Now c = -d
&lt; 0. But d &gt; 0 implies
 

  
    
      
        
          (d,0)?(a,b) =
            [d,0]?[r,q] = [dr,q]
            = (da,db)
        
      
    
  

Therefore

  
    
      
        
        
        
          
            
              
                (c,0)?(a,b)
              
            
          
          
            
              
                =
              
            
          
          
            
              
                (-1)?[d,0]?(a,b)
                  = (-1)?[dr,q]
              
            
          
        
        
          
            
              
                
              
            
          
        
        
          
            
              =
            
          
        
        
          
            
              [dr,q+180?]
                = (-da,-db)
            
          
        
        
        
          
            
              
                
              
            
          
        
        
          
            
              =
            
          
        
        
          
            
              (ca,cb)
            
          
        
        
      

    
  

again.
 Conclusion. Multiplication of a point (a,b) by a real
number c = c+i0 with and without the add the angles,
multiple the lengths rule gives (ca,cb).
 
 Some Vocabulary. 
For each point or complex number z = a+b i = (a,b)
= [r,q] in this plane, we say that a is
the real part of z; that b is the imaginary part of z;
that r = |z|
= ?[(a2+b2)] is
the magnitude, modulus or absolute value of z (different
texts prefer different terms); and that q is the
angle or argument of z.
 Remark.  The use of round brackets () in the notation for
rectangular coordinates (a,b) stems from the convention
in many algebra texts written before this one. The use of square bracket [] in
the notation for polar coordinates [r,q]
here was chosen simply because the square brackets were available. In
retrospect, cosmetic appearance alone would suggest the employment of
round-brackets for polar coordinates and square brackets for rectangular
coordinates. The development of notation is not always cosmetically optimal.
 Three Problems. 
 
   Locate in the plane the complex conjugate and reciprocals of the complex
    three numbers s = 3+4i, t = 12+(-5)i,
    and z = cos(120?) +isin(120?).
 
      Locate the three complex cube roots of 1 (unity) .Hint:
    divide the unit circle into three arcs each spanning an angle of 360/3
    =120 degrees. The required roots are at the ends of each arc (if two
    arcs share the endpoint 1 = 1+i0.
 
     Locate the fourth, fifth and sixth roots of unity. What is the
    general pattern for n-th roots of unity (where n = 2, 3, 4, ?).? 
 
 






 


 

     
 
      
     

 What is a Variable? 
Introduction 
Variation between Examples 
Variation of Letters 
A letter denotes a variable 
Cases of Double Variation 
Three Notions of a Variable 
Constants,Parameters,Variables 
Talking about numbers 
 Arithmetic Videos 
 
Fractions 
Primes 
Greatest Common Divisors 
Least Common Multiples 
Square Root Simplification 
  
Online Volumes 
 
1Elements of Reason 
1A. Pattern Based Reason 
1B. Mathematics
Curriculum Notes 
2. Three Skills for
Algebra 
3. Why
Slopes and 
More Math 
 
 More Site Areas:  
 Helping
Your Child or Teen Learn.   
 Analytic
Geometry 
 Solving
Linear Equations with Stick Diagrams   
 Fractions, 
Ratios, Rates, Proportions 
  &amp; Units  
 Euclidean
Geometry 
 Analytic
Geometry 
 Number
Theory.  
 Calculus
Intro  
 Complex
Numbers 
 Odds &amp; Ends 
 Quebec
Maths Education  
 Real
Analysis  
 LaTeX2HotEqn  
 
 Good news: Site pages  identify what you need
to study. 
 Bad news: Site pages do not explain
everything  
 
 Worse news: Learning takes time, yours.  

 
 Site Message: Calculus
&amp; calculus preparation give
a first reason &amp; focus for learning and teaching most mathematics in school
and  college. 








      
 
 [Top] [ Back ] [ Volume Entrance ] [ Next ]  
site entrance   site
reviews 
 Feedback
and Contact Form  Site uploaded with SmartFTP. 
Favourite Sites:  BBC News 
and the Mathematics
portion of  English
National Curriculum   
Francais: ||D?finition
d'une variable || Alg?bre
|| Arithmetique ||
Logique | |  
 
 All trademarks and copyrights
on this page are owned by their respective owners. Copyright to comments &amp;
contributions are owned by the Poster. The Rest ? 1995 onward by site author
Alan Selby, Ph. D.  All Rights Reserved. 

 






