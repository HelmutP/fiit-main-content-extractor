



 
 Matchtech Group plc 
 human resources for a technical world 
 
 
set text size · site map · contact us · 
help
 


 
 Log In 
 
 Matchtech Home 
 Industry Sectors 
 About Us 
 Overview of Matchtech 
 Investor Services 
 Graduates 
 
 
 
Candidate Services 
 Quick Job Search 
 Advanced Job Search 
 Careers In Recruitment 
 Candidates Register Now 
 
 
 
Employer Services 
 Search for Candidates 
 Submit a Vacancy 
 Strategic Services 
 
 
 
 
 Power Nuclear Industry News 
 
 
RWE offers municipal utilities stake in new power plant
 
Energy Business Review 6 November 2006 16:25 
 
Iberdrola gets Quatar power plant contract
 
Energy Business Review 6 November 2006 16:25 
 
Wind farm for Pietramontecorvino
 
E4: Engineering 6 November 2006 15:58 
 
Scottish and Southern Energy's lonely energy research investment
 
Datamonitor 6 November 2006 15:48 
 
RWE offers municipal utilities stake in new power plant
 
Datamonitor 6 November 2006 15:48 
 
Iberdrola gets Quatar power plant contract
 
Datamonitor 6 November 2006 15:48 
 
James Fisher sets up new company
 
Westbury 6 November 2006 15:29 
 
Spain limits conditions on Endesa deal
 
The Times 5 November 2006 23:19 
 
Billion dollar brains
 
Engineer Online 3 November 2006 20:07 
 
Fortum seeking Loviisa license extension
 
Energy Business Review 3 November 2006 05:31 
 
Fortum seeking Loviisa license extension
 
Datamonitor 2 November 2006 21:57 
 
PaR Nuclear wins refuelling contract
 
Engineer Online 2 November 2006 11:06 
 


 
 
 

 
 Related to Power &amp; Nuclear 
 
 
Building Structures
 
 
Construction Management
 
 
Controls &amp; Automation
 
 
Facilities Maintenance
 
 
Petrochemical
 
 
Water
 
 

 

Choose Industry Sector
 



Architecture
Building Services
Building Structures
Construction Management
Environmental Engineering &amp; Science
Geotechnical Engineering
Highways &amp; Transportation
Railway Infrastructure
Surveying


Aerospace
Automotive
Controls &amp; Automation
Defence
Electronics &amp; Software Systems
Facilities Maintenance
Food &amp; Drink
General Design Engineering &amp; FMCG
Manufacturing Engineering
Marine
Offshore &amp; Subsea
Overseas Vacancies
Petrochemical
Pharmaceutical &amp; Biotechnology
Power &amp; Nuclear
Production &amp; Skilled Trades
Production Maintenance
Quality
Rail Vehicles
Telecoms &amp; Broadcast
Water


IT Home
Banking, Finance &amp; Insurance
Development
ERP &amp; CRM
Helpdesk
IT Management &amp; Executives
IT Sales
Support &amp; Training


Science Home
Clinical Science
Environmental Laboratories
Food &amp; Drink
Petrochemical
Pharmaceutical &amp; Biotechnology
Physics &amp; Materials
Power &amp; Nuclear
Water


Graduates
Human Resources &amp; Personnel
Logistics &amp; Distribution
Procurement &amp; Supply Chain
Public Sector
Sales &amp; Marketing



 
 Power &amp; Nuclear 
 
Industries » Engineering » Power &amp; Nuclear Jobs
 
 
 Our Power &amp; Nuclear Division provides specialist expertise in both permanent and contract engineering recruitment for design, construction, installation and maintenance throughout the power generation and nuclear reprocessing industries. 
 Our experience covers a wide range of client types, from generators of both nuclear and conventional power through to EPC contractors, consultants and special purpose equipment manufacturers. 
 Our dedication to providing professionals who exactly match individual client needs has been the foundation of our continuing success - with independent confirmation of our abilities being proven through our achievement of Preferred Supplier status with a great many clients. 
 Candidates who are interested in Power &amp; Nuclear careers can register now to apply online for any of the advertised vacancies or send a copy of their CV to powernuclear@matchtech.com. 
 
  


 
 Search for jobs 




Keywords or Job Id — help
 



Contract or permanent
 

Either
Contract
Permanent





Within Industry sector
 

 -- All Sectors -- 

Architecture
Building Services
Building Structures
Construction Management
Environmental Engineering &amp; Science
Geotechnical Engineering
Highways &amp; Transportation
Railway Infrastructure
Surveying


Aerospace
Automotive
Controls &amp; Automation
Defence
Electronics &amp; Software Systems
Facilities Maintenance
Food &amp; Drink
General Design Engineering &amp; FMCG
Manufacturing Engineering
Marine
Offshore &amp; Subsea
Overseas Vacancies
Petrochemical
Pharmaceutical &amp; Biotechnology
Power &amp; Nuclear
Production &amp; Skilled Trades
Production Maintenance
Quality
Rail Vehicles
Telecoms &amp; Broadcast
Water


All IT Jobs
Banking, Finance &amp; Insurance
Development
ERP &amp; CRM
Helpdesk
IT Management &amp; Executives
IT Sales
Support &amp; Training


All Science Jobs
Clinical Science
Environmental Laboratories
Food &amp; Drink
Petrochemical
Pharmaceutical &amp; Biotechnology
Physics &amp; Materials
Power &amp; Nuclear
Water


Graduates
Human Resources &amp; Personnel
Logistics &amp; Distribution
Procurement &amp; Supply Chain
Public Sector
Sales &amp; Marketing




 





 
 

 
  
 

 
Summary of all jobs in this sector
 
 Power &amp; Nuclear Jobs 
 
 
 The 20 most recent Power &amp; Nuclear jobs are listed below. Please refine your search or click "Show more" at the bottom of this page to see more. 
 
 
  

Power &amp; Nuclear jobs RSS feed — What is an RSS feed?  


 
 
Location: Cardiff

HOT!
C&amp;I Engineer — Contract — Similar Jobs
 



Status: Applicants Required
Rate: £28.00 to £32.00/hour

View &amp; apply




 
 Details Important: Strong background in instrument design in clean conditions
 Reporting to the Design Manager the job holder is required to take responsibility for the design, testing and commissioning of the control and instrumentation systems on a new multi-million pound process plant. 
 
 
 
Location: Cheshire

HOT!
Expeditor — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £30,000 to £40,000
 


View &amp; apply




 
 Details Important: Expediting experience
 The companys is an established international process contractor, building and designing facilities in the UK and overseas.  They have a worldwide reputation for excellence in executing technology driven projects across a wide range of business. 
 
 
 
Location: Cheshire

HOT!
Senior Buyer — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £30,000 to £40,000
 


View &amp; apply




 
 Details Important: Solid Purchasing Experience
 The Company is an established international process contractor, building and designing facilities in the UK and overseas.  they have a worldwide reputation for excellence in executing technology driven projects across a wide range of business sectors. 
 
 
 
Location: Cheshire

HOT!
Project Procurement Manager — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £35,000 to £50,000
 


View &amp; apply




 
 Details Important: PPM skills in oil/chemical industry
 The company is an established international process contractor, building and designing facilities in the UK and overseas.  We have a worldwide reputation for excellence in executing technology driven projects across a wide range of business areas 
 
 
 
Location: Aberdeen

Panel Wirer — Contract — Similar Jobs
 



Status: Applicants Required
Rate: £12.00 to £13.50/hour

View &amp; apply




 
 Details Important: Previous experience
 Our client requires a panel wirer to work a 6 month contract based in Aberdeen. 
 
 
 
Location: Aberdeen

Procurement Officer — Permanent — Similar Jobs
 



Status: Applicants Required (CVs being submitted)
Salary: £20,000 to £25,000
 


View &amp; apply




 
 Details Important: Procurement knowledge
 We are looking for candidates who are skilled in the above duties or similar duties and using the SAP system or equivalent in a similar working environment to work for a Subsea Engineering company. 
 
 
 
Location: Teesside

Machinist — Contract — Similar Jobs
 



Status: Applicants Required
Rate: £9.30 to £15.91/hour

View &amp; apply




 
 Details Important: Time or Apprentice served
 We are looking for two Machinists for a contract role lasting in to 2007. 
 
 
 
Location: Aberdeen

Purchasing Specialist — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £20,000 to £30,000
 


View &amp; apply




 
 Details Important: Procurement knowledge/experience
 We are recruiting for one of the premier subsea engineering companies in Aberdeen. They are looking fot a Purchasing specialist to join the team as a result of a high workload. 
 
 
 
Location: Aberdeen

Sub-Contracts Administrator — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £30,000 to £40,000
 


View &amp; apply




 
 Details Important: Admin / Commercial experience
 We are recruiting for an international energy services company, providing a range of engineering, production support, maintenance management and overhaul and repair services to the oil &amp; gas, and power generation industries worldwide. 
 
 
 
Location: Aberdeen

Commercial Assistant — Permanent — Similar Jobs
 



Status: Applicants Required (CVs being submitted)
Salary: £20,000 to £30,000
 


View &amp; apply




 
 Details Important: IT Literate
 We are recruiting for an international energy services company, providing a range of engineering, production support, maintenance management and overhaul and repair services to the oil &amp; gas, and power generation industries worldwide. 
 
 
 
Location: Fleet

Procurement Manager — Permanent — Similar Jobs
 



Status: Applicants Required (CVs being submitted)
Salary: £40,000 to £50,000
 


View &amp; apply




 
 Details Important: Procurement Managerial Experience
 The Southern Regional Construction Business is  planning to expand from 30 to 50 million over the next few years. They are seeking to recruit a Procurement Manager to take responsibility for all procurement activities within the region 
 
 
 
Location: Tyneside

Mechanical Engineer — Permanent — Similar Jobs
 



Status: Applicants Required
Salary:  from £25,000
 


View &amp; apply




 
 Details Important: Prepared to work in the north east
 Mechanical Engineers required who can provide multi-disciplined technical solutions to a wide range of industries. 
 
 
 
Location: Swindon

Project Manager — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £45,000 to £60,000
 


View &amp; apply




 
 Details Important: Proven track record in the Project Management of complex projects.
 Our client is responsible for the successful delivery of a large investment programme in both conventional and renewable power projects. 
 
 
 
Location: International

Customer Support Engineer (Mechanical) — Permanent — Similar Jobs
 



Status: Applicants Required (CVs being submitted)
Salary:  from £25,000
 


View &amp; apply




 
 Details Important: Mechanical maintenance
 Mechanical Field Service Engineer required with installation and fault finding experience 
 
 
 
Location: International

Customer Support Engineer (Electrical) — Permanent — Similar Jobs
 



Status: Applicants Required (Now Interviewing)
Salary:  from £25,000
 


View &amp; apply




 
 Details Important: electrical experience
 Field Service Engineer required with strong electrical and control systems experience 
 
 
 
Location: Aberdeen

Expeditor — Permanent — Similar Jobs
 



Status: Applicants Required (CVs being submitted)
Salary: £25,000 to £30,000
 


View &amp; apply




 
 Details Important: Expediting Materials Experience
 We are recruiting for and Expeditor on behalf of an offshore diving equipment company in Aberdeen. 
 
 
 
Location: Selby

Instrument Engineer — Contract — Similar Jobs
 



Status: Applicants Required
Rate: £30.00 to £36.00/hour

View &amp; apply




 
 Details Important: Power exp
 Design, specification, installation, testing and commissioning of new and replacement process instruments interfacing to monitoring/control systems for the Power Generation business. 
 
 
 
Location: Reading

Optics Specialist — Permanent — Similar Jobs
 



Status: Applicants Required
Salary:  negotiable
 


View &amp; apply




 
 Details Important: Physics background
 Optics Specialist required for Project Orion, an exciting and ambitious new project to construct a world leading laser facility for the creation and study of hot, dense matter. 
 
 
 
Location: Surrey

Civil &amp; Structural Design Manager — Permanent — Similar Jobs
 



Status: Applicants Required
Salary: £50,000 to £60,000
 


View &amp; apply




 
 Details Important: 15 years experience
 Our client is a worldwide supplier of industrial gases and equipment, specialty and intermediate chemicals, and environmental and energy systems. A senior engineer is being sought to take on a management role. A company car/car allowance is provided. 
 
 
 
Location: Various

Field Service Engineer — Permanent — Similar Jobs
 



Status: Applicants Required
Salary:  up to £40,000
 


View &amp; apply




 
 Details Important: Service experience
 Commissioning, fault finding to component level and servicing the range of products manufactured and supplied by the company and associated companies.  The job requires extensive travel both within the UK and world wide. 
 
 
 
  The 20 most recent jobs in this sector are listed above. Please refine your search or click "Show more" to see the rest.  
 Show more 
  You can also view all vacancies in this sector. 
 


 Designed and Developed by Matchtech Group plc © 2002-2006 Matchtech Group plc. Company Registration No.4426336. All rights reserved · Terms of Access ·  Disclaimer · Privacy Statement
 



