
 Writing, Speaking, Listening, Interviewing, Communication, Negotiation Skills
      communication skills
       

   Center for Strategic Leadership Studies 

          Use Ctrl-F to Find word/phrase on this or other browser pages. 
         
 Back to Leadership, Ethics, and Command Central 
              
                Being Interviewed by the Media  
                Working with Difficult People 
                Writing for Publication 
                Fallacies in Logic 
              
                  See also:

                Cross-Cultural Communication 
                English as a Second Language 
              
                Negotiation Center of Excellence 
                Center for Negotiation Studies
                 crisis &amp; hostage negotiation
                 alternative dispute resolution
      
                Trial Advocacy Skills
                  ...questioning, rebuttal, presence 
           
                Risk &amp; Crisis Communication 
             
                Citing Online Sources 
               
                Creativity and Thinking Skills 

                Seminars &amp; Small Groups 





        please see disclaimer about links, and privacy and security notice ... 
      contact us 
       
 page updated/reviewed 17 Dec 06 


        Communications, in General 
           Effective Communication: ?If Anything Can Go Wrong, It Will?, by Pine 
          and Bauman, from AU-24 
           Leaders Communicating Effectively, by Kline, from AU-24 
           Symbolic Leadership: The Symbolic Nature of Leadership, by Vickrey, 
          from AU-24 
           Understanding Your Communication Style (local copy), 
             from SBA - includes typical mottos, behaviors, verbal cues, nonverbal cues, 
          confrontation style, etc. for each of three basic communication styles 

            Aggressive 
            Passive 
            Assertive 
           Communicating in Style: Discover How to Communicate with Everyone 
          (and Like It!) (local copy), 
            by Barrett, of PinnacleOne, presentation 
          at 2003 CMAA National Conference, posted by GSA Project Management 
          Center of Expertise 
          Effective Communication (local copy), 
            FEMA independent study course 
          Staff Work: Methods and Applications (local copy), 
            AFSC Pub 1, 2000, 
          including problem-solving, briefings, and papers 
          Headquarters Army Action Officer's Online Reference - Communicating 
          Communication, Management Benchmark Study (local copy), 
            Dept of Energy 
          -- includes chapters on networking, alliances, organizational culture, 
          and innovation 
        Speaking 
            Rudyard Kipling, from "The Elephant's Child" in Just So Stories 
            I keep six honest serving-men
            (They taught me all I knew);
            Their names are What and Why and When 
            And How and Where and Who.

          [ed. - answering those six questions is often a good place to begin] 
           Speaking Effectively (local copy), 
             by John Kline 
           Preparing the Mind, Body, and Voice (local copy), 
             The Army Lawyer, 
          Nov 2003 - rehearsing and warming up aren't just for lawyers 
          Briefing Guide (local copy), 
            US Army Sergeants Major Academy - 
          outlines for 18 types of briefings and staff estimates 
          USMC Command and Staff College Written and Oral Communications Guide 
          (local copy) 
          Preaching and Communication (local copy), 
            The Army Chaplaincy, 
          Summer-Fall 1997 - summarizes some basic theories and points of view 
          regarding rhetoric, such as 
             
 Cicero outlined five principles of rhetoric that would later 
            influence preaching for centuries. In them, the speaker: 
              discovers what should be said (invention) 
              arranges the speech in a particular order (arrangement) 
              clothes the thoughts with language (style) 
              secures the speech in (memory) 
              effectively (delivers) the speech 
            Cicero's Brutus or History of Famous Orators and The Orator [both from 
          Project Gutenberg] 
           Tips for Preparing Scientific Presentations, 
             Office of Naval Research 

          - includes the Ten Commandments of Visual Aids (such as Powerpoint 
          slides) 
          - delivery chapter has section on handling Q &amp; A after speaking 
          - most of the material applies to almost any presentation, not just 
          techical 
           Effective Presentations (local copy), 
             Army Corps of Engineers 
          Effective Speaking and Presentation: 
            Selling Ideas, Gathering Support, 
          Motivating Audiences (local copy), by Lee, in PM magazine, Jan-Feb 
          2001 
          Language - An Introduction to the Study of Speech , 
            by Edward Sapir -- 
          philosophical and academic examination 
          Toastmasters International 
            10 Tips for Successful Public Speaking 
          Key Steps to an Effective Presentation, 
            with emphasis on proper vis 
          aids 
          How to Conquer Public Speaking Fear, 
            written by medical doctor 
          Participating in Seminars, 
            by Johnston 
          Evaluating Speaking 
          Tips on the Critiquing of Writing and Speaking (local copy), 
            by Kline 
          and Lippincott 
        Storytelling &amp; Use of Narrative 
          The Use of Storytelling in the Department of the Navy (local copy) 
              Conveying information in a story provides a rich context, remaining 
            in the conscious memory longer and creating more memory traces than 
            information not in context. Therefore a story is more likely to be 
            acted upon than normal means of communications. Storytelling, 
            whether in a personal or organizational setting, connects people, 
            develops creativity, and increases confidence. The use of stories in 
            organizations can build descriptive capabilities, increase 
            organizational learning, convey complex meaning, and communicate 
            common values and rule sets. 
             
 Description capabilities are essential in strategic thinking and 
            planning, and create a greater awareness of what we could achieve. 
            Fictional stories can be powerful because they provide a mechanism 
            by which an organization can learn from failure without attributing 
            blame. 
             
 With the advent of the Internet and Intranet, there is a larger 
            opportunity to use stories to bring about change. Electronic media 
            adds moving images and sound as context setters. Hypertext 
            capabilities and collaboration software invites groups, teams and 
            communities to co-create their stories. New multiprocessing skills 
            are required to navigate this new world, skills that include the 
            quick and sure assimilation of and response to fast-flowing images 
            and sounds and sensory assaults. 
             
 In summary, when used well storytelling is a powerful 
            transformational tool in organizations, one that all of our managers 
            and leaders across the Department need to utilize. 
          NASA's ASK Talks with Dr. Gary Klein - 
            use of storytelling, even 
          internally, to improve decision making and problem solving and 
          development/use of expertise 
          "Story Model of Decisionmaking" - explained with examples (starting on 
          PDF page 30) in A Literature Review of Analytical and Naturalistic 
          Decision Making (local copy), 
            by Zsambok, Beach, and Klein, for Naval 
          Command, Control and Ocean Surveillance Center, Dec 1992 
             
 According to the theory, the story coordinates three types of 
            knowledge: 
               
 facts or information from the current situation 
               
 knowledge about similar situations 
               
 generic expectations about what makes a complete story, such as 
              believing that people do what they do for a reason 
             
 Given a set of known facts in an unfolding situation, knowledge 
            about similar situations, and expectations about what is needed to 
            make a complete story, the decisionmaker can know when important 
            information is missing, and where inferences must be made. 
             
 Construct a story 
             
 Evaluate story for coverage - concerns the extent to which the story 
            accounts for evidence 
             
 Evaluate story for coherence 
               
 consistency - concerns the extent to which the story does not 
              contain contradictions 
               
 plausibility - concerns the extent to which the story is 
              consistent with real or imagined events in the real world 
               
 completeness - concerns the extent to which a story has all of its 
              parts 
          Storytelling and Terrorism: Towards a Comprehensive 'Counter-Narrative 
          Strategy' 
            by Casebeer and Russell, in Strategic Insights, Mar 2005 
          (Local Copy) 
          Storytelling that moves people. A conversation with screenwriting 
          coach Robert McKee, 
            abstract with PubMed, at National Library of 
          Medicine 
             
 In this conversation with HBR, Robert McKee, the world's best-known 
            screenwriting lecturer, argues that executives can engage people in 
            a much deeper--and ultimately more convincing--way if they toss out 
            their Power-Point slides and memos and learn to tell good stories. 
            As human beings, we make sense of our experiences through stories. 
            But becoming a good storyteller is hard. It requires imagination and 
            an understanding of what makes a story worth telling. All great 
            stories deal with the conflict between subjective expectations and 
            an uncooperative objective reality. 
          Malignants in the Body Politic: Redefining War through Metaphor, 
            SAAS paper, 2004 
          The Center for Narrative Studies 
          The Art of Trial Advocacy: the Art of Storytelling (local copy), 
            in The Army Lawyer, Oct 1999 
             
 recommends three ways to enhance your storytelling for effect 
               
 use the present tense 
               
 speak in clear, active English 
               
 engage the senses of the audience 
          Storytelling and the Art of Teaching, 
            by Pederson, in State 
          Department's Forum, Jan-Mar 1995 
          Deep Impact Storytelling, 
            by Deacon and Murphey, in State Department's 
          Forum, Oct-Dec 2001 
          Storytelling: Passport to the 21st Century, 
            by Brown et al 
          Narrative Psychology Internet and Resource Guide, 
            by Hevern, Le Moyne College 
          Learn Storytelling 
            - including their use to communicate complex ideas 
          Tell Me a Story: Why Stories are Essential to Effective Safety 
          Training (local copy), 
            NIOSH Publication No. 2005-152, August 2005 
          Story Telling, 
            National Park Service Community Tool Box 
             
 Use it if ... 
               
 You want to help people begin working together: An engaging story 
              will serve as a unifying emotional and experiential tool. 
               
 You are trying to develop a vision and need to first find 
              agreement as to what people believe is important. 
        Listening 
            See also building rapport 
           
 See also Neuro-Linguistic Programming (NLP) 
           
 "A man who listens because he has nothing to say can hardly be a 
          source of inspiration. The only listening that counts is that of the 
          talker who alternatively absorbs and expresses ideas." -- Agnes 
          Repplier 
           Listening Effectively (local copy), 
             by John Kline 
           Practice Listening Skills (local copy) 
             - a quick checklist from the 
          Office of the Dispute Resolution Specialist, Dept of Veteran Affairs 
            
 Ten Commandments of Good Listening - as posted by the Wildland Fire 
          Leadership Development Program 
             
 the first ten - from K. Davis, Human Behavior at Work, McGraw Hill, 
            1972 
               
 Stop talking. Obvious, but not easy. 
               
 Put the speaker at ease. Create a permissive, supportive climate 
              in which the speaker will feel free to express himself or herself. 

               
 Show a desire to listen. Act interested and mean it. 
               
 Remove distractions. External preoccupation is less likely if 
              nothing external is present to preoccupy you. 
               
 Empathize. Try to experience to some degree the feelings the 
              speaker is experiencing. 
               
 Be patient. Give the speaker time to finish; don't interrupt. 
               
 Hold your temper. Don't let your emotions obstruct your thoughts. 
               
 Go easy on argument and criticism. Suspend judgment. 
               
 Ask questions. If things are still unclear when a speaker has 
              finished, ask questions which serve to clarify the intended 
              meanings. 
               
 Stop talking. In case you missed the first commandment. 
             
 additional listening techniques - from P. Bradley and J. Baird, 
            Communication for Business and the Professions, Brown, 1980 
               
 Preparation. If you know what the topic is ahead of time, learn 
              something about it so you will not be an ignorant listener. Even 
              some careful thinking will allow you to listen more accurately 
              when the communication actually begins. 
               
 Seek intent. Try to discover the intent of the source; why is he 
              or she saying these things? 
               
 Seek structure. Look for an organizational scheme of the message. 
              If the speaker is an accomplished one, you won't have to look very 
              hard; it will be obvious. But if the speaker is less skilled, the 
              responsibility falls to you. 
               
 Analyze. Do not accept what you hear at face value; analyze what 
              the speaker is saying and pay attention to body language. 
               
 Focus. Keep the main topic of the message in mind at all times, 
              using it to bring focus to the information which the speaker 
              supplies. 
               
 Motivate yourself. This may be the most important. Listening takes 
              work, and to do that you may have to "psych yourself up." 
          International Listening Association 
            Quotes on Listening
              , by the International Listening Association 
          Listening Skills - Self-Evaluation Test 
          Listening Skills Self-Evaluation 
          Listening and Empathy Responding 
          Crisis Intervention: Using Active Listening Skills in Negotiations 
          (local copy), 
            by Noesner and Webster, in the FBI Law Enforcement 
          Bulletin, August 1997 
        Interviewing 
            Remember - interviewing can be face-to-face, by phone, by fax, by 
          e-mail, by video teleconference, or by other means as technology 
          continues to change 
           
 See also listening 
           
 See also building rapport 
           
 See also Oral History(ing) on History page 
           
 See also being interviewed by media below 
           
 See also interrogation and deception detection below 
           
 Cognitive Interviewing 
             Cognitive Interview Technique (local copy), 
               from NTSB - relies more 
            on lengthy narrative responses 
               
 The traditional interviewer asks many questions, each of which 
              elicits a very brief response. The cognitive interview, on the 
              other hand, is in some ways a questionless interview. The goal is 
              to ask as few questions as possible so that witnesses give you 
              long narrative responses that each contains that much more 
              information than a traditional interview. The objective is to try 
              to elicit information, not extract information. The good 
              interviewer tries to create a social environment so the witness 
              generates information without having to wait for questions to be 
              asked. 
             How to Conduct a Cognitive Interview: a Nutrition Education Example 
            (local copy), 
              by Shafer and Lohse, for USDA 
            Cognitive Interviewing at the National Center for Health Statistics 
            (local copy), 
              CDC briefing slides, by Beatty et al 
            Verbal Reports are Data! A Theoretical Approach to Cognitive 
            Interviews (local copy),
              Federal Commission on Statistical 
            Methodology, 1999 research conference paper, by Conrad, Blair, and 
            Tracy 
            From Impressions to Data: Increasing the Objectivity of Cognitive 
            Interviews (local copy),
              Proceedings of the Section on Survey 
            Research Methods, American Statistical Association, 1996, by Conrad 
            and Blair, posted by Bureau of Labor Statistics 
            Perceptual and Memory Distortion During Officer-Involved Shootings 
            (local copy), 
              by Artwohl, in Law Enforcement Bulletin, Oct 2002 
               
 The developers of this method found that how investigators 
              interview individuals can significantly impact the ability of the 
              witnesses to remember and report the details of an event. Their 
              research indicated the cognitive interview as the most effective 
              technique for facilitating memory retrieval with cooperative 
              witnesses. Using proper interview techniques is particularly 
              important for high-stress situations because during experiential 
              thinking, the individual is more likely to be dissociative and 
              ?encodes reality in concrete images, metaphors, and narratives,? 
              whereas, in rational thinking, the individual is more logical and 
              ?encodes reality in abstract symbols, words, and numbers.? 
             
 As cognitive interviewing involves thinking aloud on the part of the 
            interviewee, one should keep in mind the following item (which may 
            also apply to cultures other than Asian) from 
               
 The Geography of Thought: How Asians and Westerners Think 
              Differently? and Why, by Richard Nisbett, 2003 
                 
 To test the possibility that Asians and Asian Americans in fact 
                find it relatively difficult to use language to represent 
                thought, Kim had people speak out loud as they solved various 
                kinds of problems. This had no effect on the performance of 
                European Americans. But the requirement to speak out loud had 
                very deleterious effects on the performance of Asians and Asian 
                Americans. 
                (page 210 of Free Press edition) 
           Interviewing Techniques (local copy), from EPA 
              gives details and tips on the five phases below, as well as getting 
            info from reluctant witness, and using interpreters 
              introduction 
              rapport 
              questions 
              summary 
              close 
            Office of Naval Inspector General (see investigation on military law 
          page) 
            Investigations Manual - Chapter on Interviewing (local copy), (PDF) 
              - includes telephone interviews 
             Investigations Guide - Chapter on Interviewing (local copy), (PDF) 
              excellent expanded checklists - includes telephone interviews 
            Slides for Factfinding - Interviews (local copy) 
          Living History Project: Instructions for Interviewing (local copy), 
            Library of Congress 
          Using Structured Interviewing Techniques (local copy), 
            GAO guide 
          Interviewing Strategies: How to Get People to Talk to You (local 
          copy), 
            emphasis on obtaining medical history, but useful points for 
          any interviewing situation (downloaded from 
          cim.usuhs.mil/gsn0514/Interviewing%20and%20History%20Taking.PDF) 
          Witness Interviews - Guidelines 
            from the Air Force Safety Center 
          (local copy) 
          Air Force Pamphlet 91-211 USAF Guide to Aviation Safety Investigation 
              Attachment 5 - Techniques for Conducting Witness Interviews 
             
 Attachment 6 - Spouse/Friend Interview Guide 
        Being Interviewed by the Media 
            See also interviewing above 
          DoD Guides for Interacting with Media 
            JP 3-61, Appendix A - Guidelines for Discussions with the Media 
            (local copy) 
            Meeting the Media - A Guide to Encountering the Media (local copy),
              U.S. Army Training and Doctrine Command's Pocket Guide to 
            Encountering the Media 
            Meeting the Media (local copy), 
              handbook from USAF Public Affairs 
            Center of Excellence (PACE) 
           Interviews: a Closer Look (local copy), 
             USINFO, State Department 
             
 Assessing the Interview Request - including 5 best tips 
             
 Establishing Ground Rules 
             
 Once the Interview Is Agreed To - including 5 best tips 
             
 During the Interview - including 5 best tips 
             
 Staying Focused 
             
 Being Effective on Television 
             
 After the Interview 
           Media Traps and Pitfalls - Avoiding Common Mistakes (local copy), 
             FBI Academy briefing 
           Preparing for a Successful Media Interview: a Systematic Approach for 
          Success (local copy), 
            USMC TECOM briefing 
           Media Skills Training (local copy), USMC 
              Why should you talk to the media? 
               
 American public opinion directly influences all levels of warfare 
               
 We have intrinsic value to the Nation and our existence is 
              dependent on the will of the American people 
               
 If we don't tell our story, no one will 
               
 The media will tell the story with or without our input ... 
          Media and the Military (local copy),
            Chapter 7 from the Joint Services 
          Warrant Officers? Course, Defence Academy, Ministry of Defense, UK - 
          ?Concentrate on what you want to say - don?t become mesmerised by what 
          you might be asked? 
          Military Justice and the Media: The Media Interview (local copy), 
            by Schwenk, posted by USAFA Department of Law - examines interviews, 
          pretrial publicity, and related matters 
          6 tips for taking control in media interviews, 
            by Krotz, Microsoft 
          Small Business Center - see article for expansion of tips below 
            Set goals for every appearance. 
            Nothing is 100% off the record. 
            Watch your body language. 
            Stay on track with your message. 
            Learn how to "bridge." 
            Prepare take-aways. 
          Oh, the Mistakes Spokespeople Make: Ten Sure-Fire Ways to Blow an 
          Interview, by Bennett 
            - click link above to see expansion of points below 
            Misunderstanding the Media 
            Misunderstanding the Spokesperson Role 
            Lacking Message Points 
            Unleashing a Core Dump 
            Over-Answering 
            Failing to Listen 
            Speaking in Jargon 
            Missing the "So What?" 
            Trashing Competitors 
            Playing Tug of War 
        Interrogation 
            See also interviewing 
           
 See also NLP 
           
 See also deception detection 
           
 See also interrogations and interviewing on Lessons Learned page 
           
 See also torture on Law page 
          FM 34-52 Intelligence Interrogation, 
            1992 version 
           
 NOTE: some sources believe the 1987 version below was more permissive 
          than the 1992 version above - however, even in the 1987 version below 
          you can see the prohibition against force. 
          Principles of Interrogation, 
            in Chapter 1, FM 34-52 (1987 version, now 
          superceded) - included the following 
             
 Interrogation is the art of questioning and examining a source to 
            obtain the maximum amount of usable information. The goal of any 
            interrogation is to obtain usable and reliable information, in a 
            lawful manner and in the least amount of time, which meets 
            intelligence requirements of any echelon of command. 
            ... 
             
 The use of force, mental torture, threats, insults, or exposure to 
            unpleasant and inhumane treatment of any kind is prohibited by law 
            and is neither authorized nor condoned by the US Government. 
            Experience indicates that the use of force is not necessary to gain 
            the cooperation of sources for interrogation. Therefore, the use of 
            force is a poor technique, as it yields unreliable results, may 
            damage subsequent collection efforts, and can induce the source to 
            say whatever he thinks the interrogator wants to hear. However, the 
            use of force is not to be confused with psychological ploys, verbal 
            trickery, or other nonviolent and noncoercive ruses used by the 
            interrogator in questioning hesitant or uncooperative sources. 
             
 The psychological techniques and principles outlined should neither 
            be confused with, nor construed to be synonymous with, unauthorized 
            techniques such as brainwashing, mental torture, or any other form 
            of mental coercion to include drugs. These techniques and principles 
            are intended to serve as guides in obtaining the willing cooperation 
            of a source. The absence of threats in interrogation is intentional, 
            as their enforcement and use normally constitute violations of 
            international law and may result in prosecution under the UCMJ. 
             
 Additionally, the inability to carry out a threat of violence or 
            force renders an interrogator ineffective should the source 
            challenge the threat. Consequently, from both legal and moral 
            viewpoints, the restrictions established by international law, 
            agreements, and customs render threats of force, violence, and 
            deprivation useless as interrogation techniques. 
          Reducing a Guilty Suspect?s Resistance to Confessing: Applying 
          Criminological Theory to Interrogation Theme Development (local copy), 
            by Boetig, in the FBI Law Enforcement Bulletin, August 2005 - 
          discusses theme-based interrogation and criminological theories 
          Strategies to Avoid Interview Contamination (local copy), 
            by Sandoval, 
          in the FBI Law Enforcement Bulletin, October 2003 - some good tips, 
          strategies, and questions 
          Criminal Confessions - Overcoming the Challenges (local copy), 
            by 
          Napier and Adams, in the FBI Law Enforcement Bulletin, November 2002 - 
          includes following principles/tips 
             
 follow the facts 
             
 identify personal vulnerabilities 
             
 know the suspect 
             
 preserve the evidence 
             
 adjust moral responsibility 
             
 use psychology versus coercion 
             
 allowing suspects to maintain dignity is professional and increases 
            the likelihood of obtaining a confession 
          Conducting Successful Interrogations (local copy), 
            by Vessel, in the 
          FBI Law Enforcement Bulletin, October 1998 
             
 Obtaining information that an individual does not want to provide 
            constitutes the sole purpose of an interrogation. 
          Magic Words to Obtain Confessions (local copy), by Napier and Adams, 
          in the FBI Law Enforcement Bulletin, October 1998 
              Magic words come from three commonly used defense 
            mechanisms-rationalization, projection, and minimization 
               
 Rationalize Suspects? Actions 
               
 Project the Blame onto Others 
               
 Minimize the Crime 
               
 Provide Reasons to Confess 
          Interviewing Self-confident Con Artists (local copy), 
            by O'Neal, in 
          the FBI Law Enforcement Bulletin, March 2001
           
 - "with the proper preparation and strategic approach, investigators 
          can take advantage of the character traits of con artists" 
          Investigative Techniques: Federal Agency Views on the Potential 
          Application of Brain Fingerprinting" (local copy), 
            GAO report, Oct 
          2001 
          Hypnosis in Interrogation, 
            by Deshere, in Studies in Intelligence, 
          Vol.4, No.1 
        Deception Detection 
            See also deception on Info Ops page 
           
 See also interviewing and NLP sections 
           
 See also interrogation 
          Distributed Information and Intelligence Analysis Group (DI2AG), 
            within Thayer School of Engineering at Dartmouth College 
            Deception Detection publications 
          How to Identify Misinformation, 
            USINFO.STATE.GOV, 27 July 2005 - with 
          examples of misinformation and conspiracies, and support material 
          Detecting Online Deception and Responding to It (local copy), 
            by Rowe, 
          Naval Postgraduate School 
          Detecting Deception, 
            by Adelson, in Monitor on Psychology, Jul-Aug 
          2004 - includes discussion of software which can analyze written 
          content for lying 
          Intuitive people worse at detecting lies,
            by Young, NewScientist.com, 
          18 Mar 2002 
             
 People who think of themselves as being intuitive make worse lie 
            detectors than those who do not trust in a "gut instinct", according 
            to new research. 
             
 One possible explanation is that intuitives in fact rely on common 
            misconceptions about how to spot a liar, he says. 
          A Four-Domain Model for Detecting Deception - An Alternative Paradigm 
          for Interviewing (local copy), 
            by Navarro, in the FBI Law Enforcement 
          Bulletin, June 2003 
             
 They can use an alternative paradigm for detecting deception based 
            on four critical domains: 
              comfort/discomfort 
              emphasis 
              synchrony 
              perception management 
          Detecting Deception (local copy), 
            by Navarro and Schafer, in the FBI 
          Law Enforcement Bulletin, July 2001 
          Interviewing Self-confident Con Artists (local copy),
            by O'Neal, in 
          the FBI Law Enforcement Bulletin, March 2001
           
 - "with the proper preparation and strategic approach, investigators 
          can take advantage of the character traits of con artists" 
          Investigative Techniques: Federal Agency Views on the Potential 
          Application of Brain Fingerprinting" (local copy),
            GAO report, Oct 
          2001 
          Hypnosis in Interrogation, by Deshere, in Studies in Intelligence, 
          Vol.4, No.1 
          Microexpressions 
          Truth Wizards Can Detect Lies (local copy),
            in the Maine Law Officer's 
          Bulletin, Nov 2004 - detecting the subtle signs that people reveal 
          when they lie 
          Lying and Deceit - The Wizards Project,
            Police Psychology Online 
             
 "With 20 minutes of training, we are able to significantly improve 
            someone's ability to recognize microexpressions which are involved 
            in many kinds of lies," Dr. O'Sullivan said. 
           
 Paul Ekman 
            Lying Faces - One Man Studies Them, 
              by Garrett 
               
 Discover Magazine [Jan 2005 issue] reports Ekman is working with 
              the Department of Defense on software that could detect liars by 
              studying facial emotions, called micro-expressions, that go 
              unnoticed by the untrained eye. 
               
 "They look just like an ordinary expression, except they're only 
              on the face about a 25th of a second," this researcher observes. 
              Ekman has shown that certain emotions flash almost undetectably 
              when people are telling high-stakes lies, where they benefit or 
              lose a lot. 
            Paul Ekman 
              home page 

            Darwin, Deception, and Facial Expression,
              by Ekman, in Annals of the 
            New York Academy of Sciences - discusses the Facial Action Coding 
            System (FACS) 
             
 Ekman, O?Sullivan, &amp; Frank. "A few can catch a liar." Psychological 
            Science, 10, 263-266, 1997 
             
 Ekman, P. &amp; O?Sullivan, M. "Who can catch a liar?" American 
            Psychologist, 46, 913-920 , 1991 
             
 Ekman, Paul. Telling Lies: Clues to Deceit in the Marketplace, 
            Marriage, and Politics, W.W. Norton &amp; Company, 3rd revised edition 
            2002 
                                                                                                        
