














Global Direct Investment Solutions - Home Page









































































































































































































































































































































































































































































  
    
    
    

    
     
    Global Direct Investment Solutions 
     
    Corporate Development for a Networked World 

    
     
    
    On The Short List 

  
  
    
     
    
    The Global Direct Investment Forum 

  
  
    
     
    The Global 
    Direct Investment Foundation 

  
  
    
     
Invest From Anywhere, To Anywhere 

    
    Contact :  
    
    TEL   847-304-4655 
    Bruce Donnelly  
    
    bruce@gdi-solutions.com    (Biography) 
    
  
  
    
     
      

  










 
                       
 




 

  
    
     Search or refer to our many
    directories and other content for 25,000+ resources 
    to support business site selection decisions in the USA and worldwide.  
    This website has attracted over 1 
    million visits in the 3 years since it was launched.    
    Just ask us 
    for free help reflecting over 15 years of experience in this niche and 
    thousands of contacts. 
     Our independent research and referral work is free for 
    executives who are facing business expansion decisions because we are supported by leading professionals who can deliver 
    valuable services to more executives than they might reach through their own 
    professional networking activities, events, and exclusive marketing efforts. 
     We can also perform custom market research projects.  
    We welcome suggestions, support for our work, and relevant
    reciprocal links. 
     
    Our goal is to make it easier for executives and their advisors to plan and 
    implement their business expansion projects to achieve 
    their strategic and ROI goals faster and better. 
 We 
    deliver well-qualified introductions to the professionals and information 
    resources they need at the time, reflecting the research and "lessons 
    learned" from many other projects.. 

    
    We personally  
    introduce 
    executives for free to valuable local market knowledge and professional 
    contacts as an independent global referral service (like a concierge in this 
    niche) through market research and the development of working relationships with 
    : 
        Top executives facing strategic choices and 
      capital investment decisions about 
      where they will do business : where to establish, acquire, expand, move or 
      restructure their operations or alliances as markets and plans change. 
        Economic development professionals representing 
      communities and regions whose leaders are highly motivated to attract and retain 
      growing companies and develop globally competitive business clusters. 
        Professional service providers who assist 
      executives with capital 
      investment project plans in North America, Europe, Asia or worldwide.  
      These may be local specialists or global experts in foreign direct 
      investment (FDI) projects worldwide. 
     
     We also perform US 
    advertising sales work for fDi - Foreign Direct Investment magazine, 
    published by the Financial Times group. 
 
    
     

  
  
    
    A 2007 North 
    American Location Guide will be published with the February 2007 issue  and featured 
    online throughout 2007 for quick reference.  This supplement will also 
    be distributed at MIPIM 2007.   It will become part of the new 
    fDi Atlas guide to global business 
    locations.  Contact us for more information.  

    
    

    
    
    

  
  
    
     
    The North American "Cities of the Future" 
    award will be featured in the April 2007 issue, similar to the 
    
    June / July 2005 issue.  Nominations end December 31, 2006.  
    This issue will be distributed at the BIO 2007 conference in 
    Boston in May. 
 
    
    
    fDi : Foreign Direct Investment magazine by FT Business (The 
    Financial Times group) is the leading global publication focusing on foreign 
    direct investment - including investment into North America from other 
    regions of the world, and vice versa.  That includes cross-border 
    competition among US states and Canadian provinces. 
 
     
    fDi 
    magazine is a trusted reference tool for executives and their advisors.  Contact 
    us about 
    
    advertising in fDi magazine .  TEL 847-304-4655 in Chicago. 
    Editorial calendar and rates 
 
    
    It reaches "C-level" executives and professionals in North America 
    (40%), 
    Europe (40%), and Asia (20%) who are responsible for corporate development strategies, 
    organizational change, and capital investment project location decisions 
    worldwide.  85% of readers are CEO's or CFO's.  80% are at large 
    companies (&gt;500 employees). 
 
    
    The fDi website attract 30,000+ visits 
    per month as a very targeted publication for top executives.  We 
    attract 50,000+ visitors per month, so together we should reach over 1 
    million in 2007. 

    
     
    
     
 
    
     
 
      
 
    
    
    Intro : about fDi magazine 
 
    
    
    www.fdimagazine.com 
     
     
 
      
 
    Search back issues of 
    fDi 
 
    Foreign Direct Investment 
 
      
 
    Profile 
    and how to advertise. 
 
    Ad Recall : fDi 
    advertisers 
 
    Examples : Who reads fDi? 

    
     
    
     
     
    
    June / July 2005 feature 
     
    2005
    
    US winners and 
     
    
    US Regional finalists 
       
     
    
    Canada winners and
    
    finalists 
     
    
    Mexican winners in 2005 
     
    
    Award presentations 
   
 
    We 
     
    welcome enquiries from senior
    
    executives who would like to subscribe to fDi, and from professional service 
    providers who specialize in this market. 

  
  




  
    The December 
    issue of fDi magazine will be distributed at the
    
    World Economic Forum annual meeting in Davos, Switzerland, January 
    24-28, 2007.
    
    The February 2007 issue of fDi magazine will be distributed 
    at MIPIM in Cannes, including the 2007 North 
    American Location Guide supplement.

    
     
    fDi magazine will also be distributed at BIO 2007 in 
    Boston and at other major events during the next year. 
 
    Editorial and event plan, rates 

    
     
    fDi magazine was a sponsor and exhibitor at the CoreNet Global Summit in Orlando 
    Florida and at the IEDC 2007 Annual 
    Conference in New York. 

  





  
    
     
     
     You can also search this  
      website for over 25,000 relevant research sources in this niche market, 
      including content which highlights national, regional, state, and 
      local economic development organizations and professional service 
      providers.  Let us help you to find what you 
    are seeking. 
       
    
    
       
        
          
            

            
            
            
             
            
            
            
            
             
             
            Search WWW  
             
            
            Search 
            www.gdi-solutions.com              
            This website  
             
             
            Search 
            www.ontheshortlist.com                 
            On The Short List 
             
             
            Search 
            www.partners4technology.com       
            Partners4Technology 
             
            
            Search 
            www.fdimagazine.com                   
            fDi - Foreign Direct Investment (FT Business) 
               
             
             Tip : See Google "Cheat 
    Sheet" help page for advanced web search features. 

          
        
       
    
    
    
     
     This Google site search tool is available by the Search button 
    at the top of each page.  Other custom search features are 
    provided through  
    www.OnTheShortList.com to make it easy 
    to selectively search many other relevant sites.  The 
    directory pages on this website keep related 
    content together so that it can be browsed quickly and easily (unlike 
    repeated database form queries).  GUIDE content such as  
    Service Profiles,
    Area Profiles and
    Area Surveys are 
    executive summary presentations to highlight the benefits of particular 
    business locations or professional services.   They may link 
    to other websites about a location or service, and provide direct links to 
    information of common interest to our professional contacts. 

  
  




  
    Try the 
    site search feature above, or our 
    Directories of economic development 
    agencies and other resources.  This site 
    provides executive summary content, website links, and contacts for over 25,000 
    business locations, service providers, and other relevant resources worldwide.  There are 
    also 
    unique custom search features 
    at  www.OnTheShortList.com 
    for business location research through the contents of this website and many other 
    resources.  
    
     
    Our 7 year 
    analysis of 
    investment project trends by state and our analysis of 
    economic development lead generation costs 
    per job created may also be of interest. 
 
    Our 
    April 2006 newsletter (4 pages 
    with graphs) summarizes this market research and analysis, including a 12 
    year chart of major project announcements. 

  
  
    
     Our work is analogous to a concierge  service, 
    personally assisting executives to invest in the growth of their companies anywhere 
    by introducing valuable contacts in confidence. 
 We organize and share valuable market 
    knowledge and networks of contacts in a globally consistent way through targeted market research 
    and relationship development work. 
  This 
    helps company executives, community leaders, and service providers to meet. 
  
    Contact us 
    (TEL 847-304-4655) to discuss business expansion interests in confidence. 

    
     
    Our independent marketing, research,  relationship development, 
    and well-qualified referral work can share knowledge of service capabilities 
    and benefits to advance the marketing interests of : 
     
       
 
    Economic development professionals 
       
       
 
    Professional services providers 
     
       
     
     
    We can introduce relevant executives and their advisors to 
    the benefits you offer.  See how to  
    request a relevant directory listing (free), and our suggestions for
    reciprocal links. 
    
  
  




  
    
     State economic development professionals who 
    are responsible for attracting business investment and providing site 
    selection assistance to executives and their consultants were invited in 
    April 2006 to provide an informative article on this simple topic : "Why 
    should a business invest in your state?" 
     The article content and size 
    was entirely discretionary to share their point of view as a free service to 
    our 500,000+ visitors in 2006. 
     We publish their
    
    "Point of View" articles here for convenient reference by executives.  
    Other replies will be published once they are received from the remaining 
    state officials. 
    
    
    "Point of View" articles were contributed by many 
    state economic development officials : Why invest 
    in ?  Alabama 
    Idaho 
    Iowa 
    Missouri  
    New Hampshire 
    New Jersey 
    North Dakota 
    Oklahoma 
    Oregon 
    Pennsylvania 
    Utah  
    Virginia 
    Wisconsin 
     We have added relevant links to other content we provide 
    from these article pages. 
 Many states did not yet 
    reply to this free offer, despite reminders and assurances that they 
    intended to do so. 

  
  
    
    
    
      State directories of US regional, county, city, and 
    utility economic development organizations and chambers of commerce which 
    promote new business investment   
     
    Northeast         
    NY  
    CT  
    MA  
    RI  
    NH 
    VT 
    ME 
     
    Mid-Atlantic      
    PA  
    NJ  
    MD  
    DE  
    DC  
    WV  
    VA  
    NC 
     
    Southeast        
    SC  
    GA  
    FL  
    AL  
    MS  
    TN 
     
    Great Lakes     
    KY  
    OH  
    MI  
    IN   
    IL  
    WI  
    MN 
     
    North Central    
    MO 
    IA  
    KS  
    NE  
    ND 
    SD 
     
    South Central   
    TX  
    OK  
    AR  
    LA 
     
    Mountain         
    AZ  
    NM 
    CO  
    UT   
    WY  
    ID  
    MT 
     
    West Coast     
    CA 
    NV  
    OR  
    WA  
    HI  
    AK 

    
     Economic 
    development directory - alphabetical list by state for convenience 
     Same links as 
    provided by the abbreviations at left (not familiar to all foreign visitors) 
     
    See how to request a relevant 
    directory listing  
     Alabama  
    Alaska  
    Arizona 
    Arkansas 
    California 
    Colorado 
    Connecticut 
    Delaware 
    Florida 
    Georgia  
    Hawaii 
    Idaho 
    Illinois 
    Indiana 
    Iowa 
    Kansas 
    Kentucky 
    Louisiana 
    Maine 
    Maryland 
    Massachusetts 
    Michigan 
    Minnesota 
    Mississippi 
    Missouri 
    Montana 
    Nebraska 
    Nevada 
    New Hampshire 
    New Jersey 
    New Mexico 
    New York 
    North Carolina 
    North 
    Dakota  Ohio 
    Oklahoma 
    Oregon 
    Pennsylvania 
    Rhode Island 
    South Carolina 
    South 
    Dakota  Tennessee 
    Texas  
    Utah 
    Vermont 
    Virginia 
    Washington  
    West Virginia  
    Wisconsin  
    Wyoming 

  
  




  
    
    Were you at the BIO 2006 conference in Chicago, or 
    will you be at other events where we could meet?  We attend various trade shows and 
    professional events each year for networking purposes.  
    Contact us 
    (TEL 847-304-4655)  if you would like to meet to introduce ourselves. 

    
    Our Event Recall 
    feature makes it easy to discover which areas are promoting themselves to 
    target industries through events such as trade shows.  Our 
    biotech directory is one example. 
    There is also an Ad Recall 
    feature to highlight areas which are known to advertise, such as to easily 
    find their ads, websites, and contacts through search tools.  A new Ad 
    Recall feature highlights 
    advertisers in fDi magazine. 

  
  
    
    Newsletter - October 
    2005  (.pdf) Tips on using Google for business location selection 
    searches, with information about 2005 progress and our 2006 marketing plans.

    
    Newsletter - November 2005  (pdf) Launching 
    our new website 
    www.OnTheShortList.com and 
    our 
    new custom Google site search features - 
    Area Search,
    USA Search,
    CRE Search, 
    etc. 

  
  
    
     A new book in 2006 by a veteran site selection consultant 
    may also be of interest. 
 
    Location 
    Location Location : A Plant Location and Site Selection Guide 

    
    We expect 550,000+ visitors in 2006 to take advantage of the 
    extensive 
    information we share about economic development agencies and professional services.  
    That is up 175%+ from 200,000 
    visits in 2004 - the year when we launched this website.  
    See our 
    daily visit or 
    monthly visit graphs and 
    regional analysis.  After only 3 years, our visit levels already 
    exceed most of the specialty magazines in this market even though they have 
    been in circulation for 20 - 50 years.

  
  

  
    
     
    This is a  personal service 
    for executives and advisors - not just website 
    content and links.   Tell us what you need to find for your 
    project, and we will help you in confidence. 
     Although we make it easy to find thousands of useful 
    resources worldwide through this website, we primarily share market 
    knowledge personally in response to specific requests from executives about 
    their interests, such as well-qualified referrals. 

    
     
    Please 
    
    contact us to discuss how our work could support your interests. 
 
    We apply our research capabilities and extensive networks of 
    contacts and working relationships beyond the extensive published content of 
    this website.  We are also experimenting with 
    Google Groups as another way to openly 
    share market knowledge. 

  
  
    
     
     
    Virus warning : 
    Hackers continue to spoof our published e-mail address or use fictitious 
    addresses with our domain name as they send out spam or infected messages.  
    Messages which may appear to be from us do not actually come from any of our 
    systems.  Please refer to our latest information about known attacks of 
    this nature. 

    
     Our actual outbound e-mails to our contacts are sent from an 
    unpublished address, and are very specific in their subject lines about the 
    content.  They rarely have any file attachments unless the recipient 
    knows us well and is expecting to receive the file per prior correspondence.

  
  

  
    
     
    
     
     The new website includes some unique 
    search features, such as those highlighted through links below (Area Search, 
    USA Search, CRE Search, etc.).  These make it easy for executives and 
    their advisors to selectively 
    search relevant websites for additional details beyond the executive summary content 
    and links which we provide. 

  





  
    
    
    
    Area Search

    
    
    
    Corporate Real Estate Search

    
    
    
    Professional Search

    
    
    
    Media Search

  
  
    
    
    
    USA Search

    
    
    
    Europe Search

    
    
    
    Event Search

    
    
    
    Professional Biographies

  
  
    
    
    Invest USA Directory

    
    
    Invest in Europe Directory

    
    
    US Governors Directory
    
    
    Technology clusters and parks

  
  
    
    
    Area Profiles
    
    
    Global Contacts Directory

    
    
    US Foreign-Trade Zones

    
    
    BRAC military base closures

  
  
    
    
    Area Surveys
    
    
    Selective property listings

    
    
    HR 
    contacts and 
    data

    
    
    Ad Recall   
    Event 
    Recall

  
  
    

    
    Projects by 
    location

    
    
    
    Consultant Tips

    
    
    Project management

    
    
    
    Demographic data pointers

  
  
    
    
    
    Projects by industry

    
    
    
    Corporate Real Estate Firms

    
    
    GIS map tools

    
    
    Market research sources

  
  
    
    
    Service Profiles

    
    
    Site 
    Selection Consultants

    
    
    Tax 
    or 
    Legal Advisors

    
    
    
    Credits &amp; Incentives Consultants

  
  


  
    

    
    Regional directories of economic development 
    organizations and foreign 
    direct investment (FDI) or investment promotion agency contacts, and related sources.   
    
  
  
    
    US 
    : Northeast
    
    US : 
    Great Lakes
    
    Canada
    
    Mexico
  
  
    
    US : Mid Atlantic
    
    US : North Central
    
    US 
    : Mountain
    
    South 
    America
  
  
    
    US 
    : Southeast
    
    US : South Central
    
    US : West Coast
    
    Central America, Caribbean
  
  
    
    Europe
    
    South 
    Asia
    
    Australia, New Zealand
    
    Middle 
    East, and  
    Africa
  
  
    
    China, 
    Taiwan
    
    Southeast Asia
    
    Korea
    
    Japan
  





  
    

    Congratulations : to Grameen Bank, Bangladesh - and to economist Muhammad 
    Yunus Congratulations to 2006 Nobel Peace 
    Prize Laureate Muhammad Yunus and the Grameen Bank he founded to promote 
    local economic development and alleviate poverty through microcredit 
    programs. 
     These enabled small groups of poor people in Bangladesh to 
    access unsecured loans in very small amounts for their own self-help 
    business efforts which would not be possible through the traditional bank 
    loan processes.  This made it possible for many poor people to find 
    their own path out of poverty through their work and savings, rather than 
    rely on high-cost sources of capital or continuous government aid, social 
    programs, or charity.  This development model has been repeated 
    elsewhere as an effective approach to poverty reduction challenges, and 
    adopted by various non-profit charitable organizations as a way to leverage 
    the economic impact of their limited resources. 
     The Nobel Lecture by the winner, typically in December, 
    should be a very interesting presentation, as was the
    2004 Lecture when  Prof. Wangari 
    Muta Maathai of Kenya won it for her Green Belt Movement's contributions to sustainable development, 
    democracy and peace.. 

    

    
    http://www.nobelpeaceprize.org 
     
    
    http://nobelprize.org/nobel_prizes/peace/laureates/2006/  
     One of the interesting concepts promoted by Muhammad Yunus 
    and others 
    is the idea of "non-loss" companies with a clear social purpose as their 
    main objective.  In short, they are managed like for-profit business 
    ventures, but are not managed for profit maximization, but rather to 
    maximize the desired social impact of their work. 
     This differs from the typical nature of "non profit" 
    organizations, which may similarly focus on trying to achieve good social 
    outcomes, but perhaps more from the perspective of raising and distributing 
    money or other benefits as charity rather than by managing the process for 
    demonstrable results as in a business venture. 
     In other words, a non-profit may measure activity (how 
    much it has given away and to how many) rather than results (what has been 
    changed to achieve sustainable social or economic progress beyond immediate 
    crisis-response).  This concept of business-like social ventures has 
    started to catch on with philanthropic business leaders who want to achieve 
    lasting results, as reflected in some of the resources listed in our
    Humanitarian / 
    Philanthropy directory. 

  





  
    
    Tip : See 
    Display if the full width of any page does not appear in your 
    browser, or if you are having trouble with a pop-up blocker when you use 
    navigational links or buttons.  Many links open new windows to make it easy to explore interests, 
    compare content, and then come back to the original page by just 
    closing the window.  There are no pop-up ads.

    
     
    This website was designed for high resolution 
    display settings (1280x1024 or higher) as typically used by the executives 
    and professionals we serve, instead of older display standards.  More 
    scrolling may be required at lower resolution settings.  We regret any 
    inconvenience to users of lower resolution display settings who may need to 
    scroll more than we anticipated. 
    
  
  

 











 



 

Send questions, suggestions, or comments about this site to

enquiries@gdi-solutions.com . 



 

Global Direct Investment Solutions, PO Box 439  Fox River 
Grove, IL 60021-0439  TEL 847-304-4655  FAX 847-304-5375 



 

The use of graphics and advertising has been  minimized to 
improve performance. 


Copyright ? 2002, 2003, 2004, 2005, 2006  
Global Direct Investment Solutions, Inc.     


Last modified: 
09/02/06 

 







