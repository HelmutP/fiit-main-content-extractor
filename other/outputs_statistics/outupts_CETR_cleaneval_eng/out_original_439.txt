






InnoVisions Canada Telework Experts: American states and telecommuting









	
		
		

	
	
		
		

	
	
		
		

	
	
		
		 
		Home |
		
		Daily News
		| 
		Join CTA 
		(free) | 
		
		Training | 
		
		Jobs
		| 
		Books
		| 
		Media
		| 
		Contact
		| 
		Search | 
		
		Links 

	
	
		
		

	
	
		
		

	










  
    


  
    


  
    
    
  
  
    
    
    
    GOVERNMENTS, PUBLIC POLICY &amp; TELEWORK

  
  
    
    
    

  
  
    
    Governments Home
  
  
    
    
  
  
    
    
    
    US GOVERNMENTS

  
  
    
    
    

  
  
    
    USA home
  
  
    
    
    

  
  
    
    Cities

  
  
    
    
  
  
    
    States

  
  
    
    
    

  
  
    
    Federal

  
  
    
    
    

  
  
    
    
    Federal employee telework program

  
  
    
    
  
  

    
    
    

    
      
        
           
        
        
          
             
            US 
            states and telework 
             For many reasons, politicians all 
            over the world are becoming increasingly supportive of telework and 
            telecommuting. This listing of American elected officials (current 
            and past) support and endorse telework. 
            Contact us if you know of others. 
			 See map showing 
			which US states use telework 
  
            
        
        
          
          
          

          
          Arizona 
            
        
        
          
           
          
          -

          
          Following 
          oil pipeline rupture, Governor directs 
          all state agencies, boards and commissions to limit daily agency 
          travel and to  make utilization of the Rideshare and Telework programs 
          an immediate high priority.
        
        
          
           
          
          -

          
          State Governor 
          Napolitano advances the State Telework Program with Executive Order
        
        
          
           
          
          -

          Former State
              Governor Symington 
    mandated 15% of all Phoenix area State workers to telework as part of 
          his increased commitment to air quality
        
        
          
          
          

          
          California 
            
        
        
          
           
          
          -

          Bill, SB 2021 Starting in
            2001, qualified employers and employees in Los Angeles, Orange, and
            Ventura will be eligible for a $500 tax credit if they use telework
            and other alternative work arrangements to reduce traffic congestion
            and improve air quality and worker productivity. Only newly created
            positions would be eligible, and employees also must travel a
            minimum of nine miles to their workplace and commute outside of
            times other than normal "rush hours"
        
        
          
           
          
          -

          Governor Pete Wilson '94: In
            an open letter to all California executives: "Telecommuting
            also must play a role (in reducing the number of cars. A
            telecommuting program at your firm can help solve transportation
            challenges and improve our air quality and regional mobility. It
            also can cut your costs and improve your employees
            productivity."
        
        
          
           
          
          -

          Gov't Code, S. 14200-14203:
            "The Legislature finds and declares the following: That
            telecommuting can be an important means to reduce air pollution and
            traffic congestion and to reduce the high costs of highway
            commuting; that telecommuting stimulates employee productivity while
            giving workers more flexibility and control over their lives. It is
            the intent of the Legislature to encourage state agencies to adopt
            policies that encourage telecommuting by state employees."
        
        
          
           
          
          -

          
          Assemblymember Liz Fugueroa: Introduced a bill into the California 
          State Assembly that authorized a $200 tax credit for each new 
          qualified residential teleworking position and a $100 tax credit for 
          each new qualified teleworking center position created during the 
          taxable year by the taxpayer and staffed by an hourly or salaried 
          employee residing in California during the taxable year. (Bill died in 
          committee)
        
        
          
           
          
          -

          
          
            
              
              Links: 
              
                
                  
                  
                  California
            Department of Personnel Admin Telework Program
                
                
                  
                  
                  California's
            Smart Valley Telecommuting Guide
                
                
                  
                  
                  California
            Department of Personnel Admin Telework Program
                
                
                  
                  
                  California's
            Smart Valley Telecommuting Guide
                
              
              
            
          
          
        
        
          
          
          

          
          Colorado 
            
        
        
          
           
          
          -

          
31 Colorado Mayors urge Colorado businesses to consider telework to
    help solve critical business and transformation issues, including the ongoing challenge to
    attract and retain highly skilled workers in a tight job market. 
        
        
          
           
          
          -

          
          
            
              
              Links
                
                  
                  
                  
                       
                  
                  Colorado
            Telework Program

                
              
              
            
          
          
        
        
          
          
          

          
          Connecticut 
            
        
        
          
           
          
          -

          
Sept 2005: The 
			Telecommuter Tax Fairness Act proposed by Senators Chris Dodd 
			(D-CT), and Joseph Lieberman (D-CT), and Representatives Christopher 
			Shays (R-CT), Tom Davis (R-VA), and Rosa DeLauro (D-CT) seeks to 
			remove a state-based penalty tax on interstate telecommuting. 
			 According to Representative Shays, 
			"Telework is a critical tool to help lower spiraling gas costs and 
			our dependence on foreign oil. The Telecommuter Tax Fairness Act 
			would facilitate telework by prohibiting a punitive tax on Americans 
			who work from home for out-of-state employers. This is the time to 
			make telecommuting easier for Americans, not harder," he said. "I am 
			glad to have the support of The Telework Coalition in the effort to 
			empower more Americans to conserve fuel by telecommuting." 

        
        
          
           
          
          -

          CT Senator 
          Chris Dodd to introduce a measure in the senate that will protect 
          Connecticut telecommuters from having to pay a second income tax to 
          the state of New York.
          See story here
        
        
          
           
          
          -

          State set aside $55 
          million to urge commuters to opt for commuting 
          options including telecommuting and other . Governor Rowland said: "The choices are very simple:....When 
          to go to work; Where to work from; and How to get to work"
        
        
          
           
          
          -

           TelecommuteCT 
          is a $.5 million program to help state employers design and implement telecommuting programs
        
        
          
          
          

          
          Florida 
            
        
        
          
           
          
          
          -

          
          Review 
          of Florida's Telecommuting Program
        
        
          
           
          
          
          -

          
          Florida 
          State Employee Telecommuting Program
        
        
          
          
          

          
          Georgia 
            
        
        
          
           
          
          -
          
2006 Georgia. State gives 
			tax breaks to firms who let workers telework. It will give employers 
			 a state income tax credit of up to 
			$20,000 if they conduct telework feasibility study 
			 
			 
			 
			a tax credit of up to $1,200 per employee if they implement telework 
			programs.
			
			Details here 
			 
			
        
        
          
           
          
          -
          
          
          Georgia bill would jump-start telework. Telecommuting may finally 
          be poised to take off in Georgia after years of false starts. A pair 
          of bills now before the state House of Representatives would reward 
          employers with annual tax credits of up to $1,500 for each employee 
          who works a certain amount of time from home rather than the office. 
          Supporters say incentives will encourage companies that have been slow 
          to get their employees out of their cubicles and off the roads. Gov. 
          Sonny Perdue championed telecommuting during his 2002 campaign and has 
          backed the idea of state financial assistance
        
        
          
           
          
          -
          
          
          
          Georgia State teleworking initiative has had trouble revving up. 
          Officials blame budget and legal constraints for problems they have 
          had getting only 1,500 Georgia state employees are teleworking at 
          least one day a month, two years after Gov. Sonny Perdue said the 
          number would be 25,000 - or roughly 25 percent of the state 
          government's workforce - by 2005
        
        
          
           
          
          -
          (2004) Georgia proposes telework tax credits
          
          Goal is to reduce congestion and air quality. Recipient teleworkers or 
          businesses could get from $500 to $5000, and would need to show how 
          their telecommuting program works and how many driving miles were 
          saved
        
        
          
           
          
          -
          Georgia State's
          telework 
          policy (pdf). Also see Georgia Governor's
          
          executive order (pdf) regarding telework
        
        
          
           
          
          -
          2003
            Clean Air Campaign? Raises the Bar on Teleworking in Metro Atlanta:
            Telework Leadership Initiative awards employers with up to $20,000
            in consulting and cash to establish telework programs The Clean
            Air Campaign? is accepting applications for the Telework Leadership
            Initiative, a groundbreaking incentive program that provides
            employers with up to $20,000 worth of resources to start or expand a
            telework program. The Clean Air Campaign is a not-for-profit
            organization working to reduce traffic congestion and improve air
            quality in metro Atlanta
        
        
          
           
          
          -
          (2002) Georgia
            Governor wants 25,000 state teleworkers as a viable
            alternative for dealing with traffic, pollution and quality-of-life
            issues
        
        
          
           
          
          -
          Governor Zell Miller '98: Mandated all 26,000 state employees to
            reduce solo trips to and from work by 20% on Ozone Action
            Days....telework is an ideal commute option. When an ozone alert is
            issued, practiced teleworkers can easily collect enough work to be
            highly productive at home, playing their part in reducing emissions
            and single occupancy vehicles during peak congestion periods".
        
        
          
           
          
          Kansas 
            
        
		
          
          

          
          -

          
			Kansas Assistive Technology 
			Cooperative (KATCO) helps persons with disabilities find telework 
			employment with loans for adaptive equipment and home office 
			modifications
        
        
          
          
          

          
          Kentucky 
            
        
        
          
           
          
          -

          
          Kentucky State 
          Telecommuting White Paper: Policy and Technology Issues
        
        
          
           
          
          -

          
          State of Kentucky 
          telecommuting program (MS 
          Word)
        
        
          
          
          

          
          
          Maine

        
        
          
           
          
          -

          Gov. John Baldacci
          
          signs executive order to reduce greenhouse gas emissions to 1990 
          levels by 2010. He has asked state officials to review and report on 
          existing telecommuting programs by January 2005. He wants the State to 
          assume a leadership role in addressing the serious risk of climate 
          change by promoting more efficient use of energy in the transportation 
          sector
        
        
          
          
          

          
          
          Maryland 
            
        
        
          
           
          -
          Apr 04 Bill supporting tax 
          incentives for private companies offering telecommuting programs in 
          Maryland made it through the House by a 139 to 0 vote,
          but 
          died in the Senate. Will try again next year.
        
        
          
           
          -
          Mar 04:
          
          Telework legislation (House Bill 1101) offers over half million 
          over three years to help employers and teleworkers. Employers would 
          get tax credit of up to 25% of telework costs (up to $5,000/yr). A 
          side bill mandates telework for state employees (sort of like the 
          federal telework law)
        
        
          
           
          -
          
Maryland's DOT initiates The Telework
              Partnership Offers
              free professional telework consulting services to Maryland
              Employers. Also, the state's Telecommuting Pilot Program requires a
    statewide telecommuting pilot program for all government agencies
              
          
        
        
          
           
          -
          
          Working From Home Gains
            Converts - Governments Warming To Telecommuting Option
        
        
          
           
          -
          Feb
            2002: Sue
            Hecht of the Maryland House of Delegates submits "Telework Tax Incentive Act". 
          Aim is to provide a
            $500 tax credit for employees and/or employers to promote teleworking in the private sector
        
        
          
           
          -
          Governor Parris Glendening (2000) offers 'pay-me-not-to-drive'
            incentives to employers and employees to address traffic and air
            pollution problems. Gives a special pay boost for lower wage workers
            while saving employers money
        
        
          
           
          -
          Establishes $600,000 fund to help employers establish telework programs to reduce traffic congestion and improve the
            quality of life of state residents. Signs into law the state's
            Telecommuting Pilot Program which requires the establishment of a
            statewide telecommuting pilot program for all government agencies.
        
        
          
          
          

          
          Massachusetts 
            
        
        
          
           
          
          -

          
          
          Massachusetts Division of Energy Resources (DOER) 
          Massachusetts Telecommuting Initiative
        
        
          
          
          

          
          Minnesota 
            
        
        
          
           
          
          -

          Senator Paul Wellstone, D-Minn. introduces Rural Telework Act of
            2000 to provide funding to local organizations to help train,
            connect and then employ rural workers to do "telework"
            jobs in the information technology field in their own communities.
        
        
          
           
          
          -

          Statute s. 15.95 sub. 10 '94 Says no state agency may propose or
            implement a capital investment plan for a state building unless it
            receives approval from the state for increasing telecommuting by
            employees who would normally work in the building, or can prove that
            such a plan is not practicable.
        
        
          
           
          
          -

          
          
            
              
              Links: 
              
                
                  
                  
                  
                  
                  Minnesota Office of Technology 
                
                
                  
                  
                  
                  Midwest Institute for 
                  Telecommuting Education
                
                
                  
                  
                  
                  University of Minnesota 
                  
                
                
              
            
          
          
        
        
          
          
          

          
          Montana 
            
        
        
          
           
          
          -

          Montana passes a bill 
          formally authorizing state agencies to promote "teleworking" by 
          employees. The thinking is that telework makes employees more 
          productive, reduces office space needs and attracts more workers to 
          the labor force. The bill merely recognizes that in the law and gives 
          the Legislature some oversight. See story
          
          here. See actual
          
          bill here
        
        
          
          
          

          
          New York State

        
        
          
           
          
          -

          
          New York State telework program
          Check it out 
          here
        
        
          
          
          

          
          
          North Carolina 
            
        
        
          
           
          
          -

          
          North Carolina 
          State Teleworking program
        
        
          
           
          
          -

          Telework
            Feasibility Study published by State Auditor Ralph Campbell
            concluded that if 5% of state employees teleworked, they would save
            over $23 million in improved productivity, reduced  office
            space and turnover costs.
        
        
          
          
          

          
          North Virginia 
            
        
        
          
           
          
          -

          2003: Gov. 
          Mark R. Warner: Virginia state can't
            afford to build big new highways, so it's looking for smaller,
            quick-fix solutions, including telecommuting, to gridlock. Governor
            willing to invest $20 million for several related projects to ease
            congestion
        
        
          
          
          

          
          Oregon 
            
        
        
          
           
          
          -

          
The Oregon 
          Department of Energy
          actively 
          promotes telework because it conserves fuel, relieves traffic 
          congestion, and improves air quality -- and because it makes good 
          business sense. The State has developed an aggressive  Telework 
          Program that: 
           
             helps cities offer state tax credits, low-interest loans, and/or other
          incentives to assist with purchasing and installing new or
    used equipment that allows an employees to telework 
             provides ongoing
          resources to assist employers develop custom-fit telework programs 
           
          
        
        
          
          
          

          
          Texas 
            
        
        
          
           
          
          -

          Texas
            State Comptroller
          recommends telework 
          and its savings potential
        
        
          
           
          
          -

          Texas State
          telework 
          Guidelines
        
        
          
          
          

          
          Utah 
            
        
        
          
           
          
          -

          Governor Mike Leavitt '99: Proclaimed May 14 as Utah Telecommuting
            Day. Employers were encouraged to allow eligible employees to spend
            the day productively working from home instead of sitting in traffic
            on a congested freeway.
        
        
          
          
          

          
          Vermont 
            
        
        
          
           
          
          -

          Department Of Labor Grant 
          establishes Vermont Work at Home. The Vermont Telecom Advancement Center (VTAC), in collaboration with 
          Vermont State and private agencies working to relieve unemployment 
          throughout Vermont, plan to train unemployed and underemployed 
          workforce, enabling them to work at home. Vermont Work at Home is hosting a
          national seminar (June 
          2004) in Stowe, Vermont
        
        
          
          
          

          
          
          Virginia 
            
        
        
          
           
          
          -

          
			 
              Telework!VA This program 
			exists to reduce overcrowding on Virginia's highways, thereby 
			reducing real estate and parking costs; as well as improving
        productivity.  Under the program's $3.2 million budget, companies 
			can receive: 
			 
			
              - $10,000 toward the cost of leasing equipment (fax machines, 
			computers and high-speed Internet connections); 
			- up to $35,000 (or $3,500 per employee), for the cost of 
			telecommuting-related equipment and services; 
			 
			
              - up to $25,000 for technical consulting; 

        
        
          
           
          
          -

          
			Virginia Governor creates
			
			Office of Telework Promotion and Broadband Assistance
        
        
          
           
          
          -

          Governor James 
          Gilmore Seeks to offer $10 million in tax
            incentives and grants for companies that let their employees
            telework rather than contributing to rush hour traffic. This
            addresses two of Virginia's most pressing problems: transportation
            and work force shortages.
        
        
          
          
          

          
          Washington State 
            
        
        
          
           
          
          -

          (June
            2001): Governor Gary Locke directs state agencies to
            dramatically increase use of telecommuting and flexible work hours
            to show how employers can reduce traffic congestion.
        
        
          
           
          
          -

          Senate Bill 5171 (Jan '01)
            Legislation sponsored by Sen. Ken Jacobsen, D-Seattle, would create:
            a temporary joint task force on telework enhancement; a telework
            enhancement board with funds to expand telework opportunities; and
            tax incentives for employers
        
		
          
          
          

          
			
          Wyoming 
            

        
		
          
           
          
          -

          
			
			Wyoming Department of Audit discovers several employees who worked 
			from home 5 days per week in violation of the rules regarding the 
			number of days allowed (1 to 3)
        
        
    
  

    
  

  








	
		 
	
	
		
		 Copyright? 1997- 
		2006.  
		InnoVisions Canada  All rights 
		reserved. (Privacy 
		statement) 

	

   








