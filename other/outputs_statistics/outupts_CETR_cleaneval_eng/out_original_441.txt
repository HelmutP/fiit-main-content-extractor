




	
		
		
		Jefferson County, Missouri Employment
		
	
	
		
		
		
			
				
					
					
						
							
							
							
							
							
							
							
							
							
						
						
							
							
							
							
							
							
							
						
						
							
							
						
						
							
							
							
						
						
							
							
							
								
								
								 
							
						
					
					
				
				
			
			
				
			
			
				
			
			
				
				
			
			
				
					
					
						
							
							
							
						
						
							About
							
							
						
						
							
							
							
						
						
							News
							
							
						
						
							
							
							
						
						
							County 
									Commission
							
							
						
						
							
							
							
						
						
							Departments 
									&amp; Office Holders
							
							
						
						
							
							
							
						
						
							Invitation 
									for Bids
							
							
						
						
							
							
							
						
						
							Calendar
							
							
						
						
							
							
							
						
						
							FAQs
							
							
						
						
							
							
							
						
						
							Community 
									Links
							
							
						
						
							
							
							
						
						
							Contact
							
							
						
						
							
							
							
						
						
							Employment
							
							
						
						
							
							
							
						
						
							Search
							
							
						
						
							
							
							
						
					
					
				
				
					
					 
						
							
								
									Jefferson County Employment
								
								
								
									
								
								
									Positions available throughout the entire county government will be updated to 
										this page routinely. For the most up-to-date listing, 
											contact the Jefferson County Human Resources Department. 
										
									
								
								
								
									
								
								
									
										Please note that the employment applications available on this page are PDF 
										documents. Adobe Acrobat Reader (4.0 or more recent version) is required to 
										view PDF files. To download the free Adobe Acrobat Reader from the Adobe web 
										site, click on the logo. 
											

								
								
								
									
								
								
									Select the department or division to see current listings.
								
								
								
									
								
								
									
										
											
												
													
														
															
																Animal Control Division 
																 
																Assessor's Office 
																 
																Auditor's Office 
																 
																Building Division 
																 
																Code Enforcement Division 
																 
																Collector of Revenue 
																 
																County Clerk and Election Authority 
																 
																County Commission 
																 
																Facility Services Division 
																 
																Fleet Services Division 
																 
																General Services Division 
																 
																Highway Division 
																 
																Human Resources Division 
															
															
																Juvenile Office 
																 
																Municipal Court 
																 
																Office of Contracts &amp; Grants 
																 
																Office of Emergency Management &amp; Public Information 
																 
																Parks &amp; Recreation Department 
																 
																Planning Division 
																 
																Prosecuting Attorney's Office 
																 
																Public Administrator's Office 
																 
																Recorder of Deeds Office 
																 
																Technical Administration Division 
																 
																Treasurer's Office 
																 
																Other Vacancies 
															
														
													
												
											
										
									
								
								
								
									
								
								
									
										Animal Control Division 
										 
										 
										

					

										 
						POSITION VACANCY ANNOUNCEMENT
							  
							Animal Control Kennel Laborer II
							 
                     Division of Animal Control
							 
							Department of Land Use, Development and Code Enforcement
							 
							$19,739 to 22,011 Annually Depending on Qualifications
							 
					 
					
					 POSITION SUMMARY:
					 
 Performs labor work cleaning and sanitizing kennels and general grounds maintenance, which includes grass cutting, weed trimming, trash pickup and snow and ice removal.  
                  Cleaning duties include general sweeping, mopping, disinfecting and scrubbing of the Animal Control facilities and kennels that become unsanitary or offensive to the public.
                    
                  Assists public as necessary, as well as officers in removing animals from vehicles and putting them in cages, animal intake processes and record keeping, and with adoption processes including handling of animals for viewing and application completion. 
					 
						MINIMUM QUALIFICATIONS:
						 
 
						 High School Diploma or Equivalency
                   
 Animal handling techniques helpful
                   
 Red Cross Pet First Aid Training desirable
                   
 
					 KNOWLEDGE, SKILLS AND ABILITIES:
						 
 
                   Knowledge of simple, routine or repetitive tasks, operations and skill to operate simple equipment that operates repetitively, such as lawn mower, weed eater, tools commonly used in carpentry and construction, and housekeeping.
                   
 Ability to lift, carry and push moderately heavy objects and materials, 50 to 75 pounds, such as dogs and other animals.
                   
 Ability to walk on undeveloped properties and uneven pavements.
                   
 Ability to recognize and identify similarities or differences between colors, shapes, and sounds such as identifying animal body language signals and sounds of distress.
                   
 Ability to work well with others, and to represent the County in a professional manner.
                   
 Must have good oral and written communication skills and the ability to resolve problems with tact and diplomacy.
						 
 
					 
               The Division of Human Resources will receive applications from internal candidates until Wednesday, December 13, 2006.  External applications will be accepted concurrently and will be considered if not filled by an internal candidate.
					 
					  

               
												


									
application
										|  top

								
								
								
									
								
								
									
										Assessor's Office 
										 
										 
										No jobs currently listed
									
								
								
									
application
										|  top

								
								
								
									
								
								
									
										Auditor's Office 
										 
										 
										No jobs currently listed
									
								
								
									
application
										|  top

								
								
								
									
								
								
									
										Building Division 
										 
										 
										

					
					 
						POSITION VACANCY ANNOUNCEMENT
							  
							Electrical Inspector
							 
                     Building Division
							 
							Department of Land Use, Development and Code Enforcement
							 
							 
							Depending on Qualifications
							 
					 
					
					 POSITION SUMMARY:
					 
 Performs on-site inspection of all new construction and renovation projects requiring building permits to ensure compliance with county building ordinances and state laws and regulations.  Evaluates drawings, plans, blueprint; reviews building permit applications for compliance with county building codes and approves acceptable applications.						 
						 
						MINIMUM QUALIFICATIONS:
						 
 
						 High School Diploma or Equivalency and trade school certification; or
                   
 Associates Degree in building technology or related field.
                   
 Requires five (5) years of experience as an electrician.
                   
 Must be able to be certified for 1 and 2 family dwellings as electrical inspector within 6 months.
                   
 
					 KNOWLEDGE, SKILLS AND ABILITIES:
						 
 
                   Must have good oral and written communication skills.
                   
 Ability to read and interpret plans, drawings and blueprints.
                   
 Ability to understand, interpret and apply county building codes and zoning ordinances and BOCA codes.
                   
 Ability to detect county code violations and deviations from approved plans.
                   
 Ability to work independently and to carry out assignments to completion with minimal instruction, adhere to prescribed routines and practices, maintain records, and to make reports requiring detail and accuracy.
                   
 Ability to work well with others and to assist the public cooperatively and courteously.
						 
 
					 
               The Division of Human Resources will receive applications from internal candidates until Monday, November 20, 2006. External applications will be accepted concurrently and will be considered if not filled by an internal candidate.
					 
					 
					 

					
					
					 
									
								
				
			
			
				
application
					|  top

			
			
			
				
			
			
				
Code Enforcement Division 
					 	
						 
					No jobs currently listed
					
					

				
application
					|  top

			
			
			
				
			
			
				
Collector of Revenue 
					 
					 
					No jobs currently listed

			
			
				
application
					|  top

			
			
			
				
			
			
				
County Clerk and Election 
						Authority 
					 
					 
					No jobs currently listed
				
			
			
				
application
					| top

			
			
			
				
			
			
				
County Commission 
					 
					 
					No jobs currently listed
										
					
					
			
			
				
application
					| top

			
			
			
				
			
			
				
Facility Services Division 
					 
					 
					No jobs currently listed 
					
				
			
			
				
application
					|  top

			
			
			
				
			
			
				
Fleet Services Division 
					 
					 
					No jobs currently listed 
				
			


				
application
					| 
					top
					
					
			 
			
			
				
			
			
				
General Services Division 
					 
					 
					No jobs currently listed

				
			
			
				
application
					| top

			
			
			
				
			
			
				
Highway Division 
					 
					 
					No jobs currently listed
					
					
            
			
			
				
application
					|  top

			
			
			
				
			
			
				
Human Resources Division 
					 
					 
					No jobs currently listed 
					
					
				
			
			
				
application
					| top

			
			
			
				
			
			
				
Juvenile Division 
					 
					
					
					 
					 
						POSITION VACANCY ANNOUNCEMENT
							 
							 
							Youth Care Worker
							 
							 
							Department of Juvenile Justice
							 
							 
							Part-time
							 
							$9.26 per hour + Shift Incentives
					 
					
					 POSITION SUMMARY:
					 
 Responsible for providing direct and appropriate care to the juvenile residents 
						placed with Jefferson County Children's Home in accordance with established 
						policies, procedures and guidelines to ensure the rights and privileges of the 
						juvenile residents are not abridged in any manner. Performs routine 
						housekeeping duties as directed; maintains and generates log entries' reports 
						and all other documentation as required by policies, procedures and guidelines.
						 
						 
						MINIMUM QUALIFICATIONS:
						 
 
							 
							High School Diploma or Equivalency
							 
 
							Must be 21 years of age or older
							 
 
							Experience in working with youths (professionally or volunteer basis) preferred
						 
 
					 KNOWLEDGE, SKILLS AND ABILITIES:
						 
 
							 
							Must possess good communication and problem solving skills.
							 
 
							Ability to perform routine housekeeping duties
							 
 
							Knowledge of basic computer skills
							 
 
							Must be available to work evening, midnight and weekend schedule.
						 
 
					 
						The Division of Human Resources will receive applications from internal 
						candidates until Friday, March 3, 2006. The position will be open for outside 
						applicant consideration after this date.
					 
					 
					 
					
					 
					 
						
				 

			
			
				
application
					| top

			
			
			
				
			
			
				
Municipal Court 
					 
					No jobs currently listed
					
					
				
			
			
				
application
					| top

			
			
			
				
			
			
				
Office of Contracts &amp; Grants 
					 
					 
					No jobs currently listed 
					
					
				
			
			
				
application
					| top

			
			
			
				
			
			
				
Office of Emergency Management &amp; 
						Public Information 
					 
					 
					No jobs currently listed

			
			
				
application
					|  top

			
			
			
				
			
			
				
Parks &amp; Recreation 
					 
					 
					No jobs currently listed

			
			 
			
			
				
			
			
				
application
					| top

			
			
			
				
			
			
				
Planning Division 
					 
					 
					No jobs currently listed

			
			 
			
				
application
					| top

			
			
			
				
			
			
				
Prosecuting Attorney's Office 
					 
					 
					

					

										 
						POSITION VACANCY ANNOUNCEMENT
							  
							Administrative Clerk II
							 
                     Prosecuting Attorney's Office
							  
							$17,000.00 to $18,960.00 Annual Salary Depending on Qualifications
							 
					 
					
					 POSITION SUMMARY:
					 
 Type a variety of documents, often legal in character, working from rough draft.  
                  Scan documents into document imaging program.  
                  Organize and compile files; run criminal histories.  
                  Assist with receptionist duties, and other miscellaneous clerical duties. 
					    
					  KNOWLEDGE, SKILLS AND ABILITIES:
						 
 
                   Knowledge of business English, spelling and to make arithmetic computations. 
                   
 Knowledge of office practices and procedures and record keeping methods.
                   
 Must have good oral and written communication skills and the ability to resolve problems with tact and diplomacy.
                   
 Ability to maintain moderately complex clerical records and to prepare reports from records and to check for accuracy.
                   
 Ability to make routine decisions in accordance with procedures, laws and regulations and to apply these to work problems.
                   
 Strong computer skills and knowledge of Microsoft Office essential.
						 
 
                   
					MINIMUM QUALIFICATIONS:
						 
						 High School Diploma or Equivalency
                   
 One year of experience in a legal environment.
                   
 

					 
                  The Division of Human Resources will receive applications from internal candidates until Friday, December 15, 2006.  The position will be open for outside applicant consideration after this date.					 
					  

               
					


				
			
			
				
application
					| top

			
			
			
				
			
			
				
Public Administrator's Office 
					 
					 
					No jobs currently listed 


			


				
application
					| top

			
			
			
				
			
			
				
Recorder of Deeds 
					 
					 
					No jobs currently listed
				
			
			
				
application
					| top

			
			
			
				
			
			
				
Technical Administration Division 
					 
					 
					No jobs currently listed

			
			
				
application
					| top

			
			
			
				
			
			
				
Treasurer's Office 
					 
					 
					No jobs currently listed

			
			
				
application
					| top

			
			
			
				
			
			
				
Other Vacancies 
					 
					 
					No jobs currently listed

			
			
			
			
			
application
				| top

			 
			
			
				
			
			
				
					
				
			
			
				
					Jefferson County 
						300 2nd Street 
						Hillsboro, MO 63050 
						800-748-3456 
						contact   |   design 
							team   |   support by 
							JOIN-N 
						Copyright © 2000 Jefferson County, Missouri 
						All rights reserved.
				
			
		
		   
		
		 
		
		
		 
	




