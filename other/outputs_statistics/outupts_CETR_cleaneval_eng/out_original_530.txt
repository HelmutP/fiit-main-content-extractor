



 
   
      
     Skip to content  
     
       WDWS 
       WHMS 
     
	     
      
        
        
        
        
        
        
      
     
   
   
     
       
News
         
           Local News  
           Obituaries 
           Opinions 
           Web logs 
		   Chats 
		   Podcasts 		  
           Photo Galleries  
           Special Reports  
           Calendar 

         
       
       
Sports
         
           Illini SportsFacts 
		  		   Chats 
           High School 
		   Little League 
		   All State &amp; All Area Teams 
		   Honor Roll 
           Photo Galleries  
		  		   Podcasts 		  


         
       
       
Community
         
           Answerbook 
          Community Links 
		   Contests 
		   Food 
		   Games 
           Gas Price Watch  
			 Half-Price Haven  
			
             Ticket Drawings 
           More…   
         
       

       
Classifieds
         
           Find a car 
           Find a job 
           
Find a home   
           
Find an apartment   
		   Today's classifieds 
		   Sunday's classifieds 
           Place an ad 
           ConnectAd 
         
       
       Customer Service  
     
 
   
     Tuesday, December 26, 
2006 East Central Illinois
 
   

 
	        
       Home » Special Reports 
    
	 Scott One 
     
     
    	
    
	


 

CHAPTER 13:  Shying away
		 
 
Finding a comfort zone
		
		   For some, building a social life comes one small step at a time.
		
		 
		BY ERNST LAMOTHE JR. 
		NEWS-GAZETTE STAFF WRITER 
		Published DECEMBER 3, 2004  

	
	     Five minutes. -  - Ashley Baltazar spent a total of five minutes at the annual Quad Day in August, where hundreds of campus organizations try to sell themselves to students.     Baltazar didn't leave because they weren't doing a good job or because she was overwhelmed by the heat. She just doesn't like crowds. So there was no chance that you saw her jumping up and down with the Orange Krush on Wednesday night .     "I don't go to games," she said. "I don't like screaming."     She believes her crowd phobia has been an issue since around sixth grade.      "I think it is because I am overweight," said Baltazar, of Richton Park. "I think people are always judging me. I know they are not, but I am paranoid about my experiences. It's been ongoing for a while. I will always to a level think people are watching me."     More than three months after Quad Day, she's opened up a bit but still shies away from attention. Being a quiet person on a social floor can be difficult, but two women are dealing with it on Scott One.     "I am not looking to be a social butterfly," added Baltazar. "I don't think it is something I like doing. I feel more comfortable sitting at my computer."     She joined the Women in Computer Science club this semester as her social outlet on a smaller scale. The club has about 12 members as opposed to the larger computer science organization on campus, which boasts hundreds of members.      "It's a nice organization because I know all the people's names," said Baltazar. "I didn't want to be in a big group."     The club combines academic education, featuring speakers from Motorola and other companies, with a social aspect such as playing Scrabble at a coffee house on campus.     Baltazar again busted out of her comfort zone and joined the floor for a bowling trip to the Illini Union on Tuesday.      "I hadn't gone bowling in a few years. I think it is fun," she said. "I don't watch bowling competitions, but I think it is pretty cool."     So is she officially an extrovert?     "I wouldn't say I am less shy," said Baltazar. "I think I am more comfortable."     For Shannon Smith, the phrase Christmas in July means it's time to trim the trees. She owns one-fourth of a Christmas tree business, started by her father, and has always been involved.      "We have pictures of me and my brother playing in the trees and they are our size. It's fun to watch over time how we grew with the trees," said Smith, of Woodstock. "It's a huge part of my life and I have a lot of fun doing it."      She carried a little bit of the family business into Scott One.     "I definitely brought back a Christmas tree and I am not sure if that is allowed in the dorm," she said.      She becomes bubbly and excited when talking about her passion, but she's a self-described shy person. A quiet farm girl, whose favorite musician isn't the Dixie Chicks but a hard-core rapper named DMX.      Smith is very comfortable in her shyness. She prefers reading to going out. Her roommate Leslie Selcke took her to a party early in the school year. Smith stayed for 10 minutes before leaving.      "One of my teachers told me that when I got to college, it was going to be an open-your-eyes experience and that all the people who were goody-two-shoes in high school would go insane," she said. "I am pretty much the same. I don't party that much. A party consists of me and my roommate watching Disney movies."     Smith enjoys quiet environments.     "I like to go to different libraries and test them out," she said. "I am a nerd like that."     Smith wakes up between 5 and 6 a.m. for her morning cardio workout in the basement of Scott Hall. In order to get good sleep, she goes to bed around 11 p.m. Few people on Scott One sleep that early.      Residents organize a movie night every Wednesday at 11 p.m., held in the women's floor study lounge. And while it's an excellent bonding activity, it also leads to knocking on the women's breezeway doors from Scott One guys late at night, which can make it difficult for people to fall asleep before midnight.      "Sometimes our floor is kind of noisy," said Smith.     Resident Adviser Doug Bach said Smith and Baltazar are easygoing and have nice personalities, which some people might not see due to their soft-spoken nature.      "Both of them are a little more reserved and quieter than most of the girls on the floor. I haven't seen too much of them this semester," said Bach, of Wheaton. "I do encourage them to participate in the programs. I would like them to feel like they are part of the floor."      As a project early in the year, Bach learned some interesting facts about each of his residents. He said Baltazar enjoys online comics and Smith is a huge Nicolas Cage fan. The latter information is evident after walking into Smith's room, where a cornucopia of Cage pictures wallpaper her room.     Those pictures and her belongings may not be part of Scott One for too much longer. Smith e-mailed Bach a week before Thanksgiving break requesting a floor transfer next semester because the floor was too loud.      "I took that personally because it is my job to make sure that everyone has what they need on the floor," said Bach. "It's important that everyone on the floor is able to get work done and get their sleep. I kind of feel like I am not fulfilling my obligations completely."        You can reach Ernst Lamothe Jr.at (217) 351-5223 or via e-mail at elamothe@news-gazette.com.   
	
    
	
    
	


			

	
	 

CLICK TO SEE PHOTO
	


			
				

				OTHER STORIES IN THIS SECTION:  
				
				CHAPTER 33:  The Finale: Lessons Learned 
				
				�Things and People have changed 
				CHPATER 32:  Another Freshman 
				
				�Treating everyone the same 
				CHAPTER 31:  Alarms 
				
				�An alarming pain 
				CHAPTER 30:  Mom's here 
				
				�Mothers and Daughters 
				CHAPTER 29:  Practicing murder 
				
				�Deciding to play a part 
				CHAPTER 28:  Well Adjusted 
				
				�Frats help frosh cope 
				CHAPTER 27:  Religion Part II 
				
				�Examining their beliefs 
				CHAPTER 26:  Religion Part I 
				
				�Living their spirituality 
				CHAPTER 25:  The list 
				
				�Listing life's changes 
				CHAPTER 24:  Decisions, decisions 
				
				�A series of decisions 
				CHAPTER 23:  Treading water 
				
				�Making a quiet splash 
				CHAPTER 22:  Airborn 
				
				�Flying off the radar 
				CHAPTER 21:  Balanced life 
				
				�A multidimensional student 
				CHAPTER 20:  Overachiever 
				
				�A will to achieve 
				CHAPTER 19:  Fresh face 
				
				�The long route to Scott One 
				CHAPTER 18:  Lessons learned 
				
				�The many sides of Doug 
				CHAPTER 17:  Break's over 
				
				�Back on the job 
				CHAPTER 16:  Untold stories 
				
				�Creating their own fun 
				CHAPTER 15:  Semester finale 
				
				�The finals frontier 
				CHAPTER 14:  Marathon man 
				
				�Big wheels keep on turning 
				CHAPTER 12:  Homecoming 
				
				�Home is where you sleep 
				CHAPTER 11:  Semi-retired 
				
				�Life after high school sports 
				CHAPTER 10:  Orange County 
				
				�A Krushing experience 
				CHAPTER 9:  No Doze 
				
				�Late night with Ryan Fraser 
				CHAPTER 8:  Voting Virgins 
				
				�A vocal voting minority 
				CHAPTER 7:  Romance Revisited 
				
				�Breaking up is hard to do 
				CHAPTER 6:  Rule Breakers 
				
				�Rules: A friendly reminder 
				CHAPTER 5:  Mutiple Choices 
				
				�Calculating test results 
				CHAPTER 4:  Living Together 
				
				�Friends &amp; roommates 
				CHAPTER 3:  Going Greek? 
				
				�The rush to fit in 
				CHAPTER 2:  Football &amp; Romance 
				
				�Watching the Illini is different as a student 
				�A long 
				CHAPTER 1:  Getting started 
				
				�Slide on in to Scott One, week 1 
				  
	
			
			
 
			
    
  

  


  
   
       
     
       
Home  
       
Advertise with Us  
       
Feedback  
	   Subscribe 
       
WDWS.com  
       
WHMS.com  

     
	    
   
  
     Contents of this site are © Copyright 2006 The News-Gazette, Inc. All rights reserved. | Privacy Policy | Terms of use  
   
 



