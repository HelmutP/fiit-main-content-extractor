








Share Our Strength - 
 
 
  Operation Frontline®
















 
 Skip directly to: Navigation, Content. 
 Note: You are seeing a plain version of this site because you're using an outdated web browser, a PDA or cell phone, or a screen reader. If you're using an older web browser like Netscape 4, we urge you to upgrade to a modern browser that supports web standards. Upgrading is easy and free, and you'll be able to enjoy a wider variety of online content. Find out more. 
 
 

 

   



 
 
 Home 
 About Us 
 Jobs 
 Contact 
 Donate to Share Our Strength 
 
 Search 


 





 

 


 
 
 

 
 
 What We Do 
 How We Fight Hunger &amp; Poverty 
 Take Action 
 
 

    

 
  
 Overview 
 Taste of the Nation 
 The Great American Bake Sale 
 Operation Frontline 
 Dinner Series 
 Our Partners 
   
 

 
  


  

 

 Operation Frontline® 

 

 Learn More 

 
 Program Impact 
      Participant Stories 
      Local Programs 
 Volunteering With OFL 
 Hall of Fame 
 Courses Available 
 Recipe Contest 
 

 


 Share Our Strength's Operation Frontline is a nutrition education program that fights childhood hunger by teaching families how to make healthy and budget-wise food choices.  

 Operation Frontline's hands-on classes are taught in the kitchen by professional chefs and nutritionists who volunteer to share their strength by teaching nutrition, cooking, and budgeting to low-income adults and children.  The goal of Operation Frontline is to provide a long-term solution to hunger and poor nutrition by teaching participants how to select, purchase, and prepare healthy low-cost food for their families.  

 
Since 1993, Operation Frontline has provided effective nutrition education to over 32,000 low-income families facing hunger and poor nutrition. Operation Frontline programs are run in 14 states across the country in partnership with city-wide social service agencies. Local programs provide Operation Frontline's volunteer-led classes at after school programs, family support centers, Head Start agencies, health clinics, and other community nonprofits. 

 Operation Frontline in Action 

 Learn more about Operation Frontline and how we mobilize thousands of chefs and nutrition volunteers to offer this one-of-a-kind cooking-based nutrition education program.  

 Operation Frontline Video 3:45 min 

 
 Real Media 
 Quicktime 
 

 Operation Frontline Video 13:00 min.  

 
 Real Media 
 Quicktime 
 

 
 Download the latest Real  Player or Windows Media players. 

  

  

 

  

 Find information about grant recipients, restaurants, and events in your area. 

 Enter ZIP Code or City, State 







      
 
 
 

 Impact Snapshot 

 
 2,999 Total classes 
 35,867 Class participants 
 108,717 People reached through special events 
 1,600 Volunteer instructors 
 

 Find out how Operation Frontline is impacting your community. 




 

  

 

 Back to top 

 More to see » Read Bill Shore’s letters, check our latest news or join our community. 

 © Share Our Strength 1730 M Street NW, Suite 700, Washington, DC 20036 
Media Center | Jobs at SOS | Donate/Support Our Work | Contact Us | Privacy Policy 





  
  
  





