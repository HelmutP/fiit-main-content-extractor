





 
 
 
 
Yahoo!7   My Yahoo!7   Yahoo! Mail   
 
 


 
 
Health Home - Help  
 
 
 
  
 
Sign In [New User? Sign Up]
 
 
 
 
 

  
 
 







 


  
 
 


  
 
 


 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  
 
 
  Home 
 
 
 
 
 
  Women's Health 
 
 
 
 
 
  Men's Health 
 
 
 
 
 
  Sexual Health 
 
 
 
 
 
  Parenting 
 
 
 
 
 
  Fitness 
 
 
 
 
 
  Diet &amp; Nutrition 
 
 
 
 
 
  Natural 
 
 
 
 
 
  Library 
 
 
 
 
  









 
 
 
  
 
  
  
 
 
 
 
 
 


  
 



  
 
 
 
  
 
 Search for
  
 
 Yahoo! Health
 Yahoo! News
 
  
 
   
 
  
 
  
 

hot topics : 
Pregnancy
| 
Sexual Health
| 
Diets
|
CHOICE Reviews | Health Message Boards

 
 
 
 
 
 
 
 


 
 
 
 
  
 
 
 
 


 
  
 

 
 
 
 
 
Home � CHOICE Magazine � Product review
 
 
 
 

 
 

  
   Blood pressure monitors - 5-minute CHOICE 

 
  
 
 
 
 
 
 
 
 
 
 
 
 






» printer friendly version




  
 
 


 
 
 
 
 
 
 

 What to look for About one in seven Australian adults suffer from some form of high blood pressure, or hypertension (as it's known medically). If you're one of them, monitoring your condition regularly at home with a blood pressure monitor (and in co-operation with your GP) can be useful. 
 Buy from a pharmacy or medical equipment supplier, where you can get proper advice and a demonstration of how to use the monitor. 
 Arm monitors are generally more accurate than wrist models; in our test, only one wrist model could match them. 
 If you buy an arm monitor, measure your arm size to get the right-size cuff. 
 Make sure the monitor's keys and display are easy to read. 
 Models that inflate the cuff automatically tend to work better than those requiring you to do it manually (by pumping a bulb), though there are exceptions. 
 The cuff has to be inflated to a level above the systolic blood pressure. However, the automatic setting on the monitor may be a lot higher, and you may find it uncomfortable. Models with adjustable inflation level allow you to program the pressure to one that's comfortable for you. Models with fuzzy logic detect the ideal inflation level automatically. 
 If a monitor has a memory to store a number of your recent blood pressure measurements, it can be helpful as a back-up. However, it's a good idea to record all measurements separately, so that you and your GP get a sufficient history. 
 Manufacturers give different advice regarding the use of rechargeable batteries; check in the shop before you buy. 
 While you should use a blood pressure monitor in co-operation with your GP, it's still very helpful if the monitor comes with good general background information on blood pressure issues, for your reference. 
 
 What's blood pressure? Blood pressure is created by the heart pumping blood through the arteries. It's measured as two numbers, in millimetres of mercury (mmHg) based on the standard method using a mercury sphygmomanometer (the machine your doctor uses in their surgery) and a stethoscope; giving a result such as '120 over 80' (120/80 mmHg). The higher figure is the systolic pressure, caused by the contracting (beating) heart. The lower figure is called diastolic, and is the pressure between beats, when the heart relaxes. 
 Hypertension can increase the risk of heart attack, stroke, heart failure and kidney disease, but doesn't necessarily do so. When weighing up your risk, your GP will also consider factors such as your age, sex, family history and weight, and whether you smoke. 
 
   
    
  WHO definitions
  

 
    
  
  Blood pressure (mmHg) Diagnosis

 Up to 120/80 Optimal
  Up to 130/85 Normal
  Up to 140/90 High normal
  Up to 160/100 Mild hypertension

 Up to 180/110 Moderate hypertension
  Above 180/110 Severe hypertension

 



 What you can do Your lifestyle can have an impact on your blood pressure. To prevent hypertension or to manage it once you've been diagnosed: 
 Have regular check-ups, especially when you know you have high blood pressure. 
 Don't smoke. 
 Limit the amount of alcohol you drink. 
 Exercise regularly (but note that there are certain sports to avoid when you have high blood pressure, such as lifting heavy weights). Consider stress-reducing techniques such as yoga and meditation. 
 Avoid salty and fatty foods, and reduce your consumption of red meat. 
 Eat plenty of cereals, fish, fruit and vegetables. 
 Achieve and maintain a healthy body weight. 
 If lifestyle changes don't lower your blood pressure enough, your doctor may prescribe medication. Make sure you take it, and don't adjust the dosage yourself based on your own blood pressure measurements. This is known as 'self-diagnosis', and fiddling with your medication yourself can have serious consequences. 
 
 
 
 See the full CHOICE report.     Go to choice.com.au for more expert, unbiased advice on appliances, electronics, food and finances. Buy smarter, go to choice.com.au first. 
 This article last reviewed April 2005.  

  
 
 






  
 
 



  
 
 
 
 
  
 
 
 
  
 
 Yahoo! Local Search - Find health practitioners, services and products in your area
 
 
  
 
 Search Locally for:
 
 
 in location:
 
 
 
 
 
 
 
  
  
  
 
  Keyword
  Business name
 
  
  
  Include Surrounding Suburbs
 
  
  
  
 
 
 
 
 
 
 
 
  
 
 




 
 
 
 
 
 
 
 

  
 
  
ADVERTISEMENT 





 



 
 


  
 
 












Most Recent Posts on Health Message Boards 





 
 
   Topic

  Num Replies








General Chat #2
1345



post op relationship and personal issues
153



Knox Hospital bandits - Melbourne
111



Long clear jelly string?
1



  More Health Boards









 
  
 
 
 

 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 Kids' lunchbox foods under the microscope 
 Three quarters of the kids' lunchbox snacks reviewed by CHOICE didn't meet all the nutrition criteria set by the independent consumer magazine. Read more... 
 
 
 
 
 
  
 
 
 
 
 
 


  
 
 


  
 
 
 
 
  
 


  
 
 
 
 
 
 


  
 
 



Don't get sick in South East Asia. Find out more. 




  
 
 


  
 
 




 
 
  
  
 Copyright © 2006 Yahoo! Australia &amp; NZ Pty Limited. All rights reserved.  
 Privacy Policy -
 Terms of Service -
 Help  
 
 






Site Map - Health News Archive - Health RSS 






  
 
 



 
 
 Partner Copyright: Copyright © 2004 Australian Consumers' Association All rights reserved.
 
 





