






	
		
		
		B.O.S.S.  Better Opportunities for Single Soldiers
		
			
		
		
			
				
				
			
			
				
			
			
		
	

	
		
			
				
				
						 
							 
								
									
										
											 
												ACS
 
										
									
									
										
											 
												AUTO CRAFTS
 
										
									
									
										
											 
												BOSS
 
										
									
									
										
											 
												BOWLING CENTER
 
										
									
									
										
											 
												CLUBS
 
										
									
									
										
											 
												CYSD
 
										
									
									
										
											 
												FITNESS CENTERS
 
										
									
									
										FRAME SHOP
									
									
										
											 
												GOLF
 
										
									
									
										
											 
												PLAYHOUSE
 
										
									
									
										
											 
												LODGING
 
										
									
									
										
											 
												MARKETING
 
										
									
									
										
											 
												OUTDOOR REC
 
										
									
									
										
											 
												SPORTS
 
										
									
									
										
											 
												VETERINARY
 
										
									
								
							 
							
								
									
									
								
								
									
									
									
									
								
								
									
									
									
								
								
									
										
											
												
													 
														HOME
 
												
												
													 
														NAF JOBS
 
												
												
													 
														EVENTS
 
												
												
													 
														FACILITIES
 
												
												
													 
														MWR DIRECTORY
 
												
												
													 
														FORT LEE GARRISON
 
												
											
										
									
									
								
								
									
									
									
									
									
								
							
						 
					
				
			
			
				
				
				
					 
						 B.O.S.S 
						 Better Opportunities for Single Soldiers 
					 
				
				
				
			
			
				
				
					 
				
				
			
			
				
				
					 Better Opportunities for Single Soldiers (BOSS) is a program that supports the overall quality of single Soldiers' lives.  BOSS identifies well-being issues and concerns by recommending improvements through the chain of command.  BOSS encourages and assists single Soldiers in identifying and planning for recreational and leisure activities.  Additionally, it gives single Soldiers the opportunity to participate in and contribute to their respective communities. 
						 
					 The BOSS program originated in 1989 to respond to the recreational needs of single Soldiers ages 18 - 25 who make up 35% of the Army.  As the program was implemented throughout the Army, it became evident that quality of life was the primary concern to single Soldiers.  In 1991, the Chief of Staff of the Army officially expanded BOSS to include all aspects of Soldiers' lives.  Some BOSS members then began to express an interest in participating in community service projects, which was added as a component of the program.  These components...recreation and leisure, well being, and community service are the core of the BOSS program. 
						 
				
				
				
			
			
				
				
				
			
			
				
				
					 
				
				
			
			
				
				
					
						
							
								 
									 Remaining FY07 BOSS Meetings 
									 Wednesdays, 1 p.m. in the BOSS Activity Center (Bldg. 4310 on Lee Ave) 
								 
							
						
						
							
								 
									 November 8th and 22nd 
								 
							
						
					
				
				
				
			
			
				
				
				
			
			
				
				
					 BOSS is a great influence on the Fort Lee Community. They are more than just an organization that is all fun and games. This organization is one of service and community. BOSS volunteers hundreds of hours of community service for Fort Lee and the surrounding area.  
					 From creating events such as the BOSS Children Easter Egg hunt in the spring to informing the community of other organizations special events. BOSS truly represents the values and morals of today's Army. 
					 (Right) Fort Lee Federal Union contributes to the BOSS organization, for all of their hard work and continued work with the Fort Lee and surrounding community. 
					 
				
				
				
			
			
				
				
					 
				
				
			
			
				
				
				
					 No place like home as (left to right) MWR Operations employees CSM Retired Greg Johnson, Lester Judkins, Col. Gwen Bingham, BOSS President Spc. Michael Gray, Command Sgt. Maj. Wayne Hall and MWR BOSS Advisor James Mitchell join in the grand opening of the BOSS building on July 21, 2006. 
				
				
				
			
			
				
				
					 
						 
BOSS Actitivity Center (Bldg 4310) Hours of Operation: 
 
						 Tuesday - Thursday from 1800 - 2100 Saturday and Sunday from 1300 - 1900 
							 
					 
					 Open to all Single Soldiers this facility is free to use. The activity center contains a lounge area where Soldiers can come in to relax, play board and card games or read. A muli-media room equipped with a 52” screen HDTV with cable, X-Box, and a VCR and DVD player.    
					 A internet lab also resides within the walls equiped with 7 brand new computers with wireless internet acess, DVD and CD-R drives, Microsoft Suite packages and games.  
					 There is a full service kitchen with a stove, microwave and dishes.  Perfect for a group meeting, a night out with friends or just a place to be alone, the BOSS Activity Center has everything a Soldier could need. 
					 For more information please call 734-7290. 
				
				
				
			
			
				
					 
						 A Place To Call Home 
					 
					 written by a Staff Writer for the Fort Lee Traveller, 
					 The Better Opportunities for Single Soldiers building was a work in progress much like the program itself. The grand opening of the renovated facility Friday recognized the achievement of both. 
					 Not long ago, the BOSS program was a membership of few. Senior advisor Command Sgt. Maj. Wayne Hall, Fort Lee garrison command sergeant major, and civilian advisor James Mitchell led a handful of representatives determined to make the program relevant on post. 
					 Housed within the cramped office space within the barracks off 
							
								  
									A Avenue 
								 
							
							, the group expanded to more than a dozen active members. Representatives became responsible for fundraising projects around post, and alerted fellow single Soldiers to social activities and events. 
					 As the program evolved, the opportunity to relocate became available. BOSS members helped the Red Cross move to their new location, and even volunteered to paint the new facility. Renovations began immediately on the BOSS building. 
					 More space meant more work, as BOSS members spent their free time painting, tearing down walls and decorating. 
					 The 
							
								Fort Lee 
							
							community chipped in with donations. Contributions of home furnishings and other items helped create what BOSS president Spc. Michael Gray said in closing remarks at the grand opening ceremony, “a place to call home.” 
					 Gray credited several organizations for their support, including Army Community Service, Morale, Welfare and Recreation and the Association of the United States Army. 
					 
						“Never before has BOSS had a centrally-located facility with enough space where representatives can conduct meetings, members can enjoy amenities � and most importantly, a place where the members can relax and interact socially,” said Gray.
 
				
				
				
			
			
				
				
						 
							
								
									
										
											
												
													 
														HOME 
 
												
												
													 
														PRIVACY POLICY
 
												
												
													 
														ADVERTISE WITH US
 
												
												
													 
														MISSION &amp; DISCLAIMER
 
												
												
													 
														WEBMASTER
 
												
											
										
									
									
								
								
									
									
								
							
						 
					
				
			
			
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			
		
		 
	












































