







Journalism Tools | Project for Excellence in Journalism (PEJ)













	 
		
		
			
								 
				 
		 
  

 
 
 
 
 


 
 

 
 
 
		 
RSS | Join our Email List

 
 
				 

								
				                                 
				 
				 
		 Primary links 
		  
 Home 
 Numbers 
 
Analysis 
 Studies 
 Commentaries and Backgrounders 
 Books 
 
 
 
Daily Briefing  Special Archives  
 
 
State of the News Media 
 2006 
 2005 
 2004 
 
 
 
Journalism Resources 
 Principles of Journalism 
 CCJ 
 JTools 
 Job Links 
 Journalism Schools 
 Journalism Organizations 
 Ethics Codes 
 Advice to Students 
 
 
 
About PEJ 
 About Us 
 The People of PEJ 
 Partners 
 FAQs 
 Contact Us 
 About Our Site 
 Permissions 
 Privacy Policy 
 RSS Feeds 
 
 
 Contact Us 
  
 
				 
							
		

				
			
				 
Home &amp;raquo Journalism Resources&amp;raquo Journalism Tools                         
		
		
		
                        
			
				
										 
					 
		 
  
     
Principles of Journalism 
 
 CCJ   JTools   Job Links   Journalism Schools   Journalism Websites   Ethics Codes   Advice to Students     
 
					 
					
					
					
										 
					
					 Journalism Tools 					
					
					
					
					
                                         
                    
					
					 
	    			      		
    				 
		 

    
    This page offers links to a Tool Box of ideas, strategies and techniques about how to produce and understand journalism. The materials were collected by the Committee of Concerned Journalists, a consortium of more than 7,000 working in news around the world. 
 There are Tools For Citizens, including a Citizens Bill of Journalism Rights, as well as tools for people who work in print, in television, radio and online, as well as for teachers and for students. The ideas, collected from talking with citizens and journalists around the country, are regularly updated. They are meant to help both the the public that consumes the news and the journalists who endeavor produce it on the public's behalf. 
 
  Tools for Citizens  
  Tools for Print Journalists  
  Tools for TV and Radio Journalists  
  Tools for Online Journalists  
  Tools for International Journalists  
  Tools for Newsroom Managers  
  Tools for Journalism Teachers  
  Tools for Journalism Students  
     

 
	
		 

	
 
					

                    					 
                    
                 

				
			
		
		
			
				 
				 
					
 
		 
 
Home | Numbers | Analysis | Daily Briefing | State of the News Media | Journalism Resources | About PEJ | Contact Us  
 
Copyright 2006 Project for Excellence in Journalism.    Privacy Policy | Site Map
 
 
 Powered by: Phase2 Technology
 
 
 
 
				 
			
		
	
	 
	    





