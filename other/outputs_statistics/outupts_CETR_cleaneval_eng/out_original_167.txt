


      

 

 

 

 
   
   


  

 
  

 













 


  

  


  
 
  
     

 


                                           
 
 
 

 

 

 

 
   
 


  

 
  

 













 


  




  
                                                       
            
 
 
 
 
 
 
 

 
                  
 

  
 
 
    
 
  
 

 
 
 
  

 


         
 
   
 
    
     

     
           
 


 
 





 










  
BW Online | February 16, 2004 | The Budget: Hey Guys, Get Real 
    
	
	
	
	
	
	
	
	
	
	
	
	      
		
		
		             
	
	
	
	
	     

      



 


 
   
 



 
      








    


    

 
        


     



 
    

 
 
 



 
      





    

     

 
 
 



 
      

 










 
			    

  
    


 
 
 



 
      
	

			 
		

		
			 
				
					SEARCH SITE 
					
					
					 
					Advanced Search
				
			 


			
		
	



	
	
	
	
	
	
	
	
	
	
	


 



    


 
  	       

     

 
 
 



 
            

 

 

 

 
   
   



  
                                                                                                                                                                         


               
     

 


                                           

  
   
 
 





 
   
 



 
      
 

Get Four  Free Issues

 

 Register 

 Subscribe to BW 
 Customer Service 

    


                       
 
 





 










     Full Table of Contents                                                                                                                                                           


             


          Cover Story                               


             


          International Cover Story                                 

 
                                   
          Up Front                                 

 
                                   
          Readers Report                                 

 
                                   
          Corrections &amp; Clarifications                               


             


          Technology &amp; You                               


             


          Books                               


             


          Economic Viewpoint                                 

 
                                   
          Economic Trends                               


             


          Business Outlook              

 
 
 



 
      

 

 










 
			
     

                            


             


          News: Analysis &amp; Commentary                                 

 
                                   
          In Biz This Week                                 

 
                                   
          Washington Outlook                               


             


          Asian Business                               


             


          European Business                               


             


          International Outlook                               


             


          Information Technology                               


             


          Finance                               


             


          Government                               


             


          People                               


             


          Science &amp; Technology                               


             


          Social Issues                               


             


          Management                               


             


          The Barker Portfolio                               


             


          Inside Wall Street                               


             


          Figures of the Week                               


             


          Editorials                                                                             INTERNATIONAL EDITIONS                                                                                                                                                              


             


      International -- To Our Readers                                

 
                                   
      International -- Readers Report                              


             


      International -- Finance                              


             


      International -- Int'l Figures of the Week                              


             


      International -- Editorials                       

  
        

  

 
 
 



 
      



	


	 
	 
		PREMIUM CONTENT  MBA Insider 
	 
 

 
	 
BW MAGAZINE 
		Get Four Free Issues 
		Register 
		Subscribe 
		Customer Service 
	 
 
	
 
	 
		ONLINE FEATURES 
		Book Reviews 
		BW Video
		Columnists
		Interactive Gallery 
		Newsletters 
		Past Covers
		Philanthropy
		Podcasts
		Special Reports 
		
		BLOGS
		
		Auto Beat
		Bangalore Tigers
		
		Blogspotting
		Brand New Day
		Byte of the Apple
		Deal Flow 
		Economics Unbound
		Fine On Media
		
		Hot Property
		Investing Insights
		
		New Tech in Asia
		
		NussbaumOnDesign
		Tech Beat 
		Working Parents
		TECHNOLOGY 
		J.D. Power Ratings
		Product Reviews
		Tech Stats
		Wildstrom: Tech Maven 
		
		
		AUTOS
Home Page
Auto Reviews
Classic Cars
Car Care &amp; Safety
Hybrids
		
		INNOVATION  &amp; DESIGN
Home Page
Architecture
Brand Equity
Auto Design
Game Room


		SMALLBIZ 
		Smart Answers 
		Success Stories 
		Today's Tip 
		
	
	INVESTING 
	
Investing: Europe 
		Annual Reports 
		BW 50 
		S&amp;P Picks &amp; Pans 
		Stock Screeners  		
		Free S&amp;P Stock Report 

		SCOREBOARDS
		Hot Growth 100 
		Mutual Funds 
		Info Tech 100 
		S&amp;P 500 
		
		B-SCHOOLS 
		Undergrad Programs 
		MBA Blogs 
		MBA Profiles
		MBA Rankings 
		Who's Hiring Grads 

		
	 
 
  
	 
		BW EXTRAS
		BW Digital 
		
		BW Mobile
		BW Online Alerts
		Dashboard Widgets
		
Podcasts 
	RSS Feeds 
		Reprints/ Permissions 
		Conferences 
		Research Services
	 
 

    

      
  
 
   
 
    FEBRUARY 16, 2004    

 
 
 



 
          

      

 
 
 



 
      

 

 










 
			
 
    

          
 

  
 


 
 
 



 
      



 
	 
		  STORY TOOLS
	 
 
 
	 
		Printer-Friendly Version
	 
	 
		E-Mail This Story
	 
	



















			


 
    



  



 

          


 
                   


             


       Graphic: Mopping Up The Red Ink                                                   

 




 




                    

  

 
 
 



 
            

 

 

 

 
   
   


  

 
  

 













 


  

  


  
 
  
     

 


                                           
 


 
  

 



 




              NEWS: ANALYSIS &amp; COMMENTARY       


             


      The Budget: Hey Guys, Get Real        


             


      Commentary: Cutting Through The Budget Smoke        


             


      John Kerry: Already On The GOP Firing Line        


             


      Commentary: Why The Dean Bubble Popped        


             


      Autos: Those Price Breaks Are Habit-Forming        


             


      Wal-Mart Eases Its Grip        


             


      Commentary: Medicare vs. Cancer Patients        	 	 

   

  
 
      

 




 
     

 
 
 



 
      
    

                          NEWS: ANALYSIS &amp; COMMENTARY                                                     The Budget: Hey Guys, Get Real               President Bush says he wants to cut the deficit in half by 2009. Here's why that's not likely to happen             
  
   


    
    
    
    
    
          



        Under increasing pressure from conservatives, George W. Bush, a man not known as a penny-pincher, is seeking to make fiscal restraint a centerpiece of his fiscal 2005 budget. After three years of surging deficits, Bush is sounding like an old-school GOP budget hawk. "With spending restraint and continued pro-growth economic policies, we can cut the deficit in half over the next five years," he says. 

 
 
 



 
      

 

 










 
			
			
     

   
   Bush's Feb. 2 fiscal blueprint indeed proposes to cut the deficit in half by 2009. And, if it were adopted, it would effectively freeze funding for one high-profile chunk of domestic spending for the next five years. But critics argue that in the end, as they say in Texas, the President is all hat, no cattle.  
   Why? For starters, the budget does nothing to trim massive spending on entitlements, by far the largest chunk of federal spending. Instead, to hit his deficit target, Bush relies on a five-year freeze on the much smaller spending that goes into hundreds of domestic programs, such as law enforcement, environmental protection, housing, and education.  
FIVE-YEAR PLAN. Yet even that is largely illusory, given that Congress has never accepted that level of restraint. Even less realistic: The budget calls for no additional funding for Iraq and Afghanistan after Sept. 30, even though the nonpartisan Congressional Budget Office figures reconstruction of the two war-torn countries could cost $50 billion and continued occupation will cost at least an additional $20 billion a year.  
   Perhaps most misleading, Bush is offering up a five-year budget rather than the traditional 10-year plan. In doing so, he is hiding the long-term costs of last year's Medicare drug law, big future hikes in Pentagon spending for non-Iraq-related projects, and the impact of his latest $1.3 trillion tax-cut plan. Indeed, the President's latest plan includes the $990 billion cost of making his '01 and '03 tax cuts permanent as well as $300 billion in new subsidies for saving and buying health insurance. But less than 20%, or about $200 billion, of the revenue drain will take effect through 2009. The rest will occur in 2010-14 -- meaning ballooning deficits just past the horizon of the five-year budget estimates.  
   So how serious are Congress and the President about deficit reduction? Politicians will come to blows over a handful of fiscal matters in the coming months. Pay special attention to battles over two major bills -- one to build and repair highways, airports, and mass transit; the other a long-awaited energy bill aimed at providing subsidies to domestic producers with the stated goal of reducing reliance on foreign oil. And don't forget the fight over Bush's proposed tax cuts.  
   Capitol Hill conservatives are putting heavy pressure on Bush to restrain spending even more than he has proposed. And, already, rising deficits have opened a major rift within the GOP. On one side, hardliners such as House Majority Leader Tom DeLay (R-Tex.) see the deficit as a wedge into the issue they really care about -- shrinking the size of government. On the other, traditional fiscal conservatives such as Senator Olympia J. Snowe (R-Me.) favor government spending for domestic programs but worry about red ink. Then there are the lawmakers whose main concern is protecting home-state projects. The GOP chairmen of both the House and Senate Appropriations Committees are warning that they can't pass Bush's freeze.  
   With an election looming, most Democrats and many Republicans prefer to avoid chopping popular programs. "It's going to be a very difficult budget year," says House Budget Committee Chairman Jim Nussle (R-Okla.).  
   The first test of fiscal credibility will come when Congress acts on a massive six-year transportation bill. House committees favor spending $375 billion. The Senate is debating a $318 billion version. But the Administration warns that Bush will veto any measure that costs more than $256 billion. There is little chance Congress will O.K. less than $285 billion. So in any my-way-or-the-highway showdown with Bush, who has yet to veto any spending bills, bet on the highway to win.  
   A second test will come once debate on the long-delayed energy bill starts again. Bush initially asked for $20 billion in production incentives. That has ballooned to $31 billion as a result of added congressional pork. The White House is trying to trim the measure. But unless lawmakers scale it back to $20 billion or less, it will be hard to claim fiscal discipline.  
   Then there are taxes. There's no chance Bush will win approval of his full $1.3 trillion tax package. But GOP lawmakers will push to make permanent a fistful of middle-class breaks that were first adopted in 2001 but due to expire over the next few years. They include marriage-penalty relief, an increase in the child credit, and a temporary fix to the painful alternative minimum tax. But if Congress is serious about deficit reduction, it will have to ditch much of the tax package  
NICKELS AND DIMES? Already, legislators are looking at finessing the Bush budget. One strategy: delay key spending votes until after the elections. Another idea is to grant Bush's total $800 billion-plus spending request for all nonentitlement programs but shift funds from defense to domestic priorities. Congress would still give Bush the money he wants for the Pentagon -- but in a later supplemental bill of about $50 billion. In the yearend confusion, the President could take credit for holding down spending, while getting all the money he wants for defense.  
   But even if such a strategy succeeds in holding spending down in 2005, it is unlikely to have much impact on long-term deficits. That's because the programs the President wants to freeze represent barely one-fifth of the budget, making his newfound deficit concerns seem illusory at best. Environmental protection, crime prevention, and the like may be important to many Americans, but they don't cost much -- at least not by Washington standards. Of the $2.4 trillion that Bush would spend in '05, these domestic programs account for just $485 billion. "This is not the problem," says Robert L. Bixby, director of the Concord Coalition, an anti-deficit group.  
   The real money is in defense -- which will cost nearly $428 billion next year, even without Iraq -- and the entitlement programs such as Medicare, Medicaid, and Social Security. The big three will soak up a staggering $1 trillion next year. And, says the CBO, they will cost more than $1.4 trillion a year by 2010.  
   To put it another way, Washington will be arguing over a mere $10 billion in domestic programs. By contrast, Medicare spending will increase by more than twice that next year -- even before a costly new drug benefit kicks in. "Spending will not be brought under control until entitlements are seriously reformed," says Heritage Foundation budget expert Brian M. Reidl. "You can't nickel-and-dime your way to balancing the budget."  
   Not that $10 billion doesn't matter. And fiscal responsibility needs to start somewhere. But Bush's budget -- and the likely congressional response to it -- will prove to be little more than election-year gamesmanship. It may help Bush, who could claim progress toward fiscal restraint. And it will allow lawmakers to campaign with new ribbons to cut. What the budget gymnastics won't do, however, is reduce the deficit by very much. 


               By Howard Gleckman, with Paul Magnusson and Stan Crock, in Washington      

 
 
 



 
          

 

 
 
 



 
       
	 BW MALL  SPONSORED LINKS 
			 
				 
 
ERP Study For Hi Tech Companies 
Download ERP success stories with a free report from SAP for high tech companies. 
 
FFIEC Security Compliance 
FFIEC Compliance True ID Protection Fast deployment; Simple integration from security leader VeriSign Learn more-Download Free whitepaper 
 
ETF Investing  
Discover iShares. More Tools. More strategies. More possibilities.
 
 
Discover iShares ETFs 
Over 100 ETFs. More tools. More possibilities. 
 
University of Phoenix - Business Degrees 
Earn your business degree online or at a UofP campus near you. Request info! 
				 
				 
					Buy a link now!
				   
			 
    

  

 
 
 



 
      















Get BusinessWeek directly on your desktop with our RSS feeds.  

Add BusinessWeek news to your Web site with our headline feed.  

Click to buy an e-print or reprint of a BusinessWeek or BusinessWeek Online story or video.  

To subscribe online to BusinessWeek magazine, please click here.  

Learn more, go to the BusinessWeekOnline home page 


    

       
   
 
         



 
 
 



 
       


 
	 
	 TODAY'S MOST POPULAR STORIES 
	 
	
	 
		  
	
 
 
How Bad Will the 2007 Property Market Be?
 
 
Three Technicians' Bullish Stance on Stocks
 
 
Retail Results Signal Tough Times Ahead
 
 
The Golden Parachute Club of 2006
 
 
Housing: Curb Your Enthusiasm About A Recovery
 
 

		   
Get Free RSS Feed &gt;&gt;   
	 

	 
		  MARKET INFO 
	 
	
	 
	

	DJIA
	12407.63
	+64.40


	S&amp;P 500
	1416.90
	+6.14


	Nasdaq
	2413.51
	+12.33



	
		 
			
			
			
				 
			
			
			  
				Stocks Rise as Oil Prices Fall 

			  
		
			 
				Create / Check Portfolio 


				Launch Popup Ticker
			 
	
		 
		
		 
	









			
		 
	 
 
 
    


 

 

 
 
 



 
      

 










 
			

    

  


 
 
 



 
      

    


  

 
  
 
        Copyright    2004,    by The McGraw-Hill Companies Inc. All rights reserved.  Terms of Use | Privacy Notice          

 
 


 
 
 



 
       
  Media Kit | Special Sections | MarketPlace | Knowledge Centers 
 

 















  



























    


 
  
   

  
 
  


