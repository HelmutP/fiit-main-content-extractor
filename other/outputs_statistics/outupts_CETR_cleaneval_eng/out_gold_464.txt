
 URL: http://www.mercycorps.org/items/1150/

One night in Ogonyo

     Over the past two decades, Uganda's civil war has forced hundreds of
   thousands of families into miserable camps. Now, buoyed by recent
   peace talks, many are clustering into less-cramped "return camps."

    
 Senior writer Roger Burks spent a night alongside displaced families
   in one of these makeshift villages, Ogonyo, where Mercy Corps is
   helping residents support themselves while they await the day they can
   return home.
  
   Behind the Horn of Africa's Brewing Storm
   Somali child 

     Randy Martin, director of Global Emergency Operations, discusses the
   worsening conditions on the Kenya-Somalia border, where Mercy Corps is
   delivering critical supplies amid the growing concerns of a regional
   war.
   
   Tsunami preparedness drill 

     Mercy Corps is helping communities in tsunami-affected areas of
   Indonesia. Since December 2004, we've assisted more than 1 million
   survivors with innovative programmes designed to "build back better."
  
   Making holiday cards 

     We would like to introduce our new online shop, which features
   Christmas cards, decorations and calendars. Your holiday purchases
   help us fund critical projects in more than 35 countries around the
   world.
  
     
