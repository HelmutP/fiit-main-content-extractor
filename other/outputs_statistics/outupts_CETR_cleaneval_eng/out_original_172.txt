







BBC - Science &amp; Nature - Articles - North Kent Marshes - a secret wilderness


























































 






	
	
	
	






































 

 





Home
 
TV
 
Radio
 
Talk
 
Where I Live
 
A-Z Index
 




















 27 December 2006 Accessibility help Text only



















 Animals 


 Prehistoric Life 


 Human Body &amp; Mind 


 Space 


 Hot Topics 


 TV &amp; Radio Follow-up 





 









 BBC Homepage   Science &amp; Nature Homepage 








 In Animals: 
 
Planet Earth
Birds
Mammals

UK wildlife

 

	 
	Articles
	 
		 
	Autumnwatch
	 

		 
	Garden wildlife
	 

	 
	This month
	 
	
	 
	Field guides
	 

	 
	Habitats
	 
	
	 
	Look around
	 
	
	 
	Mini Oddie
	 

	 
	Nature near you
	 
	
		 
	Springwatch
	 
	

	
	
 
Sea life
Wildfacts
Conservation
More articles
Pets
Children's zone
 





 
Contact Us  Like this page? Send it to a friend!  









You are here: 
BBC &gt; 
Science &amp; Nature &gt; 
Animals &gt;  
UK Wildlife &gt;  
Articles












 







More articles















			
*** Choose a subject ***
 
Birds
Mammals
Sea life
Conservation
UK Wildlife
Miscellaneous
 

















 












North Kent Marshes - a secret wilderness






 

The North Kent Marshes are the Thames Estuary's secret wilderness. Words: Irene Palmer Images: Robert Canis, John and Irene Palmer  











 




 Page 1
 
 Page 2

 Page 3

 Page 4
  


 








 







 A bird's eye view As my flight enters the broad mouth of the Thames Estuary, I savour the bird's-eye view of this area I know so well. The coastline resembles the gaping, dislocated jaws of some fossilised dinosaur that once roamed here, but time has rendered it harmless and tides have scattered its teeth to form countless islands. My eyes scan the pewter-grey mudbanks and mudflats and a distant shoreline etched with filigrees of sinuous creeks. My fellow passengers mostly ignore the fantastic views, because this isn't a 'must-see' UK destination - and yet it's one of those rare places where big skies reach to limitless horizons, giving a sense of wilderness, where encountering another person is a rare event. And though it looks empty, it teems with wildlife. 

 The estuary narrows and the coastline becomes increasingly industrialised. Buildings and chimneys crowd the shores as we descend into London. It's good to be reminded I can find an antidote to city life on my doorstep, where the 'Garden of England' hides its wild nature. 

 Complex area The North Kent Marshes is a complex area extending from Gravesend to Whitstable. Their beauty is an acquired taste - often bleak and unwelcoming, they lack the chocolate-box charms of the North Downs, and the horizon is punctuated by exclamation marks from power-station chimneys. Their natural resources have been exploited since Roman times, and the flotsam and jetsam of human enterprise have left areas of dereliction that are surprisingly species-rich - who would believe that rare ants, bees and wasps favour such places? And, despite industrialisation, large areas are wild and seldom explored. 

 Worth staying a while The marshes can be visited in a day from London, but they don't reveal themselves easily. For a longer stay, choose Rochester as a base. From here, you'll find that cycling and walking is the way to penetrate the best wildlife areas. It takes four days to walk along the Saxon Shore Way from Gravesend to Whitstable (cyclists can follow the National Cycle Route), but it's more rewarding to make diversions and dip in and out as the fancy takes. I have selected three locations to give a taste of the main habitats in the area. 
 Northward Hill Reserve 
Riverside Country Park 
Swale National Nature Reserve 

 Further information North Kent Marshes is under threat from a plan to build a four-runway airport twice the size of Heathrow at Cliffe. According to the proposals, the tens of thousands of birds that come here to feed and breed will be "discouraged" from a 26km diameter exclusion zone around the airport. Whether this will be possible is open to question. The danger of bird-strike has been dismissed by developers as "problematic but not insurmountable." Cliffe airport will be a most important test case for the Government's commitment to conservation. The consultation period on the proposal ends on 30 June. 

 Contact: 
Department for Transport.  
Kent Wildlife Trust 
RSPB 

 Tide tables 
Invaluable for exploring Kent's marshes. Available from Medway branches of WH Smith and chandlers. Quick Tide South East
 

 Precautions 
Be prepared for cold weather. The area attracts wind, and even in summer, the estuary can be several degrees cooler than inland. Beware of walking on the mudflats at low tide, because there are soft areas where it is easy to get stuck. And be aware of the speed of the rising tide when exploring areas exposed at low tide; it is easy to become marooned. Obtain tide tables or local advice. 





 Page 1
 
 Page 2

 Page 3

 Page 4
  


 


 The BBC is not responsible for the content of external sites listed. 
 
From an original article in the July 2003 issue of BBC Wildlife Magazine - Natural break. 






















 


 

 


















 


 



























 


















You may also like...





 

 the UK's best coastal wildlife hotspots 

 more UK Wildlife articles 
 more BBC Science and Nature articles 
 








 




 



Science &amp; Nature Homepage 
Animals | Prehistoric Life | Human Body &amp; Mind | Space | Hot Topics | TV &amp; Radio follow-up
 
Go to top 



 









 


 About the BBC | Help | Terms of Use | Privacy &amp; Cookies Policy  


 






