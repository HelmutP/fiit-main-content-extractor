



   
	     

                     
	         
     
	
		TIMEeurope.com - News Magazine - Current Events 
		
		
		
		
		
		
  
		
		
		 
		
		
		
		 

		
		
		
		
		
	
	
	
	
	   

   
 

	





 
	
	    
	        
			 
	
	
		
		
	
 


	    
		
		 
	    	
			
 
	    
		
	    
	        
	        
				
					
						

					
						
						
						
 
Tuesday, December 26, 2006  
					
						 
	 
		
		
	 
	 
		 
			 
			A Fond Farewell 
TIME's Person of the Year issue waves a last goodbye to those we lost in 2006 
		 
   
 

					
					
						 &lt;noscript&gt;&lt;a href="http://adremote.timeinc.net/click.ng/site=time&amp;amp;channel=cm&amp;amp;adsize=234x60&amp;amp;pagepos=cmtout_europe" target="new"&gt;&lt;img src="http://adremote.timeinc.net/image.ng/site=time&amp;amp;channel=cm&amp;amp;adsize=234x60&amp;amp;pagepos=cmtout_europe" width="224" height="60" border="0"&gt;&lt;/a&gt;&lt;/noscript&gt;

					
						

				
			

			
	 		 
		
	    
	         







   



All of TIMEeurope.com
Past 30 Days
Current Issue











	    
		
		
			 
 
	 
Home Europe Middle East Africa Technology  Business Arts Travel Global Adviser Special Reports Photos
	 
	 
	 
	Current Issue Past Covers TIME Archive  

 
	 
		 Home 
		 Collections 
		 Feedback 
	 
 
 
Other News
 
 
	 
		 U.S. 
		 World 
		 Asia 
		 Canada 
		 Pacific 
	 
 
 
 Customer Service 
	 
		 Subscribe 
		 Renew 
		 Change Address 
		 Reprints 
		 Give a Gift 
	 
	 Mobile Edition 
	 Media Kit 
	 
		 Press Releases 
	 
	 
 


 
 


 
 
 
			
						
		
		
		
			
			
			
			

			
			
			
            
			
				
				
					
					
					 
					
				
				
				
				
					
					
						  
					

					
				
				
				 
				
					
					GINA LEVAY FOR TIME
					
				  
				
					
					Leila, Our real lonely girl in her room in her house in Maryland. 

					
				 
					
					 
					
					    
						2006: Power to the People 
						
						  
						 This was the year of the web generation, a year that saw the rise of a new digital democracy. Meet 15 of the web generation's biggest movers and shakers  
						  
						  	
						  
						
						  
						
						 
							
							 
							
							Turning Iraq's Tribes Against Al-Qaeda 
							 
							
								  On Scene: In Ramadi, American commanders are going to local sheiks for help. The results are promising — but is there a problem if your ally is a bandit ringleader? 
							 
						
						 
							
							 
							
							Can the Web Poll Prevent a Rigged Election? 
							 
							
								 By simply posting the last election's voter roll online, Bangladeshi corruption fighters hope to prevent voting fraud 
							 
						
						 
							
							 
							
							America's Double Standard on Democracy in the Middle East 
							 
							
								 Viewpoint: How inconsistencies in  responses to Lebanon and Gaza undermine  U.S. credibility 
							    
					  

					
				
				
					
					
					
											
					
  

 

	
	 
	 
GLOBAL ADVISER 
		
		 
		 
			 
A Zen Palette 
			 
 Lakeland Lark   
			 
Vintage Estonia 
		 
 
 
	  
	 
	
	
	
	 
		 
SPECIAL REPORT  
		 
			  A Simple Solution    
			Diarrhea is deadly but preventable 
		   
		 
			
	  
	 
	

    
					
					
					
					 
						 
							 More Stories 
						 
					 
					
					  
						 
						  
						  
						  
						  
						
							 
							World 
							
							 
The Year That Religion Learned Humility 
							The new millennium saw the rise of fundamentalist faith as a cultural force. In 2006, says Andrew Sullivan, the religious monoliths began to break down 
 
							
						  
						
							 
							World 
							
							 
Syria in Bush's Cross Hairs 
							Exclusive: A classified document suggests the Administration is considering a plan to fund political opposition to the Damascus government. Some critics say it would be an unwarranted covert action 
 
							
						  
						
							 
							U.S. 
							
							 
A Man Of Mettle 
							Steel baron Lakshmi Mittal won a takeover battle — and taught the world a lesson 
 
							
						  
						
							 
							 
							
							 
Vladimir Putin: Turning Energy Into Power 
							Oil has troubled the waters for Russia's President 
 
							
						  
						
							 
							World 
							
							 
Would a Troop Surge in Iraq Work? 
							In some Baghdad neighborhoods, more U.S. soldiers have helped bring down violence. In other troubled regions like Anbar Province, they may exacerbate it  
 
							
						  
						
							 
							World 
							
							 
A Setback for Ahmadinejad 
							The rejection of his candidates in municipal elections highlights the domestic weaknesses of Iran's fiery president 
 
							
						  
						
							 
							Person of the Year 
							
							 
Time's Person of the Year: You 
							In 2006, the World Wide Web became a tool for bringing together the small contributions of millions of people and making them matter 
 
							
						  
						
							 
							Person of the Year 2006 
							
							 
Nicholas Stern: Calculating the Cost Of Climate Change 
							Tony Blair's top economist wants more than wasted hot air 
 
							
						  
						
							 
							World 
							
							 
Russia Tries to Look Relevant 
							Its presence at the talks is not matched by any leverage 
 
							
						  
						
							 
							Person of the Year 2006 
							
							 
A Fond Farewell 
							A last goodbye to those we lost in 2006 
 
							
						  
						
							 
							Arts | Theater 
							
							 
Two for the Road 
							The Presnyakov brothers' witty, disturbing plays about Russian life are worldwide hits 
 
							
						  
						
							 
							Arts 
							
							 
The Great China Sale 
							Foreign collectors want them. Newly minted local millionaires want them. The rise and rise of the country's modern artists 
 
							
						  
						
							 
							Art | Books 
							
							 
10 Best 
							TIME's pick of the year's top books from Africa, Europe and the Middle East, and the films that lit up 2006 
 
							
						  
						
							 
							World 
							
							 
The Princess Is Dead. Long Live the Princess 
							Scotland Yard debunks the conspiracy theories, but the mythology of Diana seems to only get stronger 
 
							
						  
						
							 
							Letter From Tuscany 
							
							 
A Dead Man's Walk Ends Far from Home 
							How Gregory Summers, a Texan triple-murder convict, came to rest in Italy 
 
							
						  
						
							 
							Notebook | Europe 
							
							 
Teaching Old Dog Owners New Tricks 
							New European governments aim to clean up their streets with a pet project 
 
							
						  
						
							 
							Books 
							
							 
Ignorance is a Killer 
							In The Ghost Map, Steven Johnson charts a Victorian cholera outbreak with lessons for the world today 
 
							
						  
						
							 
							World 
							
							 
Iran Reacts Favorably to the Baker-Hamilton Plan 
							While the White House remains wary of the proposal to talk with Iran, Tehran sources tell TIME that the regime believes such talks are in the country's best interest 
 
							
						  
						
							 
							Europe | Turkey and the E.U. 
							
							 
Slow Train to Europe 
							As talks over Cyprus grind to a halt, Turkey's membership of the E.U. looks ever more distant 
 
							
						  
						
							 
							Business | Currency 
							
							 
Dollar Doldrums 
							A stalling U.S. economy means the dollar?s slide may not be over 
 
							
						  
						
							 
							Europe | Russian Chill 
							
							 
Russia's Bitter Chill 
							Much has changed in Russia since the end of Soviet rule. But the mysterious death of former KGB spy Alexander Litvinenko, a recently vocal critic of the Kremlin, has raised fears among the country's new dissidents  
 
							
						    		
						
						
						 							
						


                   
	         
    
	                     
	          
  
		 
			 RECENT COVERS 
		 
	 
	 
	
		   
		
		 
			  
			 Dec. 18, 2006 
		 
		    
		
		 
			  
			 Dec. 11, 2006 
		 
		    
		
		 
			  
			 Dec. 4, 2006 
		 
		       
    
						

					
				

        
		

		
				
		 

	
	
		
		 
		
  
	     

                     
	         
     
	 
		 In This Issue 
	 
 
 
	
	 
	 
	
	
	
	 
	
	 
	
		
		 		
			Person of the Year 2006: It's You!	
		 
		
		 		
			Newsmaker: Steel baron Lakshmi Mittal	
		 
		
		 		
			People Who Mattered: Vladimir Putin	
		 
		
		 		
			Arts: Our top 10 books and movies	
		 
		    
		
		 
			Table of Contents
		 
		
		 
			See Recent Covers
		 
		
	 
		
	 		
		
 

 
		 
		
	
	
		
		
		 
ADVERTISEMENT 





 

		
	
	
		
		
		 			
		
  
	     

                     
	         
        
	 
		 
			 Africa 
			 		
				MORE Africa		
			 
		 
	 
	 
		 		
			
			An African Miracle 
			
		 
	
		  
		
		 	
			Putting Breast Milk to Good Use
		 
		
		
		 	
			Making House Calls - to Africa
		 
		
		
		 	
			A Taste Of Success
		 
		  
	
	    
	
 
			
			 
		 
			 Special Reports 
			 		
				MORE FEATURES		
			 
		 
	 	
 
		 
			 60 Years of Heroes 
		  They've battled tyrants and conquered their own fears, scaled mountains and reached peaks of perfection. A look back at 60 years of outstanding individuals 
			 Europe's New Frontiers 
			 A Simple Solution For A Deadly Disease  
			 Russia's New World Order 
   		
        
  	
 
   
   
		 
			 Photo Galleries 
			 		
				MORE PHOTOS		
			 
		 
 
 
		 
			 		
			  		
					Inside al-Jazeera 
		  TIME gets an exclusive preview of a new TV news station
 
		  
			 
Targeting Turkey's Tourists  
			 Airports on Alert 
			 Europe Boils 
		 		
        
  	
 
			
		 		
		
  
	     

                     
	         
       
	 
		 
			 Travel 
			 		
				MORE Travel		
			 
		 
	 
	 
		 		
			
			The Desert's Edge 
			Marrakech has become a showcase of high design, a trend that's keeping local craftsmen busy
		 
	
		  
		
		 	
			Tempering Tannat
		 
		
		
		 	
			A Heavenly Night's Rest
		 
		
		
		 	
			Old School Thai
		 
		  
	
	     
	
 			
		 
		
		
	
	
		
		
		

		
	
	
		
		
		 

	 
		 
			 From CNN International 
			 
				
				
			 
		 
	 

	 
		 
			 		
						
					Iraqi appeals court upholds Hussein death sentence 
						
			 
			 		
						
					Pipeline explosion kills at least 200
						
			 
			 		
						
					Biden wants Rice to testify on Iraq policy
						
			 
			 		
						
					Quakes strike off Taiwan; tsunami appears unlikely
						
			 
			 		
						
					Islamic forces retreat in Somalia
						
			 
		 	
	 
	
 

		
	
	
		
		
		
									
										
									
									
										
										
		
		
		
									
	
		
		
		
		
			
				
					
					
				
				
					
SEARCH THE ARCHIVE 
						

					
				
				
					
SEARCH COVERS 
						

					
				
				
					
See All Covers 

					
				
				
					
					
				
			
			
		
		
		
	
	
	
		
		
		
		
		
	
	
		
		
		
			
				
					
				
				
			
		
		
		
	
	
		
		
		
		
		
	
	
		
		
		  Subscribers get FREE access to the Archive
		
		
	
	
		
		
		
		
		
	
	
										
									
								

		
	
	

 

		
	
 	
        
        
		










 
 Quick Links: 
 Home 
 Europe 
 Africa 
 Mid East 
 Business 
 Arts 
 Travel 
 Global Adviser 
 Tech 
 Special Reports 
 Photos 
 This Week 
 







 
Copyright ©
	 Time Inc. and Time Warner Publishing B.V. All rights reserved. Reproduction in whole or in part without permission is prohibited. 
			 
			Subscribe | Reprints &amp; Permissions | TIME Opinion Panel | Customer Service  
			Privacy Policy | Cookie Policy | Terms of Use | Media Kit | Press Releases 
			 
			TIME Style &amp; Design: How We Shop Now  
			 
			Try AOL UK for 1 month FREE | Try FOUR free issues of TIME | Give the Gift of TIME 
			TIME Global Adviser | TIME Next | TIME Archive 1923 to the Present | TIME Europe Covers Gallery 			 
			Join the TIME Education Program 			 
Search | Letters to the Editor | Contact Us 
			 
			EDITIONS:  TIME.com | TIME Asia | TIME Canada | TIME Pacific | TIME For Kids 
			 





		
	

					
	
	 			
											
	
	  



