



 

 
   W3C home &gt;
   Mailing
    lists &gt;
   Public &gt;
   ietf-discuss@w3.org &gt;
   September 2000
 

 Re: Mobile Multimedia Messaging Service 












 
 
This message:
[ Message body ]
 [ Respond ]
 [ More options ]
 
 
Related messages:

[ Next message ]
[ Previous message ]

[ Next in thread ]
 [ Replies ]

 
 

 

 
 

From: James P. Salsman &lt;bovik@best.com&gt;
 
Date: Fri, 15 Sep 2000 14:52:03 -0700 (PDT) 
Message-Id: &lt;200009152152.OAA28455@shell9.ba.best.com&gt;
 
To: Jim.Mathis@Motorola.com, ned.freed@innosoft.com, paf@cisco.com
 
Cc: discuss@apps.ietf.org, ietf-mmms@imc.org, ietf@ietf.org
 
 
 If there has been no charter proposed for a MMMS working group, I 
intend to propose one as follows.  I would like to know how the 
Area Directors feel about the following ideas, many of which are 
informed by the comments of Patrik, Ned, and others in the Apps 
Discussion archives -- http://wilma.cs.utk.edu/mail-archive/discuss.1999-12

Proposed MMMS working group purpose:  The MMMS working group will 
produce a series of Internet-Drafts, and attempt to produce RFCs, 
describing the use of end-to-end Internet multimedia messaging and 
related services on mobile devices.  The specifications will concern 
application-level services as well as general end-to-end Internet 
service specifications and lower-level services such as non-wireless 
serial PPP access from mobile devices.  No new protocols will be 
created; only existing open Internet standards and protocols free from
any intellectual property encumbrance will be incorporated into MMMS 
specifications, along with specific implementation interoperability 
guidelines.

Proposed special document status and process for MMMS specifications:  
Recognizing that it may be impossible to achieve consensus on topics 
in which large and diverse corporate concerns have vested interests 
opposed to open standards, the MMMS working group will have a special 
goal to produce Internet-Drafts which will identified as "Frozen MMMS 
RFC-track Internet-Drafts."  These documents might never become RFCs 
because of an expected lack of consensus, but upon identification as 
such by the Working Group, they will be announced and archived as 
frozen documents.  Developers may rely on such documents as static 
and vendors and customers will be encouraged to refer to them in their 
procurement process.  The level of consensus required to designate 
Frozen MMMS RFC-track Internet-Drafts will be a simple majority of 
those working group participants who are not affiliated with 
organizations having direct or indirect financial interests in 
closed-protocol mobile services.  Any member of the working group not 
affiliated with organizations having direct or indirect financial 
interests in closed-protocol mobile services may call a vote on the 
designation of any Internet-Draft as a Frozen MMMS RFC-track 
Internet-Draft, and if after one week's time there are more such MMMS 
WG participants in favor than dissenting, the WG Chair, the author of 
the draft, or the participant calling the vote will then take steps to 
establish a permanent archive of the draft and publish an announcement 
of its availability to the IETF community.  Anyone affiliated with 
organizations having direct or indirect financial interests in 
closed-protocol mobile services may not call a vote or vote on such 
special designation, but all participants will have normal consensus 
rights in the traditional RFC process, and all Internet-Drafts must 
follow the existing IESG intellectual property disclosure process.  

Proposed milestones:  The goals of the MMMS WG will include the 
publication of specifications, as described above, on the following 
topics:  interoperable end-to-end Internet service access on mobile 
devices, including descriptions of existing end-to-end wireless 
Internet service arrangements; implementation guidelines for multimedia 
messaging services on mobile devices using existing Internet messaging 
standards; guidelines concerning performance implications of link 
constraints, including TCP behaviors during extended periods of 
wireless link downtime; and wired PPP access from mobile Internet 
devices.

Jim Mathis:  Would you be interested in chairing a MMMS working group 
charted as described above?  Would your organization's current apparent 
commitment to closed-protocol mobile messaging solutions make you an 
inappropriate chair for such a group?  If you would not want to, or do 
not feel you would be an appropriate chair, I would volunteer to serve 
as an interim chair.

Cheers,
James

&gt; At the November meeting, about 90 people attended the Mobile 
&gt; Multimedia Messaging Service BOF:
&gt;
&gt;  http://www.ietf.org/proceedings/99nov/46th-99nov-ietf-42.html
&gt; 
&gt; But the mailing list has been dead:
&gt;
&gt;  http://www.imc.org/ietf-mmms/mail-archive/threads.html
&gt;
&gt; MMMS-related topics seem to be fairly prevalent on the general 
&gt; IETF discussion list, with a lot of general agreement that the 
&gt; status quo has some real problems.
&gt; 
&gt; Has anyone started on a working group charter?
&gt; 
&gt; There needs to be an organized effort to provide explicit and 
&gt; well-documented alternatives to the closed-protocol, 
&gt; pseudo-internet mobile access consortia, who are a real barrier 
&gt; to easy and reasonable mobile multimedia messaging services.  
&gt; I for one would certainly contribute a great deal of time and 
&gt; effort to the success of such a working group.
 
Received on Friday, 15 September 2000 17:53:45 GMT
 

 

 
 
This message: [ Message body ] 

 
Next message: Patrik Fältström : "Re: Mobile Multimedia Messaging Service"
 
 
Previous message: comunicado@elcultural.com: "Noticias de actualidad"
 

 
Next in thread: Patrik Fältström : "Re: Mobile Multimedia Messaging Service"
 
 

Reply: Patrik Fältström : "Re: Mobile Multimedia Messaging Service"
 
 
Reply: Leslie Daigle: "Re: Mobile Multimedia Messaging Service"
 
 
Maybe reply: James P. Salsman: "Re: Mobile Multimedia Messaging Service"
 

 
 
 
Mail actions: [ respond to this message ] [ mail a new topic ] 
 
Contemporary messages sorted: [ by date ] [ by thread ] [ by subject ] [ by author ] 
 
Help: [ How to use the archives ] [ Search in the archives ]
 
 

 

 
This archive was generated by hypermail 2.2.0+W3C-0.50
: Thursday, 23 March 2006 20:11:27 GMT
 



