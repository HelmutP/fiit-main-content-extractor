





Pelvic floor exercises: a new solution for impotence?






















































Search:All NetDoctor
---------------------
Diseases
Medicines


































NetDoctor.co.uk






Home






News and features






News






News archive






Newsletter






Features






Encyclopaedia






Diseases






Examinations






Medicines






Interactive






StayQuit






Test yourself






Services






Ask the doctor






Pharmacy






Directory






Health services






Find a hospital






Find a consultant






Find a birth unit






Complementary health practitioners






Support groups






Health centres






ADHD






Allergy and asthma






Children's health






Depression






Diabetes






Erectile dysfunction (Impotence)






Heart, blood and circulation






Men's health






Nutrition






Oral health






Pregnancy
and birth






Private medical insurance






Self-pay treatment






Sex and relationships






Skin and hair






Smoking cessation






Travel medicine






Women's health






All health centres






Discussion






Discussion forums






Health finance






Accident and sickness






Critical illness






Life insurance






Private medical insurance






Travel insurance






Information






About NetDoctor






Commercial opportunities






Disclaimer






Privacy policy






NetDoctor.com














Pelvic floor exercises: a new solution for impotence?













 
Written by Rael Martell
 
 
 



 
	 Pelvic floor exercises have long been considered a useful way for women to restore their muscle tone after giving birth,  but increasing evidence is emerging that men can also benefit.  
 
 
 
	  
	  
	  
	  
  












Pelvic floor exercises may help men with erectile dysfunction.













Recent research suggests that pelvic floor exercises are highly effective in helping men who have problems developing and sustaining an erection.

 
 
A study from the University of West of England, Bristol, suggests that the exercises can help both men with erectile dysfunction (impotence) and premature ejaculation, and also those who experience 'dribbling' after urinating.

 
 
On top of this, experts believe the exercises can also help improve the quality of orgasm – the UK-based Impotence Association says the workout may 'increase awareness of sexual sensations and enhance enjoyment'.

 
 
The big squeeze 
 
The exercises target the pelvic floor, which supports the bladder and the bowel and is made up of layers of muscle that stretch from the tailbone at the back to the pubic bone in front.

 
 
Also known as Kegel exercises, named after the South Californian gynaecologist Dr Arnold Kegel, the regime involves squeezing the hammock of muscles in the abdominal region. 
 
According to the Impotence Association, the exercises strengthen the muscles around the penis, improve the blood supply in the pelvis and thus 'enhance orgasmic sensations by strengthening the pelvic muscles that produce ejaculation by their contraction'.

 
 
Although the exercises have to be learned (possibly over a matter of days), with practice they can be carried out while watching TV or doing the washing up.

 
 
How to find the pelvic floor muscles 
 
To find your pelvic floor muscles, you should sit or lie comfortably with the muscles of your thighs, bottom and stomach relaxed. 
 

 
Muscles around the anus 
 
 You should then tighten the ring of muscle around your anus without squeezing your bottom, according to government advice from the Australian Department of Health and Ageing.

 
 

 
 
Muscles around the urethra 
 
To feel these muscles,  try to stop your flow of urine mid-stream, and then restart it.  
 
You should only do this to find which muscles to use, or to check your progress. Don't practice this more than once a week, or it could affect your ability to pass urine.

 
 

 
Pelvic floor exercises 
 
The Australian Department of Health and Ageing says that once you can feel your pelvic floor muscles working, you can attempt to exercise them.  
 
Tighten and draw in the muscles around the anus and urethra, lifting the muscles up inside. 
 
 Count to five, then release and relax. The Department says you should have a 'definite feeling of letting go'.

 
 
Their guidelines recommend men repeat this up to  maximum of 8 to 10 squeezes, resting for 10 seconds after each tightening of the muscles.  
 
You should follow this by 5 to 10 short,  strong squeezes in quick succession.  
 
Repeat the slow and quick squeezes around four to five times a day.

 
 
An  alternative to drugs? 
 
The benefits of such exercises cannot be endorsed  enough by Dr Grace Dorey, a specialist continence physiotherapist at Taunton’s Somerset Nuffield Hospital and North Devon District Hospital,  and a visiting research fellow at the University of West England, Bristol.

 
 
Dr Dorey undertook a survey of 55 men with an average age of 59 who had experienced erectile dysfunction  for six months or more.

Of the men taking part in the exercises:  
 

 
 
40 per cent regained normal erectile function.  
 

 
 
35.5 per cent improved 
 

 
 
25.5 per cent showed no difference.

 
 

 
 


Perhaps the most dramatic of Dr Dorey’s findings was the improvement resulting from pelvic floor exercises compared to the use of Viagra – the results were the same.

 
 
Raising awareness 
 
Dr Dorey feels passionately about the use of pelvic floor exercises for men suffering erectile dysfunction, arguing that it has been a neglected area for the past 50 years.

 
 
'Historically, women have been advised to perform pelvic floor exercises especially before and after childbirth, hysterectomy and the menopause. This research shows that it is equally important for men to maintain the muscle tone and function of their pelvic floors.'

 
 
And speaking of her own research, she explains: 'The results show clearly that pelvic floor exercises can be highly beneficial for those men with the motivation and determination to perform them.

 
 
'Now I am intending to raise awareness of the availability of an effective alternative to drug therapy for men experiencing erectile dysfunction.'

 
 
If Dr Dorey has her way, pelvic floor exercises will in the future form part of every young male’s sex education. 'I firmly believe that lads from the age of adolescence should be taught the benefits of pelvic floor exercises as a preventative measure.'

 
 
Further information 
 


The Impotence Association:
www.impotence.org.uk

 
 
Men’s Health Forum:
www.menshealthforum.org.uk
 
 




© HMG Worldwide 2003



 
 







 



Last updated 19.08.2003 
 








Advert
















Advert
















Advert



































 


NetDoctor.co.uk:&amp;nbsp
Home


News and features:&amp;nbsp

News | News archive | Newsletter | Features



Encyclopaedia:&amp;nbsp

Diseases | Examinations | Medicines



Interactive:&amp;nbsp

StayQuit | Test yourself



Services:&amp;nbsp

Ask the doctor | Pharmacy



Directory:&amp;nbsp

Health services | Find a hospital | Find a consultant | Find a birth unit | Complementary health practitioners | Support groups



Health centres:&amp;nbsp

ADHD | Allergy and asthma | Children's health | Depression | Diabetes | Erectile dysfunction (Impotence) | Heart, blood and circulation | Men's health | Nutrition | Oral health | Pregnancy
and birth | Private medical insurance | Self-pay treatment | Sex and relationships | Skin and hair | Smoking cessation | Travel medicine | Women's health | All health centres



Discussion:&amp;nbsp
Discussion forums


Health finance:&amp;nbsp

Accident and sickness | Critical illness | Life insurance | Private medical insurance | Travel insurance



Information:&amp;nbsp

About NetDoctor | Commercial opportunities | Disclaimer | Privacy policy | Contact us | NetDoctor.com










 
 The materials in this web site are in no way
intended to replace the professional medical care, advice, diagnosis or treatment of a doctor. The web site does not have answers to all problems. Answers to specific problems may not apply to everyone. If you notice medical symptoms or feel ill, you should consult your doctor - for further information see our Terms and conditions.
		



 © Copyright 1998-2006 NetDoctor.co.uk - All rights reserved NetDoctor.co.uk is a registered trademark 
 





































