




Jump For the Cause 2002








 [NOTE: This site will be updated often, be sure to refresh the pages in your browser as necessary] 

     
       

     





  A NON-PROFIT 501(c)3 ORGANIZATION
  


 RAISING MONEY TO HELP FUND BREAST CANCER RESEARCH THROUGH THE CITY OF HOPE 


 
OCTOBER 14-19, 2002 PERRIS VALLEY SKYDIVING SOUTHERN CALIFORNIA
 


     





  

     
   


  
        ***[PARTICIPANTS INFO]*** 
        [FORMATION INFORMATION] 
        [FAQ] 
        ["BOYZWAYS" SUPPORT TEAM] 
        [JFTC STORE] 
        [JFTC FORUM] 
        [MONTHLY CONTEST] 
        [IN MEMORIAM] 
        [HONOREES PAGE] 
        [JFTC IN THE NEWS] 
        [LETTERS TO JFTC] 
        [TRY OUT EVENT SCHEDULE] 
        [FUND-RAISING IDEAS &amp; TIPS] 
        [DAY CARE FOR TOTS] 
        [ABOUT THE CITY OF HOPE] 
        [BREAST CANCER PATIENT             PROTECTION ACT]  

   Organizers 
        [Kate Cooper] 
        [Tony Domenico] 
        [Mallory Lewis/Tarcher] 
        [Brad Hood] 
        [Richard Seymour]         [Jill Scheidel] 
          (Sponsorship Chairman) 
        [Jason Russell]           (Mens Support Event Director) 
        [Al Altendorfer]           (Judge) 
        [Judy Celaya] 
          (Judge) 
        [Al Gramando] 
          (Judge) 
        [Brad Hood] 
          Chief Cameraman 
        [Norman Kent] 
          Cameraman 
        [Oscar Osczkowski] 
          Cameraman 
        [Craig O'Brien] 
          Cameraman 
        [Craig Stapleton] 
          Camera Assistant 
        [Doug Spesert] 
          Webmaster 

 

   Sponsors 
        [JFTC 2002 Sponsors] 
        [Jump Run Productions] 
        [Square One Parachute Sales] 
        [Square2 Parachute Sales] 
        [Square3 Parachute Sales] 
        [Perris Valley Skydiving]  

   Donations 
        [Participant Success Stories] 
        [Participant Fundraising] 
        [Non-Participant Fundraising] 
        [Drop Zone Fundraising] 
        [Ideas for DZ Fundraising] 
        [Make a Donation]  

  

        [Brief History] 
        [Past Events]  


  

        [Event Photos] 
        [Media/Footage]  


  

        [Contact Info] 
        [Make a Donation] 





 WE DID IT!!!!!! 

     
 PHOTO BY NORMAN KENT 
 131-way Women's World Record 

 October 19, 2002 

 11:22am 

 10.73 seconds 

 
 

 PHOTOS AND VIDEO 

 A FREE event tape will be sent to all participants &amp; judges. 
Please be patient. 
 
Photos are now available to order on the Jump Run Productions site!  

[More new photo information for participants on the Participants' Info Page.]  

Participants get 40% off the listed prices on the Jump Run site for the JFTC 2002 photos!  
[More new photo information for participants on the Participants' Info Page.] 


 
 


 TANDEM SKYDIVES 
 for Breast Cancer Research at City of Hope 
 CLICK HERE FOR MORE DETAILS!!!	
 

 
 


 JFTC mourns the loss of Shannon Embry.	
 


 


 Jason Russell's Daily Commentary From the Drop Zone 
 CLICK HERE TO ACCESS IT!!!	
 

  
    


  
  


TOP 
    
      
 
 A BRIEF HISTORY OF JUMP FOR THE CAUSE 

 Jump For The Cause's first event was in 1997, and raised over $11,000 for a local Battered Women's Shelter.  The event was 1-day only, and involved 48 jumpers who formed a Star of David in freefall.   The second event took place in 1998 and was also a Star of David Jump.  This event raised almost $20,000 to benefit battered Women and Children. 
 

 JUMP RUN PRODUCTIONS and SQUARE ONE PARACHUTE SALES incorporated JUMP FOR THE CAUSE Inc., in 1999. 
  

 The 1999 Women's World Record Skydive was the new corporation's flagship event. This 6-day event raised over $300,000 to fight breast cancer, and set a new World Record (118 Women)! 


 Jump For The Cause's fourth event took place in 2000.  At this one-day event, 35 jumpers joined paraplegic jumper, Coral Degagne, to set a new World Tandem Record. Concurrent with this event, Skydive Dallas held their Texas State Record Attempts as a fundraiser to benefit JUMP FOR THE CAUSE.  The two events raised $55,000 for The USC Department of Neurology to help find a cure for Spinal Cord Research. 


 In October of 2002, Jump For The Cause will present JUMP FOR THE CAUSE, 2002 - A WOMENS WORLD RECORD EVENT.  The event takes place October 14-19, at Perris Valley Skydiving. We plan to build a formation of 150 women, and our goal is to raise a quarter of a millon dollars for the City of Hope. 

 PUBLICITY 

 Jump Run Productions, an Emmy award winning Production Company, specializes in publicizing Skydiving related events.  What follows is a partial list of media garnered since 1997 (many of our broadcasters have done features on more than one of our events): 

 Good Morning America (5 minutes plus!), Extra, Inside Edition, The Travel Channel, local and National News, Real TV, KTLA (35 minutes on their Los Angeles' top rated AM News show) Footage placement on ? dozen specials for airing on a variety of networks, Intersticials on Lifetime Channel and a variety of print media including local and national newspapers, magazines and of course, every Parachute Magazine in the world (really!). 

 Jump For The Cause 1999 received a FAI award for "Top 10 Aerial Events of the Year", as did a Jump Run Productions Event, the 1998 Women's World Canopy Record. 

 We work with one of Los Angeles' top publicists, and as you can see, our events are extremely high profile.  We are looking forward to showcasing our sponsors as we promote our next event. 

 

 TOP 
 
 ABOUT THE FOUNDERS OF JUMP FOR THE CAUSE 

 Jump For The Cause, a 501c3 Corporation, the Skydiving Industry's premiere Fundraising Organization, was founded by Mallory Lewis and Brad Hood of JUMP RUN PRODUCTIONS and Kate Cooper and Tony Domenico of Square One Parachute Sales. 

 Jump Run Productions has been the industry's leading Emmy- Award Winning Skydiving Cinematography group since it was founded in 1995. In addition to having organized a number of highly successful charity events which garnered tremendous media attention, Cinematographer Brad Hood has filmed numerous skydiving sequences for major motion pictures such as Air Force One, Puppet Masters and Drop Zone as well as many national television commercials.  

 
 Hood is currently on the United States Parachute Team and was part of the gold medal-winning United States team at the World Air Games in Spain this past June.  Lewis is an Emmy Award winning television producer and now works with her mother's (Shari Lewis's) famous puppet, LambChop, Jump For The Causes' Official Spokes-sock. 

 Jump Run Productions 
30490 Morning View Drive 
Malibu, CA  90265 
tel  310.457.4633 
fax 310.457.4601 
email jumprun@earthlink.net 
website www.jumprunproductions.com 

 Mallory Lewis &amp; Lamb Chop 
email mallory@lewisandlamb.com 
website http://www.lambchop.tv 

 
 
 
 
 
 
 
 
 
 
 

 TOP 

 Square One, Square2 and Square3 Parachute Sales are one of the industry's success stories, boasting three locations and a thriving mail order business. In addition to being very successful business people, Kate Cooper and Tony Domenico are two of the sport's most experienced record event organizers, and were key organizers on the last three World Teams. Both Kate and Tony are multiple World Record holders, national competitors and medallists, and are much sought after as large formation trainers and key organizers.  They are two of the most highly respected jumpers on a world level, and are in an ideal position to put together a successful World Record Event. 

 The success of Square One left its founders realizing that Pro Shops,
offering both excellent service and selection, were needed in other
areas of the country. 

 Square2 was opened in Eloy in the fall of 1997, at the World Famous
Skydive Arizona dropzone to help the local and visiting jumpers there
have access to the best equipment available.  Square3 was opened in the
summer of 2000 at Crosskeys, New Jersey, to help the busy  Northeast
have it's share of great service and selection as well.  Between Square
One, Square2 and Square3, the skydiving world has several regional
choices for their equipment! 

 Square One 
425 West Rider St. 
Suite B-7 
Perris, CA 92571 
tel 800.877.7191 
Fax 909.657.8179 
e-mail sales@square1.com 
web www.square1.com 

 
 Square2 Parachute Sales 
e-mail square2@primenet.com

 
 Square3 Parachute Sales 
e-mail square3@snip.net
 
 
 
 
 
 
 
 
 
 
 
 
 
 

  TOP 
 
 
  
 Perris Valley Skydiving, the largest Skydiving Center in North America, offers a wide variety of airsports to the general public. Founded in 1965 and purchased by the Conatser family in 1976, Perris Valley Skydiving hosts the largest number of meticulously maintained Turbine Aircraft available year round. The Drop Zone owns three Twin Turbine Super Otters. We also have a Turbine Skyvan. The Drop Zone is run by Melanie and Patrick Conatser, who have been involved in aviation since childhood. 

 For those making their first parachute jump or as an experienced jumper, Perris Valley Skydiving is recognized as the premier skydiving center in the Western Hemisphere. The school has been in business for 25 years. In addition to its school training and Tandem program, Perris Valley offers facilities to accommodate team training and fun jumpers. World class coaching is available, as well as load organizing for the intermediate and experienced skydiver. 

 Perris Valley Skydiving 
P.O. Box 1823 
2091 Goetz Road 
Perris, CA 92570 
tel 800.832.8818   (213/714) SKYDIVE 
manifest 909.657.3904 
fax 909.657.6178 
email manifest@skydiveperris.com 
website www.skydiveperris.com 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  





 
  R&gt;
 
 
 
 
 
 
 
 
 

 

