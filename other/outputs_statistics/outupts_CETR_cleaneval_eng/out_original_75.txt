






Scotsman.com Sport - UEFA Cup - Race row casts a dark shadow over clubs' successes









 


  Please note: Your browser has been unable to
load the stylesheet that accompanies this page.  The page is still readable. [Accessibility statement] 
 [Skip to navigation]  
 
	 
		  
		 
Websites
			Scotsman.com websites
			
			News
			Sport
			Business
			The Scotsman
			Scotland on Sunday
			Edinburgh Evening News
			
			
			Dating
			Jobs
			Motors
			Property
			Business Finder
			
			
			Member Centre
			Web Feeds
			Media Pack
			Site Help
			
			
			Digital Archive 1817-1950
			Photo Gallery
			Reader Holidays
			Scotsman Calendar
			Money
			
			
			Fantasy Golf
			Haggis Hunt
			
			
			Edinburgh Festivals
			Heritage &amp; Culture
			Living
			Weather
			Webcams
			
			
 
		 
Search | Site map
 
		 
Jobs | Property | Motors | Dating | Money
 
	 
	 
		 
			 Log in 
			 Register now - free! 
		 
		 Member Centre 
		   
	 
 
 
 
	 
		
	 
	 Wednesday, 27th December 2006 
	 
		 UEFA Cup 
		 
		
 

Sun 28 Nov 2004
  

 
 
	 Printer friendly 
	 Send to friend 
	
 
 



 Race row casts a dark shadow over clubs' successes 


 
	GERRY BRITTON 
 
 THERE is no denying that the last few days have been exciting times for Scottish football. Rangers’ win in the Old firm derby re-ignited their own challenge for the SPL title, while magnificent results for our European representatives in midweek meant that, for once, we have a realistic chance of watching more than one of our club sides competing in the latter stages of the UEFA Cup.  
 While the back pages of our national press should have filled with morale-boosting tales of these exhilarating triumphs, the spectre of sectarianism and racism has raised its ugly head again, instantly dislacing the feel-good factor that all involved with the Scottish game should be currently experiencing.  
 
 Unfortunately, the sickening examples of intolerable prejudice that have been highlighted in our country recently have been replicated in other European countries. There was the despicable abuse meted out to England’s black players in Spain, and down south a Blackburn fan was in court over his racist abuse of Birmingham striker Dwight Yorke. The fact that Yorke had previously been an outstanding goalscorer for the Ewood Park side, and most probably a revered favourite of his abuser, makes the incident all the more ridiculous and incomprehensible.  
 Scotland in the 21st century is an ever-increasingly multi- cultural assemblage of people who profess to varying creeds and possess differing tones of skin pigmentation. Scottish football mimics this growing diversity with a growing number of players and fans with ethnic origins and religious beliefs which are different from the white men who have traditionally made up the British footballing community.  
 Yet no matter where they come from or what colour they are, every football fan can still identify a sub-standard player from the terraces, and, as a cash-splashing stakeholder in our beautiful game, is well within their rights to loudly vent their opinions on a player’s inadequate display. But it is when a sectarian or racist expletive is added to their harangue that the line has been crossed and acceptable criticism from the stand degenerates into a vile personal onslaught.  
 A thick skin is a necessary prerequisite for all wannabe professional footballers - we all know that there will be regular assassination of our on-field qualities from both supporters and coaching staff. In fact, many players channel the criticism, and find that the passionate pelters help them achieve a greater level of performance than they could ever have otherwise managed.  
 Enjoying an after-match refreshment with my former Dundee team-mate Noel Blake, I made a mental note to maximise the effort in my performance in the upcoming Dundee derby when an idiotic United fan relentlessly chided myself and Noel over our supposed shortcomings. Noel regarded the situation with his customary grin until the brainless fan then drew attention to the fact that Noel’s complexion was darker than the ruddy exterior of his harasser. Idiotic to bring race into an argument and idiotic to do so with a man who could easily have been mistaken for Marvin Hagler’s twin brother.  
 The incident which saw Dundee ban one of its season ticket holders from Dens for life for verbally abusing two black Celtic players showed that there are means to curtail the despicable actions of a mindless minority. The hard line taken by Dundee in that instance served as an example to any other fans who feel that it is acceptable to have a go at an opposition player on account of the colour of his skin. And the fact that it was an appalled 14-year-old Dundee fan who brought the moronic outburst to the attention of the authorities proves that there are ways that decent supporters can play their part.  
 At Partick, we have three black players on our books: Jean-Yves Anis and Armand One from the Ivory Coast and the magnificently-named Emmanuel Panther, brought up in not so exotic Maryhill and boasting an accent that would not be out of place on River City. In conversation with them on the subject recently, they assured me that there have been few occasions in their careers when they have been subjected to racial abuse. Unfortunately my ex-Livingston team-mate Brian McPhee has not been so lucky. He has the regrettable distinction of seeing fans of visiting sides end up in court two weeks in succession for spouting race-based vitriol in his direction.  
 During my tenure as a Dunfermline player, the reputation of the Pars was tarnished when our black central defender Dave Barnett asked to be released from his contract on the grounds that he could no longer work within our allegedly racist environment. The front pages of the tabloid press were covered with Barnett’s racist claims, much to the bemusement of the staff at East End Park, and particularly our general manager Paul D’Mello, who also happens to be black.  
 It was to everyone’s surprise, then, that when pre-season training commenced the following campaign, Dave was seated back in our apparently racist dressing-room having been granted permission by the Pars bosses to engage in our fitness conditioning.  
 Race was never a problem during my time with the Pars, as I am sure even Dave would now acknowledge. As I recall, the only burning crosses I encountered at East End then were the ones that skelped off my forehead having been fired from the scintillating left foot of our rotund wing wizard Stewart Petrie.  
 Web links 
 

 
UEFA http://www.uefa.com/
 
 
Hearts FC http://www.heartsfc.co.uk
 
 
Rangers FC http://www.rangers.co.uk
 
 

 Related topics 
 

 
UEFA Cup http://sport.scotsman.com/topics.cfm?tid=668
 
 
Rangers FC http://sport.scotsman.com/topics.cfm?tid=63
 
 

 This article: http://sport.scotsman.com/topics.cfm?tid=668&amp;id=1367012004 

 Last updated: 28-Nov-04 01:13 GMT 

	 
	   UEFA Cup: page 23 
 
	 RSS 
	 Email Alerts 
 
 
 Transformed Alkmaar threatening to spoil Ricksen's homecoming party (28-Nov-04) 
 Race row casts a dark shadow over clubs' successes (28-Nov-04)
 
 Calculated gamble paid off in Basel (27-Nov-04) 
 Co-efficient rise could see six sides in Europe (27-Nov-04) 
 O'Neill blast for Rijkaard over 'anti-football' jibe (27-Nov-04) 
 Swiss bliss for John as Hearts soar (26-Nov-04) 
 Dramatic strike seals glory night for Hearts (26-Nov-04) 
 Neilson snatches a famous victory (26-Nov-04) 
 Jambo feat could boost Scots game (26-Nov-04) 
 McLeish keen to stay top (26-Nov-04) 
 Rangers up the gears to hit top spot (26-Nov-04) 
 I missed my glory goal (26-Nov-04) 
 Victory has McLeish targeting top finish (26-Nov-04) 
 Robinson fears UEFA rap for fans' pitch charge (26-Nov-04) 
 A team full of heroes (26-Nov-04) 
 Schalke stay on course (26-Nov-04) 
 

 
	
	Page 23 of 47
	
   Web links 
 
 UEFA 
 Hearts FC 
 Rangers FC 
  
 
	 HUNTING SEASON 
	
	 It's open season on haggis once more as the annual hunt gets under way 
	haggishunt.com
	 
 
 
	 SCORE FOR YOUR SCHOOL 
	
	 Register now and you could win CIS kit, SFA training and equipment for your school 
	&gt;&gt;
	 
 
 
	 SUPERSCOTCASINO 
	
	 Play SuperScotCasino - win, win, win! All prizes paid in cash 
	 
 
 
	 FOLLOW HEARTS? 
	
	 Sign up for FREE Hearts alerts and get all the latest news direct to your mobile 
	Find out more &gt;&gt;
	 
 
 
	 FOOTBALL BRIEFING 
	
	 A weekly dose of gossip and rumours from the football  grounds of Scotland 
	Sign up now &gt;&gt;
	 
 
 
	 FOOTBALL 
	
	  
	Tables - Results - Reports
	 
 
 
	 FOLLOW HIBS? 
	
	 Sign up for FREE Hibs alerts and get all the latest news direct to your mobile 
	Find out more &gt;&gt;
	 
   
	 
	
 
 Navigation menu 
 Sections 
 

 Top Stories  
 Latest News  
 Football  
 Rugby  
 Golf  
 Racing  
 Motorsport  
 Cricket  
 Tennis  
 Athletics  
 Boxing  
 Snooker  
 Other Sports   Article Index 
 

 Topics 
 
 State of Scottish Golf   UEFA Cup   Andrew Murray   Champions' League   Heineken Cup   Topics A to Z 
 



 Other Sites 
 
 News 
 Business 
 The Scotsman 
 Scotland on Sunday 
 Evening News 
 Heritage &amp; Culture 
 Living 
 Money 
 
 

	 
	 ©2006 Scotsman.com | contact | terms &amp; conditions
 
 
   
 

 


