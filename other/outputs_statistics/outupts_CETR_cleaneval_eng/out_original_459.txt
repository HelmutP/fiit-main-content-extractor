



 
 
  
 
 


 
 
 Recent Photos 
 
 

My Flickr Photo Sets | 
MyPhotostream | 
My Favorites | 
MyProfile
 
 
 May 28, 2004 	
 
	  
 Purveyor of Cool? - Comments (6)
 
	
	 
			

rox! 
	sux!

	
 

	
NewsGator
	NewsGroups

	
Wakes
	Funerals

	
Linkage
	Plagerism

	
Amy Lee
	Jessica Simpson

	
Farscape
	Star Trek

	
XML
	XXX

	
Vancouver
	LA

	
Flea markets
	eBay

	
Alcohol
	Soft drinks

	
Voting
	Voting for Bush


	 
	
	 
                        	 Posted by  @ 10:34 am  Edit This 	 
	
	

 




 May 27, 2004 	
 
	  
 The Lord of the Rings Drinking Game - Comments (4)
 
	
	 
			 Well, I finally saw return of the king last night. I thought it  was (predictably) great, and I (predictably) kicked myself for having not seeing it on the big screen. I’ll probably watch it again tonight so I can see all the parts I missed while Melissa was chatting, cheering, crying etc. 
	  
	 Tonight when I watch it though I’m going to drink… and from the looks of things heavily. Tim and I made up a few rules to a Lord of the Rings drinking game today during lunch time. Let me know the results if you end up trying it out yourself. Use the comments feature on this site to suggest/add your own rules too. 
	 
 Every time someone falls off a cliff… take a drink 
	 Every time Sam cries… take a drink 
	 Every time a close-up of Frodo’s eyes takes up more than 50% of the screen… take a drink 
	 Every time you mistake the (male) elf briefly for a hot blonde chick… take a drink (NOTE: your ability to distinguish between the two is sure to diminish the longer you play this game, making this particular rule a very slippery slope) 
 
	 Trying watching all three The Lord of the Rings movies… The Fellowship of the Ring, The Two Towers, and The Return of the King… sequentially while playing this game and you have a whole weekend full of entertainment right there!
 
	 
	
	 
                        	 Posted by  @ 4:03 pm  Edit This 	 
	
	

 




	
 
	  
 Dress up famous nudes! - Comments (14)
 
	
	 
			 Talk about some mad Photoshop skills! This site is running a very cool image editing contest… dress up famous nudes. I’ve posted my ‘winner’ below but definitely worth a click-through. We’re not talking run of the mill famous nudes here like Pamela Anderson or other Playboy bunnies… we’re talking classical famous nudes like David, Venus, etc.. These uber-talented Photoshop users are prolly the same guys who make the women on the covers of magazines look so amazingly beautiful… and fake. 
	   
 
	 
	
	 
                        	 Posted by kk @ 11:39 am 	 
	
	

 




 May 26, 2004 	
 
	  
 The Business of Design - Comments (0)
 
	
	 
			 From time to time inevitably everyone has to justify their existence within a corporation. “What do you do exactly?” “What value do you provide the organization?” “What impact would it have on ‘us’ if ‘we’ didn’t have ‘you’?” 
	  
	 When it comes to design, measuring quantifiable value can be difficult and we are often forced to use more qualitative metrics to measure the value that good aesthetics and proper design brings to a company. 
	 In the past couple days I’ve found a couple good resources that attempt to illustrate in quantifiable terms the impact of design on an enterprise. The net net of both is a conclusion that most of us who work in the applied arts field already know… companies that are design-driven outperform those that are not. Now bring on the charts and figures! 
	 Impact of Design on Stock Market Performance - The Design Council The design council has produced an excellent research report documenting the valuation of companies considered to be ‘design-driven’ vis-a-vis those that are not over a period of 10 years. A great test in terms a CFO can understand. Over time what is the tangible effect of design on a company and it’s shareholders  The results? Design driven companies outperform the market by 200%! 
	 
 “Companies recognized as effective users of design strongly outperform their peers on the stock market - that is the key finding of a ground-breaking study which has tracked a total of 166 design-led companies over ten years of UK share price activity.  
	 A group of 63 companies, grouped into a ‘Design Portfolio’ for their consistent showing in various design award schemes, outperformed both the FTSE All Share and FTSE 100 indices by around 200% between 1994 and 2003, the study reveals.” 
 
	 The Power of Design - Business Week A great feature on design house IDEO and some case studies on how they have made their clients money through good design and design process.
 
	 
	
	 
                        	 Posted by  @ 11:10 am  Edit This 	 
	
	

 




 May 20, 2004 	
 
	  
 Spring Bling! - Comments (1)
 
	
	 
			  
	 My buddies have a little non-profit fundraising group called Debauchery for Donations and they’re pulling together a great benefit party this weekend call Spring Bling to benefit Big Bam; an organization that provides medical services to women who experience age, financial or cultural barriers that prevent detection and treatment of breast cancer. Team Debauchery (Aaron Crossley, Bixby Jamison, Dana Pauls, David Dolby, George Hume, Jessica Sterling, Jim Black, and Lisa Ludwig)  is known for having some of best events in the City and you should come check it out. 
	 The party will take place on Saturday, May 22nd and will feature live music from one of the Bay Area’s hottest tribute bands - STUNG!, the ultimate tribute to the POLICE. Followed by hip hop beats spun on two turntables and a microphone. 
	 I’ll be there… and if you’re lucky I’ll be wearing this! 
	 
 
	 
	
	 
                        	 Posted by  @ 12:40 pm  Edit This 	 
	
	

 




 May 13, 2004 	
 
	  
 Losing the War? - Comments (1)
 
	
	 
			 This is not a politcal blog or activism website. I don’t have an agenda, but I do have a problem with the way we are running our international affairs. Specifically, I think we are loosing the war in Iraq. 
	  
	 America hasn’t made friends of the Iraqi people. In the poll, 80 percent of the Iraqis questioned reported a lack of confidence in the Coalition Provisional Authority, and 82 percent said they disapprove of the U.S. and allied militaries in Iraq. (Salt Lake Tribune, May 13) 
	 Bremer, and the Iraqi Governing Council have failed and their jobs will be turned over to the UN if it’s a mission they choose to accept. “We sit in the council while the country is burning and argue over procedure,'’ says Sheikh Yawar, a Sunni tribal leader who lived abroad until last year. “We’re like the Byzantines in Constantinople, debating whether angels are male or female with the barbarians at the gate.” Under Mr. Brahimi’s plan for a transitional government, all 25 members of the US-appointed council would be culled in favor of a team of technocrats to be chosen next month by Brahimi’s team and influential Iraqis, with US input. The group would take power in July and shepherd Iraq to elections next January - in which, ideally, they would not participate. (Christian Science Monitor, May 13) 
	 The coalition is a sham and is falling apart. Spainish Troops Leaving (Yahoo!, April 19) Poland Pulling Out (Irish Times, April 20) Honduras Pulls Troops (CNN, April 19) Nicaragua Done (Reuters, April 20) Ukraine Pulls Back (MSNBC, April 21) Dominican Republic Removes Troops (CBS, April 20) 
	 The war is expensive, bloody, and indefinite. The official, Deputy Defense Secretary Paul D. Wolfowitz, acknowledged that a surge of violence and other problems in Iraq had led to rising costs, with no certainty yet when they would fall. (NY Times, May 13) 
	 The world is less safe than it was before we started, and there are more terrorists now than ever. The AP-Ipsos poll, released Wednesday at The Associated Press annual meeting, found: Half feel that, in some measure, the terrorists might be winning the war on terrorism. One in five in the poll feels strongly the terrorists are winning while an additional 30 percent say there is at least “a little truth” to that statement. More than one-third say they have less faith in government’s ability to protect them, and an additional one-fourth say there’s at least some truth to that idea. Nearly half feel strongly they are more pessimistic about the possibility of there ever being peace in the world while an additional one-fourth say there may be some truth to that. (MSNBC, April 22) 
	  
	 More than 50% of America’s troops are deployed, reservists tours are extended again, and the military is stretched thin. The US has committed most of its heavy ground forces and deployable aircraft carriers, as well as a large share of its best attack aircraft and air- and sea-lift units. (Washington Post, April   The newly announced troop extensions — which broke the Pentagon’s promise that Iraq tours of duty would be limited to one year — are forcing many to change plans, cancel weddings and delay return to normal life. Meanwhile, the anxiety felt by those at home increases each day. (ABS News, April 23) 
	 America has failed to uncover the weapons that led us into this war. During the annual Radio and Television Correspondents Dinner this week, Bush presented a slide show of quirky photographs from inside the White House. In one, the president is looking under furniture in the Oval Office. “Those weapons of mass destruction have got to be somewhere,” Bush joked. “Nope, no weapons over there … maybe under here?” (CNN, May 6th) 
	 
 
	 
	
	 
                        	 Posted by  @ 10:00 am  Edit This 	 
	
	

 




 May 11, 2004 	
 
	  
 Gmail, Plaxo, and LinkedIn - Comments (2)
 
	
	 
			 Google launched a cool new service called Gmail. I’ve landed a beta account and it seems pretty hot. You get 1000megs of space vs. MSN Hotmails 2megs making it an actually useful mailbox. It’s pretty interesting how it turns back and forth barrage of short emails into threads or ‘conversations’. It also takes advantage of Google search technology to keep my email life organized. I’ve set up my kk@burnkit.com address to forward there. Gmail would be really cool though if you could POP several accounts like you can over at Hotmail where I had been popping my old TWU mail, my Burnkit mail, and my groovy.com mail. Anyway, I’m hooked. Checked it out when it becomes generally available. 
	 I’ve also been expiramenting with this other online networking tool called LinkedIn. I got turned onto it from a good friend, Ean Jackson, in Vancouver who always seems to be on the cutting edge of this type of thing… he got me up and running on Plaxo too. It kinda seems like Friendster but for business contacts. You upload information about yourself and your professional experience and jobs you’ve held. People who have worked with you in the past can write testimonials or references. It’s kinda fun to mess with and a good way to ‘practice’ marketing oneself, but I’m still skeptical that I’ll get any good job leads or new contacts from it… we’ll see. Lemme know if any of you guys sign up and I’ll write you up a little reference and beg you for one for me as well.   Here’s my work-in-progress page.
 
	 
	
	 
                        	 Posted by  @ 9:09 am  Edit This 	 
	
	

 




 
Change 
Culture 
Creativity 
Community 
Interweb 
Design 
Politics 
Friends 
Revolution 
Photography 
God 
Vancouver 
QRS 
RaySat 
Blogging 
Family 
Music 
Photos 
Flickr 
Marketing 
Tips/Tools 
Science 
Technology 
Social Justice 
Sustainability 
Branding 
Communication 
Shout-Outs 
Search 
Media 
Deep Cove 
Iraq 
Richmond 
Interviews 
Downloads 
Mac 
Web Design 
Event Blogging 
Technique 
Tidbits 
Battlestar Galactica 
Bryght 
Video 
BitTorrent 
BeerCasting 
Web 2.0 
Broken 
 

 


 

   I'm a photographer, technologist and author based in Vancouver. This is my blog
    it's the hub for my digital life. Enjoy and pleasesubscribe to my RSS feed.
      

   

 
 
About This Site
 
 
Contact Me
 
 
Hire Me
 
 
Social Software
 
 
What is RSS?
 
 
WTF is Web 2.0?
 
 

   

    Recent Posts: 
  
	-  JPG Photography Magazine - Issue 8 Voting  
	-  Vancouver Christmas Shopping - Discollection Gift Cards  
	-  Flickr Faves: Self-Portraits by Laura Kicey  
	-  “Rebagliati Positive about 2010″ in Heads, Magazine  
	-  Celebrating Canadian Social Media Innovation  
	-  A Flurry of Vancouver Web 2.0 Startups  
	-  Who can resist a moose with an iPod…  
	-  Important Northern Voice Updates - Registration Open and Speaker Submissions Closing  
	-  threads of gastown fashion show  
	-  got hats? | ChangeEverything.ca  
  


   

    Everyday I Read: 
  
 
  


 
Other kk+ Projects 

 
 
Static Photography - Vancouver fashion and portrait photography  
  Vancouver 2010 Coverage - News and views on the upcoming 2010 Winter Games in Vancouver.  
  Bryght - Builders of online communities  
  Battlestar Aggregatica - Top secret fanboy SciFi site dedicated to my fav show, the new Battlestar Galactica.  

 

   

    kk+ Elsewhere 

   
    Flickr 
    Zaadz 
    YouTube 
    MySpace 
    Upcoming 
    43Things 
    43People 
    43Places 
    Del.icio.us 
    Photoblogger 
  Tribe.net Profile 
  LinkedIn 
  Yahoo!360 
  Last.FM 
  NowPublic 
  Lightstalkers 
   
      
    RSS     
     
    Comments RSS     
  
 

   
  Upcoming Events 

  
   

 
 
 



 
 
 More of kk's events 
 
 


 
 *spark-online 

 For 3 years I was the editor-in-chief of a great online magazine called *spark. It was a culture and technology monthly with a philosophical bent and great design and photography. Here are the archived issues. 

 
    34 /
        33 /
        32 /
    31 /
    30 /
    29 /
    28 /
    27 /
    26 /
    25 /
    24 /
    23 /
    22 /
    21 /
    20 /
    19 /
    18 /
    17 /
    16 /
    15 /
    14 /
    13 /
    12 /
    11 /
    10 /
    9 /
    8 /
    7 /
    6 /
    5 /
    4 /
    3 /
    2 /
    1
 



   

    Older Posts:   
   	 December 2006 
	 November 2006 
	 October 2006 
	 September 2006 
	 August 2006 
	 July 2006 
	 June 2006 
	 May 2006 
	 April 2006 
	 March 2006 
	 February 2006 
	 January 2006 
	 December 2005 
	 November 2005 
	 October 2005 
	 September 2005 
	 August 2005 
	 July 2005 
	 June 2005 
	 May 2005 
	 April 2005 
	 March 2005 
	 February 2005 
	 January 2005 
	 December 2004 
	 November 2004 
	 October 2004 
	 September 2004 
	 August 2004 
	 July 2004 
	 June 2004 
	 May 2004 
	 April 2004 
	 March 2004 
	 January 2004 
	 November 2003 
	 October 2003 
 

  

     
    
         

 

   


 

 



   
 
    Search This Site:   
   
  
   
     
    
   
  
  

   

    Interviews With... 

  

  

  

  

  


   

    Links &amp; Memes 

 ⇒ 
Thomas Hawk's Digital Connection: Photowalking with Flickr's Heather Champ Robert Scoble and I spent some time hanging out with Heather Champ for Episode 4 of Scoble Show. Heather talked about her photographic style, the reincarnation of JPG Magazine, Flickr, how she shoots her Polaroids and more as we spent the morning in one o 
 ⇒ 
Anyone Up for a DSLR Summit in San Francisco? What about organizing an actual DSLR conference in San Francisco? They have Web 2.0 and CES and Gnomedex and SXSW and all these other tech sort of conferences, why not organize one around digital photography? As far as I know there is not a conference bas 
 ⇒ 
Flickr Upload Limits Increased And it's even better to give the gift of Flickr since now your recipients will get unlimited uploads — the two gigabyte monthly limit is no more (yep, pro users have no limits on how many photos they can upload)! At the same time, we've upped the limit 
 ⇒ 
You Witness News Were You There When News Happened? Upload your photos and video here to have them considered for use in articles and features on Yahoo! News. 
 ⇒ 
FLICKR PICKR / Morning do Narcissism is a recurrent theme on Flickr. There is vast new potential to reveal yourself to the world. 
 ⇒ 
Continuous Partial Attention (if you're lucky) I just read an article at the Wall Street Journal that made me feel pretty depressed. It's called "Blackberry Orphans" and describes a generation of parents who can't/won't/don't give their full attention to their children and partners, so addicted are th 
 ⇒ 
Lee and Sachi’s Rockin’ Panorama Photos As I’ve mentioned on many occasions, Lee and Sachi travelled around the world and blogged the heck out of it. Lee recently posted a schwack of his favourite panorama photos. Here are a couple: 
 ⇒ 
Community Wireless in Vancouver - FON, Meraki, Something Else? | So, in recent discussions with folks like Darren Barefoot as well as David Vogt of Mobile Muse (and, of course, kicking the idea around the Bryght offices), it's become clear that community wireless in Vancouver is something that really should happen. 
 ⇒ 
MyBlogLog is Distributed Social Networking MyBlogLog is the closest to distributed social networking I've seen. 
 ⇒ 
Using open source software to design, develop, and deploy a collaborative Web site, Part 12: Hosting and deploying When you reach the final phase of development of your Drupal powered site, it's time to start thinking about how you are going to deploy it for your client. 
 ⇒ 
NowPublic flirts with ABC News Last week, NowPublic redesigned once more. They just keep getting better! If you don't know NowPublic, it is a Drupal website that is a combination of Slashdot, Digg and Flickr (albeit not as popular yet). Every user can help report the news, provide valu 
 ⇒ 
Happy Birthday from Mickipedia I'm fucking 30! 
 ⇒ 
How to become a fashion photographer A career in fashion photography doesn't have to be an impossible dream. Three industry insiders tell Ben Widdicombe their tips for getting started. 
 ⇒ 
Organic Denim for the Sustainable Fashionista Del Forte Denim is Tierra’s line of luxury denim made from 100% organic cotton entirely in the US. The jeans are extremely well-made and expertly fitted. Tierra created her line for people like her and her friends, “real women who care about style but 
 ⇒ 
How We Did It: Stewart Butterfield and Caterina Fake, Co-founders, Flickr, Buying a Business or Franchise Article The original plan had been to create an online game. But they were just about out of money. And then Butterfield had this crazy vision of building a photo-sharing website, and before you knew it Flickr was a cultural phenomenon. Ya-hoooo! 
 ⇒ 
Web Directions North What makes a better combination than web geeks, snow and Canadian beer... that's exactly what's going on at Web Directions North. 
 ⇒ 
Vancouver Fashion Finds in Unexpected Places Real Vancouverites know that shopping in Vancouver doesn't mean "go to Robson and wander." Part of the joy of living and shopping in Vancouver is finding the great deals in unexpected places. 
 ⇒ 
Photography Christmas Gift Ideas - Fotoclips Fotoclips are tiny clear plastic fasteners which serve one simple purpose in life: to help you build things with your photos. Each box comes with 110 clips: 100 "2D" clips that hold two photos together, and 10 "3D" clips that allow you to attach photos at 
 ⇒ 
The 7 Deadly Myths of Internet Copyright in Regard to Photography The following is a summary of important information regarding the use and misuse of photos on the Internet. It is not specific legal advice. Copyright is a specialized field of law, and there are sometimes exceptions to the rules. If you have a specific c 
 ⇒ 
Welcome to Our Three New Northern Voice Organizers We're pleased to announce that we've added three new organizers to our NV organizing committee, filling it out to ten people. Welcome aboard to Julie Szabo, James Sherrett and Travis Smith. 











 




 





   
 
  Powered by WordPress theme by root ported by neuro 

 
 
 



