
 Pekný úspech reprezentantov SSNKF na Európskom šampionáte.

Pekný úspech zaznamenali reprezentanti SSNKF na tohoročných majstrovstvách Európy v naturálnej kulturistike a fitness ktoré sa konali 15-16 júna 2012 v prímorskom stredisku Sitges neďaleko Barcelony pod hlavičkou svetovej federácie UIBBN. Šesť členný reprezentačný team SSNKF v zložení Ivana Vozárová, Juraj Kosorinský, Igor Vadkerti, Denisa Ősziová, Gabriel Práczel, Nataša Csölleiová si počínal veľmi dobre a na Slovensko doniesol štyri medaily.
Deň pred semifinálovým večerom ME sa uskutočnilo zasadanie prezidenta Medzinárodnej federácie (UIBBN) Jeana Vaga so zástupcami národných federácií, na ktorom sa hovorilo o jesenných majstrovstvách sveta v naturálnej kulturistike a fitness ktoré sa uskutočnia 17 novembra na Slovensku v Martine, prezident národnej federácie SSNKF Peter Búvala oboznámil členov UIBBN s prípravami MS, boli predložené propagačné materiály a návrh na schválenie nových kategórii ako v mužských tak aj ženských kategóriách.


Umiestnenia reprezentantov SSNKF:

Ivana Vozárová 1.miesto - junior bodyfitness
Juraj Kosorinský 2.miesto - nad 85kg
Igor Vadkerti 2.miesto - do 85kg
Denisa Ősziová 3.miesto - body form
Gabriel Práczel 5.miesto masters 3
Nataša Csölleiová 5.miesto - woman - 52 kg
Roman Vadkerti + Ivana Vozárová 4.miesto - páry
K štyrom medailám pridali reprezentanti SSNKF aj skvele tretie miesto v Pohári národov.















Všetkým reprezentantom ďakujeme za vzornú reprezentáciu SSNKF a Slovenska.
V Banskej Bystrici:18.06.2012

Peter Búvala
Prezident SSNKF 
