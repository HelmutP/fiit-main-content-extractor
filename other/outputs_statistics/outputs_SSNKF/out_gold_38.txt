

Pozvánka na mimoriadnu konferenciu SSNKF.
    
Pozývame Vás na výročnú konferenciu SSNKF, ktorá sa uskutoční v kaviarni Forum Caffe na námestí  SNP 27, v Banskej Bystrici  dňa 09.02.2013 o 10,00 hod.

Program : 1. Otvorenie konferencie, schválenie programu 
                  2. Schválenie mandátovej a návrhovej komisie
                  3. Hodnotenie činnosti za rok 2012
                  4. Správa o hospodárení za rok 2012 
                  5. Plán súťaží a reprezentácie na rok 2013
                  6. Rôzne – diskusia
                  7. Správa mandátovej a návrhovej komisie
                  8. Záver

Upozorňujeme kluby aj jednotlivcov, ktorí ešte nemajú zaplatený ročný príspevok aby tak učinili na číslo účtu - 4007961320/7500



      Peter Búvala
      Prezident SSNKF

