



Výkonná rada pozýva členov SSNKF na letné sústredenie do Veľkého Mederu, so zameraním na skvalitnenie rozhodovania na naších súťažiach. Prebehne zasadanie rozhodcovskej komisie, kde sa budú riešiť zmeny v súťažnej kategorii model. Týmto pozývame všetkých súčasných rozhodcov na toto letné sústredenie osobitne a súčasne ponúkame prvé školenie pre nových záujemcov o rozhodovanie s radou našej členskej základne. Zúčastniť sa tohoto sústredenia môže každý člen nášho zväzu, vrátane rodinných príslušníkov a priateľov aj bez záujmu o rozhodcovskú činnosť. Bude určite vytvorený priestor pre každého účasníka, aby sa mohol stretnúť s členmi výkonnej rady a dozvedieť sa novinky a predstavy o budúcnosti SSNKF priamo v osobnom rozhovore. Záujemcov prosíme o zaslanie záväzných prihlášok na emailovú adresu: ssnkfj@gmail.com

 

PROGRAM SÚSTREDENIA

 príchod a ubytovanie

naplánované 13. septembra 2013 od 15:00 do 19:00

Umiestnenie : Kúria Plauter Veľký Meder

 

večera

naplánované 13. septembra 2013 od 19:00 do 20:00

Umiestnenie : Kúria Plauter Veľký Meder

 

privítanie účastníkov a oboznámenie ich s programom sústrdenia

naplánované 13. septembra 2013 od 20:00 do 20:45

Umiestnenie : Kúria Plauter Veľký Meder

 

dobrovoľná rozcvička - názorné vysvetlenie rozdielu medzi srečingom a Jogou

naplánované 14. septembra 2013 od 7:00 do 7:45

Umiestnenie : Kúria Plauter Veľký Meder

 

raňajky

naplánované 14. septembra 2013 od 8:00 do 9:00

Umiestnenie : Kúria Plauter Veľký Meder

 

prednáška s dislusiou - možnosti jogy pre zefektívnenie výkonu a zníženie rizika úrazu s dôrazom na upevnenie trvalého zdravia, pochopenie funkcie energetických dráh v tele pred, počas a po tréningu 

naplánované 14. septembra 2013 od 9:15 do 10:45

Umiestnenie : Kúria Plauter Veľký Meder

 

otvorené zasadnutie rozhodcov zamerané na synchronizáciu hodnotenia upravenej ženskej kategorie model

naplánované 14. septembra 2013 od 11:00 do 11:45

Umiestnenie : Kúria Plauter Veľký Meder

 

zasadnutie výkonnej rady SSNKF

naplánované 14. septembra 2013 od 12:00 do 12:45

Umiestnenie : Kúria Plauter Veľký Meder 

 

návšteva miestneho fitness centra

naplánované 14. septembra 2013 od 12:00 do 12:45

Umiestnenie : Veľký Meder

 

obed

naplánované 14. septembra 2013 od 13:00 do 14:00

Umiestnenie : Kúria Plauter Veľký Meder

 

odvoz na kúpalisko

naplánované 14. septembra 2013 od 14:15 do 18:00

Umiestnenie : Kúria Plauter Veľký Meder

 

návrat na hotel

naplánované 14. septembra 2013 od 18:00 do 18:15

Umiestnenie : kúpalisko V. Meder

 

večera v prírode - pečené prasiatko

naplánované 14. septembra 2013 od 18:30 do 23:00

Umiestnenie : Kúria Plauter Veľký Meder

 

raňajky a krátke zhodnotenie sústredenia

naplánované 15. septembra 2013 od 8:00 do 9:00

Umiestnenie : Kúria Plauter Veľký Meder

 

odhlásenie sa z ubytovania na recepcii  hotela Plauter a návrat domov

naplánované 15. septembra 2013 od 9:15 do 10:45

Umiestnenie : recepcia hotela Plauter Veľký Meder

 

CENA UBYTOVANIA VRÁTANE STRAVOVANIA

Piatok           13.9.2013  je cena 10,-    Eur

Sobota         14.9.2013  je cena  20,-   Eur

Nedeľa         15.9.2013  je cena  10,-   Eur[gallery ids="4614,4615,4616,4617,4618"]
