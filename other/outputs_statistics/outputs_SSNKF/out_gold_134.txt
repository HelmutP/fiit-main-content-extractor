





      Srdečne Vás pozývame na víkendové športové sústredenie členov a priaznivcov naturálnej kulturistiky a fitness do Sklených Teplíc, ktoré sa bude konať v dňoch 12. - 14. 08. 2011.
 
      
      Víkend bude nabitý programom pre spoločné aktivity športového, ale aj relaxačného a vzdelávacieho charakteru, a ponúka možnosť oddýchnuť si spolu aj s našimi rodinami. 
      Podrobnejšie informácie a potvrdenie účasti je možné u organizátora akcie, Jakuba Strigáča na mail. adrese: jakubstrigac@gmail.com. 
   
      Účasť je potrebné potvrdiť najneskôr do 01.08. 2011!
 
  
   
 
Základné informácie o akcii:
Miesto pobytu: Sklené Teplice
 
 
Trvanie pobytu: 12.- 14. 08. 2011 /od 12,00 hod. - do 12,00 hod./
 
     2 noci, 3 dni - začiatok v piatok /večerou/, koniec v nedeľu /obedom/
Ubytovanie: kúpeľný dom Park – bunkové izby, vybavené sociálnym zariadením, TV-sat, chladnička
Cena: 30,60 € / osoba / noc
Cena zahŕňa:
- kúpeľný dom Park
- plnú penziu /švédske stoly, kúpeľný dom Lenka/
- 4 procedúry / víkend (bez lekárskej konzultácie) /Jaskynný kúpeľ, kúpeľ Márie Terézie, čiastočná masáž, vodoliečba
- bonus 1 x trojhodinový vstup do wellness saunového sveta
- grátis denne neobmedzený vstup do termálneho bazéna
- grátis vstup do fitness
- grátis veľká konferenčná miestnosť /kúpeľný dom Relax/ s komplet vybavením /projektor, ozvučenie, flipchart, sedenie podľa požiadavky/ 
 

 




Tešíme sa na spoločne strávený víkend, ktorý sa bude niesť v duchu naturálnej kulturistiky a zdravého životného štýlu!

