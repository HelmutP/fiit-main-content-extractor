

 Vážení členovia SSNKF,

predstavujeme vám zoznam najúspešnejších členov nášho národného športového zväzu v jednotlivých kategóriách, za rok 2012. Slávnostné vyhlásenie najúspešnejších športovcov SSNKF za rok 2012 sa uskutoční na MEDZINÁRODNÝCH MAJSTROVSTVÁCH SLOVENSKEJ REPUBLIKY V NATURÁLNEJ KULTURISTIKE A FITNESS v Senici,  18. mája 2013. 

Touto cestou srdečne pozývame všetkých športovcov k tomuto slávnostnému aktu!


 



DORAST


1.
Krajnik Kristián
12 b.



JUNIOR


1.
Nociar Matej
43 b.



MUŽI do 174 cm


1.
Petro Martin
42 b.



MUŽI do 180 cm


1.
Velčický Tomáš
42 b.



MUŽI nad 180 cm


1.
Kosorínsky Juraj
64 b.



MASTERS I.


1.
Horáček Marián
24 b.



MASTERS II.


1.
Tóth Koloman
48 b.



FITNESS MODEL do 170 cm


1.
Takácsová Laura
52 b.



FITNESS MODEL NAD 170 cm


1.
Szabová Tünde
40 b.



ŠPORT FIGÚRA


1.
Puková Erika
35 b.



FITNESS LADIES


1.
Witzlerová Iveta
18 b.



ŠPORTOVÉ KLUBY


1.
ŠK ARENA RELAX TEAM Dunajská Streda
286 b.



Do bodového hodnotenia boli započítané výsledky zo štyroch súťaží v roku 2012 (MM SR Dunajská Streda, ME-UIBBN 2012 SITGES-BARCELONA, Kežmarská GRAND PRIX 2012 a MS-UIBBN 2012 Martin).

Všetkým vám srdečne blahoželáme k dosiahnutým výsledkom a želáme len to najlepšie aj v novom súťažnom roku 2013!

VR     S S N K F

