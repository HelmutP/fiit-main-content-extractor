

 S radosťou vám oznamujeme, že organizátor tohtoročných majstrovstiev Európy vo francúzskom Marseille bude realizovať aj priamy prenos finálovej časti prostredníctvom internetu. Online stream z produkcie Sportmagu sme presmerovali priamo na naše stránky a dúfame, že všetko vyjde a vy budete môcť na diaľku prežívať atmosféru podujatia spoločne s našimi reprezentantmi. 
 Sekciu s videom sme tiež doplnili o možnosť zaslať im odkaz, či vyjadriť podporu. Neváhajte preto a ukážte našim športovcom, že ste s nimi. 

