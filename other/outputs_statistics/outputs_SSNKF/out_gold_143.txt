
 V súvislosti s jesennou súťažou v naturálnej kulturistike a fitness, organizovanou z poverenia SSNKF, ktorá sa bude konať dňa 05. novembra 2011 v Kežmarku pod názvom KEŽMARSKÁ GRAND PRIX I. ročník, Vám oznamujeme, že v prípade záujmu o ubytovanie si ho môžete zarezervovať v ubytovacom zariadení HOTEL AUTIS na telfónnom čísle: 0908 290 033 na meno Karol Tatranský.

Cena ubytovania je 10,00 EUR na osobu a noc.



Odkaz na stránku ubytovacieho zariadenia: http://www.autishotel.sk/

V prípade akýchkoľvek otázok kontaktujte organizátora súťaže Karola Tatranského na tel. čísle: 0903 353 946 
