

Uvádzame zoznam pretekárov ktorí si svoju povinnosť týkajúcu sa odovzdania monitorovacieho hárku v prvom kvartáli r. 2011 už splnili. 
Pretekári ktorí si ešte nesplnili monitorovaciu povinnosť môžu ešte dodatočne vyplnené monitorovacie hárky odoslať na adresu SSNKF najneskôr do 31.01. 2011. 
V prípade nesplnenia si tejto povinnosti bude pretekárom udelený celoročný dištanc na súťažiach SSNKF!  
01. Dušana Lešková
02. Barbora Žideková
03. Petra Janovcová
04. Róbert Habrun
05. Tomáš Holúbek
06. Marián Kútik
07. Lauková Dominika
08. Čuláková Dana
09. Matyo Zoltán
10. Ján Demian
11. Lapin Peter
12. Alexander Milan
13. Katarína Tomášová
14. Peter Petkeš
15. Pavel Novák
16. Igor Vlačuha
17. Ladislav Múčka
18. Michal Pliešovský
19. Martin Vrbjar
20. Adrian Spevák
21. Janka Mlynarčíková
22. Miroslav Jaššo
23. Miška Sethalerová
24. Tomáš Velčický
25. Ákos Kertesz
26. Fóldes Gyongy
27. Ondrúšek Michal
28. Erika Saláthová
29. Andrea Fitošová
30. Eugen Csollei
31. Nataša Csolleiová
32. Lukáš Mandák
33. Kristína Jančovičová
34. Pavel Csémi
35. Tóth Erik
36. Muráň Lukáš
37. Vadkerti Igor
38. Strigáč Jakub
39. Kabát Miloš
40. Lenka Liptayová
41. Hončár Ľubomír
42. Novák Jaroslav
43. Vadkerti Roman
44. Vrba Jozef
45. Tóth Koloman
46. Méhes Roland
47. Tomáš Prostinák
48. Palčo Martin
49. Lenka Lalíková
50. Martin Macko
51. Veronika Juhászová
52. Jozef Chalmovský
53. Dávd Gál
54. Richard Čierny
55. Martin Hruščák
56. Michal Liška
57. Martin Minár
58. František Tóth
59. Marek Ferenci
60. Iveta Hauserová
61. Vanda Patasiová
62. Lukáš Vígh
63, Peter Winkler
64, Luboš Zeman
65, Mário Macko
66, Ingrida Melicherčíková
67, Maroš Grobelník
68, Angelika Jánosdeáková
69, Baton Murati
70, Ladislav Litva
71, Marián Horáček
72, Robert Michalovič
73, Andrej Uhrík
74, Ján Kontúr
75, Peter Lehotský
76, Daniel Kubaský
77, Ernest Takács
78, Nikola Scheerová
79, Viktor Toplanský
80, Marek Toplanský
81, Matúš Hromec
82, Packová Júlia
83, Dominik Zeman
84, František Engler
85, Juraj Kosorinský
86, Magdaléna Kažimírová
87, Peter Timko
88, Ján Farkas
89, Martina Deáková
90, Richard Woltemar
91, Silvia Hoštáková
92, Michaela Kantorová
93, Michaela Hanková
94, Tomáš Bóna
95, Michal Pažič
96, Jozaf Furin
97, Richard Németh
98, Barbara Madarová
99, Juraj Májovský
100, Viola Kucserová
101, Martina Uričová
102, Jozef Kovačin
103, Ivan Hlavička
104, Branislav Sitár
105, Lenka Liptayová
106, Tomáš Kuchta

