

 Šampionát v Senici rozhodol nielen o nových majstroch a majsterkách Slovenska, ale s konečnou platnosťou určil aj konečné zloženie reprezentačného teamu, ktorý odcestuje v stredu (2. júna) krátko pred polnocou do Francúzskeho Marseil na Majstrovstvá Európy federácie UIBBN - tie sa uskutočnia v sobotu, 5. júna. 
 Reprezentačný team SSNKF pocestuje v oblečení od spoločnosťi NEBBIA výrobcu značkového kvalitného oblečenia pre fitness a športové aktivity. NEBBIA je zároveň jedným zo sponzorov reprezentácie SSNKF. 
 Všetkým reprezentantom prajeme šťastnú cestu a veľa úspechov. 
 Tu sú mená 24 reprezentantov SSNKF vo všetkých vekových kategóriách. 
 Tomáš Prostinák, Ladislav Múčka, Róbert Habrun, Peter Petkéš, Július Karabinoš, Koloman Tóth, Jaroslav Novák, Miloš Kabát, Martin Palčo, Alexander Gazda, Peter Niňaj, Pavel Novák, Peter Lapin, Igor Vlačuha, Marián Kútik, Marek Toplanský, Erik  Tóth, Michal Pliešovský, Katarína Bodová, Ivana  Šarišská, Andrea Verešová, Beáta     Karásková, Mariana Ošková. 
  
 A takto vyzerá reprezentačný team priamo v dejisku majstrovstiev deň pred súťažou. 

