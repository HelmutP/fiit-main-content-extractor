

 SLOVENSKÁ SPOLOČNOSŤ PRE NATURÁLNU KULTURISTIKU A FITNESS 
 Monitoring pobytu športovca 

   S ohľadom na princípy antidopingového programu Svetovej antidopingovej agentúry (WADA) a v súlade s aktuálne platnou reguláciou kontroly a postihu dopingu v športe ako aj s dôrazom na optimálne a efektívne zabezpečenie výkonu mimosúťažnej dopingovej kontroly zverejňuje SLOVENSKÁ SPOLOČNOSŤ PRE NATURÁLNU KULTURISTIKU A FITNESS v súlade s ANTIDOPINGOVOU  AGENTÚROU SR monitorovací systém pobytu športovcov.

Monitorovací systém na báze formulára pobytov  je určený pre všetkých športovcov SSNKF.
 MONITOROVACÍ SYSTÉM POBYTU ŠPORTOVCOV SSNKF NA ROK 2010 

 

Pretekári vypisujú kvartálne štyri krát do roka monitorovacie formuláre a to:

 

do 20. decembra 2009 -         Na 1.kvartál (január - február - marec- 2010)

do 25. marca 2010 -            Na 2.kvartál (apríl - máj - jún -2010) 

do 25.júna 2010 -                 Na 3.kvartál (júl - august - september -2010)

do 25.septembra 2010 -  Na 4.kvartál.(október - november - december- 2010) 

 

Monitorovacie formuláre po riadnom vyplnení musia byť doručené na adresu: 

 

SSNKF

Javornícka 12

Banská Bystrica 974 11

e-mail: ssnkfsk@ssnkf.sk

mobil: 0907 877 512

 

Informácie uvedené v monitorovacích formulároch musia byť pravdivé, akákoľvek aktuálna zmena bude doručená na Antidopingovú agentúru SR.

Neposkytnutie adekvátnych informácií o miestach pobytu pre účel možnej realizácie mimosúťažnej dopingovej kontroly môže mať za následok porušenie noriem Svetového antidopingového kódexu ako aj ostatných pravidiel regulujúcich výkon antidopingovej kontroly a následne môže viesť k pozastaveniu až zastaveniu pretekárskej činnosti.

V Banskej Bystrici 27.11.2009
