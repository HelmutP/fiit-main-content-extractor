

 Milé pretekárky, pretekári a priaznivci naturálnej kulturistiky, 
       touto cestou Vás ŠK Victoria pozýva na druhý ročník Victoria Natural Cup, ktorý sa uskutoční        15. mája 2010 vo Veľkom Grobe z poverenia SSNKF. Radi by sme nadviazali na prvý ročník, ktorý sa konal v roku 2009 a dúfame, že svojou atmosférou Vám utkvela v pamäti. 
       Druhý ročník Victoria Natural Cup by sa mal niesť v znamení prezentovania sa  všetkých  kategórii  naturálnej kulturistiky. Dávame priestor pre kategórie dorast, junior, muži, masters, šport model a šport figúra.  Samozrejme nezabúdame na kategóriu nováčik a to v kategóriách  dorast,  junior         a  šport  model, ktoré však závisia od počtu prihlásených súťažiacich.   
       Verím, že táto pozvánka osloví veľa pretekárov, ktorí nasmerujú svoju súťažnú prípravu do jarného obdobia a využijú pohárovú súťaž na doladenie formy na majstrovstvá Slovenska v Senici a tí najlepší k reprezentácii našej krajiny na ME Marseille. 
 Promotér súťaže 
 Viliam Rigo 

