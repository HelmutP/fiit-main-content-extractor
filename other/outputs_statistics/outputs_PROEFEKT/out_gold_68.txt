

 
	Cez víkend 29.-30.6.2013 sa konali medzinárodné mládežnícke etapové preteky v Nitre, pod názvom Regiónom Nitrianskeho kraja. Preteky sa skladali z troch etáp, časovky do vrchu, časovky jednotlivcov a kritéria. 
 
	  
 
	V prvej etape, časovke do vrchu,  obsadil pretekár PROefektu Adam Szász 6. miesto z 26 pretekárov so stratou iba 6 sekúnd na prvého pretekára. 
 
	  
 
	V druhej etape, časovke jednotlivcov, PROefekt vkladal nádeje do svojho pretekára Štefana Sanetrika, ktorý svojim somatotypom mal zálusk na dobré umiestnenie. Štefan Sanetrik predpoklady potvrdil a v dramatickom a tesnom závere zvíťazil. Štefan opísal svoje pocity takto:  
 
	  
 
	"Časovka sa mi išla celkom dobre aj napriek tomu že som príprave na časovku začal venovať len nedávno. Svedči to o tom, že trening je nastavený dobre a mohlo by sa mi podariť na MSR umiestniť celkom dobre." 
 
	  
 
	 
 
	Pódium po 2.etape 
 
	  
 
	Tretia etapa bolo kritérium v meste Vráble. V tejto etape sa pretekári PROefektu výraznejšie nepresadili a Štefan Sanetrik obsadil 16. miesto z 33 pretekárov. 
 
	  
 
	V celkovom poradí pretekov sa Štefan Sanetrik umiestnil na 7. mieste, Adam Szász na 11. mieste a Milan Kováč na  15. mieste z 33 pretekárov.  
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 

