







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4085   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Svetový pohár v Novom Meste na Morave   27.05.2014  
	 
   
 
	Tento víkend 24.-25.5.2014 sa konal Svetový pohár XCO v Novom Meste na Morave. Na štart sa postavil náš pretekár Tomáš Višňovský a povzbudiť ho na trať prišiel tréner Juraj Karas. Tomáš zhodnotil preteky takto: 
 
	  
 
	Na Vysočinu sme prišli vo štvrtok poobede. Trať už bola pripravená a čakala na nás. Po minuloročných skúsenostiach som vedel čo ma čaká. Kamenisté zjazdy, korenisté výšľapy a perfektná divácka kulisa. Trénoval som každý deň ráno a večer hlavne zjazdy. Všetko vypuklo v piatok kedy štartoval eliminátor. V sobotu štartovali juniori, juniorky a ženy U23. Bol som sa teda pozrieť ako zvládajú dievčatá nástrahy trate. Večer som ešte vybehol na trať skontrolovať či sú všetky kamene na mieste. Potom ľahká večera a šup do postele. 
 
	  
 
	Ráno bola nervozita veľká. Štart bol o deviatej ale ja som stihol ešte jedno kolo na rozjazdenie . Štartový balík tvorilo 150 pretekárov a ja som mal číslo 86 čo bolo celkom v pohode. Minúta do štartu tep 145. Výstrel zaznel a ide sa na to ! Bomby hneď od štartu v hlave si vravím len nech to neľahne. Prešli sme cez mostík a už počujem škrípanie bicyklov. Prudký manéver doprava a som z toho vonku. Spolu z dvadsiatimi pretekármi sme si dobehli balík a pokračujeme ďalej. Nájazd končil po dvoch kilometroch korenistým zjazdom. Dve vlny potom skok prudká točka doľava a premávka sa zastavila. Tlačilo sa až hore potom zjazd sekcia rock garden a rýchlo ďalej. Prechádzam cez štart a strata dve minúty a dvadsať sekúnd. Postupne som sa dostaval dopredu zjazd Rubena a zrazu som sa ocitol na zemi. 
 
	  
 
	V duchu som si povedal že ako je toto možne keď som to jazdil asi sto krát. Postavil som sa a makám ďalej . Nasledovala sekcia vertical drop keď tu zrazu prásk. Pozerám čo sa stalo a sedlo nikde. Položil som bicykel mimo trať zobral sedlovku a zvyšok preteku som zostal už len v roly diváka. Bola to škoda lebo som sa cítil dobre a trať ma bavila. Pevne dúfam že sa mi už takéto problémy  budú vyhýbať.   
 
	  
 
	 
 
	  
 
	Tomáš Višňovský 
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  Milan  28.05.2014 07:58:24 
			

			 Obsah: Za zlomenú sedlovku v podstate nemôžeš, aj tak blahoželanie za snahu 
			

 
  
			

			 Pridal:  rNjYTXXlVf  11.02.2015 14:48:51 
			

			 Obsah:  every single &lt;a href="http://autoinsurquotespa.website"&gt;cheapest  auto insurance rates Pennsylvania&lt;/a&gt; other states protect &lt;a href="http://carinsurancequotesny.pw"&gt;VA auto insurance&lt;/a&gt; drive response &lt;a href="http://autoinsquotesnj.website"&gt;insurance auto&lt;/a&gt; liabilities comparatively expensive &lt;a href="http://autoinsurancequotesfl.pw"&gt;car insuranse&lt;/a&gt; damage liability 
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


