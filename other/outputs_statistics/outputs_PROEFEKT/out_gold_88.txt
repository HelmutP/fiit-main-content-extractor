

 
	 
		Rodriguezova odplata. Aj tak by sa dala nazvať tohoročná Giro di Lombardia, posledná z veľkých klasík a zároveň jediná jesenná. Už druhý rok po sebe sa konal tento monument presne týždeň po majstrovstvách sveta a viac ako inokedy bol ich vendetou. Obidva preteky totiž vyhovovali svojím profilom vrchárom. V minulosti sa z víťazstva tešili aj klasikári, naposledy Gilbert v rokoch 2009 a 2010. Od roku 2011 sú však súčasťou preteku dve veľmi dlhé stúpania, ktoré ešte viac zvýhodňujú ľahkých a vytrvalých cyklistov. 
	 
		  
	 
		 
	 
		Profil klasiky v ročníkoch Gilbertovej nadvlády 
	 
		     
	 
		 
	 
		Profil od roku 2011 
	 
		  
	 
		Pri stúpaniach dĺžky 8 – 10 kilometrov a priemernom sklone 8 % je pre zvládnutie stúpania dôležitý najmä pomer výkonu a hmotnosti pretekára. Pre porovnanie ardénske klasiky, na ktorých sa tiež často teší z víťazstva vrchár, majú stúpania väčšinou do dvoch kilometrov. Celkové prevýšenie síce môže prekonať Lombardiu, ale ťažší pretekári sa dokážu v častých zjazdoch „oklepať“, zbaviť laktátu a obnoviť energetické zásoby pred ďalším stúpaním. Keď cyklista stratí kontakt s čelnou skupinou v polovici 10 kilometrov dlhého stúpania, nemá takmer žiadnu šancu vrátiť sa späť. Rovnaký profil ako v Lombardii by povedzme na etape Tour de France dával väčšie šance aj odolnejším klasikárom. Avšak v pretekoch, kde o víťazovi rozhodne jeden jediný deň, idú všetci cyklisti na hranici svojich možností a to nie len v záverečných kilometroch. 
	 
		     
	 
		 
	 
		Gilbert sa v nedeľu poriadne vytrápil 
	 
		     
	 
		Prvý únik vytvorila šestica cyklistov a to Alessandro De Marchi (Cannondale), Carlos Quintero (Colombia), Willen Wauters (Vacansoleil) Fabio Felline (Androni), Michael Albasini (Orica), Reto Hollenstein (IAM). Neskôr bola na čele 21-členná skupina v ktorej boli aj Van Avermaet, Cunego, Roche, Bakelants. Pelotón sa však na treťom stúpaní spojil. 
	 
		Na náš smútok si už prvé dlhé stúpanie vybralo svoje obete a prekvapujúco bol medzi nimi aj Peter Sagan. Počasie, obrovské tempo, pády a náročný profil vyradili z preteku mnoho veľkých mien. Za zmienku stoja najmä Nibali, Schleck, Uran či Scarponi. Udržať sa čela mali neskôr problém aj nový majster sveta Rui Costa, Contador, či Roche. Počas druhého dlhého zjazdu sa pelotónu vzdialili Valverde s Quintanom, ktorých dobehli Caruso, Santoromita a Gasparoto. Ich únik si však pelotón kontroloval. Vypracovať výraznejší náskok sa podarilo až jednému z najväčších bojovníkov, Thomasovi Voecklerovi. Ten sa dokázal v samostatnom úniku vzdialiť pelotónu na tri minúty a vydržal v ňom až do rozhodujúceho stúpania. Tempo hlavnej skupiny sa pomaly zvyšovalo, čo si odniesol definitívne Contador, majster sveta Costa a problémy sa nevyhli ani Gilbertovi. 
	 
		     
	 
		 
	 
		Voeckler v úniku 
	 
		     
	 
		11 kilometrov pred koncom bol už Francúz pohltený a z hlavnej skupiny sa začalo nastupovať. Ako prvý sa pokúsil o únik Pinot, ale až útok Pozzoviva znamenal výrazné preriedenie skupiny. Na jeho nástup totiž reagovali všetci, ktorým ešte zostali nejaké sily. Najlepší bol jednoznačne Rodriguez, ktorý si v závere posledného stúpania vytvoril náskok viac ako 10 sekúnd. Stíhať sa ho snažil najmä Valverde, Majka a Daniel Martin. Rodriguez nakoniec nedopustil opakovanie Florencie a víťaztvo na „klasike padajúceho lístia“ obhájil. Valverde dorazil na druhom mieste, tretí bol Majka. Daniel Martin v závere spadol a skončil štvrtý. 
	 
		     
	 
		 
	 
		Rodriguezov útok na poslednom stúpaní 
	 
		     
	 
		Cannondale sa rozhodol ísť na Bassa, ktorý podal dobrý výkon a skončil na 11. mieste. Sagan bol pre tím vraj záložný plán. Môžeme len polemizovať o tom, či sa Tourminator postavil na štart len kvôli organizátorom alebo mal v preteku skutočné ambície a jednoducho na také dlhé stúpania nestačil. Peter v tejto sezóne štartuje už len na Japan Cupe (20.10.2013), ktorý je pre Cannondale dôležitý pravdepodobne najmä z hľadiska marketingu. Do cieľa sa nepozrel ani Peter Velits. Klasikárov bojovne zastupovali Gilbert a Van Avermaet, ktorí boli na chvoste prvej dvadsiatky. Dúhový Costa obsadil so stratou viac ako 8 minút 38. priečku. 
	 
		     
	 
		 
	 
		Padalo sa aj za cieľovou páskou (Domenico Pozzovivo) 
	 
		     
	 
		Výsledky Giro di Lombardia 2013 : 
	 
		     
	 
		1          Joaquim Rodriguez Oliver (Spa) Katusha      
	 
		2          Alejandro Valverde Belmonte (Spa) Movistar Team            
	 
		3          Rafal Majka (Pol) Team Saxo-Tinkof            
	 
		4          Daniel Martin (Irl) Garmin-Sharp                   
	 
		5          Enrico Gasparotto (Ita) Astana Pro Team 
	 
		     
	 
		Kompletné výsledky TU 
	 
		Video záverečných kilometrov TU 
	 
		      
	 
		J.S. a PROefekt 
 
 
	  

