

 
	Čo to je laktátová krivka? 
	 
	Ide o test vami zvolenej pohybovej činnosti (beh, bicyklovanie, veslovanie, plávanie..) so stupňujúcou intenzitou až do maxima. Z končeka prsta alebo ušného lalôčika sa po stupňujúcich úsekoch odoberá kapilárna krv na zistenie koncentrácie laktátu v krvi, ktorý odzrkadľuje metabolické procesy pri danej záťaži. Z nameraných hodnôt sa vyhotoví laktátová krivka a z nej následne určí hladina laktátu, srdcová frekvencia a rýchlosť na aeróbnom, anaeróbnom prahu a maxime v ktorom sa skončí (obr.1). 
	 
	 
	Grafická ukážka laktátovej krivky zastaralou metódou určenia AP na 2 a ANP na 4 mmol 
	 
	Pre koho je test vhodný? Test nemá takmer žiadne obmedzenia, je vhodný špičkovým športovcom, rekreačným športovcom ako aj osobám, ktoré športujú pre zdravotný, či estetický cieľ. Všeobecne platí, že najvyšším prínosom je pre toho, kto sa snaží trénovať zmysluplne, systematicky a efektívne.br&gt;br&gt; Pre príklad uvádzam vhodnosť a potrebu testovania (diagnostikovania) z rôznych aspektov u troch odlišných osôb venujúcich sa pohybovej aktivite. 
	 
	 
	Význam testovania z rôznych aspektov u troch osôb (Prevzaté z Hamar, 2003) 
	 
	Kedy testovať, a ako často? 
	 
	Toto je značne individuálna záležitosť. Som toho názoru, že vrcholový športovec by mal záťažový test podstúpiť minimálne štyrikrát do roka. Vždy na začiatku konkrétneho obdobia (mezocyklu), aby sa tréner vedel odraziť a videl akú „robotu“ spravili v predchádzajúcom období, čo treba zlepšiť v nasledujúcom vzhľadom k pretekovému programu a ladeniu športovej formy. Taktiež to je potrebné k „nakalibrovaniu“ tréningových zón, ktoré sa najmä rozdielnym tréningom vzhľadom k obdobiu menia. U výkonnostných by som odporúčal dvakrát za rok, rekreačných športovcov a osôb športujúcich pre zdravotné a estetické ciele postačuje jedenkrát za rok. 
	 
	Kde je test možné vykonať? 
	 
	Výhodou laktátovej krivky je jej vysoká praktickosť. Možno ju vykonať v rozličných športoch s horizontálnou lokomóciou (beh, cyklistika, veslovanie, plávanie, bežecké lyžovanie..) a taktiež v podmienkach prirodzených, teda tam kde daný šport prebieha – v teréne. Alebo v podmienkach laboratórnych (bežecký pás, bicyklový ergometer, veslársky ergometer..). Oba prístupy majú svoje klady i zápory. Avšak najdôležitejšie je zvoliť si ten šport, ktorému venujeme najviac času. Treba mať na zreteli, že výsledky z testu nie sú dobre transformovateľné na iný šport, než v ktorom bola laktátová krivka vykonaná. To znamená, že tréningové zóny zistené pri atletickom teste nebudú platiť pri tréningu na bicykli a samozrejme to platí i naopak. 
	 
	Čo presne si z testu odnesiem? 
	 
	Výkony na prahových hodnotách (aeróbny prah, anaeróbny prah a maximálny výkon v teste zodpovedajúci VO2max) a spolu s tvarom krivky nám udávajú pomerne presnú informáciu ako na tom sme v základnej vytrvalosti, zmiešanej, na úrovni anaeróbneho prahu a anaeróbno – aeróbnej, čiže tzv. pretekovej vytrvalosti. Hodnoty srdcovej frekvencie na prahových hodnotách využívame k zostaveniu jednotlivých tréningových zón, podľa ktorých sa dá tréning objektívne a prakticky riadiť smerom k cieľom prípravy. Ďalej to je úroveň zotavenia, ktorá vypovedá o možnej únave, začínajúcej chorobe, či pretrénovaní alebo nedostatočne vybudovanej základnej vytrvalosti. A v neposlednej rade, porovnanie svojich výkonov so sebou samým (ak som test už podstúpil) alebo s kolegom, súperom, kamarátom, či svetovou špičkou. 
	 
	Ako sa na test pripraviť? 
	 
	Vždy prízvukujem, že je veľmi dôležité prísť na testovanie odpočinutý, dostatočne zásobený energiou a hydratovaný. V princípe tu platí to, čo pred súťažou. Pre tých, čo na súťaže nechodia to rozoberiem konkrétnejšie. Deň pred testom, je nutné si dať len ľahší tréning alebo voľno. V deň testu je potrebné sa najesť cca 2,5 – 2 hodiny pred testom, potom už len nejaká tá ľahká tyčinka a prísť fyzicky i psychicky fit. Pamätajte na to, že práve nedostatok svalového, pečeňového glykogénu a i glukózy v krvi, je to čo môže primárne negatívne ovplyvniť váš výkon a taktiež hodnoty srdcovej frekvencie a koncentrácie laktátu v krvi. 
	 
	Ako sa obliecť? 
	 
	Ak sa test vykonáva v terénnych podmienkach je potrebné sa obliecť tak ako keby sa chystáme pretekať v danom počasí. To znamená skôr menej, aby sme sa príliš nepotili. Na úvodné zahriatie je dobré mať pripravenú teplejšiu vrstvu, ktorú pred samotným testom vyzlečieme. Pri testovaní v laboratórnych podmienkach (napr. aj fitness centrum) bývajú podmienky konštantné a to je cca 19 – 23 stupňov. Tu je vhodné si obliecť ľahké šortky a funkčné tielko, či tričko. Pribaliť treba aj uterák, ktorým sa občas pretrie pot z tváre, či rúk. Kvapky potu, ktoré sa rozlievajú po tvári, rukách významne zhoršujú termoreguláciu. Tento nežiaduci jav zapríčiňuje prehrievanie organizmu. To neadekvátne zvyšuje srdcovú frekvenciu a tým aj energetický výdaj organizmu v danej intenzite. Pri správnom oblečení a dôslednom utieraní sa dá tomuto efektu dostatočne zabrániť. No a na nohy športovú obuv, v ktorej daný šport vykonávame. 
	 
	Čo si je potrebné na test priniesť? 
	 
	Mimo športového oblečenia a uteráka je dôležité dodržiavať všade spomínaný pitný režim. Skôr ako rôzne energetické drinky odporúčam čistú vodu alebo jemne zriedený iontový nápoj, ktorý nám chutí a sme naň zvyknutý z tréningu. Kofeínové prípravky zvyšujú srdcovú frekvenciu a nechceme predsa „biť“ vyššie pulzy na hraničných hodnotách ako je u nás bežné. A posledné, čo netreba zabudnúť je chuť si dať do tela. 
	 
	Ako dlho to trvá? 
	 
	Na úvod si sadneme k vstupnému pohovoru/anamnéze, cca 5 minút. Potom nasleduje kvalitné 8 – 12 minútové zahriatie, kde sa organizmus zapracuje do vyšších intenzít. 2 minútky na ponaťahovanie, utretie, napitie a začíname so samotným testom, ktorý trvá cca 12 – 20 minút. Ihneď po skončení sú vyhradené 3 minúty pasívnemu zotaveniu, po nich nasleduje 12 minútový aktívny odpočinok (vyklusanie, vybicyklovanie..) po ktorom je vhodné doplňiť laktátový spád. Spolu to je cca 35 – 50 min. 
	 
	Kto môže test vykonávať? 
	 
	Testovanie je veľmi citlivá a odborná práca, pri ktorej sa i najmenšie chybičky krásy a nesprávne zvolená metodika môžu značne prejaviť na výsledkoch. Tie sú potom skreslené a neobjektívne. Zväčša metodika samotného zostrojenia krivky býva kameňom úrazu. Testovať by mala osoba, ktorá do toho vidí hlbšie, dokonalé pozná požadovanú problematiku, má dostatočné vzdelanie, vybavenie a samozrejme skúsenosti. Je nevyhnutné pristupovať ku každému jedincovi individuálne a pozerať sa na neho komplexne. To znamená, každému nadstaviť správny testovací protokol, vcítiť sa do jeho tela, tréningových návykov, športovej minulosti a aktivity posledných troch dní. Iba ak sú tieto požiadavky splnené môžeme hovoriť o kvalitnej diagnostike, ktorá nám dáva vysokú výpovednú hodnotu. 
 
	  
 
	Článok bol publikovaný na portáloch MTBiker.sk, behame.sk a abysportnebolel.sk 
 
	 
	 
	Autor: 
	 
	Mgr. Juraj Karas, PhD. / PROefekt 
	  

