







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5197   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Využitie spinningu, cyklist. trenaž. a válcov v tréningu   26.03.2013  
	 
   
 
	DIPLOMOVÁ PRÁCA 
	 
	 
	 
	Prečo som sa rozhodol pre daný výskum? 
	 
	- v minulosti som ako aktívny pretekár v cestnej, dráhovej a horskej cyklistike využíval jazdu na cyklistických valcoch, trenažéroch i spinneroch a v kruhu výkonnostných cyklistov ako aj ich trénerov som bol svedkom častých polemík, ktorý z uvedených prostriedkov je najefektívnejší z hľadiska prírastku športovej výkonnosti 
	 
	Kedy je výhodnejšie využiť uvedené tréningové prostriedky? 
	 
	- zlé a nevhodné počasie najmä v zimných mesiacoch, 
	- nedostatočne prijateľné počasie pre tréning po chorobe, či v zdravotnom oslabení, 
	- riziko otrasov a pádu pri vážnejšom zranení ako napr. zlomenina hornej polovice tela, 
	- maximalizovať využitie časových možností pre tréning, 
	- možnosť nepretržitej jazdy bez vynúteného zastavovania vplyvom vonkajších faktorov ako semafor, defekt, pád a iné., 
	- možnosť lepšieho sústredenia na danú prípravu ako rýchle šliapanie, 
	- rozjazdenie a vyjazdenie pred a po preteku, 
	- vytvorenie lepšej aklimatizácie na zmenu geografických podmienok ako horúco a vlhko pre kontrolovanejšie prostredie tréningu 
	 
	Spinning (spinner) 
	 
	- skupinové cvičenie s hudbou a inštruktorom na stacionárnych bicykloch (spinneroch), pri ktorom sa menia pozície rúk a technika jazdy, ktorá simuluje jazdu na bicykli v teréne 
	- spinner umožňuje kruhový pohyb nôh s nastaviteľným odporom, výšky sedla, predo – zadného nastavenie sedla a výšky riadidiel 
	 
	 
	Cyklistický trenažér 
	 
	- pevné zariadenie, do ktorého sa uchytí zadné koleso bicykla, mechanickú časť trenažéra tvorí valček, ktorý trením o plášť kolesa zabezpečuje brzdivý odpor šliapania až po nastaviteľnú záťaž 2000 wattov 
	 
	 
	Cyklistické valce 
	 
	- pevná konštrukcia dvoch hlinikových nosien medzi ktorými sú umiestnené dva valce vzadu a jeden vpredu, na valce sa položí bicykel a pri jazde sa udržuje rovnováha ako pri jazde vonku 
	- brzdivé: pôsobí na nich magnetická brzdiaca jednotka vďaka ktorej sa dá regulovať záťaž priamo na nich 
	- nebrzdivé: regulácia záťaže vychádza len voľbou prevodu na bicykli 
	 
	 
	Cieľ diplomovej práce 
	 
	Cieľom práce je prispieť k rozšíreniu poznatkov v oblasti variability závislosti dosahovania špeciálnej pohybovej výkonnosti cyklistov od tréningových programov s využitím spinningu, jazdy na cyklistických valcoch a trenažéroch. 
	 
	Hypotézy výskumu 
	 
	Na základe dostupných teoretických poznatkov z odbornej literatúry a skúseností z praxe predpokladáme, že: 
	 
	1. Výstupná úroveň, špeciálnej pohybovej výkonnosti, členov experimentálneho súboru dosiahne, na základe realizácie špeciálnych tréningových prostriedkov, vyššiu kvalitatívnu úroveň ako vstupná. 
	2. Diferenciácia zmien špeciálnej pohybovej výkonnosti členov experimentálneho súboru, v dôsledku realizácie rozdielnych tréningových programov, nedosiahne štatisticky významnú úroveň. 
	 
	Úlohy výskumu 
	 
	Na dosiahnutie cieľa práce a overenie stanovenej hypotézy sme si určili nasledovné úlohy: 
	 
	1. Diagnostikovať vstupnú úroveň špeciálnej pohybovej výkonnosti členov experimentálneho a kontrolného súboru. 
	2. S experimentálnou skupinou zrealizovať tréningový mezocyklus s využitím spinningu, cyklistických valcov a trenažérov. 
	3. Diagnostikovať výstupnú úroveň špeciálnej pohybovej výkonnosti členov experimentálneho a kontrolného súboru. 
	4. Posúdiť variabilitu vplyvu spinningu, jazdy na cyklistických valcoch a trenažéroch na zmeny špeciálnej pohybovej výkonnosti členov experimentálneho súboru. 
	 
	Metodika výskumu 
	 
	Stanovenie výskumnej situácie 
	 
	- realizovali sme intraindividuálne sledovanie zmien úrovne špeciálnej pohybovej výkonnosti horských cyklistov (experimentálny súbor) a nešportovcov (kontrolný súbor) medzi vstupným a výstupným testovaním, získané údaje sme podrobili skupinovej analýze 
	- medzi vstupným a výstupným testovaním cyklisti (členovia experimentálneho súboru) absolvovali rovnaký 3 – mesačný tréningový program s rozdielnym využitím tréningových prostriedkov (spinning, cyklistické valce a trenažére) 
	- základnou metódou výskumu bol experimentálny výskum, kde sme nezávisle premennou – experimentálnym činiteľom (tréningový program) vplývali na závisle premennú (výstupná úroveň špeciálnej pohybovej výkonnosti) 
	 
	Charakteristika súboru 
	 
	- päť výkonnostných horských cyklistov zúčastňujúcich sa slovenského poháru a päť nešportovcov, všetci mužského pohlavia a dospelí 
	Metódy získavania primárnych údajov 
	 
	Na posúdenie vplyvu experimentálneho činiteľa sme zvolili ukazovatele silového a vytrvalostného charakteru. 
	- na zistenie silovej pripravenosti sme zvolili výkony dolných končatín probandov, zaznamenávané maximálnym 10 sekundovým šliapaním do pedálov izokinetického bicyklového ergometra pri 80, 90, 100 a 110 otáčkach za minútu, výstupným parametrom bol najvyšší dosiahnutý priemerný výkon na 1 kilogram hmotnosti probanda [Watt/kg]) 
	- test vytrvalostnej pripravenosti pozostával zo 4-minútového šliapania konštantnou frekvenciou (100 ot/min) so zátažou 3,5 watt na kg telesnej hmotnosti probanda, výstupným parametrom bolo percentálne snaženie podania konštantného výkonu (priemerná PF) z maxima (maximálna PF). 
	 
	Metódy spracovania a vyhodnotenia získaných údajov 
	 
	Na spracovanie a vyhodnotenie výskumných údajov sme použili nasledovné metódy: 
	 
	- základné štatistické charakteristiky polohy (aritmetický priemer), rozptylu (extrémne hodnoty, ...) 
	- metódy vecne–logickej analýzy a syntézy s využitím induktívnych a deduktívnych postupov 
	- pre potreby vytvorenia grafického znázornenia dosahovaných hodnôt pulzovej frekvencie boli použité spojnicové grafy 
	Experimentálny činiteľ 
	Experimentálnym činiteľom bol dvanásť týždňový tréningový program v základnej perióde. 
	- dvaja probandi vykonávali ŠTP hodinami spinningu 
	- dvaja probandi vykonávali ŠTP jazdou na cyklistickom trenažéry 
	- jeden proband vykonával ŠTP jazdou na cyklistických valcoch 
	 
	 
	Dynamika zaťaženia v jednotlivých mikrocykloch vzhľadom k objemu zaťaženia ŠTP (hodiny) 
	 
	 
	Dynamika zaťaženia v jednotlivých mikrocykloch vzhľadom k objemu zaťaženia VTP (hodiny) 
	 
	 
	Dynamika zaťaženia v jednotlivých mikrocykloch vzhľadom k intenzite (čas strávený okolo ANP a nad ANP v minútach) 
	 
	 
	Dynamika zaťaženia v jednotlivých mikrocykloch vzhľadom k počtu TJ 
	- Cyklisti odtrénovali stanovené množstvo na 98% 
	- Tréningový proces bol realizovaný od decembra 2008 do februára 2009 
	- Zostavenie tréningového programu sme diskutovali so samotnými športovcami a ich trénermi 
	- Objem ŠTP bol 3 až 5 TJ do jedného mikrocykla v dĺžke trvania od 1 hodiny do 2 hodín na TJ 
	 
	Výsledky práce 
	 
	Medzi vstupnými a výstupnými kriteriálnymi ukazovateľmi silového a vytrvalostného charakteru probandov experimentálneho súboru po zrealizovaní tréningového programu nastalo zlepšenie: 
	 
	- probandi, ktorí absolvovali ŠTP na cyklistických trenažéroch sa v silovej pripravenosti zlepšili v priemere o 1,35 [Watt/kg] a vo vytrvalostnej pripravenosti o 3,8%. 
	- probandi, ktorí absolvovali ŠTP na s využitím spinningu sa v silovej pripravenosti zlepšili v priemere o 0,8 [Watt/kg] a vo vytrvalostnej pripravenosti o 4,2%. 
	- proband, ktorí absolvoval ŠTP na cyklistických valcoch sa v silovej pripravenosti zlepšil o 0,2 [Watt/kg] a vo vytrvalostnej pripravenosti o 1,5%. 
	Členovia kontrolného súboru nezaznamenali výraznejšie zmeny medzi vstupným a výstupným testovaním. 
	 
	 
	Zmeny silovej pripravenosti probandov experimentálneho súboru v závislosti od druhu realizovanej ŠTP 
	 
	 
	Zmeny vytrvalostnej pripravenosti probandov experimentálneho súboru v závislosti od druhu realizovanej ŠTP 
	 
	Závery 
	 
	Všetci členovia experimentálneho súboru dosiahli po zrealizovaní 3 mesačného tréningového mezocyklu vyššiu úroveň sledovanej špeciálnej pohybovej výkonnosti, čím sa nám potvrdila hypotéza 1 
	Pre rozvoj silovej pripravenosti sa ukázala najvhodnejšia ŠTP realizovaná na cyklistických trenažéroch, keď probandi trénujúci na nich sa zlepšili v najvyššiej miere a to o 1,35 [Watt/kg] 
	Pre rozvoj vytrvalostnej pripravenosti najefektívnejší spôsob vykonávania ŠTP predstavuje spinning, keď probandi trénujúci na spinneroch zaznamenali najvyššie zlepšenie o 4,2%. 
	Avšak rozdiely v prírastkoch špecialnej pohybovej výkonnosti (výstupne testovanie) probandov experimentálneho súboru nedosiahli, v závislosti od realizovaného ŠTP, významnú úroveň, čo potvrdilo stanovenú hypotézu 2 
	- k výsledkom treba pristupovať opatrne, keďže výskum bol realizovaný na nízkej početnosti probandov (intraindividuálne) a preto výsledky môžme považovať za informatívne, platné len pre danú skupinu cyklistov 
	 
	Odporúčania pre prax 
	 
	- na základe nami zistených výsledkov môžeme usúdiť, že pre rozvoj vytrvalostných schopností počas základného mezocyklu vo výkonnostnej cyklistike je najvhodnejšie využívať spinning. Jazda na spinnery ubieha relatívne rýchlo v skupine ďaľších cyklistov, v sprievode motivujúcej hudby a inštruktora 
	- pre rozvoj silových schopností výkonnostných cyklistov sa ukázalo za najefektívnejšie využívať jazdu na cyklistických trenažéroch. Keď súčasny trh ponúka modely s presne nastaviteľným odporom záťaže vo wattoch 
	- cyklistické valce sú velmi užitočné na nácvik techniky udržiavania stability, efektívneho kruhového šliapania, reflexov a najmä schopnosti držania rovnej stopy bicykla i vo vysokom zaťažení 
 
	  
 
	Autor: 
	 
	Mgr. Juraj Karas, PhD. / PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť     Všetky diela, vrátane publikácií, článkov alebo ich častí uverejnených na
	tejto stránke sú autorskými dielami chránené v zmysle autorského zákona.
	 
	Akékoľvek použitie takéhoto diela alebo jeho časti bez súhlasu autora
	alebo osoby, ktorej náleží právo na udelenie súhlasu s použitím diela,
	rovnako ako neoprávnené zásahy do autorského diela, vrátane neoprávneného
	rozmnožovania a ukladania do pamäťových a elektronických médií, kopírovania,
	distribuovania, prenášania, zobrazovania, uverejňovania, predávania,
	udeľovania povolenia, vytvárania odvodených diel sú protiprávne a
	postihnuteľné v občianskoprávnej alebo trestnoprávnej rovine. 
 
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


