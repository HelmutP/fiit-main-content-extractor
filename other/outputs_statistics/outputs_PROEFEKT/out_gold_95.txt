

 
	Po vianočných sviatkoch sa 28. decembra konal galavečer Zlatý pedál. Jeho hlavným programom bolo odovzdávanie cien pre najúspešenejších cyklistov v rôznych disciplínach za rok 2013 a vyhodnotenie ankety Miss slovenskej cyklistiky 2013. Na galavečeri bola prítomná špička slovenskej cyklistiky a aj prezident SR Ivan Gašparovič . 
 
	  
 
	Podľa očakávaní sa absolútnym víťazom ankety, aj najlepším cestným cyklistom roka stal Peter Sagan. Druhé miesto obsadil mladši z bratov Velitsovcov, Peter Velits. Z tretieho miesta sa zaslúžene tešila Tatiana Janíčková, ktorá vyhrala všetky preteky svetového pohára aj MS v cyklotriale a pridala k tomu 2. miesto z Majstrovstiev Európy. 
 
	  
 
	PROefekt neostal na galavečeri bez ocenenia. Zverenec PROefektu, Marek Peško, bol už tretí krát v rade ocenení ako najlepší cyklista Slovenska v kategórii zjazd a fourcross. 
 
	  
 
	 
 
	Marek Peško si preberá ocenenie od prezidenta SZC Petra Privaru 
 
	  
 
	Marek Peško vyjadril poďakovanie PROefektu a práci trénera Mgr. Juraja Karasa., PhD. : 
 
	"Ďakujem za systematický a efektívny tréningový plán a najmä možnosti trénovať s PROefektom pod vedením Juraja Karasa. Za posledný rok som sa pod Jurovím vedením podstatne posunul ku vrcholu svetovej špičky. " 
 
	  
 
	 
 
	Marek Peško ďakuje za podporu po prevzatí ocenenia 
 
	  
 
	Výsledky všetkých kategórií: 
 
	  
 
	Absolútne poradie: 1. Peter Sagan, 2. Peter Velits, 3. Tatiana Janíčková 
 
	Cestná cyklistika: Peter Sagan 
 
	MTB cross country: Michal Lami 
 
	 
		Cyklokros: Martin Haring 
 
 
	Dráhová cyklistika: Alžbeta Pavlendová 
 
	Zjazd, fourcross: Marek Peško 
 
	Bikros: Kristína Madarásová 
 
	Cyklotrial: Tatiana Janíčková 
 
	Sálová cyklistika: tím v krasojazde Alica Vinczeová, Dóra Szabóová, Eszter Bagitová a Viktória Glofáková 
 
	 
		Talent roka: Tereza Medveďová 
 
 
	Veteráni: Peter Florek 
 
	Zdravotne znevýhodnení: Anna Oroszová 
 
	Celoživotný prínos pre slovenskú cyklistiku: Ľudovít Pavela 
 
	  
 
	 
 
	Spoločná fotografia 
 
	  
 
	Doplnkovou časťou programu bolo vyhlásenie ankety Miss slovenskej cyklistiky 2013. V nej skončila zverenkyňa PROefektu Zuzana Juhásová na 2. mieste a prevzala tak titul 1. vicemiss slovenskej cyklistiky. 
 
	  
 
	 
 
	Zuzana Juhásová na galavečeri 
 
	  
 
	Celkové výsledky Miss slovenskej cyklistiky 2013:
 
 
	  
 
	1. Ľubica Ďaďová, 2. Zuzana Juhásová, 3. Oľga Gajdošíková 
 
	  
 
	PROefekt 

