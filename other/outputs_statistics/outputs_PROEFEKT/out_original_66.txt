







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  PROefekt s medailou na MSR v cestnej cyklistike   24.06.2013  
	 
   
 
	  
 
	V priebehu 4 dní 20-23.6.2013 sa uskutočnil sviatok česko-slovenskej cyklistiky, Majstrovstvá slovenskej a českej republiky v cestnej cyklistike. Pretekári PROefektu sa zúčastnili týchto pretekov a niektorí aj Suľovského maratónu v sobotu 22.6. 
 
	  
 
	Majstrovstvá Slovenska v cestnej cyklistike, časovka jednotlivkýň Elite Ženy, 20.6.2013 
 
	  
 
	Vo štvrtok 20.6.2013 bola na pláne časovka jednotlivcov v kategóriach juniori, ženy U23 a Elite Ženy.  Pretekárky čakal rovinatý okruh, ktorý ale nebol zďaleka jednoduchý vďaka nepríjemnému vetru. 
 
	  
 
	Pretekov sa z PROefektu zúčastnila Andrea Juhásová. Do takýchto pretekov nastupovala prvý krát, a o to viac to bolo pre ňu náročnejšie. Viac o priebehu pretekov nám už povedala ona sama: 
 
	  
 
	"Na časovke sa mi podarilo vyjazdiť pekné 4.miesto a neschytať obrovské časové manko. Bolo to pre mňa samu príjemné prekvapenie. Aj napriek tomu, že bolo veľmi teplo sa mi išlo celkom dobre. Prvý úsek po vetre mi nerobil žiaden problém a podľa analýzy jazdy som mala na prvom meranom úseku najlepší čas zo slovenských žien. Ďalšia časť trate sa šla proti vetru a tam som strácala." 
 
	  
 
	 
 
	Andrea Juhásová na štarte časovky vo farbách PROefektu 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	Majstrovstvá Slovenska v cestnej cyklistike, pretek s hromadným štartom, 22. a 23.6.2013 
 
	  
 
	Najdôležitejšie preteky "cestárskej" sezóny na Slovensku sa konali v Dubnici nad Váhom. Pretekári museli absolvovať dva typy okruhov, jeden s kratším stúpaním na Hrabovku a druhý veterný a technický okruh v okolí Novej Dubnice. 
 
	  
 
	V pretekoch Elite Ženy boli veľkou nádejou PROefektu sestry Juhásové. Ich výkonnosť je na výbornej úrovni, bolo otázne ako si dokážu poradiť s medzinárodnou konkurenciou a náročnou traťou. Ako sa im to darilo nám opísala Andrea: 
 
	  
 
	"Keď som štartovala na MSR v maratóne mala som jasný cieľ - zvíťaziť a získať titul majstra Slovenska. Na spoločné majstrovská Slovenska a Česka v cestnej cyklistike som nemala žiadne ambície na pódiové umiestnenie, nakoľko som cestných pretekov veľa neabsolvovala, tak som išla do pretekov s tým, že získam nové skúsenosti." 
 
	  
 
	 
 
	Sestry Juhásové v pelotóne v stúpaní na Hrabovku 
 
	  
 
	"Bol to môj tretí pretek na ceste a v ženskom balíku prvý. Pred štartom som mala obavy hlavne z jazdy v balíku. Tieto obavy však po prvom kole opadli a cítila som sa oveľa lepšie. Hneď po prvom kole odišli dve české pretekárky Šulcová a Sábliková a balík nejavil snahu dobehnúť ich. Po rovine sa šlo podľa mňa celkom pomalé tempo a balík sa delil na kopci. Kopec na Hrabovku som jazdila s prehľadom a nemala som žiaden problém udržať sa v prvej skupine, dokonca som kopec vychádzala v prvej desiatke. Do posledného kola nás ostalo v hlavnom balíku asi 25 žien. O víťazkách sa rozhodlo v záverečnom stúpaní na Hrabovku. Pod kopcom som sa snažila vyjazdiť si čo najlepšiu pozíciu, ale nepodarilo sa mi to tak ako som chcela. Ostala som na malú chvíľku zatvorená na pravej strane cesty a zľava som už len videla Líviu Hanesovú a Moniku Kadlecovú ako idú dopredu. Snažila som sa ich ešte dobehnúť, ale už sa mi to nepodarilo. Nakoniec z toho bolo 3.miesto a obrovská spokojnosť." 
 
	  
 
	 
 
	Na vrchole stúpania si Andrea Juhásová prichádza pre bronzovú medailu 
 
	  
 
	V pretekoch Elite Muži štartovali z PROefekt teamu 6 pretekári, Miro Kováčik, Juraj Kras, Pavol Dohnányi, Filip Lašút a Ján Subovits a v kategórii U23 štartoval Helmut Posch. Vzhľadom na dlhodobú výkonnosť PROefekt veril v dokončenie pretekov v podobe aspoň jedného pretekára. Tento cieľ sa nepodarilo naplniť, vysoké tempo na Hrabovke v podaní P. Velitsa  a "terezín" v druhej časti pretekov rozdelil pelotón natoľko, že nikto z nás nedokončil preteky v stanovenom limite. Vysokému tempu dokázal najdlhšie vzdorovať Ján Subovits a to cca 145 km. O náročnosti pretekov svedčí fakt, že do cieľa prišlo len necelých 40 pretekárov zo 177 štartujúcich. Peťo Sagan opäť potvrdil vysokú výkonnosť a jeho výnimočný pretekový prejav mu zaistil tretí titul majstra Slovenska v Elitnej kategórii v preteku s hromadným štartom. Gratulujeme! 
 
	  
 
	 
 
	Pavol Dohnányi a Miroslav Kováćik v hlavnej skupine 
 
	  
 
	Kompletné výsledky Elite Muži a U 23 nájdete TU 
 
	  
 
	Kompletné výsledky Elite Ženy nájdete TU 
 
	  
 
	AUTHOR GARMIN bikemaratón Súľovské skaly, 22. 6. 2013 
 
	  
 
	Počas "majstrovského" víkendu sa konali aj MTB preteky v Súľove. maratón má dlhú tradíciu a zaujímavú trať, preto sa očakávala veľká medzinárodná konkurencia. Tento pretek bol aj súčasťou Valašskopovažskej MTB série a každý pretekár chcel "vyjazdiť" čo najviac bodov. 
 
	  
 
	Z pretekárov PROefekt teamu sa zúčastnili pretekov Tomáš Višňovský, Štefan Sanetrik a Helmut Posch. Helmut a Tomáš štartovali na 48 kilometrovej trati a Štefan na 25 kilometrovej trati. 
 
	  
 
	Tomáš Višňovský sa od štartu držal s prvými pretekármi ale tempo Jana Fojtíka (Symbio+ Giant) bolo na neho príliš vysoké. Preteky dokončil na druhom mieste pred Romanom Kročekom (SK BAgBIKE). 
 
	  
 
	Helmut Posch sa po štarte držal v okolí prvej desiatky ale zle nahustené galusky a zradný terén podcenil a spadol v jednom zo zjazdov. Potom už nedokázal nájsť motiváciu na plnohodnotné pretekanie a preteky dokončil na 8. mieste v kategórii. 
 
	  
 
	 
 
	Tomáš Višňovský kontroluje nádskok nad súpermi 
 
	  
 
	 
 
	"Bedňa" 48 kilometrovej trate 
 
	  
 
	Štefanovi Sanetrikovi na 25 kilometrovej trati tesne uniklo pódiové umiestnenie a preteky dokončil na 4. mieste v kategórii. 
 
	  
 
	 
 
	Štefan Sanetrik na 25 kilometrovej trati, 4.miesto v kategórii 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  Giacomo Salvatore  01.07.2013 17:27:06 
			

			 Obsah: Pekny clanok, vidim ze pretekari PROefektu su kazdym rokom lepsi! 
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


