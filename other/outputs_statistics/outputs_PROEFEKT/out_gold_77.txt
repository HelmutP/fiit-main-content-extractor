

 
	17. augusta sa v obce Neverice, blízko Nitry, konal 2. ročník Nitrianského cyklomaratónu. PROefekt mal na ňom početnú účasť, čomu zodpovedali aj dosiahnuté výsledky. 
 
	  
 
	Organizátori dodržali tradíciu z minulého roku, čomu zodpovedala rovnaká 138 km trasa s prevýšením cez 2000 metrov. Tá prechádzala cez pohoria Vtáčnik a Tríbeč, kde sa nachádzali 4 náročné dlhé stúpania. Na štarte stálo takmer 100 pretekárov a medzi nimi pretekári PROefekt teamu Ján Subovits, Tomáš Višňovský, Juraj Karas, Miroslav Kováčik, Helmut Posch, Pavol Dohnányi a Filip Lašút. Pretekov sa zúčastnili aj ďalší zverenci PROefektu, Soňa Bušíková a Ľuboš Oravec. 
 
	  
 
	Po štarte nebola núdza o úniky z hlavného pola a jazdci PROefektu sa snažili o účasť v každom z nich. Prvý dlhší únik podnikol maďarský pretekár Fejes Gabor (Dr. Bátorfi Aktk) s našim Mirom Kováčikom a v takomto zložení vydržali až po začiatok stúpania na Veľké Pole, teda asi na 60ty kilometer. V hlavnej skupine sa šlo svižné tempo, v kopcoch i v technických zjazdoch sa skupina neustále delila a pod nimi zase spájala. Po dobehnutí úniku sa pokračovalo v nasadenom tempe a na vrchole stúpania za Veľkým Polom opäť zaútočil bezkonkurenčne najsilnejší pretekár pelotónu Fejes Gabor. Jeho únik sa podarilo zachytiť nášemu Subovitsovi a Martinovi Šmýkalovi (Inter BA). Šmýkal pod najťažším stúpaním na Skýcov odpadol a bol pohltený hlavnou skupinou. Fejes Gabor na tomto stúpaní utrhol aj našeho Jána Subovitsa a neohrozene si šiel pre víťazstvo na druhej horskej prémii dňa. V zjazde ho však počkal a spolu to úspešne dobojovali asi 12 km pred cieľom, kde už Janovi ušiel definitívne. Nakoniec prišiel teda druhý za Fejesom Gaborom so stratou necelých 3 minút. Za nimi prišla početná skupinka v ktorej boli aj ďalší pretekári PROefekt teamu. Druhý v špurte skupiny bol Juraj Karas, obsadil tak 5. miesto v celkovom poradí a 3. vo svojej kategórii. Hneď za ním na 6.mieste skončil Filip Lašut a vyhral svoju kategóriu 30-39 r. Miro Kováčik aj napriek tomu, že minul vela síl v úniku s maďarským pretekárom, došiel v tejto skupine a obsadil 3. miesto vo svojej kategórii. Helmut Posch obsadil 16. miesto a Tomáš Višňovský, po črevných problémoch, 17. miesto vo svojej kategórii. 
 
	  
 
	 
 
	šprint hlavnej skupiny do cieľa 
 
	  
 
	Tentokrát nám pocity zo soboty opíše víťaz Filip Lašut: 
 
	Po tom, ako mi uz tradične po prvej pol hodine preteku klesli tepy do normálu, sa išlo čoraz lepšie. Nohy ma v kopcoch poslúchali a okrem 17-percentného úseku na Skýcove som nemal problém udržať sa v hlavnej skupine. Keď som v klesaní z posledného vážnejšieho stúpania na 100tom kilometri zanalyzoval zloženie hlavnej skupiny a uvedomil si, že môžem bojovat o celkové prvenstvo v kategorii M30-39, tak sme sa s Jurajom dohodli, že mi pomôže do šprintu. Dôležité bolo, že som si dokázal postrážiť nástupy najväčších konkurentov do krátkych prudkých stúpaní na posledných 30km. V záverečnom technickom šprinte som potom využil Jurajove navigačné schopnosti a vybojoval víťazstvo v posledných metroch pred páskou. Po predchádzajúcich bedniach je to moje prvé víťazstvo, ktoré vnímam ako odmenu za poctivú robotu pod Jurajovym 17-mesačným vedením a zároveň aj veľkú motiváciu do ďaľšieho tvrdého ale premysleného tréningu. 
 
	  
 
	Tréner PROefektu Filipov výkon hodnotí: 
 
	  
 
	Ostatné mesiace mám zmapovanú nielen Filipovu výkonnosť, ale aj jeho pretekový prejav, nakoľko sa veľa pretekov pohybujeme bok po boku jeden druhému. Práve s tým druhým som nebýval až tak veľmi spokojný, čo sa však postupne mení k lepšiemu. Filip ukázal, že v každom stúpaní bol medzi prvými, jazdil s rozvahou, dokonca potiahol a v závere sme už vsadili na istotu. Pevne verím, že si plne uvedomí svoj výkonnostný potenciál a bude ho na pretekoch aktívne ukazovať nie len v rozhodujúcich momentoch. Z jeho víťazstva mám náramnú radosť, rozhodne si ho práve na tomto preteku zaslúžil. 
 
	  
 
	 
 
	Pódium kategórie mužov 19 - 29r. 
 
	  
 
	  
 
	 
 
	Pódium kategórie mužov 30 - 39r. 
 
	  
 
	  
 
	 
 
	Pódium kategórie mužov 40 - 49r.  
 
	  
 
	  
 
	 
 
	Celkové poradie Nitrianského cyklomaratónu 2013 
 
	  
 
	Soňa Bušíková, zverenkyňa PROefektu, obsadila pri svojej prvej pretekovej skúsenosti na tejto náročnej trati 2. miesto. Ľuboš Oravec obsadil 8. miesto v kategórii 40-49r. 
 
	  
 
	 
 
	Soňa Bušíková na pódiu v kategórii žien 
 
	  
 
	 
 
	Spoločná fotka "pódiových" pretekárov PROefekt teamu 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 

