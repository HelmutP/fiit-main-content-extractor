







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Prvý pretekový víkend PROefekt teamu priniesol prvé víťazstvá   14.04.2013  
	 
   
 
	  
 
	Uplynulý víkend PROefekt team odštartoval svoje prvé ťaženie na pretekoch. Všetci sme boli zvedaví ako dopadneme, nakoľko v príprave zatiaľ kladieme dôraz na všetko ostatné okrem intenzity. I napriek tejto filozofii s trendom dlhodobého rastu výkonnosti môžem vysloviť veľkú spokojnosť. 
 
	  
 
	SOBOTA 13.4.2013 
 
	  
 
	X-CROSS MTB maratón 
 
	  
 
	Tomáš Višňovský, Michal Aftanas a sestry Juhásové sa v sobotu zúčastnili X-CROSS MTB maratónu v Partizánskom. Väčšina absolvovala strednú 50km trať. Tomáš Višnovský s veľkým náskokom, viac ako 15 minút vyhral v celkovom poradí. Tento výkon doplnil Michal Aftanas svojim 9. miestom v kategórii mužov a na kratšej trati Marek Matúška 6. miestom. Sestry Juhásové taktiež potvrdili svoju výkonnosť prvým (Zuzana) a druhým (Andrea) miestom v ženskej kategórii. 
 
	  
 
	Tomáš Višnovský bol so svojim výkonom spokojný a prvé preteky sezóny hodnotí takto: " Pretek sa mi išiel veľmi dobre aj keď som ešte nejazdil moc intenzitu. Som spokojný s nastaveným tréningom a teším sa na ďalšie preteky ". 
 
	  
 
	 
 
	Historický prvé víťazstvo PROefekt teamu 
 
	  
 
	Sestry Juhásové sú dlhodobo favoritkami takýchto pretekov a predpoklady potvrdili svojim prvým resp. druhým miestom. Víťazka kategórie žien opísala preteky takto: " Prvý pretek sezóny hodnotím veľmi pozitívne, išlo sa mi veľmi dobre. Na začiatku som mala obavy ako zvládnem pretekárske nasadenie ale nohy posluchli a aj v blate som väčšinu prešla pekne v sedle ". Jej sestra Andrea Juhásová s ňou prehrala súboj o víťazstvo, ale napriek tomu je spokojná s výkonom: " Išlo sa mi pocitovo veľmi dobre, nohy vládali. Aj na to, že bolo veľa blata som išla skoro všetko na biku. Veľmi rýchlo som sa dostala do závodného tempa a nerobilo mi žiaden problém. Zjazdy som išla na úplnu istotu, s cieľom hlavne nespadnúť. Som veľmi rada, že zlomené zápästie celkom slušne držalo a na moje prekvapenie bolelo len trochu. S výkonom som spokojná a verím, že sa budem ešte zlepšovať. " 
 
	  
 
	Výsledky nájdete tu. 
 
	  
 
	Slovenský pohár v cestnej cyklistike - časovka jednotlivcov 
 
	  
 
	Prvé kolo slovenského pohára bolo zahájené časovkou jednotlivcov. PROefekt team zastupovali Štefan Sanetrik, Adam Szász, Milan Kováč a Ján Subovits. 
 
	  
 
	Štefan Sanetrik, vzhľadom na svoj somatotyp, preukázal časovkárske kvality a dokázal sa umietniť na výbornom 3.mieste v kategórii kadet. Pripísal tak historicky prvé pódiové umiestnenia PROefekt teamu v mládežníckej kategórii. Adam Szász obsadil vynikjúce 10.miesto a Milan Kováč došiel na 17. mieste.  
 
	  
 
	 
 
	Pyšný tréner so svojím zverencom Štefanom Sanetrikom po 3.mieste v časovke jednotlivcov 
 
	  
 
	Ján Subovits, v kategórii mužov, sa snažil konkurovať pretekárom z Dukly Trenčín - Trek ale stačilo to iba na 27. miesto so stratou okolo 4 minút  na víťaza pretekov, Maroša Kováča (Dukla Trenčín - Trek). Bol to jeho prvý štart a prišiel z podmienok, ako sám povedal, kedy ešte včera odhrabával z pred domu sneh. "dukláci" pretekajú už druhý mesiac. 
 
	  
 
	Výsledky nájdete tu. 
 
	  
 
	Nedeľa 14.4.2013 
 
	  
 
	V nedeľu sa konalo prvé kolo slovenského pohára v cestnej cyklistike - pretek s hromadným štartom, v Suchej nad Parnou. Na tomto preteku sme mali zastúpenie v podobe trénera PROefektu Juraja Karasa, Helmuta Poscha, Andreja Grujbára, Pavla Dohnányiho a Jána Subovitsa. 
 
	  
 
	 
 
	Spoločná fotka pred štartom v Suchej nad Parnou 
 
	  
 
	Preteku sa zúčastnili silné slovenské, české a maďarské tímy. Dokopy nás bolo, na Slovenský pohár nevídaný počet, skoro 100 štartujúcich. Pretek režírovala Duka Trenčín - Trek, ktorá mala v únikoch veľké zastúpenie. Po šiestich 23 kilometrových kolách s malým prevýšením ale nepríjemným vetrom, sa tešil z víťazstva Mário Daško a z druhého miesta jeho tímový kolega Maroš Kováč (obaja Dukla Trenčín - Trek). 
 
	  
 
	Jazdci PROefektu nemali ambície im konkurovať, keďže Dukla Trenčín - Trek ako aj ostatné tímy, majú už stovky pretekových kilometrov v nohách. Najlepšie dokončil preteky Ján Subovits na 20. mieste, za ním Juraj Karas na 30.mieste a Andrej Grujbár na 39.mieste. Helmut Posch a Pali Dohnányi náročné preteky nedokončili. 
 
	  
 
	 
 
	Andrej Grujbár si pýta občerstvenie 
 
	  
 
	Helmut Posch svoje pochybenie zdôvodnil takto: " Spravil som technickú chybu vo veternej časti okruhu, práve keď pelotón prudko zrýchlil. Nedokázal som sa do neho skoro vrátiť. Napriek tomu som mal nohy výborné a je mi ľúto zbytočnej chyby. Zatiaľ sa od začiatku sezóny v každom preteku zlepšujem a dúfam, že potvrdím tento trend budúci víkend ". 
 
	  
 
	 
 
	Sústredený Helmut 
 
	  
 
	Juraj Karas o svojom vystúpení: 
 
	Postaviť sa ako pretekár v týchto pretekoch som sa rozhodol 30 minút pred štartom, s mottom, že aj outsider to skúsi. Momentálna kondícia nie je vôbec dobrá. Tréningu sa pozvoľna venujem mesiac, po prekonaní ťažkej chrípkky s pridružením komplikovanej infekcie. Navyše ma trápia prechladnuté kríže a času na tréning je čoraz menej.. Výsledok ma pozitívne prekvapil, naznačil potenciál a chuť do ďaľšieho tréningu i popri čoraz väčšom pracovnom vyťažení. 
 
	  
 
	Za pokojný priebeh pretekov a skvelú starostlivosť ďakujeme podpornému tímu v zložení Peťa a Sašky :) 
 
	  
 
	 
 
	Peter Ďuriač podáva občerstvenie 
 
	  
 
	Výsledky nájdete tu. 
 
	  
 
	Fotogaléria - Slovenský pohár v cestnej cyklistike, Suchá nad Parnou 
 
	  
 
	 
 
	Andrej Grujbár v "balíku"  (prevzaté od Cyklocentrum PLUS) 
 
	  
 
	 
 
	Juraj Karas šetrí sily v závetrí (prevzaté od Cyklocentrum PLUS) 
 
	  
 
	 
 
	Unavený tréner v cieli 
 
	  
 
	 
 
	Každý má čo robiť 
 
	  
 
	Cieľové video trénera PROefektu 
 
	  
 
	 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  A  15.04.2013 17:07:28 
			

			 Obsah: Pekne si to zhodnotil aj s foto ;) 
			

 
  
			

			 Pridal:  PROefekt  19.04.2013 23:36:02 
			

			 Obsah: Dík :) 
			

 
  
			

			 Pridal:  a  10.05.2013 23:16:02 
			

			 Obsah: nemáte náhodou viac foto? prosím kontaktujte ma na mail Pato.Duric@centrum.sk  ďakujem 
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


