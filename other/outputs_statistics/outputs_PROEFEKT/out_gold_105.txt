

 
	Časovka na Javorinu sa konala ako M-SR v časovke do vrchu a PROefekt mal zastúpenie v podobe kadeta Milana Kováča. Na takýto typ pretekov je špecialista a očakávali sme pekný výsledok. V sobotu 14.9. sa konalo aj posledné kolo slovenského pohára v XCM a Cyklomaratónskej série. 
 
	  
 
	  
 
	MSR v časovke do vrchu, Javorina, 14.9.2013 
 
	  
 
	  
 
	Jeden z vrcholov národnej "cestárskej" sezóny sa konal na náročnom stúpaní na Javorinu. Pretekov sa v kategórii kadetov zúčastnil výborný vrchár Milan Kováč.  
 
	  
 
	 
 
	Milan Kováč tesne pred štartom 
 
	  
 
	V kategórii pretekal s ďalšími 31 kadetmi, čo je celkom vysoký počet pretekárov na slovenské pomery. Milan tieto preteky považoval tiež za jeden z vrcholov sezóny. S časom 31:52, ktorým by obstál aj medzi mužmi, obsadil v 3. miesto. Víťazom sa stal Hasch Henrich (MŠK CK Žiar n/Hronom) v čase 30:26 a s týmto časom by vyhral aj juniorskú kategóriu. 
 
	  
 
	 
 
	  
 
	Davorin XCM Slovak Paradise, Hrabušice, 14. 9. 2013 
 
	  
 
	V Hrabušiciach sa pri veľmi nepriaznivom počasí konalo posledné kolo Slovak XCM tour a aj slovenského pohára v XCM. Pretekov sa zúčastnili viacerí zverenci PROefektu, sestry Juhásové, Tomáš Salák a Matej Fačkovec. Všetci štartovali na 40 kilometrovej trati. 
 
	  
 
	Keďže sestry Juhásové štartovali na jednej trati, mohli si to rozdať v sesterskom súboji o víťazstvo. Nakoniec bola úspešnejšia Andrea, ktorá zvíťazila o viac ako 5 minút pred Annou Mikelovou (ceming team Košice). 
 
	  
 
	 
 
	Andrea Juhásová v cieli prvá medzi ženami 
 
	  
 
	Zuzana Juhásová mala na trati technické problémy a napriek nepojazdnému bicyklu dobehla do cieľa na 3. mieste, len o jednu stotinu pred Natáliou Šimorovou. 
 
	  
 
	 
 
	Zuzana Juhásová dobieha do cieľa na 3. mieste po technických problémoch 
 
	  
 
	Tomáš Salák po výbornom výkone obsadil 2. miesto v kategórii juniorov a v celkovom poradí prišiel do cieľa na sľubnom 11. mieste. Matej Fačkovec sa zo začiatku pretekov držal s vedúcou dvojicou, ale vytrhnutá špica z kolesa ho zbrzdila a dokončil preteky na 6. mieste v kategórii. 
 
	  
 
	 
 
	Tomáš Salák a Matej Fačkovec sa držia prvej skupiny 
 
	  
 
	V konečnom poradí Slovak XCM tour a v SP XCM dominovali sestry Juhásové, keď opanovali obidva seriály. V oboch zvíťazila Zuzana, Andrea bola na druhom mieste. v Slovak XCM tour im robilo konkurenciu 128 žien. Tomáš Salák sa svojim výkonom v poslednom kole dostal na 3. miesto celkového poradia v junioroch. 
 
	  
 
	PROefekt 

