

 
	V sobotu 18.8. sa uskutočnil 5. ročník Kellys Green bike Tour 2012. Podarená akcia sa niesla v duchu sucha, slnka a fair play. 
 
	  
 
	 
 
	  
 
	Na kratšej 34 km trati sme mali zastúpenie v podobe Martina Šmýkala, ktorý v čase 1:16:24 zvíťazil. Po ňom prišiel do cieľa Juraj Karas 1:16:49 (obaja Greenbike Realiz team). Na treťom mieste skončil Matej Fačkovec (CK AB Sereď), ktorý patrí do kategórie juniorov. 
 
	  
 
	 
 
	  
 
	Juraj Karas pretek hodnotí: „Celý pretek sme mali pevne v rukách. Ihneď od štartu vrátane prvého kopca som diktoval tempo. Na jeho vrchole sme zo 461 štartujúcich ostali siedmi. Pretek bol rýchly a hákový, dôsledkom čoho sme sa začali poctivo striedať na špici skupiny. Dvaja jazdci svižné tempo nevydržali a čelná skupina sa zúžila na piatich. V stúpaní za Malým Slavínom nastúpil Martin Šmýkal. Ja som to z druhej pozície pribrzdil a slovne ho vyslal do úniku. Postupne ho zvyšní jazdci začali stíhať, čo sa im aj v zjazde z Bieleho Kríža podarilo. Opäť sme boli piati. Martin sa s touto situáciou neuspokojil a na začiatku posledného stúpania na Kolibu opäť útočil. V jeho tesnom závese zostal Fačkovec, a za ním ja. Martin ukázal svoju dominanciu v strmých výjazdoch a v závere kopca tempo ešte navýšil. Toto sme už s Fačkovcom nedokázali akceptovať a pustili Martina prvého s miernym náskokom do dlhého zjazdu, ktorý vyústil do cieľa. Ten zvládol bezchybne a zaslúžene zvíťazil. Ja som urobil chybu a do zjazdu pustil prvého Fačkovca. Avšak ten tiež pochybil a v jednej zákrute vybehol mimo stopu. Nezaváhal som a predbehol ho. V tomto poradí sme došpurtovali do cieľa“.        
 
	  
 
	Martin Šmýkal po preteku povedal: „Vzhľadom  na to, že som nedávno absolvoval odpočinkovú časť sezóny, šlo sa mi dobre. V strmších výjazdoch som získaval a veril si. Nakoniec som rozhodol vo výjazde na Kolibu a v zjazde som si vedenie poistil. Za víťazstvo som veľmi rád!“ 
 
	  
 
	 
 
	  
 
	  
 
	Výsledky z 34 km trate tu 
 
	  
 
	  
 
	Na dlhšiu 75 km trať sa vydalo 186 horských cyklistov, vrátane nášho už skúseného Martina Kostelničáka. Ten nakoniec po dramatickom závere pretekov dosiahol vynikajúce 2. miesto za víťazom Martinom Škopekom. Ba čo viac, pomohol padnutému cyklistovi, za čo získal cenu fair play.  
 
	  
 
	  
 
	 
 
	  
 
	  
 
	Martin Kostelničák hodnotí priebeh preteku takto: „Spolu s Martinom Škopekom sme si zvolili taktiku v podobe pomalšieho začiatku a potom ostrejšieho tempa v druhej polovici. Smerom na Pezinskú Babu sme začali pridávať a v zjazde z nej s nami vydržal už len Peter Mosný (Bike team BA). I toho sme postupne striasli. Na začiatku stúpania na Kolibu mi Škopek nastúpil. I napriek počiatočným kŕčom som ho plynulou jazdou dobehol. Do dlhého zjazdu z Koliby sa pustil prvý Škopek, avšak v jednej zo zákrut zaváhal a vybehol zo stopy. V ten moment som ho obehol. Tesne pred koncom zjazdu blízko cieľa som mal zhruba 20 – 30 sekundový náskok, keď som na zemi uvidel ležať bezvládneho bikera z krátkej trate. Ihneď som zastavil a dobiehajúcemu Škopekovi kričal, aby privolal pomoc. To sa aj v krátkosti udialo a až potom som prišiel do cieľa. S podobnou situáciou som sa už stretol, a aj z tohto dôvodu som ani na moment neváhal zastaviť a pomôcť.“ 
 
	  
 
	  
 
	 
 
	  
 
	  
 
	Výsledky zo 75 km trate tu 
 
	  
 
	Organizačne bol maratón zvládnutý na vysokej úrovni. Vyzdvihol by som najmä skvelé umývanie bicyklov, kedy boli pripravené viaceré boxy so stojanmi, wapkou a chemickým čističmi. Taktiež v dojazde do cieľa čapované alko a nealko nápoje zdarma. 
 
	  
 
	Všetkým zúčastneným gratulujem k predvedeným výkonom a tešíme sa na stretnutie o rok. 
 
	  
 
	PROefekt 

