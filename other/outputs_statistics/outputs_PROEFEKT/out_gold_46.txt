

 
	Juraj Tomša obsadil na Majstrovstvách sveta XTERRA 4. miesto v kategórii juniorov. Miestom konania bol Hawai a pocity z pretekov nám popísal Juraj: 
 
	  
 
	Na Hawaii sme dorazili vo štvrtok, čiže 10 dní pred pretekom. Mali sme dostatok času na aklimatizáciu. S 12 hodinovým časovým posunom som sa vysporiadal celkom dobre. Čas letel veľmi rýchlo a nastal deň preteku. Začínalo sa na D.T. Fleming Beach. O deviatej hodine štartovali profi muži a ženy, o dve minúty všetky mužské kategórie a o ďaľšie dve minúty ženské vekové kategórie. Celkovo sa na štart postavilo 824 triatlonistov. Po štarte sme sa hneď museli vysporiadať s vlnami. Potom nastala klasika zopár kopancov a úderov. Plávalo sa 1,5 km, po ktorom nasledoval asi 300 metrov dlhý beh do depa, ktoré bolo umiestnené v areáli hotela Ritz-Carlton. 
 
	  
 
	 
 
	  
 
	Z depa sme sa vydali na 32 km dlhú cyklistickú časť s prevýšením 1000 metrov. Prvých 8 km sa stúpalo po veľmi úzkych cestičkách, takže bolo veľmi málo miesta na prebiehanie. Snažil som sa postupovať dopredu aj napriek menším technickým problém. Trat bola veľmi náročná, pretože bola veľmi kopcovitá a nechýbali ani technické zjazdy. Bike som zvládol za 1 hodinu 50 minút. 
 
	  
 
	 
 
	  
 
	Nasledoval 9 km beh s prevýšením 300 metrov. Počas celého preteku bolo veľmi teplo a na behu tomu nebolo inak. Bežalo sa v lese, takže som si musel dávať pozor na rôzne kamene a korene, pretože sa dalo ľahko prísť k zraneniu. Posledných 400 metrov sa bežalo po piesočnatej pláži a potom už len cieľová rovinka, ktorú som si vychutnal aj s kŕčami v lýtkach. 
 
	  
 
	 
 
	  
 
	Cieľovou bránou som prešiel časom 3:14:34. V kategórii juniorov som obsadil 4. miesto. Týmto pretekom som ukončil sezónu 2014. Chcel by som sa poďakovať mojej rodine a priateľom za podporu, trénerovi Jurajovi Karasovi za perfektnú prípravu a tiež sponzorom, bez ktorých by som nemohol štartovať na XTERRA WORLD CHAMPIONSHIP 2014. 
 
	  
 
	 
 
	  
 
	PROefekt 

