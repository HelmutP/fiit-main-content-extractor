

 
	Uplynulý víkend pretekári PROefektu bojovali na viacerých frontoch. A to veľmi úspešne! 
 
	  
 
	SOBOTA 22.9.2012: 
 
	  
 
	Juraj Karas, Tomáš Višňovský a Milan Kováč sa zúčastnili Púchovského MTB maratónu. Tomáš obsadil v nabitej konkurencii českých jazdcov na 46 km trati kvalitné 2. miesto, keď ho porazil iba Čech Pavel Žák (TROB cyklo o.p.). 
 
	  
 
	 
 
	  
 
	Juraj obsadil 6.miesto. Pretek hodnotí takto: "Nešlo sa mi príliš ideálne, trápil som sa a bojoval. Cítil som, že organizmus nefunguje na 100%, čo sa mi potvrdilo v týchto dňoch keď sa prejavilo začínajúce nachladnutie. Sezóna je dlhá, ešte by som rád vydržal v pretekovom nasadení do posledného nasledujúceho víkendu." 
 
	  
 
	Milan taktiež potvrdil svoj tohto ročný výkonnostný progres, keď v svojej kategórii na 26 km trati obsadil 2. miesto za oddielovým kolegom Alexom Zemanom. 
 
	  
 
	 
 
	  
 
	Tomáš Višňovský pretek hodnotí: „Pred štartom som sa necítil úplne dobre, nakoľko som nastúpil na štúdium na FTVŠ UK a vo štvrtok som v rámci praktickej výučby predmetu Atletiky I. absolvoval namáhavé cvičenia, ktoré nechali na mojich nohách stopy a nohy ešte trocha pobolievali. I napriek tomu som si povedal, že sa pokúsim dať do toho všetko. V prvom kopci sa to samozrejme ihneď roztrhalo. Vpredu sa vytvorila dvojčlenná skupinka a potom štvorčlenná v ktorej som bol aj ja. Po cca 15 km odpadol z našej skupiny jeden člen. V polke trate sa mi už začali ozývať nohy a tak sa mi vzdialila skupinka asi na 15 sekúnd. Cca 10 km pred cieľom ale skupinka zle odbočila a tak som sa dostal pred nich a v zjazde som to už nepustil. Takže aj vďaka šťastiu som došiel celkovo na 3 mieste (druhý v kategórii). Trať bola veľmi pekná a aj dosť členitá. Nohy som však mal po preteku úplne na „kašu“ J. Ale spokojnosť je veľká. 
 
	  
 
	Výsledky Púchovského maratónu tu 
 
	  
 
	 
 
	  
 
	Dvojica Martinov - Šmýkal a Kostelničák si odskočili na Majstrovstvá Slovenska v kros triatlone do Myjavy. Šmýkal, ktorý ešte patril do juniorskej kategórie ukázal špecialistom, že cyklisti nie sú žiadne bábovky a vybojoval titul! Kostelničák v kategórii elite obsadil nepopulárne 4. miesto. 
 
	  
 
	Martin Šmýkal pretek hodnotí: Vyplával som ďaleko od najlepších. Keď som vyšiel z vody motalo ma ako by som bol opitý J Na bicykli som išiel čo to dá, aby som sa čo najviac priblížil špici pelotónu. Podarilo sa mi dotiahnuť na šieste miesto celkovo, čo znamenalo v junioroch miesto prvé. Túto pozíciu sa mi podarilo v behu udržať. Z titulu sa teším, bolo to zábavne spestrenie ku koncu cyklistickej sezóny. 
 
	  
 
	Výsledky MSR kros triatlonu tu 
 
	  
 
	Na horských bicykloch sa bojovalo aj na strednom Slovensku, konkrétne na MTB maratóne v Skalke pri Kremnici. Igorovi Ďuriškovi sa darilo a na dlhej 85 km trati získal cenné 3. miesto. 
 
	  
 
	Výsledky MTB Tour Kremnica tu 
 
	  
 
	 
 
	  
 
	  
 
	 
 
	  
 
	NEDEĽA 23.9.2012: 
 
	  
 
	V Košiciach sa konal horský pretek a na ňom samozrejme nemohol chýbať „náš“ rodák z Košíc Adam Szász. Jeho vystúpenie skončilo taktiež pódiovo. 
 
	  
 
	Adam pretek ohodnotil: „Dnes sa mi celkom darilo, skončil som druhý. Predo mnou bol iba pretekár z juniorskej kategórie. Išlo sa mi dobre hlavne v kopcoch, naopak v zjazdoch som sa trochu bál.“ 
 
	  
 
	V Sielnici sa konala časovka do vrchu na 9 km. V svojej kategórii opäť ukázal svoju formu Igor Ďuriška, keď obsadil 2. miesto. 
 
	  
 
	Rovnaké umiestnenie zajazdil Ivan Haas a súperom vo svojej kategórii ukázal, že na cyklokros sa svedomito pripravuje. 
 
	  
 
	 
 
	  
 
	Martinovia – Kostelničák a Šmýkal sa zúčastnili Ružinovskej časovky dlhej 4,8 km. Tu sa im už priveľmi nedarilo a obsadili 7. a 12. miesto. 
 
	  
 
	Výsledky Ružinovskej časovky tu 
 
	  
 
	Touto cestou všetkým srdečne gratulujem! Verím, že ich to naštartovalo do ešte svedomitejšej prípravy v zimných mesiacoch, ktoré nás čakajú. 
 
	  
 
	PROefekt 

