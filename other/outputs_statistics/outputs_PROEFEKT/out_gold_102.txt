

 
	Minulý víkend sa konali Majstrovstvá Európy v St. Wendeli. V kategórii U23 štartoval Tomáš Višňovský, v juniorskej kategórii a štafete Filip Sklenárik. 
 
	  
 
	Tomáš opísal svoje pocity z pretekov takto: 
 
	  
 
	"Odchod bol v stredu ráno. Prišli sme večer do pekného privátu s dobrým zázemím. Trať bola asi dvadsať minút na bicykli. Technická náročnosť nebola veľká, kolo malo 7 kilometrov s krátkymi kopcami, tiahlymi rovinkami a rýchlymi zjazdmi. 
 
	  
 
	Program začínal vo štvrtok šprintami, kde z kvalifikácie postúpil František Lami. Následne ale v prvom kole vyraďovacích jázd po kontakte s pretekárom nepostúpil ďalej. 
 
	  
 
	Piatok nasledovali štafety. Tá naša štartovala v poradí Martin Haring, Janka Keseg Števková, Filip Sklenárik a František Lami. Pretek bol perfektne rozbehnutý vďaka Maťovi ktorý predával na piatom mieste. Konštantnou jazdou bola SR v cieli evidovaná na peknom siedmom mieste. Po štafetách som  vypucoval  bicykel a pripravoval sa na sobotu. 
 
	  
 
	Štart som mal o piatej, teplotné podmienky boli dosť náročne. Tentokrát  som štartoval z päťdesiatej ôsmej  pozície. Vyštartovalo sa celkom pokojne bez pádu iba kde tu nejaká roztrhnutá reťaz :). Po zavádzačke, ktorá mala 900m sme sa napojili na trať. Pole bolo dosť potrhané. Prišiel prvý ťažký asfaltový výjazd a povedal som si, že teraz sa musím dostať dopredu.  V polovici kopca ma však poriadne zaseklo a tak som sa sypal až na samí záver poľa. Rozdýchal  som to asi po jednom kole. Postupoval som pomaly dopredu. Na rovine a v miernych stúpaniach som sa držal skupiny a keď sa to trocha zdvihlo začal som prenasledovať  skupinu pred nami. Rozbiehal som sa stále viac a viac. Pri prejazde cez depo ma kolegovia z reprezentácie povzbudzovali a  informovali ako to vyzerá predo mnou. Posledný výjazd v piatom kole okolo depa šupol som tam ešte Dána a rýchly zjazd do cieľa . Finišoval som na 38 mieste. Spokojný ale trocha aj nahnevaný sám na seba som umyl bicykel a hodil do seba nejaký ten liter vody. Okrem mňa pretekali ešte v U23 František Lami ktorý skončil 28. a Jozef Bebčák, ktorý obsadil 31 miesto.   
 
	  
 
	V nedeľu štartovala elita kde sa o pekné výsledky postarali Janka Keseg Števková ktorá obsadila 12. miesto a Maťo Haring ktorý bol 23. Mišovi Lamimu asi pretek nevyšiel podľa predstáv keď skončil v úvodzovkách len na 42. mieste. 
 
	  
 
	So svojim výkonom som spokojný ale viem, že mi chýbajú skúsenosti na takýchto pretekoch a taktiež sa musím ešte adaptovať na intenzitu pretekania, ktorá je vo svete úplne iná. Preto som vďačný za každý pretek ktorý môžem absolvovať mimo Slovenska." 
 
	  
 
	 
 
	  
 
	Preteky očami Filipa: 
 
	  
 
	"Celý čas bolo veľmi teplo a cesta autom bola veľmi únavná. Ubytko bolo veľmi dobré tak isto aj prostredie.Bol som ale nemilo prekvapený, že to tam vyzeralo ako na Slovenskom pohári - ako trať tak aj učasť. Divákov málo a povzbudzovanie dosť chabé. Skúsenosť je to aj tak veľká a som celkom so sebou spokojný, že som schopný jazdiť aj na okruhu, ktorý mi až tak nesedel okolo polovice štartoveho pola. Išlo sa mi dobre od štartu, kde sme boli v poslednej lajne. V priebehu pretekov som sa dostal tak do 2. tretiny pola. Potom som sa postupne prepracovával do polovice až som sa držal stále okolo 40 miesta. Moje slabiny boli v dlhých a miernych výšlapoch najmä v druhej polovici okruhu. Preteky som dokončil na 40. mieste. S výsledkom v XCO som celkom spokojný. S pretekmi XCE nie som spokojný ale technické problémy, za ktoré nemožem, ma odstavili. Aspoň som získal skúsenosť s takýmto typom pretekov. Som rád, že som mal tú česť štartovať za slovenskú štafetu a v preteku som nehal všetko. Medzi štafetami sme obsadili nakoniec 7. miesto." 
 
	  
 
	Ich tréner Juraj Karas hodnotí účasť Tomáša a Filipa na ME XCO takto: 
 
	  
 
	"Som rád, že sa chalani mohli konfrontovať na významných pretekoch, akými Majstrovstvá Európy sú. Popri skúsenostiach, ktoré tam získali hodnotím veľmi pozitívne ich postrehy z fungovania organizmu počas zaťaženia. Tým, že mi ich sprostredkuju viem ich tréning doladiť ešte presnejšie. Teším sa na ich ďalšie medzinárodné vystúpenia, do ktorých by mali byť pripravení ešte lepšie." 
 
	  
 
	Tomáš Višňovský, Filip Sklenárik 
 
	PROefekt 

