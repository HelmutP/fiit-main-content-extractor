







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4085   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Miláno - San Remo mení svoj charakter   22.10.2013  
	 
   
 
	  
 
	Najdlhší jednorazový pretek, ktorý sa nachádza v kalendári World Tour už nebude pre šprintérov. Z profilu trate sa síce vytratilo stúpanie Le Manie, ktoré sa nachádzalo 95 km pred cieľom, ale pribudlo nové stúpanie s názvom „Pompeiana“. Organizátori ho umiestnili na oveľa zaujímavejšie miesto - medzi tradičné kopce Cipressa a Poggio. Trasa preteku sa predĺži o jeden kilometer na 299 km. Už 105. ročník tejto klasiky je na pláne v nedeľu 23.3.2014. 
 
	 
		     
	 
		 
	 
		Milano-San Remo do roku 2014 
	 
		  
	 
		 
	 
		Milano-San Remo v roku 2014 
	   
	 
		Nový kopec začne už tri kilometre po zjazde z Cipressi. Jeho dĺžka je 5 kilometrov pri priemernom sklone 5%, maximum však dosahuje až 14%. Pelotón sa poriadne natiahne, keďže cesta vedúca na kopec je veľmi úzka. Z vrcholu stúpania bude do cieľa ostávať 20 kilometrov. Agresívnym cyklistom bude vyhovovať aj technický zjazd z mestečka Castellaro. Po zjazde neostane pelotónu ani 5 kilometrov do Poggia, takže čas na regeneráciu pre pretekárov s vyšším podielom rýchlych svalových vláken bude extrémne krátky. Čistokrvní šprintéri to na ceste do San Rema nemali nikdy jednoduché, budúci rok bude však ich víťazstvo pravdepodobne nedosiahnuteľné. Cavendish, Greipel či Kittel si tak nebudú musieť lámať hlavu ako vydržať na čele do záverečného šprintu a môžu sa pokojne pripravovať na iné vrcholy sezóny. Ťažké to budú mať aj pretekári typu Gossa, Cioleka a Degenkolba. Pre Toma Boonena by nová trať mala byť taktiež priveľkým sústom. 
	 
		     
	 
		 
	 
		 
	 
		 
	 
		Najstrmšie úseky stúpania Pompeiana 
	 
		     
	 
		Naopak Phillipe Gilbert už avizoval, že najdlhšia klasika bude jeho hlavným jarným cieľom : „San Remo je pretek ktorý milujem a rád by som vyhral . Bol som na pódiu už niekoľkokrát a teraz som presvedčený ešte viac, že môžem zvíťaziť." Gilbert ešte doplnil, že pokiaľ pelotón absolvuje novú trasu, nevidí žiadnu šancu na úspech pre šprintérov. Výnimkou je podľa neho Peter Sagan, ktorého považuje za jedného z favoritov. My dodáme, že Gilbert má zo San Rema zatiaľ dve tretie miesta. 
	 
		  
	 
		 
	 
		Finiš z roku 2013(zľava Sagan, Ciolek, Cancellara) 
	 
		     
	 
		Medzi ďalších favoritov môžeme zaradiť klasikárskeho špecialistu Fabiana Cancellaru (zo San Rema má na konte jedno víťazstvo – 2008, dve druhé miesta – 2011, 2012 a jedno tretie – 2013) a hlavne domáceho miláčika Vincenza Nibalihio. Talianski organizátori sa možno práve kvôli nemu rozhodli trať preteku sťažiť. Žralok tak bude mať pre domácim publikom dobrú šancu odčiniť smolné MS vo Florencii. Dobrý výsledok sa dá očakávať aj od Grega Van Avermaeta, ktorému by podľa nás zmena profilu mala sedieť. Agresívnou jazdou sa budú chcieť prezentovať Thomas Voeckler či Silvain Chavanel. Budeme taktiež zvedaví, či nové stúpanie priláka viac vrchárov ako tomu bolo minulé roky. Účasť Joaquima Rodrigueza by organizátorov aj fanúšikov určite potešila. 
	 
		     
	 
		 
	 
		Posledné kilometre budú naozaj náročné 
	 
		     
	 
		 
	 
		     
	 
		                                                
	 
		J.S. a PROefekt           
 
 
	  
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  Ivan Murarik  20.03.2014 21:14:32 
			

			 Obsah: Otazka Je podla Vas mozne zafanusikovat si na Pompeiane s stihnut sa presunut autom k cielu pred pelotonom ? 

 
			

 
  
			

			 Pridal:  PROefekt  20.03.2014 21:19:31 
			

			 Obsah: Myslíme si, že nie. Cesta na Popmpeianu bude uzavretá a časovo to asi nebude možné. 
			

 
  
			

			 Pridal:  RkcjSHyss  09.02.2015 13:14:38 
			

			 Obsah:  difficult now &lt;a href="http://carinsuranceca.website"&gt;car insurance California&lt;/a&gt; minimum policy requested quotes &lt;a href="http://autoinsurancequotesfl.pw"&gt;Florida car insurance&lt;/a&gt; being hit fellow christians &lt;a href="http://autoinsuranceny.club"&gt;insurance car&lt;/a&gt; car insurance policy &lt;a href="http://carinsurancenj.pw"&gt;online vintage New Jersey  car insurance&lt;/a&gt; windows 
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


