







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5197   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5121   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4546   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4184   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3601   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3305 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Zlepšenie v každom veku   15.8.2012  
	 
   
 
	   Veľa rekreačných a hobby športovcov sa domnieva, že v určitých prípadoch už zlepšenie nie je možné. Väčšinou sa jedná o okolnosti, kedy človek dlhoročne športuje, nepatrí už k najmladším alebo len jeho tréning nadobudol stereotyp. Áno, väčšina z nás si povie: „Možno by som sa i zlepšil, ale musel by som na tom bicykli sedieť častejšie a dlhšie a to už ja nechcem, nemôžem, nedá sa mi“. Tento postoj je pochopiteľný, veď na svete neexistuje len bicykel. Aj keď pre niektorých z nás znamená veľa. Som toho názoru, že každý bez rozdielu veku a výkonnosti sa môže pri rovnakom časovom úsilí, teda rovnakom objeme tréningu na aký bol zvyknutý, správne nastaveným tréningom zlepšiť. 
 
	  
 
	  
 
	   Ako príklad uvediem zverenca Igora Ďurišku. Igor je od mladosti telom aj dušou športovec. Dlhé roky sa venuje najmä cyklistike, cez zimu bežeckému lyžovanie a skialpu. Jeho športová výkonnosť ja na vysokej úrovni a patrí ku širšej špičke v kategórii veteráni 50 – 59 rokov v horských maratónoch. Keď sa mi dostal pod ruku, bol som prekvapený jeho zvedavosťou, vášňou a zapálením pre tréning. Vycítil som z neho prirodzenú inteligenciu a záujem o možnosť zefektívnenia svojej prípravy. Podstúpil vyšetrenie laktátovej krivky, pri ktorom som mu zistil prahové i maximálne parametre v absolútnych i relatívnych „wattoch“. Z nich som uvidel, že jeho základná aeróbna vytrvalosť v oblasti aeróbneho prahu (AP) je na výbornej úrovni, no mierne zaostávala oblasť anaeróbneho prahu (ANP). Avšak pozor, maximálny aeróbny výkon (VO2max), vykazoval veľmi dobré hodnoty. To svedčí o tom, že Igor má vďaka dlhoročnému vytrvalostnému tréningom rozvinutý potenciál na naozaj slušnej úrovni. Tiež to, že sa vie húževnato zaťať a zabrať aj v nepríjemnej intenzite nad ANP. Z týchto zistení som vyvodil, na čom bolo treba systematicky zapracovať. Za 4 mesiace tréningu podstúpil množstvo cielených intervalových tréningov na rozvoj jeho slabšej stránky, ANP.  Igorova tréningová morálka je vynikajúca, mohol by byť príkladom mnohým mladším športovcom. Taktiež vie počúvať svoje telo, a pri vyššej pracovnej vyťaženosti, či únave z tréningov si vedel v správny čas prípravu uspôsobiť a dať si voľno. Igorove dojmy a výsledky z pretekov nám dávali čiastkovú odozvu, že ideme správnou cestou. Chválil si pocit z jazdy a na maratónoch si vylepšoval umiestnenia. Ja som v tomto skôr skeptický a tak si vždy ešte počkám na nasledujúcu diagnostiku, kde to už vidím čierne na bielom. Po jeho otestovaní a vyhodnocovaní laktátovej krivky ma zaliala obrovská radosť, keď som videl to enormné zlepšenie na ANP. 
 
	  
 
	  
 
	 
 
	Učebnicový posun laktátovej krivky doprava u Igora Ďurišku 
 
	  
 
	  
 
	   Zlepšili sa aj ostatné parametre, ale práve ANP, jeho slabina, slabinou už nie je. Išli sme si sadnúť a dopriali si zaslúženú pizzu. Ale nezaspali sme na vavrínoch a ďalším tréningom sa posúvame zase o krok vpred. A tak len pevne verím, že čoskoro sa dostaví aj „bedňa“. I keď najdôležitejší je dobrý pocit z tréningu a radosť z každodenného zlepšovania sa. 
 
	  
 
	Nedá mi tu nenapísať moje osobné motto: 
 
	„Zmysel života nie je cieľ, ale spôsob akým ten život vedieš, akou cestou ten cieľ dosiahneš“.           
 
	  
 
	  
 
	Zlepšenie za 4 mesiace systematického tréningu 
 
	AP:  0,37 W/kg 
 
	ANP:  0,84 W/kg 
 
	Max. výkon:  0,12 W/kg 
 
	  
 
	Profil Igora Ďurišku tu  
 
	  
 
	Článok bol publikovaný na portáli navrchol.sk 
 
	  
 
	Autor: 
	 
	Mgr. Juraj Karas, PhD. / PROefekt 
 
	  
  
  
Zdieľať na Facebooku
 

 Späť     Všetky diela, vrátane publikácií, článkov alebo ich častí uverejnených na
	tejto stránke sú autorskými dielami chránené v zmysle autorského zákona.
	 
	Akékoľvek použitie takéhoto diela alebo jeho časti bez súhlasu autora
	alebo osoby, ktorej náleží právo na udelenie súhlasu s použitím diela,
	rovnako ako neoprávnené zásahy do autorského diela, vrátane neoprávneného
	rozmnožovania a ukladania do pamäťových a elektronických médií, kopírovania,
	distribuovania, prenášania, zobrazovania, uverejňovania, predávania,
	udeľovania povolenia, vytvárania odvodených diel sú protiprávne a
	postihnuteľné v občianskoprávnej alebo trestnoprávnej rovine. 
 
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


