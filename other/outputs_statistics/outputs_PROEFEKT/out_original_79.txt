







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Dve víťazstvá Tomáša Višňovského v Dubnici a Seredi   02.09.2013  
	 
   
 
	Cez víkend sa pretekári PROefekt teamu rozdelili na viacero pretekov. Tomáš Višňovský, Helmut Posch a Ján Allina sa v sobotu zúčastnili Continental Novodubnického maratónu. V nedeľu sa Tomáš Višňovský a ďalší zvereneci PROefektu, Matej Fačkovec a sestry Juhásové, zúčastnili na Cene AB Sereď a starostu obce Dvorníky. Patrik Spanyo štartoval na maďarskom amatérskom pohári v cestnej cyklistike. 
 
	  
 
	Continental Novodubnický maratón, 31.8.2013 Nová Dubnica 
 
	  
 
	V Novej Dubnici sa konal už 19. ročník tradičného MTB maratónu. Na 46 - kilometrovej trati štartovali Tomáš Višňovský, Helmut Posch a Ján Allina. 
 
	  
 
	Tomáš Višňovský prišiel na preteky s cieľom vyhrať a ušetriť čo najviac síl na nedelňajšie preteky v Seredi. Prekaziť mu to chceli Tomáš Doubek (KAKTUS BIKE - team Bratislava), Jakub Vančo (CK Banská Bystrica) , Martin Málek (CK Bradlobike) a ďalší.  
 
	  
 
	Tomáš Višňovský komentuje priebeh pretekov: 
 
	  
 
	"Maratón v sobotu som bral ako tréning. Ťahal som skupinku troch ľudí. Postupne sme sa roztrhali a boli sme dvaja. Na 15km som však zablúdil lebo som nerozoznal šípku od krížika :). Vrátil som sa po dvoch minútach a trocha som si to práskol, aby som sa dostal opäť na čelo. Potom som si išiel svoje až do cieľa." 
 
	  
 
	Tomáš Višňovský zvíťazil pred Tomášom Doubekom a Jakubom Vančom. Helmut Posch dokončil preteky na 6.mieste a Ján Allina na 39. mieste z 281 pretekárov. 
 
	  
 
	 
 
	Pódium 43 - kilometrovej trate 
 
	  
 
	  
 
	 
 
	Helmut Posch na priebežnom 5. mieste pred prvou občerstvovacou stanicou 
 
	  
 
	 
 
	Ján Allina v cieli 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	Cena AB Sereď a starostu obce Dvorníky, 1.9.2013 Dvorníky 
 
	  
 
	Na Slovensku je o preteky cross - country olympic núdza a preto všetkých pretekárov potešil nultý ročník pretekov v Dvorníkoch. Na pretekoch sa zúčastnila časť slovenskej špičky vo všetkých kategóriach, Martin Haring, Milan Barényi, Janka Števková a ďalší. Trať bola náročná technicky, aj svojim výškovým profilom. 
 
	  
 
	Tomáš Višňovský prišiel opäť na preteky s cielom vyhrať a preteky popísal takto: "Na štarte som bol troška nervózny, lebo som nevedel ako som na tom po sobote. Po štarte som sa držal v vpredu a po prvom kole som bol v kategórii prvý. Budoval si náskok a vo štvrtom kole som troška zvoľnil, aby mi nedošlo. V posledom kole som prvý krát pocítil, aké to je dostať nábeh na kŕče a tak som len do bicykloval na prvom mieste. Som rád, že moja forma má stúpajúci charakter a to hlavne vďaka PROefektu a Jurajovej systematickej príprave!" 
 
	  
 
	 
 
	Tomáš Višňovský v technických pasážach trate 
 
	  
 
	Matej Fačkovec dokončil preteky na 4. mieste v kategórii U23. Sestry Juhásové obsadili pódiové umiestnenia, Zuzana dokončila preteky druhá a Andrea tretia. 
 
	  
 
	 
 
	Pódium kategórie U23 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	Maďarský amatérsky pohár - časovka do vrchu, 1.9.2013. Ostrihom 
 
	  
 
	Patrik Spanyo sa zúčastnil v nedeľu na pretekoch v Maďarsku, hneď za slovenskými hranicami,  v Ostrihome. Ako sa mu na nich darilo nám povedal Patrik: 
 
	  
 
	"Konečne pódium, ale som veľmi nespokojný so sebou, takéto preteky by som mal s prehľadom vyhrať, urobil som množstvo chýb pri časovke, ktorých som si vedomý a nabudúce ich odstránim. Trať bola dlhá 26km z toho 16km po rovine a zvyšných 10km do stúpania miestami 8 až 10 %. " 
 
	  
 
	 
 
	Pódium pretekov 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


