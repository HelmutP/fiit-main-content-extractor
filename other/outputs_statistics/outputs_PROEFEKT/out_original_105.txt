







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4085   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Vydarený Svätojurský maratón   12.04.2014  
	 
   
 
	V druhú aprílovú sobotu sa konal tradičný Svätojurský maratón. Pre mnohých to boli prvé preteky v sezóne. Aj od nás sa zúčastnilo viacero bikerov. 
 
	  
 
	 
 
	spoločná foto pred rozjazdením 
 
	  
 
	Organizátori zabezpečili tri dĺžky trate 20, 44 a 75 kilometrov. My sme mali zastúpenie na každej trati. 
 
	  
 
	Zázemie maratónu bolo ako každý rok na futbalovom štadióne, ktorý ponúka pre pretekárov všetko, čo potrebujú. Po rýchlom a bezproblémovom zaprezentovaní a rozjazdení sa na štart 44 kilometrovej trate postavili z PROefekt teamu Tomáš Višňovský, Ľubomír Malich a Helmut Posch. Tréner Juraj Karas nepretekal,  lebo chcel povzbudiť Tomáša Višňovského na trati a pošetriť sily na slovenský pohár na ceste, konaný na druhý deň. Bol si vyskúšať nový 27,5 palcový bike a prešiel si trať 30 minút pred pretekármi aby ho Tomáš v poslednej tretine preteku dobehol a predbehol. Trať bola skvelo označená, regulovčíci boli na svojich miestach a hoc nemal štartovné číslo vždy mu svedomito ukázali správnosť cesty. Tiež pokecal s príjemným personálom na občerstvovačkách, ktoré boli rozložené na širokej palete stolov a teda pripravené na väčší nápor bikerov naraz. 
 
	  
 
	Pred pretekmi tréner Juraj určil Tomášovi taktiku: "Tvojou úlohou je vyhrať a zároveň sa nezničiť aby si ušetril sily na nedeľný dôležitý pretek (Slovenský pohár cross - country)". 
 
	  
 
	Od štartu sa išiel hneď "výživný" kopec. Bikeri sa v ňom roztrhali na "korálky". Tomáš Višňovský sa držal taktiky a išiel na prvom mieste. 
 
	  
 
	Trať sa skladala zo širokých zvážnic, ktoré striedali rýchle a jazdivé zjazdy. Pretekárov preverilo aj niekoľko brodov.  Po hravom zjazde z Bieleho kríža nad Svätý Jur nasledovalo posledné krátke stúpanie, kde už má každý pred očami cieľ. Práve tu sa Tomáš odpútal od Jána Gallika. Do cieľa prišiel s náskokom a zvíťazil. Slubný výsledok dosiahol aj Ľubomír Malich, ktorý skončil v kategórii juniorov na 4.mieste. Helmut Posch skončil na 10. mieste v kategórii aj vďaka výborne zásobeným občerstvovačkám, keďže po asi hodine pretekov stratil flašu. Ďalší zverenec PROefektu Filip Sklenárik, člen reprezentačného družstva SR v MTB XC, sa umiestnil na 2. mieste v kategórii juniorov a 4. mieste v celkovom poradí. 
 
	  
 
	 
 
	Tomáš Višňovský na pódiu typickom pre Svätojurský maratón 
 
	  
 
	Na 75 kilometrovej trati pretekala zverenkyňa PROefektu, domáca bikerka Zuzana Juhásová. Po defekte skončila na 2. mieste. 
 
	  
 
	Prvé preteky dopadli na výbornú, organizátori odviedli svedomitú prácu ako s označovaním trate, prezentáciou pretekárov a všetkým čo musí na takomto podujatí fungovať. Tešíme sa na ďalší ročník, prípadne na Svätojurský blesk, ktorý sa bude konať 6.7.2014. 
 
	  
 
	Celkové výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  jakubzeman  14.04.2014 20:52:35 
			

			 Obsah: Gratulujem Tomas 
			

 
  
			

			 Pridal:  tomasvisnovsky  14.04.2014 22:33:04 
			

			 Obsah: dakujem aj ja tebe tiez len tak dalej TATKO :) !  
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


