

 
	3.9. 2012 sme oficiálne spustili prvý Virtuálny tréningový systém (VTS) na Slovensku. Bude najmä slúžiť na riadenie tréningu, komunikáciu a tvorbu tréningových a pretekových plánov. 
 
	  
 
	 
 
	úvodna stránka systému  
 
	  
 
	Tiež v ňom má zverenec uložené svoje výstupy z diagnostiky trénovanosti, a individuálne tréningové zóny. Trénerovi pomôže pri efektívnejšom riadení tréningu, rozposielanie dát zverencovi, organizácii platieb a v neposlednom rade na rýchlejši prístup k tréningovým plánom a záznamov z naordinovaného tréningu. 
 
	  
 
	 
 
	prehľad tréningových plánov v systéme 
 
	  
 
	Tí majú možnosť ku každej tréningovej jednotke pridať svoj pocit a tiež po každom odtrénovanom týždni zadať únavu. Vďaka tomu sa zlepší spätná väzba zverenca smerom k trénerovi. Tiež majú možnosť kedykoľvek pomocu systému napísať súkromnu správu trénerovi. V prípade, že zverenec bude dlhšie mimo internetu, je možné si tréning zo systému stiahnúť vo formáte excel. 
 
	  
 
	 
 
	systém správ medzi trénerom a zverencov 
 
	  
 
	Momentálne je VTS prispôsobené na riadenie tréningu podľa srdcovej frekvencie, ale nie je problém trénovať i podľa merača výkonu. V budúcnosti by sme radi VTS optimalizovali na obidva spôsoby riadenia tréningu. 
 
	  
 
	 
 
	zobrazenie podrobnosti tréningovej jednotky v systéme 
 
	  
 
	Pri tvorbe VTS PROefektu sme vychádzali zo skúseností zahraničných používateľov takýchto systémov. Dúfame, že kvalita trénerského vedenia na báze tohto systému sa dostane na čo možno najvyššiu úroveň. 
	Priebežne Vás budeme informovať o našich nových poznatkoch z používania a administrácie. 
 
	  
 
	 
 
	návod pre zverencov na používanie systému 
 
	 
	 
	Všetkým zverencom už boli pridelené prihlasovacie údaje a zadaný prvý tréningový týždeň do systému. A tak Vám prajeme príjemné a bezproblémové používanie. Dúfame, že si na novinku rýchlo zvykneme a prinesie nám ešte lepšie výsledky! 
 
	  
 
	Obrázky v plnom rozlíšení stiahnete TU 
 
	  
 
	PROefekt 

