







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Technické novinky zo svetového pohára   30.05.2013  
	 
   
 
	  
 
	K špičkovému športovému výkonu v cyklistike nestačí len výborne vyladený motor organizmu, teda ľudský pohon. Avšak potrebujeme i kvalitný stroj. Na svetových pohároch bývame svedkami najnovších trendov a riešení vo svete bajkov. V tomto článku sa výnimočne pozrieme čo sa dalo v tomto smere vidieť v Novom Meste na Morave. 
 
	  
 
	Kolesá: 29" vládne, ale čoraz častejšie sa objavuje aj 27,5". Už druhú sezónu ho používa napríklad Nino Schurter na svojom Scott, alebo premiérovú sezónu J.A. Hermida na svojej Meride. Oddôvodňujú to svojou nižšiou výškou a lepšiou pohyblivosťou oproti 29" bajkom. 
 
	  
 
	 
 
	  
 
	Pohon: v zásade sa pretekárske pole rozdeľovalo na dva tábory, podľa výrobcov. Tí, ktorí jazdia Shimano používali Shimano XTR 2x10 (38-26 vpredu a 11-36 vzadu ) a tí, ktorí jazdia Sram používali XX1 1x11 (38 vpredu a 10-42 vzadu). 
 
	  
 
	 
 
	  
 
	  
 
	 
 
	  
 
	Šéf technik SRAM nám umožnil v stánku rovnakej značky porovnať zanedbateľný hmotnostný rozdiel medzi 10 a 11 koliečkovou kazetou. Úspora je dosiahnutá zmenšením rozdielov medzi pastorkami, na čo je potrebná tenšia reťaz. 
 
	  
 
	 
 
	  
 
	  
 
	 
 
	  
 
	Podľa technika SRAM by mala vydržať toľko ako iné, ako nám však povedal osobný technik Nina Schurtera v stánku Scott Swisspower "Ninovi reťaz mením raz za týždeň, pre istotu". Holt, kde sa nepozerá na náklady, tak sa to dá :) 
 
	  
 
	 
 
	  
 
	Odpruženie: v drvivej väčšine prevláda hardtail, keďže preteky trvajú v priemere 90 minút, terény nie sú extrémne náročné a stúpania sa idú naplno. Vyskytujú sa však aj lastovičky, napríklad Kulhavý jazdí už 3. sezónu na 29" fulle značky Specialized. Tento rok prešiel na celoodpružený bike aj Cink z tímu Merida. Čo sa týka značiek, prevláda SID od Rock-Shox, zvyšok trhu rozdeľujú Magura, DT Swiss, Fox, Suntour a iní výrobcovia sú približne v rovnakom pomere. 
 
	  
 
	 
 
	  
 
	Pneumatiky: Prevláda nemecká značka Schwalbe, pričom najčastejšie sa na suchých pretekoch používa Racing Ralph alebo kombinácia Rocket Ronu vpredu a Racing Ralphu vzadu. Českí pretekári (okrem Kulhavého a Cinka) používajú českú značku Rubena, s ktorou spolupracujú pri vývoji nových plášťov. Do konca minulej sezóny s touto značkou štartoval aj víťaz prvého preteku svetového pohára v Albstadte Austrálčan Dan McConnel. Po prestupe do Treku však už dnes jazdí na pneumatikách Bontrager. 
 
	  
 
	 
 
	  
 
	Švajčiarsky tím Scott Swisspower pred nejakým časom prešiel na galusky od Belgičana A. Dugasta. Vraj lepšie kopírujú terén a Dugast to pre nich vyrába na zákazku. 
 
	  
 
	 
 
	  
 
	Značky: najviditeľnejšie na tratiach sú globálne koncerny Specialized, Trek, Cannondale, Canyon či talianske Bianchi. Ale medzi značkami nájdete veľa menších, ako napríklad český Superior, nemecký Haibike, Focus či Ghost. 
 
	  
 
	 
 
	  
 
	A ako vníma progres v technike pretekár PROefekt teamu a účastník svetového pohára Tomáš Višnovský? 
 
	  
 
	 
 
	  
 
	A pridávame jednu zaujímavosť na záver: všetci používajú typické cyklistické kraťasy s výnimkou pretekárov tímu Cannondale, Marco Fontana a Manuel Fumic,. Tí obliekajú cyklistické šortky, ktoré zvyknú používať jazdci v Down Hille. 
 
	  
 
	 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


