







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Cez víkend 10 pódiových umiestnení pre PROefekt   09.09.2013  
	 
   
 
	Cez víkend 7.-8.9.2013 sa pretekári PROefektu zúčastnili viacerých pretekov: 19. a 20. kolo Merida Road Cup, časovky na Veľkú Javorinu, MTB maratónu v Pružine a Slovenského pohára v cestnej cyklistike v Spišskej Novej Vsi. 
 
	  
 
	V sobotu sa konal v Pružine MTB maratón a z PROefekt teamu sa ho zúčastnil Tomáš Višňovský a Ján Allina. Tomáš štartoval na dlhšej 40 kilometrovej trati a Ján na kratšej, 25 - ke. Na dlhej trati patril Tomaš Višňovský k adeptom na celkové víťazstvo a najväčšiu konkurenciu mal v skúsenom Milanovi Barényim a Dominikovi Turanovi. Tomáš nedokázal poraziť matadora Milana Barényiho a obsadil druhé miesto v celkovom poradí. Vo svojej kategórii zvíťazil o viac ako 12 minút. Na kratšej trati sa darilovi i Jánovi Allinovi, ktorý dokončil preteky na 2. mieste vo svojej kategórii. 
 
	  
 
	V Pružine Tomáš Višňovský zažil jednu zábavnú situáciu pri registrácii. Organizátorovi diktoval názov tímu takto: "Veľkým PRO, efekt team". Vo výsledkovej listine vznikol pri jeho mene tím s novým názvom :). Nájdete ho vo výsledkoch  
 
	  
 
	 
 
	Tomáš Višňovský víťazom 40km MTB maratónu v Pružine (Muži 20 - 29 rokov) 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	7. septembra sa konalo aj 19. kolo Merida Road Cupu v Lábe ako časovka dvojíc. PROefekt team mal v pretekoch len jednu dvojicu v zložení Helmut Posch a Patrik Spanyo. Viac nám o pretekoch povedal Helmut: 
 
	  
 
	"Na štart sme sa s Patrikom postavili s malými dušičkami, pretože ja som nevedel, čo mám od seba v pokročilej sezóne bez poriadnej zimnej prípravy čakať a Patrikova výkonnosť je stále opvplyvnená jeho zranením v strede sezóny. Navyše náš jediný časovkársky materiál spočíval v jedných nádstavcoch na riadidlá. Mne sa išlo v pretekoch dobre a Patrik tiež vyzeral, že nemá s nastoleným tempom žiaden problém. Spravili sme viacero chýb, ako príliš dlhé striedania alebo príliš veľká opatrnosť v dedinách. Do cieľa sme ale bez problémov dorazili spolu. V cieli sme s prekvapením zistili, že sme sa umiestnili na 2. mieste. Vzhľadom na to, že som s Patrikom odjazdil v sezóne iba veľmi málo pretekov, bol takýto výsledok v časovke dvojíc dobrý." 
 
	  
 
	Pretekov sa zúčastnil aj ďalší zverenec PROefektu Ľuboš Oravec a obsadil 2. miesto v kategórii. 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	V Spišskej Novej Vsi sa konal v sobotu Slovenský pohár v cestnej cyklistike. Časovka do vrchu zo zverencov PROefektu najviac vyhovovala Milanovi Kováčovi, ktorý ako prvoročák obsadil výborné 3. miesto. 
 
	  
 
	 
 
	Milan "Miňo" Kováč si ide pre tretie miesto v časovke do vrchu 
 
	  
 
	Časovka do vrchu sa nekonala len v Spišskej Novej Vsi, ale aj na Veľkej Javorine. Ťažké stúpanie k vysielaču s dĺžkou a prevýšením vzbudilo v pretekároch rešpekt. Zo zverencov PROefektu sa pretekov zúčastnil Erik Vicen. Erik prekvapil všetkých súperov vo svojej kategŕii a dokázal na vrchol výjsť najrýchlejšie. Potvrdil tak svoje zlepšenie pod taktovkou PROefektu a do konca sezóny ma ešte priestor svoje kvality potvrdiť. 
 
	  
 
	 
 
	pódium, Erik Vicen 1. miesto 
 
	  
 
	Viac o pretekoch sa môžete dočítať TU 
 
	Kompletné výsledky nájdete TU 
 
	  
 
	V nedeľu sa konal tradičný pretek "Rapiďák" ako 20. kolo Merida Road Cupu v Perneku. Pretekov sa zúčastnili z PROefekt teamu Patrik Spanyo, Pavol Dohnányi, Tomáš Višňovský, Filip Lašút a Helmut Posch. Na preteky prišli aj zverenkyne PROefektu sestry Juhásové, Milan Tupý a Ľuboš Oravec. 
 
	  
 
	"Juhasky" si prišli pre ďalšie skúsenosti z cestných pretekov a tiež sa pokúsiť o víťazstvo. V sobotu obe absolvovali konco-sezónnu diagnostiku a ich zlepšenie nadchlo trénera a aj ich samotné (profil Andrei TU, profil Zuzany TU). Najväčšiu konkurenciu mali v Tatiane Jasekovej (Suncycle Time Cycling Team Partizánske). V záverečnom stúpaní po strhujúcom súbojí zvíťazila naša Andrea pred Tatianou Jasekovou. Pár metrov za ňou došla sestra Zuzana, na 3. mieste. 
 
	  
 
	 
 
	Andrea so Zuzanou v Perneku 
 
	  
 
	V kategórii mužov sa pretekárom PROefektu nepodarilo dobre umiestniť. Najprv Helmut Posch musel ukončiť kvôli technickým problémom preteky a potom pretekári PROefekt teamu takticky nezvládli záver pretekov. Najlepšie postavený pretekár PROefekt teamu v cieli bol na 8. mieste Pavol Dohnányi. 
 
	  
 
	Milan Tupý dokázal v kategórii Masters D zvíťaziť a upevnil si tak svoje líderské postavenie v Merida Road Cupe. Výborné bolo tiež 2. miesto Ľuboša Oravca vo svojej kategórii. 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


