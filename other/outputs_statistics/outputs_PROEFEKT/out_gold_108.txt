

 
	V marci 2013 vznikol cyklistický tím "PROefekt team". Tím pozostáva z licencovaných jazdcov a hobby športovcov. 
 
	V tomto článku sumarizujeme výsledky, ktoré stoja za prvou sezónou pretekárov s licenciami na riadnych pretekoch. 
	  
 
	Filozofia tímu spočíva v koncepčne riadenom tréningu vychádzajúcom zo športovej diagnostiky. Trénerom, diagnostikom a aktívnym pretekárom tímu je Juraj Karas. Tréning pretekárov denno-denne riadi pomocou Virtuálneho tréningového systému (VTS), telefónu a osobného kontaktu. 
 
	  
 
	 
 
	 
	Pretekári PROefektu sa už v prvom roku pôsobenia zúčastňovali kompletného Slovenského pohára v MTB XCO (cross country), XCM (horský maratón) a Slovenského pohára v cestnej cyklistike. Taktiež prestížnych pretekov Majstrovstiev Slovenska v cestnej cyklistike, Majstrovstiev sveta v MTB maratóne, Majstrovstiev sveta a Európy vo 4X a Svetového pohára v MTB XCO. 
 
	  
 
	 
 
	Andrea Juhásová 1. a Zuzana Juhásová 2. na M-SR XCM 
 
	  
 
	Najvýraznejšie výsledky: 
 
	3. miesto M-SR v časovke jednotlivcov v kategórii kadet (15-16 roční) (cestná cyklistika) 
 
	3. miesto M-SR v časovke do vrchu v kategórii kadet (15-16 roční) (cestná cyklistika) odkaz na článok  
 
	1. miesto M-SR v časovke jednotlivcov v kategórii Masters D (cestná cyklistika) odkaz na článok 
 
	1. miesto M-SR v preteku s hromadným štartom v kategórii Masters D (cestná cy klistika) odkaz na článok 
 
	1. miesto celkové hodnotenie seriálu cestných pretekov Merida Road Cup v kategŕoii Muži (cestná cyklistika) 
 
	1. miesto celkové hodnotenie v CMS v kategórii Ženy (horská cyklistika) 
 
	3. miesto Slovenský pohár v XCO, Smolenice v kategórii Muži Elite (horská cyk listika) odkaz na článok 
 
	2. miesto M-SR v XCO v kategórii Ženy (horská cyklistika) odkaz na článok 
 
	3. miesto M-SR v preteku s hromadným štartom v kategórii Ženy (cestná cyklistika) odkaz na článok 
 
	1. miesto M-SR v XC eliminator v kate górii Ženy (horská cyklistika) odkaz na článok 
 
	1. miesto M-SR v XCM v kategó rii Ženy (horská cyklistika) odkaz na článok 
 
	1. miesto M-SR vo 4X v kategórii  Muži Elite odkaz na článok 
 
	6. miesto Majstrovstvá sveta vo 4X v  kategórii Muži Elite odkaz na článok 
 
	  
 
	 
	Športovci PROefektu zbierali pravidelne úspechy na viacerých frontoch, čo sa disciplín a kategórii týka. Celkový počet víťazstiev sa zastavil na čísle 72. Druhých miest bolo 33 a tretích 26. 
 
	Celkovo sa teda pretekári PROefektu v prvú sezónu postavili 131-krát na pódiové umiestnenie. 
 
	  
 
	 
 
	  
 
	Záverom musíme dodať, že športovci z naších radov v tejto sezóne dosiahli výkonnostné a najmä výsledkové osobné maxima svojích športových životov. 
 
	  
 
	 
 
	Marek Peško na Majstrovstvách sveta vo 4X 
 
	  
 
	Všetkým, ktorí sa rozhodli podstúpiť nie vždy ľahký, prísne koncepčne riadený tréning, ďakujem. 
 
	  
 
	Taktiež veľká vďaka patrí naším sponzorom/partnerom: 
 
	ACROSS, COLUMBIA 
 
	SPOL KOVAC, SQUEEZY SPORTS NUTRITION 
 
	ELTECH CONTROL, ALPHA OMEGA SPED a 3PACK 
 
	  
 
	Už teraz sa tešíme na výzvy a výsledky v nasledujúcej sezóne.  
 
	  
 
	P.s. V ďalšiom článku budeme individuálne hodnotiť každého pretekára s podrobne zmapovanou výkonnosťou v priebehu celej sezóny. - článok nájdete TU 
 
	  
 
	Článok bol publikovaný na portáli MTBiker.sk 
 
	  
 
	PROefekt 

