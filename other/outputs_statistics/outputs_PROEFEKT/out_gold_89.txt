

 
	  
 
	 
		Začína zima a spolu s ňou nutné tréningy vo vnútri. Jedným z prostriedkov tréningu cyklistickej, teda špeciálnej vytrvalosti, „indoor“ sa využíva tiež SPINNING. To, čo toto slovo znamená už v dnešnej dobe zaiste každý veľmi dobre vie a ozrejmovať to nie je nutné. Na adresu fenoménu menom spinning bolo do dnešného dňa popísané veľa rôznych pohľadov, mýtov, názorov a múdrosti.. Avšak práve týmto príspevkom by som rád do tejto problematiky vniesol jednotný názor a poukázal na pozitíva i negatíva. 
	 
		  
	 
		Spôsobilosť vyjadrovania sa k tejto téme podtrhujú skutočnosti, že som od roku 2009 vyškoleným inštruktorom, ktorý dva roky precvičoval v 5 rôznych fitness centrách. Jedno z nich bolo vlastné, v ktorom som si vyškolil inštruktorov. Tiež som o spinningu robil výskum v rámci diplomovej práce. Do dnešného dňa, kedy mi už na precvičovanie neostáva čas, spinningový bicykel príležitostne využívam na tréningový proces svoj a tiež zverencov.      
	 
		     
	 
		 
	 
		     
	 
		  
	 
		Aký je skutočný rozdiel medzi šliapaním na spinningu a bicykli?   
	 
		     
	 
		Rotujúce koleso na spinneroch váži od 11 do 16 kg a je reťazou (eventuálne popruhom) prepojené priamo s prevodníkom. Vďaka tomu je zaistený pevný prevod - „furtšlap“. Záťaž sa pridáva otočným koliečkom na ráme, ktorým regulujeme pritiahnutie brzdových plôch k rotujúcemu kolesu. Pri nastavení ľahšej záťaže sa pedále naďalej točia aj keď do nich prestaneme tlačiť. Na tento neprirodzený chod reagujú naše svaly nôh rozdielne ako pri šliapaní na bicykli. Hamstring (zadná strana stehna) je nútený vykonávať excentrické kontrakcie. Kvadriceps (predná strana stehna) zase nemusí vyvíjať až takú veľkú silu, keďže sila pôsobiaca na pedále je plynulejšie rozložená. Naopak pri jazde na bicykli sa viac namáha kvadriceps stláčaním pedálu dole a flexory bedrového kĺbu pracujú pri prekonávaní mŕtveho bodu. Práve efektívne prekonávanie mŕtveho bodu zodpovedá za správnu a žiaducu techniku šliapania (viď obrázok), ktorá sa tým pádom na spinningu až tak dobre trénovať nedá. Teda hamstringy pracujú intenzívnejšie na spinneri ako na bicykli. Naopak kvadriceps je na spinningu zaťažovaný menej. Dalo by sa povedať, že na spinneri je šliapanie jednoduchšie a ľahšie. Kvadriceps je najmocnejším svalom ľudského tela a na spinne mu až tak zabrať nedáme. To má za následok, že po intenzívnom spinningu do takej miery nebolia nohy, ako po rovnako intenzívnom bicyklovaní. 
	 
		  
	 
		   
	 
		   
	 
		Rozdielnosť techniky šliapania u cyklistu a necyklistu, info TU 
	 
		  
	 
		  
	 
		Čo rozdiely vnútornej odozvy organizmu na zaťaženie na bicykli a spinningu?   
	 
		     
	 
		Na spinningovom bicykli má otáčavý moment zotrvačného kolesa za následok prirodzene vyššiu frekvenciu šliapania. Je všeobecne známe, že pri nej sa „šetria“ svaly a viac zaťažuje srdcovocievny systém. To vyvoláva vyššiu srdcovú frekvenciu (SF) pri tom istom výkone. Vyššia SF taktiež spôsobuje nedostatočné odparovanie potu z kože, čo zapríčiňuje výraznejšie potenie. Preto je dôležité prijímať tekutiny približne o 50% viac ako sme zvyknutý pri tréningu vonku. Taktiež je žiaduce ochladzovať miestnosť a utierať pot z kože pre zamedzenie prehriatia.    
	 
		Sila pôsobiaca na pedále je plynulejšie rozložená, vďaka čomu sa viac zapájajú pomalé oxidatívne svalové vlákna. To má za následok využívania prevažne tukov ako energetického zdroja pre svalovú prácu. Naopak pri šliapaní na bicykli sa viac zapájajú rýchle oxidatívne a pri vyššej intenzite rýchle glykolitické svalové vlákna, vďaka čomu spaľujeme viac sacharidov. Ich nedostatok znižuje zásoby svalového glykogénu a to pri mierne nižšom úsilí. Avšak pozor, o konečnom energetickom výdaji a použitých zdrojov pre svalovú prácu aj tak primárne rozhoduje intenzita a dĺžka tréningu.   
	 
		     
	 
		  
	 
		Na čo si treba pri spinningu dávať pozor?   
	 
		     
	 
		Niektorí inštruktori zvyknú zo spinningu robiť aerobic na bicykloch, či spartakiádu. Takíto prístup môže byť pre väčšinu klientov zaujímavý a zábavný. Avšak tiež môže mnohým z nás uškodiť. Na mysli mám najmä extrémne pohyby vychyľovania sa nad bicyklom vpred a vzad. Mnoho ľudí sa mi sťažovalo, že ich zo spinnnigu rozboleli kolená. Dôvodom je ono vychyľovanie spôsobujúce zlú biomechaniku záberovej sily do pedálov spolu s neprirodzeným rotačným chodom spinnera. My cyklisti sme v ohrození ešte viac, keďže pri silovom nízko kadenčnom šliapaní dokážeme do pedálov pôsobiť oveľa vyššou silou ako iní. A tým pádom môže mať nesprávne vychyľovanie kolena fatálne následky. 
	 
		  
	 
		   
	 
		 
	 
		Nesprávna biomechanika šliapania :) 
	 
		     
	 
		  
	 
		Prečo spinning nie:   
	 
		- v prípade neklimatizovanej miestnosti vydýchaný a teplý vzduch (klimatizácia tiež nie je ideálne riešenie, najmä v prípade, že ju často nečistia..) 
	 
		- len s ťažkosťami sa priblížime svojmu ideálnemu posedu z vlastného bicykla, tiež niekomu nemusí vyhovovať mäkké sedlo 
	 
		- pri pokynoch inštruktora sa len ťažko realizuje vlastný tréning, v prípade ich ignorovania nie je z vás nadšený 
	 
		- nesprávne pohyby na spinneri môžu uškodiť 
	 
		- presne stanovená dĺžka a čas lekcie vždy nemusí vyhovovať (zvyčajne býva pre tréning krátka)  
	 
		- furtšlap s ťažkým zotrvačníkom nie je ideálny na zlepšenie techniky šliapania a teda tréningu reálneho zlepšovania výkonu na bicykli   
	 
		- vlhká a uzavretá miestnosť zvyšuje pravdepodobnosť chytenia virózy od nie úplne zdravého spolu cvičenca 
	 
		     
	 
		  
	 
		Prečo spinning áno:    
	 
		- tréning v kolektíve ubieha rýchlejšie a zábavnejšie, navyše sa dá často na čo pozerať.. :) 
	 
		- možnosť skombinovať so silovým tréningom v posilňovni bez presunu domov 
	 
		     
	 
		  
	 
		Ak spinning, tak koľko, ako a prečo?   
	 
		Pri využívaní spinningu v zimnej príprave cyklistu môžeme zvoliť viaceré prístupy. Z hľadiska toho, že lekcia zvyčajne trvá do 1 hodiny, môžeme ju poňať pestro, svižne a intenzívne „prevetraním“ všetkých energetických systémov. Ale pozor, organizmus na to musí byť pripravený tým, že to nie je vaša jediná cyklistická aktivita v týždni! V ostatných tréningoch je nutné rozvíjať príslušné schopnosti pripadajúce na dané obdobie prípravy. Taktiež takéto intenzívne lekcie by nemali byť viac ako dve do týždňa. V druhom prípade je možné spinning využiť ako doplnok pred, alebo po inej pohybovej aktivite. Avšak tu už treba dodržiavať predpísanú intenzitu, ktorá je zväčša nižšia.. 
	 
		  
	 
		   
	 
		   
	 
		     
	 
		  
	 
		Záverom musím povedať, že osobne mám spinning veľmi rád a využívam ho na prípravu. Avšak netreba stavať len na ňom. Predsa len v zime je dôležitá pestrosť, správne dodržaná intenzita a koncepčne naskladanie tréningových jednotiek rôzneho druhu. Taktiež treba mať na zreteli, že hodiny odkrútené na spinningu nie sú to isté čo na bicykli. Práve rozdielnosť techniky šliapania má za následok skutočnosť, že po zimnej spinningovej príprave sa pri jazde na normálnom bicykli vonku necítime tak ako by sme si priali.. Avšak pozor, tento fenomén netreba zovšeobecňovať aj na tréning realizovaný na každom cyklistickom trenažéri. Niekedy v budúcnosti sa trenažérom pozrieme na zúbok, a tiež si ich porovnáme medzi sebou, tak aj s cyklistickými valcami. 
	 
		     
	 
		Článok bol publikovaný na portáli MTBiker.sk 
	 
		  
	 
		Autor: 
		 
		Mgr. Juraj Karas, PhD. / PROefekt 
 
 
	  

