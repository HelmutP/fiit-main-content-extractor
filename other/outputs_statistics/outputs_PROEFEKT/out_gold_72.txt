

 
	   Veľa rekreačných a hobby športovcov sa domnieva, že v určitých prípadoch už zlepšenie nie je možné. Väčšinou sa jedná o okolnosti, kedy človek dlhoročne športuje, nepatrí už k najmladším alebo len jeho tréning nadobudol stereotyp. Áno, väčšina z nás si povie: „Možno by som sa i zlepšil, ale musel by som na tom bicykli sedieť častejšie a dlhšie a to už ja nechcem, nemôžem, nedá sa mi“. Tento postoj je pochopiteľný, veď na svete neexistuje len bicykel. Aj keď pre niektorých z nás znamená veľa. Som toho názoru, že každý bez rozdielu veku a výkonnosti sa môže pri rovnakom časovom úsilí, teda rovnakom objeme tréningu na aký bol zvyknutý, správne nastaveným tréningom zlepšiť. 
 
	  
 
	  
 
	   Ako príklad uvediem zverenca Igora Ďurišku. Igor je od mladosti telom aj dušou športovec. Dlhé roky sa venuje najmä cyklistike, cez zimu bežeckému lyžovanie a skialpu. Jeho športová výkonnosť ja na vysokej úrovni a patrí ku širšej špičke v kategórii veteráni 50 – 59 rokov v horských maratónoch. Keď sa mi dostal pod ruku, bol som prekvapený jeho zvedavosťou, vášňou a zapálením pre tréning. Vycítil som z neho prirodzenú inteligenciu a záujem o možnosť zefektívnenia svojej prípravy. Podstúpil vyšetrenie laktátovej krivky, pri ktorom som mu zistil prahové i maximálne parametre v absolútnych i relatívnych „wattoch“. Z nich som uvidel, že jeho základná aeróbna vytrvalosť v oblasti aeróbneho prahu (AP) je na výbornej úrovni, no mierne zaostávala oblasť anaeróbneho prahu (ANP). Avšak pozor, maximálny aeróbny výkon (VO2max), vykazoval veľmi dobré hodnoty. To svedčí o tom, že Igor má vďaka dlhoročnému vytrvalostnému tréningom rozvinutý potenciál na naozaj slušnej úrovni. Tiež to, že sa vie húževnato zaťať a zabrať aj v nepríjemnej intenzite nad ANP. Z týchto zistení som vyvodil, na čom bolo treba systematicky zapracovať. Za 4 mesiace tréningu podstúpil množstvo cielených intervalových tréningov na rozvoj jeho slabšej stránky, ANP.  Igorova tréningová morálka je vynikajúca, mohol by byť príkladom mnohým mladším športovcom. Taktiež vie počúvať svoje telo, a pri vyššej pracovnej vyťaženosti, či únave z tréningov si vedel v správny čas prípravu uspôsobiť a dať si voľno. Igorove dojmy a výsledky z pretekov nám dávali čiastkovú odozvu, že ideme správnou cestou. Chválil si pocit z jazdy a na maratónoch si vylepšoval umiestnenia. Ja som v tomto skôr skeptický a tak si vždy ešte počkám na nasledujúcu diagnostiku, kde to už vidím čierne na bielom. Po jeho otestovaní a vyhodnocovaní laktátovej krivky ma zaliala obrovská radosť, keď som videl to enormné zlepšenie na ANP. 
 
	  
 
	  
 
	 
 
	Učebnicový posun laktátovej krivky doprava u Igora Ďurišku 
 
	  
 
	  
 
	   Zlepšili sa aj ostatné parametre, ale práve ANP, jeho slabina, slabinou už nie je. Išli sme si sadnúť a dopriali si zaslúženú pizzu. Ale nezaspali sme na vavrínoch a ďalším tréningom sa posúvame zase o krok vpred. A tak len pevne verím, že čoskoro sa dostaví aj „bedňa“. I keď najdôležitejší je dobrý pocit z tréningu a radosť z každodenného zlepšovania sa. 
 
	  
 
	Nedá mi tu nenapísať moje osobné motto: 
 
	„Zmysel života nie je cieľ, ale spôsob akým ten život vedieš, akou cestou ten cieľ dosiahneš“.           
 
	  
 
	  
 
	Zlepšenie za 4 mesiace systematického tréningu 
 
	AP:  0,37 W/kg 
 
	ANP:  0,84 W/kg 
 
	Max. výkon:  0,12 W/kg 
 
	  
 
	Profil Igora Ďurišku tu  
 
	  
 
	Článok bol publikovaný na portáli navrchol.sk 
 
	  
 
	Autor: 
	 
	Mgr. Juraj Karas, PhD. / PROefekt 
 
	  

