







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Víťazná šnúra PROefekt teamu pokračuje   29.04.2013  
	 
   
 
	  
 
	PROefekt team sa v sobotu 27.4. zúčastnil MTB maratónu v Zlatníckej doline pod názvom Kráľ Záhoria. Pretekári PROefekt teamu už od začiatku sezóny preukazujú slušnú výkonnosť a preto sa to isté od nich očakávalo aj teraz. 
 
	  
 
	 
 
	Helmut Posch a Tomaš Višňovský pred rozjazdovaním 
 
	  
 
	Na štart sa postavil Tomáš Višňovský, Helmut Posch, Michal Aftanas a sestry Juhásové. Zuzana Juhásová štartovala na 25 kilometrovej trati a ostatní na 50 - tke.  
 
	  
 
	Ostrý štart bol až po niekoľkých stovkách metrov od areálu Zlatníckej doliny. Od začiatku preteku sa vytvorila početná skupina favoritov v ktorej nechýbal ani náš Tomáš Višňovský. V skupinke boli aj iní kvalitní pretekári ako Martin Škopek, Jan Gállik (obaja Kaktus bike team Bratislava), Dominik Turan (CK OUTSITERZ NOVATEC BRATISLAVA) alebo Peter Jež (CK aluplast team). 
 
	  
 
	 
 
	Tomaš Višňovský po štarte vo velkej skupine 
 
	  
 
	Táto skupina jazdila veľkú časť preteku pohromade. Počas pretekov sa Tomáš javil jednoznačne najsilnejší, čo aj potvrdil. Do cieľa prišiel prvý s takmer dvojminútovým náskokom a s bicyklom nad hlavou. Šťastný v cieli preteky ohodnotil takto: 
 
	  
 
	"Dnešok hodnotím veľmi dobre. Podarilo sa mi skĺbiť fyzickú, technickú a taktickú stránku, čoho výsledkom je skvelý výkon v silno obsadenom maratóne". 
 
	  
 
	 
 
	Tomaš Višňovský so svojim víťazným gestom 
 
	  
 
	Pre Tomáša je to druhý vyhratý MTB maratón z dvoch. Toto naznačuje slušný progres a sľubný potenciál smerom ku štartom na medzinárodnej scéne. Už túto stredu sa predstaví na pretekoch v Čechách. Jeho ďaľšie kroky budeme podrobne mapovať. 
 
	  
 
	 
 
	Michal Aftanas bojuje s traťou 
 
	  
 
	Michal Aftanas a Helmut Posch nakukli do najlepšej desiatky v kategórii a dokončili preteky na 7. resp. 8. mieste. Veľkým pozitívom sú ich pocity z pretekového tempa a zlepšené výkony oproti minulotýždňovému víkendu (viac TU) .  Helmut nám priblížil svoje vystúpenie takto: 
 
	  
 
	"Na štarte som urobil veľa chýb a štartoval som z cca 50.miesta. Musel som vynaložiť veľké úsilie na dotiahnutie sa medzi prvých, čo ma stálo veľa síl. V prvom kole som to aj pocítil a mal som slabé nohy. V druhom kole som sa ale spamätal a cítil som sa výborne. Takýto stav ale nie je prekvapujúci, vzhľadom na to, že sme v tréningu ešte nepracovali na zaťažení vo vysokých intenzitách. Cítim, že príprava ide tým správnym smerom, čo mi potvrdzujú aj zlepšujúce sa výkony na pretekoch a tréningoch." 
 
	  
 
	 
 
	Helmut Posch v rýchlom zjazde k cielu 
 
	  
 
	Veľké očakávania boli aj od sestier Juhásových. Zuzana, štartujúca na 25 kilometrovej trati zvíťazila s obrovským nádskokom viac ako 9 minút a preukázala, že patrí medzi slovenskú špičku v ženskej horskej cyklistike. 
 
	  
 
	 
 
	Zuzana Juhásová v technickom výjazde neďaleko pred cieľom 
 
	  
 
	Jej sestra Andrea sa snažila napodobniť Zuzanu a to sa aj podarilo. Vyhrala s rozdielom triedy a druhá česká pretekárka v cieli inkasovala strátu viac ako 10 minút. 
 
	  
 
	 
 
	Andrea Juhásová zdoláva všetky nástrahy trate v sedle 
 
	  
 
	Budúci víkend sa viacerí ukážeme na známom MTB maratóne na strednom Slovensku s názvom Vlkohron. 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


