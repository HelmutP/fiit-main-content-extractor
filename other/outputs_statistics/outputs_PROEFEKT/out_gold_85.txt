

 
	22. – 29. septembra 2013 sa konali pre Slovensko zatiaľ historicky najúspešnejšie Majstrovstvá sveta v cestnej cyklistike. Ich dejiskom bola talianska Florencia. Profesionálne Majstrovstvá sveta sa v Taliansku konali už po dvanástykrát, v toskánskej metropole však mali svoju premiéru. Florencia je svojou históriou a architektúrou jedným z najznámejších miest Európy a v spojitosti so zvlneným terénom v okolí mesta to bol zaujímavý výber miesta pre túto prestížnu udalosť. 
 
	  
 
	 
 
	  
 
	  
 
	 
 
	Trať preteku s hromadným štartom viedla aj okolo gotickej katedrály Santa Maria del Fiore, ktorá je štvrtým najväčším chrámom na svete 
 
	  
 
	Preteky s hromadným štartom 
 
	  
 
	Záverečný deň MS patril už tradične pretekom s hromadným štartom kategórie Elite. Štart bol v hlavnom meste talianskej provincie Toskánsko – Lucca, odkiaľ sa mal pelotón presunúť na kopcovité okruhy vo Florencii, kde sa nachádzal aj cieľ preteku. Profil trate bol za posledné roky asi najkopcovitejší. Cyklisti absolvovali 272,2 kilometrov, pričom prevýšenie predstavovalo 3700 metrov, z ktorých sa väčšina nachádzala v druhej polovici preteku. Okruh vo Florencii sa začínal takmer 5 kilometrovým stúpaním do mestečka Fiesole. Najťažšie pasáže  sa nachádzali v posledných dvoch kilometroch stúpania. Za vrcholom stúpania nasledoval pomerne technický zjazd, po ktorom prišlo na rad asi 600 metrové, veľmi strmé stúpanie po ulici Via Salviati. Posledné kilometre sa jazdili v uliciach Florencie, s veľmi dlhou cieľovou rovinkou. 
 
	  
 
	  
 
	 
 
	 Pohľad na nekonečnú cieľovú rovinku, kde... 
 
	  
 
	 
 
	...deň pred vyvrcholením šampionátu oslavovali holandskí fanúšikovia zlato pre Marianne Vos 
 
	  
 
	Charakter trati naznačoval, že sa majstrom sveta môže stať po dlhých rokoch vrchár, ale dobré šance sa dávali aj klasikárom „ardénskeho“ typu, či nezničiteľnému Cancellarovi. Taliansky tréner Paolo Bettini pred pretekmi upozorňoval na silu Petra Sagana, čo potvrdil aj tréner tímu Cannondale Paolo Slongo vyjadrením, že na MS ešte nikdy nebol Sagan tak dobre pripravený. Podľa Cancellaru by zase Sagan vzhľadom na absenciu na Vuelte, nemal zvládnuť dĺžku trate. Z vrchárov sa najväčšie šance dávali Valverdemu, Rodriguezovi a najmä domácemu miláčikovi, Vincenzovi Nibalimu. 
 
	Už niekoľko dní pred konaním preteku napovedali predpovede počasia, že Taliansko zasiahnu výdatné zrážky, čo sa napokon potvrdilo. Vďaka technickej náročnosti trate bolo od začiatku preteku jasné, že o dramatické momenty nebude núdza. Prvých 100 kilometrov bolo oproti druhej polovici závodu relatívne pokojných. V nich sa vytvoril únik dňa s cyklistami Raaffa Chtioui (Tunisko), Bartosz Huzarski (Poľsko), Jan Bárta (Česko), Yonder Godoy (Venezuela) a Mathias Brandle (Rakúsko).  Na MS je dobrým zvykom, že takéto úniky závod nerozhodnú a výnimkou nebola ani Florencia. Pelotón úniku dožičil najvyšší náskok okolo 8 minút a tak bolo jasné, že do boja o majstra sveta sa zapoja favoriti z pelotónu. 
 
	Spolu s cyklistami prišiel do Florencie hustý lejak a už v prvých okruhoch postihli pády, či technické problémy mnohých pretekárov. Žiaľ sa im nevyhli ani členovia slovenskej reprezentácie. Peter Sagan sa taktiež porúčal na zem a to hneď v prvom kole. Musel meniť koleso a jeho strata sa vyšplhala takmer na dve minúty. Súčasne s problémami Sagana pelotón výrazne zrýchlil, na únik stiahol takmer okamžite dve minúty a tak dostať sa späť do pelotónu vyžadovalo veľkú dávku úsilia. Počas pretekov vládol obrovský chaos a pád striedal pád. My sme sa nachádzali presne 150 metrov pred cieľom s výhľadom na dve veľkoplošné obrazovky, ktoré (asi kvôli dažďu) striedavo vypadávali. Hustý lejak, všade rozprestreté dáždniky a technické problémy usporiadateľov sťažovali aj divákom na trati sledovanie priebehu samotného preteku. 
 
	  
 
	 
 
	            Veľkoplošná obrazovka, ktorú „mohli“ sledovať diváci v cieľovej rovinke 
 
	  
 
	Bolo ale evidentné, že Peter Sagan sa v čelnej skupine nenachádza, čo u niektorých fanúšikov iných cyklistov vyvolávalo miestami až extázu (najmä u skupiny fanúšikov Nibaliho hneď vedľa nás J). Po technických problémoch sa Sagan vôbec neobjavoval na medzičasoch. V tých chvíľach to so slovenskými nádejami nevyzeralo ružovo. Po prejazde cieľom sme si ho však všimli v niektorej z menších skupín, s dosť výraznou stratou. Vzhľadom na to, že do záveru zostávala ešte veľká porcia kilometrov sme dúfali, že sa na čelo preteku vráti. Našťastie náš najlepší cyklista potvrdil svoju výnimočnosť a skutočne sa prepracoval na chvost čelnej skupiny, kde ale vzápätí prišiel ďalší pád, ďalšie doťahovanie a čerpanie drahocennej energie. Prvá polovica preteku sa pre náš tím vyvíjala naozaj zle, keď všetkých cyklistov okrem Patrika Tybora a Mateja Jurča postihli pády. Tybora však vyradili technické problémy. 
 
	Nakoniec sa Petrovi Saganovi podarilo definitívne dostať na popredné priečky pelotónu a tempo sa mierne spomalilo. Na čele pelotónu bola už len dvojica Huzarski a Bárta, ktorých si pelotón držal na bezpečnú vzdialenosť. V hlavnej skupine došlo s pribúdajúcimi kilometrami k únikom Georga Preidlera (Rakúsko) a Wilc Keldermana (Holandsko), nasledovali ich Cyril Gautier (Francúzsko) a Giovanni Visconti (Taliansko). Najlepšiu výkonnosť z tejto štvorice preukázal domáci miláčik Visconti, ktorý sa dotiahol na vedúceho Huzarskeho. 
 
	  
 
	 
 
	Visconti a Huszarski na čele preteku, v pozadí druhá veľkoplošná obrazovka 
 
	  
 
	V cieľovej rovinke nastalo v radoch talianskych fanúšikov obrovské nadšenie, ktoré však bolo vzápätí vystriedané obrovským sklamaním. Na zemi sa totiž ocitol Vincenzo Nibali a Luca Paolini. Nálada talianskych fanúšikov sa menila ako počasie a keď sa Nibali do pelotónu zázračne vrátil, v oblasti cieľa to vyvolalo zimomriavky navodzujúce skandovanie Vincenzo, Vincenzo... . 
 
	  
 
	 
 
	Napätá atmosféra na hlavnej tribúne počas predposledného kola 
 
	  
 
	 
 
	Nájdi Sagana na tejto fotografii :) 
 
	  
 
	Posledný prejazd Fiesole znamenal drvivý nástup vrchárov. Prvý nastúpil Nibali, ktorého zachytil Rodríguez, za nimi nasledovala trojica Uran, Valverde a Faria da Costa. My sme so sklamaním sledovali, že Peťo Sagan na tento únik už nedokázal reagovať a sen o zlatej medaile sa nám veľmi rýchlo začal rozplývať. Urana postihol nepekný pád a tak stratil šancu na úspech. Na čele sa teda vytvorila štvorica pretekárov. Najťažšiu úlohu mal asi Nibali, ktorému nikto nechcel striedať. V zjazde z Fiesole nastúpil Rodriguez, sťahovať musel Nibali. Na vrchole Via Salviati bol odstup Purita od trojice okolo štyroch sekúnd. Trojici prenasledovateľov sa nakoniec podarilo Španiela dostihnúť. Ten sa s tým ale nechcel zmieriť a na poslednom krátkom stúpaní tri kilometre pred cieľom opäť úspešne zaútočil. Nibalimu samozrejme zase nikto nestriedal. Costa sa 1500 metrov pred páskou rovno pokúsil o únik a nikto z dvojice ho nenasledoval. Vďaka ušetreným silám a možno aj lepším tempárskym schopnostiam Portugalčan Purita nakoniec dobehol. Stalo sa tak necelý kilometer pred cieľom, čo muselo byť pre Rodrigueza veľmi frustrujúce. Vyčerpaný Purito sa začal obzerať a komunikovať s Costom, ktorý tesne pred cieľom nastúpil a víťazstvo si už nenechal vziať. Nibaliho prešpurtoval v závere Valverde a tak bolo u domácich, ako aj u Španielov badať veľké sklamanie. 
 
	Rui Costa využil to, že nebol pasovaný za najväčšieho favorita a v závere z celej štvorice vynaložil najmenej energie. Piate miesto obsadil Grivko, tesne pred skupinou klasikárov na čele s aj tak fantastickým Saganom. Už z diaľky bolo vidieť, že Peťo si kontroluje skupinu na jej čele, a ani v závere nikomu nedovolil dostať sa pred neho. Z nášho pohľadu je škoda, že si režisér prenosu nevšímal, čo sa deje za vedúcou štvoricou. Nikto by asi nebol prekvapený, keby väčšina práce druhej skupinky bola na Saganových ramenách. Jeho veľkí rivali Gilbert a Cancellara obsadili  deviate a desiate miesto. 
 
	Rui Alberto Faria Da Costa je od roku 2000 len tretím cyklistom, ktorý sa nezúčastnil Vuelty a súčasne sa stal majstrom sveta. Každý rok stúpa významnosť pretekov v Amerike, navyše do kalendára pribudol kanadský „etapák“ Tour of Alberta. Cyklisti tak majú možnosť absolvovať dlhé sústredenia v Andách vo vysokých nadmorských výškach bez obáv, že by vyšli z pretekového rytmu. Musíme ale podotknúť, že okrem Sagana a Costu sa v prvej desiatke výsledkovej listiny nenachádzal ani jeden cyklista, ktorý Vueltu vynechal. Pretek dokončilo len 61 cyklistov, čo svedčí o jeho extrémnej náročnosti. Pre zaujímavosť posledné 61. miesto obsadil francúzsky miláčik Thomas Voeckler. Z našich sa do cieľa pozrel len Sagan. Ešte horšie dopadol britský tím, ktorý napriek maximálnej účasti deviatich cyklistov nemal vo výsledkovej listine ani jedného pretekára. 
 
	            
 
	Pohľad diváka 
 
	  
 
	Napriek výbornej atmosfére a dramatickému priebehu preteku neboli Majstrovstvá sveta v Taliansku dokonalé. Oproti MS vo Valkenburgu bola účasť divákov asi o 60 % nižšia. Náš tím si pochvaľoval podmienky, v ktorých sa mohol pripravovať. My ako diváci sme sa stretli v uliciach Florencie s nejedným problémom. Čo sa týka mestskej hromadnej dopravy, vládol chaos. Na žiadnej zastávke ste si nenašli v inom jazyku ako talianskom inštrukcie o obmedzenej doprave. Na zastávkach sa nenachádzali žiadne mapy s polohou zastávky. Takmer nikto z miestnych nevedel po anglicky a na  dobrovoľníkov, ktorí by sa pohybovali mestom aby pomohli divákom dostať sa do priestoru cieľa sme nenatrafili. Videli sme mnohé skupiny fanúšikov, ktoré blúdili mestom v snahe dostať sa k cieľu. Nám sa to aj vzhľadom na to, že sme boli vo Florencii už tretí deň podarilo. Aj kvôli dažďu a skorému príchodu sme sa dostali na lukratívne miesto v prvom rade cieľovej rovinky, kde nás vytrvalo bičoval dážď. Veľkoplošné obrazovky nemali najlepšie umiestnenie, čo sme našťastie videli už deň predtým, počas preteku žien. Veľká väčšina divákov v cieľovej rovinke buď na obrazovku vôbec alebo takmer vôbec nevidela. My sme zase nevideli, ani nevedeli, kde sa nachádza pódium. Keď ku tomu pripočítam neustále vypadávanie veľkoplošných obrazoviek, musím konštatovať, že som očakával od Talianov lepšiu organizáciu. Čo sa týka mesta, tak mu nemožno uprieť historickú významnosť a mnohé architektonické skvosty, avšak týmto skutočnostiam uberala na kráse všadeprítomná špina v uliciach.  
 
	  
 
	Slováci v skratke 
 
	  
 
	Petrovi Saganovi sa síce v posledný deň šampionátu nepodarilo dosiahnuť mnohými očakávanú medailu, ale jeho šieste miesto je zatiaľ najlepším výsledkom slovenského cyklistu na MS v súťaži jednotlivcov. Peter Velits spolu so svojím tímom Omega Pharma Quick Step obhájil prvenstvo v tímovej časovke. Veľmi pozitívnym prekvapením bolo umiestnenie našej juniorky Terezy Medveďovej, ktorá pri svojej premiére na MS obsadila v pretekoch s hromadným štartom rovnako ako Sagan šieste miesto. V ostatných kategóriách sme nezaznamenali výraznejší výsledok. Opäť sa ukázalo, že veľké rezervy majú slovenskí reprezentanti v časovkách, a týka sa to asi všetkých kategórií. 
 
	  
 
	Hodnotenie trénera PROefektu o modeloch prípravy favoritov Majstrovstiev sveta 
 
	  
 
	Pred štartom hlavného preteku sa  polemizovalo o správnosti voľby prípravy favoritov. Náročný a po dlhej dobe kopcovitý terén s extrémnou dĺžkou trate naznačoval, že potenciálny víťaz by mal absolvovať Vueltu. Tento predpoklad nevyšiel u víťaza, navyše nekorešpondoval ani s prípravou P. Sagana. Každý pretekár disponuje určitým množstvom energie a výkonnostným základom. Práve tie sú rozhodujúce pri voľbe spôsobu prípravy. U enormne vytrvalostne trénovaných jedincov typu Rodriguez, Valverde, či Nibali je potrebný extrémny stimul k navýšeniu športovej formy. K tomuto účelu sa ukazuje Vuelta ako najideálnejší model prípravy. Naopak, u nie až takto stabilne výkonných vytrvalcov typu Sagan, Hushovd ale aj Costa to platiť nemusí. Rovnako dôležité je brať do úvahy, čo športovec už v danej sezóne absolvoval. Model prípravy Costu pred MS nepoznám, avšak vieme sa odraziť od Peťovho poňatia prípravy. On strávil 6 týždňov v Aspene vo výške 2000 m.n.m, odkiaľ vyrazil na ťažké jedno dňové preteky. Treba si uvedomiť, že počas pobytu vo vysokohorskom prostredí sa venoval sofistikovanej príprave, ktorá ho dobila veľkým množstvom energie a zároveň navýšila jeho výkonnostný základ. Z tohto staval na jednorázovkách, ktoré mu pomohli doladiť športovú výkonnosť. Na MS sa naozaj nachádzal v svojej životnej top forme a konečný výsledok na tom nič nemení. Saganove myslenie prechádza renesanciou, keďže si sám dobre uvedomuje spôsob jeho cesty k vrcholu. Taktiež vie, že v tejto fáze hry to je oveľa dôležitejšie ako samotný výsledok. Hoci aj ten je bezkonkurenčný. 
 
	  
 
	Záverom len dodám, že v prípade Peťovho absolvovania Vuelty by na MS nebol schopný podať rovnako kvalitný výkon. Výsledok mohol byť eventuálne lepší v tom prípade, že by sa pretek vyvíjal inak.. 
 
	                                                                                              
 
	    
 
	 
 
	Pelotón sa prehnal aj ľavým brehom rieky Arno, v blízkosti svetoznámeho mosta Ponte Vecchio 
 
	  
 
	 
 
	Vo štvrtok sa nám cestou z hotela podarilo odfotiť hlavný únik preteku juniorov... 
 
	  
 
	 
 
	...a tiež hlavnú skupinu na ulici Via Francesco Barraca 
 
	  
 
	 
 
	Počas preteku žien bolo vidieť všade holandských fanúšikov  
 
	  
 
	J.S. a PROefekt                                                                      

