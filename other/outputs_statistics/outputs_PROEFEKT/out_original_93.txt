







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5197   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5121   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4546   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3601   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3305 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Utorkový tréningový simulovaný pretek aj s Vargom   14.8.2012  
	 
   
 
	   Dnes 14.8. 2012 sa ako každý utorok konal tréningový pretek so známym okruhom po susednom Rakúsku. Už posledné kolá naznačili stupňujúcu sa kvantitu podujatia, čo do počtu účastníkov, tak i kvalitu, čo sa týka aktívneho pretekania a rýchlostného priemeru. Dnešný pretek prekonal všetky doterajšie, čo mohlo byť spôsobené dopredu avizovanou účasťou slovenského olympionika Richarda Vargu. Na štarte sa nás zišlo cez vyše 50 cyklistov a triatlonistov. Ihneď od štartu sa aktívne pretekalo. Po nejakých 10 km sa podarilo uniknúť silnej skupine v ktorej boli títo jazdci: Kostelničák, Škopek, Kaťuša, Wachs, Klúčik, Lašút, Novák, Branislav Karas a Juraj Karas. Vzornou spoluprácou si pred prenasledujúcou početnou skupinou udržovali náskok. V strede preteku v kopci nastúpil Juraj Karas, chytil sa ho Martin Kostelničák (obaja Greenbike Realiz team). Po asi 8 km boli zvyšnými 7 pretekármi dostihnutí. Opäť sa pokračovalo spolu v 9 kusoch. Asi 7 km pred cieľom to skúsil Juraj Karas, ale vo vetre a silnej skupine za chrbtom mu to nevydržalo. Rozhodujúci únik prišiel asi 3 km pred cieľom kedy sa vďaka zaváhaniu ostatných podarilo odskočiť Wachsovi s Kaťušom. Po asi 15 sekundách po nich nastúpil Juraj Karas, ktorý ich dostihol. Pretek sa dostával do dramatického záveru, kedy vpredu spolupracovala táto trojka a za nimi hladných 6 vlkov. Z vedúcej trojice tesne pred cieľom vyšpurtoval Juraj Karas. Z prenasledujúcej šestice Klúčik, ktorému sa podarilo predbehnúť Kaťušu a Wachsa z úniku. Poradie bolo teda nasledovné: 1. miesto: Juraj Karas, 2. miesto: Daniel Klúčik, 3. miesto Michal Kaťuša. Taktiež bol prekonaný rýchlostný priemer z minulého týždňa, ktorý v rakúskom vetre predstavoval 43,1 km/h. Richardovi Vargovi sa nešlo dobre, nakoľko ešte len ráno priletel z Londýna a pretek obišiel tréningovo. 
 
	  
 
	Všetkým zúčastneným gratulujeme a tešíme sa na ďalšie utorkové kolá. 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  Sagan  2012-08-15 15:47:47 
			

			 Obsah: Môže sa zúčastniť aj cyklo-amatér? Aká dlhá trasa sa šla?

Ďakujem 
			

 
  
			

			 Pridal:  PROefekt  2012-08-15 15:58:37 
			

			 Obsah: Áno môže sa zúčastniť každý kto má na čom bicyklovať. Chodí sa trasa v Rakúsku cca. 40km. 
			

 
  
			

			 Pridal:  Jan  17.08.2012 21:12:28 
			

			 Obsah: Nabudúce sa možno pridám aj ja. Aj tu je o tom článok http://sport.sme.sk/c/6500180/pridte-si-zatrenovat-s-triatlonistom-vargom.html 
			

 
  
			

			 Pridal:  Otazka  11.09.2012 21:54:40 
			

			 Obsah: Dobry den, chcel by som sa spytat ci sa este podobne treningy pre vsetkych konaju aj v septembri. Dakujem. 
			

 
  
			

			 Pridal:  PROefekt  12.09.2012 09:05:06 
			

			 Obsah: Dobry den. Ano byvaju. Vcera sme sa avsak dohodli, ze pre skorsie zotmievanie to od buduceho tyzdna presuvame na 17:00. 
			

 
  
			

			 Pridal:  juraj  03.04.2013 08:14:57 
			

			 Obsah: Dobry den, budu treningy aj tento rok? dakujem 
			

 
 
Na začiatok 1 | 2 | &gt;&gt; Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


