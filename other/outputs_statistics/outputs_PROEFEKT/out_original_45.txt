







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Laktátová krivka: vy sa pýtate, my odpovedáme   26.03.2013  
	 
   
 
	Čo to je laktátová krivka? 
	 
	Ide o test vami zvolenej pohybovej činnosti (beh, bicyklovanie, veslovanie, plávanie..) so stupňujúcou intenzitou až do maxima. Z končeka prsta alebo ušného lalôčika sa po stupňujúcich úsekoch odoberá kapilárna krv na zistenie koncentrácie laktátu v krvi, ktorý odzrkadľuje metabolické procesy pri danej záťaži. Z nameraných hodnôt sa vyhotoví laktátová krivka a z nej následne určí hladina laktátu, srdcová frekvencia a rýchlosť na aeróbnom, anaeróbnom prahu a maxime v ktorom sa skončí (obr.1). 
	 
	 
	Grafická ukážka laktátovej krivky zastaralou metódou určenia AP na 2 a ANP na 4 mmol 
	 
	Pre koho je test vhodný? Test nemá takmer žiadne obmedzenia, je vhodný špičkovým športovcom, rekreačným športovcom ako aj osobám, ktoré športujú pre zdravotný, či estetický cieľ. Všeobecne platí, že najvyšším prínosom je pre toho, kto sa snaží trénovať zmysluplne, systematicky a efektívne.br&gt;br&gt; Pre príklad uvádzam vhodnosť a potrebu testovania (diagnostikovania) z rôznych aspektov u troch odlišných osôb venujúcich sa pohybovej aktivite. 
	 
	 
	Význam testovania z rôznych aspektov u troch osôb (Prevzaté z Hamar, 2003) 
	 
	Kedy testovať, a ako často? 
	 
	Toto je značne individuálna záležitosť. Som toho názoru, že vrcholový športovec by mal záťažový test podstúpiť minimálne štyrikrát do roka. Vždy na začiatku konkrétneho obdobia (mezocyklu), aby sa tréner vedel odraziť a videl akú „robotu“ spravili v predchádzajúcom období, čo treba zlepšiť v nasledujúcom vzhľadom k pretekovému programu a ladeniu športovej formy. Taktiež to je potrebné k „nakalibrovaniu“ tréningových zón, ktoré sa najmä rozdielnym tréningom vzhľadom k obdobiu menia. U výkonnostných by som odporúčal dvakrát za rok, rekreačných športovcov a osôb športujúcich pre zdravotné a estetické ciele postačuje jedenkrát za rok. 
	 
	Kde je test možné vykonať? 
	 
	Výhodou laktátovej krivky je jej vysoká praktickosť. Možno ju vykonať v rozličných športoch s horizontálnou lokomóciou (beh, cyklistika, veslovanie, plávanie, bežecké lyžovanie..) a taktiež v podmienkach prirodzených, teda tam kde daný šport prebieha – v teréne. Alebo v podmienkach laboratórnych (bežecký pás, bicyklový ergometer, veslársky ergometer..). Oba prístupy majú svoje klady i zápory. Avšak najdôležitejšie je zvoliť si ten šport, ktorému venujeme najviac času. Treba mať na zreteli, že výsledky z testu nie sú dobre transformovateľné na iný šport, než v ktorom bola laktátová krivka vykonaná. To znamená, že tréningové zóny zistené pri atletickom teste nebudú platiť pri tréningu na bicykli a samozrejme to platí i naopak. 
	 
	Čo presne si z testu odnesiem? 
	 
	Výkony na prahových hodnotách (aeróbny prah, anaeróbny prah a maximálny výkon v teste zodpovedajúci VO2max) a spolu s tvarom krivky nám udávajú pomerne presnú informáciu ako na tom sme v základnej vytrvalosti, zmiešanej, na úrovni anaeróbneho prahu a anaeróbno – aeróbnej, čiže tzv. pretekovej vytrvalosti. Hodnoty srdcovej frekvencie na prahových hodnotách využívame k zostaveniu jednotlivých tréningových zón, podľa ktorých sa dá tréning objektívne a prakticky riadiť smerom k cieľom prípravy. Ďalej to je úroveň zotavenia, ktorá vypovedá o možnej únave, začínajúcej chorobe, či pretrénovaní alebo nedostatočne vybudovanej základnej vytrvalosti. A v neposlednej rade, porovnanie svojich výkonov so sebou samým (ak som test už podstúpil) alebo s kolegom, súperom, kamarátom, či svetovou špičkou. 
	 
	Ako sa na test pripraviť? 
	 
	Vždy prízvukujem, že je veľmi dôležité prísť na testovanie odpočinutý, dostatočne zásobený energiou a hydratovaný. V princípe tu platí to, čo pred súťažou. Pre tých, čo na súťaže nechodia to rozoberiem konkrétnejšie. Deň pred testom, je nutné si dať len ľahší tréning alebo voľno. V deň testu je potrebné sa najesť cca 2,5 – 2 hodiny pred testom, potom už len nejaká tá ľahká tyčinka a prísť fyzicky i psychicky fit. Pamätajte na to, že práve nedostatok svalového, pečeňového glykogénu a i glukózy v krvi, je to čo môže primárne negatívne ovplyvniť váš výkon a taktiež hodnoty srdcovej frekvencie a koncentrácie laktátu v krvi. 
	 
	Ako sa obliecť? 
	 
	Ak sa test vykonáva v terénnych podmienkach je potrebné sa obliecť tak ako keby sa chystáme pretekať v danom počasí. To znamená skôr menej, aby sme sa príliš nepotili. Na úvodné zahriatie je dobré mať pripravenú teplejšiu vrstvu, ktorú pred samotným testom vyzlečieme. Pri testovaní v laboratórnych podmienkach (napr. aj fitness centrum) bývajú podmienky konštantné a to je cca 19 – 23 stupňov. Tu je vhodné si obliecť ľahké šortky a funkčné tielko, či tričko. Pribaliť treba aj uterák, ktorým sa občas pretrie pot z tváre, či rúk. Kvapky potu, ktoré sa rozlievajú po tvári, rukách významne zhoršujú termoreguláciu. Tento nežiaduci jav zapríčiňuje prehrievanie organizmu. To neadekvátne zvyšuje srdcovú frekvenciu a tým aj energetický výdaj organizmu v danej intenzite. Pri správnom oblečení a dôslednom utieraní sa dá tomuto efektu dostatočne zabrániť. No a na nohy športovú obuv, v ktorej daný šport vykonávame. 
	 
	Čo si je potrebné na test priniesť? 
	 
	Mimo športového oblečenia a uteráka je dôležité dodržiavať všade spomínaný pitný režim. Skôr ako rôzne energetické drinky odporúčam čistú vodu alebo jemne zriedený iontový nápoj, ktorý nám chutí a sme naň zvyknutý z tréningu. Kofeínové prípravky zvyšujú srdcovú frekvenciu a nechceme predsa „biť“ vyššie pulzy na hraničných hodnotách ako je u nás bežné. A posledné, čo netreba zabudnúť je chuť si dať do tela. 
	 
	Ako dlho to trvá? 
	 
	Na úvod si sadneme k vstupnému pohovoru/anamnéze, cca 5 minút. Potom nasleduje kvalitné 8 – 12 minútové zahriatie, kde sa organizmus zapracuje do vyšších intenzít. 2 minútky na ponaťahovanie, utretie, napitie a začíname so samotným testom, ktorý trvá cca 12 – 20 minút. Ihneď po skončení sú vyhradené 3 minúty pasívnemu zotaveniu, po nich nasleduje 12 minútový aktívny odpočinok (vyklusanie, vybicyklovanie..) po ktorom je vhodné doplňiť laktátový spád. Spolu to je cca 35 – 50 min. 
	 
	Kto môže test vykonávať? 
	 
	Testovanie je veľmi citlivá a odborná práca, pri ktorej sa i najmenšie chybičky krásy a nesprávne zvolená metodika môžu značne prejaviť na výsledkoch. Tie sú potom skreslené a neobjektívne. Zväčša metodika samotného zostrojenia krivky býva kameňom úrazu. Testovať by mala osoba, ktorá do toho vidí hlbšie, dokonalé pozná požadovanú problematiku, má dostatočné vzdelanie, vybavenie a samozrejme skúsenosti. Je nevyhnutné pristupovať ku každému jedincovi individuálne a pozerať sa na neho komplexne. To znamená, každému nadstaviť správny testovací protokol, vcítiť sa do jeho tela, tréningových návykov, športovej minulosti a aktivity posledných troch dní. Iba ak sú tieto požiadavky splnené môžeme hovoriť o kvalitnej diagnostike, ktorá nám dáva vysokú výpovednú hodnotu. 
 
	  
 
	Článok bol publikovaný na portáloch MTBiker.sk, behame.sk a abysportnebolel.sk 
 
	 
	 
	Autor: 
	 
	Mgr. Juraj Karas, PhD. / PROefekt 
	  
  
  
Zdieľať na Facebooku
 

 Späť     Všetky diela, vrátane publikácií, článkov alebo ich častí uverejnených na
	tejto stránke sú autorskými dielami chránené v zmysle autorského zákona.
	 
	Akékoľvek použitie takéhoto diela alebo jeho časti bez súhlasu autora
	alebo osoby, ktorej náleží právo na udelenie súhlasu s použitím diela,
	rovnako ako neoprávnené zásahy do autorského diela, vrátane neoprávneného
	rozmnožovania a ukladania do pamäťových a elektronických médií, kopírovania,
	distribuovania, prenášania, zobrazovania, uverejňovania, predávania,
	udeľovania povolenia, vytvárania odvodených diel sú protiprávne a
	postihnuteľné v občianskoprávnej alebo trestnoprávnej rovine. 
 
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
  
			

			 Pridal:  Michal K.  09.01.2015 12:45:04 
			

			 Obsah: Dobry den, mal by som otazku ohladne ANP. V poslednej dobe som sa zacal zaujimat o riadeny trening podla treningovych zon. Na Vasom obrazku, ale aj na inych strankach sa hranica ANP urcuje na urovni 4 mmol.l. Ale zas niektore strany uvadzaju, ze je to individualne a hranica ANP nemusi byt na tych 4. Ja mam urobenu laktatovu krivku a mne to tiez urcili na hranici 4. AKo to teda je ?? Dakujem  
			

 
  
			

			 Pridal:  PROefekt  09.01.2015 13:50:23 
			

			 Obsah: Dobry den, velmi dobra a vecna otazka. Ked sa pozriete na popis prveho obrazka clanku, zistite, ze obrazok je len ilustrativny a ze urcovanie ANP na 4 mmol je zastaraly a nespravny pristup. Avsak niektori diagnostici to bohuzial aj napriek tomu praktizuju.. 
			

 
 
Na začiatok 1 |  Na koniec

 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


