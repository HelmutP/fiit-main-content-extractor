







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5197   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5121   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Rekonvalescencia po operácii   01.12.2012  
	 
   
 
	  
 
	Na včerajšiu kontrolu na Kramáre šiel tréner PROefektu síce s dobrým pocitom, no zároveň s malou obavou či sa pleco hojí správne.. Dôvodom je, že si rekonvalescenciu po operácii mierne upravil a tým prekročil zaužívané lekárske prognózy. 
 
	  
 
	Ortézu som začal odkladať 3 týždne po operácii, miesto štyroch. Ľahkú rehabilitáciu som odštartoval na "vlastnú päsť" v štvrtom týždni po, miesto zaužívaných 7 až 8mich. A to najdôležitejšie, tréning som nasadil po ustúpení ukrutných bolestí 2 týždne po zákroku. V tomto čase bol môj stav kritický. Nevedel som plnohodnotne spať, miesto tréningu som bol nútený užívať lieky proti bolesti, ktoré aj tak príliš nepomáhali. Toto malo za následok úplne rozhodenie môjho inak výborného imunitného systému a pohyboval som sa na hrane s chorobou. Vykúpenie prišlo, keď som pozvoľna začínal s trenažérom. Ten som kombinoval s posilňovaním zdravých častí tela a riadením zverencov pri ich silovom tréningu, čo mi pomohlo neriešiť svoju bolesť. V treťom týždni prišla turistika, v štvrtom korčuľovanie, plávanie, sauna a vírivka. Samozrejme, že hybnosť je obmedzená a o to väčšmi bolo vtipné sledovať pohľady druhých na jednorukého „kulturistu“, plavca, korčuliara, cyklistu.. Taktiež som si uvedomoval riziká takéhoto športovania a o to viac som sa snažil byť obozretný a opatrný. 
 
	  
 
	 
 
	 ortézu som zavesil na klinec 
 
	  
 
	Včerajší verdikt lekára po prezretí RTG snímku a odokrytím kariet mnou upraveného postupu rekonvalescencie bol: „Hojí sa to pekne a rýchlo. Keď ste taký „Highlander“ s rehabilitáciou začneme skôr a klin vyberieme po novom roku.“ Práve táto informácia ma zaskočila, keď som bol pôvodne informovaný, že talizman v pleci ma bude ohrievať pol roka.. Avšak, tiež ma upozornil, že stále môže hroziť utrhnutie háčikovitého spoja kosti klinom a tým aj väzu. Práve preto musí byť zaťažovanie veľmi citlivé a postupné. 
 
	  
 
	 
 
	 ide sa trénovať, lebo slabý som 
 
	  
 
	No príroda je silná.. A my jej len musíme správnymi krokmi ponechať priestor pre sebarealizáciu. Jedným z kľúčových krokov sa javí zmysluplný pohyb prispôsobený k danej situácii organizmu.  
 
	  
 
	Ak neviete čo sa stalo trénerovi PROefektu, viac sa dozviete TU 
 
	  
 
	PROefekt 
 
	  
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


