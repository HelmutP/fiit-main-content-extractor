







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5197   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3522   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  Medzinárodné preteky Slovenské Pyreneje   26.08.2013  
	 
   
 
	Znievske dni cyklistiky sú významným a tradičným pretekom zaradeným v slovenskom pohári, nazývaným Slovenské Pyreneje. 54. ročníka sa zúčastnili profesionálny cyklisti z tímu Dukly Trenčín - Trek, poloprofesionálny rakúsky tím RSC-Amplatz, či kvalitne podporované poloprofesionálne tímy CK Epic Dohňany, CK Banská Bystrica a mnoho ďalších slovenských pretekárov z ostatných slovenských tímov. Na tejto slovenskej cestárskej klasike nechýbalo kompletné 8 členné zloženie mužskej časti PROefekt teamu: Juraj Karas, Ján Subovits, Tomáš Višňovský, Helmut Posch, Patrik Spanyo, Filip Lašut, Miroslav Kováčik a Pavol Dohnányi. Pre mnohých z nich to boli vôbec prvé tak rýchle, dlhé a kvalitne obsadené preteky. 
 
	  
 
	 
 
	rozjazdovanie pred pretekom 
 
	  
 
	 
 
	na štarte 
 
	  
 
	Tréner a jazdec Juraj Karas hodnotí priebeh preteku: 
 
	  
 
	"Na tradičnom 51 kilometrovom okruhu, ktorý sa absolvoval 3krát, sa začalo pekne zostra. 70-členný pelotón sa naťahoval a spájal. Výraznejšia selekcia prišla na prvom dlhom stúpaní na Diel. Po zjazde som ostal v asi 6 člennej skupinke, kde som spoznal Milana Barényho a Miroslava Debnára. O chvíľu si nás dotiahol aj Ján Subovits s ďaľšími pretekármi. V cca 15 člennom zložení sme doťahovali úplne čelo pretekov, kde boli cca 4 najrýchlejší jazdci prvého stúpania. To sa nám podarilo a zákonite sme zvolnili, čo pomohlo hlavnej skupine k nášmu dobehnutiu. V druhom kole sa však pokračovalo v ostrých atakoch a definitívne sa podarilo uniknúť šiestim pretekárom, kde nechýbali najlepší jazdci spomínaných profi tímov. V priebehu preteku sa z hlavného pelotónu v druhom selekčnom kopci Diel ešte podarilo odskočiť štyrom pretekárom (Kolář, Barényi, Lajcha a Poll). V poslednom kole pod Dielom sme ostali v hlavnej skupine za 10timi najlepšími z PROefektu ja, Subovits, Lašut a Kováčik. Pod heslom preteky končia až na páske, a s cieľom atakovať pre nás výborné 11. miesto som v začiatku tiahlého stúpania nastúpil s úmyslom pomôcť Subovitsovi vyviesť sa za najlepšími pretekármi, ktorí sa rozhodnú ma stíhať. Tento krok som spravil, i napriek tomu, že v hromadnom dojazde mám šancu uspieť. Avšak chcel som, aby aj Subovits ukázal vrchárske kvality a o 11. miesto sa pobil z úniku. Po taktickom a vypätom boji v tiahlom stúpaní sa podarilo definívne ujsť Subovitsovi so silným rakušanom Gaubitzer. Škoda, že Ján špurt o 11. miesto prehral, ale aj tak obsadil výborné 12. miesto. V posledných kilometroch pred cieľom sa ešte pokúšali jeden po druhom útočiť a uniknúť jazdci CK Epic Dohňany, čo bolo z mojho pohľadu značne nekoordinované a preto sa im to ani nepodarilo. A tak sme dochádzali pohromade a špurtovali o 13. miesto. To sa mi podarilo ukoristiť a skvelé predstavenie tímu ešte podtrhol 17. miestom Filip Lašut. Tempu na poslednom kopci Diel už nestačil Miro Kováčik a nakoniec vo svojich 47 rokoch dokončil na peknom 40.mieste. Patrik Spanyo po 3 mesačnej prestávke zapríčinenej presilenej achilovke pretek dobojoval na 52. mieste. Ako tréner si cením tiež obrovské zlepšenie Pala Dohnányho, ktorý minulý rok zvládol iba jeden okruh, teda 53 km. A teraz ako posledný 54ty jazdec ťažký pretek dokončil. Tomáš Višňovský a Helmut Posch sa zaradili medzi 30 jazdcov, ktorí nedokončili. Chlapci deň pred absolvovali horské preteky, ktoré sa podpísali na tom, že prvý rýchly okruh pretekov nezvládli akceptovať a mal za následok ich neskoršie zabalenie." 
 
	  
 
	 
 
	Juraj Karas, Ján Subovits, Pavol Dohnányi a Helmut Posch po štarte 
 
	  
 
	  
 
	  
 
	 
 
	záver dlhého stúpania na Diel 
 
	  
 
	Osobný postoj trénera k pretekom a vystúpeniu PROefekt teamu: 
 
	  
 
	Kláštor pod Znievom sú pre mňa srdcovou záležitosťou, keďže som tu zažil rozhodujúce momenty svojho cyklistického života. V r. 2003 sa mi tu v junioroch podarilo prvý krát vyhrať riadny pretek. V r. 2006 som po nenaplnených očakávaniach sezóny príčinou zranenia a dlhého výpadku ukončoval športovú kariéru 9. miestom a odovzdával bicykel vtedajšiemu tímu ŽP Šport Podbrezová. V r. 2011 som sa tu vrátil a ako opäť jazdec s licenciou preteky dokončil na 14.mieste. 
 
	  
 
	A tohto roku 2013 som tu prišiel so svojím tímom PROefekt porovnať sa s profi a poloprofi jazdcami, ktorých kvantitatívne tréningové a pretekové zaťaženie je oveľa vyššie. Navyše podopreté dlhoročnými skúsenosťami získaných na ťažkých medzinárodných pretekoch. Pri rozhliadnutí súperov na štarte som mal malú dušičku, aby sme aspoň dvaja preteky vôbec dokončili. Avšak dobrá nálada v tíme mi pomáhala nebyť vo svojej mysli príliš skeptický. 
 
	  
 
	Nakoniec takýto úspešný výsledok, mňa ako trénera znova utvrdzuje, že moja práca so zverencami prináša ovocie, že sme na správnej ceste v treningovom procese, ktorý nám dáva schopnosť pretekať s jazdcami z oveľa silnejších tímov a s lepšími materiálnymi podmienkami. 
 
	  
 
	Verím, že v Slovenských Pyrenejách zažijem v svojom cyklistickom živote ešte veľa pekného :) 
 
	  
 
	 
 
	V kopci Diel sa hlavná skupina neustále delila. Juraj Karas s Jánom Subovitsom dokázali stále ostať na čele 
 
	  
 
	 
 
	Pavol Dohnányi bojuje až do cieľa 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


