

 
	Po Majstrovstvách Slovenska v elitných a juniorských kategóriách v Púchove sa uplynulý víkend bojovalo o sady medaily v cestnej cyklistike i v mládežníckych kategóriách. Úspešný šampionát sa odohral v Žiari nad Hronom, kde ho organizoval cyklistický klub Žiar nad Hronom na čele s trénerom Františkom Sitorom. PROefekt tu mal zastúpenie v podobe dvoch jazdcov v dvoch mládežníckych kategóriách. 
 
	  
 
	V sobotu sa na štart časovky jednotlivcov v kategórii starší žiaci postavil Milan Kováč (ŠKC Dubnica nad Váhom), ktorý obsadil v 21 početnom štartovom poli 10. miesto. Na víťazného Adriana Foltána (CK Žiar nad Hronom) stratil iba 1 min. 11 sek. 
 
	  
 
	V kategórii kadetov sme mali zastúpenie v podobe Adama Szásza (Cyklo-team Habera Košice). Ten zaostal za víťazným Jurajom Bellanom (CK Žiar nad Hronom) o 1 min. 47 sek. a nakoniec to stačilo na 18 miesto. 
 
	  
 
	Juniorskú kategóriu vyhral favorizovaný Mário Daško (TJ Slávia ŠG Trenčín) pred Erikom Baškom (CK EPIC Dohňany) a Ľubošom Malovcom (CK Olympic Trnava). 
 
	  
 
	Výsledky z časovsky jednotlivcov tu 
 
	  
 
	V nedeľu bol na programe pretek s hromadným štartom, kde naše dve mladé želiezka čakala technická a kopcovitá trať s množstvom zákrut. Obaja sa s ňou popasovali statočne, keď Milan Kováč nakoniec vybojoval pekné 7. miesto. Na víťaza Tomáša Persona (Cyklistický spolok Žilina) stratil len 30 sekúnd. 
 
	  
 
	Milan po preteku povedal: „V druhom kole v kopci nás aj s tímovým kolegom Alexom Zemanom najlepší utrhli. Potom sme sa dali dohromady a poctivou jazdou a striedaním sa na špici sme si prvú skupinu tesne pred cieľom dotiahli. V záverečnom špurte do kopca som to však už cítil. Spravil som taktickú chybu, keďže som zažal naplno špurtovať príliš skoro a pár desiatok metrov pred páskou mi vytuhli nohy. I tak som s výkonom aj výsledkom spokojný“. 
 
	  
 
	Adam Szász, ktorý je v kategórii kadetov prvým rokom, vybojoval 14. miesto so stratou na víťaza Michala Bušfyho (Cyklistický spolok Žilina) iba zanedbateľných 12 sekúnd. 
 
	  
 
	Adam po preteku povedal: „Išlo sa dosť rýchlo, často krát sa striedalo tempo. V priebehu preteku som sa cítil dobre, v záverečnom kopci som už mal toho dosť a do špurtu sa mi už zapojiť nepodarilo. Som namotivovaný do tréningu na ďalšie preteky.“ 
 
	  
 
	Juniorskú kategóriu vyhral Ľuboš Malovec, ktorý správne načasovanie formy ukázal už v stredu na tréningových pretekoch Trnavskej Ligy, kde tiež zvíťazil. Touto cestou mu gratulujeme! Druhé miesto so stratou 1 sekundy obsadil víťaz sobotnej časovky Mário Daško. Za povšimnutie stojí tiež tretie miesto, vekom ešte kadeta Davida Zverka (CK Žiar nad Hronom), ktorý už na dvojicu stratil 20 sekúnd.     
 
	  
 
	O priebehu preteku nám porozprával víťaz Malovec: „Zo začiatku sa išlo rovnomerné tempo, v priebehu prvého okruhu sa do úniku dostali Krajíček a Holomek. V jeho závere sme ich dostihli. Na konci druhého okruhu sa od peletónu odpútali favorizovaný Daško a Zverko. Na nič som nečakal a začal ich dobiehať, čo sa mi aj podarilo. Takto v trojici sme prešli dva okruhy až do cieľa. V záverečnom kopci sa rozhodlo, Zverko nastúpil ako prvý ale zaseklo ho a potom som zrýchlil ja a vyhral.“ 
 
	  
 
	Výsledky z pretekov jednotlivcov tu 
 
	  
 
	9. augusta štartujú naši najlepší juniori na Majstrovstvách Európy v Holandsku. Držme im palce! 
 
	  
 
	Nabudúce na tento článok naviažeme semi - odborným článkom o Petrovi Saganovi, s retrospektívnymi a súčasnými príčinami jeho úspešnosti. 
 
	  
 
	PROefekt 

