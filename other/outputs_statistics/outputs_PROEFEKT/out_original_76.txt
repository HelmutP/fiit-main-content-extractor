







 
 
    

 
           
 
 
		   
 

        
	PRIHLÁSENIE PRE KLIENTOV   MENO  
	  
	HESLO 
	    
	          
	  
 
        
      
  
 
 

 

  
    
  


 
    
 
  Hlavná stránka 
  Prečo mať trénera? 
  Športová diagnostika 
  Tréning 
  
  Referencie 
  O nás 
  Naša filozofia 
  Kontakt 
  2% z daní 
     	
    
 
    
 
    

	

 

 
 
PROefekt Elite team
 
       
                 
 
 
 Višnovský  Tomáš 
 Fačkovec Matej 
 Salák Tomáš 
 Sklenárik Filip 
 Nagy Tomáš 
 Hladík Marek 
 Kais Martin 
 Juraj Karas   
 
  
 
 

 
PROefekt Hobby team
 
       
                 
 
 
 Posch Helmut 
 Padyšák Matúš 
 Pašek Ivan 
 Hojdík Vladimír 
 Heššo Peter 
 Mužila Roman 
 Košút Peter 
 Čarnoký Daniel 
 Szenyan Ivan 
 Schwarzkopf Róbert 
 Vicen Erik 
 Ďuriač Peter 
 Juriš Dominik 
 Jusko Lukáš 
 Oravec Ľuboš   
 
  
 
 



 
PROefekt Junior
 
       
 
 
 
 Kováč Milan 
 Szász Adam 
 Sanetrik Štefan 
 Zboja Samo 
 Žigo Dávid 
 Szirmai Rudolf 
 Mikula Miroslav 
 Gajdoš Rastislav 
 Tomša Juraj 
 Furdan Michal 
 Zeman Filip   
 
  
 
 

 
PROefekt Woman
 
       
 
 
 
 Bušíková Soňa 
 Kuštárová Tamara 
 Dudášová Zuzana 
 Szucsová Denisa 
 Pešková Lucia   
 
  
 
 

 
PROefekt Zverenci
 
 

 
 

 
Články 
                
 
 
				
  PROefekt team   Odborné články   Ostatné 
 
 
  
 
 
 
 

 
Najoblúbenejšie články 
                
 
 
				
  Prečo je Sagan Saganom? Počet čítaní: 13057   Štruktúra športového výkonu v cestnej cyklistike v disciplíne časovka jednotlivcov  Počet čítaní: 5198   Lance Armstrong vs doping + video záznam priznania Počet čítaní: 5122   Utorkový tréningový simulovaný pretek aj s Vargom Počet čítaní: 4547   Virtuálny tréningový systém PROefektu Počet čítaní: 4223   Tréner PROefektu sa stal „naozajstným“ cyklistom Počet čítaní: 4185   Komplexný pohľad na SPINNING očami cyklistov Počet čítaní: 4084   Helmut Posch - "Boli dni kedy som sa tešil do školy, lebo som nemusel ísť na tréning!" Počet čítaní: 3602   Laktátová krivka: vy sa pýtate, my odpovedáme Počet čítaní: 3523   Potvrdí Peter Sagan tohtoročnú formu aj na MS? Počet čítaní: 3306 
 
 
  
 
 

           
 
  
 

 
Články 
               
 

  MTB maratón Horal a finále Slovenského pohára XCO   12.08.2013  
	 
   
 
	Cez víkend 10.-11.8.2013 sa konal tradičný MTB maratón Horal. Bol súčasťou 3 - etapového preteku Horal Tour. Tretia etapa sa konala ako finále Slovenského pohára XCO. Pretekov sa zúčastnilo viacero profesionálnych jazdcov a o kvalitnú medzinárodnú konkurenciu tak bolo postarané. Samozrejme nemohli chýbať pretekári PROefektu. 
 
	  
 
	V piatok odštartovala prvá etapa Horal Tour, ktorej sa zúčastnila zverenkyňa PROefektu Andrea Juhásová. Pretekalo sa počas silnej búrky dôsledkom čoho bola preverená nie len fyzická ale aj psychická pripravenosť pretekárov. Etapa viedla zo Svitu na Štrbské pleso, mala 40 kilometrov a bola prevažne do kopca. V etape zvíťazil Ondřej Fojtík (X-Sports Cannondale Koma). V kategórii žien s prehľadom zvíťazila Andrea Juhásová o viac ako 11 minút, pred druhou v poradí, českou pretekárkou, Veronikou Jenčekovou (Eurofoam Team). 
 
	  
 
	V sobotu sa MTB maratónu Horal zúčastnili obe sestry Juhásové, Ján Subovits a Helmut Posch. Po nočnej silnej búrke bolo otázne v akom stave bude trať, ale po dlhodobých absenciách zrážok, nepredstavovalo ovlaženie povrchu väčší problém. Nepríjemné boli len vlhké korene a kamene. 
 
	  
 
	 Andrea Juhásová štartovala na Horal Lejzi, a mala pred sebou náročných 73 kilometrov. S konkurenciu si už tradične poradila a vo svojej kategórii zvíťazila. Druhé miesto obsadila Soňa Othmanová z Bratislavy. 
 
	  
 
	 
 
	Sústredená Andrea pred štartom 
 
	  
 
	Na kratšej trati, Horal Senzi, štartovala Zuzana Juhásová a Helmut Posch. Zuzana napodobnila svoju sestru a zvíťazila vo svojej kategórii. Helmut Posch obsadil 15. miesto vo svojej kategórii s čím samozrejme spokojný nebol. Kde nastal problém nám opísal Helmut : 
 
	  
 
	"Na preteky som nastupoval s malou dušičkou, pretože som mal absenciu poriadného tréningu viac ako 10 dní, kvôli teplám, ktoré zasiahli Bratislavu v posledných dňoch. Hlavným problémom bolo, že som nemohol dlhodobo spávať a nedalo sa mi teda ani normálne trénovať. Po štarte som sa držal s prvými, ale po 20 minútach sa mi zastavili nohy. Následne som pretek poňal tréningovo a v stúpaniach som jazdil na anaeróbnom prahu a v zjazdoch som bol mimoriadne opatrný. Trať bola pekná a užil som si ju. Verím, že po opätovnom zapracovaní do tréningu sa znova vrátim do pretekárskej formy." 
 
	  
 
	 
 
	Helmut Posch v záverečných častiach okruhu 
 
	  
 
	Náš elitný cestár Ján Subovits v sobotu štartoval na trati Horal Ízi a boli to jeho prvé preteky na horskom bicykli. Horský bicykel si požičal od zverenca PROefektu, Radovana Struhára. Ján dokázal, že cyklistika je iba jedna a bez problémov zvíťazil. Viac nám povedal on sám: 
 
	  
 
	"Uplynulý víkend som nemal na pláne žiadne cestné preteky, tak som sa rozhodol skúsiť niečo nové a skočiť do terénu pozrieť na Horal. V teréne nie som veľmi skúsený tak bol najvyšší čas nabrať skúsenosti. Počas víkendu som skúsil dve rôzne disciplíny a to Maratón a XC. Obidve tieto disciplíny sú náročné a vyžadujú kondíciu a technickú zručnosť jazdy na bicykli v teréne. Mne osobne viac sadol Maratón. Vychutnával som si na ňom dlhé stúpania, rozmanitosť trate a jej povrchov. Nikdy som nevedel čo ma čaká za ďalšou zákrutou musel som rátať zo všetkým a čakal som na to čím ma trať prekvapí. Touto cestou by som sa chcel poďakovať Radovanovi Struhárovy za zapožičanie bicykla pre tieto preteky. " 
 
	  
 
	 
 
	Pódium preteku Horal Ízi, Ján Subovits s prehľadom zvíťazil 
 
	  
 
	V nedeľu, sa konala 3. etapa Horal Tour, ako finále Slovenského pohára XCO. Trať bola technicky náročná, kvôli vlhkým korenistým zjazdom. Pretekov sa z PROefektu zúčasnili sestry Juhásové, Tomáš Višňovský a Ján Subovits. Pre Jána Subovitsa to bol prvý pretek typu cross-country olympic a preto štartoval v kategórii Open muži. Takéto mal z preteku pocity: 
 
	  
 
	"XC je tiež veľmi zaujímavá disciplína ktorá si vyžaduje rýchlosť a technickú zručnosť a taktiež je veľmi divácky atraktívna. Na jazdenie XC musím nabrať ešte množstvo skúseností. V budúcnosti by som chcel ešte určite navštíviť niekoľko MTB Maratónov a možno sa im venovať aj viacej ako jazde na ceste. Od štartu som o víťazstvo bojoval s Miroslavom Michálekom (Finančné centrum Team), ale keďže je v tejto disciplíne oveľa skúsenejší dokázal ma poraziť o 3 sekundy. " 
 
	  
 
	 
 
	Ján Subovits bojuje o víťazstvo vo svojom prvom preteku cross-country olympic 
 
	  
 
	V kategórii Elite muži, štartoval najlepší biker PROefekt teamu, Tomáš Višňovský. Ako sa mu na pretekoch darilo nám opísal takto: 
 
	  
 
	"Trať bola veľmi pekná, technická a korenistá. Štartoval som z druhej "lajny" ale po štarte sa mi podarilo dostať dopredu. V prvej polke preteku som sa pohyboval na piatom mieste ale potom v zátačke som stratil koncentráciu a pedálom som zachytil koreň. Pozbieral som sa a pokračoval ďalej na 6. priečke. V dlhom korenitom zjazde mi padala reťaz. V posledom kole som bol na siedmom mieste a na konci stúpania som som mal šiesteho pred sebou. V zjazde mi znova spadla reťaz a tak som sa uspokojil so siedmim miestom." 
 
	  
 
	 
 
	Tomáš Višňovský na trati 
 
	  
 
	Andrea Juhásová zvíťazila i v tretej etape Horal tour a tak ovládla aj celkové poradie. 
 
	  
 
	Kompletné výsledky nájdete TU 
 
	  
 
	PROefekt 
  
  
Zdieľať na Facebooku
 

 Späť   
   Komentáre  
	 Nekonštruktívne alebo článok znehodnocujúce komentáre, ktoré nie sú podpísané menom a priezviskom, budú vymazané. 
 Zatiaľ neboli pridané žiadné komentáre!
 
	Pridať komentár  
	

	
	 


	 Váš nick 




	   
	
 Váš komentár 


   

Sem napíšte číslo z obrázku




	   

	


   
	   - Pozor! Všetky polia musia byť vyplnené!
	

   

           

 
  
 
  


 

 

 
novinky 
                
 

Soňa Bušíkova v súťaži Miss slovenskej cyklistiky 2014 06.12.2014 o 21:39 Študentský beh 2014 16.11.2014 o 19:38 Vyhodnotenie celkového poradia Slovenského pohára v MTB XCO 2014 24.09.2014 o 19:16 Prečítajte si rozhovor s Jurajom Tomšom o MS v triatlone na Hawaii 19.09.2014 o 18:44 Bohatý pretekový víkend 13. - 14.9.2014 15.09.2014 o 20:32             
 		

  
 

 

 
Bilancia sezóny 
                
 

 Bilancujeme sezónu 2014. Sumarizáciu výsledkov si môžete pozrieť v článku. 
 
 Celý článok TU
            
 		

  
 

 
				   	
 
Video 
                
 

				

	   Vznik PROefekt teamu 
 
    Zobraziť viac videií 
 
  
 

 

 
facebook 
                
 
                    
 			   
  
 
 
Zdieľať na Facebooku
 

	

 		

  
 
			
 
  
 

 

  
 
				
 

  
 

 

  
 

 

  
 
				
	
			
        
 
  
  
 

 

         
        	 Hlavná stránka 
			 Prečo mať trénera 
                         Športová diagnostika 
                         Tréning 
                         PROefekt team 
                         Referencie 
						 O mne 
						 Kontakt 
						
						 Prihlásenie pre klientov                        
         
    
        © 2012 — 2015 PROefekt. All rights reserved.  
        
    
      
  


