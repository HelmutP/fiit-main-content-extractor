#!/usr/bin/env ruby
#Helmut Posch
#STU FIIT, test for proefekt dataset
#run from folder BP

load 'other/source_statistics/Classes_for_statistics.rb'
require 'nokogiri'
require 'mechanize'
require 'open-uri'
require 'sanitize'


$dir_output = String.new("other/outputs_tests/cetr_nypost/")
$dir_gold = String.new("/home/destik/MyRubyScripts/BP/other/data/CETR_dataset/news/gold/nypost/")

$files_gold_tmp = Dir.entries($dir_gold)
$files_gold = Array.new

dobry_poc = 0
zly_poc = 0
full_precision = 0
full_recall = 0
tested = 0

for j in 0..$files_gold_tmp.length-1
    if File.ftype($dir_gold+$files_gold_tmp[j].to_s) == "file"
      
      casti = $files_gold_tmp[j].split('.')
      $url = "http://www.mceapi.com/extract?form=xml&url=http://www.mceapi.com/data/CETR_dataset/news/original/nypost/" + casti[0].to_s + ".html"
      puts $url
      $xml_output = Nokogiri::HTML(open($url).read)
      
      success = $xml_output.css('success').text
      if  success == "true"

        str1 = String.new($xml_output.css('content').text)
        str2 = String.new(File.read($dir_gold + $files_gold_tmp[j]))
        extracted_html = Html.new(str1)
        extracted_content = extracted_html.delete_tags
        gold_html = Html.new(str2)
        gold_content = gold_html.delete_tags
        out_original = File.new($dir_output + "out_test_#{$files_gold_tmp[j]}.txt","w")
        out_original.write(extracted_content)
        out_original.close
        gold_content = gold_content.gsub(/[[:space:]]/,"")
        extracted_content = extracted_content.gsub(/[[:space:]]/,"")

        puts "TESTUJEM: #{$files_gold_tmp[j]}"
        
        if extracted_content == gold_content
          dobry_poc+=1
          puts "OK"
        else
          zly_poc+=1
          puts"BAD"
        end
        puts "DLZKA SPRAVNEHO TEXTU: #{gold_content.length}"
        extracted_sentences = Array.new(extracted_content.split('.'))
        gold_sentences = Array.new(gold_content.split('.'))
        for k in 0...extracted_sentences.length
            extracted_sentences.delete_at(k) if extracted_sentences.at(k) == nil
        end
        for k in 0...gold_sentences.length
            gold_sentences.delete_at(k) if gold_sentences.at(k) == nil
        end

          i = 0
          returned_par = 0
          correct_returned_par = 0

          if extracted_sentences.length >= 3 
            while i < extracted_sentences.length do
              returned_par += 1
              correct_returned_par += 1 if((gold_content.include?extracted_sentences.at(i).to_s))
              i = i + 1
            end
            correct_returned_par = correct_returned_par / 3
            returned_par = returned_par / 3
            precision = correct_returned_par.to_i  * 100 / returned_par.to_i
          else
            precision = -1
          end

          i = 0
          finding_par = 0
          correct_finded_par = 0

          if gold_sentences.length >= 3  && extracted_sentences.length >= 3 
            while i < gold_sentences.length do
              finding_par += 1
              correct_finded_par += 1 if((extracted_content.include?gold_sentences.at(i).to_s))
              i = i + 1
            end
            correct_finded_par = correct_finded_par / 3
            finding_par = finding_par / 3
            recall = correct_finded_par.to_i  * 100 / finding_par.to_i
          else
            recall = -1
          end

        puts "PRECISION: #{precision}%"
        puts "RECALL: #{recall}%"
        if precision!=-1 && recall!=-1
          full_recall += recall
          full_precision += precision
          tested += 1
        end
      else
          puts "TESTUJEM: #{$files_gold_tmp[j]}"
          zly_poc+=1
          puts"BAD (SUCCESS FALSE)"
          puts "PRECISION: 0%"
          puts "RECALL: 0%"
          full_recall += 0
          full_precision +=0
          tested +=1
      end
    end
end

fin_precision = full_precision.to_f / tested.to_f
fin_recall = full_recall.to_f / tested.to_f
puts "OK FILES: #{dobry_poc}"
puts "BAD FILES: #{zly_poc}"
puts "PRECISION: #{fin_precision}"
puts "RECALL: #{fin_recall}"




