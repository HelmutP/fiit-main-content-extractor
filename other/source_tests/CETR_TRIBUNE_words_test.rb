#!/usr/bin/env ruby
#Helmut Posch
#STU FIIT, test for proefekt dataset
#run from folder BP

load 'other/source_statistics/Classes_for_statistics.rb'
require 'nokogiri'
require 'mechanize'
require 'open-uri'
require 'sanitize'


$dir_output = String.new("other/outputs_tests/cetr_tribune/")
$dir_gold = String.new("other/data/CETR_dataset/news/gold/tribune/")

$files_gold_tmp = Dir.entries($dir_gold)
$files_gold = Array.new

full_precision = 0
full_recall = 0
tested = 0

for j in 0..$files_gold_tmp.length-1
    if File.ftype($dir_gold+"/"+$files_gold_tmp[j].to_s) == "file"
      casti = $files_gold_tmp[j].split('.')

      $url = "http://www.mceapi.com/extract?form=xml&url=http://www.mceapi.com/data/CETR_dataset/news/original/tribune/" + casti[0].to_s + ".html"

      $xml_output = Nokogiri::HTML(open($url).read)
      
      success = $xml_output.css('success').text
      if  success == "true"

        str1 = String.new($xml_output.css('content').text)
        str2 = String.new(File.read($dir_gold + $files_gold_tmp[j]))
        extracted_html = Html.new(str1)
        extracted_content = extracted_html.delete_tags
        gold_html = Html.new(str2)
        gold_content = gold_html.delete_tags
        out_original = File.new($dir_output + "out_test_#{$files_gold_tmp[j]}.txt","w")
        out_original.write(extracted_content)
        out_original.close
        gold_content_words = Array.new(gold_content.split(" "))
        extracted_content_words = Array.new(extracted_content.split(" "))

        puts "TESTUJEM: #{$files_gold_tmp[j]}"

        all_ex_words = extracted_content_words.length
        cor_ex_words = 0
        extracted_content_words.each do |exword|
          cor_ex_words+=1 if gold_content_words.include?(exword)   
        end
        precision = cor_ex_words * 100 / all_ex_words

        all_ex_words = gold_content_words.length
        cor_ex_words = 0
        gold_content_words.each do |gword|
          cor_ex_words+=1 if extracted_content_words.include?(gword)   
        end
        recall = cor_ex_words * 100 / all_ex_words

        puts "PRECISION: #{precision}%"
        puts "RECALL: #{recall}%"
        if precision!=-1 && recall!=-1
          full_recall += recall
          full_precision += precision
          tested += 1
        end
    end
  end
end

fin_precision = full_precision.to_f / tested.to_f
fin_recall = full_recall.to_f / tested.to_f
puts "PRECISION: #{fin_precision}"
puts "RECALL: #{fin_recall}"




