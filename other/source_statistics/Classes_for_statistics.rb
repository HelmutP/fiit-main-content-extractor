#!/usr/bin/env ruby
#Helmut Posch, Faculty of Informatics and Information technologies STU BA
#Classes needed for statstics

require 'nokogiri'
require 'sanitize'

class Html
	
	attr_accessor :commas, :full_stops, :content, :commas_anchor, :full_stops_anchor

	def initialize(content)
		@content = String.new(content)
		@full_stops = 0
		@commas = 0
		@full_stops_anchor = 0
		@commas_anchor = 0
	end
	
	def encode
		if ! @content.to_s.valid_encoding?
  			@content = @content.encode("UTF-16be", :invalid=>:replace, :replace=>"?").encode('UTF-8')
		end
	end

	def delete_tags
		html_doc = Nokogiri::HTML(@content)
		html_doc.css("script,style,head").each{|i| i.remove}
		html_doc = Sanitize.fragment(html_doc.to_s)
		@content = html_doc.to_s
	end

	def evaluate_interpunction
		delete_tags
		for i in 0..@content.to_s.length-1
			if(@content.to_s[i]==',')
				@commas += 1
			end
			if(@content.to_s[i]=='.')
				@full_stops += 1
			end
		end
		@content
	end

	def evaluate_anchor_interpunction
		anchor_text_doc = Nokogiri::HTML(@content)
		anchor_text = anchor_text_doc.css("a")
		for i in 0..anchor_text.text.length-1
			if(anchor_text.text[i]==',')
				@commas_anchor += 1
			end
			if(anchor_text.text[i]=='.')
				@full_stops_anchor += 1
			end
			#puts "#{i}"
		end
	end

end