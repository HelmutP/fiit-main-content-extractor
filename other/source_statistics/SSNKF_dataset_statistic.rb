#!/usr/bin/env ruby
#Helmut Posch
#STU FIIT, creating statistic for SSNKF

load './other/source_statistics/Classes_for_statistics.rb'
require 'sanitize'

#run from folder BP

$dir_html = String.new("other/data/SSNKF_dataset/original")
$dir_gold = String.new("other/data/SSNKF_dataset/gold")

$sum_full_stops_original = 0
$sum_commas_original = 0
$sum_commas_gold = 0
$sum_full_stops_gold = 0
$sum_full_stops_original_anchor = 0
$sum_commas_original_anchor = 0
$sum_commas_gold_anchor = 0
$sum_full_stops_gold_anchor = 0

$files_html_tmp = Dir.entries($dir_html)
$files_gold_tmp = Dir.entries($dir_gold)
$files_html = Array.new
$files_gold = Array.new

for j in 0..$files_gold_tmp.length-1
  if File.ftype($dir_html+"/"+$files_html_tmp[j]) == "file"
    $files_html.push($files_html_tmp[j])
  end
  if File.ftype($dir_gold+"/"+$files_gold_tmp[j]) == "file"
    $files_gold.push($files_gold_tmp[j])
  end
end

$files_html.sort!
$files_gold.sort!

for j in 0..$files_html.length-1
    
  #puts $files_html[j]

  if File.size($dir_html+"/"+$files_html[j]) == 0
    continue
  end

  $file_html = File.open($dir_html+"/"+$files_html[j])
  $file_gold = File.open($dir_gold+"/"+$files_gold[j])
  $original = Html.new($file_html.read)
  $gold = Html.new($file_gold.read)
  $file_html.close
  $file_gold.close

  $original.encode
  $gold.encode

  $original.evaluate_anchor_interpunction
  $gold.evaluate_anchor_interpunction

  $original.content = $original.evaluate_interpunction
  $gold.content = $gold.evaluate_interpunction

  out_original = File.new("./other/outputs_statistics/outputs_SSNKF/out_original_#{j}.txt","w")
  out_original.write($original.content)
  out_original.close

  out_gold = File.new("./other/outputs_statistics/outputs_SSNKF/out_gold_#{j}.txt","w")
  out_gold.write($gold.content)
  out_gold.close

  $sum_full_stops_original += $original.full_stops
  $sum_commas_original += $original.commas
  $sum_commas_gold += $gold.commas
  $sum_full_stops_gold += $gold.full_stops
  $sum_full_stops_original_anchor += $original.full_stops_anchor
  $sum_commas_original_anchor += $original.commas_anchor
  $sum_commas_gold_anchor += $gold.commas_anchor
  $sum_full_stops_gold_anchor += $gold.full_stops_anchor

  if j >= 10000
    break
  end
end

commas_percentage = (($sum_commas_gold.to_f/$sum_commas_original.to_f)*100).to_f.round(2)
full_stops_percentage = (($sum_full_stops_gold.to_f/$sum_full_stops_original.to_f)*100).to_f.round(2)
commas_percentage_anchor = (($sum_commas_gold_anchor.to_f/$sum_commas_original_anchor.to_f)*100).to_f.round(2)
full_stops_percentage_anchor = (($sum_full_stops_gold_anchor.to_f/$sum_full_stops_original_anchor.to_f)*100).to_f.round(2)
  
puts "Results:"
puts "Commas in gold: #{$sum_commas_gold}. Commas in original #{$sum_commas_original}. Percentage #{commas_percentage}%."
puts "Fullstops in gold: #{$sum_full_stops_gold}. Fullstops in original #{$sum_full_stops_original}. Percentage #{full_stops_percentage}%"
puts "Anchor commas in gold: #{$sum_commas_gold_anchor}. Commas in original #{$sum_commas_original_anchor}. Percentage #{commas_percentage_anchor}%."
puts "Anchor fullstops in gold: #{$sum_full_stops_gold_anchor}. Fullstops in original #{$sum_full_stops_original_anchor}. Percentage #{full_stops_percentage_anchor}%"




