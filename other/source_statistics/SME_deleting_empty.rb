#!/usr/bin/env ruby
load 'other/source/Classes_for_statistics.rb'
require 'sanitize'

$dir_html = String.new("other/data/SME_dataset/original")
$dir_gold = String.new("other/data/SME_dataset/gold")

$files_html_tmp = Dir.entries($dir_html)
$files_gold_tmp = Dir.entries($dir_gold)
$files_html = Array.new
$files_gold = Array.new

for j in 0..$files_gold_tmp.length-1
  if File.ftype($dir_html+"/"+$files_html_tmp[j]) == "file"
    $files_html.push($files_html_tmp[j])
  end
  if File.ftype($dir_gold+"/"+$files_gold_tmp[j]) == "file"
    $files_gold.push($files_gold_tmp[j])
  end
end

for j in 0..$files_html.length-1
  $file_html = File.open($dir_html+"/"+$files_html[j])
  $original = $file_html.read
  $file_html.close
  if $original.to_s.length == 1
    File.delete($dir_html+"/"+$files_html[j])
    File.delete($dir_gold+"/"+$files_html[j])
	puts "DELETED FILE:" + $files_html[j]
  else
  	#puts "NOT EMPTY FILE: " + $files_html[j]
  end
end
