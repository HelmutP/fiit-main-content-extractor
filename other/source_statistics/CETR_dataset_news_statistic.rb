#!/usr/bin/env ruby
#Helmut Posch
#STU FIIT, creating statistic for CETR dataset part news

load 'other/source/Classes_for_statistics.rb'
require 'sanitize'

#run from folder BP

$dir_html = String.new("other/data/CETR_dataset/news/original")
$dir_gold = String.new("other/data/CETR_dataset/news/gold")
$dir_target = ["/bbc","/freep","/myriad","/nypost","/nytimes","/reuters","/suntimes", "/techweb","/tribune"]

$sum_full_stops_original = Array.new(9){|i| 0}
$sum_commas_original = Array.new(9){|i| 0}
$sum_commas_gold = Array.new(9){|i| 0}
$sum_full_stops_gold = Array.new(9){|i| 0}
$sum_full_stops_original_anchor = Array.new(9){|i| 0}
$sum_commas_original_anchor = Array.new(9){|i| 0}
$sum_commas_gold_anchor = Array.new(9){|i| 0}
$sum_full_stops_gold_anchor = Array.new(9){|i| 0}

for i in 0..$dir_target.length-1

	$files_html_tmp = Dir.entries($dir_html+$dir_target[i])
	$files_gold_tmp = Dir.entries($dir_gold+$dir_target[i])
	$files_html = Array.new
	$files_gold = Array.new

	puts "Evaluating directory #{$dir_target[i]}..."

	for j in 0..$files_html_tmp.length-1
		if File.ftype($dir_html+$dir_target[i]+"/"+$files_html_tmp[j]) == "file"
			$files_html.push($files_html_tmp[j])
		end
		if File.ftype($dir_gold+$dir_target[i]+"/"+$files_gold_tmp[j]) == "file"
			$files_gold.push($files_gold_tmp[j])
		end
	end
	
	$files_html.sort!
	$files_gold.sort!

	results = File.new("./other/outputs_statistics/outupts_CETR_news/result_#{i}.txt","a+")

	for j in 0..$files_html.length-1
		
		$file_html = File.open($dir_html+$dir_target[i]+"/"+$files_html[j])
		$file_gold = File.open($dir_gold+$dir_target[i]+"/"+$files_gold[j])
		$original = Html.new($file_html.read)
		$gold = Html.new($file_gold.read)
		$file_html.close
		$file_gold.close

		$original.encode
		$gold.encode

		$original.evaluate_anchor_interpunction
		$gold.evaluate_anchor_interpunction

		$original.content = $original.evaluate_interpunction
		$gold.content = $gold.evaluate_interpunction


		out_original = File.new("./other/outputs_statistics/outupts_CETR_news/out_original_#{i}_#{j}.txt","w")
		out_original.write($original.content)
		out_original.close

		out_gold = File.new("./other/outputs_statistics/outupts_CETR_news/out_gold_#{i}_#{j}.txt","w")
		out_gold.write($gold.content)
		out_gold.close

		result = String.new("#{j}: Original commas #{$original.commas}, Original full stops #{$original.full_stops},
		Gold commas #{$gold.commas}, Gold full stops #{$gold.full_stops}, Original full stops anchor #{$original.full_stops_anchor}
		, Original commas anchor #{$original.commas_anchor}, Gold full stops anchor #{$gold.full_stops_anchor},
		Gold commas anchor #{$gold.commas_anchor}.\n")
		results.write(result)

		$sum_full_stops_original[i] += $original.full_stops
		$sum_commas_original[i] += $original.commas
		$sum_commas_gold[i] += $gold.commas
		$sum_full_stops_gold[i] += $gold.full_stops
		$sum_full_stops_original_anchor[i] += $original.full_stops_anchor
		$sum_commas_original_anchor[i] += $original.commas_anchor
		$sum_commas_gold_anchor[i] += $gold.commas_anchor
		$sum_full_stops_gold_anchor[i] += $gold.full_stops_anchor
	end
end
results.close
for i in 0..$dir_target.length-1
	commas_percentage = (($sum_commas_gold[i].to_f/$sum_commas_original[i].to_f)*100).to_f.round(2)
	full_stops_percentage = (($sum_full_stops_gold[i].to_f/$sum_full_stops_original[i].to_f)*100).to_f.round(2)
	commas_percentage_anchor = (($sum_commas_gold_anchor[i].to_f/$sum_commas_original_anchor[i].to_f)*100).to_f.round(2)
	full_stops_percentage_anchor = (($sum_full_stops_gold_anchor[i].to_f/$sum_full_stops_original_anchor[i].to_f)*100).to_f.round(2)
	
	puts "Results for #{$dir_target[i]}:"
	puts "Commas in gold: #{$sum_commas_gold[i]}. Commas in original #{$sum_commas_original[i]}. Percentage #{commas_percentage}%."
	puts "Fullstops in gold: #{$sum_full_stops_gold[i]}. Fullstops in original #{$sum_full_stops_original[i]}. Percentage #{full_stops_percentage}%"
	puts "Anchor commas in gold: #{$sum_commas_gold_anchor[i]}. Commas in original #{$sum_commas_original_anchor[i]}. Percentage #{commas_percentage_anchor}%."
	puts "Anchor fullstops in gold: #{$sum_full_stops_gold_anchor[i]}. Fullstops in original #{$sum_full_stops_original_anchor[i]}. Percentage #{full_stops_percentage_anchor}%"
end



