#!/usr/bin/env ruby
#Helmut Posch
#STU FIIT, test for proefekt dataset
#run from folder BP

load '../other/source_statistics/Classes_for_statistics.rb'
require 'nokogiri'
require 'mechanize'
require 'open-uri'
require 'sanitize'
require 'fileutils'

  	def valid_url url #kontrola konektivity so zadanou url
	    begin
        	agent = Mechanize.new
        	page = agent.get(url)
	      return true
	    rescue
	      puts "Error: connection failed."
	      return false
    	end
  	end

$dir_of_dirs = String.new("./first")
$site_dirs = Dir.entries($dir_of_dirs)
$content = String.new(" ")
$url = nil

$site_dirs.delete('.')
$site_dirs.delete('..')

for dirNum in 0...$site_dirs.length
	$site_pages = Dir.entries($dir_of_dirs + "/" + $site_dirs[dirNum])

	puts "SPUSTAM TEST DATASETU " + $site_dirs[dirNum] + "..."
	sleep(3)

	FileUtils::mkdir_p $site_dirs[dirNum]
	FileUtils::mkdir_p $site_dirs[dirNum] + "/gold"
	FileUtils::mkdir_p $site_dirs[dirNum] + "/original"

	for pageNum in 0...$site_pages.length
			if File.ftype($dir_of_dirs + "/" + $site_dirs[dirNum] + "/" + $site_pages[pageNum]) == "file"

				puts "TESTUJEM SUBOR (#{pageNum} z #{$site_pages.length})"
				$content = String.new(" ")
				$url = nil
				
				source = String.new(File.read($dir_of_dirs + "/" + $site_dirs[dirNum] + "/" + $site_pages[pageNum]))
				xml_output = Nokogiri::HTML(source.to_s)
				
				features = Array.new(xml_output.css('feature'))
				features.each do |f|
					feature = Nokogiri::HTML(f.to_s)
					if feature.css('name').text.include?("responseUrl")
						$url = String.new(feature.css('value').text)
						break
					end
				end

				if valid_url $url
					annotations = Array.new(xml_output.css('annotation'))
					text = xml_output.css('text').text

					annotations.each do |anote|
					an = Nokogiri::HTML(anote.to_s)
						if an.css('type').text.include?("Fulltext")
							startchar = an.css('spanstart').text
							endchar = an.css('spanend').text
							for charNum in startchar.to_i..endchar.to_i
								$content = $content + text[charNum]
							end
						end
					end

					gold_dir = "./" + $site_dirs[dirNum] + "/gold"
					original_dir = "./" + $site_dirs[dirNum] + "/original"

					out_gold = File.new(gold_dir + "/" + pageNum.to_s + ".html","w")
        			out_gold.write($content)
        			out_gold.close

        			agent = Mechanize.new
        			page = agent.get($url)
        			out_extracted = File.new(original_dir + "/" + pageNum.to_s + ".html","w")
        			out_extracted.write(page.body)
        			out_extracted.close
				end
			end
			break if pageNum >= 500
	end
end